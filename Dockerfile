FROM markpenaranda/elasticsearch-jdbc-river

EXPOSE 9200
EXPOSE 9300
CMD ["/usr/share/elasticsearch/bin/elasticsearch", "-D", "FOREGROUND"]
