galactus
========

## Installation ##

Requirements:

- npm | nodejs
- php >= 5.4
- mysql >= 5.5
- composer

```shell
#scripts
npm install -g bower gulp

# on project root
npm install
bower install
```

All scripts are located at ```/public/app``` while stylesheets are placed at ```/public/less```.

**If** you are updating the scripts, do not forget to run (this runs the gulp tasks and watches for changes):

```shell
gulp
```


### Re Indexing of Elasticsearch ###
You must be in the app's root directory and
```shell
sh sql_indexer.sh
```
