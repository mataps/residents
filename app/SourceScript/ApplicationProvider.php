<?php

namespace SourceScript;


use SourceScript\Common\AbstractService;
use SourceScript\Common\ClassCommandMapper;
use SourceScript\Common\ClassQueryMapper;
use SourceScript\Common\CommandMapperInterface;
use SourceScript\Common\QueryMapperInterface;
use SourceScript\Common\Repositories\EloquentGenericFinder;
use Illuminate\Support\ServiceProvider;

class ApplicationProvider extends ServiceProvider{

    protected $services = array(
        'UserManagement',
        'Profiling',
        'TransactionSystem',
        'ScholarshipManagement'
    );

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() == 'local')
        {
            ini_set('display_errors', 'On');
        }

        $this->app->bind('SourceScript\Common\CommandMapperInterface', function() {
            return new ClassCommandMapper();
        });

        $this->app->bind('SourceScript\Common\QueryMapperInterface', function() {
            return new ClassQueryMapper();
        });

        foreach ($this->services as $service)
        {
            $this->registerService($service);
        }
    }

    /**
     * @param $service
     */
    private function registerService($service)
    {
        $this->app->register('SourceScript\\' . $service . '\\' . $service . 'Provider');

        $this->app->bind('SourceScript\\' . $service . '\\' . $service .'Interface', function ($app) use ($service)
        {
            /** @var AbstractService $service */
            $service = $app->make('SourceScript\\' . $service . '\\' . $service .'Service');
            /** @var CommandMapperInterface $commandMapper */
            $commandMapper = $app->make('SourceScript\Common\CommandMapperInterface');
            /** @var QueryMapperInterface $queryMapper */
            $queryMapper = $app->make('SourceScript\Common\QueryMapperInterface');

            $service->setCommandMapper($commandMapper);
            $service->setQueryMapper($queryMapper);
            $service->setContainer($app);

            return $service;
        });

        $this->app->bindShared('SourceScript\Common\Repositories\GenericFinderInterface', function()
        {
            return new EloquentGenericFinder();
        });
    }
}