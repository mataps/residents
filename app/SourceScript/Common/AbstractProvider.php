<?php

namespace SourceScript\Common;


use Illuminate\Support\ServiceProvider;

abstract class AbstractProvider extends ServiceProvider{

    protected $modules = array();

    public function boot()
    {
        foreach ($this->modules as $module => $components)
        {
            $this->registerModule($module, $components);
        }
    }

    private function getServiceName()
    {
        return str_replace('Provider', '', get_base_class($this));
    }

    private function registerModule($module, $components)
    {

        $service = $this->getServiceName();
        $appNamespace = get_base_namespace($this);

        $repositories_namespace = $appNamespace.'\\'.$service.'\\'.'Repositories';
        $validations_namespace = $appNamespace.'\\'.$service.'\\'.'Validations';

        if (isset($components['repository']))
        {
            $repositoryInterface = $repositories_namespace.'\\'.$module.'RepositoryInterface';
            $binding = $repositories_namespace.'\\'.$components['repository'];

            $this->app->bind($repositoryInterface, $binding, true);


            if (isset($components['bindings']))
            {
                foreach ($components['bindings'] as $parameter => $method)
                {
                    $this->app['router']->bind($parameter, function($value) use ($repositoryInterface, $method)
                    {
                        $repository = app()->make($repositoryInterface);
                        return $repository->{$method}($value);
                    });
                }
            }
        }

        if (isset($components['validations']))
        {
            foreach ($components['validations'] as $rule => $extension)
            {
                $this->app['validator']->extend($rule, $validations_namespace.'\\'.$extension);
            }
        }
    }
} 