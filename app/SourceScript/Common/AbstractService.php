<?php

namespace SourceScript\Common;


use Illuminate\Container\Container;
use Illuminate\Support\Facades\Event;

abstract class AbstractService implements ServiceInterface{

    /**
     * @var Container
     */
    protected $container;

    /**
     * @var CommandMapperInterface
     */
    protected $commandMapper;

    /**
     * @var QueryMapperInterface
     */
    protected $queryMapper;

    /**
     * @param Container $container
     */
    function setContainer(Container $container)
    {
        $this->container = $container;
    }

    /**
     * @param CommandMapperInterface $commandMapper
     */
    function setCommandMapper(CommandMapperInterface $commandMapper)
    {
        $this->commandMapper = $commandMapper;
    }

    /**
     * @param QueryMapperInterface $queryMapper
     */
    function setQueryMapper(QueryMapperInterface $queryMapper)
    {
        $this->queryMapper = $queryMapper;
    }

    /**
     * @param string $query
     * @param array $params
     * @param mixed [optional]
     * @return mixed
     */
    function query($query, array $params)
    {
        $class = $this->queryMapper->getClass(get_called_class(), $query);
        $instance = $this->container->make($class);

        if (method_exists($instance, 'validate'))
        {
            $instance->validate($this->container['validator'], $params);
        }

        return call_user_func_array([$instance, 'handle'], $params);
    }

    /**
     * @param string $command
     * @param array $inputs
     * @param mixed [optional]
     * @return mixed
     */
    function execute($command, array $inputs)
    {
        $params = array_slice(func_get_args(), 1);

        $class = $this->commandMapper->getClass(get_called_class(), $command);
        $instance = $this->container->make($class);

        if (method_exists($instance, 'mergeInputs'))
        {
            $params[0] = $instance->mergeInputs($inputs);
        }

        if (method_exists($instance, 'validate'))
        {
            $instance->validate($this->container['validator'], $inputs);
        }

        $method =  call_user_func_array([$instance, 'handle'], $params);

        $action = camel_case_explode($command);


        /**
         * For logging
         */
        if(isset($instance->historable))
        {
            switch ($instance->historable)
            {
                case 'add_resource':
                    Event::fire('resource.add', [$method]);
                    break;
                
                case 'remove_resource':
                    Event::fire('resource.remove', [$method]);
                    break;

                case 'update_resource':
                    Event::fire('resource.update', [$method]);
                    break;

                default:
                    # code...
                    break;
            }
        }
        
        return $method;
    }
}