<?php

namespace SourceScript\Common;


class ClassCommandMapper implements CommandMapperInterface{

    function getClass($component, $name)
    {

        $class = get_component_namespace($component);

        $baseNamespace = get_base_namespace($component);

        return $baseNamespace . "\\$class" . '\\Commands\\' . $name;
    }
} 