<?php

namespace SourceScript\Common;


class ClassQueryMapper implements QueryMapperInterface{

    function getClass($component, $name)
    {
        $class = str_replace('/', '\\', dirname(str_replace('\\', '/', $component)));

        return $class . '\\Queries\\' . $name;
    }
}