<?php

namespace SourceScript\Common\Collections;


use Illuminate\Database\Eloquent\Collection as BaseCollection;

class ResultCollection extends BaseCollection{

    private $countRows = 0;

    function setCountRows($countRows)
    {
        $this->countRows = $countRows;

        return $this;
    }

    function getCountRows()
    {
        return $this->countRows;
    }
} 