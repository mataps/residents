<?php

namespace SourceScript\Common;


interface CommandMapperInterface {

    function getClass($component, $name);
} 