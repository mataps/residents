<?php

namespace SourceScript\Common\Domains;

abstract class AbstractDomainModel {

    protected $properties = array();

    function setProperty($name, $value)
    {
        $this->__set($name, $value);
    }

    function getProperty($name)
    {
        return $this->__get($name);
    }

    function hasProperty($name)
    {
        return isset($this->properties[$name]);
    }

    function __set($name, $value)
    {
        $this->properties[$name] = $value;
    }

    function __get($name)
    {
        return $this->properties[$name];
    }
}