<?php namespace SourceScript\Common\Exceptions;

use Illuminate\Support\Contracts\ArrayableInterface;
use Illuminate\Support\Contracts\JsonableInterface;
use Illuminate\Support\MessageBag;

class ValidationException extends HttpBadRequestException implements ArrayableInterface, JsonableInterface
{
    protected $errors;

    function __construct(array $errors = array())
    {
        parent::__construct('Validation failed!');
        $this->errors = new MessageBag($errors);
    }

    public function getError($key, $format = null)
    {
        return $this->errors->get($key, $format);
    }

    /**
     * Convert the object to its JSON representation.
     *
     * @param  int $options
     * @return string
     */
    public function toJson($options = 0)
    {
        return $this->errors->toJson($options);
    }

    /**
     * Get the instance as an array.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->errors->toArray();
    }
}