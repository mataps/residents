<?php namespace SourceScript\Common\Filters;

use Illuminate\Support\Facades\Auth;
use SourceScript\Common\Exceptions\HttpUnAuthorizedException;

class Permissions {


	/**
	 * Filter according to routes
	 * 
	 * @var array
	 */
	private $filter_map = [

		'profiling' => [
			//residents
			'view_residents' => [
				'GET|api/v1/residents/{residents}',
				'GET|api/v1/residents'
			],
			'manage_residents' => [
				'POST|api/v1/residents',
				'PUT|api/v1/residents/{residents}',
				'PATCH|api/v1/residents/{residents}',
				'DELETE|api/v1/residents/{residents}',
				'DELETE|api/v1/residents/{Residents}/remove'
			],
			//households
			'view_households' => [
				'GET|api/v1/households',
				'GET|api/v1/households/{households}'
			],
			'manage_households' => [
				'POST|api/v1/households',
				'PUT|api/v1/households/{households}',
				'PATCH|api/v1/households/{households}',
				'DELETE|api/v1/households/{households}',
				'DELETE|api/v1/households/{Households}/remove'
			],
			//affiliations
			'view_affiliations' => [
				'GET|api/v1/affiliations',
				'GET|api/v1/affiliations/{affiliations}'
			],
			'manage_affiliations' => [
				'POST|api/v1/affiliations',
				'PUT|api/v1/affiliations/{affiliations}',
				'PATCH|api/v1/affiliations/{affiliations}',
				'DELETE|api/v1/affiliations/{affiliations}',
				'DELETE|api/v1/affiliations/{Affiliations}/remove'
			],
			//positions
			'view_positions' => [
				'GET|api/v1/positions',
				'GET|api/v1/positions/{positions}'
			],
			'manage_positions' => [
				'POST|api/v1/positions',
				'PUT|api/v1/positions/{positions}',
				'PATCH|api/v1/positions/{positions}',
				'DELETE|api/v1/positions/{positions}',
				'DELETE|api/v1/positions/{Positions}/remove'
			]
		],

		'scholarship_management' => [
			'view_awards' => [
				'GET|api/v1/awards',
				'GET|api/v1/awards/{awards}'
			],
			'manage_awards' => [
				'POST|api/v1/awards',
				'PUT|api/v1/awards/{awards}',
				'PATCH|api/v1/awards/{awards}',
				'DELETE|api/v1/awards/{awards}',
				'DELETE|api/v1/awards/{Awards}/remove'
			],
			'view_schools' => [
				'GET|api/v1/schools',
				'GET|api/v1/schools/{schools}'
			],
			'manage_schools' => [
				'POST|api/v1/schools',
				'PUT|api/v1/schools/{schools}',
				'PATCH|api/v1/schools/{schools}',
				'DELETE|api/v1/schools/{schools}',
				'DELETE|api/v1/schools/{Schools}/remove'
			],
			'view_semesters' => [
				'GET|api/v1/semsters',
				'GET|api/v1/semsters/{semesters}'
			],
			'manage_semesters' => [
				'POST|api/v1/semesters',
				'PUT|api/v1/semesters/{semesters}',
				'PATCH|api/v1/semesters/{semesters}',
				'DELETE|api/v1/semesters/{semesters}',
				'DELETE|api/v1/semesters/{Semesters}/remove'
			],
			'view_grades' => [
				'GET|api/v1/grades',
				'GET|api/v1/grades/{grades}'
			],
			'manage_grades' => [
				'POST|api/v1/grades',
				'PUT|api/v1/grades/{grades}',
				'PATCH|api/v1/grades/{grades}',
				'DELETE|api/v1/grades/{grades}',
				'DELETE|api/v1/grades/{Grades}/remove'
			],
			'view_allowance_policies' => [
				'GET|api/v1/policies',
				'GET|api/v1/policies/{allowance_policies}'
			],
			'manage_allowance_policies' => [
				'POST|api/v1/policies',
				'PUT|api/v1/policies/{allowance_policies}',
				'PATCH|api/v1/policies/{allowance_policies}',
				'DELETE|api/v1/policies/{allowance_policies}',
				'DELETE|api/v1/policies/{Allowance_policies}/remove'
			],
			'view_allowances' => [
				'GET|api/v1/allowances',
				'GET|api/v1/allowances/{allowances}'
			],
			'manage_allowances' => [
				'POST|api/v1/allowances',
				'PUT|api/v1/allowances/{allowances}'
				'PATCh|api/v1/allowances/{allowances}',
				'DELETE|api/v1/allowances/{allowance}',
				'DELETE|api/v1/allowances/{Allowances}/remove'
			],
			'view_allowance_releases' => [
				'GET|api/v1/releases',
				'GET|api/v1/releases/{allowance_releases}'
			],
			'manage_allowance_releases' => [
				'POST|api/v1/releases',
				'PUT|api/v1/releases/{allowance_releases}',
				'PATCH|api/v1/releases/{allowance_releases}',
				'DELETE|api/v1/releases/{allowance_releases}',
				'DELETE|api/v1/releases/{Allowance_releases}/remove'
			],
			'view_scholarship_types' => [
				'GET|api/v1/scholarshiptypes',
				'GET|api/v1/scholarshiptypes/{scholarship_types}'
			],
			'manage_scholarship_types' => [
				'POST|api/v1/scholarshiptypes',
				'PUT|api/v1/scholarshiptypes/{scholarship_types}',
				'PATCH|api/v1/scholarshiptypes/{scholarship_types}',
				'DELETE|api/v1/scholarshiptypes/{scholarship_types}',
				'DELETE|api/v1/scholarshiptypes/{Scholarship_types}/remove'
			],
			'view_subjects' => [
				'GET|api/v1/subjects',
				'GET|api/v1/subjects/{subjects}'
			],
			'manage_subjects' => [
				'POST|api/v1/subjects',
				'PUT|api/v1/subjects/{subjects}',
				'PATCH|api/v1/subjects/{subjects}',
				'DELETE|api/v1/subjects/{subjects}',
				'DELETE|api/v1/subjects/{Subjects}/remove'
			]
		],

		'transaction_system' => [
			'view_transaction_accounts' => [
				'GET|api/v1',
				'GET|api/v1'
			]
		],

		'user_management' => [
		]
	];


	/**
	 * @param  String $uri
	 * @param  array  $methods
	 */
	public function filter($uri, array $methods)
	{
		$userPermissions = Auth::user()->permissions->lists('name');

		foreach($this->filter_map as $module => $actions)
		{
			foreach($actions as $action => $permissions)
			{
				foreach($permissions as $permission)
				{
					foreach($methods as $method)
					{
						if($method . '|' . $uri == $permission && in_array($module . '.' .$action, $userPermissions))
						{
							return true;		
						}
					}
				}
			}
		}

		throw new HttpUnAuthorizedException;
	}
}