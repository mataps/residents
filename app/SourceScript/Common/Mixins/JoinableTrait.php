<?php

namespace SourceScript\Common\Mixins;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Expression;
use Schema;

trait JoinableTrait {

    /**
     * @param Builder $builder
     * @param $columns
     * @param string $relation_name
     * @param string $operator
     * @param string $type
     * @param bool $where
     * @return Builder
     */
    public function scopeModelJoin(Builder $builder, $columns, $relation_name, $operator = '=', $type = 'left', $where = false)
    {
        $instance = new static;
        $instanceTable = $instance->getTable();

        foreach ($columns as $column) {
            $builder->addSelect(new Expression("`$instanceTable`.$column"));
        }

        $relation = $instance->{$relation_name}();
        $relatedTable = $relation->getRelated()->getTable();
        $one = $relation->getRelated()->getQualifiedKeyName();
        $two = $instanceTable . '.' . $relation->getForeignKey();

        foreach (Schema::getColumnListing($relatedTable) as $related_column) {
            $builder->addSelect(new Expression("`$relatedTable`.`$related_column` AS `$relation_name.$related_column`"));
        }

        $builder->join($relatedTable, $one, $operator, $two, $type, $where);

        return $builder;
    }

    /**
     * Create a new model instance that is existing.
     *
     * @param  array  $attributes
     * @return static
     */
    public function newFromBuilder($attributes = array())
    {
        $instance = $this->newInstance(array(), true);

        $array_attributes = $this->getRelatedAttributes($instance, (array) $attributes);

        $instance->setRawAttributes($array_attributes, true);

        return $instance;
    }

    protected function getRelatedAttributes($instance, $attributes)
    {
        $joinedRelations = array();

        foreach ($attributes as $column => $value)
        {
            $dotPost = strpos($column, '.');

            if ($dotPost != false)
            {
                $this->expandAttributes($attributes, $column, $value);

                unset($attributes[$column]);

                $relation = substr($column, 0, $dotPost);

                in_array($relation, $joinedRelations) || array_push($joinedRelations, $relation);
            }
        }

        foreach ($joinedRelations as $relation)
        {
            $relatedModel = $instance->{$relation}()->getRelated();
            $attributes[$relation] = $relatedModel->newFromBuilder($attributes[$relation]);
        }

        return $attributes;
    }

    protected function expandAttributes(&$attributes, $column, $value)
    {
        $keys = explode('.', $column);

        if (count($keys) > 2)
        {
            $children = array_slice($keys, 1);
            $children = join('.', $children);
            $keys = array($keys[0], $children);
        }

        while ($key = array_shift($keys)) {
            $attributes = &$attributes[$key];
        }

        $attributes = $value;

        return $attributes;
    }
}