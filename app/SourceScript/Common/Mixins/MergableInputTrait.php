<?php

namespace SourceScript\Common\Mixins;


trait MergableInputTrait {

    function mergeInputs(array $inputs)
    {
        $defaults = isset($this->defaults) ? $this->defaults : array();

        return array_merge($defaults, $inputs);
    }
}