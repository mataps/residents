<?php namespace SourceScript\Common\Models;


use Illuminate\Database\Eloquent\Model;
use Mockery\CountValidator\Exception;
use SourceScript\Common\Collections\ResultCollection;
// use SourceScript\Common\Models\HistorableTrait;

class BaseModel extends Model{

    // use HistorableTrait;

    protected $repositories = array();

    function newCollection(array $models = array())
    {
        return new ResultCollection($models);
    }

    function removeAttribute($name)
    {
        $attr = get_unset($this, $name);

        return $attr;
    }

    function setRepository($key, $instance)
    {
        $this->repositories[$key] = $instance;

        return $this;
    }

    function getRepository($key)
    {
        if (isset($this->repositories[$key]))
        {
            return $this->repositories[$key];
        }

        throw new Exception("Repository $key not set, Please make sure to call setRepository(\$instance, \$instance)");
    }

    function getTable()
    {
        return $this->table;
    }
    

//    function save(array $attributes = null)
//    {
//        if ($attributes)
//        {
//            $instance = $this->withTrashed();
//
//            dd($instance);
//        }
//    }
}