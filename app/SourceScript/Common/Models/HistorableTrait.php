<?php namespace SourceScript\Common\Models;

trait HistorableTrait {

	function getHistoryInstance()
    {
        $newModel = new static();
        $newModel->setTable('history_' . $this->getTable());

        return $newModel;
    }

    // public function history()
    // {
    // 	return $this->morphMany('History', 'resource');
    // }
}