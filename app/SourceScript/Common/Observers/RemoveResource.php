<?php namespace SourceScript\Common\Observers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class RemoveResource {

	/**
	 * @param  mixed $model
	 * @return void
	 */
	function handle($model)
	{
		$table = $model->getTable();

		$historyTable = 'history_' . $table;

		$data = array_except($model->toArray(), 'id');

		$data = array_add($data, 'resource_id', $model->id);

		if(Schema::hasTable($historyTable))
		{
			$history_id = DB::table($historyTable)->insertGetId($data);

			DB::table('history')->insert([
				'user_id' 		=> Auth::user()->id,
				'history_id' 	=> $history_id,
				'resource_type' => get_class($model),
				'resource_id' 	=> $model->id,
				'action'		=> 'remove',
				'created_at'	=> Carbon::now(),
				'updated_at' 	=> Carbon::now()
				]);	
		}
	}
}