<?php namespace SourceScript\Common\Paginators;

use Illuminate\Support\Facades\Schema;
use SourceScript\Common\Parameters\FilterParameters;
use Illuminate\Database\Eloquent\Builder;

trait FilterableTrait {


    /**
     * @param  Builder          $query
     * @param  FilterParameters $filters
     * @return Query
     */
    public function buildFilters(Builder $query, FilterParameters $filters)
    {
        foreach ($filters->toArray() as $column => $filter)
        {
            $dotPos = strpos($column, '.');

            if ($dotPos != false)
            {
                list($relation_name, $column) = explode('.', $column, 2);

                $relation_name = camel_case($relation_name);
                
                $query->whereHas($relation_name, function($q) use ($column, $filter, $query)
                {
                    if(is_array($filter))
                    {
                        if(isset($filter['equal']))
                        {
                            if(is_array($filter['equal']))
                            {
                                $q->whereIn($column, $filter['equal']);
                            }
                            else
                            {
                                $q->where($column, $filter['equal']);    
                            }
                        }

                        if(isset($filter['from']))
                        {
                            $q->where($column, '>=', date('Y-m-d H:i:s', strtotime($filter['from'])));
                        }
                        
                        if(isset($filter['to']))
                        {
                            $q->where($column, '<=', date('Y-m-d H:i:s', strtotime($filter['to'])));
                        }

                        if(isset($filter['filter']))
                        {
                            $q->where($column, 'like', "%$filter%");
                        }

                        if(isset($filter['null']))
                        {
                            if($filter['null'] == true)
                            {
                                $q->whereNull($column);
                            }
                            else if($filter['null'] == false)
                            {
                                $q->whereNotNull($column);
                            }    
                        }    
                    }
                    else
                    {
                        if($column == "id"){
                            $q->where($column, $filter);
                        }
                        else{
                            
                            $q->where($column, 'like', "%$filter%");
                        }
                    }
                    
                });
            }
            else
            {
                if(is_array($filter))
                {
                    if(isset($filter['equal']))
                    {
                        if(is_array($filter['equal']))
                        {
                            $query->whereIn($column, $filter['equal']);
                        }
                        else
                        {
                            $query->where($column, $filter['equal']);    
                        }
                    }
                    
                    if(isset($filter['from']))
                    {
                        $query->where($column, '>=', date('Y-m-d H:i:s', strtotime($filter['from'])));
                    }
                    
                    if(isset($filter['to']))
                    {
                        $query->where($column, '<=', date('Y-m-d H:i:s', strtotime($filter['to'])));
                    }

                    if(isset($filter['filter']))
                    {
                        $query->where($column, 'like', "%$filter%");
                    }

                    if(isset($filter['null']))
                    {
                        if($filter['null'] == true)
                        {
                            $query->whereNull($column);
                        }
                        else if($filter['null'] == false)
                        {
                            $query->whereNotNull($column);
                        }    
                    }    
                }
                else
                {
                      if($column == "id"){
                            $query->where($column, $filter);
                        }
                        else{
                            
                            $query->where($column, 'like', "%$filter%");
                        }
                }                
            }
        }
        return $query;
    }
} 