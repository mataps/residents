<?php

namespace SourceScript\Common\Paginators;


use SourceScript\Common\Parameters\PaginationParameters;
use Illuminate\Database\Eloquent\Builder;

trait PaginatableTrait {

//    function buildPagination(Builder $query, $pagination)
//    {
//        $pagination['min_id'] = isset($pagination['min_id']) ? $pagination['min_id'] : 1;
//        $pagination['limit'] = isset($pagination['limit']) ? $pagination['limit'] : NULL;
//
//        if (isset($pagination['max_id']))
//        {
//            $query->whereBetween('id', [$pagination['min_id'], $pagination['max_id']]);
//        }
//        else
//        {
//            $query->where('id', '>=', $pagination['min_id']);
//        }
//
//        $query->orderBy('id', 'DESC')->take($pagination['limit']);
//    }

    function buildPagination(Builder $query, PaginationParameters $pagination)
    {
        $page = $pagination->getPage();

        $limit = $pagination->getLimit();

        return $query->skip($limit * ($page - 1))->take($limit);
    }
} 