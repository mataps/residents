<?php

namespace SourceScript\Common\Paginators;


use SourceScript\Common\Parameters\SortParameters;
use Illuminate\Database\Eloquent\Builder;

trait SortableTrait {

    function buildSorting(Builder $query, SortParameters $sort)
    {
        $sort = $sort->getSort();

        foreach ($sort as $order)
        {
            $this->sortOrder($query, $order);
        }

        return $query;
    }

    private function sortOrder($query, $sort)
    {
        $column = explode('-', $sort);

        if (count($column) == 2)
        {
            $column = $column[1];
            $order = 'DESC';
        }
        else
        {
            $column = $column[0];
            $order = 'ASC';
        }

        $dotPost = strpos($column, '.');

        if ($dotPost != false)
        {
            $relation = substr($column, 0, $dotPost);

            $relatedModel = $query->getModel()->{$relation}()->getRelated();

            $column = $relatedModel->getTable() .'.'. substr($column, $dotPost);
        }

        $query->orderBy($column, $order);
    }
} 