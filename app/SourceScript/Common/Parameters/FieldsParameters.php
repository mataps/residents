<?php

namespace SourceScript\Common\Parameters;


class FieldsParameters {

    public static $defaults = array('*');

    protected $fields = array();

    function __construct($fields)
    {
        $this->fields = !$fields ? static::$defaults : explode(',', $fields);
    }

    /**
     * @return array
     */
    public function getFields()
    {
        return $this->fields;
    }
} 