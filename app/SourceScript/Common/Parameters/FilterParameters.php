<?php

namespace SourceScript\Common\Parameters;


class FilterParameters {

    protected $filters = array();

    function __construct($filters, $allowedFilters = array())
    {
        $filters = json_decode($filters, true) ?: array();

        if ( ! empty($allowedFilters))
        {
            $this->filters = array_intersect_key($filters, array_flip($allowedFilters));
        }
        else
        {
            $this->filters = $filters;
        }
    }

    /**
     * @return array
     */
    public function getFilters()
    {
        return $this->filters;
    }

    public function toArray()
    {
        return $this->filters;
    }
} 