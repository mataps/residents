<?php

namespace SourceScript\Common\Parameters;


class PaginationParameters {

    public static $defaults = array(
        'page' => 1,
        'limit' => 15
    );

    protected $page;

    protected $limit;

    function __construct($page, $limit)
    {
        $this->page = $page ?: static::$defaults['page'];
        $this->limit = $limit ?: static::$defaults['limit'];
    }

    /**
     * @param array $inputs
     * @return static
     */
    static function createFromArray(array $inputs)
    {
        $inputs = array_merge_defaults($inputs, static::$defaults);

        return new static($inputs['page'], $inputs['limit']);
    }

    /**
     * @return mixed
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @return mixed
     */
    public function getLimit()
    {
        return $this->limit;
    }

    public function getFrom()
    {
        $page = $this->page -1;
        $page = ($page < 0 ) ? 0 : $page;

        return $this->limit * $page;
    }

    function toArray()
    {
        return [
            'page' => $this->page,
            'limit' => $this->limit
        ];
    }
} 