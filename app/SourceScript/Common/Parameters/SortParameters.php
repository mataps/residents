<?php

namespace SourceScript\Common\Parameters;


class SortParameters {

    public static $defaults = array('-id');

    protected $sort = array();

    function __construct($sort = array())
    {
        $this->sort = !$sort ? static::$defaults : explode(',', $sort);
    }

    /**
     * @return array
     */
    public function getSort()
    {
        return $this->sort;
    }
}