<?php

namespace SourceScript\Common;


interface QueryMapperInterface {

    function getClass($component, $name);
} 