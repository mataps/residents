<?php

namespace SourceScript\Common\Repositories;


use SourceScript\Common\Exceptions\DbException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class EloquentGenericFinder implements GenericFinderInterface{

    /**
     * @param Builder $builder
     * @param array $columns
     * @return Collection
     */
    function query(Builder &$builder, $columns = array('*'))
    {
        return $builder->get($columns);
    }

    function count(Builder &$builder, $columns = array('*'))
    {
        return $builder->count('*');
    }

    /**
     * @param Builder $builder
     * @param array $columns
     * @return Model
     */
    function findOne(Builder $builder, $columns = array('*'))
    {
        return $builder->first($columns);
    }

    function findOneBy(Builder $builder, array $conditions, $columns = array('*'))
    {
        foreach ($conditions as $column => $value)
        {
            $builder = $builder->where($column, $value);
        }

        if ($result = $this->findOne($builder, $columns))
        {
            return $result;
        }

        throw new DbException( get_class($builder->getModel()) . ' not found');
    }
}