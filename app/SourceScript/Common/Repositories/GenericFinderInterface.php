<?php namespace SourceScript\Common\Repositories;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface GenericFinderInterface {

    /**
     * @param Builder $builder\
     * @return Collection
     */
    function query(Builder &$builder);

    /**
     * @param Builder $builder
     * @return Model
     */
    function findOne(Builder $builder);

    function findOneBy(Builder $builder, array $conditions);
} 