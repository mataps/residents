<?php

namespace SourceScript\Common;


use Illuminate\Container\Container;

interface ServiceInterface {

    function setContainer(Container $container);

    function setCommandMapper(CommandMapperInterface $commandMapper);

    function setQueryMapper(QueryMapperInterface $queryMapper);

    /**
     * @param string $query
     * @param array $inputs
     * @param mixed [optional]
     * @return mixed
     */
    function query($query, array $inputs);

    /**
     * @param string $command
     * @param array $inputs
     * @param mixed [optional]
     * @return mixed
     */
    function execute($command, array $inputs);
} 