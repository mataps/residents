<?php

namespace SourceScript\Common\Services;


use Illuminate\Support\Collection;
use Permission;

class PermissionMerger {

    /**
     * @param Collection $basePermissions
     * @param Collection $otherPermissions
     * @return Collection
     */
    function merge(Collection $basePermissions, Collection $otherPermissions)
    {
        foreach ($basePermissions as $permission)
        {
            if ($this->isInherit($permission) && $otherPermission = $otherPermissions->find($permission))
            {
                $permission->pivot->status = $otherPermission->pivot->status;
            }
        }
        return $basePermissions;
    }

    private function isInherit(Permission $permission)
    {
        return $permission->pivot->status == 2;
    }
}