<?php

namespace SourceScript\Common\Validations;


use Exception;

trait ExistenceValidationTrait {

    function idExists($attribute, $value, $parameters = null)
    {
        try
        {
            $this->repository->findOneById($value);

            return true;
        }
        catch(Exception $e){}

        return false;
    }

    function idsExists($attribute, $value, $parameters = null)
    {
        try
        {
            foreach ($value as $id)
            {
                $this->repository->findOneById($id);
            }

            return true;
        }
        catch(Exception $e){}

        return false;
    }
}