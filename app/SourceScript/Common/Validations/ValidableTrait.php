<?php

namespace SourceScript\Common\Validations;

use SourceScript\Common\Exceptions\ValidationException;
use Illuminate\Validation\Factory as Validator;

trait ValidableTrait {

    /**
     * checks if the given data is valid using its rules attribute
     *
     * @param Validator $validator
     * @param array $inputs
     * @throws ValidationException
     */
    public function validate(Validator $validator, array $inputs)
    {
        $rules = isset($this->rules) ? $this->rules : array();
        $messages = isset($this->messages) ? $this->messages : array();

        $validator = $validator->make($inputs, $rules, $messages);

        if ($validator->fails())
        {
            throw new ValidationException($validator->messages()->toArray());
        }
    }
} 