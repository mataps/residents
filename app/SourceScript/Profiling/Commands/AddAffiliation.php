<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\Profiling\Repositories\AffiliationRepositoryInterface;
use SourceScript\Profiling\Factories\BarangayFactory;
use SourceScript\Profiling\Factories\CityMunicipalityFactory;
use SourceScript\Profiling\Factories\DistrictFactory;
use Affiliation;
use User;

class AddAffiliation {

    use ValidableTrait;

    /**
     * For logging
     * 
     * @var string
     */
    public $historable = 'add_resource';

    /**
     * Validation rules
     * 
     * @var array
     */
    protected $rules = array(
        'name'              => 'required',
        'founded'           => 'required',
        // 'district'          => 'required',
        // 'barangay'          => 'required',
        // 'cityMunicipality'  => 'required',
        // 'street'            => 'required'
    );


    /**
     * @var DistrictFactory
     */
    private $districtFactory;

    /**
     * @var CityMunicipalityFactory
     */
    private $cityMunicipalityFactory;

    /**
     * @var BarangayFactory
     */
    private $barangayFactory;

    /**
     * @var HouseholdRepositoryInterface
     */
    private $affiliationRepository;

    
    /**
     * @param AffiliationRepositoryInterface $affiliationRepository
     * @param DistrictFactory                $districtFactory
     * @param CityMunicipalityFactory        $cityMunicipalityFactory
     * @param BarangayFactory                $barangayFactory
     */
    function __construct(
        AffiliationRepositoryInterface $affiliationRepository,
        DistrictFactory $districtFactory,
        CityMunicipalityFactory $cityMunicipalityFactory,
        BarangayFactory $barangayFactory)
    {
        $this->affiliationRepository    = $affiliationRepository;
        $this->districtFactory          = $districtFactory;
        $this->cityMunicipalityFactory  = $cityMunicipalityFactory;
        $this->barangayFactory          = $barangayFactory;
    }

    /**
     * @param array $inputs
     * @param User $creator
     * @return Affiliation
     */
    function handle(array $inputs, User $creator)
    {
        $affiliation = new Affiliation();
        $affiliation->name              = $inputs['name'];
        $affiliation->description       = $inputs['description'];
        $affiliation->founded           = date('Y-m-d', strtotime($inputs['founded']));
        $affiliation->district          = $inputs['district'] ? $this->districtFactory->create( $inputs['district'] ) : null;
        $affiliation->barangay          = $inputs['barangay'] ? $this->barangayFactory->create( $inputs['barangay'] ) : null;
        $affiliation->cityMunicipality  = $inputs['cityMunicipality'] ? $this->cityMunicipalityFactory->create( $inputs['cityMunicipality'] ) : null;
        $affiliation->street            = $inputs['street'];

        $affiliation->updater        = $creator;
        $affiliation->creator       = $creator;

        $this->affiliationRepository->save($affiliation);
        
        $affiliation->positions()->sync([1]);

        return $affiliation;
    }

} 