<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Profiling\Repositories\PositionRepositoryInterface;
use Affiliation;
use AffiliationPosition;
use User;
use DB;

class AddAffiliationPositions {

	/**
	 * @var AffiliationRepositoryInterface
	 */
	private $positionRepository;


	function __construct(PositionRepositoryInterface $positionRepositoryInterface)
	{
		$this->positionRepository = $positionRepositoryInterface;
	}

	/**
	 * @param  array       $inputs
	 * @param  Affiliation $affiliation
	 * @param  User        $creator
	 * @return void
	 */
	public function handle(array $inputs, Affiliation $affiliation, User $creator)
	{
		$positions = $this->positionRepository->findPositions($inputs['positions']);

		$affiliation->positions()->sync($positions->modelKeys());

		return AffiliationPosition::where('affiliation_id', $affiliation->id)->whereIn('position_id', $positions->modelKeys())->get();
	}
}