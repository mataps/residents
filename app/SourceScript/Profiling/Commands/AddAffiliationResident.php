<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Profiling\Repositories\ResidentAffiliationRepositoryInterface;
use SourceScript\Common\Validations\ValidableTrait;
use Affiliation;
use User;
use ResidentAffiliation;

class AddAffiliationResident {
	
	use ValidableTrait;

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'referrer_id'   => 'exists:residents,id',
		'position_id' 	=> 'required|exists:positions,id',
		'resident_id' 	=> 'required|exists:residents,id',
		'founder' 		=> 'required|boolean'
		];


	/**
	 * @var ResidentAffiliationRepositoryInterface
	 */
	private $repository;


	function __construct(ResidentAffiliationRepositoryInterface $residentAffiliationRepositoryInterface)
	{
		$this->repository = $residentAffiliationRepositoryInterface;
	}


	/**
	 * @param  array       $inputs
	 * @param  Affiliation $affiliation
	 * @param  User        $creator
	 * @return ResidentAffiliation
	 */
	public function handle(array $inputs, Affiliation $affiliation, User $creator)
	{
		$residentAffiliation = ResidentAffiliation::firstOrNew(
			array_except(array_merge($inputs, ['affiliation_id' => $affiliation->id]), ['founder', 'from', 'to', 'remarks', 'status', 'referrer_id', 'head_id'])
			);

		$residentAffiliation->created_by = $creator->id;
		$residentAffiliation->modified_by = $creator->id;

		$residentAffiliation->founder = $inputs['founder'];
		$residentAffiliation->from = $inputs['from'];
		$residentAffiliation->to = $inputs['to'];
		$residentAffiliation->remarks = $inputs['remarks'];
		$residentAffiliation->status = $inputs['status'];

		$residentAffiliation->referrer_id = $inputs['referrer_id'] ? $inputs['referrer_id'] : null;
		$residentAffiliation->head_id = $inputs['head_id'] ? $inputs['head_id'] : null;

		$this->repository->save($residentAffiliation);

		return $residentAffiliation;
	}
}