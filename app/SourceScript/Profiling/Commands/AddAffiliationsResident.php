<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Profiling\Repositories\ResidentAffiliationRepositoryInterface;
use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\Common\Collections\ResultCollection;
use User;
use ResidentAffiliation;

class AddAffiliationsResident {

	use ValidableTrait;


	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'founder' 			=> 'boolean',
		'position_id' 		=> 'integer|exists:positions,id',
		'remarks' 			=> '',
		'status' 			=> '',
		'from' 				=> 'date',
		'to' 				=> 'date',
		'resident_id' 	=> 'required|integer|exists:residents,id',
		'referrer_id' 		=> 'integer|exists:residents,id',
		'head_id' 			=> 'integer|exists:residents,id'
	];


	/**
	 * @var ResidentAffiliationRepositoryInterface
	 */
	private $repository;


	function __construct(ResidentAffiliationRepositoryInterface $residentAffiliationRepository)
	{
		$this->repository = $residentAffiliationRepository;
	}


	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $affiliations
	 * @param  User             $creator
	 */
	public function handle(array $inputs, ResultCollection $affiliations, User $creator)
	{
		isset($inputs['from']) ? $inputs['from'] = date('Y-m-d', strtotime($inputs['from'])) : '';
		isset($inputs['to']) ? $inputs['to'] = date('Y-m-d', strtotime($inputs['to'])) : '';

		$inputs['position_id'] = isset($inputs['position_id']) ? $inputs['position_id'] : 1;

		foreach($affiliations as $affiliation)
		{
			$affiliations->residents()->attach($inputs['resident_id'], [
				'founder' => $inputs['founder'],
				'from' => $inputs['from'],
				'to' => $inputs['to'],
				'remarks' => $inputs['remarks'],
				'status' => $inputs['status'],
				'referrer_id' => $inputs['referrer_id'],
				'head_id' => $inputs['head_id'],
				'position_id' => $inputs['position_id'],
				'created_by' => $creator->id,
				'modified_by' => $creator->id
				]);
		}
	}
}