<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\Profiling\Repositories\AttributionRepositoryInterface;
use Attribution;
use User;

class AddAttribution {

	use ValidableTrait;

	/**
	 * For logging
	 * 
	 * @var string
	 */
	public $historable = 'add_resource';

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'name' 		=> 'required',
		'label' 	=> 'required',
		'class' 	=> array('regex:(^[A-Fa-f0-9]{6}|[A-Fa-f0-9]{3}$)', 'required')
	];


	/**
	 * @var AttributionRepositoryInterface
	 */
	private $repository;

	function __construct(AttributionRepositoryInterface $attributionRepositoryInterface)
	{
		$this->repository = $attributionRepositoryInterface;
	}


	/**
	 * @param  array  $inputs
	 * @param  User   $creator
	 * @return Attribution
	 */
	public function handle(array $inputs, User $creator)
	{
		$attribution = new Attribution;

		$attribution->fill($inputs);

		$attribution->modified_by = $creator->id;
		$attribution->created_by = $creator->id;

		$this->repository->save($attribution);

		return $attribution;
	}
}