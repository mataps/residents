<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Common\Collections\ResultCollection;
use User;
use ResidentAttribute;
use SourceScript\Common\Validations\ValidableTrait;

class AddAttributionsResident {

	use ValidableTrait;


	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'from' => 'date',
		'to' => 'date',
		'resident_id' => 'required|integer|exists:residents,id'
	];


	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $attributions
	 * @param  User             $creator
	 */
	public function handle(array $inputs, ResultCollection $attributions, User $creator)
	{
		isset($inputs['from']) ? $inputs['from'] = date('Y-m-d', strtotime($inputs['from'])) : '';
		isset($inputs['to']) ? $inputs['to'] = date('Y-m-d', strtotime($inputs['to'])) : '';
		
		foreach($attributions as $attribute)
		{
			$residentAttribute = ResidentAttribute::firstOrNew(
				array_except(array_add($inputs, 'attribute_id', $attribute->id), ['from', 'to'])
				);

			$residentAttribute->fill(array_add($inputs, 'attribute_id', $attribute->id));

			$residentAttribute->created_by = $creator->id;
			$residentAttribute->modified_by = $creator->id;

			$residentAttribute->save();
		}
	}
}