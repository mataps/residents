<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\Profiling\Repositories\HouseholdRepositoryInterface;
use SourceScript\Profiling\Factories\BarangayFactory;
use SourceScript\Profiling\Factories\CityMunicipalityFactory;
use SourceScript\Profiling\Factories\DistrictFactory;
use Household;
use User;

class AddHousehold {

    use ValidableTrait;


    /**
     * For logging
     * 
     * @var string
     */
    public $historable = 'add_resource';

    /**
     * Validation rules
     * 
     * @var array
     */
    protected $rules = array(
        'label'             => 'required',
        'phone'             => 'required',
        'district'          => 'required',
        'barangay'          => 'required',
        'cityMunicipality'  => 'required',
        'street'            => 'required'
    );


    /**
     * @var DistrictFactory
     */
    private $districtFactory;

    /**
     * @var CityMunicipalityFactory
     */
    private $cityMunicipalityFactory;

    /**
     * @var BarangayFactory
     */
    private $barangayFactory;

    /**
     * @var HouseholdRepositoryInterface
     */
    private $householdRepository;


    /**
     * @param HouseholdRepositoryInterface $householdRepository
     * @param DistrictFactory              $districtFactory
     * @param CityMunicipalityFactory      $cityMunicipalityFactory
     * @param BarangayFactory              $barangayFactory
     */
    function __construct(
        HouseholdRepositoryInterface $householdRepository,
        DistrictFactory $districtFactory,
        CityMunicipalityFactory $cityMunicipalityFactory,
        BarangayFactory $barangayFactory)
    {
        $this->householdRepository = $householdRepository;
        $this->districtFactory          = $districtFactory;
        $this->cityMunicipalityFactory  = $cityMunicipalityFactory;
        $this->barangayFactory          = $barangayFactory;
    }

    /**
     * @param array $inputs
     * @param User $creator
     * @return Household
     */
    function handle(array $inputs, User $creator)
    {
        $household = new Household();

        $household->label = $inputs['label'];
        $household->phone = $inputs['phone'];

        $household->district            = $this->districtFactory->create( $inputs['district'] );
        $household->barangay            = $this->barangayFactory->create( $inputs['barangay'] );
        $household->cityMunicipality    = $this->cityMunicipalityFactory->create( $inputs['cityMunicipality'] );
        $household->street              = $inputs['street'];

        $household->creator             = $creator;
        $household->updater             = $creator;

        $this->householdRepository->save($household);

        return $household;
    }

} 