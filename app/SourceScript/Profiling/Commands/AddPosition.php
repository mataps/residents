<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Profiling\Repositories\PositionRepositoryInterface;
use SourceScript\Profiling\Repositories\AffiliationRepositoryInterface;
use SourceScript\Profiling\Factories\AttributionsFactory;
use SourceScript\Common\Validations\ValidableTrait;
use User;
use Position;

class AddPosition {
		
	use ValidableTrait;

	/**
     * For logging
     * 
     * @var string
     */
    public $historable = 'add_resource';

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'name' => 'required|unique:positions,name'
	];


	/**
	 * @var PositionRepositoryInterface
	 */
	private $positionRepository;


	/**
	 * @var AffiliationRepositoryInterface
	 */
	private $affiliationRepository;


	/**
	 * @var AttributionsFactory
	 */
	private $attributionsFactory;


	function __construct(
		PositionRepositoryInterface $positionRepositoryInterface,
		AffiliationRepositoryInterface $affiliationRepositoryInterface,
		AttributionsFactory $attributionsFactory)
	{
		$this->positionRepository = $positionRepositoryInterface;
		$this->affiliationRepository = $affiliationRepositoryInterface;
		$this->attributionsFactory = $attributionsFactory;
	}


	/**
	 * @param  array  $inputs
	 * @param  User   $creator
	 * @return Position
	 */
	public function handle(array $inputs, User $creator)
	{
		$position = new Position;

		$position->name = $inputs['name'];
		$position->created_by = $creator->id;
		$position->modified_by = $creator->id;

		$this->positionRepository->save($position);

		$affiliations = $this->affiliationRepository->findAffiliations($inputs['affiliations']);

		$position->affiliations()->attach($affiliations->modelKeys());

		// Attach to attribution
		$attributions = $this->attributionsFactory->get($inputs['attributions'], $creator);
		$position->attributions()->attach($attributions->modelKeys());

		return $position;
	}
}