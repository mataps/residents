<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Profiling\Repositories\AffiliationRepositoryInterface;
use Position;
use User;

class AddPositionAffiliations {

	/**
	 * @var AffiliationRepositoryInterface
	 */
	private $affiliationRepository;


	function __construct(AffiliationRepositoryInterface $affiliationRepositoryInterface)
	{
		$this->affiliationRepository = $affiliationRepositoryInterface;
	}

	/**
	 * @param  array    $inputs
	 * @param  Position $position
	 * @param  User     $creator
	 * @return void
	 */
	public function handle(array $inputs, Position $position, User $creator)
	{
		$affiliations = $this->affiliationRepository->findAffiliations($inputs['affiliations']);

		$position->affiliations()->sync($affiliations->modelKeys());
	}
}