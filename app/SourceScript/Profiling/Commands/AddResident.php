<?php

namespace SourceScript\Profiling\Commands;

use Illuminate\Events\Dispatcher;
use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\Profiling\Factories\BarangayFactory;
use SourceScript\Profiling\Factories\CityMunicipalityFactory;
use SourceScript\Profiling\Factories\DistrictFactory;
use SourceScript\Profiling\Factories\PhotoFactory;
use SourceScript\Profiling\Repositories\ResidentRepositoryInterface;
use Resident;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use User;

/**
 * States that I'm gay
 * @author  JC Ricaro <jc@seancody.com>
 * @repository https://github.com/seancody/jc-gay.git
 * @bae Jeff Zhang
 * @bae Gian
 * @bae Mark
 * @bae Kier//
 */

class AddResident {

    use ValidableTrait;


    /**
     * For logging
     * 
     * @var string
     */
    public $historable = 'add_resource';


    /**
     * Validation rules
     * 
     * @var array
     */
    protected $rules = array(
        'first_name' => 'required',
        'last_name' => 'required',
        'civil_status' => 'required|in:Single,Married,Widowed,Divorced,Deceased',
        'gender' => 'required|in:Male,Female',
        'cityMunicipality' => 'required',
        'district' => 'required',
        'barangay' => 'required',
        // 'street' => 'required'
    );

    /**
     * @var ResidentRepositoryInterface
     */
    private $residentRepository;

    /**
     * @var PhotoFactory
     */
    private $photoFactory;

    /**
     * @var DistrictFactory
     */
    private $districtFactory;

    /**
     * @var CityMunicipalityFactory
     */
    private $cityMunicipalityFactory;

    /**
     * @var BarangayFactory
     */
    private $barangayFactory;
    
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     * @param ResidentRepositoryInterface $residentRepository
     * @param PhotoFactory $photoFactory
     * @param DistrictFactory $districtFactory
     * @param CityMunicipalityFactory $cityMunicipalityFactory
     * @param BarangayFactory $barangayFactory
     */
    function __construct(
        Dispatcher $eventDispatcher,
        ResidentRepositoryInterface $residentRepository,
        PhotoFactory $photoFactory,
        DistrictFactory $districtFactory,
        CityMunicipalityFactory $cityMunicipalityFactory,
        BarangayFactory $barangayFactory
    )
    {
        $this->residentRepository       = $residentRepository;
        $this->photoFactory             = $photoFactory;
        $this->districtFactory          = $districtFactory;
        $this->cityMunicipalityFactory  = $cityMunicipalityFactory;
        $this->barangayFactory          = $barangayFactory;
        $this->eventDispatcher          = $eventDispatcher;
    }

    /**
     * @param array $inputs
     * @param User $creator
     * @return Resident
     */
    function handle(array $inputs, User $creator)
    {

        $resident = new Resident;
        $resident->is_voter = $inputs['is_voter'] ? $inputs['is_voter'] : false;
        $resident->first_name   = $inputs['first_name'];
        $resident->last_name    = $inputs['last_name'];
        $resident->middle_name  = $inputs['middle_name'];
        $resident->civil_status = $inputs['civil_status'];
        $resident->voters_id    = $inputs['voters_id'];
        $resident->gender       = $inputs['gender'];
        $resident->birthdate    = date('Y-m-d', strtotime($inputs['birthdate']));
        $resident->mobile       = $inputs['mobile'];
        $resident->mobile_1     = $inputs['mobile_1'];
        $resident->mobile_2     = $inputs['mobile_2'];
        $resident->notes     = $inputs['notes'];
        $resident->phone        = $inputs['phone'];
        $resident->email        = $inputs['email'];
        $resident->street       = $inputs['street'];
        $resident->signature_path = $inputs['signature_path'];
        $resident->atm_number   = $inputs['atm_number'];
        $resident->bank         = $inputs['bank'];
        $resident->occupation = $inputs['occupation'];
        $resident->nickname = $inputs['nickname'];
        $resident->creator      = $creator;
        $resident->updater      = $creator;
        // $resident->blacklisted_at = date('Y-m-d', strtotime($inputs['blacklisted_at']));
        // $resident->deceased_at = date('Y-m-d', strtotime($inputs['deceased_at']));
        $resident->profilePic   = $this->photoFactory->create( $inputs['profile_image'], $inputs['cityMunicipality'] );
        $resident->district     = $this->districtFactory->create( $inputs['district'] );
        $resident->barangay     = $this->barangayFactory->create( $inputs['barangay'] );
        $resident->cityMunicipality = $this->cityMunicipalityFactory->create( $inputs['cityMunicipality'] );

        $this->residentRepository->save($resident);

        //TODO: Refactor this to events
        // $this->residentRepository->incrementLastNames([ $resident->last_name, $resident->middle_name ]);

        return $resident;
    }
}