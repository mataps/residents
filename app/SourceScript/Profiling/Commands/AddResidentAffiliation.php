<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Profiling\Repositories\ResidentAffiliationRepositoryInterface;
use SourceScript\Profiling\Repositories\PositionRepositoryInterface;
use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\Common\Exceptions\ValidationException;
use Affiliation;
use User;
use Resident;
use ResidentAffiliation;

class AddResidentAffiliation {

    use ValidableTrait;


    /**
     * Validation rules
     * 
     * @var array
     */
    protected $rules = [
        'referrer_id'       => 'exists:residents,id',
        'founder'           => 'required|boolean',
        'position_id'       => 'exists:positions,id',
        'affiliation_id'    => 'required|exists:affiliations,id',
        'from'              => 'date',
        'to'                => 'date',
        'founder'           => 'boolean'
        ];


    /**
     * 
     * @var ResidentAffiliationRepositoryInterface
     */
    private $repository;


    function __construct(ResidentAffiliationRepositoryInterface $residentAffiliationRepositoryInterface,
        PositionRepositoryInterface $positionRepositoryInterface)
    {
        $this->repository = $residentAffiliationRepositoryInterface;
        $this->positionRepository = $positionRepositoryInterface;
    }


    /**
     * @param  array       $inputs
     * @param  Resident    $resident
     * @param  Affiliation $affiliation
     * @param  User        $creator
     * @return ResidentAffiliation
     */
    function handle(array $inputs, Resident $resident, User $creator)
    {
        if(ResidentAffiliation::where('resident_id', $resident->id)->where('affiliation_id', $inputs['affiliation_id'])->count() > 0)
        {
            throw new ValidationException(['Resident' => 'Resident already a member of affiliation']);
        }

        $residentAffiliation = ResidentAffiliation::firstOrNew(
            array_except(array_add($inputs, 'resident_id', $resident->id), ['founder', 'from', 'to', 'remarks', 'status', 'referrer_id', 'head_id'])
            );

        $residentAffiliation->created_by    = $creator->id;
        $residentAffiliation->modified_by   = $creator->id;

        $residentAffiliation->founder       = $inputs['founder'];
        $residentAffiliation->from          = $inputs['from'];
        $residentAffiliation->to            = $inputs['to'];
        $residentAffiliation->remarks       = $inputs['remarks'];
        $residentAffiliation->status        = $inputs['status'];

        $residentAffiliation->head_id       = $inputs['head_id'] ? $inputs['head_id'] : null;
        $residentAffiliation->referrer_id   = $inputs['referrer_id'] ? $inputs['referrer_id'] : null;

        $this->repository->save($residentAffiliation);

        if($inputs['position_id'])
        {
            $position = $this->positionRepository->findOneById($inputs['position_id']);

            $resident->attributions()->detach($position->attributions->modelKeys());

            $resident->attributions()->attach($position->attributions->modelKeys());    
        }
        

        return $residentAffiliation;
    }

} 