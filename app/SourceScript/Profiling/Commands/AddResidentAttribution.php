<?php namespace SourceScript\Profiling\Commands;

use Attribution;
use ResidentAttribute;
use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\Profiling\Repositories\ResidentAffiliationRepositoryInterface;
use ResidentAffiliation;
use User;

class AddResidentAttribution {

    use ValidableTrait;

    protected $rules = array(
    );

    /**
     * @var HouseholdRepositoryInterface
     */
    private $affiliationRepository;

    /**
     * @param HouseholdRepositoryInterface $affiliationRepository
     */
    function __construct()
    {
    }

    /**
     * @param array $inputs
     * @param User $creator
     * @return Affiliation
     */
    function handle(array $inputs, $resident, $user)
    {
        if(ResidentAttribute::where('resident_id', $resident->id)->where('attribute_id', Attribution::where('name', $inputs['attribute_name'])->first()->id)->count() > 0)
        {
            throw new ValidationException(['Resident' => 'Resident already a member of attribution']);
        }

        $residentAttribute = new ResidentAttribute();
        $residentAttribute->attribute_id = Attribution::where('name', $inputs['attribute_name'])->first()->id;
        $residentAttribute->resident_id = $resident->id;

        if ( isset($inputs['from']) ) {
            $residentAttribute->from = date('Y-m-d', strtotime($inputs['from']));
        }

        if ( isset($inputs['to']) ) {
            $residentAttribute->to = date('Y-m-d', strtotime($inputs['to']));
        }

        $residentAttribute->save();

        return $residentAttribute;
    }

} 