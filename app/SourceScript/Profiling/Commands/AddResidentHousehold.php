<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use Household;
use Resident;
use User;
use Illuminate\Support\Facades\DB;

class AddResidentHousehold {

	use ValidableTrait;

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'role' => 'required',
		'household_id' => 'required|exists:households,id'
	];


	/**
	 * @param  array     $inputs
	 * @param  Household $household
	 * @param  Resident  $resident
	 * @param  User      $creator
	 * @return HouseholdResident
	 */
	function handle(array $inputs, Resident $resident, User $creator)
	{
		$household = $inputs['household_id'];

		unset($inputs['household_id']);

		if($inputs['role'] == "owner")
		{
			DB::table('households_residents')->whereIn('household_id', [$household])->update(['role' => 'member']);
		}

		$resident->households()->sync([$household => array_merge($inputs, ['created_by' => $creator->id, 'modified_by' => $creator->id])]);
	}
}