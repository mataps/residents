<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use User;
use Resident;

class AddResidentRelation {

	use ValidableTrait;

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'resident_id' => 'required|exists:residents,id',
		'relation' => 'required'
	];

	/**
	*	Relationship Linking Variable
	*
	*/ 
	public $relation_link = [
		'spouse' => ['Male' => 'spouse', 'Female' => 'spouse'],
		'guardian' => ['Male' => 'ward', 'Female' => 'ward'],
		'father' => ['Male' => 'son', 'Female' => 'daughter'],
		'mother' => ['Male' => 'son', 'Female' => 'daughter'],
		'grandfather' => ['Male' => 'grandson', 'Female' => 'granddaughter'],
		'grandmother' => ['Male' => 'grandson', 'Female' => 'granddaughter'],
		'grandson' => ['Male' => 'grandfather', 'Female' => 'grandmother'],
		'granddaughter' => ['Male' => 'grandfather', 'Female' => 'grandmother'],
		'son' => ['Male' => 'father', 'Female' => 'mother'],
		'daughter' => ['Male' => 'father', 'Female' => 'mother'],
		'cousin' => ['Male' => 'cousin', 'Female' => 'cousin'],
		'aunt' => ['Male' => 'nephew', 'Female' => 'niece'],
		'uncle' => ['Male' => 'nephew', 'Female' => 'niece'],
		'nephew' => ['Male' => 'uncle', 'Female' => 'aunt'],
		'niece' => ['Male' => 'uncle', 'Female' => 'aunt'],
		'sister' => ['Male' => 'brother', 'Female' => 'sister'],
		'brother' => ['Male' => 'brother', 'Female' => 'sister'],
		'brother-in-law' => ['Male' => 'brother-in-law', 'Female' => 'sister-in-law'],
		'sister-in-law' => ['Male' => 'brother-in-law', 'Female' => 'sister-in-law'],
		'mother-in-law' => ['Male' => 'son-in-law', 'Female' => 'daughter-in-law'],
		'father-in-law' => ['Male' => 'son-in-law', 'Female' => 'daughter-in-law'],
		'son-in-law' => ['Male' => 'father-in-law', 'Female' => 'mother-in-law'],
		'daughter-in-law' => ['Male' => 'father-in-law', 'Female' => 'mother-in-law']


	];



	/**
	 * @param  array    $inputs
	 * @param  Resident $resident
	 * @param  User     $creator
	 */
	public function handle(array $inputs, Resident $l_resident, User $creator)
	{
		$resident = Resident::find($l_resident->id);

		$resident->relatives()->detach($inputs['resident_id']);
		$relation = $inputs['relation'];
		$resident->relatives()->attach($inputs['resident_id'], [
			'relation' => $relation,
			'created_by' => $creator->id,
			'modified_by' => $creator->id
			]);


		$link_resident = Resident::find($inputs['resident_id']);
		$gender = $resident->gender;

		$link_resident->relatives()->attach($resident->id, [
					'relation' => $this->relation_link[$relation][$gender],
					'created_by' => $creator->id,
					'modified_by' => $creator->id
			]);



	}
}