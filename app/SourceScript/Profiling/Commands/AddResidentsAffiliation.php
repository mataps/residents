<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Profiling\Repositories\ResidentAffiliationRepositoryInterface;
use SourceScript\Profiling\Repositories\PositionRepositoryInterface;
use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\Common\Collections\ResultCollection;
use User;
use ResidentAffiliation;
use SourceScript\Profiling\ProfilingInterface;

class AddResidentsAffiliation {

	use ValidableTrait;

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'founder' 			=> 'boolean',
		'position_id' 		=> 'integer|exists:positions,id',
		'remarks' 			=> '',
		'status' 			=> '',
		'from' 				=> '',
		'to' 				=> '',
		'affiliation_id' 	=> 'required|integer|exists:affiliations,id',
		'referrer_id' 		=> 'integer|exists:residents,id',
		'head_id' 			=> 'integer|exists:residents,id'
	];


	/**
	 * @var ResidentAffiliationRepositoryInterface
	 */
	private $repository;


	/**
	 * @var PositionRepositoryInterface
	 */
	private $positionRepository;


	/**
	 * @var ProfilingInterface
	 */
	private $profilingService;


	function __construct(
		ProfilingInterface $profilingServiceInterface,
		ResidentAffiliationRepositoryInterface $residentAffiliationRepository,
		PositionRepositoryInterface $positionRepositoryInterface)
	{
		$this->profilingService = $profilingServiceInterface;
		$this->repository = $residentAffiliationRepository;
		$this->positionRepository = $positionRepositoryInterface;

	}

	
	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $residents
	 * @param  User             $creator
	 */
	public function handle(array $inputs, ResultCollection $residents, User $creator)
	{
		isset($inputs['from']) ? $inputs['from'] = date('Y-m-d', strtotime($inputs['from'])) : '';
		isset($inputs['to']) ? $inputs['to'] = date('Y-m-d', strtotime($inputs['to'])) : '';

		$inputs['position_id'] = isset($inputs['position_id']) ? $inputs['position_id'] : 1;
		
		foreach($residents as $resident)
		{
			$resident->affiliations()->attach($inputs['affiliation_id'], [
				'founder' => $inputs['founder'],
				'from' => $inputs['from'],
				'to' => $inputs['to'],
				'remarks' => $inputs['remarks'],
				'status' => $inputs['status'],
				'referrer_id' => $inputs['referrer_id'],
				'head_id' => $inputs['head_id'],
				'position_id' => $inputs['position_id'],
				'created_by' => $creator->id,
				'modified_by' => $creator->id
				]);

			if($inputs['position_id'])
			{
				$position = $this->positionRepository->findOneById($inputs['position_id']);

				$resident->attributions()->detach($position->attributions->lists('id'));

		            	$resident->attributions()->attach($position->attributions->lists('id'));    
			}

			$this->profilingService->execute('UpdateIndexResident', $inputs, $resident, $resident->id);
		}
	}
}
