<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\Common\Collections\ResultCollection;
use User;
use ResidentAttribute;
use SourceScript\Profiling\Repositories\AttributionRepositoryInterface;

class AddResidentsAttribution {

	use ValidableTrait;

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'attribute_id' => 'required|integer|exists:attributions,id'
	];

	/**
	 * @var AttributionRepository
	 */
	private $attributionRepository;

	function __construct(AttributionRepositoryInterface $attributionRepositoryInterface)
	{
		$this->attributionRepository = $attributionRepositoryInterface;
	}


	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $residents
	 * @param  User             $creator
	 */
	public function handle(array $inputs, ResultCollection $residents, User $creator)
	{
		isset($inputs['from']) ? $inputs['from'] = date('Y-m-d', strtotime($inputs['from'])) : '';
		isset($inputs['to']) ? $inputs['to'] = date('Y-m-d', strtotime($inputs['to'])) : '';
		
		foreach($residents as $resident)
		{
			$residentAttribute = ResidentAttribute::firstOrNew(
				array_except(array_add($inputs, 'resident_id', $resident->id), ['from', 'to'])
				);

			$residentAttribute->fill(array_add($inputs, 'resident_id', $resident->id));

			$residentAttribute->created_by = $creator->id;
			$residentAttribute->modified_by = $creator->id;

			$residentAttribute->save();
		}

		$attribution = $this->attributionRepository->findOneById($inputs['attribute_id']);

		return $attribution;
	}
}