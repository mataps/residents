<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\Common\Collections\ResultCollection;
use Household;
use Resident;
use User;
use ResidentHousehold;

class AddResidentsHousehold {

	use ValidableTrait;

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'household_id' => 'required|exists:households,id'
	];


	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $residents
	 * @param  User             $creator
	 */
	public function handle(array $inputs, ResultCollection $residents, User $creator)
	{
		$inputs['role'] = isset($inputs['role']) ? $inputs['role'] : 'member';

		foreach($residents as $resident)
		{
			$residentHousehold = ResidentHousehold::firstOrNew(
				array_except(array_add($inputs, 'resident_id', $resident->id), ['role']));

			$residentHousehold->fill(array_add($inputs, 'resident_id', $resident->id));

			$residentHousehold->created_by = $creator->id;
			$residentHousehold->modified_by = $creator->id;

			$residentHousehold->save();
		}
	}
}