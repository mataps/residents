<?php 

namespace SourceScript\Profiling\Commands;

use Config;
use DB;
use Resident;
use Photo;
use Affiliation;
use Position;

class AssignAffiliation {


    function handle()
    {
      
        $old_aff_sql = 'select a.attribute_name as aff_name, r.fullname as fullname, r.city as city, r.barangay_name as barangay_name, ad.designation_name as designation_name, ad.OID as designation_id from cdodb_base.`attribute_resident_detail` as ard join cdodb_base.resident_fast as r on r.OID = ard.resident_OID join cdodb_base.attribute as a on ard.attribute_OID =  a.OID left join cdodb_base.attribute_affiliate_extension as aae on ard.OID = aae.`attribute_resident_detail_OID` left join cdodb_base.affiliate_designation as ad on ad.OID = aae.`affiliate_designation_OID` where a.attribute_name != "";';




        $old_aff_record = DB::select(DB::raw($old_aff_sql));

        $total = count ($old_aff_record);
        $counter=0;

        foreach ($old_aff_record as $old_record) {
            if(!$old_record){
                continue;
            }

            $counter++;
            echo $counter . "/" . $total . "\r\n";

           
            $position = Position::where('name', $old_record->designation_name)->first();
            $aff = Affiliation::where('name', $old_record->aff_name)->first();
            $resident = Resident::where('full_name', $old_record->fullname)
                                ->whereNotNull('barangay_id')
                                // ->whereHas('barangay', function($subQuery) use($old_record)
                                // {
                                //     $subQuery->where('name', $old_record->barangay_name);
                                // })
                                
                                 ->whereHas('cityMunicipality', function($subQuery)  use($old_record)
                                {
                                    $subQuery->where('name', $old_record->city);
                                })                               
                                ->orderBy('is_voter', 'desc')
                                ->first();
            if($resident){
            if($position){
                 $resident->affiliations()->attach([$aff->id => ['position_id' => $position->id]]);
            }
            else{
                     $resident->affiliations()->attach([$aff->id => []]);
            }               
            }
 

	   else{
		echo $old_record->city;
		}
 



        }
    }
}
