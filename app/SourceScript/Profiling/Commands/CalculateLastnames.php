<?php

namespace SourceScript\Profiling\Commands;

use Resident;
use SourceScript\Profiling\Repositories\ResidentRepositoryInterface;

class CalculateLastnames {

    private $residentRepository;

    function __construct(ResidentRepositoryInterface $residentRepository)
    {
        $this->residentRepository = $residentRepository;
    }

    function handle()
    {
        $lastnamesResult = $this->residentRepository->countLastNames();

        $this->residentRepository->resetLastNameCounts($lastnamesResult);
    }
}