<?php

namespace SourceScript\Profiling\Commands;

use Resident;
use Elasticsearch\Client;

class IndexResident {

    protected $client;

    function handle($input, Resident $resident)
    {           
            $param = \Config::get('elasticsearch');
            $this->client = new Client($param);

            $district = ($resident->district) ? $resident->district->name : '';
            $barangay = ($resident->barangay) ? $resident->barangay->name : ''; 
            $cityMunicipality = ($resident->cityMunicipality) ? $resident->cityMunicipality->name : '';

            $district = ($resident->district) ? $resident->district->name : '';
            $barangay = ($resident->barangay) ? $resident->barangay->name : ''; 

            $params['body'] = array(
                    'nickname' => $resident->nickname,
                    'occupation' => $resident->occupation,
                    'district'  => $district,
                    'barangay'  => $barangay,
                    'cityMunicipality'  => $cityMunicipality,
                    'first_name' => $resident->first_name,
                    'last_name' => $resident->last_name,
                    'middle_name' => $resident->middle_name,
                    'full_name' => $resident->first_name . ' ' . $resident->middle_name . ' ' . $resident->last_name,
                    'email' => $resident->email,
                    'mobile' => $resident->mobile,
                    'mobile_1' => $resident->mobile_1,
                    'mobile_2' => $resident->mobile_2,
                    'phone' => $resident->phone,
                    'precint' => $resident->precint,
                    'atm_number' => $resident->atm_number,
                    'bank' => $resident->bank,
                    'birthdate' => date("Y-m-d", strtotime($resident->birthdate)),
                    'gender' => $resident->gender,
                    'civil_status' => $resident->civil_status,
                    'street' => $resident->street,
                    'notes' => $resident->notes,
                    'occupation' => $resident->occupation,
                    'nickname' => $resident->nickname,
                    'creator' => $resident->creator,
                    'updater' => $resident->updater,
                    'attributes' => $resident->attributes,
                    'updated_at' => $resident->updated_at,
                    'created_at' => $resident->created_at,
                    'blacklisted_at' => $resident->blacklisted_at,
                    'is_voter' => (int) $resident->is_voter,
                    'deceased_at' => $resident->deceased_at,
                    'id' => $resident->id,
                    'affiliations' => $resident->affiliations->lists('name'),
                    'attributions' => $resident->attributes->lists('name')
                );
            $params['id'] = $resident->id;
            $params['index'] = 'galactus_production';
            $params['type']  = 'residents';
        
            $this->client->index($params);
            

          return $resident;
    }
}