<?php 

namespace SourceScript\Profiling\Commands;

use Config;
use DB;
use Resident;
use Position;
use Photo;
use CityMunicipality;
use Excel;

class MigrateAffiliationFromExcel {


    function handle()
    {
      
        Excel::load('affiliation.xlsx', function($reader){

            $members = $reader->all();

            $total = count($members);
            $counter = 0;
            foreach ($members as $member) {
                
                $full_name = $member->name;
                $member_name_tokenizer = explode(",", $member->name);
                if(count($member_name_tokenizer) > 1){

                $full_name = $member_name_tokenizer[1] . " " . $member_name_tokenizer[0];   

                $full_name = trim($full_name);
                }


                $city = $member->municipalitycity;
                $barangay = $member->barangay;
                $position = $member->position;


                echo $full_name;

                $city_db = CityMunicipality::where('name', $city)->first();

              

                $position = Position::where('name', $position)->first();

            $resident = Resident::where('full_name', $full_name)
                                ->whereNotNull('barangay_id')
                                ->whereHas('barangay', function($subQuery) use($barangay)
                                {
                                    $subQuery->where('name', $barangay);
                                })
                                
                                 ->whereHas('cityMunicipality', function($subQuery)  use($city)
                                {
                                    $subQuery->where('name', $city);
                                })                               
                                ->orderBy('is_voter', 'desc')
                                ->first();

                // if()


                if($resident){
                    $affiliation_id = 44;
                    $counter++;
                    echo $counter . "/" . $total;
                    if($position){
                      $resident->affiliations()->attach([$affiliation_id => ['position_id' => $position->id]]);
                 }
                  else{
                     $resident->affiliations()->attach([$affiliation_id => []]);
                     }   

                }
                

            }



        });







        }
    }
