<?php 

namespace SourceScript\Profiling\Commands;

use Config;
use DB;
use Resident;
use Photo;


class MigrateHousehold {


    function handle()
    {
      
        $old_hh_sql = 'select r.fullname as fullname, r.city as city, r.barangay_name as barangay_name, brd.building_OID as household_id from cdodb_base.`building_resident_detail` as brd join cdodb_base.resident_fast as r on r.OID = brd.resident_OID;';




        $old_hh_record = DB::select(DB::raw($old_hh_sql));

        $total = count ($old_hh_record);
        $counter=0;

        foreach ($old_hh_record as $old_record) {
            if(!$old_record){
                continue;
            }

            $counter++;
            echo $counter . "/" . $total . "\r\n";

           
  
            // $aff = Household::find($old_record->household_id);
            $resident = Resident::where('full_name', $old_record->fullname)
                                ->whereNotNull('barangay_id')
                                // ->whereHas('barangay', function($subQuery) use($old_record)
                                // {
                                //     $subQuery->where('name', $old_record->barangay_name);
                                // })
                                
                                 ->whereHas('cityMunicipality', function($subQuery)  use($old_record)
                                {
                                    $subQuery->where('name', $old_record->city);
                                })                               
                                ->orderBy('is_voter', 'desc')
                                ->first();
            if($resident){
      
                     $resident->households()->attach([$old_record->household_id => []]);
                           
            }
 



        }
    }
}