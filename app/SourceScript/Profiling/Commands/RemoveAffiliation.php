<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Profiling\Repositories\AffiliationRepositoryInterface;
use Affiliation;

class RemoveAffiliation {

    /**
     * For logging
     * 
     * @var string
     */
    public $historable = 'remove_resource';
    
    /**
     * @var AffiliationRepositoryInterface
     */
    private $affiliationRepository;

    
    function __construct(AffiliationRepositoryInterface $affiliationRepository)
    {
        $this->affiliationRepository = $affiliationRepository;
    }


    /**
     * @param array $inputs
     * @param Resident $resident
     * @return Resident
     */
    function handle(array $inputs, Affiliation $affiliation)
    {
        $affiliation->residents()->detach();

        $this->affiliationRepository->delete($affiliation);

        return $affiliation;
    }
} 