<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Profiling\Repositories\AffiliationRepositoryInterface;
use SourceScript\Common\Collections\ResultCollection;

class RemoveAffiliations {


	/**
     * For logging
     * 
     * @var string
     */
    public $historable = 'remove_resources';

	/**
	 * @var AffiliationRepositoryInterface
	 */
	private $affiliationRepository;


	function __construct(AffiliationRepositoryInterface $affiliationRepositoryInterface)
	{
		$this->affiliationRepository = $affiliationRepositoryInterface;
	}

	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $affiliations
	 * @return void
	 */
	public function handle(array $inputs, ResultCollection $affiliations)
	{
		foreach($affiliations as $affiliation)
		{
			$affiliation->members()->detach();
		}

		$this->affiliationRepository->deleteAffiliations($affiliations->modelKeys());

		return $affiliations;
	}
}