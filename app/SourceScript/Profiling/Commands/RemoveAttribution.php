<?php namespace SourceScript\Profiling\Commands;

use Attribution;
use SourceScript\Profiling\Repositories\AttributionRepositoryInterface;

class RemoveAttribution {

	/**
     * For logging
     * 
     * @var string
     */
    public $historable = 'remove_resource';

	/**
	 * @var AttributionRepositoryInterface
	 */
	private $repository;

	function __construct(AttributionRepositoryInterface $attributionRepositoryInterface)
	{
		$this->repository = $attributionRepositoryInterface;
	}

	/**
	 * @param  array       $inputs
	 * @param  Attribution $attribution
	 * @return void
	 */
	public function handle(array $inputs, Attribution $attribution)
	{
		$this->repository->delete($attribution);

		return $attribution;
	}
}