<?php namespace SourceScript\Profiling\Commands;

use Attribution;
use SourceScript\Common\Collections\ResultCollection;
use SourceScript\Profiling\Repositories\AttributionRepositoryInterface;

class RemoveAttributions {

	/**
     * For logging
     * 
     * @var string
     */
    public $historable = 'remove_resources';

	/**
	 * @var AttributionRepositoryInterface
	 */
	private $repository;

	function __construct(AttributionRepositoryInterface $attributionRepositoryInterface)
	{
		$this->repository = $attributionRepositoryInterface;
	}

	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $attributions
	 * @return void
	 */
	public function handle(array $inputs, ResultCollection $attributions)
	{
		$this->repository->deleteAttributions($attributions->modelKeys());

		return $attributions;
	}
}