<?php namespace SourceScript\Profiling\Commands;

use Household;
use SourceScript\Profiling\Repositories\HouseholdRepositoryInterface;

class RemoveHousehold {

	/**
     * For logging
     * 
     * @var string
     */
    public $historable = 'remove_resource';

	/**
	 * @var HouseholdRepositoryInterface
	 */
	private $repository;


	function __construct(HouseholdRepositoryInterface $householdRepositoryInterface)
	{
		$this->repository = $householdRepositoryInterface;
	}

	/**
	 * @param  array     $inputs
	 * @param  Household $household
	 * @return void
	 */
	public function handle(array $inputs, Household $household)
	{
		$this->repository->delete($household);

		return $household;
	}
}