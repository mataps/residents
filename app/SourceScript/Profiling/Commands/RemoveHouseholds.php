<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Common\Exceptions\ValidationException;
use SourceScript\Common\Collections\ResultCollection;
use SourceScript\Profiling\Repositories\HouseholdRepositoryInterface;

class RemoveHouseholds {

	/**
     * For logging
     * 
     * @var string
     */
    public $historable = 'remove_resources';

	/**
	 * @var HouseholdRepositoryInterface
	 */
	private $householdRepository;


	function __construct(HouseholdRepositoryInterface $householdRepositoryInterface)
	{
		$this->householdRepository = $householdRepositoryInterface;
	}


	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $households
	 */
	public function handle(array $inputs, ResultCollection $households)
	{
		if($households->count() > 0)
		{
			$this->householdRepository->deleteHouseholds($households->modelKeys());
		}
		else
		{
			throw new ValidationException(array("residents" => "Household not found"));
		}

		return $households;
	}
}