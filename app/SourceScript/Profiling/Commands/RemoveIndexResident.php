<?php

namespace SourceScript\Profiling\Commands;

use Illuminate\Database\Eloquent\Collection;
use SourceScript\Common\Collections\ResultCollection;
use SourceScript\Profiling\Repositories\ResidentRepositoryInterface;
use Resident;
use Elasticsearch\Client;

class RemoveIndexResident {

    /**
     * @var ResidentRepositoryInterface
     */
    private $residentRepository;


    /**
     * @param array $inputs
     * @param Resident $resident
     * @return Resident
     */
    function handle(array $inputs, ResultCollection $residents)
    {
        $conn_param = \Config::get('elasticsearch');
        $client = new Client($conn_param);

        // dd($residents->modelKeys());

        foreach ($residents->modelKeys() as $resident)
        {

            $param['index'] = "galactus_production";
            $param['type'] = "residents";
            $param['body']['query']['match']['id'] = $resident;

            $resident = $client->search($param);
            $resident_es_id = $resident['hits']['hits'][0]['_id'];
            // dd($resident_es_id);
            $delete_param['index'] = "galactus_production";
            $delete_param['type'] = "residents";
            $delete_param['id'] = $resident_es_id;

            $client->delete($delete_param);
            // dd($resident);
        }

        return true;
    }
} 