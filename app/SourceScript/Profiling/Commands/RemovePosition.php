<?php namespace SourceScript\Profiling\Commands;

use Position;
use SourceScript\Profiling\Repositories\PositionRepositoryInterface;

class RemovePosition {

	/**
     * For logging
     * 
     * @var string
     */
    public $historable = 'remove_resource';


	/**
	 * @var PositionRepositoryInterface
	 */
	private $repository;


	function __construct(PositionRepositoryInterface $positinRepositoryInterface)
	{
		$this->repository = $positinRepositoryInterface;
	}


	/**
	 * @param  array    $inputs
	 * @param  Position $position
	 */
	public function handle(array $inputs, Position $position)
	{
		$this->repository->delete($position);

		return $position;
	}
}