<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Profiling\Repositories\PositionRepositoryInterface;
use SourceScript\Common\Collections\ResultCollection;

class RemovePositions {

	/**
     * For logging
     * 
     * @var string
     */
    public $historable = 'remove_resources';

	/**
	 * @var PositionRepositoryInterface
	 */
	private $repository;

	function __construct(PositionRepositoryInterface $positionRepositoryInterface)
	{
		$this->repository = $positionRepositoryInterface;
	}

	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $positions
	 * @return void
	 */
	public function handle(array $inputs, ResultCollection $positions)
	{
		// foreach($positions as $position)
		// {
		// 	$position->affiliations()->detach();
		// }

		$this->repository->deletePositions($positions->modelKeys());

		return $positions;
	}
}