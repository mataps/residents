<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Profiling\Repositories\ResidentRepositoryInterface;
use Resident;

class RemoveResident {

    /**
     * For logging
     * 
     * @var string
     */
    public $historable = 'remove_resource';


    /**
     * @var ResidentRepositoryInterface
     */
    private $residentRepository;


    /**
     * @param ResidentRepositoryInterface $residentRepository
     */
    function __construct(ResidentRepositoryInterface $residentRepository)
    {
        $this->residentRepository = $residentRepository;
    }


    /**
     * @param array $inputs
     * @param Resident $resident
     * @return Resident
     */
    function handle(array $inputs, Resident $resident)
    {
        $this->residentRepository->delete($resident);

        // $this->residentRepository->decrementLastNames([ $resident->last_name, $resident->middle_name ]);
        
        return $resident;
    }
} 