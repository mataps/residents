<?php namespace SourceScript\Profiling\Commands;

use Affiliation;
use Resident;

class RemoveResidentAffiliation {

    /**
     * @param  array       $inputs
     * @param  Resident    $resident
     * @param  Affiliation $affiliation
     * @param  User        $creator
     */
    function handle(array $inputs, Resident $resident, Affiliation $affiliation)
    {
    	$resident->affiliations()->detach($affiliation->id);
    }
}