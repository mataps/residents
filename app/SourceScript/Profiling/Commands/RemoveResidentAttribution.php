<?php namespace SourceScript\Profiling\Commands;

use ResidentAttribute;
use Resident;
use Attribution;

class RemoveResidentAttribution {

	/**
	 * @param  array       $inputs
	 * @param  Resident    $resident
	 * @param  Attribution $attribution
	 */
	public function handle(array $inputs, Resident $resident, Attribution $attribution)
	{
		ResidentAttribute::where('attribute_id', $attribution->id)->where('resident_id', $resident->id)->delete();
	}
}