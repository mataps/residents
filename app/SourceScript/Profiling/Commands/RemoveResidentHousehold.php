<?php namespace SourceScript\Profiling\Commands;

use ResidentHousehold;
use Affiliation;
use Resident;
use Household;

class RemoveResidentHousehold {

    /**
     * @param  array     $inputs
     * @param  Resident  $resident
     * @param  Household $household
     * @return void
     */
    function handle(array $inputs, Resident $resident, Household $household)
    {
        $resident->households()->detach($household->id);
    }
}