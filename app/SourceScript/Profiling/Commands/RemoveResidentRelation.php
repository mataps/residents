<?php namespace SourceScript\Profiling\Commands;

use Resident;
use ResidentRelationship;

class RemoveResidentRelation {

	/**
	 * @param  array                $inputs
	 * @param  Resident             $resident
	 * @param  ResidentRelationship $residentRelationship
	 */
	public function handle(array $inputs, $members)
	{
		ResidentRelationship::whereIn('resident_id', $members)
							->whereIn('relative_id', $members)
							->forceDelete();
	}
}