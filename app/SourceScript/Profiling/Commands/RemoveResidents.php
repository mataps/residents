<?php namespace SourceScript\Profiling\Commands;

use Illuminate\Database\Eloquent\Collection;
use SourceScript\Common\Collections\ResultCollection;
use SourceScript\Profiling\Repositories\ResidentRepositoryInterface;
use Resident;
use SourceScript\Common\Exceptions\ValidationException;

class RemoveResidents {

    /**
     * For logging
     * 
     * @var string
     */
    public $historable = 'remove_resources';

    /**
     * @var ResidentRepositoryInterface
     */
    private $residentRepository;

    /**
     * @param ResidentRepositoryInterface $residentRepository
     */
    function __construct(ResidentRepositoryInterface $residentRepository)
    {
        $this->residentRepository = $residentRepository;
    }

    /**
     * @param array $inputs
     * @param Collection|ResultCollection $residents
     */
    function handle(array $inputs, ResultCollection $residents)
    {
        if($residents->count() > 0)
        {
            $this->residentRepository->deleteResidents($residents->modelKeys());

            foreach ($residents as $resident)
            {
                //TODO: Refactor this to events
                $this->residentRepository->decrementLastNames([ $resident->last_name, $resident->middle_name ]);
            }
        }
        else
        {
            throw new ValidationException(array("residents" => "Residents not found"));
        }

        return $residents;
    }
}