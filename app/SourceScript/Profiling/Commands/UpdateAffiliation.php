<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\Profiling\Repositories\AffiliationRepositoryInterface;
use SourceScript\Profiling\Factories\BarangayFactory;
use SourceScript\Profiling\Factories\CityMunicipalityFactory;
use SourceScript\Profiling\Factories\DistrictFactory;
use Affiliation;
use User;

class UpdateAffiliation {

	use ValidableTrait;


    /**
     * For logging
     * 
     * @var string
     */
    public $historable = 'update_resource';
    

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'name' 				=> 'required',
		'founded'           => 'required',
		// 'district'          => 'required',
  //       'barangay'          => 'required',
  //       'cityMunicipality'  => 'required',
  //       'street'            => 'required'
	];


    /**
     * @var DistrictFactory
     */
    private $districtFactory;

    /**
     * @var CityMunicipalityFactory
     */
    private $cityMunicipalityFactory;

    /**
     * @var BarangayFactory
     */
    private $barangayFactory;

    /**
     * @var HouseholdRepositoryInterface
     */
    private $affiliationRepository;


	function __construct(
		AffiliationRepositoryInterface $affiliationRepositoryInterface,
		DistrictFactory $districtFactory,
        CityMunicipalityFactory $cityMunicipalityFactory,
        BarangayFactory $barangayFactory)
	{
		$this->repository = $affiliationRepositoryInterface;
		$this->districtFactory          = $districtFactory;
        $this->cityMunicipalityFactory  = $cityMunicipalityFactory;
        $this->barangayFactory          = $barangayFactory;
	} 


	/**
	 * @param  array       $inputs
	 * @param  Affiliation $affiliation
	 * @param  User        $updater
	 * @return Affiliation
	 */
	public function handle(array $inputs, Affiliation $affiliation, User $updater)
	{
		$affiliation->fill($inputs);

		$affiliation->name              = $inputs['name'];
        $affiliation->description       = $inputs['description'];
        $affiliation->founded           = date('Y-m-d', strtotime($inputs['founded']));
        $affiliation->district          = $inputs['district'] ? $this->districtFactory->create( $inputs['district'] ) : null;
        $affiliation->barangay          = $inputs['barangay'] ? $this->barangayFactory->create( $inputs['barangay'] ) : null;
        $affiliation->cityMunicipality  = $inputs['cityMunicipality'] ? $this->cityMunicipalityFactory->create( $inputs['cityMunicipality'] ) : null;
        $affiliation->street            = $inputs['street'];

        $affiliation->updater 		= $updater;


        // foreach($inputs['members'] as $member)
        // {
        //     $founderData = [
        //         'affiliation_id' => $affiliation->id,
        //         'founder' => true
        //     ];

        //     $this->profilingService->execute('AddResidentAffiliation', $founderData, $creator);
        // }

        $this->repository->save($affiliation);

		return $affiliation;
	}
}