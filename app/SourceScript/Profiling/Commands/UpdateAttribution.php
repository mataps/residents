<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\Profiling\Repositories\AttributionRepositoryInterface;
use Attribution;
use User;

class UpdateAttribution {

	use ValidableTrait;


	/**
     * For logging
     * 
     * @var string
     */
    public $historable = 'update_resource';
    

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'name' => 'required',
		'label' => 'required'
	];

	/**
	 * @var AttributionRepositoryInterface
	 */
	private $repository;


	function __construct(AttributionRepositoryInterface $attributionRepositoryInterface)
	{
		$this->repository = $attributionRepositoryInterface;
	}

	/**
	 * @param  array  $inputs
	 * @param  User   $updater
	 * @return Attribution
	 */
	public function handle(array $inputs, Attribution $attribution, User $updater)
	{
		$attribution->fill($inputs);

		$attribution->modified_by = $updater->id;

		$this->repository->save($attribution);

		return $attribution;
	}
}