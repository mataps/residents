<?php

namespace SourceScript\Profiling\Commands;

use Resident;
use Elasticsearch\Client;

class UpdateIndexResident {

    protected $client;

    function handle($input, Resident $resident, $es_id)
    {           
            $param = \Config::get('elasticsearch');
            $this->client = new Client($param);

            $district = ($resident->district) ? $resident->district->name : '';
            $barangay = ($resident->barangay) ? $resident->barangay->name : ''; 
            $cityMunicipality = ($resident->cityMunicipality) ? $resident->cityMunicipality->name : '';

            $district = ($resident->district) ? $resident->district->name : '';
            $barangay = ($resident->barangay) ? $resident->barangay->name : ''; 

            $params['body'] = array(
                    'district'  => $district,
                    'barangay'  => $barangay,
                    'cityMunicipality'  => $cityMunicipality,
                    'first_name' => $resident->first_name,
                    'last_name' => $resident->last_name,
                    'middle_name' => $resident->middle_name,
                    'full_name' => $resident->first_name . ' ' . $resident->middle_name . ' ' . $resident->last_name,
                    'email' => $resident->email,
                    'mobile' => $resident->mobile,
                    'phone' => $resident->phone,
                    'precint' => $resident->precint,
                    'birthdate' => date("Y-m-d", strtotime($resident->birthdate)),
                    'gender' => $resident->gender,
                    'civil_status' => $resident->civil_status,
                    'street' => $resident->street,
                    'creator' => $resident->creator,
                    'updater' => $resident->updater,
                    'attributes' => $resident->getAttributeNames(),
                    'affiliations' => $resident->getAffiliationNames(),
                    'updated_at' => $resident->updated_at,
                    'created_at' => $resident->created_at,
                    'blacklisted_at' => $resident->blacklisted_at,
                    'deceased_at' => $resident->deceased_at,
                    'is_voter' => $resident->is_voter,
                    'id' => $resident->id
                );

            $params['index'] = 'galactus_production';
            $params['type']  = 'residents';
            $params['id'] = $es_id;
            $this->client->index($params);
            

          return $resident;
    }
}