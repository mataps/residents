<?php namespace Sourcescript\Profiling\Commands;

use SourceScript\Profiling\Repositories\PositionRepositoryInterface;
use SourceScript\Profiling\Repositories\AffiliationRepositoryInterface;
use SourceScript\Profiling\Factories\AttributionsFactory;
use SourceScript\Common\Validations\ValidableTrait;
use User;
use Position;

class UpdatePosition {

	use ValidableTrait;


	/**
     * For logging
     * 
     * @var string
     */
    public $historable = 'update_resource';

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'name' => 'required'
	];

	/**
	 * @var PositionRepositoryInterface
	 */
	private $positionRepository;


	/**
	 * @var AffiliationRepositoryInterface
	 */
	private $affiliationRepository;


	/**
	 * @var AttributionsFactory
	 */
	private $attributionsFactory;


	function __construct(
		PositionRepositoryInterface $positionRepositoryInterface,
		AffiliationRepositoryInterface $affiliationRepositoryInterface,
		AttributionsFactory $attributionsFactory)
	{
		$this->positionRepository = $positionRepositoryInterface;
		$this->affiliationRepository = $affiliationRepositoryInterface;
		$this->attributionsFactory = $attributionsFactory;
	}

	/**
	 * @param  array    $inputs
	 * @param  Position $position
	 * @param  User     $updater
	 * @return Position
	 */
	public function handle(array $inputs, Position $position, User $updater)
	{
		$position->name = $inputs['name'];

		$this->positionRepository->save($position);

		$affiliations = $this->affiliationRepository->findAffiliations($inputs['affiliations']);

		$position->affiliations()->sync($affiliations->modelKeys());

		$attributions = $this->attributionsFactory->get($inputs['attributions'], $updater);

		$position->attributions()->sync($attributions->modelKeys());

		return $position;
	}
}