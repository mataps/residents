<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\Profiling\Factories\BarangayFactory;
use SourceScript\Profiling\Factories\CityMunicipalityFactory;
use SourceScript\Profiling\Factories\DistrictFactory;
use SourceScript\Profiling\Factories\PhotoFactory;
use SourceScript\Profiling\Repositories\ResidentRepositoryInterface;
use Resident;
use User;

class UpdateResident {

    use ValidableTrait;

    /**
     * For logging
     * 
     * @var string
     */
    public $historable = 'update_resource';


    /**
     * Validation rules
     * 
     * @var array
     */
    protected $rules = array(
        'first_name' => 'required',
        'last_name' => 'required',
        'middle_name' => '',
        'birthdate' => '',
        'mobile' => '',
        'phone' => '',
        'email' => '',
        'cityMunicipality' => 'required',
        'district' => 'required',
        'barangay' => 'required',
        'precinct' => '',
        'deceased_at' => '',
        'blacklisted_at' => ''
    );


    /**
     * @var ResidentRepositoryInterface
     */
    private $residentRepository;


    /**
     * @var PhotoFactory
     */
    private $photoFactory;


    /**
     * @var DistrictFactory
     */
    private $districtFactory;


    /**
     * @var CityMunicipalityFactory
     */
    private $cityMunicipalityFactory;


    /**
     * @var BarangayFactory
     */
    private $barangayFactory;

    /**
     * @param ResidentRepositoryInterface $residentRepository
     * @param PhotoFactory $photoFactory
     * @param DistrictFactory $districtFactory
     * @param CityMunicipalityFactory $cityMunicipalityFactory
     * @param BarangayFactory $barangayFactory
     */
    function __construct(
        ResidentRepositoryInterface $residentRepository,
        PhotoFactory $photoFactory,
        DistrictFactory $districtFactory,
        CityMunicipalityFactory $cityMunicipalityFactory,
        BarangayFactory $barangayFactory)
    {
        $this->residentRepository       = $residentRepository;
        $this->photoFactory             = $photoFactory;
        $this->districtFactory          = $districtFactory;
        $this->cityMunicipalityFactory  = $cityMunicipalityFactory;
        $this->barangayFactory          = $barangayFactory;
    }


    /**
     * @param array $inputs
     * @param Resident $resident
     * @param User $updater
     * @internal param User $creator
     * @return Resident
     */
    function handle(array $inputs, Resident $resident, User $updater)
    { 
        $blacklisted_at = (strtotime($inputs['blacklisted_at'])) ? date('Y-m-d', strtotime($inputs['blacklisted_at'])) : null;
        $deceased_at = (strtotime($inputs['deceased_at'])) ? date('Y-m-d', strtotime($inputs['deceased_at'])) : null;


        $resident->first_name   = isset($inputs['first_name']) ? $inputs['first_name'] : $resident->first_name;
        $resident->is_voter = isset($inputs['is_voter']) ? $inputs['is_voter'] : false;
        $resident->voters_id = isset($inputs['voters_id']) ? $inputs['voters_id'] : $resident->voters_id;
        $resident->last_name    = isset($inputs['last_name']) ? $inputs['last_name'] : $resident->last_name;
        $resident->middle_name  = isset($inputs['middle_name']) ? $inputs['middle_name'] : $resident->middle_name;
        $resident->civil_status = isset($inputs['civil_status']) ? $inputs['civil_status'] : $resident->civil_status;
        $resident->gender       = isset($inputs['gender']) ? $inputs['gender'] : $resident->gender;
        $resident->birthdate    = isset($inputs['birthdate']) ? $inputs['birthdate'] : $resident->birthdate;
        $resident->mobile       = isset($inputs['mobile']) ? $inputs['mobile'] : $resident->mobile;
        $resident->mobile_1     = isset($inputs['mobile_1']) ? $inputs['mobile_1'] : $resident->mobile_1;
        $resident->mobile_2     = isset($inputs['mobile_2']) ? $inputs['mobile_2'] : $resident->mobile_2;
        $resident->notes     = isset($inputs['notes']) ? $inputs['notes'] : $resident->notes;
        $resident->phone        = isset($inputs['phone']) ? $inputs['phone'] : $resident->phone;
        $resident->email        = isset($inputs['email']) ? $inputs['email'] : $resident->email;
        $resident->street       = isset($inputs['street']) ? $inputs['street'] : $resident->street;
        $resident->occupation = isset($inputs['occupation']) ? $inputs['occupation'] : $resident->occupation;
        $resident->nickname = isset($inputs['nickname']) ? $inputs['nickname'] : $resident->nickname;

        $resident->modified_by      = $updater->id;
        $resident->blacklisted_at = $blacklisted_at;
        $resident->deceased_at    = $deceased_at;
        $resident->atm_number = isset($inputs['atm_number']) ? $inputs['atm_number'] : $resident->atm_number;
        $resident->bank = isset($inputs['bank']) ? $inputs['bank'] : $resident->bank;
        // $resident->profilePic   = $this->photoFactory->create( $inputs['profile_image'], $inputs['cityMunicipality'] );
        $resident->district_id     = $this->districtFactory->create( $inputs['district'] )->id;
        $resident->barangay_id     = $this->barangayFactory->create( $inputs['barangay'] )->id;
        $resident->city_municipality_id = $this->cityMunicipalityFactory->create( $inputs['cityMunicipality'] )->id;
        $resident->signature_path = isset($inputs['signature_path']) ? $inputs['signature_path'] : $resident->signature_path;

        //$this->residentRepository->save($resident);
        $resident->save();
        return $resident;
    }
}