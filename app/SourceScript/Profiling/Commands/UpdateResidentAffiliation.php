<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\Profiling\Repositories\PositionRepositoryInterface;
use Resident;
use Affiliation;
use User;

class UpdateResidentAffiliation {

	use ValidableTrait;

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = array(
		'head_id' 			=> 'exists:residents,id',
		'referrer_id' 		=> 'exists:residents,id',
        'position_id'       => 'required|exists:positions,id',
        'founder'           => 'required|boolean'
    );


	/**
	 * @var PositionRepositoryInterface
	 */
    private $positionRepository;


    function __construct(PositionRepositoryInterface $positionRepositoryInterface)
    {
    	$this->positionRepository = $positionRepositoryInterface;
    }


	/**
	 * @param  array       $inputs
	 * @param  Resident    $resident
	 * @param  Affiliation $affiliation
	 * @param  User        $updater
	 * @return ResidentAffiliation
	 */
	public function handle(array $inputs, Resident $resident, Affiliation $affiliation, User $updater)
	{
		$resident->affiliations()->updateExistingPivot($affiliation->id, array_merge($inputs, ['modified_by' => $updater->id]));

		$position = $this->positionRepository->findOneById($inputs['position_id']);

		$resident->attributions()->detach($position->attributions->lists('id'));

		$resident->attributions()->attach($position->attributions->lists('id'));    
	}
}