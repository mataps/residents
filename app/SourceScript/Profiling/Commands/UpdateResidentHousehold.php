<?php namespace SourceScript\Profiling\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use Resident;
use Household;
use User;
use Illuminate\Support\Facades\DB;

class UpdateResidentHousehold {

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'role' => 'required'
	];

	/**
	 * @param  array     $inputs
	 * @param  Resident  $resident
	 * @param  Household $household
	 * @param  User      $updater
	 * @return ResidentHousehold
	 */
	public function handle(array $inputs, Resident $resident, Household $household, User $updater)
	{
		if($inputs['role'] == "owner")
		{
			DB::table('households_residents')->whereIn('household_id', [$household->id])->whereNotIn('resident_id', [$resident->id])->update(['role' => 'member']);
		}

		$resident->households()->updateExistingPivot($household->id, array_merge($inputs, ['modified_by' => $updater->id]));
	}
}