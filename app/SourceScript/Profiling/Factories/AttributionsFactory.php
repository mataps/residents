<?php namespace SourceScript\Profiling\Factories;

use SourceScript\Common\Collections\ResultCollection;
use SourceScript\Profiling\Repositories\AttributionRepositoryInterface;
use User;

class AttributionsFactory {

	/**
	 * @var AttributionRepositoryInterface
	 */
	private $repository;


	function __construct(AttributionRepositoryInterface $attributionRepositoryInterface)
	{
		$this->repository = $attributionRepositoryInterface;
	}

	/**
	 * @param  string $names
	 * @return ResultCollection
	 */
	public function get($names, User $creator)
	{
		$names = is_array($names) ?: explode(',', str_replace(' ', '', urldecode($names)));

		$attributions = new ResultCollection;

		foreach($names as $name)
		{
			$attribution = $this->repository->firstOrCreate(['name' => $name, 'created_by' => $creator->id, 'modified_by' => $creator->id]);

			$attributions->push($attribution);
		}

		return $attributions;
	}	
}