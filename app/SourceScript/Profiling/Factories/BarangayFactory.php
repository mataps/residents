<?php

namespace SourceScript\Profiling\Factories;


use SourceScript\Profiling\Repositories\BarangayRepositoryInterface;

class BarangayFactory {

    private $repository;

    function __construct(BarangayRepositoryInterface $barangayRepository)
    {
        $this->repository = $barangayRepository;
    }

    function create($name)
    {
        if ( ! empty($name))
        {
            return $this->repository->firstOrNew(['name' => $name]);
        }

        return null;
    }
}