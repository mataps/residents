<?php

namespace SourceScript\Profiling\Factories;

use SourceScript\Profiling\Repositories\CityMunicipalityRepositoryInterface;

class CityMunicipalityFactory {

    private $repository;

    function __construct(CityMunicipalityRepositoryInterface $cityMunicipalityRepository)
    {
        $this->repository = $cityMunicipalityRepository;
    }

    function create($name)
    {
        if ( ! empty($name))
        {
            return $this->repository->firstOrNew(['name' => $name]);
        }

        return null;
    }
}