<?php

namespace SourceScript\Profiling\Factories;


use SourceScript\Profiling\Repositories\DistrictRepositoryInterface;

class DistrictFactory {

    private $repository;

    function __construct(DistrictRepositoryInterface $districtRepository)
    {
        $this->repository = $districtRepository;
    }

    function create($name)
    {
        if ( ! empty($name))
        {
            return $this->repository->firstOrNew(['name' => $name]);
        }

        return null;
    }
}