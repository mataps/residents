<?php namespace SourceScript\Profiling\Factories;

use Photo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Support\Str;

class PhotoFactory {

    function create($profile, $folderName)
    {
        if ( ! empty($profile))
        {
            return $this->createFromInput($profile, $folderName);
        }

        return null;
    }

    function createFromInput($profile, $folderName)
    {
        if ($profile instanceof UploadedFile)
        {
            $filename = $this->saveUpload($profile, $folderName);
        }
        else
        {
            $filename = $this->saveImageData($profile, $folderName);
        }

        $photo = new Photo;
        $photo->filename = $filename;

        return $photo;
    }

    function saveUpload(UploadedFile $upload, $folderName)
    {
        $filename = $this->generateFilename($upload->getClientOriginalName());
        $upload->move(public_path('uploads/' . str_replace(" ", "",Str::title($folderName)) . '/photos'), $filename);

        return $filename;
    }

    private function generateFilename($originalName)
    {
        return time() . '_' . $originalName;
    }

    function saveImageData($imageData, $folderName)
    {
        $data = $this->decodeImageData($imageData);

        $filename = $this->generateFilename('webcamPic.jpg');

        $path = public_path("uploads/" . str_replace(" ", "",Str::title($folderName)) . '/photos/') . $filename;

        $handle = fopen($path, 'w');
        fwrite($handle, $data);
        fclose($handle);

        return $filename;
    }

    private function decodeImageData($imageData)
    {
        list($type, $data) = explode(';', $imageData);
        list(, $data)      = explode(',', $data);
        return base64_decode($data);
    }
}