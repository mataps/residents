<?php namespace SourceScript\Profiling;


use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Common\ServiceInterface;
use Illuminate\Support\Collection;

interface ProfilingInterface extends ServiceInterface{

    /**
     * @param FieldsParameters $fieldsParameters
     * @param PaginationParameters $paginationParameters
     * @param SortParameters $sortParameters
     * @return Collection
     */
    // function findResidents(FieldsParameters $fieldsParameters = null, PaginationParameters $paginationParameters = null, SortParameters $sortParameters = null);
}