<?php

namespace SourceScript\Profiling;


use SourceScript\Common\AbstractProvider;

class ProfilingProvider extends AbstractProvider
{
    protected $modules = array(
        'Resident' => array(
            'repository' => 'ResidentRepository',
            'bindings' => array(
                'residents' => 'findOneById',
                'Residents' => 'findResidents'
                )
            ),
        'Attribution' => array(
            'repository' => 'AttributionRepository',
            'bindings' => array(
                'attributions' => 'findOneById',
                'Attributions' => 'findAttributions'
                )
            ),
        'Affiliation' => array(
            'repository' => 'AffiliationRepository',
            'bindings' => array(
                'affiliations' => 'findOneById',
                'Affiliations' => 'findAffiliations'
                )
            ),
        'Household' => array(
            'repository' => 'HouseholdRepository',
            'bindings' => array(
                'households' => 'findOneById',
                'Households' => 'findHouseholds'
                )
            ),
        'ResidentAffiliation' => array(
            'repository' => 'ResidentAffiliationRepository',
            'bindings' => array(
                'residents_affiliations' => 'findOneById'
                )
            ),
        'District' => array(
            'repository' => 'DistrictRepository'
            ),
        'Barangay' => array(
            'repository' => 'BarangayRepository'
            ),
        'CityMunicipality' => array(
            'repository' => 'CityMunicipalityRepository',
            'bindings' => array(
                'cities' => 'findOneById'
                )
            ),
        'Photo' => array(
            'repository' => 'PhotoRepository'
            ),
        'ResidentRelation' => array(
            'repository' => 'ResidentRelationRepository',
            'bindings' => array(
                'relatives' => 'findOneById'
                )
            ),
        'Position' => array(
            'repository' => 'PositionRepository',
            'bindings' => array(
                'positions' => 'findOneById',
                'Positions' => 'findPositions'
                )
            )
        );

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }
}