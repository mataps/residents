<?php namespace SourceScript\Profiling;

use DB;
use SourceScript\Common\AbstractService;
use SourceScript\Common\Paginators\FilterableTrait;
use SourceScript\Common\Paginators\PaginatableTrait;
use SourceScript\Common\Paginators\SortableTrait;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Common\Repositories\GenericFinderInterface;
use ResidentAffiliation;
use ResidentAttribute;
use Resident;
use Household;
use Attribution;
use Barangay;
use District;
use CityMunicipality;
use Affiliation;
use Position;
use Elasticsearch\Client;
use AffiliationPosition;
// use Es as Es;
// use Elastica;
// use \Elastica\Client as Client;
// use \Elastica\Query as Query;
// use \Elastica\Search as Search;

class ProfilingService extends AbstractService implements ProfilingInterface {

    use PaginatableTrait, FilterableTrait, SortableTrait;

    function __construct(GenericFinderInterface $finder)
    {
        $this->finder = $finder;
    }


    /**
     * @param  Position                  $position
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return Query
     */
    public function findAffiliationsByPosition(
        Position $position,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;

        $query = AffiliationPosition::query();

        $query->where('affiliation_id', $affiliation->id);
        
        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);

        if($filterParameters){
           $query = $this->buildFilters($query, $filterParameters);
        }


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);
        
        return $result;
    }


    /**
     * @param  Household                 $household
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return Query
     */
    public function findAffiliationsByhousehold(
        Household $household,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $residents = $household->residents->modelKeys();

        $query = ResidentAffiliation::query();

        $query->whereIn('resident_id', $residents);
        
        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);
        
        return $result;     
    }


    /**
     * @param  Household                 $household
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return Query
     */
    public function findAttributesByHousehold(
        Household               $household,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $residents = $household->residents->modelKeys();

        $query = ResidentAttribute::query();

        $query->whereIn('resident_id', $residents);
        
        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);
        
        return $result;
    }


    /**
     * @param  Resident                  $resident
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return Collection
     */
    function findRelatives(
        Resident $resident,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)

    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $resident->relatives()->getQuery();
        
        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);
        
        return $result;
    }


    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findAttributions(
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = Attribution::query();
        
        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findPositions(
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = Position::query();
        
        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  Affiliation               $affiliation
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return Query
     */
    function findPositionsByAffiliation(
        Affiliation $affiliation,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        // $query = $affiliation->positions()->getQuery();
        // 
        $query = AffiliationPosition::query();

        $query->where('affiliation_id', $affiliation->id);
        
        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  Household            $household
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findResidentsByHousehold(
        Household               $household,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $household->residents()->getQuery();
        
        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;        
    }


    /**
     * @param  Resident             $resident
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findHouseholdsByResident(
        Resident                $resident,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $resident->households()->getQuery();
        
        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  Resident             $resident
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findMembersByAffiliation(
        Affiliation             $affiliation,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $affiliation->members()->getQuery();
        
        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }

    /**
     * @param  Resident             $resident
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findLeadersByAffiliation(
        Affiliation             $affiliation,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $affiliation->leaders()->getQuery();
        
        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  Resident             $resident
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findFoundersByAffiliation(
        Affiliation             $affiliation,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null
        )
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $affiliation->founders()->getQuery();
        
        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  Resident             $resident
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findAffiliationsByResident(
        Resident                $resident,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $resident->affiliations()->getQuery();
        
        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }

    /**
     * @param  Affiliation               $affiliation
     * @param  CityMunicipality          $municipality
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return ResultCollection
     */
    public function findResidentsByAffiliationAndMunicipality(
        Affiliation $affiliation,
        CityMunicipality $municipality,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $affiliation->residents()->getQuery();

        $query->with(['positions' => function($subQuery) use($affiliation)
        {
            $subQuery->where('affiliation_id', $affiliation->id);
        }]);

        $query->where('city_municipality_id', $municipality->id);

        $query->orderBy('barangay_id');
        
        $query->orderBy('last_name');

        $query->orderBy('first_name');
        
        if($filterParameters)
            $this->buildFilters($query, $filterParameters);

        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);
        

        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }

    /**
     * @param  Affiliation               $affiliation
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters 
     * @param  FilterParameters|null     $filterParameters
     * @return ResultCollection
     */
    public function findResidentsByAffiliation(
        Affiliation             $affiliation,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null
        )
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $affiliation->residents()->getQuery();

        $query->with(['positions' => function($subQuery) use($affiliation)
        {
            $subQuery->where('affiliation_id', $affiliation->id);
        }]);

        $query->orderBy('barangay_id');

        $query->orderBy('last_name');
        
        if($filterParameters)
            $this->buildFilters($query, $filterParameters);

        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);
        

        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }

    
    /**
     * @param  Affiliation               $affiliation
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return Query
     */
    public function findresidentAffiliationByAffiliation(
        Affiliation             $affiliation,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = ResidentAffiliation::query();

        $query->where('affiliation_id', $affiliation->id);
        
        if($filterParameters)
            $this->buildFilters($query, $filterParameters);

        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);
        

        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  Resident                  $resident
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return Query
     */
    public function findresidentAffiliationByResident(
        Resident                $resident,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = ResidentAffiliation::query();

        $query->where('resident_id', $resident->id);
        
        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  Resident             $resident
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findAttributionsByResident(
        Resident                $resident,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $resident->attributes()->getQuery();
        
        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  FilterParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return Query
     */
    function findResidents(FieldsParameters $fieldsParameters = null, PaginationParameters $paginationParameters = null,  SortParameters $sortParameters = null, FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
       
        $query = Resident::with(array('profilePic', 'signature', 'district', 'cityMunicipality', 'attributes', 'creator', 'updater'));

        if ($filterParameters)
        {
            $this->buildFilters($query, $filterParameters);
        }

        $countRows = $this->finder->count($query, $columns);

        if ($paginationParameters)
        {
            $this->buildPagination($query, $paginationParameters);
        }

        if ($sortParameters)
        {
            $this->buildSorting($query, $sortParameters);
        }

        $results = $this->finder->query($query, $columns);

        $results->setCountRows($countRows);

        return $results;
    }

    public function findResidentsES($fieldsParameters = null, PaginationParameters $paginationParameters = null,  $sortParameters = null, FilterParameters $filterParameters = null)
    {
          
  

          ### Elastic Search Part #######
        $filters = $filterParameters->getFilters();

        $param = \Config::get('elasticsearch');
        $client = new Client($param);
        
        $search_filters = [];

        foreach ($filters as $key => $value) {
            if($key == "sort") {
                continue;
            }
            if(is_array($value)){
 
                if(isset($value['from'])){
                 
                $search_filters[] = array(
                        'range' => array( $key => array(
                                'gte' => date('Y-m-d', strtotime($value['from'])),
                                'lte' => date('Y-m-d', strtotime($value['to']))
                            ))
                    );
                    
                }
                if($key == 'attributions'){
                    foreach ($value as $attribute) {
                        $search_filters[] = array(
                            'match_phrase_prefix' =>  array('attributions'  =>  $attribute['text'])
                            );
                    }
                }
            
                if($key == 'gender'){
                         $search_filters[] = array(
                            'match' =>  array('gender'  =>  $value['equal'])
                            );
                }

            }
            else if($key == 'full_name'){
                    $full_name = explode(" ", $value);
                    foreach ($full_name as $name) {
                        $search_filters[] = array(
                                'match_phrase_prefix' => array('full_name' =>  $name)
                            );
                                                
                    }
            }
            else if($key == 'affiliations'){
                $search_filters[] = array(
                                'match_phrase_prefix' =>  array($key  =>  $value)
                    );
            }

            else if($key == 'deceased_at'){
                        if($value == 0){
                            $search_filters[] =array(
                            'match' => array('deceased_at' => false));
                        }
                        if($value == 1){
                            $search_filters[] =array(
                            'match' => array('deceased_at' => '1970-01-01T00:00:01.000+08:00'));
                        }
            }
            else if($key == 'is_voter'){
                $search_filters[] = array(
                        'match' => array('is_voter' => (int) $value)
                    );
            }
            // else if($key == "cityMunicipality" || $key == "barangay"){
            //         $search_filters[] = array(
            //                 'match' =>  array($key  => $value)
            //                 );
            // }
            else{
                if($value != "null"){
                $value = strtolower($value);

                $search_item = explode(" ", $value);
                $search_wildcard_filters = array();
                $search_wc_filters = array();
                
                foreach ($search_item as $item) {
                    $search_wc_filters[] = array('wildcard' => array($key => $item . "*"));
                }

                $search_wildcard_filters[]['bool']['must'] = $search_wc_filters;

                $search_wildcard_filters[] = array(
                            'prefix' => array($key => [ 'value' =>  $value, 'boost' => 2])
                        );
                $search_wildcard_filters[] = ['term' => [$key => [ 'value' => $value,  'boost' => 3]]];

                if($value != 'null'){
           
                $search_filters[]['bool'] = array( 
                            'should' => $search_wildcard_filters
                            );
        
            
                    } 
                }
   
           
            }
        }

       $sortParameters = $sortParameters->getSort()[0];
        $sort_key = substr($sortParameters, 1);
     
        if(!is_null($sort_key) && $sort_key != "id"){
            $starts_with  = substr($sortParameters, 0, 1);
            if($starts_with == "-"){
                    $search_param['body']['sort'] = array(
                        array($sort_key => 'desc')
                    ); 
             }
             else{
                $search_param['body']['sort'] = array(
                        array($sortParameters => 'asc')
                ); 
             }
        }

        if(count($search_filters) > 0){
              $search_param['body']['query']['bool']['must'] = $search_filters;
        }
 
    


        $search_param['size'] = $paginationParameters ? $paginationParameters->getLimit() : 50000;
        $search_param['from'] = $paginationParameters ? $paginationParameters->getFrom() : 0;
        $search_param['index'] = 'galactus_production';
        $search_param['type']  = 'residents';
       
       // dd($search_param);
        $results = $client->search($search_param);
        // dd($results);
        return $results;
    }

    function findHouseholds(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $query = Household::query();

        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        $countRows = $this->finder->count($query, $columns);

        if ($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if ($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);

        $results = $this->finder->query($query, $columns);

        $results->setCountRows($countRows);

        return $results;
    }

    function findAffiliations(FieldsParameters $fieldsParameters = null, PaginationParameters $paginationParameters = null, SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $query = \Affiliation::query();

        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        $countRows = $this->finder->count($query, $columns);

        if ($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if ($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);

        $results = $this->finder->query($query, $columns);

        $results->setCountRows($countRows);

        return $results;
    }


    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findCitiesMunicipalities(
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
         $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = CityMunicipality::query();
        
        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findBarangays(
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = Barangay::query();
        
        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findDistricts(
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {

        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = District::query();
        
        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }

    function getLastNameCounts(Resident $resident)
    {
        $results = DB::select(DB::raw('SELECT * FROM lastnames WHERE last_name = :lastname OR last_name = :middlename'), [
            'lastname' => $resident->last_name,
            'middlename' => $resident->middle_name
        ]);

        return $results;
    }

    function getRelatedMQTY(Resident $resident, PaginationParameters $paginationParameters = null)
    {
        $rawQuery = "is_voter = true and (last_name = '$resident->middle_name' or middle_name = '$resident->middle_name') and barangay_id = $resident->barangay_id";
        
        $query = Resident::whereRaw($rawQuery);

        return $this->getRelatedLastName($query, $paginationParameters);
    }

    function getRelatedLQTY(Resident $resident, PaginationParameters $paginationParameters = null)
    {

        $rawQuery = "is_voter = true and (last_name = '$resident->last_name' or middle_name = '$resident->last_name') and barangay_id = $resident->barangay_id";

        $query = Resident::whereRaw($rawQuery);

        return $this->getRelatedLastName($query, $paginationParameters);
    }

    private function getRelatedLastName($query, $paginationParameters)
    {
        $countRows = $this->finder->count($query);

        if ($paginationParameters)
        {
            $this->buildPagination($query, $paginationParameters);
        }

        $results = $this->finder->query($query);

        $results->setCountRows($countRows);

        return $results;
    }

    function findAttributes()
    {
        $query = \Attribution::query();

        $results = $this->finder->query($query);

        return $results;
    }


    /**
     * @param  Resident                  $resident
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return ResultCollection
     */
    public function findResidentHousemates(Resident $resident,
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;

        $households = $resident->households->lists('id');

        $query = Resident::query();

        $query->whereHas('households', function($subQuery) use ($households)
        {
            $subQuery->whereIn('household_id', $households);
        });

        if($filterParameters) $this->buildFilters($query, $filterParameters);

        $countRows = $this->finder->count($query, $columns);

        if($sortParameters) $this->buildSorting($query, $sortParameters);

        $results = $this->finder->query($query, $columns);

        $results->setCountRows($countRows);

        return $results;
    }

//    function searchResidentByFullName($fullName, PaginationParameters $paginationParameters = null)
//    {
//        $query = Resident::select(DB::raw('CONCAT(first_name, " ", middle_name, " ", last_name) AS full_name'))
//            ->whereRaw(DB::raw('CONCAT(first_name, " ", middle_name, " ", last_name) LIKE :fullname'))
//            ->setBindings([sprintf('%%s%', $fullName)]);
//
//        $countRows = $this->finder->count($query);
//
//        if ($paginationParameters)
//        {
//            $this->buildPagination($query, $paginationParameters);
//        }
//
//        $results = $this->finder->query($query, $columns);
//
//        $results->setCountRows($countRows);
//
//        dd($results->toArray());
//        return $results;
//    }
}
