<?php namespace SourceScript\Profiling\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use Affiliation;

class AffiliationRepository implements AffiliationRepositoryInterface{

    /**
     * @var GenericFinderInterface
     */
    private $finder;

    /**
     * @var DistrictRepositoryInterface
     */
    private $districtRepository;

    /**
     * @var BarangayRepositoryInterface
     */
    private $barangayRepository;

    /**
     * @var CityMunicipalityRepositoryInterface
     */
    private $cityMunicipalityRepository;

    function __construct(
        GenericFinderInterface $finder,
        DistrictRepositoryInterface $districtRepository,
        BarangayRepositoryInterface $barangayRepository,
        CityMunicipalityRepositoryInterface $cityMunicipalityRepository)
    {
        $this->finder = $finder;
        $this->districtRepository   = $districtRepository;
        $this->barangayRepository   = $barangayRepository;
        $this->cityMunicipalityRepository = $cityMunicipalityRepository;
    }

    /**
     * @param $id
     * @throws DbException
     * @return Affiliation
     */
    function findOneById($id)
    {
        $query = Affiliation::where('id', $id);

        $result = $this->finder->findOne($query);

        if ($result)
        {
            return $result;
        }

        throw new DbException('Affiliation not found!');
    }

    /**
     * @param Affiliation $resident
     * @return Affiliation
     * @throws DbException
     */
    function save(Affiliation $affiliation)
    {
        $creator = $affiliation->removeAttribute('creator');
        $updater = $affiliation->removeAttribute('updater');
        $district = $affiliation->removeAttribute('district');
        $barangay = $affiliation->removeAttribute('barangay');
        $cityMunicipality = $affiliation->removeAttribute('cityMunicipality');

        if ($district)
        {
            $affiliation->district_id = $district->id ?: $this->districtRepository->save($district)->id;
        }

        if ($barangay)
        {
            $affiliation->barangay_id = $barangay->id ?: $this->barangayRepository->save($barangay)->id;
        }

        if ($cityMunicipality)
        {
            $affiliation->city_municipality_id = $cityMunicipality->id ?: $this->cityMunicipalityRepository->save($cityMunicipality)->id;
        }
        
        $affiliation->created_by = $creator->id;
        $affiliation->modified_by = $updater->id;        

        if ($affiliation->save())
        {
            return $affiliation;
        }

        throw new DbException('Affiliation not saved!');
    }

    /**
     * @param Affiliation $resident
     * @throws DbException
     */
    function delete(Affiliation $affiliation)
    {
        $affiliation->residents()->detach();

        if (! $resident->delete())
        {
            throw new DbException('Affiliation not deleted!');
        }
    }

    /**
     * @param array $affiliations
     * @throws DbException
     */
    function deleteAffiliations(array $affiliations)
    {
        foreach($affiliations as $affiliation)
        {
          
            if($affiliation){
                $affiliation = Affiliation::find($affiliation);
                $affiliation->members()->detach();
                
            }
        }

        $deleted = Affiliation::destroy($affiliations);

        if ( ! $deleted)
        {
            throw new DbException('Affiliations not deleted!');
        }
    }

    /**
     * @param  string $ids
     * @return mixed
     */
    function findAffiliations($ids)
    {
        $ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return Affiliation::whereIn('id', $ids)->get();
    }
}