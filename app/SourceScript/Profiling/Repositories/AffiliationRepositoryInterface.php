<?php

namespace SourceScript\Profiling\Repositories;


use Affiliation;

interface AffiliationRepositoryInterface {

    /**
     * @param $id
     * @return Affiliation
     */
    function findOneById($id);

    /**
     * @param Affiliation $resident
     * @return Affiliation
     */
    function save(Affiliation $resident);

    /**
     * @param Affiliation $resident
     * @return void
     */
    function delete(Affiliation $resident);
} 