<?php namespace SourceScript\Profiling\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use Attribution;

class AttributionRepository implements AttributionRepositoryInterface {


	function __construct(GenericFinderInterface $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param $id
     * @throws DbException
     * @return Attribution
     */
    function findOneById($id)
    {
        $query = Attribution::where('id', $id);

        $result = $this->finder->findOne($query);

        if ($result)
        {
            return $result;
        }

        throw new DbException('Attribution not found!');
    }

    /**
     * @param Attribution $resident
     * @return Attribution
     * @throws DbException
     */
    function save(Attribution $attribution)
    {
        if ($attribution->save())
        {
            return $attribution;
        }

        throw new DbException('Attribution not saved!');
    }

    /**
     * @param Attribution $resident
     * @throws DbException
     */
    function delete(Attribution $attribution)
    {
        if ( ! $attribution->delete())
        {
            throw new DbException('Attribution not deleted!');
        }
    }

    /**
     * @param array $attributions
     * @throws DbException
     */
    function deleteAttributions(array $attributions)
    {
        $deleted = Attribution::destroy($attributions);

        if ( ! $deleted)
        {
            throw new DbException('Attributions not deleted!');
        }
    }

    /**
     * @param $ids
     * @return mixed
     */
    function findAttributions($ids)
    {
        $ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return Attribution::whereIn('id', $ids)->get();
    }

    /**
     * @param  array  $params
     * @return Attribution
     */
    function firstOrCreate(array $params)
    {
        $attribution = Attribution::firstOrCreate($params);

        return $attribution;
    }
}