<?php namespace SourceScript\Profiling\Repositories;

use Attribution;

interface AttributionRepositoryInterface {

	/**
     * @param $id
     * @return Attribution
     */
    function findOneById($id);

    /**
     * @param Attribution $attribution
     * @return Attribution
     */
    function save(Attribution $attribution);

    /**
     * @param Attribution $attribution
     * @return void
     */
    function delete(Attribution $attribution);
}