<?php

namespace SourceScript\Profiling\Repositories;

use Barangay;
use SourceScript\Common\Exceptions\DbException;

class BarangayRepository implements BarangayRepositoryInterface{

    function findOneBy(array $params)
    {
        $barangay = Barangay::where($params)->first();

        if ($barangay)
        {
            return $barangay;
        }

        throw new DbException('Barangay not found');
    }

    function firstOrNew(array $params)
    {
        $barangay = Barangay::firstOrNew($params);

        if ($barangay)
        {
            return $barangay;
        }

        throw new DbException('Cannot find or create barangay');
    }

    function save(Barangay $barangay)
    {
        if ($barangay->save())
        {
            return $barangay;
        }

        throw new DbException('Barangay not saved!');
    }
}