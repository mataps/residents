<?php

namespace SourceScript\Profiling\Repositories;

use Barangay;

interface BarangayRepositoryInterface {

    function findOneBy(array $params);

    function firstOrNew(array $params);

    function save(Barangay $barangay);
}