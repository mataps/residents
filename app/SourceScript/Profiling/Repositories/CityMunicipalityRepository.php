<?php

namespace SourceScript\Profiling\Repositories;


use CityMunicipality;
use SourceScript\Common\Exceptions\DbException;

class CityMunicipalityRepository implements CityMunicipalityRepositoryInterface{

    function findOneBy(array $params)
    {
        $cityMunicipality = CityMunicipality::where($params)->first();

        if ($cityMunicipality)
        {
            return $cityMunicipality;
        }

        throw new DbException('City/Municipality not found');
    }

    function firstOrNew(array $params)
    {
        $cityMunicipality = CityMunicipality::firstOrNew($params);

        if ($cityMunicipality)
        {
            return $cityMunicipality;
        }

        throw new DbException('Cannot find or create city/municipality');
    }

    function save(CityMunicipality $cityMunicipality)
    {
        if ($cityMunicipality->save())
        {
            return $cityMunicipality;
        }

        throw new DbException('City/Municipality not saved!');
    }

    public function findOneById($id)
    {
        $query = CityMunicipality::where('id', $id);

        $result = $query->first();

        if ($result)
            return $result;

        throw new DbException('City not found!');
    }
}