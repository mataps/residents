<?php

namespace SourceScript\Profiling\Repositories;


use CityMunicipality;

interface CityMunicipalityRepositoryInterface {

    function findOneBy(array $params);

    function firstOrNew(array $params);

    function save(CityMunicipality $cityMunicipality);
}