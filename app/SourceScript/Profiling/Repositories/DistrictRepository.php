<?php

namespace SourceScript\Profiling\Repositories;

use District;
use SourceScript\Common\Exceptions\DbException;

class DistrictRepository implements DistrictRepositoryInterface{

    function findOneBy(array $params)
    {
        $district = District::where($params)->first();

        if ($district)
        {
            return $district;
        }

        throw new DbException('District not found');
    }

    function firstOrNew(array $params)
    {
        $district = District::firstOrNew($params);

        if ($district)
        {
            return $district;
        }

        throw new DbException('Cannot find or create district');
    }

    function save(District $district)
    {
        if ($district->save())
        {
            return $district;
        }

        throw new DbException('District not saved!');
    }
}