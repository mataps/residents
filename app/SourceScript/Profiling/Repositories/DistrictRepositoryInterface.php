<?php

namespace SourceScript\Profiling\Repositories;


use District;

interface DistrictRepositoryInterface {

    function findOneBy(array $params);

    function firstOrNew(array $params);

    function save(District $district);
}