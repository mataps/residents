<?php namespace SourceScript\Profiling\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use Household;
use ResidentHousehold;

class HouseholdRepository implements HouseholdRepositoryInterface{

    /**
     * @var GenericFinderInterface
     */
    private $finder;

    /**
     * @var DistrictRepositoryInterface
     */
    private $districtRepository;

    /**
     * @var BarangayRepositoryInterface
     */
    private $barangayRepository;

    /**
     * @var CityMunicipalityRepositoryInterface
     */
    private $cityMunicipalityRepository;

    function __construct(GenericFinderInterface $finder,
        DistrictRepositoryInterface $districtRepository,
        BarangayRepositoryInterface $barangayRepository,
        CityMunicipalityRepositoryInterface $cityMunicipalityRepository)
    {
        $this->finder = $finder;
        $this->districtRepository   = $districtRepository;
        $this->barangayRepository   = $barangayRepository;
        $this->cityMunicipalityRepository = $cityMunicipalityRepository;
    }

    /**
     * @param $id
     * @throws DbException
     * @return Household
     */
    function findOneById($id)
    {
        $query = Household::where('id', $id);

        $result = $this->finder->findOne($query);

        if ($result)
        {
            return $result;
        }

        throw new DbException('Household not found!');
    }


    /**
     * @param  $ids
     * @return Collection
     */
    public function findHouseholds($ids)
    {
        $ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return Household::whereIn('id', $ids)->get();
    }

    /**
     * @param Household $resident
     * @return Household
     * @throws DbException
     */
    function save(Household $household)
    {
        $creator = $household->removeAttribute('creator');
        $updater = $household->removeAttribute('updater');
        $district = $household->removeAttribute('district');
        $barangay = $household->removeAttribute('barangay');
        $cityMunicipality = $household->removeAttribute('cityMunicipality');

        if ($district)
        {
            $household->district_id = $district->id ?: $this->districtRepository->save($district)->id;
        }

        if ($barangay)
        {
            $household->barangay_id = $barangay->id ?: $this->barangayRepository->save($barangay)->id;
        }

        if ($cityMunicipality)
        {
            $household->city_municipality_id = $cityMunicipality->id ?: $this->cityMunicipalityRepository->save($cityMunicipality)->id;
        }

        $household->created_by = $creator->id;
        $household->modified_by = $updater->id;

        if ($household->save())
        {
            return $household;
        }

        throw new DbException('Household not saved!');
    }

    /**
     * @param Household $household
     * @throws DbException
     */
    function delete(Household $household)
    {
        if(! $household->delete() && ! $household->residents()->detach() )
        {
            throw new DbException('Household not deleted!');
        }
    }

    function deleteHouseholds($households)
    {
        ResidentHousehold::whereIn('household_id', $households)->delete();

        $deleted = Household::destroy($households);

        if ( ! $deleted)
        {
            throw new DbException('Households not deleted!');
        }
    }
}