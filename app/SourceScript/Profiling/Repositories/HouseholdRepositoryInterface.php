<?php

namespace SourceScript\Profiling\Repositories;


use Household;

interface HouseholdRepositoryInterface {

    /**
     * @param $id
     * @return Household
     */
    function findOneById($id);

    /**
     * @param Household $resident
     * @return Household
     */
    function save(Household $resident);

    /**
     * @param Household $resident
     * @return void
     */
    function delete(Household $resident);
} 