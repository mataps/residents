<?php

namespace SourceScript\Profiling\Repositories;

use Photo;
use SourceScript\Common\Exceptions\DbException;

class PhotoRepository implements PhotoRepositoryInterface{

    function save(Photo $photo)
    {
        if ($photo->save())
        {
            return $photo;
        }

        throw new DbException('Photo not saved!');
    }
}