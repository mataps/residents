<?php

namespace SourceScript\Profiling\Repositories;

use Photo;

interface PhotoRepositoryInterface {

    function save(Photo $photo);
}