<?php namespace SourceScript\Profiling\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use Position;

class PositionRepository implements PositionRepositoryInterface {


	/**
	 * @var GenericFinderInterface
	 */
	private $finder;


	function __construct(GenericFinderInterface $genericFinderInterface)
	{
		$this->finder = $genericFinderInterface;
	}

	/**
	 * @param  int $id
	 * @return Position
	 */
	public function findOneById($id)
	{
		$query = Position::where('id', $id);

		$result = $this->finder->findOne($query);

		if($result)
			return $result;

		throw new DbException("Position not found!");
	}


	/**
	 * @param  Position $position
	 * @throws DbException
	 * @return Position
	 */
	public function save(Position $position)
	{
		if($position->save())
			return $position;

		throw new DbException("Position not saved!");
	}


	/**
	 * @param  Position $position
	 * @throws DbException
	 * @return void
	 */
	public function delete(Position $position)
	{
		if( $position->affiliations()->detach() && ! $position->delete())
			throw new DbException("Affiliation not deleted!");
	}


	/**
	 * @param  string $ids
	 * @return Collection
	 */
	public function findPositions($ids)
	{
		$ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

		return Position::whereIn('id', $ids)->get();
	}


	/**
	 * @param  array  $positions
	 * @throws DbException
	 * @return void
	 */
	public function deletePositions(array $positions)
	{
		$deleted = Position::destroy($positions);

        if (!$deleted)
            throw new DbException('Affiliations not deleted!');
	}

}