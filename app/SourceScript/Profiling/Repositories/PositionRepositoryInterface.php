<?php namespace SourceScript\Profiling\Repositories;

use Position;

interface PositionRepositoryInterface {

	/**
	 * @param  int $id
	 * @return Position
	 */
	public function findOneById($id);


	/**
	 * @param  Position $position
	 * @throws DbException
	 * @return Position
	 */
	public function save(Position $position);


	/**
	 * @param  Position $position
	 * @throws DbException
	 * @return void
	 */
	public function delete(Position $position);


	/**
	 * @param  string $ids
	 * @return Collection
	 */
	public function findPositions($ids);


	/**
	 * @param  array  $positions
	 * @throws DbException
	 * @return void
	 */
	public function deletePositions(array $positions);
}