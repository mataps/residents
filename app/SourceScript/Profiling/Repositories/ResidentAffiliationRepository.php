<?php namespace SourceScript\Profiling\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use ResidentAffiliation;
use Resident;

class ResidentAffiliationRepository implements ResidentAffiliationRepositoryInterface {

    function __construct(GenericFinderInterface $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param $id
     * @throws DbException
     * @return ResidentAffilation
     */
    function findOneById($id)
    {
        $query = ResidentAffiliation::where('id', $id);

        $result = $this->finder->findOne($query);

        if ($result)
        {
            return $result;
        }

        throw new DbException('Resident affilation not found!');
    }

    /**
     * @param ResidentAffilation $resident
     * @return ResidentAffilation
     * @throws DbException
     */
    function save(ResidentAffiliation $residentAffilation)
    {
        if ($residentAffilation->save())
        {
            return $residentAffilation;
        }

        throw new DbException('Resident affilation not saved!');
    }

    /**
     * @param Affiliation $resident
     * @throws DbException
     */
    function delete(ResidentAffiliation $residentAffiliation)
    {
        if ( ! $residentAffiliation->permissions()->detach() && ! $residentAffiliation->delete())
        {
            throw new DbException('Resident affilation not deleted!');
        }
    }
}