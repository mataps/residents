<?php

namespace SourceScript\Profiling\Repositories;


use ResidentAffiliation;

interface ResidentAffiliationRepositoryInterface {

    /**
     * @param $id
     * @return Affiliation
     */
    function findOneById($id);

    /**
     * @param Affiliation $resident
     * @return Affiliation
     */
    function save(ResidentAffiliation $residentAffiliation);

    /**
     * @param Affiliation $resident
     * @return void
     */
    function delete(ResidentAffiliation $residentAffiliation);
} 