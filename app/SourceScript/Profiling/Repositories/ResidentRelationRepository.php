<?php namespace SourceScript\Profiling\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use ResidentRelationship;

class ResidentRelationRepository implements ResidentRelationRepositoryInterface {

    /**
     * @var GenericFinderInterface
     */
    private $finder;

    function __construct(
        GenericFinderInterface $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param $id
     * @throws DbException
     * @return ResidentRelationship
     */
    function findOneById($id)
    {
        $query = ResidentRelationship::where('id', $id);

        $result = $this->finder->findOne($query);

        if ($result)
        {
            return $result;
        }

        throw new DbException('Resident Relationship not found!');
    }

    /**
     * @param ResidentRelationship $residentRelationship
     * @return ResidentRelationship
     * @throws DbException
     */
    function save(ResidentRelationship $residentRelationshipRelationship)
    {
		if ($residentRelationshipRelationship->save())
        {
            return $residentRelationshipRelationship;
        }

        throw new DbException('Resident Relationship not saved!');     	   
    }

    /**
     * @param ResidentRelationship $residentRelationship
     * @throws DbException
     */
    function delete(ResidentRelationship $residentRelationship)
    {
        if ($residentRelationship->delete())
        {
            throw new DbException('Resident Relationship not deleted!');
        }
    }
}