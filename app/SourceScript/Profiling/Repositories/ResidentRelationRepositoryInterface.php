<?php namespace SourceScript\Profiling\Repositories;

use ResidentRelationship;

interface ResidentRelationRepositoryInterface {

    /**
     * @param $id
     * @return Affiliation
     */
    function findOneById($id);

    /**
     * @param Affiliation $resident
     * @return Affiliation
     */
    function save(ResidentRelationship $residentRelationship);

    /**
     * @param Affiliation $resident
     * @return void
     */
    function delete(ResidentRelationship $residentRelationship);
}