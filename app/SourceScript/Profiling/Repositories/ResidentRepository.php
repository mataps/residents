<?php

namespace SourceScript\Profiling\Repositories;


use DB;
use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use Resident;

class ResidentRepository implements ResidentRepositoryInterface{

    /**
     * @var GenericFinderInterface
     */
    private $finder;
    /**
     * @var DistrictRepositoryInterface
     */
    private $districtRepository;
    /**
     * @var BarangayRepositoryInterface
     */
    private $barangayRepository;
    /**
     * @var CityMunicipalityRepositoryInterface
     */
    private $cityMunicipalityRepository;
    /**
     * @var PhotoRepositoryInterface
     */
    private $photoRepository;

    function __construct(
        GenericFinderInterface $finder,
        DistrictRepositoryInterface $districtRepository,
        BarangayRepositoryInterface $barangayRepository,
        CityMunicipalityRepositoryInterface $cityMunicipalityRepository,
        PhotoRepositoryInterface $photoRepository
    )
    {
        $this->finder               = $finder;
        $this->districtRepository   = $districtRepository;
        $this->barangayRepository   = $barangayRepository;
        $this->photoRepository      = $photoRepository;
        $this->cityMunicipalityRepository = $cityMunicipalityRepository;
    }

    /**
     * @param $id
     * @throws DbException
     * @return Resident
     */
    function findOneById($id)
    {
        $query = Resident::where('id', $id);

        $result = $this->finder->findOne($query);

        if ($result)
        {
            return $result;
        }

        throw new DbException('Resident not found!');
    }

    /**
     * @param $ids
     * @return mixed
     */
    function findResidents($ids)
    {
        $ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return Resident::whereIn('id', $ids)->get();
    }

    /**
     * @param Resident $resident
     * @return Resident
     * @throws DbException
     */
    function save(Resident $resident)
    {
        $creator = $resident->removeAttribute('creator');
        $updater = $resident->removeAttribute('updater');
        $district = $resident->removeAttribute('district');
        $barangay = $resident->removeAttribute('barangay');
        $profilePic = $resident->removeAttribute('profilePic');
        $cityMunicipality = $resident->removeAttribute('cityMunicipality');

        if ($district)
        {
            $resident->district_id = $district->id ?: $this->districtRepository->save($district)->id;
        }

        if ($barangay)
        {
            $resident->barangay_id = $barangay->id ?: $this->barangayRepository->save($barangay)->id;
        }

        if ($cityMunicipality)
        {
            $resident->city_municipality_id = $cityMunicipality->id ?: $this->cityMunicipalityRepository->save($cityMunicipality)->id;
        }

        if ($profilePic)
        {
            $resident->photo_id = $this->photoRepository->save($profilePic)->id;
        }

        $resident->created_by = $creator->id;
        $resident->modified_by = $updater->id;
        $resident->full_name = sprintf('%s %s %s', $resident->first_name, $resident->middle_name, $resident->last_name);

        if ($resident->save())
        {
            return $resident;
        }

        throw new DbException('Resident not saved!');
    }

    /**
     * @param Resident $resident
     * @throws DbException
     */
    function delete(Resident $resident)
    {
        if ( ! $resident->delete())
        {
            throw new DbException('Resident not deleted!');
        }
    }

    /**
     * @param array $residents
     * @throws DbException
     */
    function deleteResidents(array $residents)
    {
        $deleted = Resident::destroy($residents);

        if ( ! $deleted)
        {
            throw new DbException('Residents not deleted!');
        }
    }

    function findAll(array $columns = array('*'))
    {
        $query = Resident::query();

        return $this->finder->query($query, $columns);
    }

    function countLastNames()
    {
        $query = 'SELECT lastname, count(*) as count FROM ( SELECT last_name as lastname from residents UNION ALL SELECT middle_name as lastname FROM residents)lastnames_list GROUP BY lastname';
        return DB::select(DB::raw($query));
    }

    function resetLastNameCounts(array $lastnames)
    {
        DB::beginTransaction();
        DB::table('lastnames')->truncate();
        foreach ($lastnames as $lastname)
        {
            DB::table('lastnames')->insert([
                'last_name' => $lastname->lastname,
                'count' => $lastname->count
            ]);
        }
        DB::commit();
    }

    function incrementLastNames(array $lastnames)
    {
        DB::beginTransaction();
        foreach ($lastnames as $lastname)
        {
            $query = "INSERT INTO lastnames(last_name, count) VALUES (:lastname, 1) ON DUPLICATE KEY UPDATE count = count + 1";
            DB::insert(DB::raw($query), compact('lastname'));
        }
        DB::commit();
    }

    function decrementLastNames(array $lastnames)
    {
        DB::beginTransaction();
        foreach ($lastnames as $lastname)
        {
            $query = "INSERT INTO lastnames(last_name, count) VALUES (:lastname, 0) ON DUPLICATE KEY UPDATE count = count - 1";
            DB::insert(DB::raw($query), compact('lastname'));
        }
        DB::commit();
    }
}