<?php

namespace SourceScript\Profiling\Repositories;


use Illuminate\Support\Collection;
use Resident;

interface ResidentRepositoryInterface {

    /**
     * @param array $columns
     * @return mixed
     */
    function findAll(array $columns = array('*'));

    /**
     * @param $id
     * @return Resident
     */
    function findOneById($id);

    /**
     * @param Resident $resident
     * @return Resident
     */
    function save(Resident $resident);

    /**
     * @param Resident $resident
     * @return void
     */
    function delete(Resident $resident);

    /**
     * @param array $residents
     * @return mixed
     */
    function deleteResidents(array $residents);

    function countLastNames();

    function resetLastNameCounts(array $lastnames);

    function incrementLastNames(array $lastnames);

    function decrementLastNames(array $lastnames);
}