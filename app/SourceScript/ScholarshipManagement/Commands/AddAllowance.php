<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\Common\Exceptions\ValidationException;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;
use SourceScript\ScholarshipManagement\Repositories\AllowanceRepositoryInterface;
use User;
use AllowancePolicy;
use AllowanceRelease;
use Allowance;
use Semester;

class AddAllowance {

	/**
	 * History action
	 *
	 * @var string
	 */
	public $history = 'add_resource';

	/**
	 * @var AllowanceRepositoryInterface
	 */
	private $allowanceRepository;


	/**
	 * @var ScholarshipManagementInterface
	 */
	private $scholarshipManagementService;


	function __construct(
		AllowanceRepositoryInterface 	$allowanceRepositoryInterface,
		ScholarshipManagementInterface 	$scholarshipManagementInterface)
	{
		$this->allowanceRepository 			= $allowanceRepositoryInterface;
		$this->scholarshipManagementService = $scholarshipManagementInterface;
	}


	/**
	 * @param  array       $inputs
	 * @param  Semester    $semester
	 * @param  User        $creator
	 * @return Allowance
	 */
	public function handle(array $inputs, Semester $semester, AllowanceRelease $release, User $creator)
	{
		if($this->scholarshipManagementService->findAllowancesByAttributes(['allowance_release_id' => $release->id, 'semester_id' => $semester->id]))
			throw new ValidationException(['allowance' => 'Allowance already exists.']);


		$allowance = new Allowance;

		$allowance->allowance_release_id = $release->id;
		$allowance->semester_id = $semester->id;
		$allowance->stage 		= 'for_claiming';
		$allowance->created_by = $creator->id;
		$allowance->modified_by = $creator->id;
		$allowance->scholarship_id = $semester->scholarship->id;

		if($release->fixed_amount)
		{
			//fixed amount
			$allowance->policy_id = null;
			$allowance->amount = $release->amount;
		}
		else
		{
			if($semester->is_probation)
			{
				$allowance->amount = $semester->scholarshipType->probation_amount;
			}
			else
			{
				// Check if this is the scholar's first semester
				if(isset($semester->allowance_basis_id))
				{
					$policy = $this->getPolicy($semester);
					if($policy)
					{
						$allowance->policy_id = $policy->id;
						$allowance->amount = $policy->amount;	
					}
					else
					{
						throw new ValidationException(['allowance' => 'No policy found for gwa.']);
					}
				}
				else
				{
					$allowance->amount = $semester->scholarship_type->initial_allowance;
				}	
			}
			
		}


		$this->allowanceRepository->save($allowance);

		return $allowance;
	}

	/**
	 * Gets the policy for semester
	 *
	 * @param  Semester $semester
	 * @return AllowancePolicy
	 */
	private function getPolicy(Semester $semester)
	{
		return $this->scholarshipManagementService->findAllowancePolicyBySemester($semester);
	}
}
