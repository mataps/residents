<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\ScholarshipManagement\Repositories\AllowanceReleaseRepositoryInterface;
use SourceScript\ScholarshipManagement\Repositories\ScholarshipTypeRepositoryInterface;
use User;
use AllowanceRelease;
use SourceScript\Common\Validations\ValidableTrait;

class AddAllowanceRelease {

	use ValidableTrait;

	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'add_resource';

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'term' 					=> 'required',
		'school_year' 			=> 'required|regex:([1-2][0-9][0-9][0-9]-[1-2][0-9][0-9][0-9])',
		'fixed_amount' 			=> 'required|boolean',
		'amount' 				=> 'integer',
		'scholarship_types' 	=> 'required|not_in:0',
		'expiring_at' 			=> 'date'
	];


	/**
	 * @var AllowanceReleaseRepositoryInterface
	 */
	private $allowanceReleaseRepository;


	/**
	 * @var ScholarshipTypeRepositoryInterface
	 */
	private $scholarshipTypeRepository;


	function __construct(AllowanceReleaseRepositoryInterface $allowanceReleaseRepositoryInterface,
		ScholarshipTypeRepositoryInterface $scholarshipTypeRepositoryInterface)
	{
		$this->allowanceReleaseRepository = $allowanceReleaseRepositoryInterface;
		$this->scholarshipTypeRepository = $scholarshipTypeRepositoryInterface;
	}


	/**
	 * @param  array  $inputs
	 * @param  User   $user
	 * @return AllowanceRelease
	 */
	public function handle(array $inputs, User $user)
	{
		if(isset($inputs['expiring_at'])) {
			$inputs['expiring_at'] = date('U', strtotime($inputs['expiring_at']));
		}


		$allowanceRelease = new AllowanceRelease;

		$allowanceRelease->fill(array_except($inputs, ['scholarship_types']));

		$allowanceRelease->created_by = $user->id;
		$allowanceRelease->modified_by = $user->id;

		$this->allowanceReleaseRepository->save($allowanceRelease);

		$allowanceRelease->generateUuid();

		$scholarshiptypes = $this->scholarshipTypeRepository->findScholarshipTypes($inputs['scholarship_types']);
		
		$allowanceRelease->scholarshipTypes()->sync($scholarshiptypes->modelKeys());

		return $allowanceRelease;
	}
}