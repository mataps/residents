<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\ScholarshipManagement\Repositories\AllowanceRepositoryInterface;
use AllowanceRelease;
use Allowance;
use SourceScript\Common\Collections\ResultCollection;
use Illuminate\Support\Collection;
use User;
use Semester;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;

class AddAllowances {

	/**
	 * @var AllowanceRepositoryInterface
	 */
	private $allowanceRepository;

	function __construct(
		AllowanceRepositoryInterface 	$allowanceRepositoryInterface,
		ScholarshipManagementInterface $scholarshipManagementInterface)
	{
		$this->scholarshipManagementService = $scholarshipManagementInterface;
		$this->allowanceRepository 			= $allowanceRepositoryInterface;
	}


	/**
	 * @param  array            $inputs
	 * @param  [type]           $allowanceRelease
	 * @param  ResultCollection $semesters
	 * @param  User             $creator
	 * @return Collection
	 */
	public function handle(array $inputs, AllowanceRelease $allowanceRelease, ResultCollection $semesters, User $creator)
	{
		$success = new ResultCollection;
		$fail = new ResultCollection;
		foreach($semesters as $semester)
		{
			if($this->addAllowance($allowanceRelease, $semester, $creator))
			{
				$success->push($semester);
			}
			else
			{
				$fail->push($semester);
			}
		}

		$result = ['success' => $success, 'fail' => $fail];

		return $result;
	}

	/**
	 * @param AllowanceRelease $release
	 * @param Semester         $semester
	 * @param [type]           $creator
	 */
	private function addAllowance(AllowanceRelease $release, Semester $semester, $creator)
	{
		if($this->scholarshipManagementService->findAllowancesByAttributes(['allowance_release_id' => $release->id, 'semester_id' => $semester->id])) return false;

		$allowance = new Allowance;

		$allowance->allowance_release_id = $release->id;
		$allowance->semester_id = $semester->id;
		$allowance->stage 		= 'for_claiming';
		$allowance->created_by = $creator->id;
		$allowance->modified_by = $creator->id;
		$allowance->scholarship_id = $semester->scholarship->id;

		if($release->fixed_amount)
		{
			//fixed amount
			$allowance->policy_id = null;
			$allowance->amount = $release->amount;
		}
		else
		{
			if($semester->is_probation)
			{
				$allowance->amount = $semester->scholarshipType->probation_amount;
			}
			else
			{
				if(isset($semester->allowance_basis_id))
				{
					$policy = $this->getPolicy($semester);
					if($policy)
					{
						$allowance->policy_id = $policy->id;
						$allowance->amount = $policy->amount;	
					}
					else
					{
						return false;
					}
				}
				else
				{
					$allowance->amount = $semester->scholarship_type->initial_allowance;
				}	
			}
			
		}
		

		$this->allowanceRepository->save($allowance);

		return $allowance;
	}

	/**
	 * Gets the policy for semester
	 * 
	 * @param  Semester $semester
	 * @return AllowancePolicy
	 */
	private function getPolicy(Semester $semester)
	{
		return $this->scholarshipManagementService->findAllowancePolicyBySemester($semester);
	}
}