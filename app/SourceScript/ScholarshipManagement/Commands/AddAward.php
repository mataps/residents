<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\ScholarshipManagement\Repositories\AwardRepositoryInterface;
use Award;
use User;
use SourceScript\Profiling\Factories\AttributionsFactory;

class AddAward {

	use ValidableTrait;

	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'add_resource';

	/**
	 * Validation Rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'name' 		=> 'required|unique:awards'
	];


	/**
	 * @var AwardRepositoryInterface
	 */
	private $awardRepository;

	/**
	 * @var AttributionsFactory
	 */
	private $attributionsFactory;

	function __construct(
		AwardRepositoryInterface $awardRepository,
		AttributionsFactory $attributionsFactory)
	{
		$this->awardRepository = $awardRepository;
		$this->attributionsFactory = $attributionsFactory;
	}


	/**
	 * @param  array  $inputs
	 * @param  User   $creator
	 * @return School
	 */
	function handle(array $inputs, User $creator)
	{
		$award = new Award;
		$award->fill($inputs);
		$award->created_by = $creator->id;
		$award->modified_by = $creator->id;

		$this->awardRepository->save($award);

		$attributions = $this->attributionsFactory->get($inputs['attributions'], $creator);
		$award->attributions()->attach($attributions->modelKeys());


		return $award;
	}
}