<?php namespace SourceScript\ScholarshipManagement\Commands;

use Scholarship;
use User;
use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\ScholarshipManagement\Repositories\AwardRepositoryInterface;

class AddAwardToScholarship {

	use ValidableTrait;

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'award_id' 		=> 'required|exists:awards,id',
		// 'semester_id' 	=> 'required|exists:semesters,id'
	];

	/**
	 * @var AwardRepositoryInterface
	 */
	private $awardRepository;

	function __construct(AwardRepositoryInterface $awardRepositoryInterface)
	{
		$this->awardRepository = $awardRepositoryInterface;
	}


	/**
	 * @param  array       $inputs
	 * @param  Scholarship $scholarship
	 * @param  User        $creator
	 * @return void
	 */
	function handle(array $inputs, Scholarship $scholarship, User $creator)
	{
		//TODO
		//YOLO
		try{
			$awards = $scholarship->awards->lists('id');
			$scholarship->awards()->detach($inputs['award_id']);

			$scholarship->awards()->attach([$inputs['award_id'] => ['semester_id' => $inputs['semester_id'], 'year' => $inputs['year'], 'remarks' => $inputs['remarks'], 'created_by' => $creator->id, 'modified_by' => $creator->id]]);

			$award = $this->awardRepository->findOneById($inputs['award_id']);

			$resident = $scholarship->resident;

			$resident->attributions()->detach($award->attributions->lists('id'));

			$resident->attributions()->attach($award->attributions->lists('id'));
		
			return true;
		}
		catch(Exception $e){
			return false;
		}

	}
}