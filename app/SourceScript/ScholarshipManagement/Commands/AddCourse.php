<?php namespace SourceScript\ScholarshipManagement\Commands;

use Course;
use SourceScript\ScholarshipManagement\Repositories\CourseRepositoryInterface;
use User;

class AddCourse {


	/**
	 * @var CourseRepositoryInterface
	 */
	private $repository;


	function __construct(CourseRepositoryInterface $courseRepositoryInterface)
	{
		$this->repository = $courseRepositoryInterface;
	}

	/**
	 * @param  array  $inputs
	 * @param  User   $creator
	 * @return Course
	 */
	public function handle(array $inputs, User $creator)
	{
		$course = new Course;

		$course->fill($inputs);

		$course->created_by = $creator->id;
		$course->modified_by = $creator->id;

		$this->repository->save($course);

		return $course;
	}
}