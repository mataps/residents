<?php namespace SourceScript\ScholarshipManagement\Commands;

use User;
use Grade;
use Semester;
use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\ScholarshipManagement\Repositories\GradeRepositoryInterface;

class AddGrade {

	use ValidableTrait;

	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'add_resource';

	/**
	 * Validation Rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'grade' 		=> 'required',
		'subject_id' 	=> '',
		'units' 		=> 'required'
	];

	/**
	 * @var GradeRepositoryInterface
	 */
	private $gradeRepository;


	function __construct(GradeRepositoryInterface $gradeRepositoryInterface)
	{
		$this->gradeRepository = $gradeRepositoryInterface;
	}


	/**
	 * @param  array    $inputs
	 * @param  Semester $semester
	 * @param  User     $creator
	 * @return Grade
	 */
	public function handle(array $inputs, Semester $semester, User $creator)
	{
		$grade 					= new Grade;
		$grade->fill($inputs);
		$grade->semester_id 	= $semester->id;
		$grade->created_by 		= $creator->id;
		$grade->modified_by 	= $creator->id;

		$this->gradeRepository->save($grade);

		return $grade;
	}
}