<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\ScholarshipManagement\Repositories\AllowancePolicyRepositoryInterface;
use SourceScript\ScholarshipManagement\Repositories\ScholarshipTypeRepositoryInterface;
use AllowancePolicy;
use User;

class AddPolicy {

	use ValidableTrait;

	/**
	 * Validation Rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'id' 					=> 'required|integer|exists:scholarship_types,id',
		'allowance_policies' 	=> 'required|array'
	];

	/**
	 * @var AllowancePolicyRepositoryInterface
	 */
	private $allowancePolicyRepository;


	/**
	 * @var ScholarshipTypeRepositoryInterface
	 */
	private $scholarshipTypeRepository;


	function __construct(AllowancePolicyRepositoryInterface $allowancePolicyRepositoryInterface,
		ScholarshipTypeRepositoryInterface $scholarshipTypeRepositoryInterface)
	{
		$this->allowancePolicyRepository = $allowancePolicyRepositoryInterface;
		$this->scholarshipTypeRepository = $scholarshipTypeRepositoryInterface;
	}


	/**
	 * @param  array  $inputs
	 * @param  User   $creator
	 * @return AllowancePolicy
	 */
	function handle(array $inputs, User $creator)
	{
		extract($inputs);

		$scholarshipType = $this->scholarshipTypeRepository->findOneById($id);
		
		$this->allowancePolicyRepository->deleteByScholarshipType($scholarshipType);

		foreach($allowance_policies as $school_level => $school_levels)
		{
			foreach($school_levels as $school_type => $rules)
			{
				foreach($rules as $index => $rule)
				{
					$policy = new AllowancePolicy;
					$policy->amount = $rule['amount'];
					$policy->school_level = $school_level;
					$policy->school_type = $school_type;
					$policy->from = $rule['from'];
					$policy->to = $rule['to'];
					$policy->created_by = $creator->id;
					$policy->modified_by = $creator->id;
					$policy->scholarship_type_id = $id;

					$policy->save();

					// $scholarshipType->allowancePolicies()->attach([
					// 	'amount' 				=> $rule['amount'],
					// 	'school_level' 			=> $rule['school_level'],
					// 	'school_type' 			=> $rule['school_type'],
					// 	'from' 					=> $rule['from'],
					// 	'to' 					=> $rule['to'],
					// 	'created_by' 			=> $creator->id,
					// 	'modified_by' 			=> $creator->id
					// 	]);
				}
			}
		}
	}
}