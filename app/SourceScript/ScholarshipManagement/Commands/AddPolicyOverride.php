<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\ScholarshipManagement\Repositories\AllowancePolicyOverrideRepositoryInterface;
use SourceScript\ScholarshipManagement\Repositories\ScholarshipTypeRepositoryInterface;
use SourceScript\Common\Validations\ValidableTrait;
use User;
use AllowancePolicyOverride;

class AddPolicyOverride {

	use ValidableTrait;

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'occurence' 		=> 'required|integer',
		'amount' 			=> 'required|numeric',
		'scholarship_types' => 'required'
	];


	/**
	 * @var AllowancePolicyOverrideRepositoryInterface
	 */
	private $policyOverrideRepository;


	/**
	 * @var ScholarshipTypeRepositoryInterface
	 */
	private $scholarshipTypeRepository;


	function __construct(
		AllowancePolicyOverrideRepositoryInterface $policyOverrideRepositoryInterface,
		ScholarshipTypeRepositoryInterface $scholarshipTypeRepositoryInterface)
	{
		$this->policyOverrideRepository = $policyOverrideRepositoryInterface;
		$this->scholarshipTypeRepository = $scholarshipTypeRepositoryInterface;
	}


	/**
	 * @param  array  $inputs
	 * @param  User   $creator
	 * @return AllowancePolicyOverride
	 */
	public function handle(array $inputs, User $creator)
	{
		$policyOverride = new AllowancePolicyOverride;

		$policyOverride->fill(array_except($inputs, ['scholarship_types']));

		$policyOverride->created_by = $creator->id;
		$policyOverride->modified_by = $creator->id;

		$this->policyOverrideRepository->save($policyOverride);

		$scholarshipTypes = $this->scholarshipTypeRepository->findScholarshipTypes($inputs['scholarship_types']);

		$policyOverride->scholarshipTypes()->attach($scholarshipTypes);

		return $policyOverride;
	}
}