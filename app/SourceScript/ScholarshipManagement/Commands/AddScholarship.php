<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\ScholarshipManagement\Repositories\ScholarshipRepositoryInterface;
use Scholarship;
use User;

class AddScholarship {

	use ValidableTrait;

	/**
	 * Validation Rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'resident_id' 	=> 'required|integer',
		'school_id' 	=> 'integer',
		'status' 		=> 'required|in:waiting,for_verification,active,closed,denied,stop,graduated',
		'applied_at' 	=> 'required',
		'father_id' => 'exists:residents,id',
		'mother_id' => 'exists:residents,id',
		'guardian_id' => 'exists:residents,id'
	];


	/**
	 * @var ScholarshipRepositoryInterface
	 */
	private $scholarshipRepository;


	function __construct(ScholarshipRepositoryInterface $scholarshipRepository)
	{
		$this->scholarshipRepository = $scholarshipRepository;
	}


	/**
	 * @param  array  $inputs
	 * @param  User   $creator
	 * @return School
	 */
	function handle(array $inputs, User $creator)
	{
		$scholarship = new Scholarship;
		$scholarship->fill($inputs);
		$scholarship->created_by = $creator->id;
		$scholarship->modified_by = $creator->id;

		$this->scholarshipRepository->save($scholarship);

		$scholarship->generateUuid();

		return $scholarship;
	}
}