<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\ScholarshipManagement\Repositories\ScholarshipTypeRepositoryInterface;
use ScholarshipType;
use User;

class AddScholarshipType {

	use ValidableTrait;


	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'add_resource';

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = array(
		'name' => 'required|unique:scholarship_types'
		);


	/**
	 * @var ScholarshipTypeRepositoryInterface
	 */
	private $scholarshipTypeRepository;


	function __construct(ScholarshipTypeRepositoryInterface $scholarshipTypeRepositoryInterface)
	{
		$this->scholarshipTypeRepository = $scholarshipTypeRepositoryInterface;
	}


	/**
	 * @param  array  $inputs
	 * @param  User   $creator
	 * @return ScholarshipType
	 */
	public function handle(array $inputs, User $creator)
	{
		$scholarshipType 				= new ScholarshipType;
		$scholarshipType->fill($inputs);

		$scholarshipType->created_by 	= $creator->id;
		$scholarshipType->modified_by 	= $creator->id;

		$this->scholarshipTypeRepository->save($scholarshipType);

		//TODO: add default allowance polcy
		return $scholarshipType;
	}
}
