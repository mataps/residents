<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\ScholarshipManagement\Repositories\SchoolRepositoryInterface;
use School;
use User;

class AddSchool {

	use ValidableTrait;

	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'add_resource';

	/**
	 * Validation Rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'name' 					=> 'required',
		'type' 					=> 'required|in:public,private',
		'level' 				=> 'required|in:secondary,collegiate',
		'address' 				=> 'required'
	];


	/**
	 * @var SchoolRepositoryInterface
	 */
	private $schoolRepository;


	function __construct(SchoolRepositoryInterface $schoolRepository)
	{
		$this->schoolRepository = $schoolRepository;
	}


	/**
	 * @param  array  $inputs
	 * @param  User   $creator
	 * @return School
	 */
	function handle(array $inputs, User $creator)
	{
		$school = new School;
		$school->fill($inputs);
		$school->created_by = $creator->id;
		$school->modified_by = $creator->id;

		$this->schoolRepository->save($school);

		return $school;
	}
}