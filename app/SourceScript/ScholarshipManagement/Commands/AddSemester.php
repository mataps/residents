<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\ScholarshipManagement\Repositories\SemesterRepositoryInterface;
use SourceScript\Common\Exceptions\ValidationException;
use Semester;
use User;
use Grade;

class AddSemester {

	use ValidableTrait;


	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'add_resource';

	/**
	 * Validation Rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'gwa' 					=> 'required',
		'term' 					=> 'required',
		'school_year' 			=> 'required|regex:([1-2][0-9][0-9][0-9]-[1-2][0-9][0-9][0-9])',
		'scholarship_id' 		=> 'required|integer',
		// 'school_id' 			=> 'required|integer|exists:schools,id',
		// 'scholarship_type_id' 	=> 'required|integer|exists:scholarship_types,id',
		'course_id' 			=> 'exists:courses,id',
		'client_id' 			=> 'exists:residents,id',
		'status'		 		=> 'in:active,waiting',
		'school_level' 			=> 'required|in:secondary,collegiate',
		'grades' 				=> 'array',
		'created_at' => 'date'
	];


	/**
	 * @var SemesterRepositoryInterface
	 */
	private $semesterRepository;

	function __construct(SemesterRepositoryInterface $semesterRepository)
	{
		$this->semesterRepository = $semesterRepository;
	}


	/**
	 * @param  array  $inputs
	 * @param  User   $creator
	 * @return School
	 */
	function handle(array $inputs, User $creator)
	{
		// if(Semester::where('scholarship_id', $inputs['scholarship_id'])->where('school_year', $inputs['school_year'])->where('school_level', $inputs['school_level'])->where('term', $inputs['term'])->get()) throw new ValidationException(["semester" => "School year and school level already taken."]);
		// dd($inputs);

		$semester = new Semester;
		$semester->fill(array_except($inputs, ['grades', 'created_at']));
		
		if(isset($inputs['created_at']) || $inputs['created_at'] != "")
	        {
	           	$semester->timestamps = false;
	            $semester->setCreatedAt(date('Y-m-d H:i:s', strtotime($inputs['created_at'])));
	            $semester->setUpdatedAt(date('Y-m-d H:i:s', strtotime($inputs['created_at'])));
	        }

	     if(isset($inputs['date_encoded']) || $inputs['date_encoded'] != "")
        {
           	$semester->date_encoded = date('Y-m-d H:i:s', strtotime($inputs['date_encoded']));
        }

		$semester->created_by = $creator->id;
		$semester->modified_by = $creator->id;
	
		$this->semesterRepository->save($semester);

		$scholarship = $semester->scholarship;
		$scholarship->school_id = $inputs['school_id'];
		if($scholarship->status == "inactive" && in_array(date("Y"), explode("-", $semester->school_year))) $scholarship->status = "active";
		$scholarship->save();
		$scholarship->generateUuid();

		$semester->beneficiary_id = $scholarship->resident->id;

  
		$semester->save();
		$semester->generateUuid();

		if($inputs['grades'])
		{
			foreach($inputs['grades'] as $grade)
			{
				$g = new Grade;
				if(in_array('subject_id', $grade) ){
					$g->subject_id = $grade['subject_id'];
				}
				$g->units = $grade['units'];
				$g->grade = $grade['grade'];
				$g->semester_id = $semester->id;
				$g->created_by = $creator->id;
				$g->modified_by = $creator->id;

				$g->save();
			}
		}

		return $semester;
	}
}