<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\ScholarshipManagement\Repositories\SubjectRepositoryInterface;
use User;
use Subject;

class AddSubject {

	use ValidableTrait;

	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'add_resource';

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'name' => 'required'
	];


	/**
	 * @var SubjectRepositoryInterface
	 */
	private $subjectRepository;


	function __construct(SubjectRepositoryInterface $subjectRepositoryInterface)
	{
		$this->subjectRepository = $subjectRepositoryInterface;
	}


	/**
	 * @param  array  $inputs
	 * @param  User   $creator
	 * @return Subject
	 */
	function handle(array $inputs, User $creator)
	{
		$subject = new Subject;
		$subject->fill($inputs);
		$subject->created_by = $creator->id;
		$subject->modified_by = $creator->id;

		$this->subjectRepository->save($subject);

		return $subject;
	}
}