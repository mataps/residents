<?php namespace SourceScript\ScholarshipManagement\Commands;

use Config;
use DB;

class ConvertBlobImage 
{

	// This function assumes that the database has already have the list of scholars. Thus this will only migrate semester infos.

	public function handle() 
	{
        ini_set('max_execution_time', 0);
        //record the start time
        $time_start = microtime(true);

        $old_scholars_sql =  "select r.first_name as fn, r.maternal_name as mn, r.last_name as ln, m.municipality_name as city , r.birthdate as bd, s.image from cdodb_test.scholar as s join cdodb_test.resident as r on s.resident_OID = r.OID join cdodb_test.barangay as b on b.OID = r.residence_id join cdodb_test.municipality as m on m.OID = b.municipality_id";



        $scholar_records = DB::select(DB::raw($old_scholars_sql));

        $total = count($scholar_records);
        $counter = 0;

        foreach($scholar_records as $scholar) 
        {
            if(!$scholar){
                continue;
            }
            try{

            
            $counter++;
            echo $counter . "/" . $total . "\r\n";
            $bd = date('Y-m-d', strtotime($scholar->bd));
            if (!is_dir(public_path('scholar_image/'))) {
              // dir doesn't exist, make it
              mkdir(public_path('scholar_image/'));
            }
            if (!is_dir(public_path('scholar_image/' . $scholar->city))) {
              // dir doesn't exist, make it
              mkdir(public_path('scholar_image/' . $scholar->city));
            }

            file_put_contents( public_path('scholar_image/' . $scholar->city . '/' . $scholar->fn . $scholar->mn . $scholar->ln . $bd . ".jpg"), $scholar->image);
            

            }
            catch(ErrorException $e){
                continue;

            }
        }


	}
}