<?php namespace SourceScript\ScholarshipManagement\Commands;


use Semester;
use Elasticsearch\Client;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;
class DeleteIndexSemester {

    protected $client;

    /**
     * @var ScholarshipManagementService
     */
    private $scholarshipManagementService;

     function __construct(
    ScholarshipManagementInterface $scholarshipManagementService)
    {

        $this->scholarshipManagementService = $scholarshipManagementService;

    }

    function handle($input, $semester)
    {     

            $param = \Config::get('elasticsearch');
            
            $this->client = new Client($param);

            $deleteParams = array();
            $deleteParams['index'] = 'galactus_production';
            $deleteParams['type'] = 'semesters';
            $deleteParams['id'] = $semester->id;

            $this->client->delete($deleteParams);
            // if(!isset($input['from_scholarship_index'])){
            //     dd($semester->scholarship);
            //     $this->scholarshipManagementService->execute('IndexScholar', ['from_semester_index' => true], $semester->scholarship);

            // }

            
            return true;
    }
}