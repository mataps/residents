<?php namespace SourceScript\ScholarshipManagement\Commands;
use SourceScript\Common\Collections\ResultCollection;

use Scholarship;

class InactivateScholars {

    function handle($input, ResultCollection $scholars)
    {           
		 foreach($scholars as $scholar){
             $scholar->status = 'inactive';
             $scholar->save();
         }
	
          return true;
    }
}