<?php namespace SourceScript\ScholarshipManagement\Commands;

use Scholarship;
use Elasticsearch\Client;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;

class IndexScholar {

    protected $client;

    /**
     * @var ScholarshipManagementService
     */
    private $scholarshipManagementService;

    function __construct(
    ScholarshipManagementInterface $scholarshipManagementService)
    {

        $this->scholarshipManagementService = $scholarshipManagementService;

    }


    function handle($input, Scholarship $scholar)
    {       
            $scholarship = Scholarship::find($scholar->id);    
            $param = \Config::get('elasticsearch');
            $this->client = new Client($param);
            $awards = [];
            if($scholarship){
                $awards = $scholarship->awards->map(function($award) {
                    return [
                        'name' => $award->name,
                        'year' => $award->pivot->year,
                        'remarks' => $award->pivot->remarks
                    ];
                })->toArray();                
            }



            $semester = $scholar->semesters()->orderBy('school_year', 'desc')->orderBy('term', 'desc')->first();

            $params['body'] = array(
                    'scholarship_type' =>  '',
                    'school_year' => '',
                    'course_name' => '',
                    'school_name' =>  '',
                    'term' => '',

                    'last_name' => $scholar->resident->last_name,
                    'first_name' => $scholar->resident->first_name,
                    'middle_name' => $scholar->resident->middle_name,
                    'birthdate' => $scholar->resident->birthdate,
                    'street' => $scholar->resident->street,
                    'civil_status' => $scholar->resident->civil_status,
                    'birthdate' => date("Y-m-d", strtotime($scholar->resident->birthdate)),
                    'barangay' => $scholar->resident->barangay ? $scholar->resident->barangay->name : '',
                    'cityMunicipality' =>  $scholar->resident->cityMunicipality ? $scholar->resident->cityMunicipality->name : '',
                    'district' => $scholar->resident->district ? $scholar->resident->district->name : '',
                    'affiliations' => $scholar->resident->affiliations->lists('name'),
                    'attributions' => $scholar->resident->attributes->lists('name'),
                    'awards'    => $awards,
                    'status' => $scholar->status,
                    'approved_at' => date("Y-m-d", strtotime($scholar->approved_at)),
                    'applied_at' => date("Y-m-d", strtotime($scholar->applied_at)),
                    'comment' => $scholar->comment,
                    'father_name' => $scholar->father ? $scholar->father->fullName() : $scholar->father_name,
                    'referrer_name' => $scholar->referrer ? $scholar->referrer->full_name : '',
                    'mother_name' => $scholar->mother ? $scholar->mother->fullName() : $scholar->mother_name,
                    'guardian_name' => $scholar->guardian ? $scholar->guardian->fullName() : $scholar->guardian_name,
                    'scholar_contact_no' => $scholar->resident->mobile,
                    'parents_contact_no' => $scholar->parents_contact_no,
                    'others_contact_no' => $scholar->others_contact_no,
                    'es_id' => $scholar->id,
                    'id'   => (int) $scholar->id
                );
            if(count($scholar->semesters) >= 1){
               
                         $params['body']['scholarship_type'] = $semester->scholarshipType ? $semester->scholarshipType->name : '';
                         $params['body']['school_year'] = $semester->school_year;
                         $params['body']['course_name'] = $semester->course ? $semester->course->name : '';
                        $params['body']['school_name'] = $semester->school ? $semester->school->name : '';
                          $params['body']['term'] = $semester->term;
                        
            }

            // dd($params['body']);

            $params['id'] = (int) $scholar->id;
            $params['index'] = 'galactus_production';
            $params['type']  = 'scholarships';
            $this->client->index($params);

            if(!isset($input['from_semester_index'])){
                foreach($scholar->semesters as $sem){
                    $this->scholarshipManagementService->execute('IndexSemester', ['from_scholarship_index' => true], $sem);
                }
            }
          return $scholar;
    }
}