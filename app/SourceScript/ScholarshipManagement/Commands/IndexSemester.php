<?php namespace SourceScript\ScholarshipManagement\Commands;


use Semester;
use Elasticsearch\Client;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;
class IndexSemester {

    protected $client;

    /**
     * @var ScholarshipManagementService
     */
    private $scholarshipManagementService;

    function __construct(
    ScholarshipManagementInterface $scholarshipManagementService)
    {

        $this->scholarshipManagementService = $scholarshipManagementService;

    }

    function handle($input, $semester_ref)
    {     
           $semester = Semester::find($semester_ref->id);      
            // dd('here');
            $total_allowance = 0;
            $allowances = $semester->allowances;

            foreach ($allowances as $allowance) 
            {
                $total_allowance += $allowance->amount;
            }                 



            $param = \Config::get('elasticsearch');
            
            $this->client = new Client($param);
            
            $params['body'] = array(
                    'status' => $semester->status,
                    'uuid' => $semester->uuid,
                    'school_level' => $semester->school_level,
                    'is_probation' => $semester->is_probation,
                    'school_year' => $semester->school_year,
                    'scholarship_type_name' => $semester->scholarshipType->name,
                    'year_level' => $semester->year_level,
                    'term' => $semester->term,
                    'gwa' => $semester->gwa,
                    'subject_count' => $semester->subject_count,
                    'scholarship_id' => $semester->scholarship_id,
                    'course_name' => (isset($semester->course->name)) ? $semester->course->name : '',
                    'school_name' => $semester->school ? $semester->school->name : '',
                    'comment' => $semester->scholarship->comment,
                    'scholarship_status' => $semester->scholarship->status,
                    'father_name' => $semester->scholarship->father_name,
                    'mother_name' => $semester->scholarship->mother_name,
                    'guardian_name' => $semester->scholarship->guardian_name,
                    'parents_contact_no' => $semester->scholarship->parents_contact_no,
                    'others_contact_no' => $semester->scholarship->others_contact_no,
                    'scholar_contact_no' => $semester->scholarship->resident->mobile,
                    'resident_full_name' => $semester->scholarship->resident->full_name,
                    'last_name' => $semester->scholarship->resident->last_name,
                    'first_name' => $semester->scholarship->resident->first_name,
                    'middle_name' => $semester->scholarship->resident->middle_name,
                    'allowances'    => $total_allowance,
                    'es_id' => $semester->id,
                    'created_at' => date('Y-m-d', strtotime($semester->created_at)),
                    'cityMunicipality' => $semester->scholarship->resident->cityMunicipality ? $semester->scholarship->resident->cityMunicipality->name : '',
                    'barangay' => $semester->scholarship->resident->barangay ? $semester->scholarship->resident->barangay->name : '',
                    'id' => (int) $semester->id,
                    'client_name' => $semester->client ? $semester->client->full_name : '',
                    
                );


        
            
            $params['id'] = $semester->id;

           
            $params['index'] = 'galactus_production';
            $params['type']  = 'semesters';
        
            // dd($paramams);

            $this->client->index($params);
            if(!isset($input['from_scholarship_index'])){
                $this->scholarshipManagementService->execute('IndexScholar', ['from_semester_index' => true], $semester->scholarship);

            }

            
            return true;
    }
}