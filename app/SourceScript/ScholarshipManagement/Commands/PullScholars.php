<?php namespace SourceScript\ScholarshipManagement\Commands;

use Config;
use DB;
use Resident;
use School;
use Scholarship;
use AllowanceRelease;
use Semester;
use CityMunicipality;
use Barangay;
use Grade;
use Allowance;
class PullScholars 
{

	// This function assumes that the database has already have the list of scholars. Thus this will only migrate semester infos.

	public function handle() 
	{
        ini_set('max_execution_time', 0);
        //record the start time
        $time_start = microtime(true);

            // Check the status of the allowance
            $allowance_release_sql = "select sr.amount as amount, sem.semester as semester_name, sem.OID as semester_OID, sr.date_created as created_at from cdodb_test.scholar_record as sr join cdodb_test.scholar_sem_info as sem on sem.OID = sr.sem_info_OID group by sr.sem_info_OID, sr.amount";            
            $old_allowance_records = DB::select(DB::raw($allowance_release_sql));  

             $total_allowance_release = count($old_allowance_records);
             $a_counter = 0;

       foreach($old_allowance_records as $old_release) 
        {
            $a_counter++;
            echo $a_counter . "/" . $total_allowance_release . "\r\n"; 

          // Get Term
           
             if($old_release->semester_name != "CANCEL" || $old_release->semester_name != NULL){
                        $semester_details_array = explode("S", $old_release->semester_name);
            //  echo $old_release->semester_name;
                       if(count($semester_details_array) == 0 ){
                        echo "error";
                        break;
                }
                $term =(count($semester_details_array) > 1)  ? $semester_details_array[1] : 0;
                    }
            if($old_release->semester_name == "CANCEL"){
                continue;
            }
            // Build School Year 
            $start_year = ($old_release->semester_OID == "15" || $old_release->semester_OID == "17" ) ? 2014 : $semester_details_array[0];

            $end_year = $start_year + 1;

            $school_year = $start_year . "-" . $end_year;


            // Create an Allowance Release
               $allowance_release = new AllowanceRelease;
               $allowance_release->term = $term;
               $allowance_release->school_year = $school_year;
               $allowance_release->fixed_amount = 1;
               $allowance_release->amount = $old_release->amount;
               $allowance_release->expiring_at = date('Y-m-d', strtotime("2015-01-01"));
               $allowance_release->created_at =  $old_release->created_at;
               $allowance_release->updated_at =  $old_release->created_at;
               $allowance_release->created_by =  1;
               $allowance_release->modified_by =  1;
            $allowance_release->save();
            // Relate a released scholar
        }

        $old_scholars_sql =  "select scholar_record.grades as grades, resident.birthdate as birthdate,resident.precinct as precinct,resident.cellphone as cellphone,resident.telephone as telephone,resident.email as email,resident.is_registered as is_registered,resident.gender as gender,resident.civil_status as civil_status,resident.street as street, municipality.municipality_name as municipality_name, barangay.barangay_name as barangay_name, scholar_record.amount as amount, scholar_record.course_OID as course_OID, semester.start_date as start_date, semester.end_date as end_date,  school.school_name as school_name, school.school_type as school_type, scholar_record.average as gwa, scholar_record.scholar_level as scholar_level, resident.OID as resident_OID, CONCAT_WS(' ', resident.first_name, resident.maternal_name, resident.last_name) as full_name, resident.last_name as last_name, resident.first_name as first_name, resident.maternal_name as maternal_name,  resident.birthdate as birthdate, scholar.father_name as father_name, scholar.mother_name as mother_name, scholar.guardian_name as guardian_name, scholar.guardian_contact_no as guardian_contact_no, scholar.scholar_status as scholar_status, scholar.approval_date as approval_date, scholar.notes as comment, scholar.date_created as scholar_created_at, scholar_record.date_created as scholar_record_created_at, scholar.date_modified as scholar_updated_at, scholar_record.date_modified as scholar_record_updated_at,  semester.semester as semester_name, scholar.guardian_contact_no as contact_no, semester.OID as semester_OID  FROM cdodb_test.scholar as scholar left join cdodb_test.scholar_record  as scholar_record on scholar.OID = scholar_record.scholar_OID left join cdodb_test.resident as resident on scholar.resident_OID = resident.OID left join cdodb_test.scholar_school as school on school.OID = scholar_record.school_OID left join cdodb_test.scholar_sem_info as semester on semester.OID = scholar_record.sem_info_OID left join cdodb_test.barangay as barangay on barangay.OID = resident.residence_id left join cdodb_test.municipality as municipality on municipality.OID = barangay.municipality_id";
      
        $old_scholar_records = DB::select(DB::raw($old_scholars_sql));

        $total = count($old_scholar_records);
 		$counter = 0;

        foreach($old_scholar_records as $old_record) 
        {
            
            try{

            
        	$counter++;
        	echo $counter . "/" . $total . "\r\n";

        	// Get Scholar Year
        	$year_level = "";
        	if ($old_record->scholar_level != "" || $old_record->scholar_level != null ) {
	        	$scholar_level_array  = str_split($old_record->scholar_level);
                if(is_array($scholar_level_array))
                {
                  // if(count($scholar_level_array > 1)) $year_level = $scholar_level_array[1];
               	   $year_level = (isset($scholar_level_array[1])) ? $scholar_level_array[1] : "";
		 }
        	}


        	// Set Scholar Status
            $transfered = false;
        	switch ($old_record->scholar_status) {
        		case 'G':
        				$scholar_status = 'graduated';
                        $semester_status = 'graduated';
        			break;
        		case 'A':
        			$scholar_status = 'inactive';
                    $semester_status = 'active';
        			break;
        		case 'F':
        			$scholar_status = 'for_verification';
                    $semester_status = 'for_verification';
        			break;
        		case 'C':
        			$scholar_status = 'closed';
        			$semester_status = 'closed';
        			break;
                case 'D':
                    $scholar_status = 'closed';
                    $semester_status = 'closed';
                    break;
        		case 'N':
        			$scholar_status = 'waiting';
                    $semester_status = 'waiting';
            		break;
                case 'S':
                    $scholar_status = "stop";
                    $semester_status = 'stop';
                    break; 
               	case 'T':
                    $scholar_status = "inactive";
            		$semester_status = "transfered";
                    $transfered = true;
            		break;
        		default:
                    $scholar_status = "waiting";
        			$semester_status = "waiting";
        			break;
        	}


            
            echo $old_record->full_name;
            echo "\n";
        	// Get Scholar Level

        	$scholar_level = ($scholar_level_array[0] == "C") ? "collegiate" : "secondary";

        	// Get Resident ID base it in full_name and barangay

        	$resident = Resident::where('full_name', $old_record->full_name)
                                ->where('birthdate', $old_record->birthdate)
                                ->first();
                   
        	$resident_id = (isset($resident)) ? $resident->id : null; 
        	if($resident_id == null ){
        		// continue;

                echo "No Resident Record" . $old_record->full_name;
                echo "\n";
                echo $old_record->resident_OID;
                
            
                $resident = new Resident;
                $resident->full_name = $old_record->full_name;
                $resident->first_name = $old_record->first_name;
                $resident->last_name = $old_record->last_name;
                $resident->middle_name = $old_record->maternal_name;
                $resident->birthdate = $old_record->birthdate;
                $resident->precinct = $old_record->precinct;
                $resident->mobile = $old_record->cellphone;
                $resident->phone = $old_record->telephone;
                $resident->email = $old_record->email;
                $resident->is_voter = $old_record->is_registered;
                $resident->gender = ($old_record->gender == "M") ? 'Male' : 'Female';
                if($old_record->civil_status == "S"){
                    $civil_status = "Single";
                }
                else if($old_record->civil_status = "M"){
                    $civil_status = "Married";
                }
                else if($old_record->civil_status = ""){
                    $civil_status = "Married";
                }

                // Address Part
		 echo $old_record->municipality_name;
                $city = str_replace(" City", "", $old_record->municipality_name);
                $city = str_replace(".", "", $city);

                $municipality = CityMunicipality::where('name', $city)->first();
                if($municipality == null){
                    echo $city;
                    break;
                }
                $barangay = Barangay::where('name', "=", $old_record->barangay_name)->where('city_municipality_id', "=", $municipality->id)->first();
                if($barangay == null){
                    echo "No BARANGAY __________";
                    echo $municipality->id . " ";
                    echo $old_record->barangay_name;
                    break;
                }
                 $resident->street = $old_record->street;
                 $resident->city_municipality_id = $municipality->id;
                 $resident->barangay_id = $barangay->id;
                //Contact Number Part
                 $resident->save();

                 $resident_id = $resident->id;
                
                echo "saved";
        	}
         
        	// Get School ID from Galactus Table

        	$school = School::where('name', $old_record->school_name)
        					->first();
        	$school_id = (isset($school)) ? $school->id : null;

        	if($school_id == null) 
        	{
                if($old_record->school_type == "Pr"){
                    $school_type = "private";
                }
                else{
                    $school_type = "public"; 
                }
        		$school = new School;
        		$school->name = $old_record->school_name;
        		$school->type = $school_type;
        		// $school->level = $old_record->scholar_level;
        		$school->created_by = 1;
        		$school->modified_by = 1;
        		$school->save();

        		$school_id = $school->id;


        	}

        	// Get Scholarship ID base it in Resident ID

        	$scholarship = Scholarship::where('resident_id', $resident_id)->first();
			$scholarship_id = ($scholarship) ? $scholarship->id : null;

			if($scholarship_id == null) {
				$scholarship = new Scholarship;
				$scholarship->resident_id = $resident_id;
				$scholarship->school_id = $school_id;
                		$scholarship->comment = $old_record->comment;
                		$scholarship->father_name = $old_record->father_name;
                		$scholarship->mother_name = $old_record->mother_name;
				$scholarship->guardian_contact_no = $old_record->guardian_contact_no;
				$scholarship->status = $scholar_status;
				$scholarship->transferred_at = ($transfered) ? $old_record->scholar_updated_at : null;
                		$scholarship->created_at = $old_record->scholar_created_at;
				$scholarship->updated_at = $old_record->scholar_updated_at;
				$scholarship->created_by = 1;
				$scholarship->modified_by = 1;


				$scholarship->save();

				$scholarship_id = $scholarship->id;
			}

        	// Get Scholarship Type ID
            $record_name_array = $old_record->semester_name;
            $scholarship_type_id = ($old_record->semester_OID == "15" || $old_record->semester_OID == "17" ) ? 7 : 8;


            // Get Term
           
	 if($old_record->semester_name != "CANCEL" || $old_record->semester_name != NULL){
                $semester_details_array = explode("S", $old_record->semester_name);
	//	echo $old_record->semester_name;
       	       if(count($semester_details_array) == 0 ){
				echo "error";
				break;
		}
		$term =(count($semester_details_array) > 1)  ? $semester_details_array[1] : 0;
            }
    if($old_record->semester_name == "CANCEL"){
        continue;
    }
			// Build School Year 
            $start_year = ($old_record->semester_OID == "15" || $old_record->semester_OID == "17" ) ? 2014 : $semester_details_array[0];

			$end_year = $start_year + 1;

			$school_year = $start_year . "-" . $end_year;




        	// Build Array

        	$semester = new Semester;
        		$semester->scholarship_id =  $scholarship_id;
        		$semester->school_id =  $school_id;
                $semester->status = $semester_status;
        		$semester->school_year =  $school_year;
        		$semester->year_level =  $year_level;
        		$semester->term =  $term;
        		$semester->gwa =  $old_record->gwa;
        		$semester->created_at =  $old_record->scholar_record_created_at;
        		$semester->updated_at =  $old_record->scholar_record_updated_at;
        		$semester->created_by =  1;
        		$semester->modified_by =  1;
        		$semester->scholarship_type_id =  $scholarship_type_id;
        		$semester->course_id =  $old_record->course_OID;
        	   $semester->save();


            if($old_record->grades){
                $grades = explode("/", $old_record->grades);
                foreach ($grades as $grade) {
                    $g = new Grade;
                    $g->semester_id = $semester->id;
		   $g->created_by = 1;
		  $g->modified_by = 1;
                    $g->grade = $grade;
                    $g->save(); 
                }
            }

            if($scholarship->semesters()->where('school_year', '2014-2015')->first() && $scholarship->status == "inactive"){
                $scholarship->status = "active";
                $scholarship->save();
            }

 			// DB::table('semesters_scripts')
 			// 	->insert($new_scholar_record);

            // Get Allowance Details
                $amount = $old_record->amount;

            $allowance_release = AllowanceRelease::where('term', $term)->where('school_year', $school_year)->where('amount', $amount)->first();
            if($allowance_release){
            $allowance = new Allowance;
            $allowance->scholarship_id = $scholarship_id;
            $allowance->allowance_release_id = $allowance_release->id;
            $allowance->semester_id = $semester->id;
            $allowance->amount = $amount;
            $allowance->stage = "claimed";

            $allowance->created_at =  $old_record->scholar_record_created_at;
               $allowance->updated_at =  $old_record->scholar_record_updated_at;
               $allowance->created_by =  1;
               $allowance->modified_by =  1;
                $allowance->save();                
            }


            }
            catch(ErrorException $e){
                dd($e);

            }
        }


	}
}
