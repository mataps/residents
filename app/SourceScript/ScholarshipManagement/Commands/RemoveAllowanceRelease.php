<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\ScholarshipManagement\Repositories\AllowanceReleaseRepositoryInterface;
use AllowanceRelease;

class RemoveAllowanceRelease {

	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'remove_resource';

	/**
	 * @var AllowanceReleaseRepositoryInterface
	 */
	private $repository;


	function __construct(AllowanceReleaseRepositoryInterface $allowanceReleaseRepositoryInterface)
	{
		$this->repository = $allowanceReleaseRepositoryInterface;
	}


	/**
	 * @param  array            $inputs
	 * @param  AllowanceRelease $allowanceRelease
	 */
	public function handle(array $inputs, AllowanceRelease $allowanceRelease)
	{
		$this->repository->delete($allowanceRelease);

		return $allowanceRelease;
	}
}