<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\Common\Collections\ResultCollection;
use SourceScript\ScholarshipManagement\Repositories\AllowanceRepositoryInterface;
use User;

class RemoveAllowances {

	/**
	 * @var AllowanceRepositoryInterface
	 */
	private $repository;

	function __construct(AllowanceRepositoryInterface $allowanceRepositoryInterface)
	{
		$this->repository = $allowanceRepositoryInterface;
	}

	public function handle(array $inputs, ResultCollection $allowances, User $user)
	{
		$this->repository->deleteAllowances($allowances->modelKeys());
	}
}