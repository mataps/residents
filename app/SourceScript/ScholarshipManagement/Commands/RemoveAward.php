<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\ScholarshipManagement\Repositories\AwardRepositoryInterface;
use Award;

class RemoveAward {

	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'remove_resource';

	/**
	 * @var AwardRepositoryInterface
	 */
	private $awardRepository;

	function __construct(AwardRepositoryInterface $awardRepository)
	{
		$this->awardRepository = $awardRepository;
	}
	
	/**
	 * @param  array  $inputs
	 * @param  Award  $award
	 */
	public function handle(array $inputs, Award $award)
	{
		$this->awardRepository->delete($award);

		return $award;
	}

}