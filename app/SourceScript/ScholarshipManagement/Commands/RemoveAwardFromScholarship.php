<?php namespace SourceScript\ScholarshipManagement\Commands;

use Scholarship;
use Award;

class RemoveAwardFromScholarship {


	/**
	 * @param  array       $inputs
	 * @param  Scholarship $scholarship
	 * @param  Award       $award
	 * @return void
	 */
	function handle(array $inputs, Scholarship $scholarship, Award $award)
	{
		//TODO
		//YOLO
		$scholarship->awards()->detach($award->id);
	}
}