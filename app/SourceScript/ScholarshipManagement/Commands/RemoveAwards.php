<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\ScholarshipManagement\Repositories\AwardRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use SourceScript\Common\Collections\ResultCollection;

class RemoveAwards {


	/**
	 * @var AwardRepositoryInterface
	 */
	private $awardRepository;

	function __construct(AwardRepositoryInterface $awardRepository)
	{
		$this->awardRepository = $awardRepository;
	}


	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $awards
	 */
	function handle(array $inputs, ResultCollection $awards)
	{
		$this->awardRepository->deleteAwards($awards->modelKeys());
	}

}