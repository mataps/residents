<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\ScholarshipManagement\Repositories\CourseRepositoryInterface;
use SourceScript\Common\Collections\ResultCollection;

class RemoveCourses {

	/**
	 * @var CourseRepositoryInterface
	 */
	private $repository;


	public function __construct(CourseRepositoryInterface $courseRepositoryInterface)
	{
		$this->repository = $courseRepositoryInterface;
	}

	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $courses
	 */
	public function handle(array $inputs, ResultCollection $courses)
	{
		$this->repository->deleteCourses($courses->modelKeys());
	}
}