<?php namespace SourceScript\ScholarshipManagement\Commands;

use Grade;
use SourceScript\ScholarshipManagement\Repositories\GradeRepositoryInterface;

class RemoveGrade {
		
	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'remove_resource';

	/**
	 * @var GradeRepositoryInterface
	 */
	private $gradeRepository;


	function __construct(GradeRepositoryInterface $gradeRepositoryInterface)
	{
		$this->gradeRepository = $gradeRepositoryInterface;
	}



	/**
	 * @param  array   $inputs
	 * @param  Grade $grade
	 * @return Grade
	 */
	public function handle(array $inputs, Grade $grade)
	{
		$this->gradeRepository->delete($grade);

		return $grade;
	}
}