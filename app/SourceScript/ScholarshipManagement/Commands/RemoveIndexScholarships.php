<?php namespace SourceScript\ScholarshipManagement\Commands;

use Illuminate\Database\Eloquent\Collection;
use SourceScript\Common\Collections\ResultCollection;
use SourceScript\Profiling\Repositories\ResidentRepositoryInterface;
use Resident;
use Elasticsearch\Client;

class RemoveIndexScholarships {

    /**
     * @var ResidentRepositoryInterface
     */
    private $residentRepository;


    /**
     * @param array $inputs
     * @param Resident $resident
     * @return Resident
     */
    function handle(array $inputs, ResultCollection $scholarships)
    {
        $conn_param = \Config::get('elasticsearch');
        $client = new Client($conn_param);

        foreach ($scholarships->modelKeys() as $scholar)
        {
            $delete_param['index'] = "galactus_production";
            $delete_param['type'] = "scholarships";
            $delete_param['id'] = $scholar;

            $client->delete($delete_param);
        }
        return true;
    }
} 