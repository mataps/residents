<?php namespace SourceScript\ScholarshipManagement\Commands;

use AllowancePolicyOverride;
use SourceScript\ScholarshipManagement\Repositories\AllowancePolicyOverrideRepositoryInterface;

class RemovePolicyOverride {


	/**
	 * @var AllowancePolicyOverrideRepositoryInterface
	 */
	private $policyOverrideRepository;


	function __construct(AllowancePolicyOverrideRepositoryInterface $policyOverrideRepositoryInterface)
	{
		$this->policyOverrideRepository = $policyOverrideRepositoryInterface;
	}


	/**
	 * @param  array                   $input
	 * @param  AllowancePolicyOverride $allowancePolicyOverride
	 */
	public function handle(array $inputs, AllowancePolicyOverride $allowancePolicyOverride)
	{
		$this->policyOverrideRepository->delete($allowancePolicyOverride);
	}
}