<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\ScholarshipManagement\Repositories\ScholarshipRepositoryInterface;
use Scholarship;

class RemoveScholarship {


	/**
	 * @var ScholarshipRepositoryInterface
	 */
	private $scholarshipRepository;


	function __construct(ScholarshipRepositoryInterface $scholarshipRepository)
	{
		$this->scholarshipRepository = $scholarshipRepository;
	}


	/**
	 * @param  array       $inputs
	 * @param  Scholarship $scholarship
	 */
	function handle(array $inputs, Scholarship $scholarship)
	{
		$this->scholarshipRepository->delete($scholarship);
	}
}