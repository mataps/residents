<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\Common\Collections\ResultCollection;
use Scholarship;
use User;

class RemoveScholarshipAwards {

	/**
	 * @param  array            $inputs
	 * @param  Scholarship      $scholarship
	 * @param  ResultCollection $awards
	 * @param  User             $user
	 */
	public function handle(array $inputs, Scholarship $scholarship, ResultCollection $awards, User $user)
	{
		$scholarship->awards()->detach($awards->modelKeys());
	}
}