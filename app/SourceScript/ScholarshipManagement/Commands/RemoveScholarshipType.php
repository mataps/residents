<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\ScholarshipManagement\Repositories\ScholarshipTypeRepositoryInterface;
use ScholarshipType;

class RemoveScholarshipType {


	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'remove_resource';


	/**
	 * @var ScholarshipTypeRepositoryInterface
	 */
	private $scholarshipTypeRepository;


	function __construct(ScholarshipTypeRepositoryInterface $ScholarshipTypeRepositoryInterface)
	{
		$this->scholarshipTypeRepository = $scholarshipTyperepositoryInterface;		
	}

	/**
	 * @param  array           $inputs
	 * @param  ScholarshipType $scholarshipType
	 * @return void
	 */
	public function handle(array $inputs, ScholarshipType $scholarshipType)
	{
		$this->ScholarshipTypeRepository->delete($scholarshipType);

		return $scholarshipType;
	}
}