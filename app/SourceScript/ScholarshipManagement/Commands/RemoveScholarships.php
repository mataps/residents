<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\ScholarshipManagement\Repositories\ScholarshipRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use SourceScript\Common\Collections\ResultCollection;

class RemoveScholarships {


	/**
	 * @var ScholarshipRepositoryInterface
	 */
	private $scholarshipRepository;

	function __construct(ScholarshipRepositoryInterface $scholarshipRepository)
	{
		$this->scholarshipRepository = $scholarshipRepository;
	}


	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $scholarships
	 */
	function handle(array $inputs, ResultCollection $scholarships)
	{
		$this->scholarshipRepository->deleteScholarships($scholarships->modelKeys());
	}

}