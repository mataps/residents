<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\ScholarshipManagement\Repositories\SchoolRepositoryInterface;
use School;

class RemoveSchool {

	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'remove_resource';

	/**
	 * @var SchoolRepositoryInterface
	 */
	private $schoolRepository;


	function __construct(SchoolRepositoryInterface $schoolRepository)
	{
		$this->schoolRepository = $schoolRepository;
	}


	/**
	 * @param  array  $inputs
	 * @param  School $school
	 */
	function handle(array $inputs, School $school)
	{
		$this->schoolRepository->delete($school);

		return $school;
	}
}