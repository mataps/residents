<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\ScholarshipManagement\Repositories\SchoolRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use SourceScript\Common\Collections\ResultCollection;

class RemoveSchools {

	/**
	 * @var SchoolRepositoryInterface
	 */
	private $schoolRepository;

	function __construct(SchoolRepositoryInterface $schoolRepositoryInterface)
	{
		$this->schoolRepository = $schoolRepositoryInterface;
	}


	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $scholarships
	 */
	function handle(array $inputs, ResultCollection $schools)
	{
		$this->schoolRepository->deleteSchools($schools->modelKeys());
	}
}