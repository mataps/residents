<?php namespace SourceScript\ScholarshipManagement\Commands;

class RemoveSemester {


	/**
	 * @var SemesterRepositoryInterface
	 */
	private $repository;

	function __construct(SemesterRepositoryInterface $semesterRepositoryInterface)
	{
		$this->repository = $semesterRepositoryInterface;
	}

	/**
	 * @param  array    $inputs
	 * @param  Semester $semester
	 * @return Semester
	 */
	public function handle(array $inputs, Semester $semester)
	{
		$this->repository->delete($semester);

		return $semester;
	}
}