<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\ScholarshipManagement\Repositories\SemesterRepositoryInterface;
use SourceScript\Common\Collections\ResultCollection;

class RemoveSemesters {
		
	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'remove_resources';

	/**
	 * @var SemesterRepositoryInterface
	 */
	private $repository;

	function __construct(SemesterRepositoryInterface $semesterRepositoryInterface)
	{
		$this->repository = $semesterRepositoryInterface;
	}


	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $semesters
	 * @return ResultCollection
	 */
	public function handle(array $inputs, ResultCollection $semesters)
	{
		// dd($semesters);
		$this->repository->deleteSemesters($semesters->modelKeys());

		return $semesters;
	}
}