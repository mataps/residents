<?php namespace SourceScrip\ScholarshipManagement\Commands;

use SourceScript\ScholarshipManagement\Repositories\SubjectRepositoryInterface;
use Subject;

class RemoveSubject {
		
	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'remove_resource';

	/**
	 * @var SubjectRepositoryInterface
	 */
	private $repository;


	function __construct(SubjectRepositoryInterface $subjectRepositoryInterface)
	{
		$this->repository = $subjectRepositoryInterface;
	}	


	/**
	 * @param  array   $inputs
	 * @param  Subject $subject
	 * @return Subject
	 */
	public function handle(array $inputs, Subject $subject)
	{
		$this->repository->delete($subject);

		return $subject;
	}
}