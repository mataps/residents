<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\ScholarshipManagement\Repositories\SubjectRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use SourceScript\Common\Collections\ResultCollection;

class RemoveSubjects {

	/**
	 * @var SubjectRepositoryInterface
	 */
	private $subjectRepository;

	function __construct(SubjectRepositoryInterface $subjectRepositoryInterface)
	{
		$this->subjectRepository = $subjectRepositoryInterface;
	}


	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $scholarships
	 */
	function handle(array $inputs, ResultCollection $subjects)
	{
		$this->subjectRepository->deleteSubjects($subjects->modelKeys());
	}
}