<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\ScholarshipManagement\Repositories\ScholarshipRepositoryInterface;
use Scholarship;
use User;
use SourceScript\Common\Exceptions\HttpBadRequestException;
use SourceScript\Common\Exceptions\ValidationException;

class TransferScholarship {

	use ValidableTrait;

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'resident_id' 	=> 'required|integer',
		'school_id' 	=> 'integer',
		'status' 		=> 'required'
	];

	/**
	 * @var ScholarshipRepositoryInterface
	 */
	private $repository;

	function __construct(ScholarshipRepositoryInterface $scholarshipRepositoryInterface)
	{
		$this->repository = $scholarshipRepositoryInterface;
	}

	
	/**
	 * @param  array       $inputs
	 * @param  Scholarship $scholarship
	 * @param  User        $user
	 * @return Scholarship
	 */
	public function handle(array $inputs, Scholarship $scholarship, User $user)
	{
		if($scholarship->status == "closed") throw new ValidationException(['scholarship' => "Scholarship is already closed"]);		

		$newScholarship = new Scholarship;
		$newScholarship->fill($inputs);
		$newScholarship->approved_at = date("Y-m-d");
		$newScholarship->created_by = $user->id;
		$newScholarship->modified_by = $user->id;
		$newScholarship->comment = "Transfered from: " . $scholarship->resident->fullName();

		$this->repository->save($newScholarship);

		$scholarship->status = "closed";
		$scholarship->transfered_by = $user->id;
		$scholarship->transfered_to = $newScholarship->id;
		$this->repository->save($scholarship);

		return $newScholarship;
	}
}