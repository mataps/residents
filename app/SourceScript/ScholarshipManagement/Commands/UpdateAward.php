<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\ScholarshipManagement\Repositories\AwardRepositoryInterface;
use Award;
use User;
use SourceScript\Profiling\Factories\AttributionsFactory;

class UpdateAward {

	use ValidableTrait;

	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'update_resource';

	/**
	 * Validation Rules
	 * 
	 * @var array
	 */
	protected $rules = [
	];


	/**
	 * @var AwardRepository
	 */
	private $awardRepository;

	/**
	 * @var AttributionsFactory
	 */
	private $attributionsFactory;

	function __construct(AwardRepositoryInterface $awardRepository,
		AttributionsFactory $attributionsFactory)
	{
		$this->awardRepository = $awardRepository;
		$this->attributionsFactory = $attributionsFactory;
	}


    /**
     * @param array $inputs
     * @param Award $award
     * @param User $updater
     * @return Award
     */
    function handle(array $inputs, Award $award, User $updater)
    {
    	$award->name 		= $inputs['name'];
    	$award->description = $inputs['description'];

        $this->awardRepository->save($award);

        $attributions = $this->attributionsFactory->get($inputs['attributions'], $updater);

		$award->attributions()->sync($attributions->modelKeys());

        return $award;
    }
}