<?php namespace SourceScript\ScholarshipManagement\Commands;

use Course;
use User;
use SourceScript\ScholarshipManagement\Repositories\CourseRepositoryInterface;

class UpdateCourse {

	/**
	 * @var CourseRepositoryInterface
	 */
	private $repository;

	function __construct(CourseRepositoryInterface $courseRepositoryInterface)
	{
		$this->repository = $courseRepositoryInterface;
	}

	/**
	 * @param  array  $inputs
	 * @param  Course $course
	 * @param  User   $updater
	 * @return Course
	 */
	public function handle(array $inputs, Course $course, User $updater)
	{
		$course->fill($inputs);
		$course->modified_by = $updater->id;

		$this->repository->save($course);

		return $course;
	}
}