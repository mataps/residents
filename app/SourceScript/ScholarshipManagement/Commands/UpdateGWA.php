<?php namespace SourceScript\ScholarshipManagement\Commands;

use User;
use Semester;
use SourceScript\ScholarshipManagement\Repositories\SemesterRepositoryInterface;
use SourceScript\Common\Validations\ValidableTrait;

class UpdateGWA {

	use ValidableTrait;

	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'update_resource';


	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [		
		'gwa' 					=> 'required'		
	];

	/**
	 * @var SemesterRepositoryInterface
	 */
	private $semester;


	function __construct(SemesterRepositoryInterface $semesterRepository)
	{
		$this->semesterRepository = $semesterRepository;
	}


	/**
	 * @param  array    $inputs
	 * @param  Semester $semester
	 * @param  User     $updater
	 * @return Semester
	 */
	function handle(array $inputs, Semester $semester, User $updater)
	{
		
		$semester->gwa = $inputs['gwa'];
		$semester->modified_by = $updater->id;

		$this->semesterRepository->save($semester);		

		return $semester;
	}
}