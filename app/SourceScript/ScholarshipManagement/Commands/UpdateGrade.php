<?php namespace SourceScript\ScholarshipManagement\Commands;

use Semester;
use User;
use Grade;
use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\ScholarshipManagement\Repositories\GradeRepositoryInterface;

class UpdateGrade {

	use ValidableTrait;

	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'update_resource';

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'grade' 		=> 'required',
		'units' 		=> 'required',
		'subject_id' 	=> ''
	];

	/**
	 * @var GradeRepositoryInterface
	 */
	private $gradeRepository;


	function __construct(GradeRepositoryInterface $gradeRepositoryInterface)
	{
		$this->gradeRepository = $gradeRepositoryInterface;
	}


	/**
	 * @param  array  $inputs
	 * @param  Grade  $grade
	 * @param  User   $updater
	 * @return 
	 */
	public function handle(array $inputs, Semester $semester, Grade $grade, User $updater)
	{
		$grade->semester_id = $semester->id;
		$grade->fill($inputs);
		$grade->modified_by = $updater->id;

		$this->gradeRepository->save($grade);

		return $grade;
	}
}