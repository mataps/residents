<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\ScholarshipManagement\Repositories\ScholarshipRepositoryInterface;
use Scholarship;
use User;

class UpdateScholarship {

	use ValidableTrait;


	/**
	 * Validation Rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'guardian_id' => 'exists:residents,id',
		'father_id' => 'exists:residents,id',
		'mother_id' => 'exists:residents,id'
	];


	/**
	 * @var ScholarshipRepositoryInterface
	 */
	private $scholarshipRepository;


	function __construct(ScholarshipRepositoryInterface $scholarshipRepository)
	{
		$this->scholarshipRepository = $scholarshipRepository;
	}


	/**
	 * @param  array       $inputs
	 * @param  Scholarship $scholarship
	 * @param  User        $updater
	 * @return Scholarship
	 */
	function handle(array $inputs, Scholarship $scholarship, User $updater)
	{
		if(isset($inputs['status'])){
			$scholarship->status 		= $inputs['status'];
		}
		$scholarship->comment 		= $inputs['comment'];
		
		$scholarship->father_name 		= $inputs['father_name'];
		$scholarship->mother_name 		= $inputs['mother_name'];
		$scholarship->guardian_name 		= $inputs['guardian_name'];
		$scholarship->father_id 				= $inputs['father_id'];
		$scholarship->referrer_id 				= $inputs['referrer_id'];
		$scholarship->mother_id 				= $inputs['mother_id'];
		$scholarship->guardian_id 				= $inputs['guardian_id'];
		$scholarship->parents_contact_no 		= $inputs['parents_contact_no'];
		$scholarship->others_contact_no 		= $inputs['others_contact_no'];
		$scholarship->father_name 		= $inputs['father_name'];
		$scholarship->modified_by 	= $updater->id;

		$this->scholarshipRepository->save($scholarship);

		return $scholarship;
	}
}