<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\ScholarshipManagement\Repositories\ScholarshipTypeRepositoryInterface;
use ScholarshipType;
use User;

class UpdateScholarshipType {

	use ValidableTrait;

	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'update_resource';

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = array(
		'name' => 'required'
		);

	/**
	 * @var ScholarshipTypeRepository
	 */
	private $scholarshipTypeRepository;


	function __construct(ScholarshipTypeRepositoryInterface $scholarshipTypeRepositoryInterface)
	{
		$this->scholarshipTypeRepository = $scholarshipTypeRepositoryInterface;
	}


	/**
	 * @param  array           $inputs
	 * @param  ScholarshipType $scholarshipType
	 * @param  User            $updater
	 * @return ScholarshipType
	 */
	public function handle(array $inputs, ScholarshipType $scholarshipType, User $updater)
	{
		$scholarshipType->fill($inputs);

		$this->scholarshipTypeRepository->save($scholarshipType);

		return $scholarshipType;
	}

}