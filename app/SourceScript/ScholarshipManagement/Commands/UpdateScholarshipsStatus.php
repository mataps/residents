<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\ScholarshipManagement\Repositories\ScholarshipRepositoryInterface;
use SourceScript\Common\Collections\ResultCollection;
use Scholarship;
use User;
use SourceScript\Common\Validations\ValidableTrait;

class UpdateScholarshipsStatus {

	use ValidableTrait;

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'status' => 'required|in:waiting,for_verification,active,closed,denied,stop,graduated'
	];

	/**
	 * @var ScholarshipRepositoryInterface
	 */
	private $repository;


	function __construct(ScholarshipRepositoryInterface $scholarshipRepositoryInterface)
	{
		$this->repository = $scholarshipRepositoryInterface;
	}

	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $scholarships
	 * @param  User             $user
	 * @return ResultCollection
	 */
	public function handle(array $inputs, ResultCollection $scholarships, User $user)
	{
		foreach($scholarships as $scholarship)
		{
			$this->changeStatus($inputs, $scholarship, $user);
		}

		return $scholarships;
	}

	/**
	 * @param  Scholarship $scholarship
	 * @param  User        $user
	 * @return Scholarship
	 */
	private function changeStatus(array $inputs, Scholarship $scholarship, User $user)
	{
		$scholarship_status = $scholarship->status;

		switch ($inputs['status'])
		{
			case 'waiting':
				# code...
				break;

			case 'for_verification':
				# code...
				break;
				
			case 'active':
				$scholarship->approved_by = $user->id;
				$scholarship->approved_at = date("Y-m-d H:i:s");
				break;

			case 'closed':
				break;
			case 'denied':
				$scholarship->denied_by = $user->id;
				$scholarship->denied_at = date("Y-m-d H:i:s");
				break;
			case 'stop':
				# code...
				break;

			case 'graduated':
				# code...
			
				break;

			default:
				# code...
				break;
		}

		$scholarship->status 		= $inputs['status'];
		$scholarship->modified_by 	= $user->id;

		$this->repository->save($scholarship);

		return $scholarship;
	}
}