<?php namespace SourceScript\ScholarshipManagement\Commands;

use User;
use School;
use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\ScholarshipManagement\Repositories\SchoolRepositoryInterface;

class UpdateSchool {

	use ValidableTrait;

	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'update_resource';

	/**
	 * Validation Rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'name' 					=> 'required',
		'type' 					=> 'required|in:public,private',
		'level' 				=> 'required|in:secondary,collegiate',
		'address' 				=> 'required'
	];

	/**
	 * @var SchoolRepositoryInterface
	 */
	private $schoolRepository;


	function __construct(SchoolRepositoryInterface $schoolRepositoryInterface)
	{
		$this->schoolRepository = $schoolRepositoryInterface;
	}

	/**
	 * @param  array  $inputs
	 * @param  School $school
	 * @param  User   $updater
	 * @return School
	 */
	function handle(array $inputs, School $school, User $updater)
	{
		$school->fill($inputs);
		$school->modified_by = $updater->id;

		$this->schoolRepository->save($school);

		return $school;
	}
}