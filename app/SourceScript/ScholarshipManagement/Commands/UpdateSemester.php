<?php namespace SourceScript\ScholarshipManagement\Commands;

use User;
use Semester;
use Scholarship;
use SourceScript\ScholarshipManagement\Repositories\SemesterRepositoryInterface;
use SourceScript\Common\Validations\ValidableTrait;

class UpdateSemester {

	use ValidableTrait;

	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'update_resource';


	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'school_id' 			=> 'required',
		'school_year' 			=> 'required',
		'term' 					=> 'required',
		'gwa' 					=> 'required',
		'beneficiary_id' 		=> 'required|exists:residents,id',
		'client_id' 			=> 'exists:residents,id',
		'status'		 		=> 'in:active,waiting',
		'created_at' 			=> 'date',
		'school_level' 			=> 'required|in:secondary,collegiate'
		// 'scholarship_type_id' 	=> 'required|integer|exists:scholarship_types,id',
		// 'course_id' => 'exists:courses,id'
	];

	/**
	 * @var SemesterRepositoryInterface
	 */
	private $semester;


	function __construct(SemesterRepositoryInterface $semesterRepository)
	{
		$this->semesterRepository = $semesterRepository;
	}


	/**
	 * @param  array    $inputs
	 * @param  Semester $semester
	 * @param  User     $updater
	 * @return Semester
	 */
	function handle(array $inputs, Semester $semester, User $updater)
	{
		$old_created_at = $semester->created_at;
		$old_scholarship_type_id = $semester->scholarship_type_id;
		$old_client_id = $semester->client_id;
		if($inputs['client_id'] == "" || $inputs['client_id'] == null ){
			$inputs['client_id'] = $old_client_id;
		}
		$semester->fill(array_except($inputs, 'created_at'));
		 if(isset($inputs['created_at']) || $inputs['created_at'] != "")
        {
           	$semester->timestamps = false;
            $semester->setCreatedAt(date('Y-m-d H:i:s', strtotime($inputs['created_at'])));
            $semester->setUpdatedAt(date('Y-m-d H:i:s', strtotime($inputs['created_at'])));
        }

		 if(isset($inputs['date_encoded']) || $inputs['date_encoded'] != "")
        {
           $semester->date_encoded = date('Y-m-d H:i:s', strtotime($inputs['date_encoded']));
        }




		$semester->modified_by = $updater->id;

		$this->semesterRepository->save($semester);
		if($old_created_at != $semester->created_at || $old_scholarship_type_id != $semester->scholarship_type_id){
			$semester->generateUuid();
		}
		$this->updateAllowanceBasisId($semester->scholarship);

		return $semester;
	}

	function updateAllowanceBasisId(Scholarship $scholar)
	{

			$semesters = $scholar->semesters;
			if(count($semesters) > 1){
				$college = $semesters->filter(function($item){
					if($item->school_level == 'collegiate'){
						return true;
					}
				})->values();
				$secondary = $semesters->filter(function($item){
					if($item->school_level == 'secondary'){
						return true;
					}
				})->values();
				// var_dump($college);
				foreach ($college as $key => $semester) {
					if($key == 0) {
						$semester->allowance_basis_id = null;
					}
					else{
						// exit();
						$semester->allowance_basis_id = $college[$key-1]->id;
						
					}
					$semester->save();
				}
				foreach ($secondary as $key => $semester) {
					if($key == 0) {
						$semester->allowance_basis_id = null;
					}
					else{
						// exit();
						$semester->allowance_basis_id = $secondary[$key-1]->id;
						
					}
					$semester->save();
				}
			}
		}
}