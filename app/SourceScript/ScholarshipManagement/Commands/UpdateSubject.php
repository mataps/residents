<?php namespace SourceScript\ScholarshipManagement\Commands;

use User;
use Subject;
use SourceScript\ScholarshipManagement\Repositories\SubjectRepositoryInterface;
use SourceScript\Common\Validations\ValidableTrait;

class UpdateSubject {

	use ValidableTrait;

	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'update_resource';

	/**
	 * Validation Rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'name' => 'required'
	];


	/**
	 * @var SubjectRepositoryInterface
	 */
	private $subjectRepository;


	function __construct(SubjectRepositoryInterface $subjectRepositoryInterface)
	{
		$this->subjectRepository = $subjectRepositoryInterface;
	}


	/**
	 * @param  array   $inputs
	 * @param  Subject $subject
	 * @param  User    $updater
	 * @return Subject
	 */
	function handle(array $inputs, Subject $subject, User $updater)
	{
		$subject->name 			= $inputs['name'];
		$subject->description 	= $inputs['description'];
		$subject->modified_by 	= $updater->id;

		$this->subjectRepository->save($subject);

		return $subject;
	}
}