<?php namespace SourceScript\ScholarshipManagement\Factories;

use Scholarship;
use School;
use SourceScript\ScholarshipManagement\Repositories\SemesterRepositoryInterface;
use User;
use Semester;

class SemesterFactory {

	private $repository;

	function __construct(SemesterRepositoryInterface $semesterRepositoryInterface)
	{
		$this->repository = $semesterRepositoryInterface;
	}

	/**
	 * @param  Scholarship 	$scholarship
	 * @param  School      	$school
	 * @param  integer      $term
	 * @param  integer      $year
	 * @param  User 		$creator
	 * @return Semester
	 */
	function create(Scholarship $scholarship, $term, $year, User $creator)
	{
		$semester = Semester::where('scholarship_id', $scholarship->id)->where('term', $term)->where('school_year', $year)->first();

		if(!$semester)
		{
			$semester 				= new Semester;
			$semester->term 		= $term;
			$semester->school_year 	= $year;
			$semester->school_id 	= $scholarship->school->id;
			$semester->created_by 	= $creator->id;
			$semester->modified_by 	= $creator->id;

			$this->repository->save($semester);
		}

		return $semester;
	}
}