<?php namespace SourceScript\ScholarshipManagement\Commands;

use SourceScript\ScholarshipManagement\Repositories\ScholarshipRepositoryInterface;
use SourceScript\ScholarshipManagement\Repositories\GradeRepositoryInterface;
use Grade;

class RemoveGrade {

	/**
	 * @var ScholarshipRepositoryInterface
	 */
	private $repository;

	function __construct(GradeRepositoryInterface $gradeRepositoryInterface)
	{
		$this->repository = $gradeRepositoryInterface;
	}


	/**
	 * @param  array  $inputs
	 * @param  Grade  $grade
	 */
	public function handle(array $inputs, Grade $grade)
	{
		$this->repository->delete($grade);
	}
}