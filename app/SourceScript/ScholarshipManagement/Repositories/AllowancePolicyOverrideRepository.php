<?php namespace SourceScript\ScholarshipManagement\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use AllowancePolicyOverride;

class AllowancePolicyOverrideRepository implements AllowancePolicyOverrideRepositoryInterface {


	/**
	 * @var GenericFinderInterface
	 */
	private $finder;


	function __construct(GenericFinderInterface $finder)
	{
		$this->finder = $finder;
	}

	/**
	 * @param  integer $id
	 * @throws DBException
	 * @return AllowancePolicyOverride
	 */
	public function findOneById($id)
	{
		$query = AllowancePolicyOverride::where('id', $id);

		$result = $this->finder->findOne($query);

		if ($result)
			return $result;

		throw new DbException('Allowance Policy not found!');
	}

	/**
	 * @param  AllowancePolicyOverride $policy
	 * @throws DbException
	 * @return Award
	 */
	public function save(AllowancePolicyOverride $policy)
	{
		if ($policy->save())
			return $policy;

		throw new DbException('Allowance Policy not saved!');
	}

	/**
	 * @param  AllowancePolicyOverride $policy
	 * @throws DbException
	 */
	public function delete(AllowancePolicyOverride $policy)
	{
		if ( ! $policy->delete())
			throw new DbException('Allowance Policy not deleted!');
	}


	/**
	 * @param  $ids
	 * @return mixed
	 */
	public function findAllowancePolicyOverrides($ids)
	{
		$ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return AllowancePolicyOverride::whereIn('id', $ids)->get();
	}


	/**
	 * @param  array  $policies
	 * @throws DbException
	 */
	public function deleteAllowancePolicyOverrides(array $policies)
	{
		$deleted = AllowancePolicyOverride::destroy($policies);

		if(! $delete)
		{
			throw new DbException('Allowance policy not deleted!');
		}
	}
}