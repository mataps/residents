<?php namespace SourceScript\ScholarshipManagement\Repositories;

use AllowancePolicyOverride;

interface AllowancePolicyOverrideRepositoryInterface {

	/**
	 * @param  integer $id
	 * @return AllowancePolicyOverride
	 */
	public function findOneById($id);

	/**
	 * @param  AllowancePolicyOverride $policy
	 * @return AllowancePolicyOverride
	 */
	public function save(AllowancePolicyOverride $policy);

	/**
	 * @param  AllowancePolicyOverride $policy
	 * @return AllowancePolicyOverride
	 */
	public function delete(AllowancePolicyOverride $policy);
}