<?php namespace SourceScript\ScholarshipManagement\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use AllowancePolicy;
use ScholarshipType;

class AllowancePolicyRepository implements AllowancePolicyRepositoryInterface {


	/**
	 * @var GenericFinderInterface
	 */
	private $finder;


	function __construct(GenericFinderInterface $finder)
	{
		$this->finder = $finder;
	}

	/**
	 * @param  integer $id
	 * @throws DBException
	 * @return AllowancePolicy
	 */
	public function findOneById($id)
	{
		$query = AllowancePolicy::where('id', $id);

		$result = $this->finder->findOne($query);

		if ($result)
			return $result;

		throw new DbException('Allowance Policy not found!');
	}

	/**
	 * @param  AllowancePolicy $policy
	 * @throws DbException
	 * @return Award
	 */
	public function save(AllowancePolicy $policy)
	{
		if ($policy->save())
			return $policy;

		throw new DbException('Allowance Policy not saved!');
	}

	/**
	 * @param  AllowancePolicy $policy
	 * @throws DbException
	 */
	public function delete(AllowancePolicy $policy)
	{
		if ( ! $policy->delete())
			throw new DbException('Allowance Policy not deleted!');
	}


	/**
	 * @param  ScholarshipType $scholarshipType
	 * @return void
	 */
	public function deleteByScholarshipType(ScholarshipType $scholarshipType)
	{
		AllowancePolicy::where('scholarship_type_id', $scholarshipType->id)->delete();
	}
}