<?php namespace SourceScript\ScholarshipManagement\Repositories;

use AllowancePolicy;

interface AllowancePolicyRepositoryInterface {

	/**
	 * @param  integer $id
	 * @return AllowancePolicy
	 */
	public function findOneById($id);

	/**
	 * @param  AllowancePolicy $policy
	 * @return AllowancePolicy
	 */
	public function save(AllowancePolicy $policy);

	/**
	 * @param  AllowancePolicy $policy
	 * @return AllowancePolicy
	 */
	public function delete(AllowancePolicy $policy);
}