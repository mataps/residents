<?php namespace SourceScript\ScholarshipManagement\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use AllowanceRelease;

class AllowanceReleaseRepository implements AllowanceReleaseRepositoryInterface {

	/**
	 * @var GenericFinderInterface
	 */
	private $finder;


	function __construct(GenericFinderInterface $genericFinderInterface)
	{
		$this->finder = $genericFinderInterface;
	}


	/**
	 * @param  int $id
	 * @throws DbException If allowance release isnt found
	 * @return AllowanceRelease
	 */
	public function findOneById($id)
	{
		$query = AllowanceRelease::where('id', $id);

		$result = $this->finder->findOne($query);

		if($result)
			return $result;

		throw new DbException("Allowance release not found!");
		
	}


	/**
	 * @param  AllowanceRelease $allowanceRelease
	 * @throws DbException If allowance release isnt saved
	 * @return AllowanceRelease
	 */
	public function save(AllowanceRelease $allowanceRelease)
	{
		if($allowanceRelease->save())
			return $allowanceRelease;

		throw new DbException("Allowance Release not saved!");
	}

	/**
	 * @param  AllowanceRelease $allowanceRelease
	 * @throws DbException If allowances release isnt deleted
	 */
	public function delete(AllowanceRelease $allowanceRelease)
	{
		if($allowanceRelease->delete())
			throw new DbException("Allowance Release not deleted!");
			
	}
}