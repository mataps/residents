<?php namespace SourceScript\ScholarshipManagement\Repositories;

use AllowanceRelease;

interface AllowanceReleaseRepositoryInterface {

	/**
	 * @param  integer $id
	 * @throws DBException
	 * @return AllowancRelease
	 */
	public function findOneById($id);

	/**
	 * @param  AllowanceRelease $allowanceRelease
	 * @throws DbException
	 * @return AllowanceRelease
	 */
	public function save(AllowanceRelease $allowanceRelease);
	
	/**
	 * @param  AllowanceRelease $policy
	 * @return AllowanceRelease
	 */
	public function delete(AllowanceRelease $allowanceRelease);

}