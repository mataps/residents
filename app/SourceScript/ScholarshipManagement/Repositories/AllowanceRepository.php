<?php namespace SourceScript\ScholarshipManagement\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use Allowance;

class AllowanceRepository implements AllowanceRepositoryInterface {

	/**
	 * @var GenericFinderInterface
	 */
	private $finder;


	function __construct(GenericFinderInterface $finder)
	{
		$this->finder = $finder;
	}


	/**
	 * @param  integer $id
	 * @throws DBException
	 * @return Allowance
	 */
	public function findOneById($id)
	{
		$query = Allowance::where('id', $id);

		$result = $this->finder->findOne($query);

		if ($result)
			return $result;

		throw new DbException('School not found!');
	}


	/**
	 * @param  Allowance $school
	 * @throws DbException
	 * @return Scholarship
	 */
	public function save(Allowance $allowance)
	{
		if ($allowance->save())
			return $allowance;

		throw new DbException('Allowance not saved!');
	}


	/**
	 * @param  Allowance $allowance
	 * @throws DbException
	 */
	public function delete(Allowance $allowance)
	{
		if ( ! $allowance->delete())
			throw new DbException('Allowance not deleted!');
	}

	public function findAllowances($ids)
	{
		$ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return Allowance::whereIn('id', $ids)->get();
	}

	public function deleteAllowances(array $ids)
	{
		$deleted = Allowance::destroy($ids);

        if ( ! $deleted)
        {
            throw new DbException('Allowances not deleted!');
        }
	}


}