<?php namespace SourceScript\ScholarshipManagement\Repositories;

use Allowance;

interface AllowanceRepositoryInterface {

	/**
	 * @param  integer $id
	 * @return AllowancePolicy
	 */
	public function findOneById($id);

	/**
	 * @param  Allowance $allowance
	 * @return Allowance
	 */
	public function save(Allowance $allowance);

	/**
	 * @param  Allowance $policy
	 * @return Allowance
	 */
	public function delete(Allowance $allowance);
}