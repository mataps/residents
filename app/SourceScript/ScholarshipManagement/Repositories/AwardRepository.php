<?php namespace SourceScript\ScholarshipManagement\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use Award;

class AwardRepository implements AwardRepositoryInterface {

	/**
	 * @var GenericFinderInterface
	 */
	private $finder;


	function __construct(GenericFinderInterface $finder)
	{
		$this->finder = $finder;
	}

	/**
	 * @param  integer $id
	 * @throws DBException
	 * @return Award
	 */
	public function findOneById($id)
	{
		$query = Award::where('id', $id);

		$result = $this->finder->findOne($query);

		if ($result)
			return $result;

		throw new DbException('Award not found!');
	}

	/**
	 * @param  Award $award
	 * @throws DbException
	 * @return Award
	 */
	public function save(Award $award)
	{
		if ($award->save())
			return $award;

		throw new DbException('Award not saved!');
	}

	/**
	 * @param  Award $award
	 * @throws DbException
	 */
	public function delete(Award $award)
	{
		if ( ! $award->delete())
			throw new DbException('Award not deleted!');
	}

    /**
     * @param $ids
     * @return mixed
     */
    function findAwards($ids)
    {
        $ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return Award::whereIn('id', $ids)->get();
    }


    /**
     * @param  array  $awards
     * @throws DbException
     */
	public function deleteAwards(array $awards)
	{
		$deleted = Award::destroy($awards);

        if ( ! $deleted)
        {
            throw new DbException('Awards not deleted!');
        }
	}
}