<?php namespace SourceScript\ScholarshipManagement\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use Course;

class CourseRepository implements CourseRepositoryInterface {

	/**
	 * @var GenericFinderInterface
	 */
	private $genericFinderInterface;

	function __construct(GenericFinderInterface $genericFinderInterface)
	{
		$this->finder = $genericFinderInterface;
	}


	/**
	 * Saves a course
	 * 
	 * @param  Course $course
	 * @throws DbException
	 * @return Course
	 */
	public function save(Course $course)
	{
		if($course->save()) return $course;

		throw new DbException("Course not saved");
	}


	/**
	 * Find a course by id
	 * 
	 * @param  integer $id
	 * @throws DbException
	 * @return Course
	 */
	public function findOneById($id)
	{
		$query = Course::where('id', $id);

		$result = $this->finder->findOne($query);

		if($result) return $result;

		throw new DbException("Course not found");
		
	}


	/**
	 * Deletes a course
	 * 
	 * @param  Course $course
	 * @throws DbException
	 */
	public function delete(Course $course)
	{
		if(!$course->delete())
			throw new DbException("Course not deleted");
	}


	/**
	 * Finds courses based on ids
	 * @param  string $ids
	 * @return ResultCollection
	 */
	public function findCourses($ids)
	{
		$ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return Course::whereIn('id', $ids)->get();
	}


	/**
	 * Delete courses based on ids
	 * @param  array $ids
	 * @throws DbException
	 */
	public function deleteCourses(array $ids)
	{
		$deleted = Course::destroy($ids);

        if ( ! $deleted)
        {
            throw new DbException('Courses not deleted!');
        }
	}
}