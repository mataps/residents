<?php namespace SourceScript\ScholarshipManagement\Repositories;

use Course;

interface CourseRepositoryInterface {


	/**
	 * Saves a course
	 * 
	 * @param  Course $course
	 * @throws DbException
	 * @return Course
	 */
	public function save(Course $course);


	/**
	 * Find a course by id
	 * 
	 * @param  integer $id
	 * @throws DbException
	 * @return Course
	 */
	public function findOneById($id);


	/**
	 * Deletes a course
	 * 
	 * @param  Course $course
	 * @throws DbException
	 */
	public function delete(Course $course);


	/**
	 * Finds courses based on ids
	 * @param  string $ids
	 * @return ResultCollection
	 */
	public function findCourses($ids);


	/**
	 * Delete courses based on ids
	 * @param  array $ids
	 * @throws DbException
	 */
	public function deleteCourses(array $ids);
}