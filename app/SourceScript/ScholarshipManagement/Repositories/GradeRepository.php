<?php namespace SourceScript\ScholarshipManagement\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use Grade;

class GradeRepository implements GradeRepositoryInterface {


	/**
	 * @var GenericFinderInterface
	 */
	private $finder;

	function __construct(GenericFinderInterface $genericFinderInterface)
	{
		$this->finder = $genericFinderInterface;
	}


	/**
	 * @param  integer $id
	 * @throws DBException
	 * @return Grade
	 */
	public function findOneById($id)
	{
		$query = Grade::where('id', $id);

		$result = $this->finder->findOne($query);

		if ($result)
			return $result;

		throw new DbException('Allowance Policy not found!');
	}


	/**
	 * @param  Grade $grade
	 * @throws DbException
	 * @return Award
	 */
	public function save(Grade $grade)
	{
		if ($grade->save())
			return $grade;

		throw new DbException('Allowance Policy not saved!');
	}


	/**
	 * @param  Grade $grade
	 * @throws DbException
	 */
	public function delete(Grade $grade)
	{
		if ( ! $grade->delete())
			throw new DbException('Allowance Policy not deleted!');
	}


}