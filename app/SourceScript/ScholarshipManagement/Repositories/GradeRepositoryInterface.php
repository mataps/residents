<?php namespace SourceScript\ScholarshipManagement\Repositories;

use Grade;

interface GradeRepositoryInterface {

	/**
	 * @param  integer $id
	 * @throws DBException
	 * @return Grade
	 */
	public function findOneById($id);

	/**
	 * @param  Grade $grade
	 * @throws DbException
	 * @return Award
	 */
	public function save(Grade $grade);

	/**
	 * @param  Grade $grade
	 * @throws DbException
	 */
	public function delete(Grade $grade);
}
