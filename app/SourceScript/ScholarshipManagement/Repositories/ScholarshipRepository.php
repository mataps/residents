<?php namespace SourceScript\ScholarshipManagement\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use Scholarship;

class ScholarshipRepository implements ScholarshipRepositoryInterface {

    function __construct(GenericFinderInterface $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param $id
     * @throws DbException
     * @return Scholarship
     */
    function findOneById($id)
    {
        $query = Scholarship::where('id', $id);

        $result = $this->finder->findOne($query);

        if ($result)
        {
            return $result;
        }

        throw new DbException('Scholarship not found!');
    }

    /**
     * @param Scholarship $scholarship
     * @return Scholarship
     * @throws DbException
     */
    function save(Scholarship $scholarship)
    {
        if ($scholarship->save())
        {
            return $scholarship;
        }

        throw new DbException('Scholarship not saved!');
    }

    /**
     * @param Scholarship $scholarship
     * @throws DbException
     */
    function delete(Scholarship $scholarship)
    {
        if (! $scholarship->delete())
        {
            throw new DbException('Scholarship not deleted!');
        }
    }

    public function findScholarships($ids)
    {
        $ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return Scholarship::whereIn('id', $ids)->get();
    }

    function deleteScholarships(array $scholarships)
    {
        $deleted = Scholarship::destroy($scholarships);

        if ( ! $deleted)
        {
            throw new DbException('Scholarhips not deleted!');
        }
    }
}