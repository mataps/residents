<?php namespace SourceScript\ScholarshipManagement\Repositories;


use Scholarship;

interface ScholarshipRepositoryInterface {

    /**
     * @param $id
     * @return Scholarship
     */
    function findOneById($id);

    /**
     * @param Scholarship $scholarship
     * @return Scholarship
     */
    function save(Scholarship $scholarship);

    /**
     * @param Scholarship $scholarship
     * @return void
     */
    function delete(Scholarship $scholarship);
} 