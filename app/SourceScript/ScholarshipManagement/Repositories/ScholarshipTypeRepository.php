<?php namespace SourceScript\ScholarshipManagement\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use ScholarshipType;

class ScholarshipTypeRepository implements ScholarshipTypeRepositoryInterface {

	/**
	 * @var GenericFinderInterface
	 */
	private $finder;


	function __construct(GenericFinderInterface $finder)
	{
		$this->finder = $finder;
	}

	/**
	 * @param  integer $id
	 * @throws DBException
	 * @return ScholarshipType
	 */
	public function findOneById($id)
	{
		$query = ScholarshipType::where('id', $id);

		$result = $this->finder->findOne($query);

		if ($result)
			return $result;

		throw new DbException('Scholarship type not found!');
	}

	/**
	 * @param  ScholarshipType $scholarshipType
	 * @throws DbException
	 * @return ScholarshipType
	 */
	public function save(ScholarshipType $scholarshipType)
	{
		if ($scholarshipType->save())
			return $scholarshipType;

		throw new DbException('Scholarship type not saved!');
	}

	/**
	 * @param  ScholarshipType $scholarshipType
	 * @throws DbException
	 */
	public function delete(ScholarshipType $scholarshipType)
	{
		if ( ! $scholarshipType->delete())
			throw new DbException('Scholarship type not deleted!');
	}


	/**
	 * @param  $ids
	 * @return ResultCollection
	 */
	public function findScholarshipTypes($ids)
	{
		$ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return ScholarshipType::whereIn('id', $ids)->get();
	}

}