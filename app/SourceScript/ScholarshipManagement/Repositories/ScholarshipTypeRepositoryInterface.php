<?php namespace SourceScript\ScholarshipManagement\Repositories;

use ScholarshipType;

interface ScholarshipTypeRepositoryInterface {

	/**
     * @param $id
     * @return ScholarshipType
     */
    function findOneById($id);

    /**
     * @param ScholarshipType $scholarshipType
     * @return Scholarship
     */
    function save(ScholarshipType $scholarshipType);

    /**
     * @param ScholarshipType $scholarshipType
     * @return void
     */
    function delete(ScholarshipType $scholarshipType);	
}