<?php namespace SourceScript\ScholarshipManagement\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use School;

class SchoolRepository implements SchoolRepositoryInterface {

	/**
	 * @var GenericFinderInterface
	 */
	private $finder;


	function __construct(GenericFinderInterface $finder)
	{
		$this->finder = $finder;
	}

	/**
	 * @param  integer $id
	 * @throws DBException
	 * @return School
	 */
	public function findOneById($id)
	{
		$query = School::where('id', $id);

		$result = $this->finder->findOne($query);

		if ($result)
			return $result;

		throw new DbException('School not found!');
	}

	/**
	 * @param  School $school
	 * @throws DbException
	 * @return Scholarship
	 */
	public function save(School $school)
	{
		if ($school->save())
			return $school;

		throw new DbException('School not saved!');
	}

	/**
	 * @param  School $school
	 * @throws DbException
	 */
	public function delete(School $school)
	{
		if ( ! $school->delete())
			throw new DbException('School not deleted!');
	}

	public function findSchools($ids)
	{
		$ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return School::whereIn('id', $ids)->get();
	}

	public function deleteSchools(array $schools)
	{
		$deleted = School::destroy($schools);

        if ( ! $deleted)
        {
            throw new DbException('Schools not deleted!');
        }
	}

}