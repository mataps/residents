<?php namespace SourceScript\ScholarshipManagement\Repositories;

use School;

interface SchoolRepositoryInterface {

	/**
	 * @param  integer $id
	 * @return School
	 */
	public function findOneById($id);

	/**
	 * @param  School $school [description]
	 * @return School
	 */
	public function save(School $school);

	/**
	 * @param  School $school
	 * @return School
	 */
	public function delete(School $school);
}