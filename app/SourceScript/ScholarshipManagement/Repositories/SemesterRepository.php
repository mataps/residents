<?php namespace SourceScript\ScholarshipManagement\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use Semester;

class SemesterRepository implements SemesterRepositoryInterface {

    /**
     * GenericFinderInterface
     * 
     * @var [type]
     */
    private $finder;


	function __construct(GenericFinderInterface $finder)
    {
        $this->finder = $finder;
    }


    /**
     * @param $id
     * @throws DbException
     * @return Semester
     */
    function findOneById($id)
    {
        $query = Semester::where('id', $id);

        $result = $this->finder->findOne($query);

        if ($result)
        {
            return $result;
        }

        throw new DbException('Semester not found!');
    }


    /**
     * @param 	Semester $semester
     * @return Semester
     * @throws DbException
     */
    function save(Semester $semester)
    {
        if ($semester->save())
        {
            return $semester;
        }

        throw new DbException('Semester not saved!');
    }


    /**
     * @param Semester $semester
     * @throws DbException
     */
    function delete(Semester $semester)
    {
        if ( ! $semester->permissions()->detach() && ! $semester->delete())
        {
            throw new DbException('Semester not deleted!');
        }
    }


    /**
     * @param  string $ids
     * @return Collection
     */
    function findSemesters($ids)
    {
        $ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return Semester::whereIn('id', $ids)->get();
    }

    /**
     * @param  array  $semesters
     * @throws DbException
     */
    function deleteSemesters(array $semesters)
    {

        $semesters = Semester::destroy($semesters);

   

        if(!$semesters)
        {
            throw new DbException("Semesters not deleted");
        }
    }
}