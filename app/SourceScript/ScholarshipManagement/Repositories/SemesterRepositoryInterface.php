<?php namespace SourceScript\ScholarshipManagement\Repositories;

use Semester;

interface SemesterRepositoryInterface {

	/**
	 * @param  integer $id
	 * @return Semester
	 */
	public function findOneById($id);


	/**
	 * @param  Semester $semester
	 * @return Semester
	 */
	public function save(Semester $semester);


	/**
	 * @param  Semester $semester
	 * @return Semester
	 */
	public function delete(Semester $semester);
}