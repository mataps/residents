<?php namespace SourceScript\ScholarshipManagement\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use Subject;

class SubjectRepository implements SubjectRepositoryInterface {

	/**
	 * @var GenericFinderInterface
	 */
	private $finder;


	function __construct(GenericFinderInterface $finder)
	{
		$this->finder = $finder;
	}


	/**
	 * @param  integer $id
	 * @throws DBException
	 * @return Subject
	 */
	public function findOneById($id)
	{
		$query = Subject::where('id', $id);

		$result = $this->finder->findOne($query);

		if($result)
			return $result;

		throw new DbException('Subject not found!');
	}


	/**
	 * @param  Subject $subject
	 * @return Subject
	 */
	public function save(Subject $subject)
	{
		if($subject->save())
			return $subject;

		throw new DbException('Subject not saved!');
	}


	/**
	 * @param  Subject $subject
	 * @return Subject
	 */
	public function delete(Subject $subject)
	{
		if( ! $school->subject())
			throw new DbException('Subject not deleted!');
	}

    /**
     * @param $ids
     * @return mixed
     */
    function findSubjects($ids)
    {
        $ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return Subject::whereIn('id', $ids)->get();
    }

	public function deleteSubjects(array $subjects)
	{
		$deleted = Subject::destroy($subjects);

        if ( ! $deleted)
        {
            throw new DbException('Subjects not deleted!');
        }
	}
}