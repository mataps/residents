<?php namespace SourceScript\ScholarshipManagement\Repositories;

use Subject;

interface SubjectRepositoryInterface {

	/**
	 * @param  integer $id
	 * @return Subject
	 */
	public function findOneById($id);

	/**
	 * @param  Subject $subject
	 * @return Subject
	 */
	public function save(Subject $subject);

	/**
	 * @param  Subject $subject
	 * @return Subject
	 */
	public function delete(Subject $subject);
}
