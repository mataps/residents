<?php namespace SourceScript\ScholarshipManagement;


use SourceScript\Common\AbstractProvider;

class ScholarshipManagementProvider extends AbstractProvider
{
    protected $modules = array(
        'Scholarship' => array(
            'repository' => 'ScholarshipRepository',
            'bindings' => array(
                'scholarships' => 'findOneById',
                'Scholarships' => 'findScholarships'
            )
        ),
        'Course' => array(
            'repository' => 'CourseRepository',
            'bindings' => array(
                'courses' => 'findOneById',
                'Courses' => 'findCourses'
                )
            ),
        'School' => array(
            'repository' => 'SchoolRepository',
            'bindings' => array(
                'schools' => 'findOneById',
                'Schools' => 'findSchools'
                )
            ),
        'Award' => array(
            'repository' => 'AwardRepository',
            'bindings' => array(
                'awards' => 'findOneById',
                'Awards' => 'findAwards'
                )
            ),
        'Subject' => array(
            'repository' => 'SubjectRepository',
            'bindings' => array(
                'subjects' => 'findOneById',
                'Subjects' => 'findSubjects'
                )
            ),
        'AllowancePolicy' => array(
            'repository' => 'AllowancePolicyRepository',
            'bindings' => array(
                'policies' => 'findOneById'
                )
            ),
        'Semester' => array(
            'repository' => 'SemesterRepository',
            'bindings' => array(
                'semesters' => 'findOneById',
                'Semesters' => 'findSemesters',
                'grades' => 'findOneById'
                )
            ),
        'Allowance' => array(
            'repository' => 'AllowanceRepository',
            'bindings' => array(
                'allowances' => 'findOneById',
                'Allowances' => 'findAllowances'
                )
            ),
        'Grade' => array(
            'repository' => 'GradeRepository',
            'bindings' => array(
                'grades' => 'findOneById'
                )
            ),
        'ScholarshipType' => array(
            'repository' => 'ScholarshipTypeRepository',
            'bindings' => array(
                'scholarshiptypes' => 'findOneById'
                )
            ),
        'AllowanceRelease' => array(
            'repository' => 'AllowanceReleaseRepository',
            'bindings' => array(
                'releases' => 'findOneById'
                )
            ),
        'AllowancePolicyOverride' => array(
            'repository' => 'AllowancePolicyOverrideRepository',
            'bindings' => array(
                'policyOverrides' => 'findOneById',
                'PolicyOverrides' => 'findAllowancePolicyOverrides'
                )
            )
        );

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }
}