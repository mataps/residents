<?php namespace SourceScript\ScholarshipManagement;

use Illuminate\Support\Collection;
use SourceScript\Common\AbstractService;
use SourceScript\Common\Paginators\FilterableTrait;
use SourceScript\Common\Paginators\PaginatableTrait;
use SourceScript\Common\Paginators\SortableTrait;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Common\Repositories\GenericFinderInterface;
use Course;
use Scholarship;
use Barangay;
use School;
use Award;
use Subject;
use AllowancePolicy;
use AllowancePolicyOverride;
use Allowance;
use Semester;
use ScholarshipType;
use AllowanceRelease;
use Illuminate\Support\Facades\Db;
use Elasticsearch\Client;
use Elasticsearch\Common\Exceptions\Curl\CouldNotConnectToHost;
use Elasticsearch\Common\Exceptions\BadRequest400Exception;

class ScholarshipManagementService extends AbstractService implements ScholarshipManagementInterface {

    use PaginatableTrait, SortableTrait, FilterableTrait;


    /**
     * @var GenericFinderInterface
     */
    private $repository;


    function __construct(GenericFinderInterface $genericRepository)
    {
        $this->repository = $genericRepository;
    }


    /**
     * Finds beneficiarylist data.
     * 
     * @param  array  $inputs
     * @return Collection
     */
    function findBeneficiaryList(array $inputs)
    {
        extract($inputs);

        $query = Scholarship::query();


        $query->with(['semesters' => function($subQuery) use ($school_year, $term)
        {
            if(isset($school_year) && isset($term))
                $subQuery->where('semesters.school_year', $school_year)->where('semesters.term', $term);
        }]);


        $result = $this->repository->query($query);


        return $result;
    }


    /**
     * Finds leaderlist data.
     * 
     * @param  array $inputs
     * @return Collection
     */
    function findLeaderList(array $inputs)
    {
        extract($inputs);

        $query = Scholarship::query();

        $query->join('semesters', function($join) use ($term, $school_year)
        {
            $join->on('scholarships.id', '=', 'semesters.scholarship_id');
            if(isset($term) && isset($school_year))
                $join->where('term', '=', $term)->where('school_year', '=', $school_year);
        });

        $query->orderBy('semesters.gwa', 'desc');

        // $query->with(['semesters' => function($subQuery) use ($term, $school_year)
        // {
        //     $subQuery->where('term', $term)->where('school_year', $school_year);
        // }]);


        $result = $this->repository->query($query);

        return $result;
    }

    /**
     * Finds MasterList data
     *
     * @param array $inputs
     * @return collection
     */

    function findMasterlistScholarships(array $inputs) 
    {
        extract($inputs);

        $query = Scholarship::query();

        return $query;
    }


    /**
     * Finds an allowance override by semester
     * 
     * @param  Semester $semester
     * @return AllowancePolicy
     */
    function findAllowancePolicyOverrideBySemester(Semester $semester)
    {
        $query = AllowancePolicyOverride::query();

        $semestersQuery = Semester::query()->where('scholarship_id', $semester->allowance_basis)->orderBy('created_at');

        $semesters = $this->repository->query($semestersQuery);

        $result = $this->repository->findOne($query);

        return $result;
    }

    /**
     * Find allowance by allowanceRelease.
     * 
     * @param  AllowanceRelease $allowanceRelease
     * @return Allowance
     */
    function findAllowanceByAllowanceRelease(AllowanceRelease $allowanceRelease)
    {
        $query = Allowance::query();

        $query->where('allowance_release_id', $allowanceRelease->id);

        $result = $this->repository->findOne($query);

        return $result;
    }

    public function findAllowancesByAttributes(array $attributes)
    {
        $query = Allowance::query();

        extract($attributes);

        if(isset($allowance_release_id))
            $query->where('allowance_release_id', $allowance_release_id);

        if(isset($semester_id))
            $query->where('semester_id', $semester_id);

        $query->whereNull('deleted_at');

        $result = $this->repository->findOne($query);

        return $result;
    }


    /**
     * Finds allowance policy by semester
     * 
     * @param  Semester $semester
     * @return AllowancePolicy
     */
    function findAllowancePolicyBySemester(Semester $semester)
    {
        $query = AllowancePolicy::query();

        $query
            ->where('scholarship_type_id', $semester->scholarship_type_id)
            ->where('school_type', $semester->school->type)
            ->where('school_level', $semester->school_level)
            ->where('from', '<=', $semester->gwa)
            ->where('to', '>=', $semester->gwa)
            ->orderBy('to', 'desc');

        $result = $this->repository->findOne($query);

        return $result;
    }


    /**
     * @param  Semester                  $semester
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $fieldsParameters
     * @return Collection
     */
    public function findReleasesBySemester(
        Semester $semester,
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;

        $query = AllowanceRelease::with(array('allowances' => function($subQuery) use ($semester)
        {
            $subQuery->where('semester_id', $semester->id);
        }));

        $query->where(function($subQuery)
        {
            $subQuery->whereNull('expiring_at')
                ->orWhere('expiring_at', '>', date('Y-m-d H:i:s'));
        });

        $query->whereHas('scholarshipTypes', function($subQuery) use ($semester)
        {
            $subQuery->whereIn('scholarship_types.id', [$semester->scholarship_type_id]);
        });

        $query->where('school_year', $semester->school_year)->where('term', $semester->term);
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  Scholarship               $scholarship
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return Collection
     */
    public function findReleasesByScholarship(
        Scholarship $scholarship,
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;

        $query = AllowanceRelease::query();

        //TODO: by scholarship type
        $query->whereIn('school_year', $scholarship->semesters->lists('school_year'))->whereIn('term', $scholarship->semesters->lists('term'));
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;        
    }


    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    public function findAllowanceReleases(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null
        )
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;

        $query = AllowanceRelease::query();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  AllowanceRelease     $allowanceRelease
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    public function findAllowancesByAllowanceRelease(
        AllowanceRelease $allowanceRelease,
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;

        $query = $allowanceRelease->allowances()->getQuery();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;        
    }


    /**
     * @param  Scholarship          $scholarship
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    public function findAllowancesByScholarship(
        Scholarship $scholarship,
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $scholarship->allowances()->getQuery();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  Semester                  $semester
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return Collection
     */
    public function findAllowancesBySemester(
        Semester $semester,
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null
        )
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $semester->allowances()->getQuery();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;   
    }

    /**
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return ResultCollection
     */
    function findCourses(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $query = Course::query();


        if($filterParameters)
            $this->buildFilters($query, $filterParameters);

        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;

    }

    /**
     * @param  AllowanceRelease          $allowanceRelease
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return ResultCollection
     */
    public function findSemestersByAllowanceReleaseAndBarangay(
        AllowanceRelease $allowanceRelease,
        Barangay $barangay,
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null,
        $created_at = null
        )
    {
        $query = Semester::whereIn('scholarship_type_id', $allowanceRelease->scholarshipTypes->lists('id'));

        $query->whereNotNull('gwa');

        if($created_at != null && $created_at != ""){
             $query->whereBetween('semesters.date_encoded', $created_at);
        }

        if($allowanceRelease->school_year) $query->where('school_year', $allowanceRelease->school_year);
        if($allowanceRelease->term) $query->where('term',  $allowanceRelease->term);
        if($allowanceRelease->year_level) $query->where('year_level', $allowanceRelease->year_level);

        $query->with(['allowances' => function($subQuery) use ($allowanceRelease)
        {
            $subQuery->where('allowance_release_id', $allowanceRelease->id);
        }]);

  
        $query->whereHas('scholarship.resident.barangay', function($subQuery) use ($barangay)
        {
            $subQuery->where('id', $barangay->id);
        });

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);

        // $countRows = $this->repository->count($query);

        // if($paginationParameters)
        //     $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);


        // $result = $this->repository->query($query);

        // $result->setCountRows($countRows);


        $query->orderBy('id', 'desc');

        $result = $query->get();

 


        return $result;
    }

    /**
     * @param  AllowanceRelease          $allowanceRelease
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return ResultCollection
     */
    public function findSemestersByAllowanceRelease(
        AllowanceRelease $allowanceRelease,
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null
        )
    {
        $query = Semester::query();

        $query->whereIn('scholarship_type_id', $allowanceRelease->scholarshipTypes->lists('id'));

        $query->whereNotNull('gwa');

        if($allowanceRelease->school_year) $query->where('school_year', $allowanceRelease->school_year);
        if($allowanceRelease->term) $query->where('term',  $allowanceRelease->term);
        if($allowanceRelease->year_level) $query->where('year_level', $allowanceRelease->year_level);

        $query->with(['allowances' => function($subQuery) use ($allowanceRelease)
        {
            $subQuery->where('allowance_release_id', $allowanceRelease->id);
        }]);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);

        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }

    /**
     * @param  Scholarship          $scholarship
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findSemestersByScholarship(
        Scholarship $scholarship,
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $scholarship->semesters()->getQuery();
        
        if($filterParameters)
            $this->buildFilters($query, $filterParameters);

        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }    


    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findSemesters(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = Semester::query();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    function findSemestersES(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null,
        $permissions = null
        )
    {
         $filters = $filterParameters->getFilters();
         $param = \Config::get('elasticsearch');
         $client = new Client($param);
          $search_filters = [];
          $search_or_filters = [];

           foreach ($filters as $key => $value) {
            if($key == "sort") {
                continue;
            }
            

             if($value != 'null'){
                $value = str_replace("-", " ", $value);

                  if (strpos($value,'||') !== false) {
                    $text = explode("||", $value);
                    foreach ($text as $search_item) {
                        if($search_item != ""){
                         $search_or_filters[] = array('match_phrase_prefix' =>  array($key  =>  $search_item) );
                        }
                    }
                    $search_filters[] = array('bool' => array(
                            'should'  => $search_or_filters
                        ));
                 }
                else if($key == 'term' || $key == "year_level"){
                    $search_filters[] = array(
                                    "term" =>  array($key  =>  $value)
                        );
                }

                else{
                    $value = strtolower($value);
                    $search_wildcard_filters = array();
                    $search_wc_filters = array();
                    
                    if($value != "null"){
                    $search_item = explode(" ", $value);

                    foreach ($search_item as $item) {
                        $search_wc_filters[] = array('wildcard' => array($key => $item . "*"));
                    }

                    $search_wildcard_filters[]['bool']['must'] = $search_wc_filters;

                    $search_wildcard_filters[] = array(
                                'prefix' => array($key => [ 'value' =>  $value, 'boost' => 2])
                            );
                    $search_wildcard_filters[] = ['term' => [$key => [ 'value' => $value,  'boost' => 3]]];

                    if($value != 'null'){
               
                    $search_filters[]['bool'] = array( 
                                'should' => $search_wildcard_filters
                                );

                    } 
                    }
  
                }


                }
              


        }

        // dd($search_filters);
        $sortParameters = $sortParameters->getSort()[0];
        $sort_key = substr($sortParameters, 1); 

   

        if(!is_null($sort_key) && $sort_key != "id"){
            $starts_with  = substr($sortParameters, 0, 1);
            if($starts_with == "-"){
                    $search_param['body']['sort'] = array(
                        array($sort_key => 'desc')
                    ); 
             }
             else{
                $search_param['body']['sort'] = array(
                        array($sortParameters => 'asc')
                ); 
             }
        }

      if(count($search_filters) + count($search_or_filters) > 0){
     $search_param['body']['sort'] = array(
                array('school_year' => 'desc')
        );  
        }

             $search_param['body']['query']['bool']= array(
                
                // 'should' => $search_or_filters,
                'must'=> $search_filters,
            );

       $search_param['size'] = $paginationParameters->getLimit();
        $search_param['from'] = $paginationParameters->getFrom();
        $search_param['index'] = 'galactus_production';
        $search_param['type']  = 'semesters';
       
       // dd($search_param);
       
        $results = $client->search($search_param);
        // dd($results);
        return $results;      

    }


    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findScholarshipTypes(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = ScholarshipType::query();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findScholarships(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = Scholarship::query();

        $query->with('resident', 'resident.district', 'resident.barangay', 'resident.cityMunicipality', 'school');
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }

  public function findScholarshipsES($fieldsParameters = null, PaginationParameters $paginationParameters = null,  $sortParameters = null, FilterParameters $filterParameters = null)
    {
          ### Elastic Search Part #######
        $filters = $filterParameters->getFilters();
        // dd($filters);
        // array_walk($filters, function(&$value, $key) { $value = strtolower($value); });
        $param = \Config::get('elasticsearch');
        $client = new Client($param);

        $search_filters = [];
        // dd($filters);
        foreach ($filters as $key => $value) {
             if($key == "sort") {
                continue;
            }
            if(is_array($value)){
                if(isset($value['from'])){
                    // dd($value['from']);
                $search_filters[] = array(
                        'range' => array( $key => array(
                                'gte' => date('Y-m-d', strtotime($value['from'])),
                                'lte' => date('Y-m-d', strtotime($value['to']))
                            ))
                    );
                    
                }
                if($key == 'attributions'){
                    foreach ($value as $attribute) {
                        $search_filters[] = array(
                            'match_phrase_prefix' =>  array('attributions'  =>  $attribute['text'])
                            );
                    }
                }
                         else if($key == 'full_name'){
                    $full_name = explode(" ", $value);
                    foreach ($full_name as $name) {
                        $search_filters[] = array(
                                'match_phrase_prefix' => array('full_name' =>  $name)
                            );
                        // $search_filters[] = array(
                        //         'match_phrase_prefix' => array('middle_name' => $name)
                        //     ); 
                        // $search_filters[] = array(
                        //         'match_phrase_prefix' => array('last_name' => $name)
                        //     );                                                   
                    }
                }
            else if($key == 'affiliations'){
                // dd('mark');
            
                $search_filters[] = array(
                                'match_phrase_prefix' =>  array($key  =>  $value)
                    );
            }
            else if($key == 'status'){
                $search_filters[] = array(
                                'match' =>  array($key  =>  $value['equal'])
                    );
            }
            
            }
            else if($key == 'awards'){
                
                //dd($value);
                $value = strtolower($value);
                $text = explode("|", $value);
                $search_filters[] = array('match_phrase_prefix' => array('awards.name' => $text[0]) );
                
                if (strpos($value,'|') !== false) {
                    $search_filters[] = array('wildcard' =>  array('awards.year'  =>  $text[1] . "*") );    
                }
            }
            else{
                $value = strtolower($value);
                   if($value != 'null'){
                      if (strpos($value,'||') !== false) {
                    $text = explode("||", $value);
                    foreach ($text as $search_item) {
                        if($search_item != ""){
                         $search_or_filters[] = array('match_phrase_prefix' =>  array($key  =>  $search_item) );
                        }
                    }
                    $search_filters[] = array('bool' => array(
                            'should'  => $search_or_filters
                        ));
                }      
            else{
                                $search_wildcard_filters = array();
                $search_wc_filters = array();
                
                if($value != "null"){
                                $search_item = explode(" ", $value);


                foreach ($search_item as $item) {
                    $search_wc_filters[] = array('wildcard' => array($key => $item . "*"));
                }

                $search_wildcard_filters[]['bool']['must'] = $search_wc_filters;

                $search_wildcard_filters[] = array(
                            'prefix' => array($key => [ 'value' =>  $value, 'boost' => 2])
                        );
                $search_wildcard_filters[] = ['term' => [$key => [ 'value' => $value,  'boost' => 3]]];

                if($value != 'null'){
           
                $search_filters[]['bool'] = array( 
                            'should' => $search_wildcard_filters
                            );
                    
        
          
            
                } 
                }
   
           
            }
                    
                }
            }
                // $value = str_replace(" ", "* ", $value);
             

            
        }

        // $search_filters[] = array(
        //         'wildcard' => array('scholarship_types' => 'D*')
        //     );

        $sortParameters = $sortParameters->getSort()[0];
        $sort_key = substr($sortParameters, 1);
           
        if(!is_null($sort_key) && $sort_key != "id"){
            $starts_with  = substr($sortParameters, 0, 1);
            if($starts_with == "-"){
                    $search_param['body']['sort'] = array(
                        array($sort_key => 'desc')
                    ); 
             }
             else{
                $search_param['body']['sort'] = array(
                        array($sortParameters => 'asc')
                ); 
             }
        }
        // dd(count($search_filters));
        if(count($search_filters) > 0){
              $search_param['body']['query']['bool']= array(
                'must'=> $search_filters
            );
        }
        else{            

        }


        $search_param['size'] = $paginationParameters ? $paginationParameters->getLimit() : 99999999;
        $search_param['from'] = $paginationParameters ? $paginationParameters->getFrom() : 0;
        $search_param['index'] = 'galactus_production';
        $search_param['type']  = 'scholarships';
       
        $results = $client->search($search_param);
        // dd($results);
        return $results;
    }



    /**
     * @param  Semester             $semester
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findGradesBySemester(
        Semester $semester,
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $semester->grades()->getQuery();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return ResultCollection
     */
    function findPolicies(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = AllowancePolicy::query();

        // $query->where('active', true);
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return ResultCollection
     */
    function findPolicyOverrides(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = AllowancePolicyOverride::query();

        // $query->where('active', true);
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  Scholarship          $scholarship
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    public function findAwardsByScholarship(
        Scholarship $scholarship,
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $scholarship->awards()->getQuery();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            // $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }

    /**
     * @param  Semester             $semester
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    public function findAwardsBySemester(
        Semester $semester,
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $semester->awards()->getQuery();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            // $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    public function findSchools(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = School::query();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    public function findAwards(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = Award::query();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    public function findSubjects(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = Subject::query();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    public function findAllowances(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = Allowance::query();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  string $name
     * @return array
     */
    public function searchAwardsByName($name)
    {
        $query = Award::select(['id', 'name'])->where('name', 'LIKE', '%'.$name.'%')->get();

        return $query->toArray();
    }


    /**
     * @param  string $name
     * @return array
     */    
    public function searchSubjectsByName($name)
    {
        $query = Subject::select(['id', 'name'])->where('name', 'LIKE', '%'.$name.'%')->get();

        return $query->toArray();
    }


    /**
     * @param  string $name
     * @return array
     */
    public function searchSchoolsByName($name)
    {
        $query = School::select(['id', 'name'])->where('name', 'LIKE', '%'.$name.'%')->get();

        return $query->toArray();
    }
    
    
    /**
    * Get All Inactive Scholar
    *  
    * @param array $inputs
    * @return array
    */
    public function findInActiveScholar($inputs)
    {
           $query = Scholarship::with(['semesters' => function($subQuery) use ($inputs)
           {
            $subQuery->whereNotIn('school_year', [$inputs['from'], $inputs['to']]);
            if($inputs['level'] != 'secondary')
                $subQuery->whereNotIn('term', [$inputs['from_semester'], $inputs['to_semester']]);   
           }]);
           
           $query->where('status', 'active');
           
           $result = $this->repository->query($query);
           
           return $result;
    }
}