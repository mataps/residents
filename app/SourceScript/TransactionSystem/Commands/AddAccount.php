<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\TransactionSystem\Repositories\TransactionAccountRepositoryInterface;
use SourceScript\UserManagement\Repositories\PermissionRepositoryInterface;
use User;
use TransactionAccount;
use Permission;

class AddAccount {

	use ValidableTrait;

	/**
     * For logging
     * 
     * @var string
     */
    public $historable = 'add_resource';

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'name' => 'required|unique:transaction_accounts',
		'affiliation_id' => 'required|exists:affiliations,id',
		'designation' => 'required',
		'label' => 'required',
		'signatory' => 'required'
	];

	/**
	 * @var TransactionAccountRepositoryInterface
	 */
	private $accountRepository;


	function __construct(TransactionAccountRepositoryInterface $transactionAccountRepositoryInterface, PermissionRepositoryInterface $permissionRepositoryInterface)
	{
		$this->accountRepository = $transactionAccountRepositoryInterface;
		$this->permissionRepository = $permissionRepositoryInterface;
	}


	/**
	 * @param  array  $inputs
	 * @param  User   $creator
	 * @return Account
	 */
	function handle(array $inputs, User $creator)
	{
		$account = new TransactionAccount;
		$account->fill($inputs);

		$this->accountRepository->save($account);

		$permission = new Permission;
		$permission->name = Permission::TRANSACTION_SYSTEM_MANAGE_TRANSACTIONS . "_" . str_replace(" ", "_", $inputs['name']);

		$this->permissionRepository->save($permission);

		$permission = new Permission;
		$permission->name = Permission::TRANSACTION_SYSTEM_VIEW_TRANSACTIONS . "_" . str_replace(" ", "_", $inputs['name']);

		$this->permissionRepository->save($permission);

		return $account;
	}
}