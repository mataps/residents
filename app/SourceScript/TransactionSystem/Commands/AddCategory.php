<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\TransactionSystem\Repositories\TransactionCategoryRepositoryInterface;
use User;
use TransactionCategory;

class AddCategory {

	use ValidableTrait;

	/**
     * For logging
     * 
     * @var string
     */
    public $historable = 'add_resource';

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'name' => 'required'
	];

	/**
	 * @var TransactionCategoryRepositoryInterface
	 */
	private $categoryRepository;


	function __construct(TransactionCategoryRepositoryInterface $transactionCategoryRepositoryInterface)
	{
		$this->categoryRepository = $transactionCategoryRepositoryInterface;
	}

	/**
	 * @param  array  $inputs
	 * @param  User   $creator
	 * @return Category
	 */
	function handle(array $inputs, User $creator)
	{
		$category = new TransactionCategory;
		$category->fill($inputs);
		$category->created_by = $creator->id;
		$category->modified_by = $creator->id;

		$this->categoryRepository->save($category);

		return $category;
	}
}