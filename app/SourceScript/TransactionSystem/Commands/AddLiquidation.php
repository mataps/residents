<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\Common\Exceptions\ValidationException;
use SourceScript\TransactionSystem\Repositories\LiquidationRepositoryInterface;
use Liquidation;
use User;
use Transaction;

class AddLiquidation {

	use ValidableTrait;

	/**
     * For logging
     * 
     * @var string
     */
    public $historable = 'add_resource';

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'label' => 'required',
		'amount' => 'required|numeric'
	];


	/**
	 * @var LiquidationRepositoryInterface
	 */
	private $liquidationRepository;


	function __construct(LiquidationRepositoryInterface $liquidationRepositoryInterface)
	{
		$this->liquidationRepository = $liquidationRepositoryInterface;
	}


	/**
	 * @param  array       $inputs
	 * @param  Transaction $transaction
	 * @param  User        $creator
	 * @return Liquidation
	 */
	public function handle(array $inputs, Transaction $transaction, User $creator)
	{
		$liquidations = $this->liquidationRepository->findLiquidationsByTransaction($transaction);

		if(($liquidations->sum('amount') + $inputs['amount']) > $transaction->amount) throw new ValidationException(['Liquidations' => 'Liquidation amount invalid']);

		$liquidation = new Liquidation;

		$liquidation->fill($inputs);
		$liquidation->transaction_id 	= $transaction->id;
		$liquidation->created_by 		= $creator->id;
		$liquidation->modified_by 		= $creator->id;

		$this->liquidationRepository->save($liquidation);

		return $liquidation;
	}
}