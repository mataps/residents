<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\Common\Collections\ResultCollection;
use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\TransactionSystem\Factories\VoucherFactory;
use SourceScript\TransactionSystem\Factories\TransactionItemFactory;
use User;
use Transaction;

class AddSalaries {

    use ValidableTrait;


    /**
     * History action
     * 
     * @var string
     */
    public $history = 'add_resources';

    /**
     * Validation rules
     * 
     * @var array
     */
    protected $rules = array(
        // 'item_name'         => 'required',
        // 'transaction_accountst_type_id'   => 'required|exists:transaction_accounts,id',
        // 'sub_category_id'   => 'required|exists:transaction_sub_categories,id',
        // 'item_name'         => 'required',
       
        // 'transaction_type'  => 'required|in:expense,income,non-monetary',
        // 'details'           => 'required',
        // 'voucher_id' 		=> 'required',
        // 'referrer_id' 		=> 'required',
        // 'client_id' 		=> 'required',
        // 'client_type' 		=> 'required|in:Resident,Affiliation',
        // 'beneficiary_id' 	=> 'required',
        // 'beneficiary_type' 	=> 'required|in:Resident,Affiliation',
        // 'liquidatable' 		=> 'required'
    );



	/**
     * @var VoucherFactory
     */
    private $voucherFactory;


    function __construct(VoucherFactory $voucherFactory, TransactionItemFactory $transactionItemFactory)
    {
        $this->voucherFactory = $voucherFactory;
        $this->transactionItemFactory = $transactionItemFactory;
    }


	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $residents
	 * @param  User             $creator
	 * @return Collection
	 */
	public function handle(array $inputs, User $creator)
	{
		$transactions = new ResultCollection;

		$members = $inputs['members'];

		foreach($members as $member)
		{
			if($member['amount'] == 0 || is_null($member['amount'])) continue;
			// dd($member);
			$transaction = new Transaction;
        
	        
	        $transaction->referrer_id       = $member['id'];
	        $transaction->beneficiary_id    = $member['id'];
	        $transaction->beneficiary_type  = 'Resident';
	        $transaction->sub_category_id   = $inputs['sub_category_id'];
	        $transaction->item_id           = $this->transactionItemFactory->create($inputs['item_name'], $creator);
	        $transaction->transaction_type  = $inputs['transaction_type'];
	        $transaction->details           = (isset($inputs['details'])) ? $inputs['details'] : '';
	        $transaction->remarks           = (isset($inputs['remarks'])) ? $inputs['remarks'] : '';
	        $transaction->amount            = $member['amount'];
	        $transaction->liquidatable 		= false;
	        // $transaction->vendor_id 		= $inputs['vendor_id'];
	        $voucher = $this->voucherFactory->create(array_add($inputs, 'client_id', $member['id']), $creator);

	        $transaction->voucher_id        = $voucher->id;
	        $transaction->client_id         = $voucher->client_id;
	        $transaction->client_type       = 'Resident';
	        

	        $transaction->created_by        = $creator->id;
	        $transaction->modified_by       = $creator->id;

     	   if(isset($inputs['created_at']))
        	{
            $transaction->timestamps = false;
            $transaction->setCreatedAt(date('Y-m-d H:i:s', strtotime($inputs['created_at'])));
            $transaction->setUpdatedAt(date('Y-m-d H:i:s', strtotime($inputs['created_at'])));
        	}

	        $transaction->save();

	        $transaction->generateUuid();

	        $transactions->add($transaction);
		}

		return $transactions;
	}
}