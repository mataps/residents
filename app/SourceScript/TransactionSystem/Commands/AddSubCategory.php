<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\TransactionSystem\Repositories\TransactionSubCategoryRepositoryInterface;
use User;
use TransactionSubCategory;

class AddSubCategory {

	use ValidableTrait;


	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'name' => 'required',
		'category_id' => 'required|exists:transaction_categories,id'
	];


	/**
	 * @var TransactionSubCategoryRepositoryInterface
	 */
	private $repository;


	function __construct(TransactionSubCategoryRepositoryInterface $transactionSubCategoryRepository)
	{
		$this->repository = $transactionSubCategoryRepository;
	}

	/**
	 * @param  array  $inputs
	 * @param  User   $creator
	 * @return TransactionSubcategory
	 */
	public function handle(array $inputs, User $creator)
	{
		$subCategory = new TransactionSubCategory;

		$subCategory->fill($inputs);

		$subCategory->modified_by = $creator->id;
		$subCategory->created_by = $creator->id;

		$this->repository->save($subCategory);

		return $subCategory;
	}
}