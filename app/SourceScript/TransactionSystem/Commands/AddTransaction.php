<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\Profiling\ProfilingInterface;
use SourceScript\Profiling\Repositories\ResidentRepositoryInterface;
use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\TransactionSystem\Factories\VoucherFactory;
use SourceScript\TransactionSystem\Factories\TransactionItemFactory;
use Transaction;
use User;
use Voucher;
use ResidentAffiliation;

class AddTransaction {

    /**
     * For logging
     *
     * @var string
     */
    public $historable = 'add_resource';

    use ValidableTrait;

    /**
     * Validation rules
     *vendor_i
     * @var array
     */
    protected $rules = array(
        'beneficiary_id'    => 'required|integer',
        'beneficiary_type'  => 'required|in:Resident,Affiliation',
        'account_type_id'   => 'required|exists:transaction_accounts,id',
        'sub_category_id'   => 'required|exists:transaction_sub_categories,id',
        'item_name'         => 'required',
        'amount'            => 'required|numeric',
        'client_id'         => 'required|exists:residents,id',
        'referrer_id'       => 'required|exists:residents,id',
        'transaction_type'  => 'required|in:expense,income,non-monetary,transfer-income,transfer-expense',
        'details'           => 'required',
        'liquidatable'      => 'boolean',
        //'vendor_id'         => 'exists:transaction_vendors,id',
        'created_at' => 'date',
        'transfer_account_id' => 'exists:transaction_accounts,id'
    );


    /**
     * @var VoucherFactory
     */
    private $voucherFactory;

    /**
     * @var ProfilingService
     */
    private $profilingService;


    function __construct(VoucherFactory $voucherFactory,
        TransactionItemFactory $transactionItemFactory,
        ProfilingInterface $profilingInterface,
        ResidentRepositoryInterface $residentRepositoryInterface)
    {
        $this->residentRepository = $residentRepositoryInterface;
        $this->voucherFactory = $voucherFactory;
        $this->transactionItemFactory = $transactionItemFactory;
        $this->profilingService = $profilingInterface;
    }

    /**
     * @param array $inputs
     * @param User $creator
     * @return Affiliation
     */
    function handle(array $inputs, User $creator)
    {
        if($inputs['beneficiary_type'] == 'Affiliation')
        {
            $residents = $this->residentRepository->findResidents($inputs['client_id']);
            if(ResidentAffiliation::where('resident_id', $inputs['client_id'])->where('affiliation_id', $inputs['beneficiary_id'])->first())
            {
                $this->profilingService->execute('AddResidentsAffiliation', ['affiliation_id' => $inputs['beneficiary_id']], $residents, $creator);
                foreach($residents as $resident)
                {
                    $this->profilingService->execute('UpdateIndexResident', [], $resident, $resident->id);
                }    
            }
        }

        $transaction = new Transaction;

        $transaction->name              = $inputs['name'];
        $transaction->referrer_id       = $inputs['referrer_id'];
        $transaction->beneficiary_id    = $inputs['beneficiary_id'];
        $transaction->beneficiary_type  = $inputs['beneficiary_type'];
        $transaction->sub_category_id   = $inputs['sub_category_id'];
        $transaction->item_id           = $this->transactionItemFactory->create($inputs['item_name'], $creator);
        $transaction->transaction_type  = $inputs['transaction_type'];
        $transaction->details           = $inputs['details'] ? $inputs['details'] : '';
        $transaction->amount            = $inputs['amount'];
        $transaction->remarks           = $inputs['remarks'] ? $inputs['remarks'] : '';
        $transaction->liquidatable      = isset($inputs['liquidatable']) ? true : false;
        $transaction->vendor_id         = $inputs['vendor_id'];
        $transaction->transfer_account_id = $inputs['transfer_account_id'];

        $voucher = $this->voucherFactory->create($inputs, $creator);

        $transaction->voucher_id        = $voucher->id;
        $transaction->client_id         = $inputs['client_id'];
        $transaction->client_type       = $inputs['client_type'];

        $transaction->created_by        = $creator->id;
        $transaction->modified_by       = $creator->id;

        $transaction->settled_at        = null;
        $transaction->liquidated_at     = null;

        if($inputs['created_at'])
        {

            $transaction->timestamps = false;
            $transaction->setCreatedAt(date('Y-m-d H:i:s', strtotime($inputs['created_at'])));
            $transaction->setUpdatedAt(date('Y-m-d H:i:s', strtotime($inputs['created_at'])));
        }

        $transaction->save();

        $transaction->generateUuid();

        return $transaction;
    }

}
