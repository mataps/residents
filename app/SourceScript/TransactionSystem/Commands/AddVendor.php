<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\TransactionSystem\Repositories\VendorRepositoryInterface;
use SourceScript\Profiling\Factories\BarangayFactory;
use SourceScript\Profiling\Factories\CityMunicipalityFactory;
use SourceScript\Profiling\Factories\DistrictFactory;
use User;
use TransactionVendor;

class AddVendor {

    use ValidableTrait;


    /**
     * Validation rules
     * 
     * @var array
     */
    protected $rules = [
    	'name' => 'required',
    	// 'district'          => 'required',
     //    'barangay'          => 'required',
     //    'cityMunicipality'  => 'required',
     //    'street'            => 'required'
    ];


    /**
     * @var DistrictFactory
     */
    private $districtFactory;

    /**
     * @var CityMunicipalityFactory
     */
    private $cityMunicipalityFactory;

    /**
     * @var BarangayFactory
     */
    private $barangayFactory;


    /**
     * @var VendorRepositoryInterface
     */
    private $vendorRepository;


    function __construct(
    	VendorRepositoryInterface $vendorRepository,
        DistrictFactory $districtFactory,
        CityMunicipalityFactory $cityMunicipalityFactory,
        BarangayFactory $barangayFactory)
    {
    	$this->vendorRepository 		= $vendorRepository;
        $this->districtFactory          = $districtFactory;
        $this->cityMunicipalityFactory  = $cityMunicipalityFactory;
        $this->barangayFactory          = $barangayFactory;
    }


    /**
     * @param  array  $inputs
     * @param  User   $creator
     * @return TransactionVendor
     */
	public function handle(array $inputs, User $creator)
	{
		$vendor = new TransactionVendor;

		$vendor->name 					= $inputs['name'];
		$vendor->description 			= $inputs['name'];

		$vendor->district            	= $this->districtFactory->create( $inputs['district'] );
        $vendor->barangay           	= $this->barangayFactory->create( $inputs['barangay'] );
        $vendor->cityMunicipality    	= $this->cityMunicipalityFactory->create( $inputs['cityMunicipality'] );
        $vendor->street              	= $inputs['street'];


		$vendor->created_by = $creator->id;
		$vendor->modified_by = $creator->id;

		$this->vendorRepository->save($vendor);

		return $vendor;
	}
}