<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use Sourcescript\TransactionSystem\Repositories\VoucherRepositoryInterface;
use Voucher;
use User;

class AddVoucher {

	/**
     * For logging
     * 
     * @var string
     */
    public $historable = 'add_resource';

	use ValidableTrait;

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'account_type_id'   => 'required|exists:transaction_accounts,id',
		'client_id'         => 'required|integer',
		'client_type' 		=> 'required|in:Resident,Affiliation'
	];

	function __construct(VoucherRepositoryInterface $voucherRepositoryInterface)
	{
		$this->voucherRepository = $voucherRepositoryInterface;
	}

	/**
	 * @param  array  $inputs
	 * @param  User   $creator
	 * @return Voucher
	 */
	public function handle(array $inputs, User $creator)
	{
		$voucher = new Voucher;
		$voucher->fill($inputs);

		$voucher->created_by 	= $user->id;
		$voucher->modified_by 	= $user->id;

		$this->voucherRepository->save($voucher);

		return $voucher;
	}
}