<?php namespace SourceScript\TransactionSystem\Commands;


use Resident;
use Elasticsearch\Client;

class IndexTransaction {

    protected $client;

    function handle($input, $transaction)
    {           
            // dd('here');
            

            $param = \Config::get('elasticsearch');
            $this->client = new Client($param);
            $params['body'] = array(
                    'voucher_uuid'       => $transaction->voucher->uuid,
                    'voucher_id'         => $transaction->voucher->id,
                    'id'                 => $transaction->id,
                    'vendor_name'        => ($transaction->vendor) ? $transaction->vendor->name : null,
                    'client_name'        => $transaction->voucher ? ($transaction->voucher->client ? $transaction->voucher->client->fullName() : null) : null,
                    'client_type'        => $transaction->voucher->client_type,
                    'beneficiary_name'   => $transaction->beneficiary ? $transaction->beneficiary->fullName() : null,
                    'beneficiary_type'   => $transaction->beneficiary_type,
                    'referer_name'       => $transaction->referrer ? $transaction->referrer->fullName() : null,
                    'account_name'       => $transaction->account->name,
                    'account_type_id'    => $transaction->account->id,
                    'category'           => $transaction->subCategory->category->name,
                    'sub_category'       => $transaction->subCategory->name,
                    'item'               => $transaction->item->name, 
                    'settled_at'         => isset($transaction->settled_at) ? date("Y-m-d", strtotime($transaction->settled_at)) : null,
                    'created_at'         => date('Y-m-d', strtotime($transaction->created_at)),
                    'details'            => $transaction->details, 
                    'remarks'            => $transaction->remarks,
                    'amount'             => $transaction->amount,
                    'vendor'             => $transaction->vendor_id,
                    'liquidated_at'      => $transaction->liquidated_at,
                    'transaction_type'   => $transaction->transaction_type
                );


        
            // dd($transaction->beneficiary_type == "Affiliation");
            if($transaction->beneficiary_type == "Affiliation"){
                $params['body']['beneficiary_name'] = $transaction->beneficiary->name;
            }
            $params['id'] = $transaction->id;

           
            $params['index'] = 'galactus_production';
            $params['type']  = 'transactions';
        
            // dd($paramams);
            // dd($params);

            $this->client->index($params);
            
            return true;
    }
}