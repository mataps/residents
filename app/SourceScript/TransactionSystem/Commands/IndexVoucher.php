<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\TransactionSystem\Repositories\VoucherRepositoryInterface;
use SourceScript\TransactionSystem\Repositories\TransactionRepositoryInterface;
use SourceScript\TransactionSystem\TransactionSystemInterface;
use Voucher;
use User;

class IndexVoucher {

	/**
	 * @var VoucherRepositoryInterface
	 */
	private $voucherRepository;

	function __construct(
		TransactionSystemInterface $transactionSystemService,
		VoucherRepositoryInterface $voucherRepositoryInterface,
		TransactionRepositoryInterface $transactionRepositoryInterface)
	{
		$this->transactionSystemService = $transactionSystemService;
		$this->voucherRepository = $voucherRepositoryInterface;
		$this->transactionRepository = $transactionRepositoryInterface;
	}


	/**
	 * @param  array   $inputs
	 * @param  Voucher $voucher
	 * @param  User    $user
	 * @return Voucher
	 */
	public function handle(array $inputs, Voucher $voucher)
	{
		
		foreach($voucher->transactions as $transaction)
		{
	        $this->transactionSystemService->execute('IndexTransaction', $inputs, $transaction);

		}
	

		return $voucher;
	}
}