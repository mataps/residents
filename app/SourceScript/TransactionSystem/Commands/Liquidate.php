<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\TransactionSystem\Repositories\LiquidationRepositoryInterface;
use SourceScript\TransactionSystem\Repositories\TransactionRepositoryInterface;
use SourceScript\Common\Exceptions\ValidationException;
use Transaction;
use User;

class Liquidate {

	use ValidableTrait;


	/**
	 * @var LiquidationRepositoryInterface
	 */
	private $liquidationRepository;


	/**
	 * @var TransactionRepositoryInterface
	 */
	private $transactionRepository;


	function __construct(
		LiquidationRepositoryInterface $liquidationRepositoryInterface,
		TransactionRepositoryInterface $transactionRepositoryInterface)
	{
		$this->liquidationRepository = $liquidationRepositoryInterface;
		$this->transactionRepository = $transactionRepositoryInterface;
	}


	/**
	 * @param  array       $inputs
	 * @param  Transaction $transaction
	 * @param  User        $liquidator
	 * @return Transaction
	 */
	public function handle(array $inputs, Transaction $transaction, User $liquidator)
	{
		if($transaction->transaction_type == "income") throw new ValidationException(['error' => "Cant liquidate income"]);

		if(!$transaction->liquidatable || $transaction->liquidated_at !== null) throw new ValidationException(['transaction' => 'Transaction not liquidatable']);

		extract($inputs);

		$liquidations = $this->liquidationRepository->findLiquidationsByTransaction($transaction);

		if($liquidations->sum('amount') > $transaction->amount) throw new ValidationException(['error' => 'Liquidation amount invalid']);

		$transaction->liquidated_by = $liquidator;
		$transaction->liquidated_at = date('Y-m-d H:i:s');

		$this->transactionRepository->save($transaction);

		$change = $transaction->amount - $liquidations->sum('amount');


		$income = new Transaction;
		$income->referrer_id 		= $transaction->referrer_id;
		$income->client_id 			= $transaction->id;
		$income->beneficiary_type 	= "Affiliation";
		$income->beneficiary_id 	= $transaction->account->affiliation->id;
		$income->client_type 		= $transaction->client_type;
		$income->client_id 			= $transaction->client_id;
		$income->amount 			= $change;
		$income->sub_category_id 	= $transaction->sub_category_id;
		$income->item_id 			= $transaction->item_id;
		$income->transaction_type 	= 'income';
		$income->details 			= 'Liquidation returns from ' . $transaction->uuid;
		$income->voucher_id 		= $transaction->voucher_id;

		$income->created_by 		= $liquidator->id;
		$income->modified_by 		= $liquidator->id;
		$income->settled_at 		= date('Y-m-d H:i:s');

		if($change > 0)
		{
			$this->transactionRepository->save($income);

			$income->generateUuid();
		}


		return $income;
	}
}