<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\TransactionSystem\Factories\TransactionItemFactory;
use SourceScript\TransactionSystem\Repositories\TransactionRepositoryInterface;
use SourceScript\Common\Validations\ValidableTrait;
use Transaction;
use User;
use SourceScript\Common\Exceptions\ValidationException;

class OverrideUpdateTransaction {

	use ValidableTrait;

	/**
     * For logging
     * 
     * @var string
     */
    public $historable = 'update_resource';

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'account_type_id'   => 'required|exists:transaction_accounts,id',
        'sub_category_id'   => 'required|exists:transaction_sub_categories,id',
        'item_name'         => 'required',
        'amount'            => 'required|numeric',
        'transaction_type'  => 'required|in:expense,income,non-monetary',
        'details'           => 'required',
        'voucher_id' 		=> 'required',
        'referrer_id' 		=> 'required',
        // 'client_id' 		=> 'required',
        'client_type' 		=> 'required|in:Resident,Affiliation',
        'beneficiary_id' 	=> 'required',
        'beneficiary_type' 	=> 'required|in:Resident,Affiliation',
        'liquidatable' 		=> 'boolean'
	];

	/**
	 * @var TransactionRepositoryInterface
	 */
	private $repository;


	/**
	 * @var TransactionItemFactory
	 */
	private $transactionItemFactory;


	function __construct(
		TransactionRepositoryInterface $transactionRepositoryInterface,
		TransactionItemFactory $transactionItemFactory)
	{
		$this->repository = $transactionRepositoryInterface;
        $this->transactionItemFactory = $transactionItemFactory;
	}


	/**
	 * @param  array       $inputs
	 * @param  Transaction $transaction
	 * @param  User        $updater
	 * @return Transaction
	 */
	public function handle(array $inputs, Transaction $transaction, User $updater)
	{
	

		$transaction->fill(array_except($inputs, ['item_name', 'account_type_id']));

		$transaction->item_id = $this->transactionItemFactory->create($inputs['item_name'], $updater);

		$transaction->modified_by = $updater->id;

		$transaction->client_id = $transaction->voucher->client_id;

		$this->repository->save($transaction);

		return $transaction;
	}
}