<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\TransactionSystem\Repositories\TransactionRepositoryInterface;
use SourceScript\Common\Exceptions\ValidationException;
use Transaction;
use User;
use Config;
use DB;
use Resident;
use TransactionVendor;
use Voucher;
class PullTransaction {
	/**
	 * @var TransactionRepositoryInterface
	 */
	private $transactionRepository;


	function __construct(TransactionRepositoryInterface $transactionRepositoryInterface)
	{
		$this->transactionRepository = $transactionRepositoryInterface;
	}


	/**
	 * @param  array       $inputs
	 * @param  Transaction $transaction
	 * @param  User        $user
	 * @return Transaction
	 */
	public function handle()
	{
        ini_set('max_execution_time', 0);
        //record the start time
        $time_start = microtime(true);
	    
 		$old_data = DB::select("SELECT CONCAT_WS('-', ct.voucher_no, ct.OID) as uuid, ct.is_deleted as is_deleted, ct.accounts_OID as account_id, ct.transaction_type as transaction_type, ct.voucher_no as voucher_uuid, cr.fullname as referrer_name, cc.fullname  as client_name, cb.fullname as beneficiary_name, ctd.transaction_subcategory_oid as sub_category_id, ctd.items_oid as item_id, ctd.details as details, ctd.remarks as remarks, ctd.amount as amount, ct.settle_date as settled_at, cau.user_name as created_by_name, ct.transaction_date as transaction_date, ct.date_created as created_at, ct.date_modified as updated_at, cv.vendor_name as vendor_name from cdodb_base.transactions as ct left join cdodb_base.transaction_detail as ctd on ct.oid = ctd.transactions_oid LEFT JOIN cdodb_base.transactors AS ctr ON ctr.transaction_detail_oid = ctd.oid LEFT JOIN cdodb_base.resident_fast AS cc ON cc.oid = ctr.client_oid LEFT JOIN cdodb_base.resident_fast AS cb ON  cb.oid = ctr.beneficiary_oid LEFT JOIN cdodb_base.resident_fast AS cr ON cr.oid = ctr.referrer_oid LEFT JOIN cdodb_base.app_user AS cau ON cau.oid = ct.handler_oid LEFT JOIN cdodb_base.vendors AS cv ON cv.oid = ctd.vendors_oid where ct.transaction_type != 'F' GROUP BY ctd.OID");

 	
 		$total = count($old_data);
 		$counter = 0;
 	
 		foreach ($old_data as $oldTrans) {
 			$counter++;	
 			echo $counter . "/" . $total . "\r\n";

		$referrer= Resident::where('full_name', $oldTrans->referrer_name)->first();
 		$referrer_id = (isset($referrer)) ? $referrer->id : null;  		
 		$client= Resident::where('full_name', $oldTrans->client_name)->first();
 		$client_id = (isset($client)) ? $client->id : null; 
 		$beneficiary= Resident::where('full_name', $oldTrans->beneficiary_name)->first();
 		$beneficiary_id = (isset($beneficiary)) ? $beneficiary->id : null; 	
 		$created_by = User::where('username', 'old_' . $oldTrans->created_by_name)->first();
 		$created_by_id = (isset($created_by)) ? $created_by->id : null; 

 		$vendor = TransactionVendor::where('name', $oldTrans->vendor_name)->first();
 		$vendor_id = (isset($vendor)) ? $vendor->id : null;



 		if($oldTrans->transaction_type == 'E'){
 			$transaction_type = 'expense';
 		}
 		else if($oldTrans->transaction_type == 'I'){
 			$transaction_type = 'income';
 		} 
 		else if($oldTrans->transaction_type == 'F'){
 			$transaction_type = 'non-monetary';
 		}
 		else {
 			$transaction_type = '';
 		}

 		if($oldTrans->created_at == null || $oldTrans->created_at == ''){
 			if($oldTrans->transaction_date == null || $oldTrans->transaction_date == '') {
		 		$created_at = date('Y-m-d', strtotime('1970-01-01')); 				
 			}
 			$created_at = $oldTrans->transaction_date;
 		}
 		else {
 			$created_at = $oldTrans->created_at;
 		}

		$v = new Voucher;
		$v->uuid = $oldTrans->voucher_uuid;
		$v->client_id = $client_id;
		$v->client_type = "Resident";
		$v->account_type_id = $oldTrans->account_id;
		$v->status = 'approved';
		$v->remarks = $oldTrans->remarks;
		$v->details = $oldTrans->details;
		$v->created_by = $created_by_id;
		$v->created_at = $created_at;
		$v->modified_by = $created_by_id;
		$v->updated_at = $oldTrans->updated_at;
		if($oldTrans->is_deleted){
			$v->deleted_at = $oldTrans->updated_at;
		}
		$v->save();
		$voucher_id = $v->id; 

  			$new_transaction = array(
 					'uuid' => $oldTrans->uuid,
 					'referrer_id' => $referrer_id,
 					'client_id' => $client_id,
 					'client_type' => 'Resident',
 					'beneficiary_id' => $beneficiary_id,
 					'beneficiary_type' => 'Resident',
 					'settled_at'	=> ($oldTrans->settled_at) ? $oldTrans->settled_at : null,
 					'sub_category_id' => $oldTrans->sub_category_id,
 					'item_id' => $oldTrans->item_id,
 					'transaction_type' => $transaction_type,
 					'details' => $oldTrans->details,
 					'remarks' => $oldTrans->remarks,
 					'amount' => $oldTrans->amount,
 					'created_by' => $created_by_id,
 					'modified_by' => $created_by_id,
 					'created_at' => $created_at,
 					'updated_at' => $oldTrans->updated_at,
 					'vendor_id' => $vendor_id,
 					'voucher_id' => $voucher_id,
 					'liquidatable' => 0,


 				);

		if($oldTrans->is_deleted){
			$new_transaction['deleted_at'] = $oldTrans->updated_at;
		}

 			DB::table('transactions_script')
 				->insert($new_transaction);


 		}






	}
}