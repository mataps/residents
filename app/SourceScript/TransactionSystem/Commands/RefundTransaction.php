<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\TransactionSystem\Repositories\TransactionRepositoryInterface;
use SourceScript\Common\Exceptions\ValidationException;
use Transaction;
use User;

class RefundTransaction {
	/**
	 * @var TransactionRepositoryInterface
	 */
	private $transactionRepository;


	function __construct(TransactionRepositoryInterface $transactionRepositoryInterface)
	{
		$this->transactionRepository = $transactionRepositoryInterface;
	}


	/**
	 * @param  array       $inputs
	 * @param  Transaction $transaction
	 * @param  User        $user
	 * @return Transaction
	 */
	public function handle(array $inputs, Transaction $transaction, User $user)
	{
		if($transaction->transaction_type == "income" || $transaction->refund_id !== null) throw new ValidationException(['transaction' => 'Transaction not refundable']);

		extract($inputs);

		$income = new Transaction;

		$income->referrer_id 		= $transaction->referrer_id;
		$income->client_id 			= $transaction->id;
		$income->beneficiary_type 	= "Affiliation";
		$income->beneficiary_id 	= $transaction->account->affiliation->id;
		$income->client_type 		= $transaction->client_type;
		$income->client_id 			= $transaction->client_id;
		$income->amount 			= $transaction->amount;
		$income->sub_category_id 	= $transaction->sub_category_id;
		$income->item_id 			= $transaction->item_id;
		$income->transaction_type 	= 'income';
		$income->details 			= 'Refund from ' . $transaction->uuid;
		$income->voucher_id 		= $transaction->voucher_id;

		$income->created_by 		= $user->id;
		$income->modified_by 		= $user->id;

		$this->transactionRepository->save($income);

		$income->generateUuid();

		$transaction->refund_id = $income->id;

		$this->transactionRepository->save($transaction);

		return $income;
	}
}