<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\TransactionSystem\Repositories\TransactionAccountRepositoryInterface;
use TransactionAccount;

class RemoveAccount {
	
	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'remove_resource';

	/**
	 * @var TransactionAccountRepositoryInterface
	 */
	private $repository;


	function __construct(TransactionAccountRepositoryInterface $transactionAccountRepositoryInterface)
	{
		$this->repository = $transactionAccountRepositoryInterface;
	}


	/**
	 * @param  array              $inputs
	 * @param  TransactionAccount $transactionAccount
	 * @return TransactionAccount
	 */
	public function handle(array $inputs, TransactionAccount $transactionAccount)
	{
		$this->repository->delete($transactionAccount);

		return $transactionAccount;
	}
}