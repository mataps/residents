<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\TransactionSystem\Repositories\TransactionAccountRepositoryInterface;
use SourceScript\Common\Collections\ResultCollection;

class RemoveAccounts {


	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'remove_resources';

	/**
	 * @var TransactionAccountRepositoryInterface
	 */
	private $repository;


	function __construct(TransactionAccountRepositoryInterface $transactionAccountRepositoryInterface)
	{
		$this->repository = $transactionAccountRepositoryInterface;
	}


	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $accounts
	 * @return ResultCollection
	 */
	public function handle(array $inputs, ResultCollection $accounts)
	{
		$this->repository->deleteAccounts($accounts->modelKeys());

		return $accounts;
	}
}