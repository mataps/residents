<?php namespace SourceScript\TransactionSystem\Commands;

use TransactionCategory;
use SourceScript\Common\Collections\ResultCollection;
use SourceScript\TransactionSystem\Repositories\TransactionCategoryRepositoryInterface;

class RemoveCategories {

	/**
	 * @var TransactionCategoryRepositoryInterface
	 */
	private $repository;

	function __construct(TransactionCategoryRepositoryInterface $transactionCategoryRepository)
	{
		$this->repository = $transactionCategoryRepository;
	}


	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $categories
	 */
	public function handle(array $inputs, ResultCollection $categories)
	{
		$this->repository->deleteCategories($categories->modelKeys());
	}
}