<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\TransactionSystem\Repositories\TransactionCategoryRepositoryInterface;
use TransactionCategory;

class RemoveCategory {
		

	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'remove_resource';


	/**
	 * @var TransactionCategoryRepositoryInterface
	 */
	private $repository;


	function __construct(TransactionCategoryRepositoryInterface $transactionCategoryRepositoryInterface)
	{
		$this->repository = $transactionCategoryRepositoryInterface;
	}

	/**
	 * @param  array               $inputs
	 * @param  TransactionCategory $transactionCategory
	 * @return TransactionCategory
	 */
	public function handle(array $inputs, TransactionCategory $transactionCategory)
	{
		$this->repository->delete($transactionCategory);

		return $transactionCategory;
	}	
}