<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\TransactionSystem\Repositories\LiquidationRepositoryInterface;
use Liquidation;

class RemoveLiquidation {


	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'remove_resource';

	/**
	 * @var LiquidationRepositoryInterface
	 */
	private $repository;

	function __construct(LiquidationRepositoryInterface $liquidationRepositoryInterface)
	{
		$this->repository = $liquidationRepositoryInterface;
	}


	/**
	 * @param  array       $inputs
	 * @param  Liquidation $liquidation
	 * @return Liquidation
	 */
	public function handle(array $inputs, Liquidation $liquidation)
	{
		$this->repository->delete($liquidation);

		return $liquidation;
	}
}