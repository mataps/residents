<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\Common\Collections\ResultCollection;
use SourceScript\TransactionSystem\Repositories\LiquidationRepositoryInterface;

class RemoveLiquidations {


	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'remove_resources';


	/**
	 * @var LiquidationRepositoryInterface
	 */
	private $repository;

	function __construct(LiquidationRepositoryInterface $liquidationRepositoryInterface)
	{
		$this->repository = $liquidationRepositoryInterface;
	}


	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $liquidations
	 * @return ResultCollection
	 */
	public function handle(array $inputs, ResultCollection $liquidations)
	{
		$this->repository->deleteLiquidations($liquidations->modelKeys());

		return $liquidations;
	}
}