<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\TransactionSystem\Repositories\TransactionSubCategoryRepositoryInterface;
use SourceScript\Common\ResultCollection;

class RemoveSubCategories {


	/**
	 * @var TransactionSubCategoryRepositoryInterface
	 */
	private $repository;


	function __construct(TransactionSubCategoryRepositoryInterface $transactionSubCategoryRepository)
	{
		$this->repository = $transactionSubCategoryRepository;
	}

	/**
	 * @param  array            $inputs        [description]
	 * @param  ResultCollection $subCategories [description]
	 * @return [type]                          [description]
	 */
	public function handle(array $inputs, ResultCollection $subCategories)
	{
		$this->repository->deleteSubCategories($subCategories->modelKeys());
		
		return $subCategories;	
	}
}