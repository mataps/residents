<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\TransactionSystem\Repositories\TransactionSubCategoryRepositoryInterface;
use TransactionSubCategory;

class RemoveSubCategory {


	/**
	 * @var TransactionSubCategoryRepositoryInterface
	 */
	private $repository;


	function __construct(TransactionSubCategoryRepositoryInterface $subCategoryRepository)
	{
		$this->repository = $subCategoryRepository;
	}

	/**
	 * @param  array                  $inputs
	 * @param  TransactionSubCategory $subCategory
	 */
	public function handle(array $inputs, TransactionSubCategory $subCategory)
	{
		$this->repository->delete($subCategory);
	}
}