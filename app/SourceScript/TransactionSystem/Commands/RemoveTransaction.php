<?php namespace SourceScript\TransactionSystem\Commands;

use Transaction;
use SourceScript\TransactionSystem\Repositories\TransactionRepositoryInterface;

class RemoveTransaction {

	/**
	 * History action
	 * 
	 * @var string
	 */
	public $history = 'remove_resource';

	/**
	 * @var TransactionRepositoryInterface
	 */
	private $repository;


	function __construct(TransactionRepositoryInterface $transactionRepositoryInterface)
	{
		$this->repository = $transactionRepositoryInterface;
	}


	/**
	 * @param  array       $inputs
	 * @param  Transaction $transaction
	 */
	public function handle(array $inputs, Transaction $transaction)
	{
		$this->repository->delete($transaction);

		return $transaction;
	}
}