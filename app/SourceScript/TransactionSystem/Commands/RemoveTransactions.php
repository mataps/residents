<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\Common\Collections\ResultCollection;

class RemoveTransactions {

	/**
	 * @var TransactionRepositoryInterface
	 */
	private $transactionRepository;

	function __construct(TransactionRepositoryInterface $transactionRepositoryInterface)
	{
		$this->transactionRepository = $transactionRepositoryInterface;
	}

	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $TransactionSystem
	 */
	public function handle(array $inputs, ResultCollection $transactions)
	{
		$this->transactionRepository->deleteTransactions($transactions->modelKeys());
	}
}