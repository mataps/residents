<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\TransactionSystem\Repositories\VendorRepositoryInterface;
use SourceScript\Common\Collections\ResultCollection;

class RemoveVendors {

	/**
	 * @var VendorRepositoryInterface
	 */
	private $repository;


	function __construct(VendorRepositoryInterface $vendorRepositoryInterface)
	{
		$this->repository = $vendorRepositoryInterface;
	}


	/**
	 * @param  array            $inputs
	 * @param  ResultCollection $vendors
	 * @return ResultCollection
	 */
	public function handle(array $inputs, ResultCollection $vendors)
	{
		$this->repository->deleteVendors($vendors->modelKeys());

		return $vendors;
	}
}