<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\TransactionSystem\TransactionSystemInterface;
use SourceScript\TransactionSystem\Repositories\TransactionRepositoryInterface;
use User;
use Transaction;
use SourceScript\TransactionSystem\Repositories\TransactionAccountRepositoryInterface;

class SettleTransaction {

	/**
	 * @var TransactionRepositoryInterface
	 */
	private $transactionRepository;

	/**
	 * @var TransactionSystemInterface
	 */
	private $transactionSystemService;

	function __construct(TransactionRepositoryInterface $transactionRepositoryInterface,
		TransactionSystemInterface $transactionSystemInterface,
		TransactionAccountRepositoryInterface $transactionAccountRepositoryInterface
		)
	{
		$this->transactionRepository = $transactionRepositoryInterface;
		$this->transactionSystemService = $transactionSystemInterface;
		$this->transactionAccountRepository = $transactionAccountRepositoryInterface;
	}

	/**
	 * @param  array       $inputs
	 * @param  Transaction $transaction
	 * @param  User        $settler
	 * @return Transaction
	 */
	public function handle(array $inputs, Transaction $transaction, User $settler)
	{

		if($transaction->transaction_type == 'transfer-income' || $transaction->transaction_type == 'transfer-expense') $this->createTransferee($transaction, $settler);
		
		$transaction->settled_at = date('Y-m-d H:i:s');

		$this->transactionRepository->save($transaction);

		return $transaction;
	}

	private function createTransferee(Transaction $transaction, User $settler)
	{
		$data = [
			'beneficiary_id' => $transaction->transaction_type == 'transfer-income' ? $transaction->voucher->account->affiliation->id : $this->transactionAccountRepository->findOneById($transaction->transfer_account_id)->affiliation->id,
			'beneficiary_type' => 'Affiliation',
			'account_type_id' => $transaction->transfer_account_id,
			'sub_category_id' => $transaction->voucher->account->id == 25 ? 441 : $transaction->sub_category_id,
			'item_name' => $transaction->item ? $transaction->item->name : null,
			'amount' => $transaction->amount,
			'client_id' => $transaction->client_id,
			'referrer_id' => $transaction->referrer_id,
			'transaction_type' => $transaction->transaction_type == 'transfer-income' ? 'transfer-expense' : 'transfer-income',
			'details' => $transaction->transaction_type == 'transfer-income' ? 'Cash to ' . $transaction->voucher->account->name : 'Cash from ' . $this->transactionAccountRepository->findOneById($transaction->transfer_account_id)->name,
			'transfer_account_id' => $transaction->voucher->account->id,
			'name' => '',
			'remarks' => '',
			'vendor_id' => '',
			'client_type' => 'Resident',
			'created_at' => $transaction->created_at
		];

		$tr = $this->transactionSystemService->execute('AddTransaction', $data, $settler);

		$tr->settled_at = date('Y-m-d H:i:s');

		$this->transactionRepository->save($tr);

		$this->transactionSystemService->execute('IndexTransaction', [], $tr);

		return $tr;
	}
}