<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\TransactionSystem\TransactionSystemInterface;
use SourceScript\TransactionSystem\Repositories\VoucherRepositoryInterface;
use SourceScript\TransactionSystem\Repositories\TransactionRepositoryInterface;
use Voucher;
use User;

class SettleVoucher {

	/**
	 * @var VoucherRepositoryInterface
	 */
	private $voucherRepository;

	function __construct(
		VoucherRepositoryInterface $voucherRepositoryInterface,
		TransactionRepositoryInterface $transactionRepositoryInterface,
		TransactionSystemInterface $transactionSystemService)
	{
		$this->voucherRepository = $voucherRepositoryInterface;
		$this->transactionRepository = $transactionRepositoryInterface;
        $this->transactionSystemService = $transactionSystemService;
	}


	/**
	 * @param  array   $inputs
	 * @param  Voucher $voucher
	 * @param  User    $user
	 * @return Voucher
	 */
	public function handle(array $inputs, Voucher $voucher, User $user)
	{
		$voucher->settled_at = date('Y-m-d');

		foreach($voucher->transactions as $transaction)
		{
			if(is_null($transaction->settled_at))
			{
				$transaction->settled_at = date('Y-m-d');
				
				$this->transactionRepository->save($transaction);
	        	$this->transactionSystemService->execute('IndexTransaction', $inputs, $transaction);
			}
		}


		$this->voucherRepository->save($voucher);

		return $voucher;
	}
}