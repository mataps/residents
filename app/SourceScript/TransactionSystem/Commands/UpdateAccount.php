<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\TransactionSystem\Repositories\TransactionAccountRepositoryInterface;
use User;
use TransactionAccount;

class UpdateAccount {

	/**
     * For logging
     * 
     * @var string
     */
    public $historable = 'update_resource';


    /**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'name' => 'required|unique:transaction_accounts',
		'affiliation_id' => 'required|exists:affiliations,id',
		'designation' => 'required',
		'label' => 'required',
		'signatory' => 'required'
	];

	/**
	 * @var TransactionAccountRepositoryInterface
	 */
	private $accountRepository;

	function __construct(TransactionAccountRepositoryInterface $transactionAccountRepositoryInterface)
	{
		$this->accountRepository = $transactionAccountRepositoryInterface;
	}

	public function handle(array $inputs, TransactionAccount $account, User $updater)
	{
		$account->fill($inputs);

		$this->accountRepository->save($account);

		return $account;
	}
}