<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\TransactionSystem\Repositories\TransactionAccountRepositoryInterface;
use SourceScript\TransactionSystem\Repositories\TransactionCategoryRepositoryInterface;
use User;
use TransactionCategory;

class UpdateCategory {

	use ValidableTrait;

	/**
     * For logging
     * 
     * @var string
     */
    public $historable = 'update_resource';

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'name' => 'required'
	];


	/**
	 * @var TransactionCategoryRepositoryInterface
	 */
	private $categoryRepository;


	function __construct(TransactionCategoryRepositoryInterface $transactionCategoryRepositoryInterface)
	{
		$this->categoryRepository = $transactionCategoryRepositoryInterface;
	}


	/**
	 * @param  array               $inputs
	 * @param  TransactionCategory $category
	 * @param  User                $updater
	 * @return TransactionCategory
	 */
	public function handle(array $inputs, TransactionCategory $category, User $updater)
	{
		$category->fill($inputs);

		$category->modified_by = $updater->id;

		$this->categoryRepository->save($category);

		return $category;
	}

}