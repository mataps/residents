<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\Common\Exceptions\ValidationException;
use SourceScript\TransactionSystem\Repositories\LiquidationRepositoryInterface;
use Liquidation;
use User;
use Transaction;

class UpdateLiquidation {

	use ValidableTrait;

	/**
     * For logging
     * 
     * @var string
     */
    public $historable = 'update_resource';

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'label' => 'required',
		'amount' => 'required|numeric'
	];


	/**
	 * @var LiquidationRepositoryInterface
	 */
	private $liquidationRepository;


	function __construct(LiquidationRepositoryInterface $liquidationRepositoryInterface)
	{
		$this->liquidationRepository = $liquidationRepositoryInterface;
	}


	/**
	 * @param  array       $inputs
	 * @param  Transaction $transaction
	 * @param  Liquidation $liquidation
	 * @param  User        $updater
	 * @return Liquidation
	 */
	public function handle(array $inputs, Transaction $transaction, Liquidation $liquidation, User $updater)
	{
		$liquidations = $this->liquidationRepository->findLiquidationsByTransaction($transaction);

		$liquidations = $liquidations->keyBy('id');

		$liquidations->forget($liquidation->id);

		if(($liquidations->sum('amount') + $inputs['amount']) > $transaction->amount) throw new ValidationException(['Liquidations' => 'Liquidation amount invalid']);

		$liquidation->fill($inputs);

		$this->liquidationRepository->save($liquidation);

		return $liquidation;
	}

}