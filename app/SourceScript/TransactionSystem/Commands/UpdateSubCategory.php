<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\TransactionSystem\Repositories\TransactionSubCategoryRepositoryInterface;
use User;
use TransactionSubCategory;

class UpdateSubCategory {

	use ValidableTrait;


	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'name' => 'required',
		'category_id' => 'required|exists:transaction_categories,id'
	];


	/**
	 * @var TransactionSubCategoryRepositoryInterface
	 */
	private $repository;


	function __construct(TransactionSubCategoryRepositoryInterface $transactionSubCategoryRepository)
	{
		$this->repository = $transactionSubCategoryRepository;
	}


	/**
	 * @param  array                  $inputs
	 * @param  TransactionSubCategory $subCategory
	 * @param  User                   $updater
	 * @return TransactionSubCategory
	 */
	public function handle(array $inputs, TransactionSubCategory $subCategory, User $updater)
	{
		$subCategory->fill($inputs);

		$subCategory->modified_by = $updater->id;

		$this->repository->save($subCategory);

		return $subCategory;
	}

}