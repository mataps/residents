<?php namespace SourceScript\TransactionSystem\Commands;

use SourceScript\TransactionSystem\Factories\TransactionItemFactory;
use SourceScript\TransactionSystem\Repositories\TransactionRepositoryInterface;
use SourceScript\Common\Validations\ValidableTrait;
use Transaction;
use User;
use SourceScript\TransactionSystem\TransactionSystemInterface;
use SourceScript\Common\Exceptions\ValidationException;

class UpdateTransaction {

	use ValidableTrait;

	/**
     * For loggingte              
     * 
     * @var string
     */
    public $historable = 'update_resource';

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'account_type_id'   => 'required|exists:transaction_accounts,id',
        'sub_category_id'   => 'required|exists:transaction_sub_categories,id',
        'item_name'         => 'required',
        'amount'            => 'required|numeric|max:10000000|min:0',
        'transaction_type'  => 'required|in:expense,income,non-monetary,transfer-income,transfer-expense',
        'details'           => 'required',
        'voucher_id' 		=> 'required',
        'referrer_id' 		=> 'required',
        'client_id' 		=> 'exists:residents,id',
        'client_type' 		=> 'required|in:Resident,Affiliation',
        'beneficiary_id' 	=> 'required',
        'beneficiary_type' 	=> 'required|in:Resident,Affiliation',
        'liquidatable' 		=> 'boolean',
        'transfer_account_id' => 'exists:transaction_accounts,id',
        'created_at' => 'date'
	];

	/**
	 * @var TransactionRepositoryInterface
	 */
	private $repository;


	/**
	 * @var TransactionItemFactory
	 */
	private $transactionItemFactory;


	function __construct(
		TransactionRepositoryInterface $transactionRepositoryInterface,
		TransactionItemFactory $transactionItemFactory,
		TransactionSystemInterface $transactionSystemService)
	{
		$this->transactionSystemService = $transactionSystemService;
		$this->repository = $transactionRepositoryInterface;
        $this->transactionItemFactory = $transactionItemFactory;
	}


	/**
	 * @param  array       $inputs
	 * @param  Transaction $transaction
	 * @param  User        $updater
	 * @return Transaction
	 */
	public function handle(array $inputs, Transaction $transaction, User $updater)
	{
        $account = $transaction->account_type_id;
		// if(!is_null($transaction->settled_at)) throw new ValidationException(["error" => "Transaction already settled"]);

		$transaction->fill(array_except($inputs, ['item_name', 'account_type_id', 'created_at', 'client_id	']));

		$transaction->item_id = $this->transactionItemFactory->create($inputs['item_name'], $updater);

		$transaction->modified_by = $updater->id;

		$transaction->client_id = $transaction->voucher->client_id;

		if($inputs['client_id'])
		{
			$voucher = $transaction->voucher;
			$voucher->client_id = $inputs['client_id'];

			foreach($voucher->transactions as $tr)
			{
				$tr->client_id = $inputs['client_id'];
				$tr->save();
				$this->transactionSystemService->execute('IndexTransaction', [], $tr);
			}
			$voucher->save();
		}

		if($inputs['created_at'] !== $transaction->created_at->__toString())
        {
            $transaction->timestamps = false;
            $transaction->setCreatedAt(date('Y-m-d H:i:s', strtotime($inputs['created_at'])));
            $transaction->setUpdatedAt(date('Y-m-d H:i:s', strtotime($inputs['created_at'])));

            $voucher = $transaction->voucher;
            $voucher->setCreatedAt(date('Y-m-d H:i:s', strtotime($inputs['created_at'])));
            $voucher->setUpdatedAt(date('Y-m-d H:i:s', strtotime($inputs['created_at'])));

            $voucher->save();
            $voucher->generateUuid();

            foreach($voucher->transactions as $tr)
            {
				$this->transactionSystemService->execute('IndexTransaction', $inputs, $tr);
            }
        }

		$this->repository->save($transaction);

		if($inputs['created_at'] !== $transaction->created_at->__toString() || $inputs['account_type_id'] !== $account)
		{
        	$transaction->generateUuid();	
		}


		return $transaction;
	}
}