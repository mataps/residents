<?php namespace SourceScript\TransactionSystem\Factories;

use TransactionItem;

class TransactionItemFactory {

	/**
	 * @param  $transactionItemName
	 * @param  $creator
	 * @return integer
	 */
	public function create($transactionItemName, $creator)
	{
		$transactionItem = TransactionItem::firstOrNew(array('name' => $transactionItemName));

		$transactionItem->created_by = $creator->id;
		$transactionItem->modified_by = $creator->id;

		$transactionItem->save();

		return $transactionItem->id;
	}
}