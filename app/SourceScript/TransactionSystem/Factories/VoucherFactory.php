<?php namespace SourceScript\TransactionSystem\Factories;

use SourceScript\TransactionSystem\Repositories\VoucherRepositoryInterface;
use Voucher;
use User;

class VoucherFactory {

	/**
	 * @var VoucherRepositoryInterface
	 */
	private $repository;


	function __construct(VoucherRepositoryInterface $voucherRepositoryInterface)
	{
		$this->repository = $voucherRepositoryInterface;
	}


	/**
	 * @param  array  $inputs
	 * @param  User   $creator
	 * @return Voucher
	 */
	public function create(array $inputs, User $creator)
	{
		if(!isset($inputs['voucher_id']) || ! Voucher::find($inputs['voucher_id']) || ! $inputs['voucher_id'])
		{
			$voucher = new Voucher;

			$voucher->client_id         = $inputs['client_id'];
            $voucher->client_type       = "Resident";
            $voucher->account_type_id   = $inputs['account_type_id'];
            $voucher->created_by        = $creator->id;
            $voucher->modified_by       = $creator->id;

            if(isset($inputs['created_at']))
            {
            	$voucher->timestamps = false;
							$voucher->setCreatedAt(date('Y-m-d H:i:s', strtotime($inputs['created_at'])));
							$voucher->setUpdatedAt(date('Y-m-d H:i:s', strtotime($inputs['created_at'])));
            }

            $voucher->save();

            $voucher->generateUuid();

            return $voucher;
		}
		else
		{
			return Voucher::find($inputs['voucher_id']);
		}
	}
}
