<?php namespace SourceScript\TransactionSystem\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use Liquidation;
use Transaction;

class LiquidationRepository implements LiquidationRepositoryInterface {


	/**
	 * @var GenericFinderInterface
	 */
	private $finder;


	function __construct(GenericFinderInterface $finder)
	{
		$this->finder = $finder;
	}


	/**
	 * @param  integer $id
	 * @throws DbException
	 * @return Transaction
	 */
	public function findOneById($id)
	{
		$query = Liquidation::where('id', $id);

		$result = $this->finder->findOne($query);

		if($result) return $result;

		throw new DbException('Liquidation not found!');
	}


	/**
	 * @param  Liquidation $liquidation
	 * @throws DbException
	 * @return Liquidation
	 */
	public function save(Liquidation $liquidation)
	{
		if($liquidation->save()) return $liquidation;

		throw new DbException('Liquidation not saved!');
	}


	/**
	 * @param  Liquidation $liquidation
	 * @throws DbException
	 */
	public function delete(Liquidation $liquidation)
	{
		if(!$liquidation->delete()) throw new DbException('Liquidation not deleted!');
	}


	/**
     * @param $ids
     * @return mixed
     */
	public function findLiquidations($ids)
	{
		$ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return Liquidation::whereIn('id', $ids)->get();
	}


	/**
     * @param array $liquidations
     * @throws DbException
     */
    public function deleteLiquidations(array $liquidations)
    {
    	$deleted = Liquidation::destroy($liquidations);

    	if(!$deleted) throw new DbException('Liquidations not deleted!');
    }


    /**
     * @param  Transaction $transaction
     * @return Collection
     */
    public function findLiquidationsByTransaction(Transaction $transaction)
    {
    	$query = Liquidation::where('transaction_id', $transaction->id);

    	$result = $this->finder->query($query);

    	return $result;
    }

}