<?php namespace SourceScript\TransactionSystem\Repositories;

use Liquidation;
use Transaction;

interface LiquidationRepositoryInterface {

	/**
	 * @param  integer $id
	 * @throws DbException
	 * @return Transaction
	 */
	public function findOneById($id);


	/**
	 * @param  Liquidation $liquidation
	 * @throws DbException
	 * @return Liquidation
	 */
	public function save(Liquidation $liquidation);


	/**
	 * @param  Liquidation $liquidation
	 * @throws DbException
	 */
	public function delete(Liquidation $liquidation);


	/**
     * @param $ids
     * @return mixed
     */
	public function findLiquidations($ids);


	/**
     * @param array $liquidations
     * @throws DbException
     */
    public function deleteLiquidations(array $liquidations);


    /**
     * @param  Transaction $transaction
     * @return Collection
     */
    public function findLiquidationsByTransaction(Transaction $transaction);
}