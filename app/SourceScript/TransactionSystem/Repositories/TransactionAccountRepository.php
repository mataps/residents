<?php namespace SourceScript\TransactionSystem\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use TransactionAccount;

class TransactionACcountRepository implements TransactionAccountRepositoryInterface {

    /**
	 * @var GenericFinderInterface
	 */
	private $finder;


	function __construct(GenericFinderInterface $finder)
	{
		$this->finder = $finder;
	}

	/**
	 * @param  integer $id
	 * @throws DBException
	 * @return TransactionAccount
	 */
	public function findOneById($id)
	{
		$query = TransactionAccount::where('id', $id);

		$result = $this->finder->findOne($query);

		if ($result)
			return $result;

		throw new DbException('Account not found!');
	}

	/**
	 * @param  TransactionAccount $account
	 * @throws DbException
	 * @return TransactionAccount
	 */
	public function save(TransactionAccount $account)
	{
		if ($account->save())
			return $account;

		throw new DbException('Account not saved!');
	}

	/**
	 * @param  TransactionAccount $account
	 * @throws DbException
	 */
	public function delete(TransactionAccount $account)
	{
		if ( ! $account->delete())
			throw new DbException('Account not deleted!');
	}

	/**
     * @param $ids
     * @return mixed
     */
    public function findAccounts($ids)
    {
        $ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return TransactionAccount::whereIn('id', $ids)->get();
    }


 	public function deleteAccounts(array $accounts)
 	{
 		$deleted = TransactionAccount::destroy($accounts);

 		if(! $deleted)
 		{
 			throw new DbException("Accounts not deleted");
 			
 		}
 	}
}