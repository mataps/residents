<?php namespace SourceScript\TransactionSystem\Repositories;

use TransactionAccount;

interface TransactionAccountRepositoryInterface {


	/**
	 * @param  integer $id
	 * @throws DBException
	 * @return TransactionAccount
	 */
	public function findOneById($id);

	/**
	 * @param  TransactionAccount $award
	 * @throws DbException
	 * @return TransactionAccount
	 */
	public function save(TransactionAccount $account);

	/**
	 * @param  TransactionAccount $award
	 * @throws DbException
	 */
	public function delete(TransactionAccount $account);
}