<?php namespace SourceScript\TransactionSystem\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use TransactionCategory;

class TransactionCategoryRepository implements TransactionCategoryRepositoryInterface {

    /**
	 * @var GenericFinderInterface
	 */
	private $finder;


	function __construct(GenericFinderInterface $finder)
	{
		$this->finder = $finder;
	}

	/**
	 * @param  integer $id
	 * @throws DBException
	 * @return TransactionCategory
	 */
	public function findOneById($id)
	{
		$query = TransactionCategory::where('id', $id);

		$result = $this->finder->findOne($query);

		if ($result)
			return $result;

		throw new DbException('Category not found!');
	}

	/**
	 * @param  TransactionCategory $category
	 * @throws DbException
	 * @return TransactionCategory
	 */
	public function save(TransactionCategory $category)
	{
		if ($category->save())
			return $category;

		throw new DbException('Category not saved!');
	}

	/**
	 * @param  TransactionCategory $category
	 * @throws DbException
	 */
	public function delete(TransactionCategory $category)
	{
		if ( ! $category->delete())
			throw new DbException('Category not deleted!');
	}

	public function deleteCategories(array $categories)
	{
		$deleted = TransactionCategory::destroy($categories);

        if ( ! $deleted)
        {
            throw new DbException('Category not deleted!');
        }	
	}

	/**
     * @param $ids
     * @return mixed
     */
    function findCategories($ids)
    {
        $ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return TransactionCategory::whereIn('id', $ids)->get();
    }

    public function firstOrCreate($data = [])
    {
    	return TransactionCategory::firstOrCreate($data);
    }
}