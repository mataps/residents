<?php namespace SourceScript\TransactionSystem\Repositories;

use TransactionCategory;

interface TransactionCategoryRepositoryInterface {


	/**
	 * @param  integer $id
	 * @throws DBException
	 * @return TransactionCategory
	 */
	public function findOneById($id);

	/**
	 * @param  TransactionCategory $award
	 * @throws DbException
	 * @return TransactionCategory
	 */
	public function save(TransactionCategory $category);

	/**
	 * @param  TransactionCategory $award
	 * @throws DbException
	 */
	public function delete(TransactionCategory $category);
}