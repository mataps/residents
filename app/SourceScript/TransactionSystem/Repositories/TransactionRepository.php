<?php namespace SourceScript\TransactionSystem\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use Transaction;
use Resident;

class TransactionRepository implements TransactionRepositoryInterface {

    function __construct(GenericFinderInterface $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param $id
     * @throws DbException
     * @return Transaction
     */
    function findOneById($id)
    {
        $query = Transaction::where('id', $id);

        $result = $this->finder->findOne($query);

        if ($result)
        {
            return $result;
        }

        throw new DbException('Transaction not found!');
    }

    /**
     * @param Transaction $transaction
     * @return Transaction
     * @throws DbException
     */
    function save(Transaction $transaction)
    {
        if ($transaction->save())
        {
            return $transaction;
        }

        throw new DbException('Transaction not saved!');
    }

    /**
     * @param Transaction $transaction
     * @throws DbException
     */
    function delete(Transaction $transaction)
    {
        if (! $transaction->delete())
        {
            throw new DbException('Transaction not deleted!');
        }
    }

    function findSettledTransactions($ids)
    {
        $ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return Transaction::whereIn('id', $ids)->whereNotNull('settled_at')->get();   
    }

    public function findTransactions($ids)
    {
        $ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return Transaction::whereIn('id', $ids)->whereNull('settled_at')->get();
    }

    public function deleteTransactions(array $transactions)
    {
        $deleted = Transaction::destroy($transactions);
        
        if(!$deleted)
        {
            throw new DbException('Transactions not deleted');  
        }   
    }


    /**
     * @param $id
     * @throws DbException
     * @return Transaction
     */
    function findResidentById($id)
    {
        $query = Resident::where('id', $id);

        $result = $this->finder->findOne($query);

        if ($result)
        {
            return $result;
        }

        throw new DbException('Resident not found!');
    }
}