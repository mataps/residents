<?php namespace SourceScript\TransactionSystem\Repositories;

use Transaction;

interface TransactionRepositoryInterface {

    /**
     * @param $id
     * @return Transaction
     */
    function findOneById($id);

    /**
     * @param Transaction $transaction
     * @return Transaction
     */
    function save(Transaction $transaction);

    /**
     * @param Transaction $transaction
     * @return void
     */
    function delete(Transaction $transaction);
}