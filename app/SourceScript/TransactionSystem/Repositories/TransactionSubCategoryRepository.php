<?php namespace SourceScript\TransactionSystem\Repositories;


use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use TransactionSubCategory;

class TransactionSubCategoryRepository implements TransactionSubCategoryRepositoryInterface {

    /**
	 * @var GenericFinderInterface
	 */
	private $finder;


	function __construct(GenericFinderInterface $finder)
	{
		$this->finder = $finder;
	}

	/**
	 * @param  integer $id
	 * @throws DBException
	 * @return TransactionSubCategory
	 */
	public function findOneById($id)
	{
		$query = TransactionSubCategory::where('id', $id);

		$result = $this->finder->findOne($query);

		if ($result)
			return $result;

		throw new DbException('Category not found!');
	}

	/**
	 * @param  TransactionSubCategory $category
	 * @throws DbException
	 * @return TransactionSubCategory
	 */
	public function save(TransactionSubCategory $category)
	{
		if ($category->save())
			return $category;

		throw new DbException('Category not saved!');
	}

	/**
	 * @param  TransactionSubCategory $category
	 * @throws DbException
	 */
	public function delete(TransactionSubCategory $category)
	{
		if ( ! $category->delete())
			throw new DbException('Category not deleted!');
	}

	public function deleteSubCategories(array $subCategories)
	{
		$deleted = TransactionSubCategory::destroy($subCategories);

        if ( ! $deleted)
        {
            throw new DbException('Sub category not deleted!');
        }	
	}

	/**
     * @param $ids
     * @return mixed
     */
    function findSubCategories($ids)
    {
        $ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return TransactionSubCategory::whereIn('id', $ids)->get();
    }
}