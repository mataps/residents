<?php namespace SourceScript\TransactionSystem\Repositories;

use TransactionSubCategory;

interface TransactionSubCategoryRepositoryInterface {


	/**
	 * @param  integer $id
	 * @throws DBException
	 * @return TransactionSubCategory
	 */
	public function findOneById($id);

	/**
	 * @param  TransactionSubCategory $award
	 * @throws DbException
	 * @return TransactionSubCategory
	 */
	public function save(TransactionSubCategory $category);

	/**
	 * @param  TransactionSubCategory $award
	 * @throws DbException
	 */
	public function delete(TransactionSubCategory $category);
}