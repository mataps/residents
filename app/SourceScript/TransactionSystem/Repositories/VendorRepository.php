<?php namespace SourceScript\TransactionSystem\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use TransactionVendor;
use SourceScript\Profiling\Repositories\DistrictRepositoryInterface;
use SourceScript\Profiling\Repositories\BarangayRepositoryInterface;
use SourceScript\Profiling\Repositories\CityMunicipalityRepositoryInterface;

class VendorRepository implements VendorRepositoryInterface {

    /**
     * @var GenericFinderInterface
     */
    private $finder;

    /**
     * @var DistrictRepositoryInterface
     */
    private $districtRepository;

    /**
     * @var BarangayRepositoryInterface
     */
    private $barangayRepository;
    
    /**
     * @var CityMunicipalityRepositoryInterface
     */
    private $cityMunicipalityRepository;

    function __construct(GenericFinderInterface $finder,
        DistrictRepositoryInterface $districtRepository,
        BarangayRepositoryInterface $barangayRepository,
        CityMunicipalityRepositoryInterface $cityMunicipalityRepository)
    {
        $this->finder = $finder;
        $this->districtRepository   = $districtRepository;
        $this->barangayRepository   = $barangayRepository;
        $this->cityMunicipalityRepository = $cityMunicipalityRepository;
    }

    /**
     * @param $id
     * @throws DbException
     * @return TransactionVendor
     */
    function findOneById($id)
    {
        $query = TransactionVendor::where('id', $id);

        $result = $this->finder->findOne($query);

        if ($result)
        {
            return $result;
        }

        throw new DbException('Transaction Vendor not found!');
    }

    /**
     * @param TransactionVendor $TransactionVendor
     * @return TransactionVendor
     * @throws DbException
     */
    function save(TransactionVendor $transactionVendor)
    {
        $district = $transactionVendor->removeAttribute('district');
        $barangay = $transactionVendor->removeAttribute('barangay');
        $cityMunicipality = $transactionVendor->removeAttribute('cityMunicipality');

        if ($district)
        {
            $transactionVendor->district_id = $district->id ?: $this->districtRepository->save($district)->id;
        }

        if ($barangay)
        {
            $transactionVendor->barangay_id = $barangay->id ?: $this->barangayRepository->save($barangay)->id;
        }

        if ($cityMunicipality)
        {
            $transactionVendor->city_municipality_id = $cityMunicipality->id ?: $this->cityMunicipalityRepository->save($cityMunicipality)->id;
        }

        if ($transactionVendor->save())
        {
            return $transactionVendor;
        }

        throw new DbException('Transaction Vendor not saved!');
    }

    /**
     * @param TransactionVendor $TransactionVendor
     * @throws DbException
     */
    function delete(TransactionVendor $transactionVendor)
    {
        if (! $transactionVendor->delete())
        {
            throw new DbException('Transaction Vendor not deleted!');
        }
    }

    /**
     * @param array $residents
     * @throws DbException
     */
    function deleteVendors(array $vendors)
    {
        $deleted = TransactionVendor::destroy($vendors);

        if ( ! $deleted)
        {
            throw new DbException('Vendors not deleted!');
        }
    }

    /**
     * @param $ids
     * @return mixed
     */
    function findVendors($ids)
    {
        $ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return TransactionVendor::whereIn('id', $ids)->get();
    }
}