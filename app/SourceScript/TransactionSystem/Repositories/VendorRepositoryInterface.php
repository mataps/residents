<?php namespace SourceScript\TransactionSystem\Repositories;

use TransactionVendor;

interface VendorRepositoryInterface {

    /**
     * @param $id
     * @return TransactionVendor
     */
    function findOneById($id);

    /**
     * @param TransactionVendor $transactionVendor
     * @return TransactionVendor
     */
    function save(TransactionVendor $transactionVendor);

    /**
     * @param TransactionVendor $transactionVendor
     * @return void
     */
    function delete(TransactionVendor $transactionVendor);
}