<?php namespace SourceScript\TransactionSystem\Repositories;

use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use Voucher;

class VoucherRepository implements VoucherRepositoryInterface {

    function __construct(GenericFinderInterface $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param $id
     * @throws DbException
     * @return Voucher
     */
    function findOneById($id)
    {
        $query = Voucher::where('id', $id);

        $result = $this->finder->findOne($query);

        if ($result)
        {
            return $result;
        }

        throw new DbException('Voucher not found!');
    }

    /**
     * @param Voucher $voucher
     * @return Voucher
     * @throws DbException
     */
    function save(Voucher $voucher)
    {
        if ($voucher->save())
        {
            return $voucher;
        }

        throw new DbException('Voucher not saved!');
    }

    /**
     * @param Voucher $voucher
     * @throws DbException
     */
    function delete(Voucher $voucher)
    {
        if ( ! $voucher->permissions()->detach() && ! $voucher->delete())
        {
            throw new DbException('Voucher not deleted!');
        }
    }
}