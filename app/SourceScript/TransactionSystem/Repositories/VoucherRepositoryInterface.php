<?php namespace SourceScript\TransactionSystem\Repositories;

use Voucher;

interface VoucherRepositoryInterface {


	/**
	 * @param  integer $id
	 * @throws DBException
	 * @return TransactionCategory
	 */
	public function findOneById($id);

	/**
	 * @param  TransactionCategory $award
	 * @throws DbException
	 * @return TransactionCategory
	 */
	public function save(Voucher $category);

	/**
	 * @param  TransactionCategory $award
	 * @throws DbException
	 */
	public function delete(Voucher $category);
}