<?php namespace SourceScript\TransactionSystem;

use SourceScript\Common\AbstractProvider;

class TransactionSystemProvider extends AbstractProvider
{
    protected $modules = array(
        'Transaction' => array(
            'repository' => 'TransactionRepository',
            'validations' => array(),
            'bindings' => array(
                'unsettledTransactions' => 'findTransactions',
                'transactions'  => 'findOneById',
                'residents'     => 'findResidentById',
                'SettledTransactions' => 'findSettledTransactions'
            )
        ),
        'TransactionAccount' => array(
            'repository' => 'TransactionAccountRepository',
            'validations' => array(),
            'bindings' => array(
                'accounts' => 'findOneById',
                'Accounts' => 'findAccounts'
                )
        ),
        'TransactionCategory' => array(
            'repository' => 'TransactionCategoryRepository',
            'validations' => array(),
            'bindings' => array(
                'categories' => 'findOneById',
                'Categories' => 'findCategories'
            )
        ),
        'TransactionSubCategory' => array(
            'repository' => 'TransactionSubCategoryRepository',
            'validations' => array(),
            'bindings' => array(
                'sub_categories' => 'findOneById',
                'Sub_Categories' => 'findSubCategories'
            )
        ),
        'Voucher' => array(
            'repository' => 'VoucherRepository',
            'bindings' => array(
                'vouchers' => 'findOneById'
                )
            ),
        'Liquidation' => array(
            'repository' => 'LiquidationRepository',
            'validations' => array(),
            'bindings' => array(
                'liquidations' => 'findOneById',
                'Liquidations' => 'findLiquidations'
                )
            ),
        'Vendor' => array(
            'repository' => 'VendorRepository',
            'bindings' => array(
                'vendors' => 'findOneById',
                'Vendors' => 'findVendors'
                )
            )
    );

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }
}