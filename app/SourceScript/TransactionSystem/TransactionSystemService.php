<?php namespace SourceScript\TransactionSystem;

use SourceScript\Common\Exceptions\ValidationException;
use SourceScript\Common\Collections\ResultCollection;
use SourceScript\Common\AbstractService;
use SourceScript\Common\Paginators\FilterableTrait;
use SourceScript\Common\Paginators\PaginatableTrait;
use SourceScript\Common\Paginators\SortableTrait;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Common\Repositories\GenericFinderInterface;
use Transaction;
use TransactionAccount;
use TransactionCategory;
use TransactionSubCategory;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Resident;
use TransactionItem;
use Household;
use Voucher;
use Elasticsearch\Client;
use Elasticsearch\Common\Exceptions\Curl\CouldNotConnectToHost;
use Elasticsearch\Common\Exceptions\BadRequest400Exception;
use TransactionVendor;
use Log;

class TransactionSystemService extends AbstractService implements TransactionSystemInterface{

    use PaginatableTrait, SortableTrait, FilterableTrait;

    function __construct(GenericFinderInterface $genericRepository)
    {
        $this->repository = $genericRepository;
    }

    /**
     * Finds Categories
     * 
     * @param  date  $from
     * @param  date  $to
     * @param  mixed  $accountTypes
     * @param  boolean $summary
     * @return TransactionCategory
     */
    function getReportData($from = null, $to = null, $accountTypes = null, $summary = false, $transactionType = null, $liquidation = false)
    {
        if(!$to)
            $to = Carbon::now()->setTime(23,59,59);
        else
            $to = Carbon::createFromTimeStamp(strtotime($to))->setTime(23,59,59);

        if($from)
            $from = Carbon::createFromTimeStamp(strtotime($from))->setTime(0,0,0);

        if(is_null($accountTypes))
            $accountTypeNames = TransactionAccount::lists('name');
        else
            $accountTypeNames = TransactionAccount::whereIn('id', explode(',', $accountTypes))->lists('name');

        $query = TransactionCategory::
                with([
                    'subCategories',
                    'subCategories.transactions' => function($q) use ($from, $to, $accountTypes, $transactionType, $liquidation)
                        {
                            $q->whereNotNull('transactions.settled_at');

                            if($transactionType)
                            {
                                if($transactionType == "income" || $transactionType == "transfer-income")
                                    $q->whereIn('transactions.transaction_type', ['income', 'transfer-income']);

                                if($transactionType == "expense" || $transactionType == "transfer-expense")
                                    $q->whereIn('transactions.transaction_type', ['expense', 'transfer-expense']);
                            }
                            
                            $q->orderBy('item_id');

                            if($from && $to)
                            {
                                $q->whereBetween('transactions.created_at', [$from, $to]);
                            }

                            if($accountTypes)
                            {
                                $q->leftJoin('vouchers', 'transactions.voucher_id', '=', 'vouchers.id');

                                $q->whereIn('vouchers.account_type_id', explode(',', $accountTypes));
                            }

                            if($liquidation)
                            {
                                $q->whereNotNull('liquidated_at');

                                $q->where('liquidatable', true);  
                            }
                        },
                    'subCategories.transactions.liquidations',
                    'subCategories.transactions.item',
                    'subCategories.transactions.client.barangay',
                    'subCategories.transactions.beneficiary.barangay',
                    'subCategories.transactions.client.district',
                    'subCategories.transactions.beneficiary.district',
                    'transactions' => function($q) use ($from, $to, $accountTypes, $transactionType, $liquidation)
                    {
                        $q->whereNotNull('transactions.settled_at');

                        if($transactionType)
                        {
                            if($transactionType == "income" || $transactionType == "transfer-income")
                                $q->whereIn('transactions.transaction_type', ['income', 'transfer-income']);

                            if($transactionType == "expense" || $transactionType == "transfer-expense")
                                $q->whereIn('transactions.transaction_type', ['expense', 'transfer-expense']);
                        }
   
                        $q->orderBy('item_id');

                        if($from && $to)
                        {
                            $q->whereBetween('transactions.created_at', [$from, $to]);
                        }

                        if($accountTypes)
                        {
                            $q->leftJoin('vouchers', 'transactions.voucher_id', '=', 'vouchers.id');

                            $q->whereIn('vouchers.account_type_id', explode(',', $accountTypes));
                        }

                        if($liquidation)
                        {
                            $q->whereNotNull('liquidated_at');

                            $q->where('liquidatable', true);  
                        }
                    }
                    ]);

        $transactions = Transaction::query();

        $liquidationTotal = 0;

        $transactions->whereNotNull('settled_at');

        if($from && $to)
            $transactions->whereBetween('created_at', [$from, $to]);

        if( ! is_null($accountTypes) )
        {
            $transactions->whereHas('voucher', function($q) use ($accountTypes)
            {
                if($accountTypes)
                    $q->whereIn('account_type_id', explode(',', $accountTypes));
            });
        }


        if($transactionType)
        {
            if($transactionType == "income" || $transactionType == "transfer-income")
                $transactions->whereIn('transactions.transaction_type', ['income', 'transfer-income']);

            if($transactionType == "expense" || $transactionType == "transfer-expense")
                $transactions->whereIn('transactions.transaction_type', ['expense', 'transfer-expense']);
        }

        if($liquidation)
        {
            $transactions->whereNotNull('liquidated_at');

            $transactions->where('liquidatable', true);


            foreach($transactions->get() as $transaction)
            {
              $liquidationTotal = $liquidationTotal + $transaction->liquidations->sum('amount');
            }
        }


        $title = implode(', ', $accountTypeNames);

        $title .= $summary ? ' Cash Position Report' : ' Detailed Cash Position Report';
 
        $data = array(
            'signatory_1' => $transactions->first() ? $transactions->first()->account->signatory : '',
            'label_1' => $transactions->first() ? $transactions->first()->account->label : '',
            'signatory_2' => $transactions->first() ? $transactions->first()->account->signatory_2 : '',
            'label_2' => $transactions->first() ? $transactions->first()->account->label_2 : '',
            'title' => $title,
            'transactions' => $query->get(),
            'total' => $transactions->sum('amount'),
            'liquidated' => $liquidationTotal,
            'from' => $from ? $from->format('l, F d Y') : 'Start',
            'to' => $to->format('l, F d Y')
            );

        return $data;
    }


    /**
     * Get available cash on given date
     * 
     * @param  string $date
     * @param  mixed $accountTypes
     * @return decimal
     */
    function getAvailableCash($from, $accountTypes = null, $transfer = false)
    {
        if($from)
            $from = Carbon::createFromTimeStamp(strtotime($from))->setTime(0, 0, 0);

        if($transfer)
        {
            $totalIncome = Transaction::query()->whereHas('voucher', function($q) use ($accountTypes)
            {
                if( ! is_null($accountTypes) )
                    $q->whereIn('account_type_id', explode(',', $accountTypes));
            })->where('transaction_type', 'transfer-income')->whereNotNull('settled_at')->where('created_at', '<', $from)->sum('amount');


            $totalExpense = Transaction::query()->whereHas('voucher', function($q) use ($accountTypes)
            {
                if( ! is_null($accountTypes) )
                    $q->whereIn('account_type_id', explode(',', $accountTypes));
            })->where('transaction_type', 'transfer-expense')->whereNotNull('settled_at')->where('created_at', '<', $from)->sum('amount');

            return $totalIncome - $totalExpense;
        }
        else
        {
            $totalIncome = Transaction::query()->whereHas('voucher', function($q) use ($accountTypes)
            {
                if( ! is_null($accountTypes) )
                    $q->whereIn('account_type_id', explode(',', $accountTypes));
            })->whereIn('transaction_type', ['income', 'transfer-income'])->whereNotNull('settled_at')->where('created_at', '<', $from)->sum('amount');


            $totalExpense = Transaction::query()->whereHas('voucher', function($q) use ($accountTypes)
            {
                if( ! is_null($accountTypes) )
                    $q->whereIn('account_type_id', explode(',', $accountTypes));
            })->whereIn('transaction_type', ['expense', 'transfer-expense'])->whereNotNull('settled_at')->where('created_at', '<', $from)->sum('amount');

            return $totalIncome - $totalExpense;
        }
    }


    /**
     * @param  Transaction               $transaction
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return Query
     */
    function findLiquidationsByTransaction(
        Transaction $transaction,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null
        )
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $transaction->liquidations()->getQuery();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  TransactionAccount             $account
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findTransactionsByAccount(
        TransactionAccount      $account,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $account->transactions()->getQuery();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }

    /**
     * @param  Voucher              $voucher
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findTransactionsByVoucher(
        Voucher                 $voucher,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $voucher->transactions()->getQuery();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }



    /**
     * @param  ResultCollection             $accounts
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findTransactionsByAccounts(
        ResultCollection $accounts,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;

        $query = Transaction::query();

        $accounts = $accounts->lists('id');

        $query->whereIn('account_type_id', $accounts);

        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }

    /**
     * @param  Resident             $resident
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findTransactionsByResident(
        Resident $resident,
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = $resident->transactions()->getQuery();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }

    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findTransactions(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null,
        $permissions = null
        )
    {
        //Log::info("liquidatable Maria");
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = Transaction::query();
               
        $query = $query->whereHas('voucher', function($query) use ($permissions){
            $query->whereIn('account_type_id', $permissions);
        });

        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);



        $result = $this->repository->query($query);

        $result->setCountRows($countRows);


        return $result;
    }

    public function findLiquidatableCount($permissions = null)
    {
        $query = Transaction::query();

        $query = $query->whereHas('voucher', function($query) use ($permissions){
            $query->whereIn('account_type_id', $permissions);
        });

        $query->where('liquidatable', true);

        $query->whereNull('liquidated_at');

        $result = $query->count();

        return $result;
    }


    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findVendors(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null
        )
    {
        $query = TransactionVendor::query();

        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);

        $result = $this->repository->query($query);

        $result->setCountRows($countRows);


        return $result;
    }

    /**
     *  Find Transaction ES
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findTransactionsES(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null,
        $permissions = null
        )
    {
        // dd($permissions);
        // $permissions
          ### Elastic Search Part #######
        $filters = $filterParameters->getFilters();
        // dd($filters);
        // array_walk($filters, function(&$value, $key) { $value = strtolower($value); });
        $param = \Config::get('elasticsearch');
        $client = new Client($param);

        $search_filters = [];

        foreach ($filters as $key => $value) {
            if($key == "sort") {
                continue;
            }

           if(is_array($value)){
                if(isset($value['from'])){
               
                $search_filters[] = array(
                        'range' => array( $key => array(
                                'gte' => date('Y-m-d', strtotime($value['from'])),
                                'lte' => date('Y-m-d', strtotime($value['to']))
                            ))
                    );
                    
                }
             
            }
           else if($key == 'created_at_persist'){
   
              $value = str_replace('"', '', $value);

              $from = date('Y-m-d', strtotime($value));
              // dd($from, $to);
                $search_filters[] = [
                        'range' => [
                                'created_at' => [
                                                'gte' => date('Y-m-d H:i',strtotime($from . " 00:00")),
                                                'lte' => date('Y-m-d H:i',strtotime($from . " 23:00:00")),
                                                'format' => 'yyyy-MM-dd HH:mm'
                                            ]
                        ]
                ];

                // dd($search_filters);

           }
           else if($key == "account_type_id") {
                if($value > 0)
                {
                  $search_filters[] = array(
                    'match' =>  array($key  =>  $value)
                    );                  
                }
           }

           else if($key == "liquidatable"){
                

                if($value == "1" || $value == "0" )
                {                  
                  $search_filters[] = array(
                    'match' =>  array($key  =>  $value)
                    );                  
                }
           }

           else{
                // $value = str_replace(" ", "* ", $value);
                if($value != 'null'){
                    // $search_filters[] = array(
                    //         'simple_query_string' => array(
                    //                 'query' => $value . "*",
                    //                 'default_operator' => 'and',
                    //                 'fields' => [$key]

                    //             )

                    //     );
                $search_item = explode(" ", $value);
                foreach ($search_item as $item) {
                        $search_filters[] = array(
                            'wildcard' => array($key => $item . "*")
                        );
                }
                }               
           }


        }

           $search_filters[] = array(
                'terms' => array(
                        'account_type_id' => $permissions
                    )
            );
        // dd($search_filters);
        $sortParameters = $sortParameters->getSort()[0];
        $sort_key = substr($sortParameters, 1);

        $search_param['body']['sort'] = array(
                array('id' => 'desc')
        );  
        if(!is_null($sort_key) && $sort_key != "id"){
            $starts_with  = substr($sortParameters, 0, 1);
            if($starts_with == "-"){
                    $search_param['body']['sort'] = array(
                        array($sort_key => 'desc')
                    ); 
             }
             else{
                $search_param['body']['sort'] = array(
                        array($sortParameters => 'asc')
                ); 
             }
        }

        if(count($search_filters) > 0){
              $search_param['body']['query']['bool']= array(
                'must'=> $search_filters

            );
        }
        else{            
        }
        $search_param['size'] = $paginationParameters->getLimit();
        $search_param['from'] = $paginationParameters->getFrom();
        $search_param['index'] = 'galactus_production';
        $search_param['type']  = 'transactions';
       
       // dd($search_filters);
        $results = $client->search($search_param);
        // dd($results);
        return $results;         

    }

    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findVouchers(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = Voucher::query();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);

        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }

    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findItems(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = TransactionItem::query();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }    


    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findAccounts(
        $permissions = null,
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = TransactionAccount::query();
        
        $query->whereIn('id', $permissions);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        // $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findCategories(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = TransactionCategory::query();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  FieldParameters      $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return Colection
     */
    function findSubCategories(
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = TransactionSubCategory::query();
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  Household                 $household
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return Query
     */
    public function findVouchersByHousehold(
        Household $household,
        FieldsParameters $fieldsParameters = null,
        PaginationParameters $paginationParameters = null,
        SortParameters $sortParameters = null,
        FilterParameters $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = Voucher::query();

        $residents = $household->residents->modelKeys();

        $query->whereIn('client_id', $residents);

        $query->orWhereHas('transactions', function($q) use ($residents)
        {
            $q->whereIn('beneficiary_id', $residents);
        });
        
        $countRows = $this->repository->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        if($sortParameters)
            $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->repository->query($query);

        $result->setCountRows($countRows);

        return $result;
    }
}
