<?php

namespace SourceScript\UserManagement\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\UserManagement\Factories\GroupFactory;
use SourceScript\UserManagement\Factories\PermissionFactory;
use SourceScript\UserManagement\Repositories\GroupRepositoryInterface;
use Group;

class AddGroup{
    use ValidableTrait;

    protected $rules = array(
        'name'          => 'required|unique:groups,name',
        'permissions'   => 'required'
    );

    protected $messages = array(
        'are_permissions' => 'Invalid permission'
    );

    /**
     * @var GroupRepositoryInterface
     */
    private $groupRepository;
    /**
     * @var PermissionFactory
     */
    private $permissionFactory;
    /**
     * @var GroupFactory
     */
    private $groupFactory;

    /**
     * @param GroupRepositoryInterface $groupRepository
     * @param GroupFactory $groupFactory
     * @param PermissionFactory $permissionFactory
     */
    function __construct(GroupRepositoryInterface $groupRepository, GroupFactory $groupFactory, PermissionFactory $permissionFactory)
    {
        $this->groupRepository      = $groupRepository;
        $this->permissionFactory    = $permissionFactory;
        $this->groupFactory         = $groupFactory;
    }

    /**
     * @param array $inputs
     * @return Group
     */
    function handle(array $inputs)
    {
        $group              = new Group;
        $group->name        = $inputs['name'];
        $group->description = $inputs['description'];
        $group->permissions = $this->permissionFactory->createFromInput($inputs['permissions']);

        $this->groupRepository->save($group);

        return $group;
    }
} 