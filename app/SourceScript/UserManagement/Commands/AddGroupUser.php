<?php namespace Sourcescript\UserManagement\Commands;

use SourceScript\UserManagement\Repositories\UserRepositoryInterface;
use SourceScript\Common\Validations\ValidableTrait;
use Group;
use User;

class AddGroupUser {

	use ValidableTrait;

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'user_id' => 'required|exists:users,id'
	];

	/**
	 * @var UserRepositoryInterface
	 */
	private $userRepository;

	function __construct(UserRepositoryInterface $userRepositoryInterface)
	{
		$this->userRepository = $userRepositoryInterface;
	}

	/**
	 * @param  array  $inputs
	 * @param  Group  $group
	 * @param  User   $creator
	 * @return User
	 */
	public function handle(array $inputs, Group $group, User $creator)
	{
		$user = $this->userRepository->findOneById($inputs['user_id']);

		$user = User::find($inputs['user_id']);

		$group->users()->save($user);

		return $user;
	}

}