<?php

namespace SourceScript\UserManagement\Commands;

use Illuminate\Hashing\HasherInterface;
use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\UserManagement\Factories\PermissionFactory;
use SourceScript\UserManagement\Repositories\UserRepositoryInterface;
use User;

class AddUser{

    use ValidableTrait;

    protected $rules = array(
        'username'      => 'required|unique:users,username',
        'password'      => 'required|confirmed',
        'group_id'      => 'required|is_group',
        'resident_id'   => 'required'
    );

    protected $messages = array(
        'is_group' => 'Invalid group'
    );

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;
    /**
     * @var PermissionFactory
     */
    private $permissionFactory;
    /**
     * @var HasherInterface
     */
    private $hasher;

    /**
     * @param UserRepositoryInterface $userRepository
     * @param HasherInterface $hasher
     * @param PermissionFactory $permissionFactory
     */
    function __construct(UserRepositoryInterface $userRepository, HasherInterface $hasher, PermissionFactory $permissionFactory)
    {
        $this->userRepository = $userRepository;
        $this->hasher = $hasher;
        $this->permissionFactory = $permissionFactory;
    }

    /**
     * @param array $inputs
     * @return User
     */
    function handle(array $inputs)
    {
        $user = new User;
        $user->fill($inputs);
        $user->permissions = $this->permissionFactory->createFromInput($inputs['permissions']);
        $user->password = $this->hasher->make($inputs['password']);

        $this->userRepository->save($user);

        return $user;
    }
} 