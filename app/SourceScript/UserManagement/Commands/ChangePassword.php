<?php namespace SourceScript\UserManagement\Commands;

use SourceScript\UserManagement\Repositories\UserRepositoryInterface;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Hashing\HasherInterface;
use SourceScript\Common\Validations\ValidableTrait;
use User;

class ChangePassword {

	use ValidableTrait;

	/**
	 * Validation rules
	 * 
	 * @var array
	 */
	protected $rules = [
		'old_password' => 'required|password_correct',
		'password' => 'required|confirmed'
	];


	/**
	 * Validation custom messages
	 * 
	 * @var array
	 */
	protected $messages = [
		'old_password.password_correct' => 'Incorrect password'
	];


	/**
	 * @var UserRepositoryInterface
	 */
	private $repository;


	/**
	 * @var HasherInterface
	 */
	private $hasher;


	function __construct(UserRepositoryInterface $userRepositoryInterface, HasherInterface $hasherInterface)
	{
		$this->repository = $userRepositoryInterface;
		$this->hasher = $hasherInterface;

		Validator::extend('password_correct', function($attribute, $value, $parameters)
		{
			return Auth::validate(['username' => Auth::user()->username, 'password' => $value]);
		});
	}


	/**
	 * @param  array  $inputs
	 * @param  User   $user
	 * @return User
	 */
	public function handle(array $inputs, User $user)
	{
		$user->password = $this->hasher->make($inputs['password']);

		$this->repository->savePassword($user);

		return $user;
	}
}