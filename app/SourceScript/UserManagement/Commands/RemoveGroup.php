<?php

namespace SourceScript\UserManagement\Commands;


use SourceScript\UserManagement\Repositories\GroupRepositoryInterface;
use Group;

class RemoveGroup {

    /**
     * @var GroupRepositoryInterface
     */
    private $groupRepository;

    /**
     * @param GroupRepositoryInterface $groupRepository
     */
    function __construct(GroupRepositoryInterface $groupRepository)
    {
        $this->groupRepository = $groupRepository;
    }

    /**
     * @param array $inputs
     * @param Group $group
     * @return Group
     */
    function handle(array $inputs, Group $group)
    {
        return $this->groupRepository->delete($group);
    }
} 