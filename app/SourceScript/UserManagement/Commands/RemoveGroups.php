<?php

namespace SourceScript\UserManagement\Commands;


use SourceScript\Common\Collections\ResultCollection;
use SourceScript\UserManagement\Repositories\GroupRepositoryInterface;

class RemoveGroups {

    /**
     * @var GroupRepositoryInterface
     */
    private $groupRepository;

    /**
     * @param GroupRepositoryInterface $groupRepository
     */
    function __construct(GroupRepositoryInterface $groupRepository)
    {
        $this->groupRepository = $groupRepository;
    }

    /**
     * @param array $inputs
     * @param ResultCollection $groups
     */
    function handle(array $inputs, ResultCollection $groups)
    {
        $this->groupRepository->deleteGroups($groups->modelKeys());
    }
}