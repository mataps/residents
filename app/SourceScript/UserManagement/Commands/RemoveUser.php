<?php

namespace SourceScript\UserManagement\Commands;

use SourceScript\UserManagement\Repositories\UserRepositoryInterface;
use User;

class RemoveUser {

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    function handle($inputs, User $user)
    {
        $this->userRepository->delete($user);
    }
} 