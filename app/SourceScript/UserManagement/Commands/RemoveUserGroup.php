<?php namespace SourceScript\UserManagement\Commands;

use SourceScript\UserManagement\Repositories\UserRepositoryInterface;
use User;

class RemoveUserGroup {

	/**
	 * @var UserRepositoryInterface
	 */
	private $userRepository;

	function __construct(UserRepositoryInterface $userRepositoryInterface)
	{
		$this->userRepository = $userRepositoryInterface;
	}

	/**
	 * @param  array  $inputs
	 * @param  Group  $group
	 * @param  User   $user
	 * @return User
	 */
	public function handle(array $inputs, User $user)
	{
		$user->group_id = null;

		$this->userRepository->save($user);

		return $user;
	}
}