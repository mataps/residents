<?php

namespace SourceScript\UserManagement\Commands;


use SourceScript\Common\Collections\ResultCollection;
use SourceScript\UserManagement\Repositories\UserRepositoryInterface;

class RemoveUsers {

    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;

    /**
     * @param UserRepositoryInterface $userRepository
     */
    function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     * @param array $inputs
     * @param ResultCollection $users
     */
    function handle(array $inputs, ResultCollection $users)
    {
        $this->userRepository->deleteUsers($users->modelKeys());
    }
}