<?php

namespace SourceScript\UserManagement\Commands;

use SourceScript\UserManagement\Factories\PermissionFactory;
use SourceScript\UserManagement\Repositories\GroupRepositoryInterface;
use Group;

class UpdateGroup {

    protected $rules = array(
        'name' => array('required'),
        'permissions' => array('are_permissions')
    );

    /**
     * @var GroupRepositoryInterface
     */
    private $groupRepository;
    /**
     * @var PermissionFactory
     */
    private $permissionFactory;

    /**
     * @param GroupRepositoryInterface $groupRepository
     * @param PermissionFactory $permissionFactory
     */
    function __construct(GroupRepositoryInterface $groupRepository, PermissionFactory $permissionFactory)
    {
        $this->groupRepository = $groupRepository;
        $this->permissionFactory = $permissionFactory;
    }

    function handle(array $inputs, Group $group)
    {
        $group->fill($inputs);

        $group->permissions = $this->permissionFactory->createFromInput($inputs['permissions']);

        $this->groupRepository->save($group);

        return $group;
    }
} 