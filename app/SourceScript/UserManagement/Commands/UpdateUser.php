<?php

namespace SourceScript\UserManagement\Commands;

use SourceScript\Common\Validations\ValidableTrait;
use SourceScript\UserManagement\Factories\PermissionFactory;
use SourceScript\UserManagement\Repositories\UserRepositoryInterface;
use Illuminate\Hashing\HasherInterface;
use User;

class UpdateUser
{
    use ValidableTrait;

    protected $rules = array(
        'username' => 'required',
        'password' => 'confirmed',
        'group_id' => 'required|is_group',
        // 'resident_id' => 'required'
    );

    protected $messages = array(
        'is_group' => 'Invalid group'
    );

    /**
     * @var
     */
    private $userRepository;
    /**
     * @var HasherInterface
     */
    private $hasher;
    /**
     * @var PermissionFactory
     */
    private $permissionFactory;

    /**
     * @param UserRepositoryInterface $userRepository
     * @param HasherInterface $hasher
     * @param PermissionFactory $permissionFactory
     */
    function __construct(UserRepositoryInterface $userRepository, HasherInterface $hasher, PermissionFactory $permissionFactory)
    {
        $this->userRepository = $userRepository;
        $this->hasher = $hasher;
        $this->permissionFactory = $permissionFactory;
    }

    /**
     * @param array $inputs
     * @param User $user
     * @return User
     */
    function handle(array $inputs, User $user)
    {
        $user->username = $inputs['username'];
        $user->group_id = $inputs['group_id'];
        $user->available_start = (isset($inputs['available_start'])) ? $inputs['available_start'] : null;
        $user->available_end = (isset($inputs['available_end'])) ? $inputs['available_end'] : null;

        $user->permissions = $this->permissionFactory->createFromInput($inputs['permissions']);
        $user->resident_id = null;

        if (isset($inputs['password']))
        {
            $user->password =  $this->hasher->make($inputs['password']);
        }


        if ($inputs['resident_id'] != "")
        {
            $user->resident_id = $inputs['resident_id'];
        }


        $this->userRepository->save($user);

        return $user;
    }
} 