<?php

namespace SourceScript\UserManagement\Exceptions;

use SourceScript\Common\Exceptions\HttpUnAuthorizedException;

class InvalidCredentialsException extends HttpUnAuthorizedException{

} 