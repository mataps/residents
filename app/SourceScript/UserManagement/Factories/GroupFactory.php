<?php

namespace SourceScript\UserManagement\Factories;


use SourceScript\UserManagement\Repositories\GroupRepositoryInterface;

class GroupFactory {

    private $repository;

    function __construct(GroupRepositoryInterface $groupRepository)
    {
        $this->repository = $groupRepository;
    }

    function create($name)
    {
        if ( ! empty($name))
        {
            return $this->repository->firstOrNew(['name' => $name]);
        }

        return null;
    }
}