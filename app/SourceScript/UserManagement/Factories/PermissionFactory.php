<?php

namespace SourceScript\UserManagement\Factories;


use Illuminate\Support\Collection;

class PermissionFactory {

    function createFromInput($inputs)
    {
        $permissions = new Collection;

        foreach ($inputs as $input)
        {
            $permissions[] = new \Permission([
                'id' => $input['id'],
                'status' => isset($input['status']) ? $input['status'] : 0
            ]);
        }

        return $permissions;
    }

    function create()
    {

    }
}