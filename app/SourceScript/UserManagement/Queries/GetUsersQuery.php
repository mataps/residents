<?php

namespace SourceScript\UserManagement\Queries;


use SourceScript\Common\Paginators\FilterableTrait;
use SourceScript\Common\Paginators\PaginatableTrait;
use SourceScript\Common\Paginators\SortableTrait;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Common\Repositories\GenericFinderInterface;
use User;

class GetUsersQuery {
    use PaginatableTrait, SortableTrait, FilterableTrait;

    public static $rules = array(
        'username' => '',
        'group.name' => ''
    );

    private $messages = array(
        'are_permissions' => 'Invalid permission'
    );

    private $finder;

    function __construct(GenericFinderInterface $finder)
    {
        $this->finder = $finder;
    }

    function handle(FilterParameters $filters = null, FieldsParameters $fields = null, PaginationParameters $pagination = null, SortParameters $sort = null)
    {
        $columns = $fields ? $fields->getFields() : FieldsParameters::$defaults;
        $query = User::modelJoin($columns, 'group');

        if ($filters)
        {
            $this->buildFilters($query, $filters);
        }

        if ($pagination)
        {
            $this->buildPagination($query, $pagination);
        }

        if ($sort)
        {
            $this->buildSorting($query, $sort);
        }

        return $this->finder->query($query, $columns);
    }

    public static function getAllowedFilters()
    {
        return array_keys(static::$rules);
    }
} 