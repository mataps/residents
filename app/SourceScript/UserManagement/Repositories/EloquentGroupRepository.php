<?php

namespace SourceScript\UserManagement\Repositories;


use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use Group;

class EloquentGroupRepository implements GroupRepositoryInterface{

    function __construct(GenericFinderInterface $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param $id
     * @throws DbException
     * @return Group
     */
    function findOneById($id)
    {
        $query = Group::where('id', $id);

        $result = $this->finder->findOne($query);

        if ($result)
        {
            return $result;
        }

        throw new DbException('Group not found!');
    }

    /**
     * @param $ids
     * @return mixed
     */
    function findGroups($ids)
    {
        $ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return Group::whereIn('id', $ids)->get();
    }

    /**
     * @param Group $group
     * @return Group
     * @throws DbException
     */
    function save(Group $group)
    {
        $permissions = $group->removeAttribute('permissions');

        foreach ($permissions as $permission)
        {
            $sync[$permission->id] = ['status' => $permission->status];
        }

        if ($group->save() && $group->permissions()->sync($sync))
        {
            return $group;
        }

        throw new DbException('Group not saved!');
    }

    /**
     * @param Group $group
     * @throws DbException
     */
    function delete(Group $group)
    {
        if ( ! $group->permissions()->detach() && ! $group->delete())
        {
            throw new DbException('Group not deleted!');
        }
    }

    function deleteGroups(array $groups)
    {
        $deleted = Group::destroy($groups);

        if ( ! $deleted)
        {
            throw new DbException('Groups not deleted!');
        }
    }

    function firstOrNew(array $params)
    {
        $group = Group::firstOrNew($params);

        if ($group)
        {
            return $group;
        }

        throw new DbException('Cannot find or create group');
    }
}