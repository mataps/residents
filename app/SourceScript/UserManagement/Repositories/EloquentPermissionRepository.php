<?php

namespace SourceScript\UserManagement\Repositories;


use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use Permission;

class EloquentPermissionRepository implements PermissionRepositoryInterface{

    function __construct(GenericFinderInterface $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param $id
     * @throws DbException
     * @return Permission
     */
    function findOneById($id)
    {
        $query = Permission::where('id', $id);

        $result = $this->finder->findOne($query);

        if ($result)
        {
            return $result;
        }

        throw new DbException('Permission not found!');
    }


    /**
     * @param  Permission $permission
     * @throws DbException
     * @return Permission
     */
    function save(Permission $permission)
    {
        if($permission->save())
            return $permission;

        throw new DbException('Permission not saved');
    }
}