<?php

namespace SourceScript\UserManagement\Repositories;


use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Repositories\GenericFinderInterface;
use User;

class EloquentUserRepository implements UserRepositoryInterface{

    function __construct(GenericFinderInterface $finder)
    {
        $this->finder = $finder;
    }

    /**
     * @param $id
     * @throws DbException
     * @return User
     */
    function findOneById($id)
    {
        $query = User::where('id', $id);

        $result = $this->finder->findOne($query);

        if ($result)
        {
            return $result;
        }

        throw new DbException('User not found');
    }


    /**
     * @param User $user
     * @return User
     * @throws DbException
     */
    function save(User $user)
    {
        $user->removeAttribute('password_confirmation');
        $permissions = $user->removeAttribute('permissions');
        $sync = [];
        foreach ($permissions as $permission)
        {
            $sync[$permission->id] = ['status' => $permission->status];
        }

        if ($user->save() && $user->permissions()->sync($sync))
        {
            return $user;
        }

        throw new DbException('User not saved!');
    }


    /**
     * @param User $user
     * @return User
     * @throws DbException
     */
    function savePassword(User $user)
    {
        if($user->save())
        {
            return $user;
        }

        throw new DbException('User not saved!');
    }


    /**
     * @param User $user
     * @throws DbException
     */
    function delete(User $user)
    {
        if ( ! $user->delete())
        {
            throw new DbException('User not deleted!');
        }
    }


    /**
     * @param $ids
     * @return mixed
     */
    function findUsers($ids)
    {
        $ids = is_array($ids) ?: explode(',', str_replace(' ', '', urldecode($ids)));

        return User::whereIn('id', $ids)->get();
    }

    function deleteUsers(array $users)
    {
        $deleted = User::destroy($users);

        if ( ! $deleted)
        {
            throw new DbException('Users not deleted!');
        }
    }
}