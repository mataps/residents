<?php

namespace SourceScript\UserManagement\Repositories;

use Group;

interface GroupRepositoryInterface {

    /**
     * @param $id
     * @return Group
     */
    function findOneById($id);

    /**
     * @param Group $group
     * @return Group
     */
    function save(Group $group);

    /**
     * @param Group $group
     * @return void
     */
    function delete(Group $group);

    function firstOrNew(array $params);
} 