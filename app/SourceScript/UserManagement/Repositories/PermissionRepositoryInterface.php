<?php

namespace SourceScript\UserManagement\Repositories;

use Group;

interface PermissionRepositoryInterface {

    /**
     * @param $id
     * @return Group
     */
    function findOneById($id);
} 