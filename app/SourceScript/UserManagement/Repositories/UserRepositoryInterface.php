<?php

namespace SourceScript\UserManagement\Repositories;


use Illuminate\Database\Eloquent\Builder;
use User;

interface UserRepositoryInterface {

    /**
     * @param $id
     * @return User
     */
    function findOneById($id);

    /**
     * @param User $user
     * @return User
     */
    function save(User $user);

    /**
     * @param User $user
     * @return void
     */
    function delete(User $user);

    function deleteUsers(array $users);
} 