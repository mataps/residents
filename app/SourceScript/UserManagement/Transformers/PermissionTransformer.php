<?php

namespace SourceScript\UserManagement\Transformers;


use SourceScript\Common\Transformers\AbstractTransformer;
use Illuminate\Database\Eloquent\Model;

class PermissionTransformer extends AbstractTransformer{

    protected $fields = array(
        'id'            => '',
        'name'          => 'transformName',
        'description'   => ''
    );

    public function transform(Model $model, $except = array())
    {
        return [
            'id'            => $model->id,
            'component'     => $this->getComponentName($model->name),
            'functionality' => $this->getFunctionality($model->name),
            'description'   => $model->description
        ];
    }

    private function getComponentName($name)
    {
        list($component, $functionality) = explode('.', $name);
        return studly_case($component);
    }

    private function getFunctionality($name)
    {
        list($component, $functionality) = explode('.', $name);
        return $functionality;
    }
} 