<?php

namespace SourceScript\UserManagement;


use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Common\ServiceInterface;
use Illuminate\Support\Collection;
use User;

interface UserManagementInterface extends ServiceInterface{

    /**
     * @param array $credentials
     * @return User
     */
    function findUserByCredentials(array $credentials);

    /**
     * @param PaginationParameters $pagination
     * @return Collection
     */
    function findGroups(
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null);

    /**
     * @param FilterParameters $filterParameters
     * @param FieldsParameters $fieldsParameters [optional]
     * @param PaginationParameters $pagination [optional]
     * @return Collection
     */
    function findUsers(FilterParameters $filterParameters, FieldsParameters $fieldsParameters = null, PaginationParameters $pagination = null);

    /**
     * @param PaginationParameters $pagination [optional]
     * @return Collection
     */
    function findPermissions(PaginationParameters $pagination = null);
} 