<?php

namespace SourceScript\UserManagement;

use SourceScript\Common\AbstractProvider;
use SourceScript\Common\Repositories\EloquentGenericRepository;
use SourceScript\UserManagement\Repositories\EloquentGroupQueryRepository;

class UserManagementProvider extends AbstractProvider{

    protected $modules = array(
        'User' => array(
            'repository' => 'EloquentUserRepository',
            'validations' => array(),
            'bindings' => array(
                'users' => 'findOneById',
                'Users' => 'findUsers'
            )
        ),
        'Group' => array(
            'repository' => 'EloquentGroupRepository',
            'validations' => array(
                'is_group' => 'GroupValidation@idExists',
                'are_groups' => 'GroupValidation@idsExists'
            ),
            'bindings' => array(
                'groups' => 'findOneById',
                'Groups' => 'findGroups'
            )
        ),
        'Permission' => array(
            'repository' => 'EloquentPermissionRepository',
            'validations' => array(
                'is_permission' => 'PermissionValidation@idExists',
                'are_permissions' => 'PermissionValidation@idsExists'
            )
        )
    );

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bindShared('SourceScript\UserManagement\Repositories\IGroupQueryRepository', function()
        {
            return new EloquentGroupQueryRepository();
        });
    }
}