<?php namespace SourceScript\UserManagement;

use SourceScript\Common\AbstractService;
use SourceScript\Common\Exceptions\DbException;
use SourceScript\Common\Paginators\FilterableTrait;
use SourceScript\Common\Paginators\PaginatableTrait;
use SourceScript\Common\Paginators\SortableTrait;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Common\Repositories\GenericFinderInterface;
use SourceScript\Common\Services\PermissionMerger;
use SourceScript\UserManagement\Exceptions\InvalidCredentialsException;
use Group;
use Illuminate\Hashing\HasherInterface;
use Illuminate\Support\Collection;
use Permission;
use User;
use History;

class UserManagementService extends AbstractService implements UserManagementInterface{
    use PaginatableTrait, SortableTrait, FilterableTrait;

    /**
     * @var HasherInterface
     */
    private $hasher;
    /**
     * @var PermissionMerger
     */
    private $permissionMerger;

    function __construct(GenericFinderInterface $finder, HasherInterface $hasher, PermissionMerger $permissionMerger)
    {
        $this->finder = $finder;
        $this->hasher = $hasher;
        $this->permissionMerger = $permissionMerger;
    }


    /**
     * @param  string                    $model
     * @param  integer                   $id
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return ResultCollection
     */
    public function findHistoryByResource(
        $model,
        $id,
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $query = History::query();

        $query->where('resource_id', $id)->where('resource_type', $model)->orderBy('created_at', 'desc');

        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @return Resultcollection
     */
    public function findHistory(
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $query = History::query();

        $query->orderBy('created_at', 'desc');

        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param  FieldsParameters|null     $fieldsParameters
     * @param  PaginationParameters|null $paginationParameters
     * @param  SortParameters|null       $sortParameters
     * @param  FilterParameters|null     $filterParameters
     * @param  User                      $user
     * @return ResultCollection
     */
    public function findHistoryByUser(
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null,
        User                    $user)
    {
        $query = History::query();

        $query->where('user_id', $user->id);

        $query->orderBy('created_at', 'desc');

        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }


    /**
     * @param array $credentials
     * @throws InvalidCredentialsException
     * @return User
     */
    function findUserByCredentials(array $credentials)
    {
        try {
            $query = User::query();
            $user = $this->finder->findOneBy($query, ['username' => $credentials['username']]);

            if ($this->hasher->check($credentials['password'], $user->password))
            {
                return $user;
            }

        }
        catch(DbException $e){}

        throw new InvalidCredentialsException('Incorrect Username or Password!');
    }

    /**
     * @param PaginationParameters $pagination
     * @return \Illuminate\Support\Collection
     */
    function findGroups(
        FieldsParameters        $fieldsParameters = null,
        PaginationParameters    $paginationParameters = null,
        SortParameters          $sortParameters = null,
        FilterParameters        $filterParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        
        $query = Group::query();
        
        $countRows = $this->finder->count($query);

        if($paginationParameters)
            $this->buildPagination($query, $paginationParameters);

        // if($sortParameters)
        //     $this->buildSorting($query, $sortParameters);

        if($filterParameters)
            $this->buildFilters($query, $filterParameters);


        $result = $this->finder->query($query);

        $result->setCountRows($countRows);

        return $result;
    }

    /**
     * @param  Group                $group
     * @param  FieldsParameters     $fieldsParameters
     * @param  PaginationParameters $paginationParameters
     * @param  SortParameters       $sortParameters
     * @param  FilterParameters     $filterParameters
     * @return ResultCollection
     */
    public function findUsersByGroup(
        Group                   $group,
        FieldsParameters        $fieldsParameters,
        PaginationParameters    $paginationParameters,
        SortParameters          $sortParameters,
        FilterParameters        $filterParameters)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;

        $query = $group->users()->getQuery();

        $countRows = $this->finder->count($query);

        if ($filterParameters)
        {
            $this->buildFilters($query, $filterParameters);
        }

        if ($paginationParameters)
        {
            $this->buildPagination($query, $paginationParameters);
        }

        if ($sortParameters)
        {
            $this->buildSorting($query, $sortParameters);
        }

        $results = $this->finder->query($query, $columns);

        $results->setCountRows($countRows);

        return $results;        
    }

    /**
     * @param FilterParameters $filterParameters
     * @param FieldsParameters $fieldsParameters [optional]
     * @param PaginationParameters $paginationParameters [optional]
     * @param SortParameters $sortParameters
     * @return Collection
     */
    function findUsers(FilterParameters $filterParameters, FieldsParameters $fieldsParameters = null, PaginationParameters $paginationParameters = null, SortParameters $sortParameters = null)
    {
        $columns = $fieldsParameters ? $fieldsParameters->getFields() : FieldsParameters::$defaults;
        $query = User::modelJoin($columns, 'group')->with('permissions');

        $countRows = $this->finder->count($query);

        if ($filterParameters)
        {
            $this->buildFilters($query, $filterParameters);
        }

        if ($paginationParameters)
        {
            $this->buildPagination($query, $paginationParameters);
        }

        if ($sortParameters)
        {
            $this->buildSorting($query, $sortParameters);
        }

        $results = $this->finder->query($query, $columns);

        $results->setCountRows($countRows);

        return $results;
    }

    /**
     * @param PaginationParameters $pagination
     * @return Collection
     */
    function findPermissions(PaginationParameters $pagination = null)
    {
        $query = Permission::query();

        if ($pagination)
        {
            $this->buildPagination($query, $pagination);
        }

        return $this->finder->query($query);
    }

    function getMergedPermissions(User $user)
    {
        $userPermissions = $user->allPermissions;
        $groupPermissions = $user->group->allPermissions;

        $mergedPermissions = $this->permissionMerger->merge($userPermissions, $groupPermissions);

        $user->permissions = $mergedPermissions;

        return $user;
    }
}