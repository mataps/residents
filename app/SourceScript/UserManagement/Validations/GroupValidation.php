<?php

namespace SourceScript\UserManagement\Validations;


use SourceScript\Common\Validations\ExistenceValidationTrait;
use SourceScript\UserManagement\Repositories\GroupRepositoryInterface;

class GroupValidation {
    use ExistenceValidationTrait;

    /**
     * @var GroupRepositoryInterface
     */
    private $repository;

    function __construct(GroupRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }
} 