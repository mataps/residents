<?php

namespace SourceScript\UserManagement\Validations;


use SourceScript\Common\Validations\ExistenceValidationTrait;
use SourceScript\UserManagement\Repositories\PermissionRepositoryInterface;

class PermissionValidation {
    use ExistenceValidationTrait;

    /**
     * @var PermissionRepositoryInterface
     */
    private $repository;

    function __construct(PermissionRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }
} 