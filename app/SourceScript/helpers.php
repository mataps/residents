<?php


if(! function_exists('issetOr'))
{
    function issetOr($value)
    {
        return isset($value) ? $value : null;
    }
}

if ( ! function_exists('get_component_namespace'))
{
    function get_component_namespace($param)
    {
        $class = explode('\\', is_object($param) ? get_class($param) : $param);
        $class = $class[1];

        return $class;
    }
}

if ( ! function_exists('get_base_class'))
{
    function get_base_class($obj)
    {
        $class = explode('\\', get_class($obj));
        $class =  array_pop($class);

        return $class;
    }
}

if ( ! function_exists('get_base_namespace'))
{
    function get_base_namespace($param)
    {
        $class = explode('\\', is_object($param) ? get_class($param) : $param);
        $class =  array_shift($class);

        return $class;
    }
}

if ( ! function_exists('get_unset'))
{
    function get_unset($param, $attribute)
    {
        if (is_object($param))
        {
            $value = $param->{$attribute};
            unset($param->{$attribute});
        }
        else
        {
            $value = $param[$attribute];
            unset($param[$attribute]);
        }

        return $value;
    }
}

if ( ! function_exists('array_merge_defaults'))
{
    function array_merge_defaults(array $params, array $defaults)
    {
        $intersect = array_intersect_key($params, $defaults); //Get data for which a default exists

        $diff = array_diff_key($defaults, $params); //Get defaults which are not present in data

        return $diff + $intersect; //Arrays have different keys, return the union of the two
    }
}



if( ! function_exists('camel_case_explode') )
{
    function camel_case_explode($string, $lowercase = true)
    {
        $array = preg_split('/([A-Z][^A-Z]*)/', $string, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);
        if ($lowercase) array_map('strtolower', $array);
        return $array;
    }
}

if ( ! function_exists('file_force_put_contents'))
{
    function file_force_put_contents($filePath, $contents)
    {
        $parts = explode('/', $filePath);
        array_pop($parts);
        $folderPath = implode('/', $parts);
        if ( ! is_writable($folderPath) )
        {
            if ( ! mkdir($folderPath, 0777, true))
            {
                die("Failed to create $filePath");
            }
        }
        file_put_contents($filePath, $contents);
    }
}