<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AllowanceReset extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'allowance-reset';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$scholars = Scholarship::all();
		$total = count($scholars);
		$counter = 0;
		foreach ($scholars as $scholar) {
			$counter++;
			$this->info($counter . "/" . $total);
			$semesters = $scholar->semesters;
			if(count($semesters) > 1){
				$college_group = $semesters->filter(function($item){
					if($item->school_level == 'collegiate'){
						return true;
					}
				})->groupBy('scholarship_type_id')->sortBy('uuid')->sortBy('created_at')->values();
				$secondary_group = $semesters->filter(function($item){
					if($item->school_level == 'secondary'){
						return true;
					}
				})->groupBy('scholarship_type_id')->sortBy('uuid')->sortBy('created_at')->values();
				// var_dump($college);
				foreach ($college_group as $college) {
						foreach ($college as $key => $semester) {
					if($key == 0) {
						$semester->allowance_basis_id = null;
					}
					else{
						var_dump($key);
						// exit();
						$semester->allowance_basis_id = $college[$key-1]->id;
						
					}
					$semester->save();
				}

				}
				
				foreach ($secondary_group as $secondary) {
						foreach ($secondary as $key => $semester) {
					if($key == 0) {
						$semester->allowance_basis_id = null;
					}
					else{
						var_dump($key);
						// exit();
						$semester->allowance_basis_id = $secondary[$key-1]->id;
						
					}
					$semester->save();
				}	
				}


			}
		}
	}



}
