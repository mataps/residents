<?php

use Illuminate\Console\Command;
use SourceScript\Profiling\ProfilingInterface;

class AssignAffiliation extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'assign-affiliation';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Migrate all affiliation to new system';

	/**
	 * Create a new command instance.
	 *
	 * @param ProfilingInterface $profiler
	 * @return void
	 */
	public function __construct(ProfilingInterface $profiler)
	{
		parent::__construct();
		$this->profiler = $profiler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->profiler->execute('AssignAffiliation', array());
	}

}
