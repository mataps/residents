<?php

use Illuminate\Console\Command;
use SourceScript\Profiling\ProfilingInterface;

class CalculateLastNames extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'calculate:lastnames';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Regenerates last name counts based on residents.';

	/**
	 * Create a new command instance.
	 *
	 * @param ProfilingInterface $profiler
	 * @return void
	 */
	public function __construct(ProfilingInterface $profiler)
	{
		parent::__construct();
		$this->profiler = $profiler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->profiler->execute('CalculateLastnames', array());
	}

}
