<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Elasticsearch\Client;

// use TransactionTransformer;

class ElasticSearchIndex extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'elasticsearch:index';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';


	/**
	 *  The parameter for Elastic Search Client
	 */

	protected $params = array();

	protected $client; 

    private $transactionTransformer;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();

		$this->params['hosts'] = array (
			    '127.0.0.1:9200'         // IP + Port

		);

		// $this->client = new Client($this->params);


	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		//
		//
		$this->info('Elastic Search Indexing');
		$this->info('Initializing....');


		$this->client = new Client($this->params);

		$this->indexResidents();
		$this->indexTransactions();
	
	}

	public function indexResidents()
	{
		$count = Resident::count();

		$this->info('Residents Indexing');
		$this->info('Indexing ' . $count . ' records of residents.');

		
		for($i = 141326; $i < $count; $i++){
		
			$resident = Resident::skip($i)->take(1)->first();
			// dd($resident->profilePic);

			$counter = $i + 1;

		$residents = Resident::all();
		$this->info('Residents Indexing');
		$this->info('Indexing ' . $count . ' records of residents.');


			$cityMunicipality = ($resident->cityMunicipality) ? $resident->cityMunicipality->name : 'None';
			$profile_pic = ($resident->profilePic) ? '/uploads/' . Str::title($cityMunicipality) . '/photos/' . $resident->profilePic->filename : 'none';


			$district = ($resident->district) ? $resident->district->name : 'None';

			$barangay = ($resident->barangay) ? $resident->barangay->name : 'None'; 
			
		

			$profile_pic = ($resident->profilePic) ? $resident->profilePic()->filename : 'none';

			$cityMunicipality = ($resident->cityMunicipality) ? $resident->cityMunicipality->name : 'None';

			$district = ($resident->district) ? $resident->district->name : 'None';

			$barangay = ($resident->barangay) ? $resident->barangay->name : 'None'; 
			// dd($profile_pic);

			$params['body'] = array(
					'profile_pic.name' => $profile_pic,

					'district'	=> $district,
					'barangay'	=> $barangay,
					'cityMunicipality'	=> $cityMunicipality,
					'first_name' => $resident->first_name,
					'last_name' => $resident->last_name,
					'middle_name' => $resident->middle_name,
					'full_name' => $resident->first_name . ' ' . $resident->middle_name . ' ' . $resident->last_name,
					'email' => $resident->email,
					'mobile' => $resident->mobile,
					'phone' => $resident->phone,
					'precint' => $resident->precint,
					'birthdate' => date("Y-m-d", strtotime($resident->birthdate)),
					'gender' => $resident->gender,
					'civil_status' => $resident->civil_status,
					'street' => $resident->street,
					'creator' => $resident->creator,
					'updater' => $resident->updater,
					'attributes' => $resident->attributes,
					'updated_at' => $resident->updated_at,
					'created_at' => $resident->created_at,
					'history' => $resident->history

				);

			$params['index'] = 'galactus';
			$params['type']  = 'residents';
			$params['id']    = $resident->id;
			
			$this->client->index($params);
			$this->info( $counter . '/' . $count . ' of Residents');


		}
	
	}



		public function indexTransactions()
		{
				$count = Transaction::count();
				$transactions = Transaction::all();
				$this->info('Transactions Indexing');
				$this->info('Indexing ' . $count . ' records of Transactions.');

				foreach ($transactions as $key => $transaction) {
					$counter = $key + 1;

					$transaction_transformed = array(
							'id' => $transaction->id,
							'voucher' => $transaction->voucher,
							'client' => $transaction->client,
							'referrer' => $transaction->referrer,
							'beneficiary' => $transaction->beneficiary,
							'account' => $transaction->account,
							'category' => $transaction->category,
							'subCategory' => $transaction->subCategory,
							'item' => $transaction->item,
							'type' => $transaction->type,
							'settled' => $transaction->settled,
							'details' => $transaction->details,
							'remarks' => $transaction->remarks,
							'amount' => $transaction->amount,
							'status' => $transaction->status,
							'creator' => $transaction->creator,
							'updater' => $transaction->updater,
							'updated_at' => $transaction->updated_at,
							'created_at' => $transaction->created_at,
						);

					$params['body'] = $transaction_transformed;
					$params['index'] = 'galactus';
					$params['type']  = 'transactions';
					$params['id']    = $transaction->id;
					$this->client->index($params);
					$this->info( $counter . '/' . $count . ' of Transactions');

				}


		}
	


}
