<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FixApplied extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'fix-applied-at';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = '';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		
		$old_scholars_sql =  "select semester as semester_name, sem.OID as semester_OID, first_name, maternal_name, last_name, substr(scholar_level, 1, 1) as scholar_level, substr(scholar_level, 2, 1) as term  from cdodb_test.scholar_record join (select scholar.OID, resident.first_name, resident.maternal_name, resident.last_name from cdodb_test.scholar join cdodb_test.resident on resident_OID = resident.OID) as scholar on scholar.OID = scholar_OID join cdodb_test.scholar_sem_info as sem on sem.OID = sem_info_OID where is_probationary = 1";
		$old_scholars_sql = "select * from cdodb_test.scholar_dup";

		$old_scholar_records = DB::select(DB::raw($old_scholars_sql));

        $total = count($old_scholar_records);
 		$counter = 0;     

 		foreach($old_scholar_records as $old_record)
 		{
 			try{
 		
 				$sql = "select s.id from galactus_edbms.scholarships as s join galactus_edbms.residents as r on r.id = s.resident_id where r.full_name = :full_name";
        		$scholarship = DB::select(DB::raw($sql), array(
        				'full_name' => $old_record->resident_name
        			));
        		if(!$scholarship) continue;

        		$this->info($old_record->resident_name);
        		$scholar_id = $scholarship[0]->id;
        		$scholar = Scholarship::find($scholar_id);
				// Get Term
				if(!$scholar) continue;

				$scholar->applied_at = $old_record->application_date;
				$scholar->approved_at = $old_record->approval_date;
				$scholar->save();

				$counter++;
				$this->info($counter . "/" . $total);

 			}
 			catch(Exception $e){
 				dd("error");
 			}

 		}

 	}

}
