<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FixBarangay extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'fix-barangay';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Fix Residents with no Barangay';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$null_residents = Resident::where("barangay_id", "=", "0")
								  ->get();
		$total = count($null_residents);
		$counter = 0;
		foreach ($null_residents as $resident) {

			$sql = "select BARANGAY_NAME from cdodb_test.residents_export where FULLNAME=:full_name and CITY=:city limit 0, 1";
			


			$record = DB::select(DB::raw($sql), array(
					'full_name' => $resident->full_name,
					'city' => $resident->cityMunicipality->name
					// 'mobile' => $resident->mobile
				));

			if(count($record) > 0){

				$barangay_name = $record[0]->BARANGAY_NAME;
				$barangay = Barangay::where('name', $barangay_name)->where('city_municipality_id', $resident->city_municipality_id)->first();
				
				if($barangay){
					$resident->barangay_id = $barangay->id;

					$resident->save();
					
				}

				$counter++;
				$this->info($counter . "/" . $total);
			}

		}
	}

	

}
