<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FixBarangayMismatch extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'barangay-mismatch';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Fix Barangay Mismatch.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$sql = "select residents.id as id, b.name as barangay_name, residents.city_municipality_id as resident_city_id, b.city_municipality_id as barangay_city_id from residents join barangays as b on b.id = residents.barangay_id where b.city_municipality_id != residents.city_municipality_id";
		$records = DB::select(DB::raw($sql));
		$total = count($records);
		$count = 0;
		foreach ($records as $record) {
//			dd($record);
			$count++;
			$this->info($count . '/' . $total);
			$barangay = Barangay::where('name', $record->barangay_name)->where('city_municipality_id', $record->resident_city_id)->first();
	
			if($barangay){
				$resident = Resident::find($record->id);
				if($resident){
					
					$resident->barangay_id = $barangay->id;
					$resident->save();
				}
				else { var_dump("RESIDENT IS NULL"); var_dump($resident); }


			}
			else{ var_dump("BARANGAY : " . $record->barangay_name . " CITY : " . $record->resident_city_id); var_dump($barangay); }

		}
	}


}
