<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FixPrecint extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'fix-precint';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		 ini_set('max_execution_time', 0);
        //record the start time
        $time_start = microtime(true);

        $databases = [
            'Aliaga2014',
            'Bongabon2014',
            'Cabanatuan2014',
            'Cabiao2014',
            'Carranglan2014',
            'Cuyapo2014',
            'Gabaldon2014',
            'Gapan2014',
            'GenNatividad2014',
            'GenTinio2014',
            'Guimba2014',
            'Jaen2014',
            'Laur2014',
            'Licab2014',
            'Llanera2014',
            'Lupao2014',
            'Munoz2014',
            'Nampicuan2014',
            'Palayan2014',
            'Pantabangan2014',
            'Penaranda2014',
            'Quezon2014',
            'Rizal2014',
            'SanAntonio2014',
            'SanIsidro2014',
            'SanJose2014',
            'SanLeonardo2014',
            'StaRosa2014',
            'StoDomingo2014',
            'Talavera2014',
            'Talugtug2014',
            'Zaragoza2014'
        ];


        foreach ($databases as $database)
        {
            $db = 'comelec';
            Config::set("database.connections.$db.database", $database);
            $connection = DB::connection($db);
            $connection->setDatabaseName($database);
            $connection->reconnect();

            $total = $connection->table('doctable')->count();

            $this->info($database);
            foreach (range(0, $total) as $counter)
            {
                $result = $connection->table('precintcode')->offset($counter-1)->limit(1)->first();
				$this->info( $result->voters_id);
                $resident = Resident::where('voters_id', $result->voters_id)->first();
                $this->info($counter . "/" . $total);
				if($resident){
					$this->info($resident->full_name);

					$resident->precinct = $resident->precinct . $result->RESPRECINCTCODE;
					$resident->save();

				}


				

             }


         }
	}

	

}
