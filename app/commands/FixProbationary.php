<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FixProbationary extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'fix-probationary';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$old_scholars_sql =  "select semester as semester_name, sem.OID as semester_OID, first_name, maternal_name, last_name, substr(scholar_level, 1, 1) as scholar_level, substr(scholar_level, 2, 1) as term  from cdodb_test.scholar_record join (select scholar.OID, resident.first_name, resident.maternal_name, resident.last_name from cdodb_test.scholar join cdodb_test.resident on resident_OID = resident.OID) as scholar on scholar.OID = scholar_OID join cdodb_test.scholar_sem_info as sem on sem.OID = sem_info_OID where is_probationary = 1";
		$old_scholar_records = DB::select(DB::raw($old_scholars_sql));

        $total = count($old_scholar_records);
 		$counter = 0;     

 		foreach($old_scholar_records as $old_record)
 		{
 			try{
 		

        		$resident = Resident::where('first_name', $old_record->first_name)
        							->where('middle_name', $old_record->maternal_name)
        							->where('last_name', $old_record->last_name)
        							->first();
        		if(!$resident) continue;
        		$this->info($resident->full_name);
        		$scholar = Scholarship::where('resident_id', $resident->id)->first();

				// Get Term
				if(!$scholar) continue;

				if($old_record->semester_name != "CANCEL" || $old_record->semester_name != NULL){
				    $semester_details_array = explode("S", $old_record->semester_name);
				//	echo $old_record->semester_name;
				       if(count($semester_details_array) == 0 ){
					echo "error";
					break;
				}
					$term =(count($semester_details_array) > 1)  ? $semester_details_array[1] : 0;
				}
				if($old_record->semester_name == "CANCEL"){
					continue;
				}
				// Build School Year 
				$start_year = ($old_record->semester_OID == "15" || $old_record->semester_OID == "17" ) ? 2014 : $semester_details_array[0];

				$end_year = $start_year + 1;

				$school_year = $start_year . "-" . $end_year;

        		$semester = Semester::where('scholarship_id', $scholar->id)
        							->where('school_year', $school_year)
        							->where('term', $term)
        							->first();
        		if($semester){
					$counter++;
					$this->info($counter . "/" . $total);
	        		$semester->is_probation = 1;

	        		$semester->save();
	        	}

 			}
 			catch(Exception $e){
 				dd("error");
 			}

 		}

	}



}
