<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FixReferrer extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'fix-referrer';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = '';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$old_scholars_sql =  "select semester as semester_name, sem.OID as semester_OID, first_name, maternal_name, last_name, birthdate, substr(scholar_level, 1, 1) as scholar_level, substr(scholar_level, 2, 1) as term, ref_first_name, ref_maternal_name, ref_last_name, ref_birthdate from cdodb_test.scholar_record join (select referrer_OID, scholar.OID, resident.first_name, resident.maternal_name, resident.last_name, resident.birthdate, referrer.first_name as ref_first_name, referrer.maternal_name as ref_maternal_name, referrer.last_name as ref_last_name, referrer.birthdate as ref_birthdate from cdodb_test.scholar join cdodb_test.resident on resident_OID = resident.OID join cdodb_test.resident as referrer on referrer.OID = referrer_OID) as scholar on scholar.OID = scholar_OID join cdodb_test.scholar_sem_info as sem on sem.OID = sem_info_OID where scholar.`referrer_OID` is not null";
		$old_scholar_records = DB::select(DB::raw($old_scholars_sql));

        $total = count($old_scholar_records);
 		$counter = 0;     

 		foreach($old_scholar_records as $old_record)
 		{
			try{
 				

        		$resident = Resident::where('first_name', $old_record->first_name)
        							->where('middle_name', $old_record->maternal_name)
        							->where('last_name', $old_record->last_name)
        							->first();


        		$referrer = Resident::where('first_name', $old_record->ref_first_name)
        							->where('middle_name', $old_record->ref_maternal_name)
        							->where('last_name', $old_record->ref_last_name)
        							->where('birthdate', $old_record->ref_birthdate)
        							->first();
        	if(!$resident) continue;
        	if(!$referrer) continue;
        	
        	$scholar = Scholarship::where('resident_id', $resident->id)->first();

				// Get Term
        	if(!$scholar) continue;
			
			$counter++;
			$this->info($counter . "/" . $total);
			$scholar->referrer_id = $referrer->id;
			$scholar->save();


 			}
 			catch(Exception $e){
 				dd($e);
 			}
		
	}

}


}
