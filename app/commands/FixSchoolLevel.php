<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FixSchoolLevel extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'fix-school-level';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Assign School Level Base on Course';
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$semesters = Semester::whereNotNull('course_id')->get();
		$total = count($semesters);
		$counter = 0;
		foreach ($semesters as $semester) {
			$course = $semester->course;
			if($course){
				if($course->name == "HS")
				{
					$semester->school_level = "secondary";
					$semester->save();
				}	
				else{
					$semester->school_level = "collegiate";
					$semester->save();
				}				
			}

			$counter++;
			$this->info($counter . "/" . $total);
		}
	}



}
