<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class HistoryTables extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'history:tables';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$modules = explode(",", $this->option('modules'));

		$profiler_tables = [
			'residents',
			'households',
			'affiliations',
			'attributions',
			'positions'
		];

		$transaction_tables = [
			'transaction_accounts',
			'transaction_categories',
			'transaction_items',
			'transaction_sub_categories',
			'transactions',
			'vouchers',
			'liquidations'
		];

		$scholarship_tables = [
			'awards',
			'schools',
			'semesters',
			'grades',
			'allowance_policies',
			'allowances',
			'allowance_releases',
			'scholarship_types',
			'subjects'
		];

		$usermanagement_tables = [
			'groups',
			'users'
		];

		foreach(array_merge($profiler_tables, $transaction_tables, $scholarship_tables, $usermanagement_tables) as $tablename)
		{
			if(Schema::hasTable('history_' . $tablename))
			{
				Schema::drop('history_' . $tablename);
			}

			$columns = DB::select(DB::raw('describe ' . $tablename));

			Schema::create('history_' . $tablename, function($table) use ($columns)
			{
				$table->increments('id');
				$table->integer('resource_id')->index()->unsigned();

				foreach($columns as $column)
				{
					$type = explode('(', $column->Type)[0];
					preg_match('#\((.*?)\)#', $column->Type, $length);
					// $unsigned = (explode(' ', $column->Type)[1] == 'unsigned') ? true : false;
					if( ! in_array($column->Field, ['id', 'created_at', 'updated_at']))
					{
						switch ($type) {
							case 'int':
								$table->integer($column->Field)->nullable();
								break;

							case 'varchar':
								$table->string($column->Field, $length[1])->default($column->Default)->nullable();
								break;

							case 'decimal':
								$table->decimal($column->Field)->nullable();
								break;

							case 'tinyint':
								$table->tinyInteger($column->Field)->nullable();
								break;

							case 'text':
								$table->text($column->Field)->default($column->Default)->nullable();
								break;

							case 'enum':
								preg_match('#\((.*?)\)#', $column->Type, $insideParenthesis);

								$replaced = str_replace("'", "", $insideParenthesis[1]);

								$valuesArray = explode(',', $replaced);

								$table->enum($column->Field, $valuesArray)->default($column->Default)->nullable();
								break;

							case 'date':
								$table->date($column->Field)->nullable();
								break;

							case 'timestamp':
								if($column->Field !== 'created_at' && $column->Field !== 'updated_at' && $column->Field !== 'deleted_at')
								{
									$table->timestamp($column->Field)->nullable();	
								}
								break;
							default:
								# code...
								break;
						}
					}
				}

				$table->timestamps();
				$table->softDeletes();
			});

			echo "history_" . $tablename . " table created\n";
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('example', InputArgument::OPTIONAL, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('modules', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
