<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use SourceScript\Profiling\ProfilingInterface;


class MigrateHousehold extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'migrate-household';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Migrate Household.';

	/**
	 * Create a new command instance.
	 *
	 * @param ProfilingInterface $profiler
	 * @return void
	 */
	public function __construct(ProfilingInterface $profiler)
	{
		parent::__construct();
		$this->profiler = $profiler;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->profiler->execute('MigrateHousehold', array());
	}

	

}
