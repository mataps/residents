<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use SourceScript\Profiling\ProfilingInterface;

class MigrateHouseholdAccess extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'migrate-household-access';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';


	public $profilingService;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(ProfilingInterface $profilingInterface)
	{
		parent::__construct();

			$this->profilingService = $profilingInterface;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{

		$city_list = array(
				array('key' => 19, 'db_name' => 'general_tinio_access_db', 'name' => 'General Tinio'),
				array('key' => 34, 'db_name' => 'penaranda_access_db', 'name' => 'Penaranda'),
				array('key' => 37, 'db_name' => 'san_antonio_access_db', 'name' => 'San Antonio'),
				array('key' => 139, 'db_name' => 'san_isidro_access_db', 'name' => 'San Isidro'),
				array('key' => 132, 'db_name' => 'san_leonardo_access_db', 'name' => 'San Leonardo'),

			);

		$creator = User::find(1);

		foreach ($city_list as $city) {
			# code...
	

		$city_municipality_id = $city['key'];

		$access_data = "select * from ". $city['db_name'] .".records as r join ". $city['db_name'] .".barangay as b on b.ID = r.Residence left join ". $city['db_name'] .".religion on religion.OID = r.Religion group by r.Residence, r.Street, r.HouseNo, r.AreaCode, r.OwnerName";

		$house_records = DB::select(DB::raw($access_data));

		$total = count($house_records);
		$counter = 0;



		foreach ($house_records as $record) {
			$barangay_name = $record->barangay_name;
			$barangay_id = Barangay::where('name', $barangay_name)->where('city_municipality_id', $city_municipality_id)->first()->id;


			$household = Household::firstOrNew(array('owner_name' => $record->OwnerName,'area_code' => $record->AreaCode,'district_id'=> 4, 'city_municipality_id' => $city_municipality_id, 'barangay_id'=> $barangay_id));

			$household->district_id = 4;
			$household->city_municipality_id = $city_municipality_id;
			$household->barangay_id = $barangay_id;
			$household->street = $record->Street . " Purok " .$record->Purok;
			$household->area_code = $record->AreaCode;
			$household->house_no = $record->HouseNo; 
			$household->owner_name = $record->OwnerName;
			$household->notes = $record->Notes;
			$household->consumer_name = $record->ConsumerName;

			$household->save();


			$housemates_sql = "select * from ". $city['db_name'] .".records as r join ". $city['db_name'] .".barangay as b on b.ID = r.Residence left join ". $city['db_name'] .".religion on religion.OID = r.Religion where r.Residence = :Residence and r.Street = :Street and r.HouseNo = :HouseNo and r.AreaCode = :AreaCode and r.OwnerName = :OwnerName";
			
			$housemates_record = DB::select(DB::raw($housemates_sql), array(
        				'Residence' => $record->Residence,
        				'Street' => $record->Street,
        				'HouseNo' => $record->HouseNo,
        				'AreaCode' => $record->AreaCode,
        				'OwnerName' => $record->OwnerName
        			));

			$total_house_resident = count($housemates_record);
			$hr_counter = 0;

			foreach ($housemates_record as $hr) {


				$middle_name = $hr->MLastName;
				$last_name = $hr->FLastName;

				$birthdate = date("Y-m-d", strtotime($hr->Bday));

				if($hr->Gender == "F"){
					if($hr->SLastName != ""){
						$middle_name = $hr->FLastName;
						$last_name = $hr->SLastName;

					}
				}



				$resident = Resident::where('first_name', $hr->FirstName)
									->where('middle_name', $middle_name)
									->where('last_name', $last_name)
									->where('barangay_id', $barangay_id)
									->where('birthdate', $birthdate)
									->where('city_municipality_id', $city_municipality_id)
									->first();




			

				$father = Resident::where('first_name', $hr->FFirstName)
								  ->where('last_name', $hr->FLastName)
								  ->where('city_municipality_id', $city_municipality_id)
								  ->first();

				$mother = Resident::where('first_name', $hr->MFirstName)
								  ->where('last_name', $hr->MLastName)
								  ->where('city_municipality_id', $city_municipality_id)
								  ->first();

				$spouse = Resident::where('first_name', $hr->SFirstName)
								  ->where('last_name', $hr->SLastName)
								  ->where('city_municipality_id', $city_municipality_id)
								  ->first();

				if(!$resident){
					if($hr->Gender == "F"){

						$resident = Resident::where('first_name', $hr->FirstName)
						->where('middle_name', $hr->MLastName)
						->where('last_name', $hr->FLastName)
						->where('barangay_id', $barangay_id)
						 ->where('barangay_id', $barangay_id)
						->where('city_municipality_id', $city_municipality_id)
						->first();
					}

				}

				if($resident){
				
				$hr_counter++;
				$this->info($hr_counter . "/" . $total_house_resident . "-" . $resident->id . " ACCESS REF:" . $hr->OID);

				if($resident->gender != "Male" && $resident->gender != "Female"){
					if($hr->Gender == "F"){
						$resident->gender = "Female";
					}
					$resident->gender = "Male";
				}
					
				$resident->occupation = $hr->SEStatus;
				$resident->nickname = $hr->NickName;

				$resident->save();

				if($father){
					
					// Father
					$father_inputs = array('resident_id' => $father->id, 'relation' => 'father');
					$this->profilingService->execute('AddResidentRelation', $father_inputs, $resident, $creator);
				}

				if($mother){

					// Mother
					$mother_inputs = array('resident_id' => $mother->id, 'relation' => 'mother');
					$this->profilingService->execute('AddResidentRelation', $mother_inputs, $resident, $creator);

				}


				if($spouse){

					// spouse
					$spouse_inputs = array('resident_id' => $spouse->id, 'relation' => 'spouse');
					$this->profilingService->execute('AddResidentRelation', $spouse_inputs, $resident, $creator);

				}


				// Household
				$household_input = array('household_id' => $household->id, 'role' => 'member');
				$this->profilingService->execute('AddResidentHousehold', $household_input, $resident, $creator);

				}

			}
			$counter++;
			$this->info($city['name'] . " : " . $counter . "/" . $total);
		}
	}
}


}
