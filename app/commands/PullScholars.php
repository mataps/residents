<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;
class PullScholars extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'pull-scholars';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Will copy all old scholars from old db.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(ScholarshipManagementInterface $scholars)
	{
		parent::__construct();

		$this->scholars = $scholars;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->scholars->execute('PullScholars', array());
	}


}
