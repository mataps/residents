<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use SourceScript\TransactionSystem\TransactionSystemInterface;
class PullTransactions extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'pull-transactions';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(TransactionSystemInterface $transactions)
	{
		parent::__construct();
		$this->transactions = $transactions;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->transactions->execute('PullTransaction', array());
	}



}
