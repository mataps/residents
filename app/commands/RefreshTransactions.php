<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use SourceScript\TransactionSystem\TransactionSystemInterface;

class RefreshTransactions extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'refresh:transactions';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * @var TransactionSystemInterface
	 */
	private $transactionSystemService;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct(TransactionSystemInterface $transactionSystemInterface)
	{
		parent::__construct();
		$this->transactionSystemService = $transactionSystemInterface;
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		if($this->option('date'))
		{
			$transactions = Transaction::where('created_at', '<', $this->option('date'). ' 23:59:59')
				->where('created_at', '>', $this->option('date') . ' 00:00:00')->get();

			$count = $transactions->count();

			foreach($transactions as $index => $transaction)
			{
				$replacementDate = date("Y-m-d H:i:s", strtotime(substr($transaction->uuid, 0, 8)));

				$transaction->timestamps = false;
				$transaction->setCreatedAt($replacementDate);

				$transaction->save();
				$this->transactionSystemService->execute('IndexTransaction', [], $transaction);

				$this->info($index + 1 . "/" . $count . " Transactions updated");
			}
		}

        if($this->option('account_type_id'))
        {
            $accountTypeId = $this->option('account_type_id');


            $vouchers = Voucher::where('account_type_id', $accountTypeId)->orderBy('created_at', 'asc')->get();

            $count = $vouchers->count();

            foreach($vouchers as $index => $voucher)
            {
                $voucher->generateUuid();

                $transactions = $voucher->transactions;

                foreach($transactions as $transaction)
                {
                    $transaction->generateUuid();
                    $this->transactionSystemService->execute('IndexTransaction', [], $transaction);
                }

                $this->info($index + 1 . "/" . $count . " Vouchers updated");
            }
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			// array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('date', null, InputOption::VALUE_OPTIONAL, 'Transaction date to refresh.', null),
            array('account_type_id', null, InputOption::VALUE_OPTIONAL, 'Voucher account type to refresh uuid.', null)
		);
	}

}
