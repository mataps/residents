<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ResidentNotesMigrate extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'migrate-resident-notes';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$sql = "select r.first_name, r.maternal_name, r.last_name, concat_ws(' ', r.first_name, r.maternal_name, r.last_name) as full_name, r.notes, b.barangay_name as barangay, m.municipality_name as city from cdodb_base.resident as r join cdodb_base.barangay as b on b.OID = r.residence_id join cdodb_base.municipality as m on m.OID = b.municipality_id  where r.notes is not null OR r.notes != ''";

		$resident_record = DB::select(DB::raw($sql));

		$total = count($resident_record);
		$counter = 0;

		foreach($resident_record as $record){
		

			$counter++;
			$this->info($counter . "/" . $total . " - " . $record->full_name . " from " . $record->barangay . " " . $record->city);

			$resident = Resident::where('full_name', $record->full_name)
								->whereHas('barangay', function($subQuery) use($record)
	                            {
	                                $subQuery->where('name', $record->barangay);
	                            })
                                
                                 ->whereHas('cityMunicipality', function($subQuery)  use($record)
                                {
                                    $subQuery->where('name', $record->city);
                                })                               
                             
                                ->first();
             if($resident){
            
             	       $resident->notes = $record->notes;
           			  $resident->save();

           			  $this->info("Success..");
             }
             else{
 					$this->error("FAIL");
             }



		}


	}


}
