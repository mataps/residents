<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SetBarangayNames extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'barangay_name:set';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'This will pull the name of the barangay from server 10.0.10.11.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$cities = CityMunicipality::groupBy('name')->get();
		

		foreach($cities as $city){
			//get all barangays
			// $count = CityMunicipality::where('name', $city->name)->count();
			// if($count > 1){
			$this->info('Fetching City Code..');
				Config::set("database.connections.comelec.database", 'ref');
				$connection = DB::connection('comelec');
				$connection->setDatabaseName('allmun');
				$connection->reconnect();
			
				$city_fetched_name = $connection->table('allmun')->where('ID_CITY', '49' . $city->name )->first()->AREANAME;
				$this->info('City Fetched ' . $city_fetched_name);

			

			$this->info('Eliminating Duplicates for ' . $city->name .'...');
			// $results = DB::select("UPDATE residents JOIN cities_municipalities ON residents.city_municipality_id = cities_municipalities.id SET residents.city_municipality_id = " . $city->id  ." WHERE cities_municipalities.name='". $city->name . "'");
			DB::table('residents')
				->join('cities_municipalities', 'residents.city_municipality_id', '=', 'cities_municipalities.id')
				->where('cities_municipalities.name', '=', $city->name)
				->update(array('residents.city_municipality_id' => $city->id));


			// $results = DB::select("UPDATE barangays JOIN cities_municipalities ON barangays.city_municipality_id = cities_municipalities.id SET barangays.city_municipality_id = " . $city->id  ." WHERE cities_municipalities.name='". $city->name . "'");
			
			DB::table('barangays')
				->join('cities_municipalities', 'barangays.city_municipality_id', '=', 'cities_municipalities.id')
				->where('cities_municipalities.name', '=', $city->name)
				->update(array('barangays.city_municipality_id' => $city->id));

			$this->info('DELETING DUPLICATES');
			// $results = DB::select('DELETE from cities_municipalities where name = "' . $city->name . '" and `id` != '. $city->id );
			DB::table('cities_municipalities')
				->where('name', '=', $city->name)
				->where('id', '!=', $city->id)
				->delete();


			DB::table('cities_municipalities')
				->where('name', '=', $city->name)
				->update(array('name' => $city_fetched_name));

			// }	

			$barangays = Barangay::where('city_municipality_id', $city->id)->where('name', 'like', '%0%')->groupBy('name')->get();
			// dd($barangays);
			foreach($barangays as $barangay_item){



				$this->info('City Name : ' . $city->name);
				$this->info('Barangay Code : ' . $barangay_item->name);
				$barangay_code = $barangay_item->name;

				$this->info('Getting the first id');

				$city_name = $city_fetched_name;
				$city_id = $city->id;
				$barangay_count = Barangay::where('city_municipality_id', $city_id)
									->where('name', $barangay_code)
									->count();
				if($barangay_count){
					$this->info($barangay_count . ' of barangay record found');
					$barangay = Barangay::where('city_municipality_id', $city_id)
									->where('name', $barangay_code)
									->first();
				}
				else{
					$this->error('No Record Found');
					$this->info('Creating New Record');
					$barangay = new Barangay();
					$barangay->name = $barangay_code;
					$barangay->city_municipality_id = $city_id;
					$barangay->save();
					$this->info('New Record is ' . $barangay->id);
					
				}

				$this->info('Will be using id number: ' . $barangay->id . ' for resident relationships');
				$this->info('Pointing Relationships ...');
				// $results = DB::select("UPDATE residents JOIN barangays ON residents.barangay_id = barangays.id SET residents.barangay_id = '" . $barangay->id  ."' WHERE barangays.name='". $barangay_code . "' AND barangays.city_municipality_id = " . $city_id);
				
				$count_of_resident = DB::table('residents')
										->join('barangays', 'residents.barangay_id', '=', 'barangays.id')
										->where('barangays.name', '=', $barangay_code)
										->count();

				$this->info('Updating ' . $count_of_resident . ' of records..');
				DB::table('residents')
				->join('barangays', 'residents.barangay_id', '=', 'barangays.id')
				->where('barangays.name', '=', $barangay_code)
				->where('residents.city_municipality_id', '=', $city_id)
				->update(array('residents.barangay_id' => $barangay->id));


				$this->info('Eliminating Duplicates ...');
				// $results = DB::select('DELETE from barangays where name = "' . $barangay_code . '" and `city_municipality_id` = '. $city_id .' and id > ' . $barangay->id);
				DB::table('barangays')
				->where('name', '=', $barangay_code)
				->where('city_municipality_id', '=', $city->id)
				->where('id', '!=', $barangay->id)
				->delete();

				$this->info('Fetching City Code for ' . $city_name);
				Config::set("database.connections.comelec.database", 'ref');
				$connection = DB::connection('comelec');
				$connection->setDatabaseName('allmun');
				$connection->reconnect();
				if($city_name == 'Gen Natividad'){ $city_name = 'GENERAL MAMERTO NATIVIDAD'; }
				if($city_name == 'Gen Tinio'){ $city_name = 'GENERAL TINIO'; }
			
				$city_code = $connection->table('allmun')->where('AREANAME', 'like', '%' . strtoupper($city_name) . '%')->first()->ID_CITY;
				$this->info('City Fetched ' . $city_code);
				// $data = DB::select('SELECT * from allmun limit 0, 1');
				$bgy_id = $city_code . $barangay_code;

				$brgy_name = $connection->table('allbgy')->where('ID_BARANGAY', $bgy_id)->first()->AREANAME;
				
				$this->info('Fetching Barangay Name');
				$this->info('Barangay Name : '. $brgy_name);
				$barangay->name = $brgy_name;
				$barangay->save();
				$this->info('Done');	
			}
			
		}


	}


}
