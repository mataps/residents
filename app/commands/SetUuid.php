<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SetUuid extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'set-uuid';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$semesters = Semester::orderBy('created_at', 'ASC')->get();
		$total = count($semesters);
		$counter = 0;
		foreach ($semesters as $semester) {

			$semester->generateUuid();

			$counter++;
			$this->info($counter . "/" . $total);
		}
	}



}
