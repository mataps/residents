<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\TransactionSystem\TransactionSystemInterface;
use SourceScript\Common\Collections\ResultCollection;

class Api_AccountsTransactionsController extends ApiController {


	/**
	 * @var TransactionSystemInterface
	 */
	private $transactionManagementService;


	/**
	 * @var TransactionTransformer
	 */
	private $transactionTransformer;


	function __construct(TransactionSystemInterface $transactionManagementInterface, TransactionTransformer $transactionTransformer)
	{
		$this->transactionManagementService = $transactionManagementInterface;
		$this->transactionTransformer = $transactionTransformer;
	}


	/**
	 * Display transactions per account
	 * 
	 * @param  TransactionAccount $account
	 * @return Response
	 */
	public function index(TransactionAccount $account)
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$transactions = $this->transactionManagementService->findTransactionsByAccount($account, $fields, $pagination, $sort, $filters);

		return $this->respond($this->transactionTransformer->transformCollection($transactions, $fields, $pagination, $sort));
	}


	/**
	 * Displays transactions per accounts
	 * 
	 * @param  ResultCollection $accounts
	 * @return Response
	 */
	public function multiple(ResultCollection  $accounts)
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$transactions = $this->transactionManagementService->findTransactionsByAccounts($accounts, $fields, $pagination, $sort, $filters);

		return $this->respond($this->transactionTransformer->transformCollection($transactions, $fields, $pagination, $sort));
	}
}