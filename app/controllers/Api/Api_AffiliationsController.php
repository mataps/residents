<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Profiling\ProfilingInterface;
use SourceScript\Common\Collections\ResultCollection;

class Api_AffiliationsController extends ApiController {

    /**
     * @var ProfilingInterface
     */
    private $profilingService;

    /**
     * @var AffiliationTransformer
     */
    private $affiliationTransformer;


    function __construct(
    	ProfilingInterface $profilingService,
    	AffiliationTransformer $affiliationTransformer)
    {
        $this->profilingService = $profilingService;
        $this->affiliationTransformer = $affiliationTransformer;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = new PaginationParameters(Input::get('page'), Input::get('limit'));

        $filters = new FilterParameters(Input::get('filters'));

        $affiliations = $this->profilingService->findAffiliations($fields, $pagination, $sort, $filters);

        return $this->respond($this->affiliationTransformer->transformCollection($affiliations, $fields, $pagination, $sort));
    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $inputs = Input::only([
        	'name',
        	'founded',
        	'description',
        	'district',
            'cityMunicipality',
            'barangay',
            'street',
        	]);

        $creator = Auth::user();

        $group = $this->profilingService->execute('AddAffiliation', $inputs, $creator);

        return $this->respondData($this->affiliationTransformer->transform($group));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  Affiliation  $group
	 * @return Response
	 */
	public function show(Affiliation $group)
	{
		return $this->affiliationTransformer->transform($group);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Affiliation  $group
	 * @return Response
	 */
	public function update(Affiliation $group)
	{
        $inputs = Input::only([
        	'name',
        	'description',
        	'founded',
        	'district',
            'cityMunicipality',
            'barangay',
            'street'
        	]);

       	$creator = Auth::user();

        $group = $this->profilingService->execute('UpdateAffiliation', $inputs, $group, $creator);

        return $this->respondData($this->affiliationTransformer->transform($group));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  Affiliation  $group
	 * @return Response
	 */
	public function destroy(Affiliation $group)
	{
        $inputs = Input::all();

        $this->profilingService->execute('RemoveAffiliation', $inputs, $group);

        return $this->respondOk('Affiliation deleted');
	}


    /**
     * Remove the specified resources from storage.
     * 
     * @param  ResultCollection $affiliations
     * @return Reponse
     */
    public function massDelete(ResultCollection $affiliations)
    {
        $inputs = Input::all();

        $this->profilingService->execute('RemoveAffiliations', $inputs, $affiliations);

        return $this->respondOk('Affiliations deleted');
    }

    /**
     * Prints listing of affiliation members
     * @param  Affiliation $affiliation
     * @return Response
     */
    public function printPdf(Affiliation $affiliation, CityMunicipality $cityMunicipality)
    {
        $filters = new FilterParameters(Input::get('filters'));
        $data['disable_page_break'] = Input::get('disable_page_break', false);
        // dd($data);
        $data['affiliation'] = $affiliation;
        $residents = $this->profilingService->findResidentsByAffiliationAndMunicipality($affiliation, $cityMunicipality, null, null, null, $filters);

        $barangays = $residents->lists('barangay_id');
        // dd($barangays);

        $data['barangays'] = [];
        foreach($barangays as $barangay)
        {
            $data['barangays'][$barangay] = $residents->filter(function($item) use ($barangay)
            {
                return $item->barangay_id == $barangay;
            });          
        }
        
        // dd($data);
        return $this->respondPdf($data, 'pdf.affiliationMembers', $affiliation->fullName(), 'portrait');
    }
}
