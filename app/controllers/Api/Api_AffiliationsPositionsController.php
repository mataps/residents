<?php

use Illuminate\Support\Collection;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Profiling\ProfilingInterface;

class Api_AffiliationsPositionsController extends ApiController {


	/**
	 * @var ProfilingInterface
	 */
	private $profilingService;


	/**
	 * @var PositionTransformer
	 */
	private $positionTransformer;


	function __construct(
		AffiliationPositionTransformer $positionTransformer,
		ProfilingInterface $profilingInterface)
	{
		$this->positionTransformer = $positionTransformer;
		$this->profilingService = $profilingInterface;
	}


	/**
	 * Display a listing of resource.
	 * 
	 * @param  Affiliation $affiliation
	 * @return Response
	 */
	public function index(Affiliation $affiliation)
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$positions = $this->profilingService->findPositionsByAffiliation($affiliation, $fields, $pagination, $sort, $filters);

		return $this->respond($this->positionTransformer->transformCollection($positions, $fields, $pagination, $sort));
		}


	/**
	 * Stores a resource.
	 * 
	 * @return Response
	 */
	public function store(Affiliation $affiliation)
	{
		$inputs = Input::only([
			'positions'
			]);

		$creator = Auth::user();

		$positions = $this->profilingService->execute('AddAffiliationPositions', $inputs, $affiliation, $creator);

		return $this->respondData($this->positionTransformer->transformCollection($positions));
	}
}