<?php

use SourceScript\Common\Collections\ResultCollection;
use SourceScript\Profiling\ProfilingInterface;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;

class Api_AffiliationsResidentsController extends ApiController {

	/**
	 * @var ResidentAffiliationTransformer
	 */
	private $residentAffiliationTransformer;

	private $profilingService;

	function __construct(
		ProfilingInterface $profilingInterface,
		ResidentAffiliationTransformer $residentAffiliationTransformer)
	{
		$this->profilingService = $profilingInterface;
		$this->residentAffiliationTransformer = $residentAffiliationTransformer;
	}

	/**
	 * Display a listing of the resource.
	 * 
	 * @param  Affiliation $affiliation
	 * @return Response
	 */
	public function index(Affiliation $affiliation)
	{
		$sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = new PaginationParameters(Input::get('page'), Input::get('limit'));

        $filters = new FilterParameters(Input::get('filters'));

        $residentAffiliations = $this->profilingService->findResidentsByAffiliation($affiliation, $fields, $pagination, $sort, $filters);

        return $this->respond($this->residentAffiliationTransformer->transformCollection($residentAffiliations, $fields, $pagination, $sort, ['affiliation']));
	}

	public function store(Affiliation $affiliation)
	{
		$inputs = Input::only([
			'position_id',
			'from',
			'to',
			'founder',
			'referrer_id',
			'resident_id',
			'remarks',
			'status',
			'head_id'
			]);

		$creator = Auth::user();

		$residentAffilation = $this->profilingService->execute('AddAffiliationResident', $inputs, $affiliation, $creator);

		return $this->respondData($this->residentAffiliationTransformer->transform($residentAffilation));
	}


	public function massStore(ResultCollection $affiliations)
	{
		$inputs = Input::only([
			'position_id',
			'from',
			'to',
			'founder',
			'referrer_id',
			'resident_id',
			'remarks',
			'status',
			'head_id'
			]);

		$creator = Auth::user();

		$residentAffilation = $this->profilingService->execute('AddAffiliationsResident', $inputs, $affiliations, $creator);

		return $this->respondOk('Resident added to affiliation');
	}


	/**
	 * @param  Affiliation $affiliation
	 * @param  Resident    $resident
	 * @return Response
	 */
	public function update(Affiliation $affiliation, Resident $resident)
	{
		$inputs = Input::only([
			'position_id',
			'from',
			'to',
			'founder',
			'referrer_id',
			'remarks',
			'status',
			'head_id'
			]);

		$updater = Auth::user();

		$residentAffilation = $this->profilingService->execute('UpdateResidentAffiliation', $inputs, $resident, $affiliation, $updater);

		return $this->respondOk('Resident affiliation updated');
	}


	public function destroy(Affiliation $affiliation, Resident $resident)
	{
		$inputs = Input::all();

		$this->profilingService->execute('RemoveResidentAffiliation', $inputs, $resident, $affiliation);

		return $this->respondOk('Resident removed');
	}
}