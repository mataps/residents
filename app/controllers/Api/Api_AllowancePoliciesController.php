<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;

class Api_AllowancePoliciesController extends ApiController {

	/**
	 * @var ScholarshipManagementInterface
	 */
	private $scholarshipManagementService;


	/**
	 * @var AllowancePolicyFormattedTransformer
	 */
	private $allowancePolicyTransformer;


	function __construct(ScholarshipManagementInterface $scholarshipManagementInterface, AllowancePolicyFormattedTransformer $allowancePolicyTransformer)
	{
		$this->scholarshipManagementService = $scholarshipManagementInterface;
		$this->allowancePolicyTransformer = $allowancePolicyTransformer;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$policies = $this->scholarshipManagementService->findScholarshipTypes($fields, $pagination, $sort, $filters);

		return $this->respond($this->allowancePolicyTransformer->transformCollection($policies, $fields, $pagination, $sort));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::only([
			'id',
			'allowance_policies'
			]);

		$creator = Auth::user();

		$this->scholarshipManagementService->execute('AddPolicy', $inputs, $creator);
		
		return $this->respondOk('Allowance policies saved');
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(AllowancePolicy $policy)
	{
		return $this->allowancePolicyTransformer->transform($policy);
	}
}