<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;
Use SourceScript\Common\Collections\ResultCollection;

class Api_AllowancePolicyOverridesController extends ApiController {

	/**
	 * @var ScholarshipManagementInterface
	 */
	private $scholarshipManagementService;


	/**
	 * @var AllowancePolicyOverrideTransformer
	 */
	private $policyOverrideTransformer;


	function __construct(
		ScholarshipManagementInterface $scholarshipManagementInterface,
		AllowancePolicyOverrideTransformer $policyOverrideTransformer)
	{
		$this->scholarshipManagementService = $scholarshipManagementInterface;
		$this->policyOverrideTransformer = $policyOverrideTransformer;
	}


	/**
	 * Displays a listing of resource.
	 * 
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$policyOverrides = $this->scholarshipManagementService->findPolicyOverrides($fields, $pagination, $sort, $filters);

		return $this->respond($this->policyOverrideTransformer->transformCollection($policyOverrides, $fields, $pagination, $sort));
	}


	/**
	 * Displays a specific resource.
	 * 
	 * @param  AllowancePolicyOverride $policyOverride
	 * @return Response
	 */
	public function show(AllowancePolicyOverride $policyOverride)
	{
		return $this->policyOverride->transform($policyOverride);
	}


	/**
	 * Stores a resource.
	 * 
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::only([
			'occurence',
			'amount',
			'scholarship_types'
			]);

		$creator = Auth::user();

		$policyOverride = $this->scholarshipManagementService->execute('AddPolicyOverride', $inputs, $creator);

		return $this->respondData($policyOverride);
	}


	/**
	 * Removes a specified resource.
	 * 
	 * @param  AllowancePolicyOverride $policyOverride
	 * @return Response
	 */
	public function destroy(AllowancePolicyOverride $policyOverride)
	{
		$inputs = Input::all();

		$this->scholarshipManagementService->execute('RemovePolicyOVerride', $inputs, $policyOverride);

		return $this->respondData('Policy Override removed!');
	}


	/**
	 * Removes specified resources.
	 * 
	 * @param  ResultCollection $policyOverrides
	 * @return Response
	 */
	public function massDelete(ResultCollection $policyOverrides)
	{
		$inputs = Input::all();

		$this->scholarshipManagementService->execute('RemovePolicyOVerrides', $inputs, $policyOverrides);

		return $this->respondData('Policy Overrides removed!');		
	}
}