<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;

class Api_AllowanceReleaseController extends ApiController {

	/**
	 * @var ScholarshipManagementInterface
	 */
	private $ScholarshipManagementInterface;

	/**
	 * @var AllowanceReleaseTransformer
	 */
	private $allowanceReleaseTransformer;


	function __construct(
		ScholarshipManagementInterface $scholarshipManagementInterface,
		AllowanceReleastTransformer $allowanceReleaseTransformer)
	{
		$this->scholarshipManagementService = $scholarshipManagementInterface;
		$this->allowanceReleaseTransformer = $allowanceReleaseTransformer;
	}

	/**
	 * Display a listing of the resource.
	 * 
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

		$sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        $allowanceReleases = $this->scholarshipManagementService->findAllowanceReleases($fields, $pagination, $sort, $filters);

        return $this->respond($this->allowanceReleaseTransformer->transformCollection($allowanceReleases, $fields, $pagination, $sort));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::only([
			'term',
			'school_year',
			'fixed_amount',
			'amount',
			'by_scholarship_type',
			'scholarship_type_id'
			]);

		$creator = Auth::user();

		$allowanceRelease = $this->scholarshipManagementService->execute('AddAllowanceRelease', $inputs, $user);

		return $this->respondData($this->allowanceReleaseTransformer->transform($allowanceRellease));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(AllowanceRelease $allowanceRelease)
	{
		return $this->allowanceReleaseTransformer->transform($allowanceRelease);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(AllowanceRelease $allowanceRelease)
	{
		// allowance release cant be editted
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(AllowanceRelease $allowanceRelease)
	{
		$inputs = Input::all();


		$this->scholarshipManagementService->execute('RemoveAllowanceRelease', $inputs);

		return $this->respondOk('Allowance release deleted!');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function massDelete(ResultCollection $allowanceReleases)
	{
		$inputs = Input::all();

		$this->scholarshipManagementService->execute('RemoveAllowanceReleases', $inputs);

		return $this->respondOk('Allowance releases deleted!');
	}

}