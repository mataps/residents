<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;

class AllowanceReleasesAllowanceController extends ApiController {

	/**
	 * @var ScholarshipManagementInterface
	 */
	private $scholarshipManagementService;


	/**
	 * @var AllowanceTransformer
	 */
	private $allowanceTransformer;


	function __construct(ScholarshipManagementInterface $scholarshipManagementInterface,
		AllowanceTransformer $allowanceTransformer)
	{
		$this->scholarshipManagementService = $scholarshipManagementInterface;
		$this->allowanceTransformer = $allowanceTransformer;
	}


	/**
	 * Display a listing of the resource.
	 * 
	 * @return Response
	 */
	public function index(AllowanceRelease $allowanceRelease)
	{
		$inputs = Input::all();

		$sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        $allowances = $this->scholarshipManagementService->findAllowancesByAllowanceRelease($allowanceRelease, $fields, $pagination, $sort, $filters);

        return $this->respond($this->allowanceTransformer->transformCollection($allowances, $fields, $pagination, $sort));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */	
	public function store(AllowanceRelease $allowanceRelease)
	{
		/**
		 * Add allowance
		 */
	}
}