<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;

class Api_AllowanceReleasesController extends ApiController {

	/**
	 * @var ScholarshipManagementInterface
	 */
	private $ScholarshipManagementInterface;

	/**
	 * @var AllowanceReleaseTransformer
	 */
	private $allowanceReleaseTransformer;


	function __construct(
		ScholarshipManagementInterface $scholarshipManagementInterface,
		AllowanceReleaseTransformer $allowanceReleaseTransformer)
	{
		$this->scholarshipManagementService = $scholarshipManagementInterface;
		$this->allowanceReleaseTransformer = $allowanceReleaseTransformer;
	}

	/**
	 * Display a listing of the resource.
	 * 
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

		$sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        $allowanceReleases = $this->scholarshipManagementService->findAllowanceReleases($fields, $pagination, $sort, $filters);

        return $this->respond($this->allowanceReleaseTransformer->transformCollection($allowanceReleases, $fields, $pagination, $sort));
	}

	public function reportsPdf(AllowanceRelease $allowanceRelease)
	{
		$inputs = Input::only(['claimed']);
		$data['release'] = $allowanceRelease;
		$data['barangays'] = [];

		$request = json_decode(Input::get('filters'));

		$cityMunicipality = isset($request->cityMunicipality) ? $request->cityMunicipality : "";
		$barangay = isset($request->barangay) ? $request->barangay : "";
		$created_at_start = isset($request->created_at_start) ? date('Y-m-d', strtotime($request->created_at_start)) : date('Y-m-d', strtotime('1970-01-01'));
		$created_at_end = isset($request->created_at_end) ? date('Y-m-d', strtotime($request->created_at_end)) : date('Y-m-d');


		$created_at = array( $created_at_start, $created_at_end);

		if($cityMunicipality != "" && $barangay == ""){
			$barangays = Barangay::where('city_municipality_id', $cityMunicipality->id)->get();
		}
		else if($cityMunicipality == "" && $barangay != ""){
			$barangays = Barangay::where('id', $barangay->id)->get();
		
		}
		elseif ($cityMunicipality != "" && $barangay != "") {
			$barangays = Barangay::where('id', $barangay->id)->where('city_municipality_id', $cityMunicipality->id)->get();
		
		}
		else{
			$barangays = Barangay::all();
		}

	


		foreach($barangays as $barangay)
		{
			$barangaySems = $semesters = $this->scholarshipManagementService->findSemestersByAllowanceReleaseAndBarangay($allowanceRelease, $barangay, null, null, null, null, $created_at);
		
			if($barangaySems->count() > 0)
			{
				$data['barangays'][$barangay->id]['barangay'] = $barangay;
				$data['barangays'][$barangay->id]['cityMunicipality'] = $barangay->cityMunicipality;
				$data['barangays'][$barangay->id]['semesters'] = $barangaySems;
				$total = 0;
				foreach($barangaySems as $barangaySem)
				{
					$total = $total + $barangaySem->computeAllowance($allowanceRelease);
				}
				$data['barangays'][$barangay->id]['total'] = $total;	
			}
		}

		return $this->respondPdf($data, 'pdf.allowanceReleaseReport', 'report.pdf', 'landscape', 'legal');
	}

	public function reportsSpreadsheet(AllowanceRelease $allowanceRelease)
	{
		$inputs = Input::only(['claimed']);
		$data['release'] = $allowanceRelease;
		$data['barangays'] = [];

		$request = json_decode(Input::get('filters'));

		$cityMunicipality = isset($request->cityMunicipality) ? $request->cityMunicipality : "";
		$barangay = isset($request->barangay) ? $request->barangay : "";
		$created_at_start = isset($request->created_at_start) ? date('Y-m-d', strtotime($request->created_at_start)) : date('Y-m-d', strtotime('1970-01-01'));
		$created_at_end = isset($request->created_at_end) ? date('Y-m-d', strtotime($request->created_at_end)) : date('Y-m-d');


		$created_at = array( $created_at_start, $created_at_end);

		if($cityMunicipality != "" && $barangay == ""){
			$barangays = Barangay::where('city_municipality_id', $cityMunicipality->id)->get();
		}
		else if($cityMunicipality == "" && $barangay != ""){
			$barangays = Barangay::where('id', $barangay->id)->get();
		
		}
		elseif ($cityMunicipality != "" && $barangay != "") {
			$barangays = Barangay::where('id', $barangay->id)->where('city_municipality_id', $cityMunicipality->id)->get();
		
		}
		else{
			$barangays = Barangay::all();
		}

	


		foreach($barangays as $barangay)
		{
			$barangaySems = $semesters = $this->scholarshipManagementService->findSemestersByAllowanceReleaseAndBarangay($allowanceRelease, $barangay, null, null, null, null, $created_at);
			if($barangaySems->count() > 0)
			{
				$data['barangays'][$barangay->id]['barangay'] = $barangay;
				$data['barangays'][$barangay->id]['cityMunicipality'] = $barangay->cityMunicipality;
				$data['barangays'][$barangay->id]['semesters'] = $barangaySems;
				$total = 0;
				foreach($barangaySems as $barangaySem)
				{
					$total = $total + $barangaySem->computeAllowance($allowanceRelease);
				}
				$data['barangays'][$barangay->id]['total'] = $total;	
			}
		}

		Excel::create("Allowance_Relase_Report", function($excel) use ($data)
        {
            $excel->setTitle("Allowance Release Report");

            $excel->setCreator('Galactus')->setCompany('SourceScript');

            $excel->setDescription("allowance release report.");

           foreach ($data['barangays'] as $set) {
		//$set = $data['barangays'][317];
            	$set['release'] = $data['release'];
        	    	
            	 $excel->sheet($set['barangay']->name, function($sheet) use ($set)
		            {
		                $sheet->loadView('spreadsheet.allowanceReleaseReport')->with($set);
		            });
            }

           
        })->export('xls');

		// return $this->respondExcel($data, 'spreadsheet.allowanceReleaseReport', 'Allowance release report', 'Allowance reelease report');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::only([
			'term',
			'school_year',
			'fixed_amount',
			'amount',
			'scholarship_types',
			'expiring_at'
			]);

		$creator = Auth::user();

		$allowanceRelease = $this->scholarshipManagementService->execute('AddAllowanceRelease', $inputs, $creator);

		return $this->respondData($this->allowanceReleaseTransformer->transform($allowanceRelease));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(AllowanceRelease $allowanceRelease)
	{
		return $this->allowanceReleaseTransformer->transform($allowanceRelease);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(AllowanceRelease $allowanceRelease)
	{
		$inputs = Input::all();


		$this->scholarshipManagementService->execute('RemoveAllowanceRelease', $inputs);

		return $this->respondOk('Allowance release deleted!');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function massDelete(ResultCollection $allowanceReleases)
	{
		$inputs = Input::all();

		$this->scholarshipManagementService->execute('RemoveAllowanceReleases', $inputs);

		return $this->respondOk('Allowance releases deleted!');
	}
}
