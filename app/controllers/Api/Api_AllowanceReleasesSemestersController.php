<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;
use SourceScript\Common\Collections\ResultCollection;

class Api_AllowanceReleasesSemestersController extends ApiController {

	function __construct(ScholarshipManagementInterface $scholarshipManagementInterface,
		SemesterTransformer $semesterTransformer)
	{
		$this->scholarshipManagementService = $scholarshipManagementInterface;
		$this->semesterTransformer = $semesterTransformer;
	}

	public function index(AllowanceRelease $allowanceRelease)
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        $semesters = $this->scholarshipManagementService->findSemestersByAllowanceRelease($allowanceRelease, $fields, $pagination, $sort, $filters);

        return $this->respond($this->semesterTransformer->transformCollection($semesters, $fields, $pagination, $sort));
	}

	public function bulkClaimAllowance(AllowanceRelease $allowanceRelease, ResultCollection $semesters)
	{
		$inputs = Input::all();

		$user = Auth::user();

		$semesters = $this->scholarshipManagementService->execute('AddAllowances', $inputs, $allowanceRelease, $semesters, $user);

		$data = [
			'successful' => $this->semesterTransformer->transformCollection($semesters['success']),
			'failed' => $this->semesterTransformer->transformCollection($semesters['fail'])
			];

		return $this->respondData($data);
	}
}
