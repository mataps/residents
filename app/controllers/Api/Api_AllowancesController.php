<?php

use SourceScript\Common\Collections\ResultCollection;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;

class Api_AllowancesController extends ApiController {

	/**
	 * @var ScholarshipManagementService
	 */
	private $scholarshipManagementService;


	/**
	 * @var AllowanceTransformer
	 */
	private $allowanceTransformer;


	function __construct(
		ScholarshipManagementInterface $scholarshipManagementService,
		AllowanceTransformer $allowanceTransformer)
	{
		$this->scholarshipManagementService = $scholarshipManagementService;
		$this->allowanceTransformer = $allowanceTransformer;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	function index()
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        $allowances = $this->scholarshipManagementService->findAllowances($fields, $pagination, $sort, $filters);

        return $this->respond($this->allowanceTransformer->transformCollection($allowances, $fields, $pagination, $sort));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	function show(Allowance $allowance)
	{
		return $this->respondData($this->allowanceTransformer->transform($allowance));
	}

	function massDelete(ResultCollection $allowances)
	{
		$user = Auth::user();

		$this->scholarshipManagementService->execute('RemoveAllowances', [], $allowances, $user);

		return $this->respondOk('Allowances removed');
	}
}