<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Common\Collections\ResultCollection;
use SourceScript\Profiling\ProfilingInterface;

class Api_AttributionsController extends ApiController {

	/**
	 * @var ProfilingInterface
	 */
	private $profilingInterface;

	/**
	 * @var AttributionTransformer
	 */
	private $attributionTransformer;

	function __construct(
		ProfilingInterface $profilingInterface,
		AttributionTransformer $attributionTransformer)
	{
		$this->profilingService = $profilingInterface;
		$this->attributionTransformer = $attributionTransformer;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

		$sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        $attributions = $this->profilingService->findAttributions($fields, $pagination, $sort, $filters);

        return $this->respond($this->attributionTransformer->transformCollection($attributions, $fields, $pagination, $sort));
	}


	public function customSearch()
	{
		$inputs = Input::all();

		$sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $filters = new FilterParameters(Input::get('filters'));

        $attributions = $this->profilingService->findAttributions($fields, null, $sort, $filters);

        $results = [];

       	foreach($attributions as $attribution)
       	{
       		$results[]['text'] = $attribution->name;
       	}

        return $results;
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Attribution $attribution)
	{
		return $this->attributionTransformer->transform($attribution);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::only([
			'name',
			'label',
			'class'
			]);

		$creator = Auth::user();

		$attribution = $this->profilingService->execute('AddAttribution', $inputs, $creator);

		return $this->respondData($this->attributionTransformer->transform($attribution));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Attribution $attribution)
	{
		$inputs = Input::only([
			'name',
			'label',
			'class'
			]);

		$updater = Auth::user();

		$attribution = $this->profilingService->execute('UpdateAttribution', $inputs, $attribution, $updater);

		return $this->respondData($this->attributionTransformer->transform($attribution));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Attribution $attribution)
	{
		$inputs = Input::all();

		$this->profilingService->execute('RemoveAttribution', $inputs, $attribution);

		return $this->respondOk('Attribution Deleted');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function massDelete(ResultCollection $attributions)
	{
		$inputs = Input::all();

		$this->profilingService->execute('RemoveAttributions', $inputs, $attributions);

		return $this->respondOk('Attrubutions Deleted');
	}
}