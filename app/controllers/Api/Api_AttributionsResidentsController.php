<?php

use SourceScript\Profiling\ProfilingInterface;
use SourceScript\Common\Collections\ResultCollection;

class Api_AttributionsResidentsController extends ApiController {

	/**
	 * @var ProfilingInterface
	 */
	private $profilingService;


	function __construct(ProfilingInterface $profilingInterface)
	{
		$this->profilingService = $profilingInterface;
	}


	/**
	 * @param  ResultCollection $attributions
	 * @return Response
	 */
	public function massStore(ResultCollection $attributions)
	{
		$inputs = Input::only([
			'from',
			'to',
			'resident_id'
			]);

		$creator = Auth::user();

		$this->profilingService->execute('AddAttributionsResident', $inputs, $attributions, $creator);

		return $this->respondOk('Attributions added to resident');
	}
}