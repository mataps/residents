<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;

class Api_AwardsController extends ApiController {


	/**
	 * @var ScholarshipManagementService
	 */
	private $scholarshipManagementService;

	/**
	 * @var AwardsTransformer
	 */
	private $awardTransformer;


	function __construct(
		ScholarshipManagementInterface $scholarshipManagementService,
		AwardTransformer $awardTransformer)
	{
		$this->scholarshipManagementService = $scholarshipManagementService;
		$this->awardTransformer = $awardTransformer;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$awards = $this->scholarshipManagementService->findAwards($fields, $pagination, $sort, $filters);

		return $this->respond($this->awardTransformer->transformCollection($awards, $fields, $pagination, $sort));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::only([
			'name',
			'description',
			'attributions'
			]);

		$creator = Auth::user();

		$scholarship = $this->scholarshipManagementService->execute('AddAward', $inputs, $creator);

		return $this->respondData($this->awardTransformer->transform($scholarship));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Award $award)
	{
		return $this->awardTransformer->transform($award);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Award $award)
	{
		$inputs = Input::only([
			'name',
			'description',
			'attributions'
			]);

		// dd($inputs);

		$updater = Auth::user();

		$award = $this->scholarshipManagementService->execute('UpdateAward', $inputs, $award, $updater);

		return $this->respondData($this->awardTransformer->transform($award));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Award $award)
	{
		$inputs = Input::all();

		$this->scholarshipManagementService->execute('RemoveAward', $inputs, $award);

		return $this->repondOk('Award deleted');
	}

	
	public function remove($awards)
	{
		$inputs = Input::all();

		$this->scholarshipManagementService->execute('RemoveAwards', $inputs, $awards);

		return $this->respondOk('Awards deleted');
	}


	/**
	 * Type ahead search
	 * 
	 * @param  string $name
	 * @return Response
	 */
	public function searchName($name)
	{
		$results = $this->scholarshipManagementService->searchAwardsByName($name);

        return $this->respondData($results);
	}

	/**
	 * @param  Semester $semester
	 * @return Response
	 */
	public function semester(Semester $semester)
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$awards = $this->scholarshipManagementService->findAwardsBySemester($semester, $fields, $pagination, $sort, $filters);

		return $this->respond($this->awardTransformer->transformCollection($awards, $fields, $pagination, $sort));
	}
}
