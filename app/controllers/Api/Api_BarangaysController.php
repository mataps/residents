<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Common\Collections\ResultCollection;
use SourceScript\Profiling\ProfilingInterface;

class Api_BarangaysController extends ApiController {

	/**
	 * @var ProfilingInterface
	 */
	private $profilingService;


	/**
	 * @var BarangayTransformer
	 */
	private $barangayTransformer;


	function __construct(
		ProfilingInterface $profilingInterface,
		BarangayTransformer $barangayTransformer
		)
	{
		$this->barangayTransformer = $barangayTransformer;
		$this->profilingService = $profilingInterface;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

		$sort = new SortParameters(Input::get('sort'));

		$fields = new FieldsParameters(Input::get('fields'));

		$pagination = PaginationParameters::createFromArray($inputs);

		$filters = new FilterParameters(Input::get('filters'));

		$barangays = $this->profilingService->findBarangays($fields, $pagination, $sort, $filters);

		return $this->respond($this->barangayTransformer->transformCollection($barangays, $fields, $pagination, $sort));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Barangay $barangay)
	{
		return $this->barangayTransformer->transform($barangay);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::only([
			'name',
			'district_id'
			]);

		$creator = Auth::user();

		$barangay = $this->profilingService->execute('AddBarangay', $inputs, $creator);

		return $this->respondData($barangay);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Barangay $barangay)
	{
		$inputs = Input::only([
			'name',
			'district_id'
			]);

		$updater = Auth::user();

		$barangay = $this->profilingService->execute('UpdateBarangay', $inputs, $barangay, $updater);

		return $this->respondData($this->barangayTransformer->transform($barangay));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Barangay $barangay)
	{
		$inputs = Input::all();

		$this->profilingService->execute('RemoveBarangay', $inputs);

		return $this->respondOk('Barangay deleted');
	}
}