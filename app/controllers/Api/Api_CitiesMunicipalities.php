<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Common\Collections\ResultCollection;
use SourceScript\Profiling\ProfilingInterface;

class Api_CitiesMunicipalities extends ApiController {


	/**
	 * @var ProfilingInterface
	 */
	private $profilingService;


	/**
	 * @var CityMunicipalityTransformer
	 */
	private $cityMunicipalityTransformer;

	function __construct(
		ProfilingInterface $profilingInterface,
		CityMunicipalityTransformer $cityMunicipalityTransformer)
	{
		$this->cityMunicipalityTransformer = $cityMunicipalityTransformer;
		$this->profilingService = $profilingInterface;
	}

	public function index()
	{
		$inputs = Input::all();

		$sort = new SortParameters(Input::get('sort'));

		$fields = new FieldsParameters(Input::get('fields'));

		$pagination = PaginationParameters::createFromArray($inputs);

		$filters = new FilterParameters(Input::get('filters'));

		$citiesmunicipalities = $this->profilingService->findCitiesMunicipalities($fields, $pagination, $sort, $filters);

		return $this->respond($this->cityMunicipalityTransformer->transformCollection($citiesmunicipalities, $fields, $pagination, $sort));
	}

	public function show(CityMunicipality $cityMunicipality)
	{
		return $this->cityMunicipalityTransformer->transform($cityMunicipality);
	}

	public function store()
	{

	}

	public function update()
	{

	}

	public function destroy()
	{

	}
}