<?php

use SourceScript\Common\Collections\ResultCollection;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;

class Api_CoursesController extends ApiController {

	/**
	 * @var ScholarshipManagementInterface
	 */
	private $scholarshipSystem;

	/**
	 * @var CourseTransformer
	 */
	private $courseTransformer;

	function __construct(
		ScholarshipManagementInterface $scholarshipManagementInterface,
		CourseTransformer $courseTransformer)
	{
		$this->courseTransformer = $courseTransformer;
		$this->scholarshipSystem = $scholarshipManagementInterface;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$courses = $this->scholarshipSystem->findCourses($fields, $pagination, $sort, $filters);

		return $this->respond($this->courseTransformer->transformCollection($courses, $fields, $pagination, $sort));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::only(['name', 'abbreviation']);

		$user = Auth::user();

		$course = $this->scholarshipSystem->execute('AddCourse', $inputs, $user);

		return $this->respondData($this->courseTransformer->transform($course));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Course $course)
	{
		return $this->courseTransformer->transform($course);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Course $course)
	{
		$inputs = Input::only(['name', 'abbreviation']);

		$updater = Auth::user();

		$course = $this->scholarshipSystem->execute('UpdateCourse', $inputs, $course, $updater);

		return $this->respondData($this->courseTransformer->transform($course));
	}

	/**
	 * Removes courses
	 * 
	 * @param  ResultCollection $courses
	 * @return Response
	 */
	public function massDelete(ResultCollection $courses)
	{
		$inputs = Input::all();

		$this->scholarshipSystem->execute('RemoveCourses', $inputs, $courses);

		return $this->respondOk('Courses deleted');
	}


}
