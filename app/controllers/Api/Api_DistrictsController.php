<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Common\Collections\ResultCollection;
use SourceScript\Profiling\ProfilingInterface;

class Api_DistrictsController extends ApiController {

	/**
	 * @var ProfilingInterface
	 */
	private $profilingService;


	/**
	 * @var DistrictTransformer
	 */
	private $districtTransformer;


	function __construct(
		DistrictTransformer $districtTransformer,
		ProfilingInterface $profilingInterface)
	{
		$this->districtTransformer = $districtTransformer;
		$this->profilingService = $profilingInterface;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

		$sort = new SortParameters(Input::get('sort'));

		$fields = new FieldsParameters(Input::get('fields'));

		$pagination = PaginationParameters::createFromArray($inputs);

		$filters = new FilterParameters(Input::get('filters'));

		$barangays = $this->profilingService->findDistricts($fields, $pagination, $sort, $filters);

		return $this->respond($this->districtTransformer->transformCollection($barangays, $fields, $pagination, $sort));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(District $district)
	{
		return $this->districtTransformer->transform($district);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::only([
			'name'
			]);

		$creator = Auth::user();

		$district = $this->profilingService->execute('AddDistrict', $inputs, $creator);

		return $this->respondData($district);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(District $district)
	{
		$inputs = Input::only([
			'name'
			]);		

		$updater = Auth::user();

		$district = $this->profilingService->execute('AddDistrict', $inputs, $district, $updater);

		return $this->respondData($district);
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy()
	{
		$inputs = Input::all();

		$this->profilingService->execute('RemoveDistrict', $inputs);

		return $this->respondOk('District deleted');
	}
}