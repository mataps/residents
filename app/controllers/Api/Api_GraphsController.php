<?php

use SourceScript\Profiling\ProfilingInterface;

class Api_GraphsController extends ApiController {


	function __construct(
		ProfilingInterface $profilingInterface,
		GraphCitiesTransformer $graphCitiesTransformer)
	{
		$this->profilingService = $profilingInterface;
		$this->graphCitiesTransformer = $graphCitiesTransformer;
	}


	public function cities()
	{
		$cities = $this->profilingService->findCitiesMunicipalities();

		return $this->respond($this->graphCitiesTransformer->transformCollection($cities));
	}
}