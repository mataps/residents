<?php

use Illuminate\Support\Collection;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\UserManagement\UserManagementInterface;

class Api_GroupsController extends ApiController {

    /**
     * @var UserManagementInterface
     */
    private $userService;
    /**
     * @var GroupTransformer
     */
    private $groupTransformer;

    function __construct(UserManagementInterface $userService, GroupTransformer $groupTransformer)
    {
        $this->userService = $userService;
        $this->groupTransformer = $groupTransformer;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $inputs = Input::all();

		$sort = new SortParameters(Input::get('sort'));

		$fields = new FieldsParameters(Input::get('fields'));

		$pagination = PaginationParameters::createFromArray($inputs);

		$filters = new FilterParameters(Input::get('filters'));

        $groups = $this->userService->findGroups($fields, $pagination, $sort, $filters);

        return $this->respond($this->groupTransformer->transformCollection($groups, $fields, $pagination, $sort));
    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $inputs = Input::only('name', 'description', 'permissions');

        $group = $this->userService->execute('AddGroup', $inputs);

        return $this->respondData($this->groupTransformer->transform($group));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  Group  $group
	 * @return Response
	 */
	public function show(Group $group)
	{
		return $this->groupTransformer->transform($group);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Group  $group
	 * @return Response
	 */
	public function update(Group $group)
	{
        $inputs = Input::all();

        $group = $this->userService->execute('UpdateGroup', $inputs, $group);

        return $this->respondData($this->groupTransformer->transform($group));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  Group  $group
	 * @return Response
	 */
	public function destroy(Group $group)
	{
        $inputs = Input::all();

        $this->userService->execute('RemoveGroup', $inputs, $group);

        return $this->respondOk('Group deleted');
	}

    function removeGroups(Collection $groups)
    {
        $inputs = Input::all();

        $this->userService->execute('RemoveGroups', $inputs, $groups);

        return $this->respondOk('Groups deleted');
    }

}
