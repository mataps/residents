<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\UserManagement\UserManagementInterface;

class Api_GroupsUsersController extends ApiController {

	/**
	 * @var UserManagementInterface
	 */
	private $userManagementService;

	/**
	 * @var UserTransformer
	 */
	private $userTransformer;

	function __construct(UserManagementInterface $userManagementInterface,
		UserTransformer $userTransformer)
	{
		$this->userManagementService = $userManagementInterface;
		$this->userTransformer = $userTransformer;
	}

	/**
	 * Listing of users by group
	 * 
	 * @param  Group  $group
	 * @return Response
	 */
	public function index(Group $group)
	{
		$inputs = Input::all();

		$sort = new SortParameters(Input::get('sort'));

		$fields = new FieldsParameters(Input::get('fields'));

		$pagination = PaginationParameters::createFromArray($inputs);

		$filters = new FilterParameters(Input::get('filters'));

		$users = $this->userManagementService->findUsersByGroup($group, $fields, $pagination, $sort, $filters);

		return $this->respond($this->userTransformer->transformCollection($users, $fields, $pagination, $sort));
	}

	/**
	 * Store a user to a group
	 * 
	 * @param  Group  $group
	 * @return Response
	 */
	public function store(Group $group)
	{
		$inputs = Input::only(['user_id']);

		
		$user = Auth::user();

		$user = $this->userManagementService->execute('AddGroupUser', $inputs, $group, $user);

		return $this->respond($this->userTransformer->transform($user));
	}


	/**
	 * Removes user form a group
	 * 
	 * @param  Group  $group
	 * @param  User   $user
	 * @return Response
	 */
	public function destroy(Group $group, User $user)
	{
		
		$user->group_id = null;
		$user->save();
		    return $this->respondOk('User detached from group');

	}
}