<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\UserManagement\UserManagementInterface;

class Api_HistoryController extends ApiController {

	/**
	 * Resource routes
	 * 
	 * @var array
	 */
	private $resource_routes = [
		'Resident' => 'residents',
		'Household' => 'households',
		'Affiliation' => 'affiliations',
		'Attribution' => 'attributions',
		'Position' => 'positions',

		'TransactionAccount' => 'accounts',
		'TransactionCategory' => 'categories',
		'TransactionSubCategory' => 'sub_categories',
		'Transaction' => 'transactions',
		'Voucher' => 'vouchers',
		'Liquidation' => 'liquidations',

		'Award' => 'awards',
		'School' => 'schools',
		'Semester' => 'semesters',
		'Grade' => 'grades',
		'AllowancePolicy' => 'allowance_policies',
		'Allowance' => 'allowances',
		'AllowanceRelease' => 'allowance_releases',
		'ScholarshipType' => 'scholarship_types',
		'Subject' => 'subjects'
	];


	/**
	 * @var HistoryTransformer
	 */
	private $historyTransformer;


	/**
	 * @var UserManagementInterface
	 */
	private $userManagementService;


	function __construct(
		UserManagementInterface $userManagementInterface,
		HistoryTransformer $historyTransformer)
	{
		$this->userManagementService = $userManagementInterface;
		$this->historyTransformer = $historyTransformer;
	}


	/**
	 * History listing
	 * 
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

		$sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$history = $this->userManagementService->findHistory($fields, $pagination, $sort, $filters);

		return $this->respond($this->historyTransformer->transformCollection($history, $fields, $pagination, $sort));
	}

	

	/**
	 * @param  string $resource
	 * @param  integer $id
	 * @return Response
	 */
	public function resource($resource, $id)
	{
		$inputs = Input::all();

		$sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$model = array_search($resource, $this->resource_routes);

		$history = $this->userManagementService->findHistoryByResource($model, $id, $fields, $pagination, $sort, $filters);

		return $this->respond($this->historyTransformer->transformCollection($history, $fields, $pagination, $sort));
	}


	/**
	 * Displays user activity
	 * @param  User   $user
	 * @return Response
	 */
	public function userActivity(User $user)
	{
		$inputs = Input::all();

		$sort = new SortParameters(Input::get('sort'));

		$fields = new FieldsParameters(Input::get('fields'));

		$pagination = PaginationParameters::createFromArray($inputs);

		$filters =new FilterParameters(Input::get('filters'));

		$history = $this->userManagementService->findHistoryByUser($fields, $pagination, $sort, $filters, $user);

		return $this->respond($this->historyTransformer->transformCollection($history, $fields, $pagination, $sort));
	}
}