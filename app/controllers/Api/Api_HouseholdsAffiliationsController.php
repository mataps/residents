<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Profiling\ProfilingInterface;

class Api_HouseholdsAffiliationsController extends ApiController {


	/**
	 * @var ProfilingInterface
	 */
	private $profilingService;


	/**
	 * @var HouseholdAffiliationTransformer
	 */
	private $householdAffiliationTransformer;


	function __construct(ProfilingInterface $profilingInterface,
		HouseholdAffiliationTransformer $householdAffiliationTransformer)
	{
		$this->profilingService = $profilingInterface;
		$this->householdAffiliationTransformer = $householdAffiliationTransformer;
	}


	/**
	 * Displays a listing of resources.
	 * 
	 * @param  Household $household
	 * @return Response
	 */
	public function index(Household $household)
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$affiliations = $this->profilingService->findAffiliationsByHousehold($household, $fields, $pagination, $sort, $filters);

		return $this->respond($this->householdAffiliationTransformer->transformCollection($affiliations, $fields, $pagination, $sort));
	}
}