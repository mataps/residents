<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Profiling\ProfilingInterface;

class Api_HouseholdsAttributionsController extends ApiController {

	/**
	 * @var ProfilingInterface
	 */
	private $profilingService;


	/**
	 * @var ResidentAttributeTransformer
	 */
	private $residentAttributesTransformer;



	function __construct(ProfilingInterface $profilingInterface,
		HouseholdAttributionTransformer $residentAttributionTransformer)
	{
		$this->profilingService = $profilingInterface;
		$this->residentAttributionTransformer = $residentAttributionTransformer;
	}


	/**
	 * Displays a listing of resources.
	 * 
	 * @param  Household $household
	 * @return Response
	 */
	public function index(Household $household)
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$attributes = $this->profilingService->findAttributesByHousehold($household, $fields, $pagination, $sort, $filters);

		return $this->respond($this->residentAttributionTransformer->transformCollection($attributes, $fields, $pagination, $sort));
	}
}