<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Profiling\ProfilingInterface;
use SourceScript\Common\Collections\ResultCollection;

class Api_HouseholdsController extends ApiController {

    /**
     * @var ProfilingInterface
     */
    private $profilingService;
    
    /**
     * @var HouseholdTransformer
     */
    private $householdTransformer;

    function __construct(ProfilingInterface $profilingService, HouseholdTransformer $householdTransformer)
    {
        $this->profilingService = $profilingService;
        $this->householdTransformer = $householdTransformer;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = new PaginationParameters(Input::get('page'), Input::get('limit'));

        $filters = new FilterParameters(Input::get('filters'));

        $households = $this->profilingService->findHouseholds($fields, $pagination, $sort, $filters);

        return $this->respond($this->householdTransformer->transformCollection($households, $fields, $pagination, $sort));
    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $inputs = Input::only([
        	'label',
        	'phone',
        	'district',
            'cityMunicipality',
            'barangay',
            'street'
        ]);

        $creator = Auth::user();

        $household = $this->profilingService->execute('AddHousehold', $inputs, $creator);

        return $this->respondData($this->householdTransformer->transform($household));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  Household  $household
	 * @return Response
	 */
	public function show(Household $household)
	{
		return $this->householdTransformer->transform($household);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Household  $household
	 * @return Response
	 */
	public function update(Household $household)
	{
        $inputs = Input::only([
        	'label',
        	'phone',
        	'district',
            'cityMunicipality',
            'barangay',
            'street'
        	]);

        $updater = Auth::user();

        $household = $this->profilingService->execute('UpdateHousehold', $inputs, $household, $updater);

        return $this->respondData($this->householdTransformer->transform($household));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  Household  $household
	 * @return Response
	 */
	public function destroy(Household $household)
	{
        $inputs = Input::all();

        $this->profilingService->execute('RemoveHousehold', $inputs, $household);

        return $this->respondOk('Household deleted');
	}


    /**
     * Remove the specified resources from storage
     * 
     * @param  Collection $schools
     * @return Response
     */
    public function massDelete(ResultCollection $households)
    {
        $inputs = Input::all();

        $this->profilingService->execute('RemoveHouseholds', $inputs, $households);

        return $this->respondOk('Households deleted');
    }


}
