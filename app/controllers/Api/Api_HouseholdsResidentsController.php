<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Profiling\ProfilingInterface;

class Api_HouseholdsResidentsController extends ApiController {

	/**	 
	 * @var ProfilingInterface
	 */
	private $profilingService;

	/**
	 * @var ResidentTranformer
	 */
	private $residentTransformer;

	function __construct(
		ProfilingInterface $profilingInterface,
		HouseholdResidentTransformer $residentTransformer)
	{
		$this->residentTransformer = $residentTransformer;
		$this->profilingService = $profilingInterface;
	}

	/**
	 * Lists affiliations of a resident
	 * 
	 * @param  Resident $resident
	 * @return Response
	 */
	public function index(Household $household)
	{
		$inputs = Input::all();

		$sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        $residents = $this->profilingService->findResidentsByHousehold($household, $fields, $pagination, $sort, $filters);

        return $this->respond($this->residentTransformer->transformCollection($residents, $fields, $pagination, $sort));
	}
}