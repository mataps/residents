<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\TransactionSystem\TransactionSystemInterface;

class Api_HouseholdsVouchersController extends ApiController {

	/**
	 * @var TransactionSystemInterface
	 */
	private $transactionSystemService;


	/**
	 * @var HouseholdTransactionTransformer
	 */
	private $householdTransactionTransformer;


	function __construct(
		TransactionSystemInterface $transactionSystemInterface,
		HouseholdVoucherTransformer $householdTransactionTransformer)
	{
		$this->transactionSystemService = $transactionSystemInterface;
		$this->householdTransactionTransformer = $householdTransactionTransformer;
	}


	/**
	 * Listing of resource.
	 * 
	 * @param  Household $household
	 * @return Response
	 */
	public function index(Household $household)
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$transactions = $this->transactionSystemService->findVouchersByHousehold($household, $fields, $pagination, $sort, $filters);

		return $this->respond($this->householdTransactionTransformer->transformCollection($transactions, $fields, $pagination, $sort));
	}
}