<?php

use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\UserManagement\UserManagementInterface;

class Api_PermissionsController extends ApiController {

    /**
     * @var UserManagementInterface
     */
    private $userService;
    /**
     * @var PermissionTransformer
     */
    private $permissionTransformer;

    function __construct(UserManagementInterface $userService, PermissionTransformer $permissionTransformer)
    {
        $this->userService = $userService;
        $this->permissionTransformer = $permissionTransformer;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $pagination = new PaginationParameters(Input::get('page'), Input::get('limit'));

        $permissions = $this->userService->findPermissions($pagination);

        return $this->respond($this->permissionTransformer->transformCollection($permissions, null, $pagination));
    }
}
