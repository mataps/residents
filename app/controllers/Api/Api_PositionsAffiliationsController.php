<?php

use Illuminate\Support\Collection;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Profiling\ProfilingInterface;

class Api_PositionsAffiliationsController extends ApiController {


	/**
	 * @var ProfilingInterface
	 */
	private $profilingService;


	/**
	 * @var AffiliationTransformer
	 */
	private $affiliationTransformer;


	function __construct(ProfilingInterface $profilingService, AffiliationPositionTransformer $affiliationTransformer)
	{
		$this->profilingService = $profilingService;
		$this->affiliationTransformer = $affiliationTransformer;
	}


	/**
	 * Display a listing of resource
	 * 
	 * @param  Position $position
	 * @return Response
	 */
	public function index(Position $position)
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        $affiliations = $this->profilingService->findAffiliationsByPosition($position, $fields, $pagination, $sort, $filters);

        return $this->respond($this->affiliationTransformer->transformCollection($affiliations, $fields, $pagination, $sort));
	}


	/**
	 * Stores resource.
	 * 
	 * @param  Position $position
	 * @return Response
	 */
	public function store(Position $position)
	{
		$inputs = Input::only(['affiliations']);

		$creator = Auth::user();

		$this->profilingService->execute('AddPositionAffiliations', $inputs, $position, $creator);

		return $this->respondOk('Affiliations added to position!');
	}
}