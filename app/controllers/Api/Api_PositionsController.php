<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Profiling\ProfilingInterface;
use SourceScript\Common\Collections\ResultCollection;

class Api_PositionsController extends ApiController {


	/**
	 * @var ProfilingInterface
	 */
	private $profilingService;


	/**
	 * @var PositionTransformer
	 */
	private $positionTransformer;


	function __construct(ProfilingInterface $profilingInterface, PositionTransformer $positionTransformer)
	{
		$this->profilingService = $profilingInterface;
		$this->positionTransformer = $positionTransformer;
	}


	/**
	 * Displays a listing of resources.
	 * 
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$positions = $this->profilingService->findPositions($fields, $pagination, $sort, $filters);

		return $this->respond($this->positionTransformer->transformCollection($positions, $fields, $pagination, $sort));
	}

	/**
	 * Displays a specified resource
	 * 
	 * @param  Position $position
	 * @return Response
	 */
	public function show(Position $position)
	{
		return $this->positionTransformer->transform($position);
	}


	/**
	 * Stores a resource.
	 * 
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::only(['name', 'affiliations', 'attributions']);

		$creator = Auth::user();

		$position = $this->profilingService->execute('AddPosition', $inputs, $creator);

		return $this->respondData($this->positionTransformer->transform($position));
	}


	/**
	 * Updates a specified resource.
	 * 
	 * @param  Position $position
	 * @return Response
	 */
	public function update(Position $position)
	{
		$inputs = Input::only(['name', 'affiliations', 'attributions']);
		$updater = Auth::user();

		$position = $this->profilingService->execute('UpdatePosition', $inputs, $position, $updater);

		return $this->respondData($this->positionTransformer->transform($position));
	}


	/**
	 * Deletes a specified resource.
	 * 
	 * @param  Position $position
	 * @return Response
	 */
	public function destroy(Position $position)
	{
		$inputs = Input::all();

		$this->profilingService->execute('RemovePosition', $inputs);

		$this->respondOk('Position removed!');
	}


	/**
     * Remove the specified resources from storage.
     * 
     * @param  ResultCollection $positions
     * @return Reponse
     */
	public function massDelete(ResultCollection $positions)
	{
		$inputs = Input::all();

		$this->profilingService->execute('RemovePositions', $inputs, $positions);

		return $this->respondOk('Positions deleted!');
	}

}