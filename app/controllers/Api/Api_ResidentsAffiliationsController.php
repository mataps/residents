<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Profiling\ProfilingInterface;
use SourceScript\Common\Collections\ResultCollection;

class Api_ResidentsAffiliationsController extends ApiController {

	/**
	 * @var ProfilingInterface
	 */
	private $profilingService;

	/**
	 * @var AffiliationTransformer
	 */
	private $affiliationTransformer;

	function __construct(
		ProfilingInterface $profilingServiceInterface,
		AffiliationResidentTransformer $affiliationTransformer)
	{
		$this->profilingService = $profilingServiceInterface;
		$this->affiliationTransformer = $affiliationTransformer;
	}

	/**
	 * Lists affiliations of a resident
	 * 
	 * @param  Resident $resident
	 * @return Response
	 */
	public function index(Resident $resident)
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        $affiliations = $this->profilingService->findAffiliationsByResident($resident, $fields, $pagination, $sort, $filters);

        return $this->respond($this->affiliationTransformer->transformCollection($affiliations, $fields, $pagination, $sort));
	}


	/**
	 * Stores a resident and affiliation relation
	 * 
	 * @param  Resident    $resident
	 * @param  Affiliation $affiliation
	 * @return Response
	 */
	public function store(Resident $resident)
	{
		$inputs = Input::only([
			'founder',
			'position_id',
			'remarks',
			'status',
			'from',
			'to',
			'affiliation_id',
			'referrer_id',
			'head_id'
		]);

		$creator = Auth::user();

		$this->profilingService->execute('AddResidentAffiliation', $inputs, $resident, $creator);

        try{
            $this->profilingService->execute('UpdateIndexResident', $inputs, $resident, $resident->id);            
        }catch(Exception $e){
            Log::error($e);
        }catch(CouldNotConnectToHost $e){
            Log::error($e);
        }


		return $this->respondOk('Resident added to affiliation');
	}


	/**
	 * 
	 * @param  ResultCollection $residents [description]
	 * @return [type]                      [description]
	 */
	public function massStore(ResultCollection $residents)
	{
		$inputs = Input::only([
			'founder',
			'position_id',
			'remarks',
			'status',
			'from',
			'to',
			'affiliation_id',
			'referrer_id',
			'head_id'
		]);

		$creator = Auth::user();

		$this->profilingService->execute('AddResidentsAffiliation', $inputs, $residents, $creator);
		
		return $this->respondOk('Residents added to affiliation');
	}


	/**
	 * Update a residents affiliation relation
	 * 
	 * @param  Resident    $resident
	 * @param  Affiliation $affiliation
	 * 
	 * @return Response
	 */
	public function update(Resident $resident, Affiliation $affiliation)
	{
		$inputs = Input::only([
			'position_id',
			'remarks',
			'status',
			'from',
			'to',
			'founder',
			'affiliation_id',
			'referrer_id',
			'head_id'
			]);

		$updater = Auth::user();

		$this->profilingService->execute('UpdateResidentAffiliation', $inputs, $resident, $affiliation, $updater);

        try{
            $this->profilingService->execute('UpdateIndexResident', $inputs, $resident, $resident->id);            
        }catch(Exception $e){
            Log::error($e);
        }catch(CouldNotConnectToHost $e){
            Log::error($e);
        }


		return $this->respondOk('Resident affiliation updated');
	}

	/**
	 * Removes a resident from an affiliation
	 * 
	 * @param  Resident    $resident
	 * @param  Affiliation $affiliation
	 * @return Response
	 */
	public function destroy(Resident $resident, Affiliation $affiliation)
	{
		$inputs = Input::all();

		$this->profilingService->execute('RemoveResidentAffiliation', $inputs, $resident, $affiliation);
        
        try{
            $this->profilingService->execute('UpdateIndexResident', $inputs, $resident, $resident->id);            
        }catch(Exception $e){
            Log::error($e);
        }catch(CouldNotConnectToHost $e){
            Log::error($e);
        }
		return $this->respondOk('Resident removed');
	}
}