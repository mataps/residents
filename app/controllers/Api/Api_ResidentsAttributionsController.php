<?php

use SourceScript\Common\Collections\ResultCollection;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Profiling\ProfilingInterface;

class Api_ResidentsAttributionsController extends ApiController {

	/**
	 * @var ProfilingInterface
	 */
	private $profilingService;

	/**
	 * @var AttributionTransformer
	 */
	private $residentAttributionTransformer;

	function __construct(
		ProfilingInterface $profilingServiceInterface,
		AttributionTransformer $residentAttributionTransformer)
	{
		$this->profilingService = $profilingServiceInterface;
		$this->residentAttributionTransformer = $residentAttributionTransformer;
	}


	/**
	 * Lists attributes of a resident
	 * 
	 * @param  Resident $resident
	 * @return Response
	 */
	public function index(Resident $resident)
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        $attributes = $this->profilingService->findAttributionsByResident($resident, $fields, $pagination, $sort, $filters);

        return $this->respond($this->residentAttributionTransformer->transformCollection($attributes, $fields, $pagination, $sort));
	}


	/**
	 * Stores a resident and affiliation relation
	 * 
	 * @param  Resident    $resident
	 * @param  Affiliation $affiliation
	 * @return Response
	 */
	public function store(Resident $resident)
	{
		$inputs = Input::only([
			'attribute_name',
			'from',
			'to'
			]);

		$creator = Auth::user();

		$designation = $this->profilingService->execute('AddResidentAttribution', $inputs, $resident, $creator);

        try{
            $this->profilingService->execute('IndexResident', $inputs, $resident);            
        }catch(Exception $e){
            Log::error($e);
        }catch(CouldNotConnectToHost $e){
            Log::error($e);
        }
		return $this->respondOk($designation);
	}

	public function massStore(ResultCollection $residents)
	{
		$inputs = Input::only([
			'attribute_id',
			'from',
			'to'
			]);

		$creator = Auth::user();

		$attribution = $this->profilingService->execute('AddResidentsAttribution', $inputs, $residents, $creator);

		foreach ($residents as $resident) {
		        try{
		            $this->profilingService->execute('IndexResident', $inputs, $resident);            
		        }catch(Exception $e){
		            Log::error($e);
		        }catch(CouldNotConnectToHost $e){
		            Log::error($e);
		        }
		}

		return $attribution;
	}


	/**
	 * Removes a resident from an affiliation
	 * 
	 * @param  Resident    $resident
	 * @param  Affiliation $affiliation
	 * @return Response
	 */
	public function destroy(Resident $resident, Attribution $attribution)
	{
		$inputs = Input::all();

		$this->profilingService->execute('RemoveResidentAttribution', $inputs, $resident, $attribution);
       
        try{
            $this->profilingService->execute('UpdateIndexResident', $inputs, $resident, $resident->id);            
        }catch(Exception $e){
            Log::error($e);
        }catch(CouldNotConnectToHost $e){
            Log::error($e);
        }

		return $this->respondOk('Resident removed');
	}
}