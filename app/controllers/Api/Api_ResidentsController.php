<?php

use Illuminate\Support\Collection;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Profiling\ProfilingInterface;
use Elasticsearch\Client;
use Elasticsearch\Common\Exceptions\Curl\CouldNotConnectToHost;
use Elasticsearch\Common\Exceptions\BadRequest400Exception;

class Api_ResidentsController extends ApiController {

    /**
     * @var ProfilingInterface
     */
    private $profilingService; 
    /**
     * @var ResidentTransformer
     */
    private $residentTransformer;

    function __construct(ProfilingInterface $profilingService, ResidentTransformer $residentTransformer)
    {
        $this->profilingService = $profilingService;
        $this->residentTransformer = $residentTransformer;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        try
        {
            $residents = $this->profilingService->findResidentsES($fields, $pagination, $sort, $filters);

            $result = ['data' => []];
            foreach ($residents['hits']['hits'] as $resident) {
                $resident['_source']['es_id'] = $resident['_id'];
                $resident['_source']['full_name'] = ucwords(strtolower($resident['_source']['full_name']));
                $resident['_source']['last_name'] = ucwords(strtolower($resident['_source']['last_name']));
                $resident['_source']['first_name'] = ucwords(strtolower($resident['_source']['first_name']));
                $resident['_source']['middle_name'] = ucwords(strtolower($resident['_source']['middle_name']));
                array_push($result['data'], $resident['_source']);
             }

            $next_page = $pagination->getPage() + 1;
            $previous_page = $pagination->getPage() - 1;
            $end_count =  ($residents['hits']['total'] > 0) ? ($pagination->getFrom() + $pagination->getLimit()) : 0;
            $result['paging'] = [
                    'total' => $residents['hits']['total'],
                    'start_count' => $pagination->getFrom() + 1,
                    'end_count' => $end_count,
                    'next' => "?page=". $next_page ."&limit=" . $pagination->getLimit()
                ];

            if($pagination->getPage() > 1)
                $result['paging']['previous'] = "?page=" . $previous_page . "&limit=" . $pagination->getLimit();

             return $result;
         }
        catch(CouldNotConnectToHost $e)
        {
            dd($e);
            $residents = $this->profilingService->findResidents($fields, $pagination, $sort, $filters);
         
            return $this->respond($this->residentTransformer->transformCollection($residents, $fields, $pagination, $sort));
        }
        catch(BadRequest400Exception $e)
        {
            dd($e);
            $residents = $this->profilingService->findResidents($fields, $pagination, $sort, $filters);

            return $this->respond($this->residentTransformer->transformCollection($residents, $fields, $pagination, $sort));
            
        }
    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $inputs = Input::only([
            'is_voter',
            'profile_image',
            'first_name',
            'last_name',
            'middle_name',
            'civil_status',
            'gender',
            'birthdate',
            'mobile',
            'mobile_1',
            'mobile_2',
            'phone',
            'email',
            'district',
            'cityMunicipality',
            'barangay',
            'street',
            'voters_id',
            'signature_path',
            'deceased_at',
            'blacklisted_at',
            'signature_path',
            'atm_number',
            'bank',
            'nickname',
            'occupation',
            'notes'
        ]);        
        
        $creator = Auth::user();

        $resident = $this->profilingService->execute('AddResident', $inputs, $creator);
        
        try{
            $this->profilingService->execute('IndexResident', $inputs, $resident);            
        }catch(CouldNotConnectToHost $e){
             Log::error($e);
        }

        return $this->respondData($this->residentTransformer->transform($resident));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  Resident  $resident
	 * @return Response
	 */
	public function show(Resident $resident)
	{
		return $this->residentTransformer->transform($resident);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Resident  $resident
	 * @return Response
	 */
	public function update(Resident $resident)
	{

        $inputs = Input::only([
            'is_voter',
            'profile_image',
            'first_name',
            'last_name',
            'middle_name',
            'civil_status',
            'gender',
            'birthdate',
            'mobile',
            'mobile_1',
            'mobile_2',
            'phone',
            'email',
            'district',
            'cityMunicipality',
            'barangay',
            'street',
            'voters_id',
            'deceased_at',
            'blacklisted_at',
            'signature_path',
            'atm_number',
            'bank',
            'nickname',
            'occupation',
            'notes'
        ]);

        $updater = Auth::user();

        $resident = $this->profilingService->execute('UpdateResident', $inputs, $resident, $updater);

      
        $this->profilingService->execute('IndexResident', $inputs, $resident);            
        
        return $this->respondData($this->residentTransformer->transform($resident));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  Resident  $resident
	 * @return Response
	 */
	public function destroy(Resident $resident)
	{
        $inputs = Input::all();

        $this->profilingService->execute('RemoveResident', $inputs, $resident);

        return $this->respondOk('Resident deleted');
	}

    function removeResidents(Collection $residents)
    {
        $inputs = Input::all();

        $this->profilingService->execute('RemoveResidents', $inputs, $residents);

        return $this->respondOk('Residents deleted');
    }

    function showLastNameCounts(Resident $resident)
    {
        $counts = $this->profilingService->getLastNameCounts($resident);

        return $this->respondData([
            'lastnames'     => $counts[0]->count - 1,
            'middlenames'   => $counts[1]->count - 1,
        ]);
    }

    function showRelatedMQTY(Resident $resident)
    {
        $pagination = PaginationParameters::createFromArray(Input::all());

        $results = $this->profilingService->getRelatedMQTY($resident, $pagination);

        return $this->respond($this->residentTransformer->transformCollection($results, null, $pagination));
    }


    public function addResidentAffiliation(Resident $resident, Affiliation $affiliation)
    {
        $inputs = Input::all();
        $inputs['resident_id'] = $resident;
        $results = $this->profilingService->execute('AddResidentAffiliation', $inputs, $resident);


        try{
            if(Input::has('es_id')){ 
                $es_id = Input::get('es_id');
            }
            else{
                
                $conn_param = \Config::get('elasticsearch');
                $client = new Client($conn_param); 
                $param['index'] = "galactus_production";
                $param['type'] = "residents";
                $param['body']['query']['match']['id'] = $resident;
                $es_resident = $client->search($param);
                $es_id = $es_resident['hits']['hits'][0]['_id'];
            }

            $this->profilingService->execute('IndexResident', $inputs, $resident);            
        }catch(Exception $e){
            Log::error($e);
        }catch(CouldNotConnectToHost $e){
            Log::error($e);
        }

        return $this->respondOk($results);
    }

    public function addResidentAttribution($resident)
    {
        $inputs = Input::all();
        $inputs['resident_id'] = $resident;
        $results = $this->profilingService->execute('AddResidentAttribution', $inputs, $resident);


        try{
            if(Input::has('es_id')){ 
                $es_id = Input::get('es_id');
            }
            else{
                
                $conn_param = \Config::get('elasticsearch');
                $client = new Client($conn_param); 
                $param['index'] = "galactus_production";
                $param['type'] = "residents";
                $param['body']['query']['match']['id'] = $resident;
                $es_resident = $client->search($param);
                $es_id = $es_resident['hits']['hits'][0]['_id'];
            }

            $this->profilingService->execute('IndexResident', $inputs, $resident);            
        }catch(Exception $e){
            Log::error($e);
        }catch(CouldNotConnectToHost $e){
            Log::error($e);
        }

        return $this->respondOk($results);
    }

    public function getResidentAffiliation(Resident $resident)
    {
        $data = $resident->affiliations;
        return $this->respondData($data);
    }
    
    function showRelatedLQTY(Resident $resident)
    {
        $pagination = PaginationParameters::createFromArray(Input::all());

        $results = $this->profilingService->getRelatedLQTY($resident, $pagination);

        return $this->respond($this->residentTransformer->transformCollection($results, null, $pagination));
    }

    /**
     * Get resident housemates
     * 
     * @param  Resident $resident
     * @return Response
     */
    function getResidentHousemates(Resident $resident)
    {
        $inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        $residents = $this->profilingService->findResidentHousemates($resident, $fields, $pagination, $sort, $filters);

        return $this->respond($this->residentTransformer->transformCollection($residents, $fields, $pagination, $sort));
    }
    /**
     * Generate residents pdf report.
     * 
     * @return Pdf
     */
    public function getResidentsPdf(Resident $residents)
    {
        $pdf = PDF::loadView('pdf.reports', ['residents' => $residents]);

        return $pdf->stream('reports.pdf');
    }

    /**
     * Generate residents html report.
     * 
     * @return html
     */
    public function getResidentsHtml(Resident $residents)
    {

        return View::make('pdf.reports', ['residents' => $residents]); 
    }
    /**
     * Generate residents excel report.
     * 
     * @return Excel
     */
    public function residentsSpreadsheet()
    {
        $inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = explode(',', Input::get('fields'));

        // $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        $data = $this->profilingService->findResidentsES($fields, null, $sort, $filters);

        $residents = [];

        

        foreach($data['hits']['hits'] as $resident)
        {
            foreach($fields as $field)
            {
                $residents[$resident['_source']['id']][$field] = $resident['_source'][$field];
            }
        }

        return $this->respondExcelFromArray($residents, 'List of residents', 'List of residents');
    }
}
