<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Profiling\ProfilingInterface;
use SourceScript\Common\Collections\ResultCollection;

class Api_ResidentsHouseholdsController extends ApiController
{

	function __construct(
		ProfilingInterface $profilingServiceInterface,
		ResidentHouseholdTransformer $householdTransformer)
	{
		$this->profilingService = $profilingServiceInterface;
		$this->householdTransformer = $householdTransformer;
	}


	/**
	 * Lists affiliations of a resident
	 * 
	 * @param  Resident $resident
	 * @return Response
	 */
	public function index(Resident $resident)
	{
		$inputs = Input::all();

		$sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        $households = $this->profilingService->findHouseholdsByResident($resident, $fields, $pagination, $sort, $filters);

        return $this->respond($this->householdTransformer->transformCollection($households, $fields, $pagination, $sort));
	}


	/**
	 * Stores a resident and affiliation relation
	 * 
	 * @param  Resident    $resident
	 * @param  Affiliation $affiliation
	 * @return Response
	 */
	public function store(Resident $resident)
	{
		$inputs = Input::only([
			'role',
			'household_id',
			'from',
			'to'
			]);

		$creator = Auth::user();

		$this->profilingService->execute('AddResidentHousehold', $inputs, $resident, $creator);

		return $this->respondOk('Resident added to household');
	}


	public function massStore(ResultCollection $residents)
	{
		$inputs = Input::only([
			'role',
			'household_id',
			'from',
			'to'
			]);

		$creator = Auth::user();

		$this->profilingService->execute('AddResidentsHousehold', $inputs, $residents, $creator);

		return $this->respondOk('Residents added to household');
	}


	/**
	 * Update a residents household relation
	 * 
	 * @param  Resident    $resident
	 * @param  Household   $household
	 * 
	 * @return Response
	 */
	public function update(Resident $resident, Household $household)
	{
		$inputs = Input::only([
			'role',
			'from',
			'to'
			]);

		$updater = Auth::user();

		$this->profilingService->execute('UpdateResidentHousehold', $inputs, $resident, $household, $updater);

		return $this->respondOk('Residents household updated');
	}


	/**
	 * Removes a resident from an affiliation
	 * 
	 * @param  Resident    $resident
	 * @param  Household   $household
	 * 
	 * @return Response
	 */
	public function destroy(Resident $resident, Household $household)
	{
		$inputs = Input::all();

		$this->profilingService->execute('RemoveResidentHousehold', $inputs, $resident, $household);

		return $this->respondOk('Resident removed');
	}
}