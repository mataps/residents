<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Profiling\ProfilingInterface;

class Api_ResidentsRelationshipsController extends ApiController {


	/**
	 * @var ProfilingInterface
	 */
	private $profilingService;


	/**
	 * @var ResidentTransformer
	 */
	private $residentTransformer;

	function __construct(
		ProfilingInterface $profilingInterface,
		RelativeTransformer $residentTransformer)
	{
		$this->residentTransformer = $residentTransformer;
		$this->profilingService = $profilingInterface;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Resident $resident)
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        $residents = $this->profilingService->findRelatives($resident, $fields, $pagination, $sort, $filters);

        // dd($residents);

        return $this->respond($residents);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Resident $resident)
	{
		$inputs = Input::only([
			'resident_id',
			'relation'
			]);

		$creator = Auth::user();

		$this->profilingService->execute('AddResidentRelation', $inputs, $resident, $creator);

		return $this->respondOk('Resident relationship added!');
	}

	/**
	 * Updates a specified resource.
	 * 
	 * @param  Resident             $resident
	 * @param  ResidentRelationship $residentRelationship
	 * @return Response
	 */
	public function update(Resident $resident, ResidentRelationship $residentRelationship)
	{
		$inputs = Input::only([
			'resident_id',
			'relation'
			]);

		$updater = Auth::user();

		$this->profilingService->execute('UpdateResidentRelation', $inputs, $residentRelationship, $updater);

		return $this->respondOk('Resident relationship updated!');
	}


	/**
	 * Removes a resource.
	 * 
	 * @param  Resident $resident
	 * @return Response
	 */
	public function destroy($id, $relative)
	{
		$inputs = Input::all();

		$members = [$id, $relative];
		$this->profilingService->execute('RemoveResidentRelation', $inputs, $members);

		return $this->respondOk('Resident relationship deleted!');
	}
}