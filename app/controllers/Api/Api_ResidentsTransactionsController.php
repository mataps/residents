<?php

use SourceScript\Common\AbstractService;
use SourceScript\Common\Paginators\FilterableTrait;
use SourceScript\Common\Paginators\PaginatableTrait;
use SourceScript\Common\Paginators\SortableTrait;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Common\Repositories\GenericFinderInterface;
use SourceScript\TransactionSystem\TransactionSystemInterface;

class Api_ResidentsTransactionsController extends ApiController {

    /**
     * @var TransactionSystemInterface
     */
    private $transactionSystemService;
    /**
     * @var TransactionTransformer
     */
    private $transactionTransformer;

    function __construct(TransactionSystemInterface $transactionSystemService, TransactionTransformer $transactionTransformer)
    {
        $this->transactionSystemService = $transactionSystemService;
        $this->transactionTransformer = $transactionTransformer;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Resident $resident)
	{
        $inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$transactions = $this->transactionSystemService->findTransactionsByResident($resident, $fields, $pagination, $sort, $filters);

		return $this->respond($this->transactionTransformer->transformCollection($transactions, $fields, $pagination, $sort));
    }


    public function massStore(ResultCollection $residents)
    {
        $inputs = Input::only([
            'name',
            'voucher_id',
            'client_type',
            'client_id',
            'referrer_id',
            'beneficiary_id',
            'beneficiary_type',
            'account_type_id',
            'sub_category_id',
            'item_name',
            'transaction_type',
            'details',
            'amount'
            ]);
    }
}