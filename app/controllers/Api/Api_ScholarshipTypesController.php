<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;

class Api_ScholarshipTypesController extends ApiController {

	/**
	 * @var ScholarshipManagementService
	 */
	private $scholarshipManagementService;

	/**
	 * @var ScholarshipTypeTransformer
	 */
	private $scholarshipTypeTransformer;

	function __construct(
		ScholarshipManagementInterface $scholarshipManagementInterface,
		ScholarshipTypeTransformer $scholarshipTypeTransformer)
	{
		$this->scholarshipManagementService = $scholarshipManagementInterface;
		$this->scholarshipTypeTransformer 	= $scholarshipTypeTransformer;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$scholarshipTypes = $this->scholarshipManagementService->findScholarshipTypes($fields, $pagination, $sort, $filters);

		return $this->respond($this->scholarshipTypeTransformer->transformCollection($scholarshipTypes, $fields, $pagination, $sort));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(ScholarshipType $scholarshipType)
	{
		return $this->scholarshipTypeTransformer->transform($scholarshipType);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::only([
			'name',
			'description',
			'initial_allowance',
			'probation_amount'
			]);

		$creator = Auth::user();

		$scholarshipType = $this->scholarshipManagementService->execute('AddScholarshipType', $inputs, $creator);

		return $this->respondData($this->scholarshipTypeTransformer->transform($scholarshipType));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(ScholarshipType $scholarshipType)
	{
		$inputs = Input::only([
			'name',
			'description',
			'initial_allowance',
			'probation_amount'
			]);

		$updater = Auth::user();

		$scholarshipType = $this->scholarshipManagementService->execute('UpdateScholarshipType', $inputs, $scholarshipType, $updater);

		return $this->respondData($this->scholarshipTypeTransformer->transform($scholarshipType));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(ScholarshipType $scholarshipType)
	{
		$inputs = Input::all();

		$this->scholarshipManagementService->execute('RemoveScholarshipType', $inputs, $school);

		return $this->respondOk('Scholarship type deleted');
	}
}