<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;

class Api_ScholarshipsAllowanceReleasesController extends ApiController {

	/**
	 * @var ScholarshipManagementInterface
	 */
	private $scholarshipManagementService;


	/**
	 * @var AllowanceReleaseTransformer
	 */
	private $releaseTransformer;


	function __construct(
		ScholarshipManagementInterface $scholarshipManagementInterface,
		AllowanceReleaseTransformer $releaseTransformer)
	{
		$this->scholarshipManagementService = $scholarshipManagementInterface;
		$this->releaseTransformer = $releaseTransformer;
	}


	/**
	 * Display a listing of the resource.
	 * 
	 * @param  Scholarship $scholarship
	 * @return Response
	 */
	public function index(Scholarship $scholarship)
	{
		$inputs = Input::all();

		$sort = new SortParameters(Input::get('sort'));

		$fields = new FieldsParameters(Input::get('fields'));

		$pagination = PaginationParameters::createFromArray($inputs);

		$filters = new FilterParameters(Input::get('filters'));

		$releases = $this->scholarshipManagementService->findReleasesByScholarship($scholarship, $fields, $pagination, $sort, $filters);

		return $this->respond($this->releaseTransformer->transformCollection($releases, $fields, $pagination, $sort));
	}
}