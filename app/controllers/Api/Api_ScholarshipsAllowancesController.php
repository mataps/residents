<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\scholarshipManagement\ScholarshipManagementInterface;

class Api_ScholarshipsAllowancesController {

	/**
	 * @var ScholarshipManamgentInterface
	 */
	private $scholarshipManagementService;

	function __construct(
		ScholarshipManagementInterface $scholarshipManagementInterface,
		AllowanceTransformer $allowanceTransformer)
	{
		$this->scholarshipManagementService = $scholarshipManagementInterface;
		$this->allowanceTransformer = $allowanceTransformer;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Scholarship $scholarship)
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$allowances = $this->scholarshipManagementService->findAllowancesByScholarship($scholarship, $fields, $pagination, $sort, $filters);

		return $this->respond($this->allowanceTransformer->transformCollection($allowances, $fields, $pagination, $sort));
	}
}