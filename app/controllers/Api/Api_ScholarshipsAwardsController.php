<?php

use SourceScript\Common\Collections\ResultCollection;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;
use SourceScript\Profiling\ProfilingInterface;
use Elasticsearch\Client;
use Elasticsearch\Common\Exceptions\Curl\CouldNotConnectToHost;
use Elasticsearch\Common\Exceptions\BadRequest400Exception;

class Api_ScholarshipsAwardsController extends ApiController {

	/**
	 * @var ScholarshipManagementInterface
	 */
	private $scholarshipManagementService;
    /**
     * @var ProfilingInterface
     */
    private $profilingService; 

	function __construct(
		ProfilingInterface $profilingService,
		ScholarshipManagementInterface $scholarshipManagementService,
		ScholarshipAwardTransformer $awardTransformer)
	{
       $this->profilingService = $profilingService;
		$this->scholarshipManagementService = $scholarshipManagementService;
		$this->awardTransformer = $awardTransformer;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Scholarship $scholarship)
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$awards = $this->scholarshipManagementService->findAwardsByScholarship($scholarship, $fields, $pagination, $sort, $filters);



		return $this->respond($this->awardTransformer->transformCollection($awards, $fields, $pagination, $sort));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Scholarship $scholarship)
	{
		$inputs = Input::only(['semester_id', 'award_id', 'year', 'remarks']);
		
		$creator = Auth::user();

		$saved = $this->scholarshipManagementService->execute('AddAwardToScholarship', $inputs, $scholarship, $creator);
		if($saved){
			// dd($scholarship->awards);
	        try{
	            $this->profilingService->execute('IndexResident', $inputs, $scholarship->resident);    
	            $this->scholarshipManagementService->execute('IndexScholar', $inputs, $scholarship);        
	        }catch(CouldNotConnectToHost $e){
	             Log::error($e);
	        }
			
		}

		return $this->awardTransformer->transform($scholarship->awards()->find($inputs['award_id']));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Scholarship $scholarship, Award $award)
	{
		return $this->awardTransformer->transform($award);
	}

	/**
	 * Mass detach of awards from scholarship
	 * @param  Scholarship      $scholarship
	 * @param  ResultCollection $awards
	 * @return Response
	 */
	public function massDelete(Scholarship $scholarship, ResultCollection $awards)
	{
		$inputs = Input::all();

		$user = Auth::user();

		$this->scholarshipManagementService->execute('RemoveScholarshipAwards', $inputs, $scholarship, $awards, $user);

		return $this->respondOk('Awards removed from scholarships');
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Scholarship $scholarship, Award $award)
	{
		$inputs = Input::all();

		$this->scholarshipManagementService->execute('RemoveAwardFromScholarship', $inputs, $scholarship, $award);

		return $this->respondOk('Award removed from scholar');
	}
}
