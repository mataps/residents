<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;
use SourceScript\Common\Collections\ResultCollection;
use SourceScript\Profiling\ProfilingInterface;

class Api_ScholarshipsController extends ApiController {
    /**
     * @var ProfilingInterface
     */
    private $profilingService; 

    /**
     * @var ScholarshipManagementInterface
     */
    private $scholarshipManagementService;

    /**
     * @var ScholarshipTransformer
     */
    private $scholarshipTransformer;

    function __construct(ScholarshipManagementInterface $scholarshipManagementService, ProfilingInterface $profilingService, ScholarshipTransformer $scholarshipTransformer)
    {
    	 $this->profilingService = $profilingService;
        $this->scholarshipManagementService = $scholarshipManagementService;
        $this->scholarshipTransformer = $scholarshipTransformer;
    }


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        try
        {
            $scholarships = $this->scholarshipManagementService->findScholarshipsES($fields, $pagination, $sort, $filters);
            
            // dd('here');
            $result = ['data' => []];
            foreach ($scholarships['hits']['hits'] as $scholar) {
                $scholar['_source']['es_id'] = $scholar['_id'];
                $scholar['_source']['first_name'] = ucwords(strtolower($scholar['_source']['first_name']));
                $scholar['_source']['last_name'] = ucwords(strtolower($scholar['_source']['last_name']));
                $scholar['_source']['middle_name'] = ucwords(strtolower($scholar['_source']['middle_name']));
                array_push($result['data'], $scholar['_source']);
             }

            $next_page = $pagination->getPage() + 1;
              $previous_page = $pagination->getPage() - 1;
            $end_count =  ($scholarships['hits']['total'] > 0) ? ($pagination->getFrom() + $pagination->getLimit()) : 0;
            $result['paging'] = [
                    'total' => $scholarships['hits']['total'],
                    'start_count' => $pagination->getFrom() + 1,
                    'end_count' => $end_count,
                    'next' => "?page=". $next_page ."&limit=" . $pagination->getLimit(),


                ];
            if($pagination->getPage() > 1){
                 $result['paging']['previous'] = "?page=" . $previous_page . "&limit=" . $pagination->getLimit();
	
            }


             return $result;
         }
        catch(CouldNotConnectToHost $e)
        {
          $scholarships = $this->scholarshipManagementService->findScholarships($fields, $pagination, $sort, $filters);

        return $this->respond($this->scholarshipTransformer->transformCollection($scholarships, $fields, $pagination, $sort));
      }
        catch(BadRequest400Exception $e)
        {
  	    $scholarships = $this->scholarshipManagementService->findScholarships($fields, $pagination, $sort, $filters);

        return $this->respond($this->scholarshipTransformer->transformCollection($scholarships, $fields, $pagination, $sort));
              
        }

   }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $inputs = Input::only([
        	'resident_id',
        	'school_id',
        	'comment',
        	'status',
        	'referred_by',
        	'applied_at',
        	'father_name',
        	'mother_name',
        	'guardian_name',
        	'father_id',
        	'referrer_id',
        	'mother_id',
        	'guardian_id',
        	'guardian_contact_no',
        	'parents_contact_no',
        	'others_contact_no',
        	]);	

        $creator = Auth::user();

        $scholarship = $this->scholarshipManagementService->execute('AddScholarship', $inputs, $creator);

        $res = Resident::find($inputs['resident_id']);
        $res->mobile = Request::input('scholar_contact_no');
		$res->save();

        
        $relationship_array = Input::only(['father_id', 'mother_id']);
        foreach ($relationship_array as $key => $value) {
        	if(isset($relationship_array[$key])){
        		$relation = str_replace("_id", "", $key);
        		
        		$resident = Resident::find($inputs['resident_id']);
        		$r_inputs = [ 'relation' => $relation, 'resident_id' => $value ];
		        $this->profilingService->execute('AddResidentRelation', $r_inputs, $resident, $creator);	
        	}
        }
        

        try{
            $this->scholarshipManagementService->execute('IndexScholar', $inputs, $scholarship);            
        }catch(CouldNotConnectToHost $e){
             Log::error($e);
        }

        return $this->respondData($this->scholarshipTransformer->transform($scholarship));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  Scholarship  $scholarship
	 * @return Response
	 */
	public function show(Scholarship $scholarship)
	{
		return $this->scholarshipTransformer->transform($scholarship);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Scholarship  $scholarship
	 * @return Response
	 */
	public function update(Scholarship $scholarship)
	{
        $inputs = Input::only([
        	'comment',
        	'referred_by',
        	'applied_at',
        	'father_name',
        	'mother_name',
        	'guardian_name',
        	'parents_contact_no',
        	'others_contact_no',
        	'mother_id',
        	'father_id',
        	'referrer_id',
        	'guardian_id',
        	'resident'
        	]);

        $updater = Auth::user();

        // dd($inputs);

        $scholarship = $this->scholarshipManagementService->execute('UpdateScholarship', $inputs, $scholarship, $updater);
        $resident = $this->profilingService->execute('UpdateResident', $inputs['resident'], $scholarship->resident, $updater);
  
        $relationship_array = Input::only(['father_id', 'mother_id']);
        foreach ($relationship_array as $key => $value) {
        	if(isset($relationship_array[$key])){
        		$relation = str_replace("_id", "", $key);
        		
        		$resident = Resident::find($scholarship->resident_id);
        		$r_inputs = [ 'relation' => $relation, 'resident_id' => $value ];
		        $this->profilingService->execute('AddResidentRelation', $r_inputs, $resident, $updater);	
        	}
        }      

        try{
            $this->scholarshipManagementService->execute('IndexScholar', $inputs, $scholarship);  
            $this->profilingService->execute('IndexResident', $inputs, $scholarship->resident);          
        }catch(CouldNotConnectToHost $e){
             Log::error($e);
        }

        return $this->respondData($this->scholarshipTransformer->transform($scholarship));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  Scholarship  $scholarship
	 * @return Response
	 */
	public function destroy(Scholarship $scholarship)
	{
        $inputs = Input::all();

        $this->scholarshipManagementService->execute('RemoveScholarship', $inputs, $scholarship);

        return $this->respondOk('Scholarship deleted');
	}

	/**
	 * @param  ResultCollection $scholarships
	 * @return Resp
	 */
	public function massDelete(ResultCollection $scholarships)
	{
		$inputs = Input::all();

		$user = Auth::user();

		$this->scholarshipManagementService->execute('RemoveScholarships', $inputs, $scholarships, $user);
		
		$this->scholarshipManagementService->execute('RemoveIndexScholarships', $inputs, $scholarships, $user);

		return $this->respondOk('Scholarships deleted');
	}


	/**
	 * Change a scholarship status
	 * 
	 * @param  Scholarship $scholarship
	 * @return Response
	 */
	public function updateStatus(Scholarship $scholarship)
	{
		$inputs = Input::only(['status']);

		$user = Auth::user();

		$scholarship = $this->scholarshipManagementService->execute('UpdateScholarshipStatus', $inputs, $scholarship, $user);
        try{
            $this->scholarshipManagementService->execute('IndexScholar', $inputs, $scholarship);            
        }catch(CouldNotConnectToHost $e){
             Log::error($e);
        }

		return $this->respondData($this->scholarshipTransformer->transform($scholarship));
	}

	public function bulkUpdateStatus(ResultCollection $scholarships)
	{
		$inputs = Input::only(['status']);

		$user = Auth::user();

		$scholarships = $this->scholarshipManagementService->execute('UpdateScholarshipsStatus', $inputs, $scholarships, $user);

		foreach($scholarships as $scholarship)
		{
			$this->scholarshipManagementService->execute('IndexScholar', $inputs, $scholarship);
		}

		return $this->respondData($this->scholarshipTransformer->transformCollection($scholarships));
	}


	public function transfer(Scholarship $scholarship)
	{
		$inputs = Input::only([
        	'resident_id',
        	'school_id',
        	'comment',
        	'status',
        	'referred_by',
        	'parents_contact_no',
        	'others_contact_no',
        	'father_id',
        	'mother_id',
        	'guardian_name',
        	'comment'
        	]);

		$user = Auth::user();

		$newScholarship = $this->scholarshipManagementService->execute('TransferScholarship', $inputs, $scholarship, $user);
        try{
            $this->scholarshipManagementService->execute('IndexScholar', $inputs, $newScholarship);            
        }catch(CouldNotConnectToHost $e){
             Log::error($e);
        }

		return $this->respondData($this->scholarshipTransformer->transform($newScholarship));
	}


	/**
	 * Batch removes scholarships
	 * 
	 * @param  ResultCollection $scholarships
	 * @return Response
	 */
	public function remove($scholarships)
	{
		$inputs = Input::all();
		
		$this->scholarshipManagementService->execute('RemoveScholarships', $inputs, $scholarships);

		return $this->respondOk('Scholarships deleted');	
	}


	/**
	 * Generates scholar ID
	 * 
	 * @param  Scholarship $scholarship
	 * @return PDF
	 */
	public function generateId(Scholarship $scholarship)
	{
		// dd($scholarship);
		$pdf = PDF::loadView('pdf.id', ['data' => $scholarship])->setOrientation('landscape');
		// dd($pdf);
		// return View::make('pdf.id')->with('data', $scholarship);
		return $pdf->stream('id.pdf');
	}


	/**
	 * Leader list excel report.
	 * 
	 * @return Excel
	 */
	public function leaderListPdf()
	{
		$inputs = Input::only([
			'term',
			'school_year',
			'barangay_id'
			]);

		$data['scholarships'] = $this->scholarshipManagementService->findLeaderList($inputs);
		
		return $this->respondPdf($data, 'pdf.leaderList', 'leaderlist.pdf', 'landscape');
	}


	/**
	 * Leader list pdf report.
	 * 
	 * @return Excel
	 */
	public function leaderlistSpreadsheet()
	{
		$inputs = Input::only([
			'term',
			'school_year',
			'barangay_id'
			]);

		$data['scholarships'] = $this->scholarshipManagementService->findLeaderList($inputs);

		return $this->respondExcel($data, 'spreadsheet.leaderList', 'leaderlist.spreadsheet', 'leaderlist');
	}


	/**
	 * Beneficiary list pdf report.
	 * 
	 * @return PDF
	 */
	public function beneficiaryListPdf()
	{
		$inputs = Input::only([
			'term',
			'school_year',
			]);

		$data['scholarships'] = $this->scholarshipManagementService->findBeneficiaryList($inputs);

		return $this->respondPdf($data, 'pdf.beneficiaryList', 'beneficiarylist.pdf', 'landscape');
	}


	/**
	 * Beneficiary list spreadsheet report.
	 * 
	 * @return Excel
	 */
	public function beneficiarylistSpreadsheet()
	{
		$inputs = Input::only([
			'term',
			'school_year',
			]);

		$data['scholarships'] = $this->scholarshipManagementService->findBeneficiaryList($inputs);

		return $this->respondExcel($data, 'spreadsheet.beneficiaryList', 'beneficiarylist.spreadsheet', 'beneficiarylist');
	}
	
	/**
	* Inactivate Scholars
	* @return Response
	**/
	public function inactivate()
	{
		$inputs = Input::only([
			'level',
			'from',
			'to',
			'from_semester',
			'to_semester'
		]);
		// Get Scholars within the specified range
		$scholars = $this->scholarshipManagementService->findInActiveScholar($inputs);
	
		// Get Total Count 
		$total_records = $scholars->count();
		// Inactive Scholars and 
		$this->scholarshipManagementService->execute('InactivateScholars', $inputs, $scholars);
		
		foreach ($scholars as $scholar) {
			$this->scholarshipManagementService->execute('IndexScholar', $inputs, $scholar);
		}

		return $this->respondOk( $total_records . ' scholars have been updated');
	}

	/**
	 * Masterlist Scholarships spreadsheet report.
	 *
	 *	@return excel
	 */
	public function masterlistScholarshipsSpreadsheet()
	{
		set_time_limit(720);

		$sort = new SortParameters(Input::get('sort'));

        	$fields = new FieldsParameters(Input::get('fields'));

	        $filters = new FilterParameters(Input::get('filters'));

		// $data = $this->scholarshipManagementService->findScholarships($fields, null, $sort, $filters);

		// $scholars = $data->map(function($scholarship)
		// {
		// 	return [
		// 		'Last Name' 			=> $scholarship->resident->last_name,
		// 		'First Name' 			=> $scholarship->resident->first_name,
		// 		'Middle Name'		 	=> $scholarship->resident->middle_name,
		// 		'District' 				=> $scholarship->resident->district ? $scholarship->resident->district->name : '',
		// 		'City/Municipality' 	=> $scholarship->resident->cityMunicipality ? $scholarship->resident->cityMunicipality->name : '',
		// 		'Barangay' 				=> $scholarship->resident->barangay ? $scholarship->resident->barangay->name : '',
		// 		"Street" 				=> $scholarship->resident->street,
		// 		'School' 				=> $scholarship->school ? $scholarship->school->name : '',
		// 		'Mothers Name' 			=> $scholarship->mother_name,
		// 		'Fathers Name' 			=> $scholarship->father_name,
		// 		'Comment' 				=> $scholarship->comment
		// 	];
		// });
		// 
		$data = $this->scholarshipManagementService->findScholarshipsES($fields, null, $sort, $filters);

		foreach($data['hits']['hits'] as $scholar)
		{
			$scholars[] = [
				'Last Name' 		=> $scholar['_source']['last_name'],
				'First Name' 		=> $scholar['_source']['first_name'],
				'Middle Name' 		=> $scholar['_source']['middle_name'],
				'District' 			=> $scholar['_source']['district'],
				'City/Municipality' => $scholar['_source']['cityMunicipality'],
				'Barangay' 			=> array_key_exists('barangay_name', $scholar['_source']) ? $scholar['_source']['barangay_name'] : null,
				'Status' 			=> $scholar['_source']['status'],
				'Fathers Name' 		=> $scholar['_source']['father_name'],
				'Mothers Name' 		=> $scholar['_source']['mother_name'],
				'Scholar Contact Number' => $scholar['_source']['scholar_contact_no'],
				'Comment' 			=> $scholar['_source']['comment']
			];
		}

		return $this->respondExcelFromArray($scholars, 'Scholarships Masterlist', 'Scholarships Masterlist', 'Scholarships Masterlist');
	}

	public function refreshRelation(Scholarship $scholarship, $relation){
		switch ($relation) {
			case 'father':
				$scholarship->father_id = null;
				$scholarship->father_name = null;
				$scholarship->save();

				break;
			case 'mother':
				$scholarship->mother_id = null;
				$scholarship->mother_name = null;
				$scholarship->save();				
				break;

			default:
				# code...
				break;
		}

		$this->respondOk("Refresh");
	}
}
