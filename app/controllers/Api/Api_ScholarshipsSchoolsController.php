<?php

class Api_ScholarshipsSchoolsController extends \ApiController {

	function __construct()
	{

	}

	public function index(Scholarship $scholarship)
	{
		$pagination = new PaginationParameters(Input::get('page'), Input::get('limit'));

		$schools = $this->scholarshipManagementService->findSchoolsByScholarship($scholarship, $pagination);

		return $this->repond($this->scholarshipTransformer->transformCollection($schools, null, $pagination));
	}

	public function store(Scholarship $scholarship)
	{
			
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  Scholarship 	$scholarship
	 * @param  School 		$school
	 * @return Response
	 */
	public function show(Scholarship $scholarship, School $school)
	{
		return $this->schoolTransformer->transform($school);
	}

	public function update()
	{

	}

	
	public function destroy()
	{

	}
	 
}