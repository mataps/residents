<?php

use Illuminate\Support\Collection;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;

class Api_ScholarshipsSemestersController extends ApiController {


	/**
	 * @var ScholarshipManagementInterface
	 */
	private $scholarshipManagementService;


	function __construct(ScholarshipManagementInterface $scholarshipManagementInterface, SemesterTransformer $semesterTransformer)
	{
		$this->scholarshipManagementService = $scholarshipManagementInterface;
		$this->semesterTransformer = $semesterTransformer;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	function index(Scholarship $scholarship)
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$semesters = $this->scholarshipManagementService->findSemestersByScholarship($scholarship, $fields, $pagination, $sort, $filters);

		return $this->respond($this->semesterTransformer->transformCollection($semesters, $fields, $pagination, $sort));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	function show(Scholarship $scholarship, Semester $semester)
	{
		return $this->semesterTransformer->transform($semester);
	}
}