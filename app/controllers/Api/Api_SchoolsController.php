<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;

class Api_SchoolsController extends ApiController {


	/**
	 * @var ScholarshipManagementInterface
	 */
	private $scholarshipManagementService;

	/**
	 * @var SchoolTransformer
	 */
	private $schoolTransformer;

	function __construct(
		ScholarshipManagementInterface 	$scholarshipManagementService,
		SchoolTransformer 				$schoolTransformer)
	{
		$this->scholarshipManagementService = $scholarshipManagementService;
		$this->schoolTransformer 			= $schoolTransformer;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$schools = $this->scholarshipManagementService->findSchools($fields, $pagination, $sort, $filters);

		return $this->respond($this->schoolTransformer->transformCollection($schools, $fields, $pagination, $sort));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::only([
			'name',
			'type',
			'level',
			'phone',
			'address'
			]);

		$creator = Auth::user();

		$school = $this->scholarshipManagementService->execute('AddSchool', $inputs, $creator);

		return $this->respondData($this->schoolTransformer->transform($school));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(School $school)
	{
		return $this->schoolTransformer->transform($school);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(School $school)
	{
		$inputs = Input::only([
			'name',
			'address',
			'type',
			'level',
			'phone',
			'address'
			]);

		$updater = Auth::user();

		$school = $this->scholarshipManagementService->execute('UpdateSchool', $inputs, $school, $updater);

		return $this->respondData($this->schoolTransformer->transform($school));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(School $school)
	{
		$inputs = Input::all();

		$this->scholarshipManagementService->execute('RemoveSchool', $inputs, $school);

		return $this->respondOk('School deleted');
	}


	/**
	 * Remove the specified resources from storage
	 * 
	 * @param  Collection $schools
	 * @return [type]          [description]
	 */
	public function massDelete(Collection $schools)
	{
		$inputs = Input::all();

		$this->scholarshipManagementService->execute('RemoveSchools', $inputs, $schools);

		return $this->respondOk('Schools deleted');
	}
}
