<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;

class Api_SemestersAllowanceReleasesController extends ApiController {

	/**
	 * @var ScholarshipManagementInterface
	 */
	private $scholarshipManagementService;


	/**
	 * @var AllowanceReleaseTransformer
	 */
	private $releaseTransformer;


	/**
	 * @var AllowanceTransformer
	 */
	private $allowanceTransformer;


	function __construct(
		ScholarshipManagementInterface $scholarshipManagementInterface,
		SemesterAllowanceReleaseTransformer $releaseTransformer,
		AllowanceTransformer $allowanceTransformer)
	{
		$this->scholarshipManagementService = $scholarshipManagementInterface;
		$this->releaseTransformer = $releaseTransformer;
		$this->allowanceTransformer = $allowanceTransformer;
	}


	/**
	 * Display a listing of the resource.
	 * 
	 * @param  Semester $semester
	 * @return Response
	 */
	public function index(Semester $semester)
	{
		$inputs = Input::all();

		$sort = new SortParameters(Input::get('sort'));

		$fields = new FieldsParameters(Input::get('fields'));

		$pagination = PaginationParameters::createFromArray($inputs);

		$filters = new FilterParameters(Input::get('filters'));

		$releases = $this->scholarshipManagementService->findReleasesBySemester($semester, $fields, $pagination, $sort, $filters);

		return $this->respond($this->releaseTransformer->transformCollection($releases, $fields, $pagination, $sort, [], $semester));
	}


	public function show(Semester $semester, AllowanceRelease $allowanceRelease)
	{

	}
	

	public function generateAllowance(Semester $semester, AllowanceRelease $allowanceRelease)
	{
		$inputs = Input::all();

		$creator = Auth::user();

		$allowance = $this->scholarshipManagementService->execute('AddAllowance', $inputs, $semester, $allowanceRelease, $creator);

		return $this->respondData($this->allowanceTransformer->transform($allowance));
	}

}