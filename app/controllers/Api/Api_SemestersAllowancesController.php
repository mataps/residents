<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;
use SourceScript\Common\Collections\ResultCollection;

class Api_SemestersAllowancesController extends ApiController {

	/**
	 * @var ScholarshipManagementInterface
	 */
	private $scholarshipManagementService;

	function __construct(
		scholarshipManagementInterface $scholarshipManagementInterface,
		AllowanceTransformer $allowanceTransformer)
	{
		$this->scholarshipManagementService = $scholarshipManagementInterface;
		$this->allowanceTransformer = $allowanceTransformer;
	}


	/**
	 * Allowances of a semester
	 * 
	 * @param  Semester $semester
	 * @return Response
	 */
	public function index(Semester $semester)
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        $allowances = $this->scholarshipManagementService->findAllowancesBySemester($semester, $fields, $pagination, $sort, $filters);

        return $this->respond($this->allowanceTransformer->transformCollection($allowances, $fields, $pagination, $sort));
	}

	/**
	 * Generates allowance
	 * 
	 * @param  Scholarship $scholarship
	 * @return Response
	 */
	function generate(Semester $semester)
	{
		$inputs = Input::all();

		$creator = Auth::user();

		$allowance = $this->scholarshipManagementService->execute('AddAllowance', $inputs, $semester, $creator);
		
		return $this->respondData($this->allowanceTransformer->transform($allowance));
	}


	/**
	 * Generates allowances of multiple scholars
	 * 
	 * @param  Collection $scholarships
	 * @return Response
	 */
	function batchGenerate(ResultCollection $semesters)
	{
		$inputs = Input::all();

		$creator = Auth::user();

		$allowances = $this->scholarshipManagementService->execute('AddAllowances', $inputs, $semesters, $creator);

		return $this->respondData($allowances);

	}
}