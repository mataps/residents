<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;
use SourceScript\Profiling\ProfilingInterface;

class Api_SemestersController extends ApiController {


	/**
	 * @var ScholarshipManagementService
	 */
	private $scholarshipManagementService;

	/**
	 * @var SemesterTransformer
	 */
	private $semesterTransformer;


	function __construct(
		ScholarshipManagementInterface $scholarshipManagementService,
		SemesterTransformer $semesterTransformer,
		ProfilingInterface $profilingService)
	{
        $this->profilingService = $profilingService;
		$this->scholarshipManagementService = $scholarshipManagementService;
		$this->semesterTransformer = $semesterTransformer;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		// $semesters = $this->scholarshipManagementService->findSemesters($fields, $pagination, $sort, $filters);

	   try
        {
       		$semesters = $this->scholarshipManagementService->findSemestersES($fields, $pagination, $sort, $filters);

            $result = ['data' => []];
            foreach ($semesters['hits']['hits'] as $semester) {
                $semester['_source']['es_id'] = $semester['_id'];
                $semester['_source']['id'] = $semester['_id'];
                $semester['_source']['resident_full_name'] = ucwords(strtolower($semester['_source']['resident_full_name'] ));
                $semester['_source']['last_name'] = ucwords(strtolower($semester['_source']['last_name'] ));
                $semester['_source']['first_name'] = ucwords(strtolower($semester['_source']['first_name'] ));
                $semester['_source']['middle_name'] = ucwords(strtolower($semester['_source']['middle_name'] ));


            
                array_push($result['data'], $semester['_source']);
             }

            $next_page = $pagination->getPage() + 1;
            $previous_page = $pagination->getPage() - 1;
            $end_count =  ($semesters['hits']['total'] > 0) ? ($pagination->getFrom() + $pagination->getLimit()) : 0;
            $result['paging'] = [
                    'total' => $semesters['hits']['total'],
                    'start_count' => $pagination->getFrom() + 1,
                    'end_count' => $end_count,
                    'next' => "?page=". $next_page ."&limit=" . $pagination->getLimit()
                ];

            if($pagination->getPage() > 1)
            	$result['paging']['previous'] = "?page=" . $previous_page . "&limit=" . $pagination->getLimit();

             return $result;
        }
        catch(CouldNotConnectToHost $e)
        {
        	
		$semesters = $this->transactionSystemService->findTransactions($fields, $pagination, $sort, $filters, $permission);
		return $this->respond($this->semesterTransformer->transformCollection($semesters, $fields, $pagination, $sort));
     	
		}
		catch(BadRequest400Exception $e)
		{
			$semesters = $this->transactionSystemService->findTransactions($fields, $pagination, $sort, $filters, $permission);
			return $this->respond($this->semesterTransformer->transformCollection($semesters, $fields, $pagination, $sort));
     	
		}



	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Semester $semester)
	{
		return $this->respond($this->semesterTransformer->transform($semester));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::only([
			'is_probation',
			'allowance_basis_id',
			'scholarship_type_id',
			'scholarship_id',
			'school_id',
			'school_year',
			'term',
			'gwa',
			'comment',
			'subject_count',
			'year_level',
			'school_level',
			'course_id',
			'beneficiary_id',
			'client_id',
			'status',
			'grades',
			'created_at',
			'date_encoded'
			]);

		// if($inputs['grades']) $inputs['grades'] = json_decode($inputs['grades'], true);
		// dd($inputs);

		$creator = Auth::user();

		$semester = $this->scholarshipManagementService->execute('AddSemester', $inputs, $creator);

		$this->scholarshipManagementService->execute('IndexSemester', $inputs, $semester);

		return $this->respond($this->semesterTransformer->transform($semester));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  semester  $semester
	 * @return Response
	 */
	public function update(Semester $semester)
	{
		$inputs = Input::only([
			'is_probation',
			'status',
			'allowance_basis_id',
			'scholarship_type_id',
			'school_id',
			'school_year',
			'term',
			'gwa',
			'comment',
			'subject_count',
			'year_level',
			'course_id',
			'beneficiary_id',
			'client_id',
			'school_level',
			'created_at',
			'date_encoded'
			
			]);


		$inputs['beneficiary_id'] = $semester->scholarship->resident->id;


		$residentInput = Input::only(['scholarship.resident']);

		$resident_input = $residentInput['scholarship']['resident'];
		// dd($resident_input);
		$updater = Auth::user();

		$semester = $this->scholarshipManagementService->execute('UpdateSemester', $inputs, $semester, $updater);
		
		foreach($semester->scholarship->semesters as $sem){
					$this->scholarshipManagementService->execute('IndexSemester', $inputs, $sem);
		}
        
		return $this->respond($this->semesterTransformer->transform($semester));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Resident  $resident
	 * @return Response
	 */
	public function updateGWA(Semester $semester)
	{
		
		$inputs = Input::only(['gwa']);
		
		$updater = Auth::user();

		$semester = $this->scholarshipManagementService->execute('UpdateGWA', $inputs, $semester, $updater);
		
		foreach($semester->scholarship->semesters as $sem){
					$this->scholarshipManagementService->execute('IndexSemester', $inputs, $sem);
		}
        
		return $this->respond($this->semesterTransformer->transform($semester));
	}



	/**
	 * @param  Semester $semester
	 * @return Response
	 */
	public function destroy(Semester $semester)
	{
		$inputs = Input::all();

		$this->scholarshipManagementService->execute('RemoveSemester', $inputs, $semester);

		return $this->respondOk('Semester removed');
	}


	/**
	 * @param  ResultCollection $semesters
	 * @return Response
	 */
	public function massDestroy($semesters)
	{
		
		$inputs = Input::all();
		

		foreach ($semesters as $semester) {
			$this->scholarshipManagementService->execute('DeleteIndexSemester', $inputs, $semester);
		}


		$this->scholarshipManagementService->execute('RemoveSemesters', $inputs, $semesters);

		foreach ($semesters as $semester) {
			$this->scholarshipManagementService->execute('IndexScholar', ['from_semester_index' => true], $semester->scholarship);
		}




		return $this->respondOk('Semesters removed');
	}

	public function semestersSpreadsheet()
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        $data = $this->scholarshipManagementService->findSemestersES($fields, $pagination, $sort, $filters);

        $semesters = [];
        $fields = explode(',', Input::get('fields'));

	

        foreach($data['hits']['hits'] as $semester)
        {
        	// dd($fields);
            foreach($fields as $field)
            {
                $semesters[$semester['_id']][$field] = $semester['_source'][$field];
            }
        }

        return $this->respondExcelFromArray($semesters, 'List of semesters', 'List of semesters');

	}
}
