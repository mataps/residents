<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;

class Api_SemestersGradesController extends ApiController {
	

	/**
	 * @var ScholarshipManagementInterface
	 */
	private $scholarshipmanagementService;

	/**
	 * @var GradeTransformer
	 */
	private $gradeTransformer;


	function __construct(ScholarshipManagementInterface $scholarshipManagementInterface, GradeTransformer $gradeTransformer)
	{
		$this->scholarshipmanagementService = $scholarshipManagementInterface;
		$this->gradeTransformer = $gradeTransformer;
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index(Semester $semester)
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        $grades = $this->scholarshipmanagementService->findGradesBySemester($semester, $fields, $pagination, $sort, $filters);

        return $this->respond($this->gradeTransformer->transformCollection($grades, $fields, $pagination, $sort));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(Semester $semester, Grade $grade)
	{
		return $this->gradeTransformer->transform($grade);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Semester $semester)
	{
		$inputs = Input::only(['grade', 'units', 'subject_id']);

		$creator = Auth::user();

		$grade = $this->scholarshipmanagementService->execute('AddGrade', $inputs, $semester, $creator);

		return $this->respondData($this->gradeTransformer->transform($grade));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(Semester $semester, Grade $grade)
	{
		$inputs = Input::only(['grade', 'units', 'subject_id']);

		$updater = Auth::user();

		$grade = $this->scholarshipmanagementService->execute('UpdateGrade', $inputs, $semester, $grade, $updater);

		return $this->respondData($this->gradeTransformer->transform($grade));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Semester $semester, Grade $grade)
	{
		$inputs = Input::all();

		$this->scholarshipmanagementService->execute('RemoveGrade', $inputs, $grade);

		return $this->respondOk('Grade deleted');
	}
}