<?php

class Api_Sigplus extends ApiController{

    public function showSigplus()
    {
        return View::make('sigplus');
    }

    public function postSigplus()
    {
        function hex2bincustom($h)
        {
            if (!is_string($h)) return null;
            $r='';
            for ($a=0; $a<strlen($h); $a+=2)
            {
                $r.=chr(hexdec($h{$a}.$h{($a+1)}));
            }
            return $r;
        }

        $hex = hex2bincustom(Input::get('sigImgData'));
        $folder = date('m-d-Y');
        $fileName = time() . '_signature.jpg';

        $file = join('/', array(public_path() . '/uploads', $folder, $fileName));
        file_force_put_contents($file, $hex);

        return Response::json(['data'=>['file_name'=>$fileName, 'url'=>join('/', array('/uploads', $folder, $fileName))]]);
    }
}