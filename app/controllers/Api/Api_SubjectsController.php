<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;

class Api_SubjectsController extends ApiController {


	/**
	 * @var ScholarshipManagementService
	 */
	private $scholarshipManagementService;

	/**
	 * @var SubjectTransfromer
	 */
	private $subjectTransformer;


	function __construct(
		ScholarshipManagementInterface 	$scholarshipManagementService,
		SubjectTransformer 				$subjectTransformer)
	{
		$this->scholarshipManagementService = $scholarshipManagementService;
		$this->subjectTransformer 			= $subjectTransformer;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$subjects = $this->scholarshipManagementService->findSubjects($fields, $pagination, $sort, $filters);

		return $this->respond($this->subjectTransformer->transformCollection($subjects, $fields, $pagination, $sort));
	}

	public function nonPaginated()
	{
		$subjects = $this->scholarshipManagementService->findSubjects($pagination);

		return $this->respond($this->subjectTransformer->transformCollection($subjects, null, $pagination));	
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::only(['name', 'description']);

		$creator = Auth::user();

		$subject = $this->scholarshipManagementService->execute('AddSubject', $inputs, $creator);

		return $this->respondData($this->subjectTransformer->transform($subject));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  Subject  $subject
	 * @return Response
	 */
	public function show(Subject $subject)
	{
		return $this->subjectTransformer->transform($subject);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Subject  $subject
	 * @return Response
	 */
	public function update(Subject $subject)
	{
		$inputs = Input::only(['name', 'description']);

		$updater = Auth::user();

		$subject = $this->scholarshipManagementService->execute('UpdateSubject', $inputs, $subject, $updater);

		return $this->respondData($this->subjectTransformer->transform($subject));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(Subject $subject)
	{
		$inputs = Input::all();

		$this->scholarshipManagementService->execute('RemoveSubject', $inputs, $subject);

		return $this->respondOk('Subject deleted');
	}


	public function remove($subjects)
	{
		$inputs = Input::all();

		$this->scholarshipManagementService->execute('RemoveSubjects', $inputs, $subjects);

		return $this->respondOk('Subjects Deleted');
	}

	
	/**
	 * Type ahead search
	 * 
	 * @param  string $name
	 * @return Response
	 */
	public function searchName($name)
	{
		$results = $this->scholarshipManagementService->searchSubjectsByName($name);

        return $this->respondData($results);
	}


}
