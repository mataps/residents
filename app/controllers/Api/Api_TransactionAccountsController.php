<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\TransactionSystem\TransactionSystemInterface;

class Api_TransactionAccountsController extends ApiController {


	/**
	 * @var TransactionSystemInterface
	 */
	private $transactionManagementService;


	/**
	 * @var TransactionAccountTransformer
	 */
	private $transactionAccountTransformer;


	function __construct(TransactionSystemInterface $transactionManagementInterface, TransactionAccountTransformer $transactionAccountTransformer)
	{
		$this->transactionManagementService = $transactionManagementInterface;
		$this->transactionAccountTransformer = $transactionAccountTransformer;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        $permission = Auth::user()->getAllowedTransactionAccounts();

		$accounts = $this->transactionManagementService->findAccounts($permission, $fields, $pagination, $sort, $filters);	

		return $this->respond($this->transactionAccountTransformer->transformCollection($accounts, $fields, $pagination, $sort));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(TransactionAccount $account)
	{
		return $this->transactionAccountTransformer->transform($account);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::only(['name', 'signatory', 'label', 'designation', 'signatory_2', 'label_2', 'designation_2', 'affiliation_id','fixed_voucher_number_format']);

		$creator = Auth::user();

		$account = $this->transactionManagementService->execute('AddAccount', $inputs, $creator);

		return $this->respond($this->transactionAccountTransformer->transform($account));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(TransactionAccount $account)
	{
		$inputs = Input::only(['name', 'signatory', 'label', 'designation', 'signatory_2', 'label_2', 'designation_2', 'affiliation_id', 'fixed_voucher_number_format']);

		$updater = Auth::user();

		$account = $this->transactionManagementService->execute('UpdateAccount', $inputs, $account, $updater);

		return $this->respond($this->transactionAccountTransformer->transform($account));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(TransactionAccount $account)
	{
		$inputs = Input::all();

		$this->transactionManagementService->execute('RemoveAccount', $inputs);

		return $this->respondOk('Account deleted');
	}
}