<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\TransactionSystem\TransactionSystemInterface;
use SourceScript\Common\Collections\ResultCollection;

class Api_TransactionCategoriesController extends ApiController {


	/**
	 * @var TransactionSystemInterface
	 */
	private $transactionSystemService;


	/**
	 * @var TransactionCategoryTransformer
	 */
	private $transactionCategoryTransformer;


	function __construct(TransactionSystemInterface $transactionSystemInterface, TransactionCategoryTransformer $transactionCategoryTransformer)
	{
		$this->transactionSystemService = $transactionSystemInterface;
		$this->transactionCategoryTransformer = $transactionCategoryTransformer;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$categories = $this->transactionSystemService->findCategories($fields, $pagination, $sort, $filters);	

		return $this->respond($this->transactionCategoryTransformer->transformCollection($categories, $fields, $pagination, $sort));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(TransactionCategory $category)
	{
		return $this->transactionCategoryTransformer->transform($category);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::only(['name']);

		$creator = Auth::user();

		$category = $this->transactionSystemService->execute('AddCategory', $inputs, $creator);

		return $this->respond($this->transactionCategoryTransformer->transform($category));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(TransactionCategory $category)
	{
		$inputs = Input::only(['name']);

		$updater = Auth::user();

		$category = $this->transactionSystemService->execute('UpdateCategory', $inputs, $category, $updater);

		return $this->respond($this->transactionCategoryTransformer->transform($category));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(TransactionCategory $category)
	{
		$inputs = Input::all();

		$this->transactionSystemService->execute('RemoveCategory', $inputs);

		return $this->respondOk('Category deleted');
	}


	/**
	 * Remove categories
	 * 
	 * @param  ResultCollection $categories
	 * @return Response
	 */
	public function massDelete(ResultCollection $categories)
	{
		$inputs = Input::all();

		$this->transactionSystemService->execute('RemoveCategories', $inputs, $categories);

		return $this->respondOk('Categories deleted!');			
	}
}