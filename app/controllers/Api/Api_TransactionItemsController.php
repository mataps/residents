<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\TransactionSystem\TransactionSystemInterface;

class Api_TransactionItemsController extends ApiController {


	/**
	 * @var TransactionSystemInterface
	 */
	private $transactionManagementService;


	/**
	 * @var TransactionAccountTransformer
	 */
	private $transactionItemTransformer;


	function __construct(TransactionSystemInterface $transactionManagementInterface, TransactionItemTransformer $transactionItemTransformer)
	{
		$this->transactionManagementService = $transactionManagementInterface;
		$this->transactionItemTransformer = $transactionItemTransformer;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$items = $this->transactionManagementService->findItems($fields, $pagination, $sort, $filters);	

		return $this->respond($this->transactionItemTransformer->transformCollection($items, $fields, $pagination, $sort));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(TransactionAccount $account)
	{
		return $this->transactionAccountTransformer->transform($account);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::only(['name', 'signatory']);

		$creator = Auth::user();

		$account = $this->transactionManagementService->execute('AddAccount', $inputs, $creator);

		return $this->respond($this->transactionAccountTransformer->transform($account));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(TransactionAccount $account)
	{
		$inputs = Input::only([]);

		$updater = Auth::user();

		$account = $this->transactionManagementService->execute('UpdateAccount', $inputs, $updater);

		return $this->respond($this->transactionAccountTransformer->transform($account));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(TransactionAccount $account)
	{
		$inputs = Input::all();

		$this->transactionManagementService->execute('RemoveAccount', $inputs);

		return $this->respondOk('Account deleted');
	}
}