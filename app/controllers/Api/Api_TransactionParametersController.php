<?php

class Api_TransactionParametersController extends ApiController {


	function __construct(
		TransactionCategoryItemTransformer $transactionCategoryItemTransformer,
		TransactionAccountTransformer $transactionAccountTransformer)
	{
		$this->transactionAccountTransformer = $transactionAccountTransformer;
		$this->categoryTransformer = $transactionCategoryItemTransformer;
	}

	public function parameters()
	{
		$parameters = [
			'categories' => $this->categories(),
			'account_types' => $this->accountTypes()
		];

		return $this->respondData($parameters);
	}

	public function accountTypes()
	{
		$accountTypes = TransactionAccount::all();

		return $this->transactionAccountTransformer->transformCollection($accountTypes);
	}


	public function categories()
	{
		//TODO
		$items = TransactionCategoryItem::all();

		return $this->categoryTransformer->transformCollection($items);
	}
}