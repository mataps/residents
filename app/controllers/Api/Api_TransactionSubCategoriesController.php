<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\TransactionSystem\TransactionSystemInterface;
use SourceScript\Common\Collections\ResultCollection;

class Api_TransactionSubCategoriesController extends ApiController {


	/**
	 * @var TransactionSystemInterface
	 */
	private $transactionSystemService;


	/**
	 * @var TransactionSubCategoryTransformer
	 */
	private $transactionSubCategoryTransformer;


	function __construct(TransactionSystemInterface $transactionSystemInterface, TransactionSubCategoryTransformer $transactionSubCategoryTransformer)
	{
		$this->transactionSystemService = $transactionSystemInterface;
		$this->transactionSubCategoryTransformer = $transactionSubCategoryTransformer;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$categories = $this->transactionSystemService->findSubCategories($fields, $pagination, $sort, $filters);	

		return $this->respond($this->transactionSubCategoryTransformer->transformCollection($categories, $fields, $pagination, $sort));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(TransactionSubCategory $subCategory)
	{
		return $this->transactionSubCategoryTransformer->transform($subCategory);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::only([
			'category_id',
			'name'
			]);

		$creator = Auth::user();

		$subCategory = $this->transactionSystemService->execute('AddSubCategory', $inputs, $creator);

		return $this->respondData($this->transactionSubCategoryTransformer->transform($subCategory));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(TransactionSubCategory $subCategory)
	{
		$inputs = Input::only([
			'category_id',
			'name'
			]);

		$updater = Auth::user();

		$subCategory = $this->transactionSystemService->execute('UpdateSubCategory', $inputs, $subCategory, $updater);

		return $this->respondData($this->transactionSubCategoryTransformer->transform($subCategory));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy(TransactionSubCategory $subCategory)
	{
		$inputs = [];

		$this->transactionSystemService->execute('RemoveSubCategory', $inputs, $subCategory);

		return $this->respondOk('Subcategory deleted');
	}


	/**
	 * @param  ResultCollection $subCategories
	 * @return [type]                         
	 */
	public function massDestroy(ResultCollection $subCategories)
	{
		$inputs = Input::all();

		$this->transactionSystemService->execute('RemoveSubCategories', $inputs, $subCategories);

		return $this->respondOk('Sub categories removed');
	}
}