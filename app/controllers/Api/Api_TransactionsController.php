<?php

use SourceScript\Common\Paginators\FilterableTrait;
use SourceScript\Common\Paginators\PaginatableTrait;
use SourceScript\Common\Paginators\SortableTrait;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Common\Repositories\GenericFinderInterface;
use SourceScript\TransactionSystem\TransactionSystemInterface;
use Elasticsearch\Client;
use Elasticsearch\Common\Exceptions\Curl\CouldNotConnectToHost;
use Elasticsearch\Common\Exceptions\BadRequest400Exception;
use SourceScript\Common\Collections\ResultCollection;

class Api_TransactionsController extends ApiController {

    /**
     * @var TransactionSystemInterface
     */
    private $transactionSystemService;
    /**
     * @var TransactionTransformer
     */
    private $transactionTransformer;

    function __construct(TransactionSystemInterface $transactionSystemService, TransactionTransformer $transactionTransformer)
    {
        $this->transactionSystemService = $transactionSystemService;
        $this->transactionTransformer = $transactionTransformer;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

        $inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$permission = Auth::user()->getAllowedTransactionAccounts();

        try
        {
            $transactions = $this->transactionSystemService->findTransactionsES($fields, $pagination, $sort, $filters, $permission);

            $result = ['data' => []];
            foreach ($transactions['hits']['hits'] as $resident) {
                $resident['_source']['es_id'] = $resident['_id'];
                $resident['_source']['client_name'] = ucwords(strtolower($resident['_source']['client_name']));
                $resident['_source']['beneficiary_name'] = ucwords(strtolower($resident['_source']['beneficiary_name']));
                $resident['_source']['referer_name'] = ucwords(strtolower($resident['_source']['referer_name']));
                array_push($result['data'], $resident['_source']);
             }

            $next_page = $pagination->getPage() + 1;
            $previous_page = $pagination->getPage() - 1;
            $end_count =  ($transactions['hits']['total'] > 0) ? ($pagination->getFrom() + $pagination->getLimit()) : 0;
            $result['paging'] = [
                    'total' => $transactions['hits']['total'],
                    'start_count' => $pagination->getFrom() + 1,
                    'end_count' => $end_count,
                    'next' => "?page=". $next_page ."&limit=" . $pagination->getLimit()
                ];

            if($pagination->getPage() > 1){
            		$result['paging']['previous'] = "?page=" . $previous_page . "&limit=" . $pagination->getLimit();
            }
             return $result;
        }
        catch(CouldNotConnectToHost $e)
        {
        	Log::info("Could not connect to ES");
		$transactions = $this->transactionSystemService->findTransactions($fields, $pagination, $sort, $filters, $permission);
		return $this->respond($this->transactionTransformer->transformCollection($transactions, $fields, $pagination, $sort));

		}
		catch(BadRequest400Exception $e)
		{
			//Log::error($e);
			//Log::info("Bad Request");
			$transactions = $this->transactionSystemService->findTransactions($fields, $pagination, $sort, $filters, $permission);
			return $this->respond($this->transactionTransformer->transformCollection($transactions, $fields, $pagination, $sort));

		}
    }

    public function liquidatables()
    {

		$permission = Auth::user()->getAllowedTransactionAccounts();

    	$count = $this->transactionSystemService->findLiquidatableCount($permission);

    	return $this->respond(['count' => $count]);
    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $inputs = Input::only([
        	'name',
        	'voucher_id',
        	'client_type',
        	'client_id',
        	'referrer_id',
        	'beneficiary_id',
        	'beneficiary_type',
        	'account_type_id',
        	'sub_category_id',
        	'item_name',
        	'transaction_type',
        	'details',
        	'amount',
        	'remarks',
        	'liquidatable',
        	'vendor_id',
        	'created_at',
        	'transfer_account_id'
        	]);
        $creator = Auth::user();

        $transaction = $this->transactionSystemService->execute('AddTransaction', $inputs, $creator);
        // dd($group);
        $this->transactionSystemService->execute('IndexTransaction', $inputs, $transaction);

        return $this->respondData($this->transactionTransformer->transform($transaction));
	}


	/**
	 * Stores multiple transactions.
	 *
	 * @param  ResultCollection $residents
	 * @return Response
	 */
	public function massStore(ResultCollection $residents)
	{
		$inputs = Input::only([
			'name',
        	// 'voucher_id',
        	'account_type_id',
        	'sub_category_id',
        	'item_name',
        	'transaction_type',
        	'details',
        	'amount',
        	'remarks'
			]);

		$creator = Auth::user();

		$transactions = $this->transactionSystemService->execute('AddTransactions', $inputs, $residents, $creator);

		foreach($transactions as $transaction)
		{
			$this->transactionSystemService->execute('IndexTransaction', $inputs, $transaction);
		}

		return $this->respond($this->transactionTransformer->transformCollection($transactions));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  Transaction  $group
	 * @return Response
	 */
	public function show(Transaction $transaction)
	{
		return $this->transactionTransformer->transform($transaction);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  Transaction  $group
	 * @return Response
	 */
	public function update(Transaction $transaction)
	{
        $inputs = Input::only([
        	'name',
        	'voucher_id',
        	'client_type',
        	'client_id',
        	'referrer_id',
        	'beneficiary_id',
        	'beneficiary_type',
        	'account_type_id',
        	'sub_category_id',
        	'item_name',
        	'transaction_type',
        	'details',
        	'amount',
        	'remarks',
        	'liquidatable',
        	'vendor_id',
        	'transfer_account_id',
        	'created_at'
        	]);

        $user = Auth::user();

        $transaction = $this->transactionSystemService->execute('UpdateTransaction', $inputs, $transaction, $user);

        $this->transactionSystemService->execute('IndexTransaction', $inputs, $transaction);

        return $this->respondData($this->transactionTransformer->transform($transaction));
	}

	/**
	* Override Update Transaction
	* @param Transaction $group
	* @return Response
	*
	*
	*/

	public function overrideUpdate(Transaction $transaction)
	{
	        $inputs = Input::only([
        	'name',
        	'voucher_id',
        	'client_type',
        	'client_id',
        	'referrer_id',
        	'beneficiary_id',
        	'beneficiary_type',
        	'account_type_id',
        	'sub_category_id',
        	'item_name',
        	'transaction_type',
        	'details',
        	'amount',
        	'remarks',
        	'liquidatable',
        	'vendor_id'
        	]);

	        $user = Auth::user();

	        $transaction = $this->transactionSystemService->execute('OverrideUpdateTransaction', $inputs, $transaction, $user);

	        $this->transactionSystemService->execute('IndexTransaction', $inputs, $transaction);

	        return $this->respondData($this->transactionTransformer->transform($transaction));

	}

	/**
	* Salary Store
	*
	* @return Response
	*
	*/
	public function salaryStore()
	{
		$inputs =  Input::all();

		$user = Auth::user();


		$transactions = $this->transactionSystemService->execute('AddSalaries', $inputs, $user);

		foreach($transactions as $transaction)
		{
			$this->transactionSystemService->execute('IndexTransaction', $inputs, $transaction);
		}

		return $this->respond($this->transactionTransformer->transformCollection($transactions));

	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  Transaction  $group
	 * @return Response
	 */
	public function destroy(Transaction $group)
	{
        $inputs = Input::all();

        $this->transactionSystemService->execute('RemoveTransaction', $inputs, $group);

        return $this->respondOk('Transaction deleted');
	}


	/**
	 * Settles an account
	 *
	 * @param  Transaction $transaction
	 * @return Response
	 */
	public function settleAccount(Transaction $transaction)
	{
		$transaction = $this->transactionSystemService->execute('SettleTransaction', [], $transaction, Auth::user());

        $this->transactionSystemService->execute('IndexTransaction', [], $transaction);

		return $this->transactionTransformer->transform($transaction);
	}


	/**
	 * Liquidates a transaction.
	 *
	 * @param  Transaction $transaction
	 * @return Response
	 */
	public function liquidate(Transaction $transaction)
	{
		$inputs = Input::all();

		$liquidator = Auth::user();

		$transaction = $this->transactionSystemService->execute('Liquidate', $inputs, $transaction, $liquidator);

		if($transaction->amount > 0 )
		{
			$this->transactionSystemService->execute('IndexTransaction', $inputs, $transaction);
		}

		return $this->transactionTransformer->transform($transaction);
	}


	/**
	 * Refunds transaction
	 *
	 * @param  Transaction $transaction
	 * @return Response
	 */
	public function refund(Transaction $transaction)
	{
		$inputs = Input::all();

		$user = Auth::user();

		$refund = $this->transactionSystemService->execute('RefundTransaction', $inputs, $transaction, $user);

		return $this->transactionTransformer->transform($refund);
	}


	/**
	 * Generate Excel Reports
	 *
	 * @return Excel
	 */
	public function reportsSpreadsheet()
	{
		$inputs = Input::only([
			'from',
			'to',
			'account_type_id',
			'summary',
			'transaction_type'
			]);

		$inputs['summary'] = (boolean) $inputs['summary'];

		$data = $this->transactionSystemService->getReportData($inputs['from'], $inputs['to'], $inputs['account_type_id'], $inputs['summary'], $inputs['transaction_type']);
		$this->respondExcel($data, $inputs['summary'] ? 'spreadsheet.summaryTransaction' : 'spreadsheet.detailedTransaction', "Transaction Reports", "Transaction Reports", $data['from'] . ' - ' . $data['to']);
	}


	/**
	 * Generate Excel cashflow report
	 *
	 * @return Excel
	 */
	public function cashflowReportsSpreadsheet()
	{
		$inputs = Input::only([
			'from',
			'to',
			'account_type_id',
			'summary',
			'transaction_type',
			'transfer'
			]);

		$inputs['summary'] = (boolean) $inputs['summary'];

		if($inputs['transfer'])
		{
			$data['availableCash'] = $this->transactionSystemService->getAvailableCash($inputs['from'], $inputs['account_type_id'], true);
			$data['income'] = $this->transactionSystemService->getReportData($inputs['from'], $inputs['to'], $inputs['account_type_id'], $inputs['summary'], 'transfer-income');
			$data['expense'] = $this->transactionSystemService->getReportData($inputs['from'], $inputs['to'], $inputs['account_type_id'], $inputs['summary'], 'transfer-expense');
			$this->respondExcel($data, $inputs['summary'] ? 'spreadsheet.summaryCashflow' : 'spreadsheet.detailedCashflow', "Transaction Reports", "Transaction Reports", $data['income']['from'] . ' - ' . $data['income']['to']);	
		}
		else
		{
			$data['availableCash'] = $this->transactionSystemService->getAvailableCash($inputs['from'], $inputs['account_type_id']);
			$data['income'] = $this->transactionSystemService->getReportData($inputs['from'], $inputs['to'], $inputs['account_type_id'], $inputs['summary'], 'income');
			$data['expense'] = $this->transactionSystemService->getReportData($inputs['from'], $inputs['to'], $inputs['account_type_id'], $inputs['summary'], 'expense');
			$this->respondExcel($data, $inputs['summary'] ? 'spreadsheet.summaryCashflow' : 'spreadsheet.detailedCashflow', "Transaction Reports", "Transaction Reports", $data['income']['from'] . ' - ' . $data['income']['to']);	
		}

	}


	/**
	 * Generate PDF Reports
	 *
	 * @return PDF
	 */
	public function reportsPdf()
	{
		$inputs = Input::only([
			'from',
			'to',
			'account_type_id',
			'summary',
			'transaction_type',
			'paper_size'
			]);

		$summary = ($inputs['summary'] == "true") ? 1 : 0;

		if(!$inputs['paper_size']) $inputs['paper_size'] = 'legal';

		$inputs['summary'] = (boolean) $summary;

		$data = $this->transactionSystemService->getReportData($inputs['from'], $inputs['to'], $inputs['account_type_id'], $inputs['summary'], $inputs['transaction_type']);

		$data['user'] = Auth::user();

		if(Input::get('test') == "true"){
			dd($data);
		}

		return $this->respondPdf($data, $inputs['summary'] ? 'pdf.summaryTransaction' : 'pdf.detailedTransaction', "report.pdf", $inputs['summary'] ? 'portrait' : 'landscape', $inputs['paper_size']);
	}


	/**
	 * Generate PDF cashflow report
	 *
	 * @return PDF
	 */
	public function cashflowReportsPdf()
	{
		$inputs = Input::only([
			'from',
			'to',
			'account_type_id',
			'summary',
			'paper_size',
			'transfer'
			]);

		$inputs['summary'] = (boolean) $inputs['summary'];

		$data['user'] = Auth::user();

		if($inputs['transfer'])
		{
			$data['availableCash'] = $this->transactionSystemService->getAvailableCash($inputs['from'], $inputs['account_type_id'], true);
			$data['income'] = $this->transactionSystemService->getReportData($inputs['from'], $inputs['to'], $inputs['account_type_id'], $inputs['summary'], "transfer-income");
			$data['expense'] = $this->transactionSystemService->getReportData($inputs['from'], $inputs['to'], $inputs['account_type_id'], $inputs['summary'], "transfer-expense");

			return $this->respondPdf($data, $inputs['summary'] ? 'pdf.summaryTransferCashflow' : 'pdf.detailedTransferCashflow', 'cashflow.pdf', $inputs['summary'] ? 'portrait' : 'landscape', $inputs['paper_size']);
			
		}
		$data['availableCash'] = $this->transactionSystemService->getAvailableCash($inputs['from'], $inputs['account_type_id']);
		$data['income'] = $this->transactionSystemService->getReportData($inputs['from'], $inputs['to'], $inputs['account_type_id'], $inputs['summary'], "income");
		$data['expense'] = $this->transactionSystemService->getReportData($inputs['from'], $inputs['to'], $inputs['account_type_id'], $inputs['summary'], "expense");

		return $this->respondPdf($data, $inputs['summary'] ? 'pdf.summaryCashflow' : 'pdf.detailedCashflow', 'cashflow.pdf', $inputs['summary'] ? 'portrait' : 'landscape', $inputs['paper_size']);	
	}

	public function liquidationReportsSpreadsheet()
	{
		$inputs = Input::only([
			'from',
			'to',
			'account_type_id',
			'summary'
			]);

		$inputs['summary'] = (boolean) $summary['inputs'];

		$data = $this->transactionSystemService->getReportData($inputs['from'], $inputs['to'], $inputs['account_type_id'], $inputs['summary'], null);

		$this->respondExcel($data, $inputs['summary'] ? 'spreadsheet.summaryLiquidation' : 'spreadsheet.detailedLiquidation', "Transaction Reports", "Transaction Reports", $data['from'] . ' - ' . $data['to']);


	}


	public function liquidationReportsPdf()
	{
		$inputs = Input::only([
			'from',
			'to',
			'account_type_id',
			'summary',
			'transaction_type',
			'paper_size'
			]);

		$data = $this->transactionSystemService->getReportData($inputs['from'], $inputs['to'], $inputs['account_type_id'], $inputs['summary'], $inputs['transaction_type'], true);

		$data['user'] = Auth::user();

		return $this->respondPdf($data, $inputs['summary'] ? 'pdf.summaryLiquidation' : 'pdf.detailedLiquidation', 'liquidation.pdf', $inputs['summary'] ? 'portrait' : 'landscape', $inputs['paper_size']);
	}


	/**
	 * Voucher of single transaction.
	 *
	 * @param  Transaction $transaction
	 * @return Response
	 */
	public function voucherPdf(Transaction $transaction)
	{
		return $this->respondPdf(['transaction' => $transaction], 'pdf.transactionVoucher', 'voucher.pdf');
	}


	/**
	 * Mass delete of transactions
	 * 
	 * @param  ResultCollection $awards
	 * @return Response
	 */
	public function remove(ResultCollection $transactions)
	{
		$inputs = Input::all();

		$this->transactionSystemService->execute('RemoveTransactions', $inputs, $transactions);

		return $this->respondOk('Transactions deleted');		
	}

	public function printTransactionsPdf(ResultCollection $transactions)
	{
		$income = $transactions->filter(function($transaction)
		{
			return in_array($transaction->transaction_type, ['income', 'transfer-income']);
		})->sum('amount');

		$expense = $transactions->filter(function($transaction)
		{
			return in_array($transaction->transaction_type, ['expense', 'transfer-expense']);
		})->sum('amount');

		$total = [
		'expense' => $expense,
		'income' => $income,
		'total' => $expense - $income
		];

		return $this->respondPdf(['data' => $transactions, 'total' => $total], 'pdf.transactions', 'voucher.pdf', 'portrait');
	}
}
