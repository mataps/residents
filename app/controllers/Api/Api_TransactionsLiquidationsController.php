<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\TransactionSystem\TransactionSystemInterface;
use SourceScript\Common\Collections\ResultCollection;

class Api_TransactionsLiquidationsController extends ApiController {


	/**
	 * @var TransactionSystemInterface
	 */
	private $transactionService;


	/**
	 * @var TransactionLiquidationTransformer
	 */
	private $transactionLiquidationTransformer;


	function __construct(
		TransactionSystemInterface $transactionSystemInterface,
		TransactionLiquidationTransformer $TransactionLiquidationTransformer)
	{
		$this->transactionService = $transactionSystemInterface;
		$this->transactionLiquidationTransformer = $TransactionLiquidationTransformer;
	}


	/**
	 * Returns a listing of resource.
	 * 
	 * @param  Transaction $transaction
	 * @return Response
	 */
	public function index(Transaction $transaction)
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$liquidations = $this->transactionService->findLiquidationsByTransaction($transaction, $fields, $pagination, $sort, $filters);

		return $this->respond($this->transactionLiquidationTransformer->transformCollection($liquidations, $fields, $pagination, $sort));
	}


	/**
	 * Store a newly created resource in storage.
	 * 
	 * @param  Transaction $transaction
	 * @return Response
	 */
	public function store(Transaction $transaction)
	{
		$inputs = Input::only([
			'label',
			'description',
			'amount'
			]);

		$creator = Auth::user();

		$liquidation = $this->transactionService->execute('AddLiquidation', $inputs, $transaction, $creator);

		return $this->respondData($this->transactionLiquidationTransformer->transform($liquidation));
	}


	/**
	 * Update the specified resource in storage.
	 * 
	 * @param  Transaction $transaction
	 * @param  Liquidation $liquidation
	 * @return Response
	 */
	public function update(Transaction $transaction, Liquidation $liquidation)
	{
		$inputs = Input::only([
			'label',
			'description',
			'amount'
			]);

		$updater = Auth::user();

		$liquidation = $this->transactionService->execute('UpdateLiquidation', $inputs, $transaction, $liquidation, $updater);

		return $this->respondData($this->transactionLiquidationTransformer->transform($liquidation));
	}


	/**
	 * @param  Transaction $transaction
	 * @param  Liquidation $liquidation
	 * @return Response
	 */
	public function destroy(Transaction $transaction, Liquidation $liquidation)
	{
		$inputs = Input::all();

		$this->transactionService->execute('RemoveLiquidation', $inputs, $liquidation);

		return $this->respondOk('Liquidation removed');
	}


	/**
	 * @param  Transaction $transaction
	 * @param  Liquidation $liquidation
	 * @return Response
	 */
	public function massDestroy(Transaction $transaction, ResultCollection $liquidations)
	{
		$inputs = Input::all();

		$this->transactionService->execute('RemoveLiquidations', $inputs, $liquidations);

		return $this->respondOk('Liquidations removed');
	}
}