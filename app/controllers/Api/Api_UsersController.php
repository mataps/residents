<?php

use Illuminate\Support\Collection;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\UserManagement\Queries\GetUsersQuery;
use SourceScript\UserManagement\UserManagementInterface;

class Api_UsersController extends ApiController {

    /**
     * @var UserManagementInterface
     */
    private $userService;
    /**
     * @var UserTransformer
     */
    private $userTransformer;

    function __construct(UserManagementInterface $userService, UserTransformer $userTransformer)
    {
        $this->userService = $userService;
        $this->userTransformer = $userTransformer;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $filters = new FilterParameters(Input::get('filters'), GetUsersQuery::getAllowedFilters());

        $pagination = new PaginationParameters(Input::get('page'), Input::get('limit'));

        $users = $this->userService->findUsers($filters, $fields, $pagination, $sort);

        return $this->respond($this->userTransformer->transformCollection($users, $fields, $pagination, $sort));
    }


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
        $inputs = Input::only([
            'username',
            'password',
            'password_confirmation',
            'permissions',
            'group_id',
            'resident_id'
        ]);

        $user = $this->userService->execute('AddUser', $inputs);

        return $this->respondData($this->userTransformer->transform($user));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  User  $user
	 * @return Response
	 */
	public function show(User $user)
	{
		return $this->userTransformer->transform($user);
	}

    public function account()
    {
        $user = Auth::user();

        return $this->userTransformer->transform($user);
    }


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  User  $user
	 * @return Response
	 */
	public function update(User $user)
	{
        $inputs = Input::all();

        $user = $this->userService->execute('UpdateUser', $inputs, $user);

        return $this->respondData($this->userTransformer->transform($user));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  User  $user
	 * @return Response
	 */
	public function destroy(User $user)
	{
        $inputs = Input::all();

        $this->userService->execute('RemoveUser', $inputs, $user);

        return $this->respondOk('User deleted');
	}


    public function deactivated()
    {
        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $filters = new FilterParameters(Input::get('filters'), GetUsersQuery::getAllowedFilters());

        $pagination = new PaginationParameters(Input::get('page'), Input::get('limit'));

        $users = User::onlyTrashed()->get();

        return $this->respond($this->userTransformer->transformCollection($users, $fields, $pagination, $sort));
    }

    public function deactivatedUserShow($id)
    {
        $user = User::onlyTrashed()->where('id', $id)->first();

        return $this->userTransformer->transform($user);
    }

    public function reactivate($id)
    {
        $user = User::onlyTrashed()->where('id', $id)->restore();
        return $this->userTransformer->transform($user);
    }

    function removeUsers(Collection $users)
    {
        $inputs = Input::all();

        $this->userService->execute('RemoveUsers', $inputs, $users);

        return $this->respondOk('Users deleted');
    }

    function getMergedPermissions(User $user)
    {
        $user = $this->userService->getMergedPermissions($user);

        return $this->respondData($this->userTransformer->transform($user));
    }

    /**
     * Change password of the logged in account
     * 
     * @return Response
     */
    public function changePassword()
    {
        $inputs = Input::only([
            'old_password',
            'password',
            'password_confirmation'
            ]);

        $user = Auth::user();

        $this->userService->execute('ChangePassword', $inputs, $user);

        return $this->respondData('Password changed');
    }
}
