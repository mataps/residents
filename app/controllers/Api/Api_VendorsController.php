<?php

use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Collections\ResultCollection;
use SourceScript\TransactionSystem\TransactionSystemInterface;

class Api_VendorsController extends ApiController {

	/**
	 * @var TransactionSystemInterface
	 */
	private $transactionSystemService;

	/**
	 * @var VendorTransformer
	 */
	private $vendorTransformer;

	function __construct(
		TransactionSystemInterface $transactionSystemService,
		VendorTransformer $vendorTransformer)
	{
		$this->transactionSystemService = $transactionSystemService;
		$this->vendorTransformer = $vendorTransformer;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

        $vendors = $this->transactionSystemService->findVendors($fields, $pagination, $sort, $filters);

        return $this->respond($this->vendorTransformer->transformCollection($vendors, $fields, $pagination, $sort));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$inputs = Input::only([
			'name',
			'description',
			'district',
            'cityMunicipality',
            'barangay',
            'street'
			]);

		$creator = Auth::user();

		$vendor = $this->transactionSystemService->execute('AddVendor', $inputs, $creator);

		return $this->respondData($this->vendorTransformer->transform($vendor));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show(TransactionVendor $vendor)
	{
		return $this->vendorTransformer->transform($vendor);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update(TransactionVendor $vendor)
	{
		$inputs = Input::only([
			'name',
			'description',
			'district',
            'cityMunicipality',
            'barangay',
            'street'
			]);

		$updater = Auth::user();

		$vendor = $this->transactionSystemService->execute('UpdateVendor', $inputs, $vendor, $updater);

		return $this->respondData($this->vendorTransformer->transform($vendor));
	}


	/**
	 * @param  ResultCollection $vendors [description]
	 * @return [type]                    [description]
	 */
	public function massDelete(ResultCollection $vendors)
	{
		$inputs = Input::all();

		$this->transactionSystemService->execute('RemoveVendors', $inputs, $vendors);

		return $this->respondOk('Vendors deleted');
	}


}
