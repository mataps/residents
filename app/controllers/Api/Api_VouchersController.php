<?php

use SourceScript\Common\Paginators\FilterableTrait;
use SourceScript\Common\Paginators\PaginatableTrait;
use SourceScript\Common\Paginators\SortableTrait;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Common\Repositories\GenericFinderInterface;
use SourceScript\TransactionSystem\TransactionSystemInterface;

class Api_VouchersController extends ApiController {

	/**
     * @var TransactionSystemInterface
     */
    private $transactionSystemService;

    /**
     * @var VoucherTransformer
     */
    private $voucherTransformer;

    function __construct(TransactionSystemInterface $transactionSystemService, VoucherTransformer $voucherTransformer)
    {
        $this->transactionSystemService = $transactionSystemService;
        $this->voucherTransformer = $voucherTransformer;
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$vouchers = $this->transactionSystemService->findVouchers($fields, $pagination, $sort, $filters);

		return $this->respond($this->voucherTransformer->transformCollection($vouchers, $fields, $pagination, $sort));
    }

	public function show(Voucher $voucher)
	{
		return $this->voucherTransformer->transform($voucher);
	}

	public function store()
	{
		$inputs = Input::only([
			'client_id',
			'client_type',
			'account_type_id',
			'status',
			'remarks',
			'details'
			]);

		$creator = Auth::user();

		$this->transactionSystemService->execute('AddVoucher', $inputs, $creator);
	}

	public function destroy()
	{

	}

	public function settle(Voucher $voucher)
	{
		$inputs = Input::all();

		$user = Auth::user();

		$this->transactionSystemService->execute('SettleVoucher', $inputs, $voucher, $user);
		$this->transactionSystemService->execute('IndexVoucher', $inputs, $voucher);
	}

	/**
	 * Print voucher
	 * 
	 * @param  Voucher $voucher
	 * @return PDF
	 */
	public function voucherPdf(Voucher $voucher)
	{
		$incomeTotal = $voucher->incomes->sum('amount');

		$expenseTotal = $voucher->expenses->sum('amount');

		$total = [
			'expense' => $expenseTotal,
			'income' => $incomeTotal,
			'total' => $expenseTotal - $incomeTotal
		];

		$pdf = PDF::loadView('pdf.voucher', ['data' => $voucher, 'total' => $total])->setOrientation('portrait');

		return $pdf->stream('voucher.pdf');
	}


	/**
	 * For testing purposes
	 * 
	 * @param  Voucher $voucher
	 * @return View
	 */
	public function voucherHtml(Voucher $voucher)
	{
		$incomeTotal = $voucher->incomes->sum('amount');

		$expenseTotal = $voucher->expenses->sum('amount');

		$total = [
			'expense' => $expenseTotal,
			'income' => $incomeTotal,
			'total' => $expenseTotal - $incomeTotal
		];
		return View::make('pdf.voucher', ['data' => $voucher, 'total' => $total]);

	}

	public function individualVoucherPdf(Voucher $voucher)
	{
		$incomeTotal = $voucher->incomes->sum('amount');

		$expenseTotal = $voucher->expenses->sum('amount');

		$total = [
			'expense' => $expenseTotal,
			'income' => $incomeTotal,
			'total' => $expenseTotal - $incomeTotal
		];

		$pdf = PDF::loadView('pdf.individualVoucher', ['data' => $voucher]);
		
		return $pdf->stream('voucher.pdf');	
	}
}
