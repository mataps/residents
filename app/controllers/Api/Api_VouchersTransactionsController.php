<?php

use SourceScript\Common\Paginators\FilterableTrait;
use SourceScript\Common\Paginators\PaginatableTrait;
use SourceScript\Common\Paginators\SortableTrait;
use SourceScript\Common\Parameters\FieldsParameters;
use SourceScript\Common\Parameters\FilterParameters;
use SourceScript\Common\Parameters\PaginationParameters;
use SourceScript\Common\Parameters\SortParameters;
use SourceScript\Common\Repositories\GenericFinderInterface;
use SourceScript\TransactionSystem\TransactionSystemInterface;

class Api_VouchersTransactionsController extends ApiController {

	/**
     * @var TransactionSystemInterface
     */
    private $transactionSystemService;
    /**
     * @var TransactionTransformer
     */
    private $transactionTransformer;

    function __construct(TransactionSystemInterface $transactionSystemService, VoucherTransactionTransformer $transactionTransformer)
    {
        $this->transactionSystemService = $transactionSystemService;
        $this->voucherTransactionTransformer = $transactionTransformer;
    }


	public function index(Voucher $voucher)
	{
		$inputs = Input::all();

        $sort = new SortParameters(Input::get('sort'));

        $fields = new FieldsParameters(Input::get('fields'));

        $pagination = PaginationParameters::createFromArray($inputs);

        $filters = new FilterParameters(Input::get('filters'));

		$transactions = $this->transactionSystemService->findTransactionsByVoucher($voucher, $fields, $pagination, $sort, $filters);

		return $this->respond($this->voucherTransactionTransformer->transformCollection($transactions, $fields, $pagination, $sort));
	}

	public function store()
	{

	}
}