<?php

use SourceScript\Profiling\ProfilingInterface;
use SourceScript\UserManagement\UserManagementInterface;

class MainController extends Controller
{
    /**
     * @var UserManagementInterface
     */
    private $userService;
    /**
     * @var UserTransformer
     */
    private $userTransformer;
    /**
     * @var PermissionTransformer
     */
    private $permissionTransformer;
    /**
     * @var GroupTransformer
     */
    private $groupTransformer;

    /**
     *
     * @param UserManagementInterface $userService
     * @param ProfilingInterface $profilingService
     * @param UserTransformer $userTransformer
     * @param PermissionTransformer $permissionTransformer
     * @param GroupTransformer $groupTransformer
     */
    public function __construct(
        UserManagementInterface $userService,
        ProfilingInterface $profilingService,
        UserTransformer $userTransformer,
        PermissionTransformer $permissionTransformer,
        GroupTransformer $groupTransformer
    )
	{
        $this->userService = $userService;
        $this->profilingService = $profilingService;
        $this->userTransformer = $userTransformer;

        $this->beforeFilter('auth', array('only'=> array('showHomepage')));
        $this->permissionTransformer = $permissionTransformer;
        $this->groupTransformer = $groupTransformer;
    }

	public function showHomepage()
	{
        $user = $this->userTransformer->transform( $this->userService->getMergedPermissions(Auth::user()) );
        $permissions = $this->permissionTransformer->transformCollection( $this->userService->findPermissions() );
        $groups = $this->groupTransformer->transformCollection( $this->userService->findGroups() );
        $version = [Config::get('app.version')];
        $build = [Config::get('app.build')];
        $districts = $this->profilingService->findDistricts();
        $citiesMunicipalities = $this->profilingService->findCitiesMunicipalities();
        $barangays = $this->profilingService->findBarangays();

        $affiliations = $this->profilingService->findAffiliations();
        $attributes = $this->profilingService->findAttributes();

        $constants = compact('user', 'permissions', 'groups', 'version', 'build');
        // $values = compact('districts', 'citiesMunicipalities', 'barangays', 'affiliations', 'attributes');

        return View::make('index', compact('constants'));
    }

	public function showLogin()
	{
		return View::make('login');
	}

    public function postLogin()
    {
        $inputs = Input::only('username', 'password');
        
        try
        {
            $user = $this->userService->findUserByCredentials($inputs);
        }
        catch(SourceScript\UserManagement\Exceptions\InvalidCredentialsException $exception)
        {
            Session::flash('error', $exception->getMessage());
            return Redirect::back();
        }

        if($user->group && $user->group->available_start != null){
            $current_time = date("H:i:s");
            $available_start = date("H:i:s", strtotime($user->group->available_start));
            $available_end = date("H:i:s", strtotime($user->group->available_end));
            if ($current_time < $available_start || $available_end < $$current_time)
            {
              Session::flash('error', 'Your Acount belongs to a Group that is not allowed to access at this moment.');
              return Redirect::back();              
            }
        }

        if($user->available_start != null){
            $current_time = date("H:i:s");
            $available_start = date("H:i:s", strtotime($user->available_start));
            $available_end = date("H:i:s", strtotime($user->available_end));
            if ($current_time < $available_start || $available_end < $$current_time)
            {
              Session::flash('error', 'Your Acount is not allowed to access at this moment.');
              return Redirect::back();              
            }
        }

        Auth::login($user);

        return Redirect::to('/');
    }

    function logout()
    {
        Session::flush();

        return Redirect::to('login');
    }
}