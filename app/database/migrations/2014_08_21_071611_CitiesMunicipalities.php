<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CitiesMunicipalities extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cities_municipalities', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100);
			$table->integer('district_id')->unsigned()->index()->nullable();
			$table->integer('population')->unsigned()->nullable();
			$table->timestamps();

			$table->foreign('district_id')->references('id')->on('districts');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('cities_municipalities');
	}

}
