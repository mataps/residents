<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Barangays extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('barangays', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100);
			$table->integer('city_municipality_id')->unsigned()->index()->nullable();
			$table->integer('population')->unsigned()->nullable();
			$table->timestamps();

			$table->foreign('city_municipality_id')->references('id')->on('barangays');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('barangays');
	}

}
