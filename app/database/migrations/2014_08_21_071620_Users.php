<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Users extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('resident_id')->unsigned()->nullable()->index();
			$table->integer('group_id')->unsigned()->index();
			$table->string('username', 100)->unique();
			$table->string('password', 100);
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('group_id')->references('id')->on('groups');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('users');
	}
}
