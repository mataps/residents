<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Residents extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('residents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('voters_id', 255)->nullable();
			$table->string('last_name', 100);
			$table->string('first_name', 100);
			$table->string('middle_name', 100)->nullable();
			$table->string('full_name', 200);
			$table->string('email', 50)->nullable();
			$table->string('mobile', 20)->nullable();
			$table->string('phone', 20)->nullable();
			$table->string('precinct', 20)->nullable();
			$table->date('birthdate')->nullable();
			$table->string('gender', 10)->nullable();
			$table->string('civil_status', 10)->nullable();
            $table->integer('district_id')->unsigned()->nullable()->index();
            $table->integer('city_municipality_id')->unsigned()->nullable()->index();
            $table->integer('barangay_id')->unsigned()->nullable()->index();
            $table->string('street', 120)->nullable();
            $table->integer('area_code')->unsigned()->nullable()->index();
			$table->string('signature_path', 255)->nullable();
			$table->integer('photo_id')->unsigned()->nullable()->index();
            $table->integer('created_by')->unsigned()->nullable()->index();
            $table->integer('modified_by')->unsigned()->nullable()->index();

			$table->timestamps();
			$table->softDeletes();

            $table->foreign('district_id')->references('id')->on('districts');
            $table->foreign('city_municipality_id')->references('id')->on('cities_municipalities');
            $table->foreign('barangay_id')->references('id')->on('barangays');
            $table->foreign('photo_id')->references('id')->on('photos');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('modified_by')->references('id')->on('users');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('residents');
	}

}
