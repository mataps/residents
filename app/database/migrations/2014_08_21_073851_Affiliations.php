<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Affiliations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('affiliations', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name', 100);
            $table->date('founded')->nullable();
			$table->text('description')->nullable();
			$table->timestamps();
            $table->softDeletes();

            $table->integer('created_by')->unsigned()->index();
            $table->integer('modified_by')->unsigned()->index();

            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('modified_by')->references('id')->on('users');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('affiliations');
	}

}