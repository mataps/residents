<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResidentsAffiliations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('residents_affiliations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('affiliation_id')->unsigned()->index();
			$table->integer('resident_id')->unsigned()->index();
			$table->text('position', 100)->nullable();
			$table->text('status', 100)->nullable();
			$table->text('remarks', 250)->nullable();
			$table->boolean('founder')->default(false);
			$table->date('from')->nullable();
			$table->date('to')->nullable();
			$table->timestamps();

            $table->foreign('affiliation_id')->references('id')->on('affiliations');
            $table->foreign('resident_id')->references('id')->on('residents');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('residents_affiliations');
	}

}
