<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Households extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('households', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('label', 100);
			$table->string('phone', 20);
//            $table->integer('address_id')->unsigned()->nullable()->index();
            $table->integer('created_by')->unsigned()->index();
            $table->integer('modified_by')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();

//            $table->foreign('address_id')->references('id')->on('addresses');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('modified_by')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('households');
	}

}
