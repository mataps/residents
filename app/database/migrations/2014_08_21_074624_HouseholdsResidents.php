<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HouseholdsResidents extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('households_residents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('household_id')->unsigned()->index();
			$table->integer('resident_id')->unsigned()->index();
			$table->string('role', 100);
			$table->date('from')->nullable();
			$table->date('to')->nullable();

			$table->integer('created_by')->unsigned()->index();
            $table->integer('modified_by')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();

            $table->foreign('household_id')->references('id')->on('households');
            $table->foreign('resident_id')->references('id')->on('residents');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('modified_by')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('households_residents');
	}

}
