<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Scholarships extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('scholarships', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('uuid')->nullable();
            $table->integer('resident_id')->unsigned()->index();
            $table->integer('scholarship_type_id')->unsigned()->index();
            $table->integer('school_id')->unsigned()->index();
			$table->text('comment')->nullable();
            $table->date('from')->nullable();
            $table->date('to')->nullable();
            $table->enum('status', ['waiting', 'for_verification', 'active', 'closed', 'denied', 'stop', 'graduated', 'inactive']);
            $table->integer('referred_by')->nullable()->default(null)->index()->unsigned();
			$table->integer('transfered_to')->nullable()->default(null)->index()->unsigned();
			$table->date('transfered_at')->nullable()->default(null);
			$table->date('applied_at')->nullable()->default(null);
			$table->date('approved_at')->nullable()->default(null);
			
			
			$table->integer('transfered_by')->unsigned()->index()->nullable()->default(null);
			$table->integer('denied_by')->unsigned()->index()->nullable()->default(null);
			$table->integer('approved_by')->unsigned()->index()->nullable()->default(null);

			$table->integer('created_by')->unsigned()->index();
            $table->integer('modified_by')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();


			// $table->foreign('scholarship_type_id')->references('id')->on('scholarship_types');
			$table->foreign('denied_by')->references('id')->on('users');
			$table->foreign('approved_by')->references('id')->on('users');
			$table->foreign('referred_by')->references('id')->on('residents');
            $table->foreign('resident_id')->references('id')->on('residents');
            $table->foreign('transfered_to')->references('id')->on('residents');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('transfered_by')->references('id')->on('users');
            $table->foreign('modified_by')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('scholarships');
	}
}
