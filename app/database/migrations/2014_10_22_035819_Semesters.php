<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Semesters extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('semesters', function($table)
		{
			$table->increments('id');
			$table->integer('scholarship_id')->unsigned()->index();
			$table->integer('school_id')->unsigned()->index();
			$table->date('from')->nullable();
			$table->date('to')->nullable();

			/**
			 * TODO
			 */
			$table->string('school_year')->nullable();
			$table->integer('term')->nullable();

			$table->decimal('gwa')->nullable();
			$table->text('comment')->nullable();

			$table->integer('created_by')->unsigned()->index();
            $table->integer('modified_by')->unsigned()->index();
            $table->timestamps();
            $table->softDeletes();

            // $table->foreign('scholarship_id')->references('id')->on('residents');
            // $table->foreign('school_id')->references('id')->on('schools');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('modified_by')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('semesters');
	}

}
