<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Schools extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('schools', function(Blueprint $table)
		{
			$table->increments('id');
			// $table->integer('address_id')->nullable()->unsigned()->index();
			$table->text('address');
			$table->string('name');
			$table->enum('type', ['private', 'public']);
			$table->enum('level', ['secondary', 'collegiate']);
			$table->string('phone', 20)->nullable();

			$table->integer('created_by')->unsigned()->index();
            $table->integer('modified_by')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('created_by')->references('id')->on('users');
            $table->foreign('modified_by')->references('id')->on('users');
			// $table->foreign('address_id')->references('id')->on('addresses');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('schools');
	}

}
