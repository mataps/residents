<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Grades extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('grades', function(Blueprint $table) 
		{
			$table->increments('id');
			$table->integer('subject_id')->unsigned()->index();
			$table->integer('semester_id')->insigned()->index();
			$table->decimal('grade');
			$table->integer('units')->nullable();

			$table->integer('created_by')->unsigned()->index();
            $table->integer('modified_by')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();

			// $table->foreign('semester_id')->references('id')->on('semesters');
			// $table->foreign('subject_id')->references('id')->on('subjects');
			$table->foreign('created_by')->references('id')->on('users');
            $table->foreign('modified_by')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('grades');
	}

}
