<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AwardScholarship extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('award_scholarship', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('award_id')->unsigned()->index();
			$table->integer('scholarship_id')->unsigned()->index();
			$table->integer('semester_id')->unsigned()->index();

			$table->integer('created_by')->unsigned()->index();
            $table->integer('modified_by')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('award_scholarship');
	}

}
