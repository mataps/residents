<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Allowances extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('allowances', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('policy_id');
			$table->integer('semester_id')->unsigned()->index();
			$table->enum('stage',['for_claiming', 'claimed'])->default('for_claiming');


			$table->integer('created_by')->unsigned()->index();
            $table->integer('modified_by')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();

			// $table->foreign('policy_id')->references('id')->on('allowance_policies');
			$table->foreign('semester_id')->references('id')->on('semesters');
			$table->foreign('created_by')->references('id')->on('users');
            $table->foreign('modified_by')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('allowances');
	}

}