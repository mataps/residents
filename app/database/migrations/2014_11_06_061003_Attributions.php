<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Attributions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attributions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('label');
			$table->string('class')->nullable();

			$table->integer('created_by')->unsigned()->index();
            $table->integer('modified_by')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('created_by')->references('id')->on('users');
            $table->foreign('modified_by')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attributions');
	}

}
