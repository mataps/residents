<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResidentsAttributes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('residents_attributes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('resident_id');
			$table->string('attribute_id');
			$table->date('from');
			$table->date('to');
			$table->timestamps();

//			$table->foreign('resident_id')->references('id')->on('residents');
//			$table->foreign('attribute_id')->references('id')->on('attributes');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('residents_attributes');
	}

}
