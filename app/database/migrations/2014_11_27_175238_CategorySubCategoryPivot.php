<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CategorySubCategoryPivot extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('category_sub_categories', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('category_id')->unsigned()->index();
			$table->integer('sub_category_id')->unsigned()->index();
			$table->timestamps();

			$table->integer('created_by')->unsigned()->index();
			$table->integer('modified_by')->unsigned()->index();

			$table->foreign('created_by')->references('id')->on('users');
	        $table->foreign('modified_by')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('category_sub_categories');
	}

}
