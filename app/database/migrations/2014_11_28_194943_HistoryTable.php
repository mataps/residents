<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HistoryTable extends Migration {

	/**
	 * Tables
	 * 
	 * @var array
	 */
	private $tables = [
		'accounting_transactions',
		'addresses',
		'affiliations',
		'allowances',
		'allowance_policies',
		'attributions',
		'awards',
		'award_scholarship',
		'barangays',
		'cities_municipalities',
		'districts',
		'grades',
		'groups',
		'groups_permissions',
		'households',
		'lastnames',
		'permissions',
		'photos',
		'referral_transactions',
		'residents',
		'residents_affiliations',
		'residents_attributes',
		'scholarships',
		'schools',
		'subjects',
		'transactions',
		'transaction_accounts',
		'transaction_categories',
		'transaction_items',
		'users',
		'users_permissions'
		];

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// foreach($this->tables as $tablename)
		// {
		// 	$columns = DB::select(DB::raw('describe ' . $tablename));


		// 	Schema::create('history_' . $tablename, function(Blueprint $table) use ($columns)
		// 	{
		// 		$table->increments('id');
		// 		$table->integer('resource_id')->index()->unsigned();

		// 		foreach($columns as $column)
		// 		{
		// 			$type = explode('(', $column->Type)[0];
		// 			preg_match('#\((.*?)\)#', $column->Type, $length);
		// 			// $unsigned = (explode(' ', $column->Type)[1] == 'unsigned') ? true : false;
		// 			if( ! in_array($column->Field, ['id', 'created_at', 'updated_at']))
		// 			{
		// 				switch ($type) {
		// 					case 'int':
		// 						$table->integer($column->Field);
		// 						break;

		// 					case 'varchar':
		// 						$table->string($column->Field, $length[1])->default($column->Default);
		// 						break;

		// 					case 'decimal':
		// 						$table->decimal($column->Field);
		// 						break;

		// 					case 'tinyint':
		// 						$table->tinyInteger($column->Field)->nullable();
		// 						break;

		// 					case 'text':
		// 						$table->text($column->Field)->default($column->Default);
		// 						break;

		// 					case 'enum':
		// 						preg_match('#\((.*?)\)#', $column->Type, $insideParenthesis);

		// 						$replaced = str_replace("'", "", $insideParenthesis[1]);

		// 						$valuesArray = explode(',', $replaced);

		// 						$table->enum($column->Field, $valuesArray)->default($column->Default);
		// 						break;

		// 					case 'date':
		// 						$table->date($column->Field)->nullable();

		// 						break;
		// 					default:
		// 						# code...
		// 						break;
		// 				}
		// 			}
		// 		}

		// 		$table->timestamps();
		// 		$table->softDeletes();
		// 	});
		// }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		// foreach($this->tables as $table)
		// {
		// 	Schema::dropIfExists('history_' . $table);
		// }
	}

}
