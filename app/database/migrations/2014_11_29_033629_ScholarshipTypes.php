<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ScholarshipTypes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('scholarship_types', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->text('description');

			$table->integer('created_by')->unsigned()->index();
			$table->integer('modified_by')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('created_by')->references('id')->on('users');
			$table->foreign('modified_by')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('scholarship_types');
	}

}
