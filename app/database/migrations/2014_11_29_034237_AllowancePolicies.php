<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllowancePolicies extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('allowance_policies', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('scholarship_type_id')->unsigned()->index();

			$table->enum('school_type', ['public', 'private']);
			$table->enum('school_level', ['secondary', 'collegiate']);

			$table->decimal('from');
			$table->decimal('to');

			$table->decimal('amount');

			$table->integer('created_by')->unsigned()->index();
            $table->integer('modified_by')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('created_by')->references('id')->on('users');
            $table->foreign('modified_by')->references('id')->on('users');
			$table->foreign('scholarship_type_id')->references('id')->on('scholarship_types');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('allowance_policies');
	}

}
