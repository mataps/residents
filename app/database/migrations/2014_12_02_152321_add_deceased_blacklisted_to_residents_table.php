<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeceasedBlacklistedToResidentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('residents', function(Blueprint $table)
		{
			$table->timestamp('blacklisted_at')->after('photo_id')->nullable();
			$table->timestamp('deceased_at')->after('photo_id')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('residents', function(Blueprint $table)
		{
			$table->dropColumn('blacklisted_at');
			$table->dropColumn('deceased_at');
		});
	}

}
