<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Vouchers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('vouchers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('uuid')->nullable();
			$table->integer('client_id')->unsigned()->index();
			$table->string('client_type');
			$table->integer('account_type_id')->unsigned()->index();

			$table->enum('status', ['floating', 'rejected', 'approved']);
			$table->text('remarks')->nullable();
			$table->text('details')->nullable();

			$table->dateTime('settled_at')->nullable()->default(null);
			$table->integer('created_by')->unsigned()->index();
			$table->integer('modified_by')->unsigned()->index();

			$table->timestamps();
			$table->softDeletes();

			$table->foreign('created_by')->references('id')->on('users');
			$table->foreign('modified_by')->references('id')->on('users');
			$table->foreign('account_type_id')->references('id')->on('transaction_accounts');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('vouchers');
	}

}
