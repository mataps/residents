<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Transactions extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transactions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('uuid')->nullable();
			$table->integer('voucher_id')->unsigned()->index();
			
			$table->integer('referrer_id')->unsigned()->index();

			$table->integer('client_id')->unsigned()->index();
			$table->string('client_type');

			$table->integer('beneficiary_id')->unsigned()->index();
			$table->string('beneficiary_type');
			$table->integer('sub_category_id')->unsigned()->index();
			$table->integer('item_id')->unsigned()->index();
			$table->enum('transaction_type', ['expense', 'income', 'non-monetary']);
			$table->string('name')->nullable();
			$table->text('details')->nullable();
			$table->text('remarks')->nullable();
			$table->decimal('amount', 10, 2);
            $table->integer('created_by')->unsigned()->index();
            $table->integer('modified_by')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();


			$table->foreign('voucher_id')->references('id')->on('vouchers');
			$table->foreign('referrer_id')->references('id')->on('residents');
			$table->foreign('sub_category_id')->references('id')->on('transaction_sub_categories');
	        $table->foreign('created_by')->references('id')->on('users');
	        $table->foreign('modified_by')->references('id')->on('users');
	
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transactions');
	}

}
