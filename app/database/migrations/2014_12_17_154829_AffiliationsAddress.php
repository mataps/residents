<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AffiliationsAddress extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('affiliations', function(Blueprint $table)
		{
			$table->integer('district_id')->index()->unsigned()->after('description');
			$table->integer('city_municipality_id')->index()->unsigned()->after('district_id');
			$table->integer('barangay_id')->index()->unsigned()->after('city_municipality_id');
			$table->string('street')->after('barangay_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('affiliations', function(Blueprint $table)
		{
			$table->dropColumn(['district_id', 'city_municipality_id', 'barangay_id', 'street']);
		});
	}

}
