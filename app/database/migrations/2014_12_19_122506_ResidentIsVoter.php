<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResidentIsVoter extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('residents', function(Blueprint $table)
		{
			$table->boolean('is_voter')->default(false)->after('voters_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('residents', function(Blueprint $table)
		{
			$table->dropColumn('is_voter');
		});
	}

}
