<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HouseholdAreCodes extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('households', function(Blueprint $table)
		{
			$table->string('area_code')->nullable()->after('street');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('households', function(Blueprint $table)
		{
			$table->dropColumn('area_code');
		});
	}

}
