<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllowanceReleasesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('allowance_releases', function(Blueprint $table)
		{
			$table->increments('id');

			$table->integer('term')->nullable();
			$table->string('school_year')->nullable();

			$table->integer('created_by')->index()->unsigned();
			$table->integer('modified_by')->index()->unsigned();
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('created_by')->references('id')->on('users');
			$table->foreign('modified_by')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('allowance_releases');
	}

}
