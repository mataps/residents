<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllowanceReleaseType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('allowance_releases', function(Blueprint $table)
		{
			$table->boolean('fixed_amount')->default(false)->after('school_year');
			$table->decimal('amount')->nullable()->after('fixed_amount');
			$table->boolean('by_scholarship_type')->default(false)->after('amount');
			$table->integer('scholarship_type_id')->index()->unsigned()->nullable()->after('by_scholarship_type');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('allowance_releases', function(Blueprint $table)
		{
			$table->dropColumn(['fixed_amount', 'amount', 'by_scholarship_type', 'scholarship_type_id']);
		});
	}

}