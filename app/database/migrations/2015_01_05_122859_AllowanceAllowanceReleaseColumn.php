<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllowanceAllowanceReleaseColumn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('allowances', function(Blueprint $table)
		{
			$table->integer('allowance_release_id')->index()->unsigned()->after('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('allowances', function(Blueprint $table)
		{
			$table->dropColumn('allowance_release_id');
		});
	}

}
