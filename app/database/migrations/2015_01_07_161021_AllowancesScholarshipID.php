<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllowancesScholarshipID extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('allowances', function(Blueprint $table)
		{
			$table->integer('scholarship_id')->unsigned()->index()->after('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('allowances', function(Blueprint $table)
		{
			$table->dropColumn('scholarship_id');
		});
	}
}
