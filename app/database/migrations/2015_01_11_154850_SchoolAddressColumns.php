<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SchoolAddressColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('schools', function(Blueprint $table)
		{
			$table->integer('district_id')->index()->unsigned();
			$table->integer('barangay_id')->index()->unsigned();
			$table->integer('city_municipality_id')->index()->unsigned();
			$table->string('street');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('schools', function(Blueprint $table)
		{
			$table->dropColumn(['district_id', 'barangay_id', 'city_municipality_id', 'street']);
		});
	}

}
