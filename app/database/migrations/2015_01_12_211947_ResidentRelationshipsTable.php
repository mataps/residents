<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResidentRelationshipsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('resident_relationships', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('resident_id')->unsigned()->index();
			$table->integer('relative_id')->unsigned()->index();
			$table->string('relation')->nullable(); //parent, sibling

			$table->integer('created_by')->unsigned()->index();
			$table->integer('modified_by')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();

			$table->foreign('created_by')->references('id')->on('users');
			$table->foreign('modified_by')->references('id')->on('users');
			$table->foreign('resident_id')->references('id')->on('residents');
			$table->foreign('relative_id')->references('id')->on('residents');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('resident_relationships');
	}

}
