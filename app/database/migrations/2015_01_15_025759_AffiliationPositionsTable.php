<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AffiliationPositionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('positions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->integer('created_by')->index()->unsigned();
			$table->integer('modified_by')->index()->unsigned();

			$table->timestamps();
			$table->softDeletes();

			$table->foreign('created_by')->references('id')->on('users');
			$table->foreign('modified_by')->references('id')->on('users');
		});

		Schema::create('affiliations_positions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('position_id')->index()->unsigned();
			$table->integer('affiliation_id')->index()->unsigned();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('positions');

		Schema::drop('affiliations_positions');
	}

}
