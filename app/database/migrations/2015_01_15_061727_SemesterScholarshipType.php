<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SemesterScholarshipType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('scholarships', function(Blueprint $table)
		{
			$table->dropColumn('scholarship_type_id');
		});

		Schema::table('semesters', function(Blueprint $table)
		{
			$table->integer('scholarship_type_id')->unsigned()->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('scholarships', function(Blueprint $table)
		{
			$table->integer('scholarship_type_id')->index()->unsigned();
		});

		Schema::table('semesters', function(Blueprint $table)
		{
			$table->dropColumn('scholarship_type_id');
		});
	}
}
