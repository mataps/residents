<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllowanceReleaseScholarshipTypePivot extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('allowance_releases_scholarship_types', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('allowance_release_id')->unsigned()->index();
			$table->integer('scholarship_type_id')->unsigned()->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('allowance_releases_scholarship_types');
	}

}
