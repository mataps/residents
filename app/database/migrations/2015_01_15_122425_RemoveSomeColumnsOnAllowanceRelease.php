<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveSomeColumnsOnAllowanceRelease extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('allowance_releases', function(Blueprint $table)
		{
			$table->dropColumn(['by_scholarship_type', 'scholarship_type_id']);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('allowance_releases', function(Blueprint $table)
		{
			$table->boolean('by_scholarship_type')->default(false)->after('amount');
			$table->integer('scholarship_type_id')->index()->unsigned()->nullable()->after('by_scholarship_type');
		});
	}

}