<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeSettledAtDataType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transactions', function(Blueprint $table)
		{
			$table->dropColumn('settled_at')->nullable();
		});

		Schema::table('transactions', function(Blueprint $table)
		{
			$table->timestamp('settled_at')->after('amount');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transactions', function(Blueprint $table)
		{
			$table->dropColumn('settled_at');
		});

		Schema::table('transactions', function(Blueprint $table)
		{
			$table->date('settled_at')->nullable()->default(null);
		});
	}

}