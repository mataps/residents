<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AttributionAtAffiliationReferrer extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('residents_affiliations', function(Blueprint $table)
		{
			$table->integer('referrer_id')->index()->unsigned()->nullable()->after('id');
		});

		Schema::table('residents_attributes', function(Blueprint $table)
		{
			$table->integer('referrer_id')->index()->unsigned()->nullable()->after('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('residents_affiliations', function(Blueprint $table)
		{
			$table->dropColumn('referrer_id');
		});

		Schema::table('residents_attributes', function(Blueprint $table)
		{
			$table->dropColumn('referrer_id');
		});
	}

}
