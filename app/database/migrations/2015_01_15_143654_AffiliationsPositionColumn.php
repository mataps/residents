<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AffiliationsPositionColumn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('residents_affiliations', function(Blueprint $table)
		{
			$table->dropColumn('position');
		});

		Schema::table('residents_affiliations', function(Blueprint $table)
		{
			$table->integer('position_id')->unsigned()->index()->nullable()->after('resident_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('residents_affiliations', function(Blueprint $table)
		{
			$table->dropColumn('position_id');
		});

		Schema::table('residents_affiliations', function(Blueprint $table) 
		{
			$table->string('position')->after('resident_id');
		});
	}

}
