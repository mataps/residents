<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HeadOnResidentAffiliations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('residents_affiliations', function(Blueprint $table)
		{
			$table->integer('head_id')->after('position_id')->index()->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('residents_affiliations', function(Blueprint $table)
		{
			$table->dropColumn('head_id');
		});
	}

}
