<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllowanceTableAmoutnColumn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('allowances', function(Blueprint $table)
		{
			$table->decimal('amount')->after('semester_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('allowances', function(Blueprint $table)
		{
			$table->dropColumn('amount');
		});
	}

}
