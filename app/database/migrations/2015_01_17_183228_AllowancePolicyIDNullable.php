<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllowancePolicyIDNullable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('allowances', function(Blueprint $table)
		{
			$table->dropColumn('policy_id');
		});

		Schema::table('allowances', function(Blueprint $table)
		{
			$table->integer('policy_id')->unsigned()->index()->nullable()->after('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('allowances', function(Blueprint $table)
		{
			$table->dropColumn('policy_id');
		});

		Schema::table('allowances', function(Blueprint $table)
		{
			$table->integer('policy_id')->after('id');
		});
	}

}
