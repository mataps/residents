<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllowancePolciesOverrides extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('policies_overrides', function(Blueprint $table) 
		{
			$table->increments('id');
			$table->integer('occurence');
			$table->decimal('amount');

			$table->integer('created_by')->unsigned()->index();
			$table->integer('modified_by')->unsigned()->index();

			$table->timestamps();
			$table->softDeletes();

			$table->foreign('created_by')->references('id')->on('users');
			$table->foreign('modified_by')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('policies_overrides');
	}
}
