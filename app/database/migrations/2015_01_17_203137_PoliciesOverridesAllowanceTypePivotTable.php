<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PoliciesOverridesAllowanceTypePivotTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('policies_overrides_scholarship_types', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('policies_override_id')->unsigned()->index();
			$table->integer('scholarship_type_id')->unsigned()->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('policies_overrides_scholarship_types');
	}

}
