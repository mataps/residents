<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LiquidationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('liquidations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('transaction_id')->index()->unsigned();
			$table->string('label');
			$table->text('description');
			$table->decimal('amount');

			$table->integer('created_by')->index()->unsigned();
			$table->integer('modified_by')->index()->unsigned();

			$table->timestamps();
			$table->softDeletes();

			$table->foreign('created_by')->references('id')->on('users');
			$table->foreign('modified_by')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('liquidations');
	}

}
