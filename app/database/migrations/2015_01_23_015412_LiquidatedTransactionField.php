<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LiquidatedTransactionField extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transactions', function(Blueprint $table)
		{
			$table->timestamp('liquidated_at')->nullable()->after('settled_at')->default(null);
			$table->integer('liquidated_by')->unsigned()->index()->nullable()->after('liquidated_at');
		});
	}
 
	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transactions', function(Blueprint $table)
		{
			$table->dropColumn(['liquidated_at', 'liquidated_by']);
		});
	}

}
