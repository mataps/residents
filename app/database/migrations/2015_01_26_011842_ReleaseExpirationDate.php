<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReleaseExpirationDate extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('allowance_releases', function(Blueprint $table)
		{
			$table->timestamp('expiring_at')->after('amount')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *+
	 * @return void
	 */
	public function down()
	{
		Schema::table('allowance_releases', function(Blueprint $table)
		{
			$table->dropColumn('expiring_at');
		});
	}

}
