<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResidentAttributionTableFix extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('residents_attributes', function(Blueprint $table)
		{
			$table->dropColumn(['from', 'to']);
		});

		Schema::table('residents_attributes', function(Blueprint $table)
		{
			$table->date('from')->after('attribute_id')->nullable();
			$table->date('to')->after('from')->nullable();
			$table->integer('created_by')->index()->unsigned();
			$table->integer('modified_by')->index()->unsigned();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('residents_attributes', function(Blueprint $table)
		{
			$table->dropColumn(['from', 'to', 'created_by', 'modified_by']);
		});

		Schema::table('residents_attributes', function(Blueprint $table)
		{
			$table->date('from')->after('attribute_id');
			$table->date('to')->after('from');
		});
	}

}
