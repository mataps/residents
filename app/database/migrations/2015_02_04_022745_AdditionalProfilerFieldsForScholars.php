<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdditionalProfilerFieldsForScholars extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('residents', function(Blueprint $table)
		{
			$table->string('atm_number')->nullable()->after('phone');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('residents', function(Blueprint $table)
		{
			$table->dropColumn('atm_number');
		});
	}

}
