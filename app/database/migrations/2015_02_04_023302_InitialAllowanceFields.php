<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialAllowanceFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('scholarship_types', function(Blueprint $table)
		{
			$table->decimal('initial_allowance')->nullable()->after('description');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('scholarship_types', function(Blueprint $table)
		{
			$table->dropColumn('initial_allowance');
		});
	}

}
