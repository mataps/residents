<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VendorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transaction_vendors', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('description');
			$table->string('street');
			$table->integer('city_municipality_id')->unsigned()->index();
			$table->integer('district_id')->unsigned()->index();
			$table->integer('barangay_id')->unsigned()->index();

			$table->integer('created_by')->unsigned()->index();
			$table->integer('modified_by')->unsigned()->index();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transaction_vendors');
	}

}
