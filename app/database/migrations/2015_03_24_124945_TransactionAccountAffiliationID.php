<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransactionAccountAffiliationID extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transaction_accounts', function(Blueprint $table)
		{
			$table->integer('affiliation_id')->index()->unsigned()->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transaction_accounts', function(Blueprint $table)
		{
			$table->dropColumn('affiliation_id');
		});
	}

}
