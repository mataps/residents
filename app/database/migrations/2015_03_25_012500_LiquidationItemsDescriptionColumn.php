<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LiquidationItemsDescriptionColumn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('liquidations', function(Blueprint $table)
		{
			$table->dropColumn('description');
		});

		Schema::table('liquidations', function(Blueprint $table)
		{
			$table->text('description')->nullable()->after('label');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('liquidations', function(Blueprint $table)
		{
			Schema::table('liquidations', function(Blueprint $table)
			{
				$table->dropColumn('description');
			});

			Schema::table('liquidations', function(Blueprint $table)
			{
				$table->text('description')->nullable()->after('label');
			});
		});
	}

}
