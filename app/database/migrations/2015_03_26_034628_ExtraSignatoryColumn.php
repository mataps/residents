<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ExtraSignatoryColumn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transaction_accounts', function(Blueprint $table)
		{
			$table->string('label')->after('signatory')->nullable();
			$table->string('designation')->after('label')->nullable();

			$table->string('signatory_2')->after('designation')->nullable();
			$table->string('label_2')->after('signatory_2')->nullable();
			$table->string('designation_2')->after('label_2')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transaction_accounts', function(Blueprint $table)
		{
			$table->dropColumn(['signatory_2', 'label', 'designation', 'label_2', 'designation_2']);
		});
	}

}
