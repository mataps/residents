<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SemesterYearLevelColumn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('semesters', function(Blueprint $table)
		{
			$table->integer('year_level')->nullable()->after('school_year');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('semesters', function(Blueprint $table)
		{
			$table->dropColumn('year_level');
		});
	}

}
