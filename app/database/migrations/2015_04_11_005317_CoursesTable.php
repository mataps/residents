<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CoursesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('courses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('abbreviation');

			$table->integer('created_by')->unsigned();
			$table->integer('modified_by')->unsigned();

			$table->timestamps();
			$table->softDeletes();

			$table->foreign('created_by')->on('users')->references('id');
			$table->foreign('modified_by')->on('users')->references('id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('courses');
	}

}
