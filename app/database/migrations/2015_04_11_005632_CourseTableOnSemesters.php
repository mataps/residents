<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CourseTableOnSemesters extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('semesters', function(Blueprint $table)
		{
			$table->integer('course_id')->unsigned()->nullable()->default(null);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('semesters', function(Blueprint $table)
		{
			$table->dropColumn('course_id');
		});
	}

}
