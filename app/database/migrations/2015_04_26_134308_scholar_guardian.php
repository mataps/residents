<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ScholarGuardian extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('scholarships', function(Blueprint $table)
		{
			$table->string('father_name', 255)->nullable()->default(null);
			$table->string('mother_name', 255)->nullable()->default(null);
			$table->string('guardian_contact_no', 255)->nullable()->default(null);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('scholarships', function(Blueprint $table)
		{
			$table->dropColumn('father_name');
			$table->dropColumn('mother_name');
			$table->dropColumn('guardian_contact_no');
		});
	}

}
