<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReleasesUuidColumn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('allowance_releases', function(Blueprint $table)
		{
			$table->string('uuid')->after('id')->nullable()->unique();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('allowance_releases', function(Blueprint $table)
		{
			$table->dropColumn('uuid');
		});
	}

}
