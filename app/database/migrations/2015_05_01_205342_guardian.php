<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Guardian extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::table('scholarships', function(Blueprint $table)
		{
			$table->string('guardian_name', 255)->nullable()->default(null);
			$table->string('parents_contact_no', 255)->nullable()->default(null);
			$table->string('others_contact_no', 255)->nullable()->default(null);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::table('scholarships', function(Blueprint $table)
		{
			$table->dropColumn('guardian_name');
			$table->dropColumn('parents_contact_no');
			$table->dropColumn('others_contact_no');
		});
	}


}
