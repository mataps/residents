<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransactionsTransferColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transactions', function(Blueprint $table)
		{
			DB::statement("ALTER TABLE transactions MODIFY COLUMN transaction_type ENUM('income','expense','non-monetary', 'transfer-income', 'transfer-expense')");
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transactions', function(Blueprint $table)
		{
			DB::statement("ALTER TABLE transactions MODIFY COLUMN transaction_type ENUM('income','expense','non-monetary')");
		});
	}

}
