<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AwardDescriptionYearColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('award_scholarship', function(Blueprint $table)
		{
			$table->string('year')->nullable()->after('scholarship_id');
			$table->string('remarks')->nullable()->after('year');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('award_scholarship', function(Blueprint $table)
		{
			$table->dropColumn(['year', 'remarks']);
		});
	}

}
