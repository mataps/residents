<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GuardianFatherMotherIds extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('scholarships', function(Blueprint $table)
		{
			$table->integer('father_id')->unsigned()->nullable()->after('id');
			$table->integer('mother_id')->unsigned()->nullable()->after('father_id');
			$table->integer('guardian_id')->unsigned()->nullable()->after('mother_id');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('scholarships', function(Blueprint $table)
		{
			$table->dropColumn(['father_id', 'mother_id', 'guardian_id']);
		});
	}

}
