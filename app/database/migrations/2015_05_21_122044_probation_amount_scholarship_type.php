<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProbationAmountScholarshipType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('scholarship_types', function(Blueprint $table)
		{
			$table->decimal('probation_amount')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('scholarship_types', function(Blueprint $table)
		{
			$table->dropColumn('probation_amount');
		});
	}

}
