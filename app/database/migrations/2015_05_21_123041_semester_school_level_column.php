<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SemesterSchoolLevelColumn extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('semesters', function(Blueprint $table)
		{
			$table->enum('school_level', ['secondary', 'collegiate'])->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('semesters', function(Blueprint $table)
		{
			$table->dropColumn('school_level');
		});
	}

}
