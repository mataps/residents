<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AdditionalMobileFields extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('residents', function(Blueprint $table)
		{
			$table->string('mobile_1')->nullable();
			$table->string('mobile_2')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('residents', function(Blueprint $table)
		{
			$table->dropColumn('mobile_1');
			$table->dropColumn('mobile_2');
		});
	}

}
