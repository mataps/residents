<?php

use Faker\Factory as Faker;

class AffiliationsSeeder extends Seeder{

    protected $faker;

    function __construct()
    {
        $this->faker = Faker::create();
        Affiliation::truncate();
        ResidentAffiliation::truncate();
    }

    function run()
    {
        $affiliations = [
            'Leaders',
            'Toda',
            'Youth Organization',
            'Home owners Association',
            'Senior Citizens'
        ];
        $positions = [
            'officer',
            'leader',
            'member'
        ];

        $residents = Resident::lists('id');
        $districts = District::lists('id');
        $citiesMunicipalities = CityMunicipality::lists('id');
        $barangays = Barangay::lists('id');
        $users = User::lists('id');
        $positions = Position::lists('id');

        foreach($affiliations as $name)
        {
            $affiliation 				= new Affiliation;
            $affiliation->name 			= $name;
            $affiliation->founded       = $this->faker->date();
            $affiliation->description 	= $this->faker->words(10,150);

            $affiliation->district_id             = $this->faker->randomElement($districts);
            $affiliation->city_municipality_id    = $this->faker->randomElement($citiesMunicipalities);
            $affiliation->barangay_id             = $this->faker->randomElement($barangays);
            $affiliation->street                  = $this->faker->streetAddress;

            $affiliation->created_by = $this->faker->randomElement($users);
            $affiliation->modified_by = $this->faker->randomElement($users);

            $affiliation->save();

            $affiliation->positions()->attach(1);

            foreach (range(1, rand(1, 20)) as $i)
            {
                $position = $this->faker->randomElement($positions);

                $affiliation->members()->attach($this->faker->randomElement($residents), [
                    'head_id' => $this->faker->randomElement($residents),
                    'referrer_id' => $this->faker->randomElement($residents),
                    'position_id' => $this->faker->randomElement($positions),
                    'from' => $this->faker->dateTimeThisDecade,
                    'to' => $this->faker->dateTimeThisDecade,
                    'founder' => $this->faker->randomElement([true, false]),
                    'created_by' => $this->faker->randomElement($users),
                    'modified_by' => $this->faker->randomElement($users)
                ]);
            }
        }
    }
} 