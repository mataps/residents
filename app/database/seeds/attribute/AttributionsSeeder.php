<?php

use Faker\Factory as Faker;
class AttributionsSeeder extends Seeder{

    protected $faker;

    function __construct()
    {
        $this->faker = Faker::create();
        Attribution::truncate();
    }

    function run()
    {
        $users = User::lists('id');

        $attributes = [
            [
                'name' => 'blacklisted',
                'label' => 'BL',
                'class' => 'label-default'
            ],
            [
                'name' => 'leader',
                'label' => 'L',
                'class' => 'label-success',
            ],
            [
                'name' => 'party_member',
                'label' => 'PM',
                'class' => 'label-primary'
            ],
            [
                'name' => 'opposition_party',
                'label' => 'OP',
                'class' => 'label-danger'
            ]
        ];

        foreach($attributes as $attrs)
        {
            $attribute = new Attribution;

            $attribute->fill($attrs);
            $attribute->created_by = $this->faker->randomElement($users);
            $attribute->modified_by = $this->faker->randomElement($users);

            $attribute->save();
        }
    }
} 