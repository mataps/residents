<?php

use Faker\Factory as Faker;

class DefaultGroupsSeeder extends Seeder{

    protected $faker;

    function __construct()
    {
        $this->faker = Faker::create();
        Group::truncate();
        GroupPermission::truncate();
    }

    function run()
    {
        $groups = array(
            'Administrators' => array(
                Permission::USER_MANAGEMENT_MANAGE_USERS,
                Permission::USER_MANAGEMENT_VIEW_USERS,
                Permission::USER_MANAGEMENT_MANAGE_GROUPS,
                Permission::USER_MANAGEMENT_VIEW_GROUPS,
                Permission::PROFILING_MANAGE_RESIDENTS,
                Permission::PROFILING_VIEW_RESIDENTS,
                Permission::PROFILING_MANAGE_HOUSEHOLDS,
                Permission::PROFILING_VIEW_HOUSEHOLDS,
                Permission::PROFILING_MANAGE_AFFILIATIONS,
                Permission::PROFILING_VIEW_AFFILIATIONS,
                Permission::TRANSACTION_SYSTEM_MANAGE_TRANSACTIONS,
                Permission::TRANSACTION_SYSTEM_VIEW_TRANSACTIONS,
                Permission::SCHOLARSHIP_MANAGEMENT_MANAGE_SCHOLARSHIPS,
                Permission::SCHOLARSHIP_MANAGEMENT_VIEW_SCHOLARSHIPS,
            ),
            'Profile Encoders' => array(
                Permission::PROFILING_MANAGE_RESIDENTS,
                Permission::PROFILING_VIEW_RESIDENTS,
            ),
            'Scholarship Manager' => array(
                Permission::SCHOLARSHIP_MANAGEMENT_MANAGE_SCHOLARSHIPS,
                Permission::SCHOLARSHIP_MANAGEMENT_VIEW_SCHOLARSHIPS,
            )
        );

        foreach ($groups as $name => $permissions)
        {
            $group = Group::create(['name' => $name]);

            foreach ($permissions as $permission)
            {
                $perm = Permission::where('name', $permission)->first();
                if (!$perm)
                {
                    dd($permission);
                }
                GroupPermission::create([
                    'permission_id' => $perm->id,
                    'group_id' => $group->id,
                    'status' => 1
                ]);
            }
        }
    }
} 