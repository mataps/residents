<?php

class HistorySeeder extends Seeder {

	/**
	 * Tables
	 * 
	 * @var array
	 */
	private $tables = [
		'accounting_transactions',
		'addresses',
		'affiliations',
		'allowances',
		'allowance_policies',
		'attributions',
		'awards',
		'award_scholarship',
		'barangays',
		'cities_municipalities',
		'districts',
		'grades',
		'groups',
		'groups_permissions',
		'households',
		'lastnames',
		'permissions',
		'photos',
		'referral_transactions',
		'residents',
		'residents_affiliations',
		'residents_attributes',
		'scholarships',
		'schools',
		'subjects',
		'transactions',
		'transaction_accounts',
		'transaction_categories',
		'transaction_items',
		'transaction_vendors',
		'users',
		'users_permissions'
		];


	function __construct()
	{
		foreach($this->tables as $table)
		{
			DB::table($table)->truncate();
		}

		History::truncate();
	}

	function run()
	{
		foreach($this->tables as $table)
		{

		}
	}
}