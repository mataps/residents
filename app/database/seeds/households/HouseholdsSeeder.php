<?php

use Faker\Factory as Faker;

class HouseholdsSeeder extends Seeder{

    protected $faker;

    function __construct()
    {
        $this->faker = Faker::create();
        Household::truncate();
        ResidentHousehold::truncate();
    }

    function run()
    {
        $maxRange = 10;
        $users = User::lists('id');
        $districts = District::lists('id');
        $citiesMunicipalities = CityMunicipality::lists('id');
        $barangays = Barangay::lists('id');

        foreach (range(1, $maxRange) as $index)
        {
            $household = new Household;
            $household->label 			= $this->faker->word;
            $household->phone 			= $this->faker->phoneNumber;

            $household->district_id             = $this->faker->randomElement($districts);
            $household->city_municipality_id    = $this->faker->randomElement($citiesMunicipalities);
            $household->barangay_id             = $this->faker->randomElement($barangays);
            $household->street                  = $this->faker->streetAddress;

            $household->created_by 		= $this->faker->randomElement($users);
            $household->modified_by		= $household->created_by;

//            $address = Address::create([
//                'municipality' => $this->faker->state,
//                'barangay' => $this->faker->streetName,
//                'street' => $this->faker->streetAddress,
//                'purok' => $this->faker->streetSuffix
//            ]);
//            $household->address()->associate($address);

            $household->save();

            foreach (range(1, rand(1, 5)) as $i)
            {
                $role = $i == 1 ? 'owner' : 'member';
                $household->residents()->attach($this->faker->randomElement($users), array('role' => $role, 'created_by' => $this->faker->randomElement($users), 'modified_by' => $this->faker->randomElement($users)));
            }

            echo "$index/$maxRange Households seeded\n";
        }
    }
} 