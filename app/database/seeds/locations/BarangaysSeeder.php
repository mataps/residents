<?php

use Faker\Factory as Faker;

class BarangaysSeeder extends Seeder{

    protected $faker;

    function __construct()
    {
        $this->faker = Faker::create();
        Barangay::truncate();
    }

    function run()
    {
        $maxRange = 10;
        $cities_municipalities = CityMunicipality::lists('id');

        foreach(range(1, $maxRange) as $index)
        {
            $barangay = new Barangay;
            $barangay->name = $this->faker->streetName;
            $barangay->population = $this->faker->randomDigit;
            $barangay->city_municipality_id = $this->faker->randomElement($cities_municipalities);

            $barangay->save();

            echo "$index/$maxRange Barangays seeded\n";
        }
    }

}