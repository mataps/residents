<?php

use Faker\Factory as Faker;

class CitiesMunicipalitiesSeeder extends Seeder{

    protected $faker;

    function __construct()
    {
        $this->faker = Faker::create();
        CityMunicipality::truncate();
    }

    function run()
    {
        $maxRange = 20;
        $districts = District::lists('id');

        foreach(range(1, $maxRange) as $index)
        {
            $cityMunicipality = new CityMunicipality;
            $cityMunicipality->name = $this->faker->city;
            $cityMunicipality->population = $this->faker->randomDigit;
            $cityMunicipality->district_id = $this->faker->randomElement($districts);

            $cityMunicipality->save();

            echo "$index/$maxRange CitiesMunicipalities seeded\n";
        }
    }

}