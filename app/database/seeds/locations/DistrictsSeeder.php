<?php

use Faker\Factory as Faker;

class DistrictsSeeder extends Seeder{

    protected $faker;

    function __construct()
    {
        $this->faker = Faker::create();
        District::truncate();
    }

    function run()
    {
        $maxRange = 10;

        foreach(range(1, $maxRange) as $index)
        {
            $district = new District;
            $district->name = "District $index";
            $district->population = $this->faker->randomDigit;

            $district->save();

            echo "$index/$maxRange Districts seeded\n";
        }
    }

}