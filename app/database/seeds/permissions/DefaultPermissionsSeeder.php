<?php

use Faker\Factory as Faker;

class DefaultPermissionsSeeder extends Seeder{

    protected $faker;

    function __construct()
    {
        $this->faker = Faker::create();
        Permission::truncate();
    }

    function run()
    {
        $permissions = array(
            Permission::USER_MANAGEMENT_MANAGE_USERS,
            Permission::USER_MANAGEMENT_VIEW_USERS,
            Permission::USER_MANAGEMENT_MANAGE_GROUPS,
            Permission::USER_MANAGEMENT_VIEW_GROUPS,
            Permission::PROFILING_MANAGE_RESIDENTS,
            Permission::PROFILING_VIEW_RESIDENTS,
            Permission::PROFILING_MANAGE_HOUSEHOLDS,
            Permission::PROFILING_VIEW_HOUSEHOLDS,
            Permission::PROFILING_MANAGE_AFFILIATIONS,
            Permission::PROFILING_VIEW_AFFILIATIONS,
            Permission::TRANSACTION_SYSTEM_MANAGE_TRANSACTIONS,
            Permission::TRANSACTION_SYSTEM_VIEW_TRANSACTIONS,
            Permission::SCHOLARSHIP_MANAGEMENT_MANAGE_SCHOLARSHIPS,
            Permission::SCHOLARSHIP_MANAGEMENT_VIEW_SCHOLARSHIPS,
            Permission::PROFILING_VIEW_ATTRIBUTIONS,
            Permission::PROFILING_MANAGE_ATTRIBUTIONS,
            Permission::TRANSACTION_SYSTEM_GENERATE_REPORT
        );

        $accounts = TransactionAccount::lists('name');

        // TRANSACTION_SYSTEM_VIEW_TRANSACTIONS_ACCOUNT_NAME
        // 

        foreach ($permissions as $name)
        {
            $permission         = new Permission;
            $permission->name   = $name;
            $permission->save();
        }

        foreach($accounts as $index => $accountName)
        {
            $permission = new Permission;
            $permission->name = Permission::TRANSACTION_SYSTEM_VIEW_TRANSACTIONS . "_" . str_replace(" ", "_", $accountName);
            $permission->save();

            $permission = new Permission;
            $permission->name = Permission::TRANSACTION_SYSTEM_MANAGE_TRANSACTIONS . "_" . str_replace(" ", "_", $accountName);
            $permission->save();
        }
    }
} 