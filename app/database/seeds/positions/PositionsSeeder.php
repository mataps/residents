<?php

use Faker\Factory as Faker;

class PositionsSeeder extends Seeder {

	/**
	 * @var Faker
	 */
	protected $faker;


	function __construct()
	{
		$this->faker = Faker::create();
		Position::truncate();
	}

	/**
	 * Run seeder
	 * 
	 * @return void
	 */
	public function run()
	{
		$positions = [
			'member',
			'president',
			'secretary',
			'adviser',
			'proponent'
		];

		$users = User::lists('id');

		foreach($positions as $name)
		{
			$position = new Position;
			$position->name = $name;

			$position->created_by = $this->faker->randomElement($users);
			$position->modified_by = $this->faker->randomElement($users);

			$position->save();
		}
	}
}