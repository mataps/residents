<?php


use SourceScript\Profiling\ProfilingInterface;

class LastnamesCounter extends Seeder {

    /**
     * @var ProfilingInterface
     */
    private $profiler;

    function __construct(ProfilingInterface $profiler)
    {

        $this->profiler = $profiler;
    }

    function run()
    {
        $this->profiler->execute('CalculateLastnames', array());
    }
}