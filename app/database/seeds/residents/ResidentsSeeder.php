<?php

use Faker\Factory as Faker;

class ResidentsSeeder extends Seeder{

    protected $faker;

    function __construct()
    {
        $this->faker = Faker::create();
        Resident::truncate();
    }

    function run()
    {
        if(App::environment() == "staging")
        {
            $maxRange = 1000;
        }
        else
        {
            $maxRange = 100;    
        }
        $genders = [User::GENDER_MALE, User::GENDER_FEMALE];
        $civil_statuses = [User::SINGLE, User::MARRIED, User::DIVORCED, User::WIDOWED];
        $users = User::lists('id');
        $districts = District::lists('id');
        $citiesMunicipalities = CityMunicipality::lists('id');
        $barangays = Barangay::lists('id');

        foreach(range(1, $maxRange) as $index)
        {
            $resident 				= new Resident;
            $resident->last_name 	= $this->faker->lastName;
            $resident->first_name 	= $this->faker->firstName;
            $resident->middle_name 	= $this->faker->lastName;
            $resident->full_name 	= sprintf('%s %s %s', $resident->first_name, $resident->middle_name, $resident->last_name);
            $resident->email 		= $this->faker->email;
            $resident->mobile 		= $this->faker->phoneNumber;
            $resident->phone 		= $this->faker->phoneNumber;
            $resident->precinct     = NULL;
            $resident->birthdate 	= $this->faker->dateTime;
            $resident->gender 		= $this->faker->randomElement($genders);
            $resident->civil_status = $this->faker->randomElement($civil_statuses);
            $resident->district_id  = $this->faker->randomElement($districts);
            $resident->city_municipality_id = $this->faker->randomElement($citiesMunicipalities);
            $resident->barangay_id  = $this->faker->randomElement($barangays);
            $resident->street       = $this->faker->streetAddress;
            $resident->area_code    = NULL;
            $resident->created_by   = $this->faker->randomElement($users);
            $resident->modified_by   = $resident->created_by;

//            $address = Address::create([
//                'municipality' => $this->faker->state,
//                'barangay' => $this->faker->streetName,
//                'street' => $this->faker->streetAddress
//            ]);
//            $resident->address()->associate($address);

            $resident->save();

            echo "$index/$maxRange Residents seeded\n";
        }
        
        $residents = Resident::lists('id');

        foreach(User::all() as $user)
        {
            $user->resident_id = $this->faker->randomElement($residents);
            $user->save();
        }
    }

}