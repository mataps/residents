<?php

use Faker\Factory as Faker;

class AllowancePoliciesSeeder extends Seeder {


	/**
	 * @var Faker
	 */
	protected $faker;

	/**
	 * @var array
	 */
	protected $users;

	function __construct(Faker $faker)
	{
		$this->faker = $faker->create();

		$this->users = User::lists('id');
	}

	public function run()
	{
		AllowancePolicy::truncate();

		$schoolTypes = ['public', 'private'];

		$schoolLevels = ['secondary', 'collegiate'];

		$ranges = [50 => 75, 75 => 100];

		$scholarshipTypes = ScholarshipType::lists('id');


		foreach($scholarshipTypes as $scholarshipType)
		{
			foreach($schoolTypes as $schoolType)
			{
				foreach($schoolLevels as $schoolLevel)
				{
					foreach($ranges as $from => $to)
					{
						$policy 						= new AllowancePolicy;
						$policy->scholarship_type_id 	= $scholarshipType;
						$policy->amount 				= 5000;
						$policy->school_type 			= $schoolType;
						$policy->school_level 			= $schoolLevel;
						$policy->from 					= $from;
						$policy->to 					= $to;
						$policy->created_by 			= $this->faker->randomElement($this->users);
						$policy->modified_by 			= $this->faker->randomElement($this->users);

						$policy->save();

						echo "Allowance policy seeded\n";
					}
					
				}
			}	
		}		
	}
}