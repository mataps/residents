<?php

use SourceScript\ScholarshipManagement\ScholarshipManagementInterface;

use Faker\Factory as Faker;

class ScholarshipsSeeder extends Seeder {

	/**
	 * @var Faker
	 */
	protected $faker;

	/**
	 * @var array
	 */
	protected $users;

	/**
	 * @var ScholarshipManagementService
	 */
	private $scholarshipManagementService;

	function __construct(Faker $faker, ScholarshipManagementInterface $scholarshipManagementInterface)
	{
		$this->scholarshipManagementService = $scholarshipManagementInterface;
		$this->faker = $faker->create();

		$this->users = User::lists('id');
	}


	/**
	 * Run migration
	 * 
	 */
	function run()
	{
		ScholarshipType::truncate();
		Scholarship::truncate();
		Semester::truncate();
		School::truncate();
		Subject::truncate();
		Grade::truncate();
		Award::truncate();
		ScholarshipAward::truncate();
		AllowanceRelease::truncate();
		Allowance::truncate();
		DB::table('allowance_releases_scholarship_types')->truncate();

		
		$this->schools();
		$this->scholarshipTypes();
		$this->scholarships();
		$this->subjects();
		$this->semesters();
		$this->awards();
		$this->awardScholarship();
		$this->allowanceRelease();
		$this->allowances();
	}

	private function scholarships()
	{
		$maxRange = 50;

		$residents = Resident::lists('id');
		$schools = School::lists('id');

		$scholars = $this->faker->randomElements($residents, 30);

		foreach($scholars as $index => $scholar)
		{
			$scholarship 						= new Scholarship;
			$scholarship->resident_id 			= $scholar;
			$scholarship->school_id 			= $this->faker->randomElement($schools);
			$scholarship->comment 				= $this->faker->sentence(rand(3,5));
			$scholarship->status 				= $this->faker->randomElement(['waiting', 'for_verification', 'active', 'closed', 'denied', 'stop']);
			$scholarship->created_by 			= $this->faker->randomElement($this->users);
			$scholarship->modified_by 			= $this->faker->randomElement($this->users);
			$scholarship->father_id = $this->faker->randomElement($residents);

			$scholarship->save();

			$scholarship->generateUuid();

			echo "$scholar/$maxRange Scholarships seeded\n";
		}
	}

	private function semesters()
	{
		$currentYear = date("Y");
		$scholarships = Scholarship::lists('id');
		$subjects = Subject::lists('id');
		$subjectCount = count($subjects);
		$scholarshipTypes = ScholarshipType::lists('id');

		foreach($scholarships as $key => $scholarship)
		{
			$school = $this->faker->randomElement(School::lists('id'));

			$maxRange = rand(1,10);

			foreach(range(1993, 1993 + $maxRange) as $index => $year)
			{
				foreach(range(1,2) as $term)
				{
					$semester 						= new Semester;
					$semester->term 				= $term;
					$semester->scholarship_type_id 	= $this->faker->randomElement($scholarshipTypes);
					$semester->school_year 			= $year . '-' . ($year + 1);
					$semester->scholarship_id 		= $scholarship;
					$semester->school_id 			= $school;
					$semester->created_by 			= $this->faker->randomElement($this->users);
					$semester->modified_by 			= $this->faker->randomElement($this->users);
					$semester->gwa 					= rand(75,100);

					$semester->save();

					$semester->allowance_basis_id	= (Semester::where('scholarship_id', $scholarship)->first()) ? $semester->id - 1 : null;

					$semester->save();
					$semester->generateUuid();
					echo "Scholarship:$scholarship $index/$maxRange Semesters seeded\n";	
				}
			}
		}
	}

	private function schools()
	{
		$maxRange = 20;
		$districts = District::lists('id');
        $citiesMunicipalities = CityMunicipality::lists('id');
        $barangays = Barangay::lists('id');

		foreach(range(0,20) as $index)
		{
			$school 				= new School;
			$school->name 			= $this->faker->company;
			// $school->address_id 	= $this->faker->randomElement($addresses);
			$school->address 		= $this->faker->address;
			$school->type 			= $this->faker->randomElement(['public', 'private']);
			$school->level 			= $this->faker->randomElement(['secondary', 'collegiate']);
			$school->phone 			= $this->faker->phoneNumber;
			$school->created_by 	= $this->faker->randomElement($this->users);
			$school->modified_by 	= $this->faker->randomElement($this->users);

			$school->save();
		}

		echo "$index/$maxRange Schools seeded\n";
	}

	public function subjects()
	{
		$subjects = array(
			'Accounting',
			'Astronomy',
			'Biochemistry',
			'Business Economics',
			'Chemistry',
			'Civil Engineering',
			'Cultural Studies',
			'Finance',
			'Geography',
			'Journalism',
			'Philosophy',
			'Physics',
			'Software Engineering'
		);

		$maxRange = count($subjects);

		foreach($subjects as $index => $subject)
		{
			$subj 				= new Subject;
			$subj->name 		= $subject;
			$subj->description 	= $this->faker->sentence(rand(1,5));
			$subj->created_by 	= $this->faker->randomElement($this->users);
			$subj->modified_by 	= $this->faker->randomElement($this->users);

			$subj->save();

			echo "$subject/$maxRange Subjects seeded\n";
		}
	}

	public function awards()
	{
		$awards = array(
			'Deans Lister',
			'Presidents Lister',
			'Scout Master',
			'Leadership Award'
		);

		$maxRange = count($awards);

		foreach($awards as $index => $award)
		{
			$awr 				= new Award;
			$awr->name 			= $award;
			$awr->description 	= $this->faker->sentence(rand(1,5));
			$awr->created_by 	= $this->faker->randomElement($this->users);
			$awr->modified_by 	= $this->faker->randomElement($this->users);

			$awr->save();

			echo "$award/$maxRange Awards seeded\n";
		}
	}

	public function awardScholarship()
	{
		$awards = Award::lists('id');
		$scholarships = Scholarship::lists('id');
		$semesters = Semester::lists('id');

		foreach($awards as $key => $award)
		{
			$awr = Award::find($award);
			$awr->scholarships()->attach($this->faker->randomElements($scholarships, rand(1, count($scholarships))), ['semester_id' => $this->faker->randomElement($semesters),'created_by' => $this->faker->randomElement($this->users), 'modified_by' => $this->faker->randomElement($this->users)]);
		}

		echo "Award to scholarshiip done\n";
	}


	public function allowanceRelease()
	{
		$scholarshipTypes= ScholarshipType::lists('id');


		// foreach(range(1993, 2013) as $year)
		// {
		// 	foreach(range(1,2) as $term)
		// 	{
		// 		$release = new AllowanceRelease;
		// 		$release->term = $term;
		// 		$release->school_year = $year . '-' . ($year + 1);
		// 		$release->fixed_amount = false;
		// 		$release->created_by = $this->faker->randomElement($this->users);
		// 		$release->modified_by = $this->faker->randomElement($this->users);
                //
		// 		$release->save();
                //
		// 		$release->generateUuid();
                //
		// 		$release->scholarshipTypes()->attach($scholarshipTypes);
		// 	}
		// }
	}

	public function allowances()
	{
		$allowanceReleases = AllowanceRelease::all();

		foreach($allowanceReleases as $allowanceRelease)
		{
			$semesters = $this->scholarshipManagementService->findSemestersByAllowanceRelease($allowanceRelease);

			foreach($semesters as $semester)
			{
				$this->scholarshipManagementService->execute('AddAllowance', [], $semester, $allowanceRelease, User::find(1));
			}
		}
	}

	public function scholarshipTypes()
	{
		$types = array(
			'CHED',
			'CHED Regular',
			'DA',
			'DSWD',
			'Tesda'
			);

		$maxRange = count($types);

		foreach($types as $index => $type)
		{
			ScholarshipType::create([
				'name' 			=> $type,
				'description' 	=> $this->faker->sentence(rand(1,5)),
				'created_by' 	=> $this->faker->randomElement($this->users),
				'modified_by' 	=> $this->faker->randomElement($this->users)
				]);

			echo "$index/$maxRange Scholarship types seeded\n";
		}
	}

	public function allowancePolicies()
	{
		$schoolTypes = ['public', 'private'];

		$schoolLevels = ['secondary', 'collegiate'];

		$ranges = [50 => 75, 75 => 100];

		$scholarshipTypes = ScholarshipType::lists('id');


		foreach($scholarshipTypes as $scholarshipType)
		{
			foreach($schoolTypes as $schoolType)
			{
				foreach($schoolLevels as $schoolLevel)
				{
					foreach($ranges as $from => $to)
					{
						$policy 						= new AllowancePolicy;
						$policy->scholarship_type_id 	= $scholarshipType;
						$policy->amount 				= 5000;
						$policy->school_type 			= $schoolType;
						$policy->school_level 			= $schoolLevel;
						$policy->from 					= $from;
						$policy->to 					= $to;
						$policy->created_by 			= $this->faker->randomElement($this->users);
						$policy->modified_by 			= $this->faker->randomElement($this->users);

						$policy->save();

						echo "Allowance policy seeded\n";
					}
					
				}
			}	
		}
		
	}
}
