<?php

use Faker\Factory as Faker;

class TransactionAccountsSeeder extends Seeder{

    protected $faker;

    function __construct()
    {
        $this->faker = Faker::create();
        TransactionAccount::truncate();
    }

    function run()
    {
        $users = User::lists('id');

        $accounts = [
            '4CDO' => 'Accounting office',
            'NE4' => 'NE4 Foundation',
            'NE4X' => 'NE4 Foundation',
            'NE4LAB' => 'NE4 Foundation',
            'PASSPORT' => 'Accounting office',
            'SPECIAL' => 'Accounting office',
            'REFERRAL' => 'Accounting office',
            '4CDOFLOAT' => 'NE4 Foundation',
            'COLLECTIONS' => 'Accounting office',
            'POLITICAL' => 'Accounting office',
            'INFRA' => 'Accounting office',
            'SPECIAL REQUEST' => 'Accounting office'
        ];

        foreach ($accounts as $name => $department)
        {
            $household = new TransactionAccount;
            $household->name 		= $name;
            $household->signatory	= $department;
            $household->affiliation_id   = 1;

            $household->save();
        }
    }
}