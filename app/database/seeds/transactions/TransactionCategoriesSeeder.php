<?php

use Faker\Factory as Faker;

class TransactionCategoriesSeeder extends Seeder{

    private $faker;

    private $user;

    function __construct()
    {
        $this->faker = Faker::create();
        $this->users = User::lists('id');
        TransactionCategory::truncate();
        TransactionSubCategory::truncate();
        DB::table('category_sub_categories')->truncate();
    }

    function run()
    {
        $categories = [
            'Assistance',
            'Personnel',
            'Utilities',
            'Vehicle',
            'Office',
            'Kitchen'
        ];


        foreach ($categories as $category)
        {
            $transactionCategory = new TransactionCategory;
            $transactionCategory->name = $category;

            $transactionCategory->created_by = $this->faker->randomElement($this->users);
            $transactionCategory->modified_by = $this->faker->randomElement($this->users);

            $transactionCategory->save();
        }

        $subCategories = [
            'Burial',
            'Financial',
            'Health & Medication',
            'Marketing',
            'Gasoline',
            'Travel',
            'Equipment',
            'Health & Beauty',
            'Home & Living',
            'Supply'
        ];

        $categories = TransactionCategory::lists('id');

        foreach($subCategories as $subCategory)
        {
            $transactionSubCategory = new TransactionSubCategory;
            $transactionSubCategory->name = $subCategory;
            $transactionSubCategory->category_id = $this->faker->randomElement($categories);

            $transactionSubCategory->created_by = $this->faker->randomElement($this->users);
            $transactionSubCategory->modified_by = $this->faker->randomElement($this->users);

            $transactionSubCategory->save();
        }


        // $subCategories = TransactionSubCategory::lists('id');

        // foreach($categories as $category)
        // {
        //     foreach($subCategories as $subCategory)
        //     {
        //         DB::table('category_sub_categories')->insert(array(
        //                 'category_id' => $category,
        //                 'sub_category_id' => $subCategory,
        //                 'created_at' => Carbon\Carbon::now(),
        //                 'updated_at' => Carbon\Carbon::now(),
        //                 'created_by' => $this->faker->randomElement($this->users),
        //                 'modified_by' => $this->faker->randomElement($this->users)
        //             ));
        //     }
        // }
    }
}