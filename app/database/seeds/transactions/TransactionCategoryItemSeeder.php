<?php

use Faker\Factory as Faker;

class TransactionCategoryItemsSeeder extends Seeder {
	
	function __construct()
	{
		$this->faker = Faker::create();
		TransactionItem::truncate();
	}

	function run()
	{
		$users = User::lists('id');

		$items = [
			'Food & Beverages',
			'Flower',
			'Cash',
			'Cleaning Equipment',
			'Snacks',
			'Reading Materials',
			'Lighter',
			'Personal Clothes'
		];

		foreach($items as $item)
		{
			$transactionItem 				= new TransactionItem;
			$transactionItem->name 			= $item;
			$transactionItem->description 	= $this->faker->sentence(rand(3, 10));
			$transactionItem->created_by 	= $this->faker->randomElement($users);
			$transactionItem->modified_by 	= $this->faker->randomElement($users);

			$transactionItem->save();
		}
	}
}