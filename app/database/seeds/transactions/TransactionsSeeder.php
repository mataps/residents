<?php

use Faker\Factory as Faker;

class TransactionsSeeder extends Seeder{

    protected $faker;

    function __construct()
    {
        $this->faker = Faker::create();
        Transaction::truncate();
        Voucher::truncate();
    }

    function run()
    {
        if(App::environment() == "staging")
        {
            $maxRange = 1000;    
        }
        else
        {
            $maxRange = 100;
        }
        
        $users = User::lists('id');
        $residents = Resident::lists('id');
        $affiliations = Affiliation::lists('id');
        $statuses = [Transaction::FLOATING, Transaction::REJECTED, Transaction::APPROVED];
        $accounts = TransactionAccount::lists('id');
        $categories = TransactionSubCategory::lists('id');
        $items = TransactionItem::lists('id');
        $types = ['Resident', 'Affiliation'];



        foreach(range(1, $maxRange) as $voucherIndex)
        {
            $category = $this->faker->randomElement($categories);

            $voucher = new Voucher;

            $voucher->account_type_id   = $this->faker->randomElement($accounts);
            $voucher->client_type       = 'Resident';
            $voucher->client_id         = $this->faker->randomElement($residents);
            $voucher->status            = $this->faker->randomElement(['floating', 'rejected', 'approved']);
            $voucher->settled_at        = Date('Y-m-d');
            $voucher->created_by        = $this->faker->randomElement($users);
            $voucher->modified_by       = $this->faker->randomElement($users);

            $voucher->save();

            $voucher->generateUuid();

            foreach(range(1, rand(1,5)) as $index)
            {
                $type = $this->faker->randomElement($types);

                $transactionType = $this->faker->randomElement(['income', 'expense']);

                $transaction = new Transaction;
                $transaction->voucher_id        = $voucher->id;
                $transaction->settled_at        = $this->faker->dateTimeBetween($startDate = '-30 years', $endDate = 'now');
                $transaction->referrer_id       = $this->faker->randomElement($residents);
                $transaction->client_type       = $voucher->client_type;
                $transaction->client_id         = $voucher->client_id;
                $transaction->beneficiary_type  = $type;
                $transaction->beneficiary_id    = $type == 'Resident' ? $this->faker->randomElement($residents) : $this->faker->randomElement($affiliations);
                $transaction->sub_category_id   = $category;
                $transaction->item_id           = $this->faker->randomElement($items);
                $transaction->transaction_type  = $transactionType;
                $transaction->details           = $this->faker->words(10,20);
                $transaction->remarks           = $this->faker->words(10,20);
                $transaction->amount            = rand(1000,50000);
                $transaction->created_by        = $this->faker->randomElement($users);
                $transaction->modified_by       = $this->faker->randomElement($users);

                $transaction->save();

                $transaction->generateUuid();

                if($transactionType == "income")
                {
                    if(rand(0,1))
                    {
                        $transaction->liquidatable = 1;
                        $transaction->liquidated_at = date('Y-m-d H:i:s');

                        $transaction->save();

                        $item = new Liquidation;

                        $item->label = $this->faker->words(1,3);
                        $item->description = $this->faker->words(5,20);
                        $item->amount = $transaction->amount;
                        $item->transaction_id = $transaction->id;
                        $item->created_by = $this->faker->randomElement($users);
                        $item->modified_by = $this->faker->randomElement($users);

                        $item->save();
                    }
                }
            }

            echo "$voucherIndex/$maxRange Transactions seeded\n";

        }
    }
}