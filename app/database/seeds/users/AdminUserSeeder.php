<?php

use Faker\Factory as Faker;


class AdminUserSeeder extends Seeder{

    protected $faker;

    function __construct()
    {
        $this->faker = Faker::create();
        User::truncate();
        // User::where('username', 'admin')->delete();
    }

    function run()
    {
        $user = new User;
        $user->username = 'admin';
        $user->password = Hash::make('123');
        $user->group()->associate(Group::where('name', 'Administrators')->first());

        if ($admin = User::withTrashed()->where('username', 'admin')->first())
        {
            return;
        }
        $user->save();

        $permissions = Permission::lists('id');

        foreach ($permissions as $permission)
        {
            UserPermission::create([
                'user_id' => $user->id,
                'permission_id' => $permission,
                'status' => 1
            ]);
        }
    }
} 