<?php

use Faker\Factory as Faker;

class UsersSeeder extends Seeder
{
    protected $faker;

    function __construct()
    {
        $this->faker = Faker::create();
        User::where('username', '!=', 'admin')->delete();
    }

    function run()
    {
        $maxRange = 10;
        $groups = Group::lists('id');

        foreach (range(1, $maxRange) as $index)
        {
            $user = new User;
            
            $user->username = $this->faker->userName;
            $user->password = Hash::make('123');
            $user->group_id = $this->faker->randomElement($groups);
            $user->save();

            echo "$index/$maxRange Users seeded\n";
        }
    }
} 