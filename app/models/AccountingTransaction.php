<?php


use SourceScript\Common\Models\BaseModel;

class AccountingTransaction extends BaseModel{

    protected $table = 'accounting_transactions';

    protected $guarded = array();
} 