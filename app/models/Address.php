<?php

use SourceScript\Common\Mixins\JoinableTrait;
use SourceScript\Common\Models\BaseModel;

class Address extends BaseModel {
    use JoinableTrait;

    protected $table = 'addresses';

    protected $guarded = array();
}