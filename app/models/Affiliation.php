<?php

use SourceScript\Common\Mixins\JoinableTrait;
use SourceScript\Common\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Affiliation extends BaseModel {

    use JoinableTrait, SoftDeletingTrait;

    /**
     * Table name
     * 
     * @var string
     */
    protected $table = 'affiliations';

    /**
     * Guarded fields
     * 
     * @var array
     */
    protected $guarded = array();


    public function fullName()
    {
        return $this->name;
    }

    /**
     * Returns the address of the resident
     * 
     * @return string
     */
    public function fullAddress()
    {
        return implode(', ', [
            $this->street,
            $this->barangay ? $this->barangay->name : '',
            $this->district ? $this->district->name : '',
            $this->cityMunicipality ? $this->cityMunicipality->name : ''
            ]);
    }

    
    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function founders()
    {
        return $this->belongsToMany('Resident', 'residents_affiliations')->where('founder', true)->withPivot(['position_id', 'founder', 'resident_id', 'affiliation_id', 'from', 'to']);;
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function members()
    {
        return $this->belongsToMany('Resident', 'residents_affiliations')->withPivot(['position_id', 'founder', 'resident_id', 'affiliation_id', 'from', 'to']);
    }

    public function residents()
    {
        return $this->belongsToMany('Resident', 'residents_affiliations')->withPivot(['position_id', 'founder', 'from', 'to', 'referrer_id', 'head_id']);
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function transactions()
    {
        return $this->morphMany('Transaction', 'beneficiary')->orWhere('client_id', $this->id);
    }

    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function district()
    {
        return $this->belongsTo('District', 'district_id');
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function cityMunicipality()
    {
        return $this->belongsTo('CityMunicipality', 'city_municipality_id');
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function barangay()
    {
        return $this->belongsTo('Barangay', 'barangay_id');
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function creator()
    {
        return $this->belongsTo('User', 'created_by');
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function updater()
    {
        return $this->belongsTo('User', 'modified_by');
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function positions()
    {
        return $this->belongsToMany('Position', 'affiliations_positions', 'affiliation_id', 'position_id');
    }
}