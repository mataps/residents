<?php

use SourceScript\Common\Models\BaseModel;

class AffiliationPosition extends BaseModel {

	protected $table = "affiliations_positions";

	protected $guarded = [];

	public function affiliation()
	{
		return $this->belongsTo('Affiliation', 'affiliation_id');
	}

	public function position()
	{
		return $this->belongsTo('Position', 'position_id');
	}
}
