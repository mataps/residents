<?php

use SourceScript\Common\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Allowance extends BaseModel {

	use SoftDeletingTrait;

	/**
	 * The database table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'allowances';


	public function scholarship()
	{
  		$semester = $this->belongsTo('Semester', 'semester_id');

		return $semester->getREsults()->belongsTo('Scholarship');
	}


	public function policy()
	{
		return $this->belongsTo('AllowancePolicy', 'policy_id');
	}


	public function semester()
	{
		return $this->belongsTo('Semester');
	}

	public function creator()
	{
		return $this->belongsTo('User', 'created_by');
	}

	public function updater()
	{
		return $this->belongsTo('User', 'modified_by');
	}

	public function release()
	{
		return $this->belongsTo('AllowanceRelease', 'allowance_release_id');
	}
}