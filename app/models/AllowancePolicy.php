<?php

use SourceScript\Common\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class AllowancePolicy extends BaseModel {

	use SoftDeletingTrait;

	/**
	 * The database table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'allowance_policies';

	/**
	 * Guarded fields
	 * 
	 * @var array
	 */
	protected $guarded = [];


	/**
	 * @return Relation
	 */
	public function scholarships()
	{
		return $this->hasMany('Scholarship');
	}


	/**
	 * @return Relation
	 */
	public function scholarshipType()
	{
		return $this->belongsTo('ScholarshipType', 'scholarship_type_id');
	}


	/**
	 * @return Relation
	 */
	public function creator()
	{
		return $this->belongsTo('User', 'created_by');
	}


	/**
	 * @return Relation
	 */
	public function updater()
	{
		return $this->belongsTo('User', 'modified_by');
	}
}