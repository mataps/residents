<?php

use SourceScript\Common\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class AllowancePolicyOverride extends BaseModel {

	use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 * 
	 * @var string
	 */
	protected $table = 'policies_overrides';


	/**
	 * Guaded fields.
	 * 
	 * @var array
	 */
	protected $guarded = [];


	/**
	 * @return Relation
	 */
	public function scholarshipTypes()
	{
		return $this->belongsToMany('ScholarshipType', 'policies_overrides_scholarship_types', 'policies_override_id', 'scholarship_type_id');
	}


	/**
	 * @return Relation
	 */
	public function creator()
	{
		return $this->belongsTo('User', 'created_by');
	}


	/**
	 * @return Relation
	 */
	public function updater()
	{
		return $this->belongsTo('User', 'modified_by');
	}
}