<?php

use SourceScript\Common\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class AllowanceRelease extends BaseModel {

	use SoftDeletingTrait;

	/**
	 * Table name
	 * 
	 * @var string
	 */
	protected $table = "allowance_releases";

	/**
	 * Guarded fields
	 * 
	 * @var array
	 */
	protected $guarded = array();


	/**
	 * @return Relationship
	 */
	public function creator()
	{
		return $this->belongsTo('User', 'created_by');
	}


	/**
	 * @return Relationship
	 */
	public function updater()
	{
		return $this->belongsTo('User', 'modified_by');
	}


	/**
	 * @return Relationship
	 */
	public function allowances()
	{
		return $this->hasMany('Allowance', 'allowance_release_id');
	}


	/**
	 * @return Relationship
	 */
	public function scholarshipTypes()
	{
		return $this->belongsToMany('ScholarshipType', 'allowance_releases_scholarship_types', 'allowance_release_id', 'scholarship_type_id');
	}

	public function generateUuid()
	{
		$id = $this->idForAllowanceRelease();

		do {
			$this->uuid = $this->school_year . '-';
			$this->uuid .= $this->term . '-';
			$this->uuid .= str_pad($id, 3, "0", STR_PAD_LEFT);

			++$id;
		} while(AllowanceRelease::where('uuid', $this->uuid)->count() > 0);

		$this->save();
	}

	public function idForAllowanceRelease()
	{
		$id = AllowanceRelease::where('school_year', $this->school_year)
			->where('term', $this->term)
			->count();

		return $id;
	}
}