<?php

use SourceScript\Common\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Attribution extends BaseModel {

	use SoftDeletingTrait;

	/**
	 * The database table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'attributions';

	protected $guarded = array();

	/**
	 * @return Relation
	 */
	public function creator()
	{
		return $this->belongsTo('User', 'created_by');
	}


	/**
	 * @return Relation
	 */
	public function updater()
	{
		return $this->belongsTo('User', 'modified_by');
	}

	public function residents()
	{
		return $this->belongsToMany('Attribution', 'residents_attributes', 'resident_id', 'attribute_id')->withPivot(['from', 'to']);
	}
}