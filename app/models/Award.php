<?php

use SourceScript\Common\Models\BaseModel;

class Award extends BaseModel {

	/**
	 * The database table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'awards';

	protected $fillable = ['name', 'description'];

	public function creator()
	{
		return $this->belongsTo('User', 'created_by');
	}

	public function updater()
	{
		return $this->belongsTo('User', 'modified_by');
	}

	public function scholarships()
	{
		return $this->belongsToMany('Scholarship', 'award_scholarship');
	}

	/**
	 * Eloquent relation
	 * 
	 * @return Relation
	 */
	public function attributions()
	{
		return $this->belongsToMany('Attribution', 'attributions_awards', 'award_id', 'attribution_id');
	}
}