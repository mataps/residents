<?php

use SourceScript\Common\Mixins\JoinableTrait;
use SourceScript\Common\Models\BaseModel;

class Barangay extends BaseModel {

    use JoinableTrait;

    protected $table = 'barangays';

    protected $guarded = array();

    public function cityMunicipality()
    {
    	return $this->belongsTo('CityMunicipality', 'city_municipality_id');
    }

    public function residents()
    {
    	return $this->hasMany('Resident');
    }
}
