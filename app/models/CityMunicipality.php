<?php

use SourceScript\Common\Mixins\JoinableTrait;
use SourceScript\Common\Models\BaseModel;

class CityMunicipality extends BaseModel{
    use JoinableTrait;

    protected $table = 'cities_municipalities';

    protected $guarded = array();


    public function residents()
    {
    	return $this->hasMany('Resident');
    }

    public function getNameAttribute($value)
    {
    	return Str::title($value);
    }

    public function barangays()
    {
        return $this->hasMany('Barangay');
    }
}