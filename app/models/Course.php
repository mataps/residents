<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;

use SourceScript\Common\Models\BaseModel;

class Course extends BaseModel {

	use SoftDeletingTrait;

	/**
	 * Gurded felds
	 * 
	 * @var array
	 */
	protected $guarded = [];

	/**
	 * Table name
	 * 
	 * @var string
	 */
	protected $table = "courses";


	public function creator()
	{
		return $this->belongsTo('User', 'created_by');
	}

	public function updater()
	{
		return $this->belongsTo('User', 'modified_by');
	}
}