<?php

use SourceScript\Common\Mixins\JoinableTrait;
use SourceScript\Common\Models\BaseModel;

class District extends BaseModel{
    use JoinableTrait;

    protected $table = 'districts';

    protected $guarded = array();
}