<?php

use SourceScript\Common\Models\BaseModel;

class Grade extends BaseModel {

	/**
	 * The database table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'grades';

	protected $guarded = array();

	public function semester()
	{
		return $this->belongsTo('Semester');
	}
	
	public function subject()
	{
		return $this->belongsTo('Subject');
	}
}