<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use SourceScript\Common\Mixins\JoinableTrait;
use SourceScript\Common\Models\BaseModel;

class Group extends BaseModel{
    use JoinableTrait, SoftDeletingTrait;

    protected $table = 'groups';

    protected $guarded = array();

    function permissions()
    {
        return $this->belongsToMany('Permission', 'groups_permissions')->where('status', '<>', '0')->withPivot('status');
    }

    function allPermissions()
    {
        return $this->belongsToMany('Permission', 'groups_permissions')->withPivot('status');
    }

    function users()
    {
        return $this->hasMany('User');
    }
}