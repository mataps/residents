<?php

use SourceScript\Common\Models\BaseModel;

class GroupPermission extends BaseModel{

    protected $table = 'groups_permissions';

    protected $guarded = array();
} 