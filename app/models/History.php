<?php

use SourceScript\Common\Models\BaseModel;

class History extends BaseModel {

	protected $table = 'history';

	//add,update,remove


	function user()
	{
		return $this->hasOne('User', 'id', 'user_id');
	}

	function historable()
	{
		return $this->morphTo();
	}

	public function getResourceAttribute()
	{
		return $this;
	}
}