<?php

use SourceScript\Common\Models\BaseModel;

class Household extends BaseModel {

    /**
     * Table name
     * 
     * @var string
     */
    protected $table = 'households';

    /**
     * Guarded fields
     * 
     * @var array
     */
    protected $guarded = array();


    /**
     * Returns the address of the resident
     * 
     * @return string
     */
    public function fullAddress()
    {
        return implode(', ', [
            $this->street,
            $this->barangay->name,
            $this->district->name,
            $this->cityMunicipality->name
            ]);
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function owner()
    {
        return $this->belongsToMany('Resident', 'households_residents')->where('role', 'owner');
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function members()
    {
        return $this->belongsToMany('Resident', 'households_residents')->where('role', 'member');
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function residents()
    {
        return $this->belongsToMany('Resident', 'households_residents');
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function district()
    {
        return $this->belongsTo('District', 'district_id');
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function cityMunicipality()
    {
        return $this->belongsTo('CityMunicipality', 'city_municipality_id');
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function barangay()
    {
        return $this->belongsTo('Barangay', 'barangay_id');
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
	public function creator()
    {
        return $this->belongsTo('User', 'created_by');
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function updater()
    {
        return $this->belongsTo('User', 'updated_by');
    }
}