<?php

use SourceScript\Common\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Liquidation extends BaseModel {


	use SoftDeletingTrait;

	/**
	 * Database table name.
	 * 
	 * @var string
	 */
	protected $table = 'liquidations';


	/**
	 * Guarded fields.
	 * 
	 * @var array
	 */
	protected $guarded = [];


	/**
	 * @return Relation
	 */
	public function transaction()
	{
		return $this->belongsTo('Transaction');
	}


	/**
	 * @return Relation
	 */
	public function creator()
	{
		return $this->belongsTo('User', 'created_by');
	}


	/**
	 * @return Relation
	 */
	public function updater()
	{
		return $this->belongsTo('User', 'modified_by');
	}
}