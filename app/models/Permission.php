<?php


use SourceScript\Common\Models\BaseModel;

class Permission extends BaseModel{

    const USER_MANAGEMENT_MANAGE_USERS = 'user_management.manage_users';
    const USER_MANAGEMENT_VIEW_USERS = 'user_management.view_users';

    const USER_MANAGEMENT_MANAGE_GROUPS = 'user_management.manage_groups';
    const USER_MANAGEMENT_VIEW_GROUPS = 'user_management.view_groups';

    const PROFILING_MANAGE_RESIDENTS = 'profiling.manage_residents';
    const PROFILING_VIEW_RESIDENTS = 'profiling.view_residents';

    const PROFILING_MANAGE_ATTRIBUTIONS = 'profiling.manage_attributions';
    const PROFILING_VIEW_ATTRIBUTIONS = 'profiling.view_attributions';

    const PROFILING_MANAGE_HOUSEHOLDS = 'profiling.manage_households';
    const PROFILING_VIEW_HOUSEHOLDS = 'profiling.view_households';

    const PROFILING_MANAGE_AFFILIATIONS = 'profiling.manage_affiliations';
    const PROFILING_VIEW_AFFILIATIONS = 'profiling.view_affiliations';

    const TRANSACTION_SYSTEM_MANAGE_TRANSACTIONS = 'transaction_system.manage_transactions';
    const TRANSACTION_SYSTEM_VIEW_TRANSACTIONS = 'transaction_system.view_transactions';
    const TRANSACTION_SYSTEM_GENERATE_REPORT = 'transaction_system.generate_report';

    const SCHOLARSHIP_MANAGEMENT_MANAGE_SCHOLARSHIPS = 'scholarship_management.manage_scholarships';
    const SCHOLARSHIP_MANAGEMENT_VIEW_SCHOLARSHIPS = 'scholarship_management.view_scholarships';


    protected $table = 'permissions';

    protected $guarded = array();
} 