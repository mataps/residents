<?php


use SourceScript\Common\Models\BaseModel;

class Photo extends BaseModel{

    protected $table = 'photos';

    protected $guarded = array();

    function residents()
    {
        return $this->hasMany('Resident');
    }
} 