<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use SourceScript\Common\Models\BaseModel;

class Position extends BaseModel {

	use SoftDeletingTrait;


	/**
	 * Table name
	 * 
	 * @var string
	 */
	protected $table = 'positions';


	/**
	 * Guarded fields
	 * 
	 * @var array
	 */
	protected $guarded = array();


	/**
	 * Eloquent relation
	 * 
	 * @return Relation
	 */
	public function creator()
	{
		return $this->belongsTo('User', 'created_by');
	}


	/**
	 * Eloquent relation
	 * 
	 * @return Relation
	 */
	public function updater()
	{
		return $this->belongsTo('User', 'modified_by');
	}


	/**
	 * Eloquent relation
	 * 
	 * @return Relation
	 */
	public function affiliations()
	{
		return $this->belongsToMany('Affiliation', 'affiliations_positions', 'position_id', 'affiliation_id');
	}


	/**
	 * Eloquent relation
	 * 
	 * @return Relation
	 */
	public function attributions()
	{
		return $this->belongsToMany('Attribution', 'attributions_positions', 'position_id', 'attribution_id');
	}


	/**
	 * Eloquent relation
	 * 
	 * @return Relation
	 */
	public function residentAffiliations()
	{
		return $this->hasMany('ResidentAffiliation', 'position_id');
	}
}