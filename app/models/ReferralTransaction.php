<?php


use SourceScript\Common\Models\BaseModel;

class ReferralTransaction extends BaseModel{

    protected $table = 'referral_transactions';

    protected $guarded = array();
} 