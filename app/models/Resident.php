<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use SourceScript\Common\Mixins\JoinableTrait;
use SourceScript\Common\Models\BaseModel;

class Resident extends BaseModel {

    use JoinableTrait, SoftDeletingTrait;

    /**
     * Table name
     * 
     * @var string
     */
    protected $table = 'residents';


    /**
     * Guarded fields
     * 
     * @var array
     */
    protected $guarded = array();


    /**
     * Returns the address of the resident
     * 
     * @return string
     */
    public function fullAddress()
    {
        return implode(', ', [
            $this->street,
            $this->barangay ? $this->barangay->name : '',
            $this->district ? $this->district->name : '',
            $this->cityMunicipality ? $this->cityMunicipality->name : ''
            ]);
    }

    public function fullName()
    {
        return implode(' ', [
            $this->first_name,
            $this->middle_name,
            $this->last_name
            ]);
    }


    /**
     * Returns name
     * 
     * @return string
     */
    public function name()
    {
        return $this->last_name . ', ' . $this->first_name;
    }

    
    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function profilePic()
    {
        return $this->belongsTo('Photo', 'photo_id');
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function signature()
    {
        return $this->belongsTo('Photo', 'photo_id');
    }

    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function parents()
    {
        return $this->belongstoMany('Resident', 'resident_relationships', 'resident_id', 'relative_id')->withPivot('relation')->where('relation', 'parent');
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function relatives()
    {
        return $this->belongsToMany('Resident', 'resident_relationships', 'resident_id', 'relative_id')->withPivot(['relation']);
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function district()
    {
        return $this->belongsTo('District', 'district_id');
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function cityMunicipality()
    {
        return $this->belongsTo('CityMunicipality', 'city_municipality_id');
    }
    
    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function barangay()
    {
        return $this->belongsTo('Barangay', 'barangay_id');
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
	public function households()
	{
		return $this->belongsToMany('Household', 'households_residents')->withPivot(array('role', 'resident_id', 'household_id', 'from', 'to'));
	}

    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
	public function household()
	{
		return $this->belongsTo('Household');
	}


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function attributes()
    {
        return $this->belongsToMany('Attribution', 'residents_attributes', 'resident_id', 'attribute_id')->withPivot(['from', 'to']);
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function attributions()
    {
        return $this->belongsToMany('Attribution', 'residents_attributes', null, 'attribute_id')->withPivot(['from', 'to']);
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
	public function affiliations()
	{
		return $this->belongsToMany('Affiliation', 'residents_affiliations', 'resident_id', 'affiliation_id')->withPivot(array('status', 'remarks', 'from', 'to', 'head_id', 'referrer_id', 'position_id', 'founder'));
	}


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function creator()
    {
        return $this->belongsTo('User', 'created_by');
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function updater()
    {
        return $this->belongsTo('User', 'modified_by');
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function transactions()
    {
        return $this->morphMany('Transaction', 'beneficiary');
    }

    /**
     * Get the Names of Affiliation
     * @return  Array description
     * 
     */
    public function getAffiliationNames()
    {
        $output = $this->affiliations->lists('name');

        return $output;
    }
    /**
     * Get the Names of Attributes
     * @return  Array description
     * 
     */
    public function getAttributeNames()
    {
	   return $this->attributions->lists('name');
    }

    public function positions()
    {
        return $this->belongstoMany('Position', 'residents_affiliations', 'resident_id', 'position_id');
    }

    public function position()
    {
        return $this->belongstoMany('Position', 'residents_affiliations', 'resident_id', 'position_id');
    }

    public function getAge()
    {
        if($this->birthdate)
        {
            return \Carbon\Carbon::now()->diffInYears(\Carbon\Carbon::createFromTimestamp(strtotime($this->birthdate)));
        }
        return '';
    }

    public function semesters()
    {
        return $this->hasMany('Semester');
    }
}