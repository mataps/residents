<?php

use SourceScript\Common\Models\BaseModel;
use SourceScript\Common\Mixins\JoinableTrait;

class ResidentAffiliation extends BaseModel {

	use JoinableTrait;	
	
    protected $table = 'residents_affiliations';

    protected $guarded = array();


    public function resident()
    {
    	return $this->belongsTo('Resident', 'resident_id');
    }

    public function affiliation()
    {
    	return $this->belongsTo('Affiliation', 'affiliation_id');
    }

    public function referrer()
    {
        return $this->belongsTo('Resident', 'referrer_id');
    }

    public function position()
    {
        return $this->belongsTo('Position', 'position_id');
    }

    public function creator()
    {
        return $this->belongsTo('User', 'created_by');
    }

    public function updater()
    {
        return $this->belongsTo('User', 'modified_by');
    }

    public function head()
    {
        return $this->belongsTo('Resident', 'head_id');
    }
}