<?php

use SourceScript\Common\Mixins\JoinableTrait;
use SourceScript\Common\Models\BaseModel;

class ResidentAttribute extends BaseModel {

    protected $table = 'residents_attributes';

    protected $guarded = array();

    public function resident()
    {
    	return $this->belongsTo('Resident', 'resident_id');
    }

    public function attribution()
    {
    	return $this->belongsTo('Attribution', 'attribute_id');
    }
}