<?php

use SourceScript\Common\Models\BaseModel;

class ResidentHousehold extends BaseModel {

    protected $table = 'households_residents';

    protected $guarded = array();

    public function household()
    {
    	return $this->belongsTo('Household', 'household_id');
    }

    public function resident()
    {
    	return $this->belongsTo('Resident', 'resident_id');
    }

    public function creator()
    {
    	return $this->belongsTo('User', 'created_by');
    }

    public function updater()
    {
    	return $this->belongsTo('User', 'modified_by');
    }
} 