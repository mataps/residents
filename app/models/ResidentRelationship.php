<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use SourceScript\Common\Mixins\JoinableTrait;
use SourceScript\Common\Models\BaseModel;

class ResidentRelationship extends BaseModel {

	use JoinableTrait, SoftDeletingTrait;


	/**
	 * Table name
	 * 
	 * @var string
	 */
	protected $table = 'resident_relationships';


	/**
	 * Guarded fields
	 * 
	 * @var array
	 */
	protected $guarded = [];


	/**
	 * Eloquent relation.
	 * 
	 * @return Relationship
	 */
	public function resident()
	{
		return $this->belongsTo('Resident', 'resident_id');
	}

	/**
	 * Eloquent relation.
	 * 
	 * @return Relationship
	 */
	public function relative()
	{
		return $this->belongsTo('Resident', 'relative_id');
	}
}