<?php

use SourceScript\Common\Mixins\JoinableTrait;
use SourceScript\Common\Models\BaseModel;

class Scholarship extends BaseModel {

	use JoinableTrait;

    /**
     * Table name
     * 
     * @var string
     */
    protected $table = 'scholarships';

    /**
     * Guarded fields
     * 
     * @var array
     */
    protected $guarded = array();


    /**
     * @return Relation
     */
    public function resident()
    {
        return $this->belongsTo('Resident', 'resident_id');
    }


    /**
     * @return Relation
     */
    public function creator()
    {
        return $this->belongsTo('User', 'created_by');
    }


    /**
     * @return Relation
     */
    public function updater()
    {
        return $this->belongsTo('User', 'modified_by');
    }


    /**
     * @return Relation
     */
    public function awards()
    {
        return $this->belongsToMany('Award', 'award_scholarship')->withPivot(['remarks', 'year']);
    }


    /**
     * @return Relation
     */
    public function school()
    {
        return $this->belongsTo('School');
    }


    /**
     * @return Relation
     */
    public function semesters()
    {
        return $this->hasMany('Semester');
    }


    /**
     * @return Relation
     */
    public function allowances()
    {
        return $this->hasMany('Allowance', 'allowance_id');
    }

    public function generateUuid()
    {
        $this->uuid = Date("Ymd", strtotime($this->created_at));
        $this->uuid .= "-";
        $this->uuid .= str_pad($this->id, 5, "0", STR_PAD_LEFT);

        $this->save();
    }

    public function father()
    {
        return $this->belongsTo('Resident', 'father_id');
    }

    public function mother()
    {
        return $this->belongsTo('Resident', 'mother_id');
    }
    public function referrer()
    {
        return $this->belongsTo('Resident', 'referrer_id');
    }


    public function guardian()
    {
        return $this->belongsTo('Resident', 'guardian_id');
    }

    public function transfered()
    {
        return $this->belongsTo('Scholarship', 'transfered_to', 'id');
    }
}