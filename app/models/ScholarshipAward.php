<?php

use SourceScript\Common\Models\BaseModel;

class ScholarshipAward extends BaseModel {
	
	/**
	 * The database table used by the model
	 * 
	 * @var string
	 */
	protected $table = "award_scholarship";
}