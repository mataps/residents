<?php

use SourceScript\Common\Mixins\JoinableTrait;
use SourceScript\Common\Models\BaseModel;

class ScholarshipType extends BaseModel {

	use JoinableTrait;

	/**
	 * Table name
	 * 
	 * @var string
	 */
	protected $table = 'scholarship_types';


	/**
	 * Guarded fields
	 * 
	 * @var array
	 */
	protected $guarded = array();


	/**
	 * @return Relation
	 */
	public function creator()
	{
		return $this->belongsTo('User', 'created_by');
	}


	/**
	 * @return Relation
	 */
	public function updater()
	{
		return $this->belongsTo('User', 'modified_by');
	}


	/**
	 * @return Relation
	 */
	public function scholarships()
	{
		return $this->hasMany('Scholarship', 'scholarship_type_id');
	}


	/**
	 * @return Relation
	 */
	public function allowancePolicies()
	{
		return $this->hasMany('AllowancePolicy', 'scholarship_type_id');
	}
}