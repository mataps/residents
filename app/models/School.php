<?php

use SourceScript\Common\Models\BaseModel;

class School extends BaseModel {

	/**
	 * The databse table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'schools';

	protected $guarded = array();

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array();

	public function address()
	{
		return $this->belongsTo('Address');
	}

	public function creator()
	{
		return $this->belongsTo('User', 'created_by');
	}

	public function updater()
	{
		return $this->belongsTo('User', 'modified_by');
	}
}