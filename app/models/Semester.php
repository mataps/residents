<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use SourceScript\Common\Models\BaseModel;

class Semester extends BaseModel {

	// use SoftDeletingTrait;
	
	/**
	 * The table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'semesters';


	/**
	 * Guarded fields
	 * 
	 * @var array
	 */
	protected $guarded = array();


	/**
	 * The attributes excluded from the model's JSON form
	 * 
	 * @var array
	 */
	protected $hidden = array();

	/**
	 * @return Relation
	 */
	public function allowanceBasis()
	{
		return $this->belongsTo('Semester', 'allowance_basis_id');
	}


	/**
	 * @return Relation
	 */
	public function scholarship()
	{
		return $this->belongsTo('Scholarship');
	}


	public function getScholar()
	{
		$scholarship = $this->scholarship;

		return Resident::find($scholarship->resident_id);
	}

	/**
	 * @return Relation
	 */
	public function school()
	{
		return $this->belongsTo('School');
	}


	/**
	 * @return Relation
	 */
	public function creator()
	{
		return $this->belongsTo('User', 'created_by');
	}
	

	/**
	 * @return Relation
	 */
	function updater()
	{
		return $this->belongsTo('User', 'modified_by');
	}


	/**
	 * @return Relation
	 */
	public function grades()
	{
		return $this->hasMany('Grade');
	}


	/**
	 * @return Relation
	 */
	public function allowances()
	{
		return $this->hasMany('Allowance');
	}


	/**
     * @return Relation
     */
    public function scholarshipType()
    {
        return $this->belongsTo('ScholarshipType', 'scholarship_type_id', 'id');
    }

    public function course()
    {
    	return $this->belongsTo('Course', 'course_id');
    }

    /**
     * @return Relation
     */
    public function awards()
    {
        return $this->belongsToMany('Award', 'award_scholarship', 'semester_id', 'award_id');
    }

    public function generateUuid()
    {
    	$id =$this->idForSemester();
    	//var_dump($id);
		$uuid = date("Ymd", strtotime($this->created_at));
 		$uuid .= '-' . strtoupper(str_replace(" ", "", $this->scholarshipType->name));
 		$uuid .= str_pad($id, 5, "0", STR_PAD_LEFT);
 		
 		$this->uuid = $uuid;
 		$this->save();
    }

    private function idForSemester()
    {
    	$date = date('Y-m-d', strtotime($this->created_at));
    	$sql = "select count(id) as count from semesters where created_at > '" . $date . " 00:00:01'" .  " and created_at < '" . $date . " 23:59:59'" . "and scholarship_type_id = :scholarship_type_id and uuid is not null";

    	$sems = DB::select(DB::raw($sql), array(
    			'scholarship_type_id' => $this->scholarship_type_id
     		));
    	
		$id = $sems[0]->count;
        return $id + 1;
    }

    /**
     * Eloquent relation
     * 
     * @return Resident
     */
    public function client()
    {
    	return $this->belongsTo('Resident', 'client_id');
    }



    public function computeAllowance(AllowanceRelease $allowanceRelease)
    {
    	if( $this->active == "active")
    	{    		
	    	if($allowanceRelease->fixed_amount)
	    	{
	    		return $allowanceRelease->amount;
	    	}
	    	else
	    	{
	    		if($this->is_probation)
	    		{
	    			return $this->scholarship_type->probation_amount;
	    		}
	    		else
	    		{
	    			if(isset($this->allowance_basis_id))
		    		{
		    			$policy = $this->getPolicy($this);
		    			if($policy) return $policy->amount;

		    			return 0;
		    		}
		    		else
		    		{
		    			return $this->scholarship_type->initial_allowance;
		    		}	
	    		}
	    	}
	    }else
	    {
	    	return 0;
	    }
    }

    /**
     * Gets the policy by semester
     * 
     * @param  Semester $semester
     * @return Policy
     */
    private function getPolicy(Semester $semester)
	{
		$policy = AllowancePolicy::where('scholarship_type_id', $semester->scholarship_type_id)
            ->where('school_type', $semester->school ? $semester->school->type : null)
            ->where('school_level', $semester->school_level)
            ->where('from', '<=', $semester->gwa)
            ->where('to', '>=', $semester->gwa)
            ->orderBy('to', 'desc')->first();

        return $policy;
	}
}