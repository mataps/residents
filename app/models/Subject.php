<?php

use SourceScript\Common\Models\BaseModel;

class Subject extends BaseModel {

	/**
	 * The database table used by the model
	 * 
	 * @var string
	 */
	protected $table = 'subjects';

	protected $fillable = ['name', 'description'];
}