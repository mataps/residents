<?php
use SourceScript\Common\Mixins\JoinableTrait;
use SourceScript\Common\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Transaction extends BaseModel {

	use JoinableTrait, SoftDeletingTrait;

    const FLOATING = 'Floating';
    const REJECTED = 'Rejected';
    const APPROVED = 'Approved';

    protected $table = 'transactions';

    protected $guarded = array();

    protected $dates = ['created_at', 'updated_at', 'settled_at', 'liquidated_at'];

    protected $prepend = ['beneficiary_name'];

    public function referrer()
    {
    	return $this->belongsTo('Resident', 'referrer_id')->withTrashed();
    }

    public function voucher()
    {
        return $this->belongsTo('Voucher', 'voucher_id')->withTrashed();
    }

    public function client()
    {
        //$voucher = $this->belongsTo('Voucher', 'voucher_id');

        //return $voucher->getResults()->belongsTo('Resident', 'client_id');
        return $this->belongsTo('Resident', 'client_id')->withTrashed();
    }

    public function beneficiary()
    {
        return $this->morphTo()->withTrashed();
    }

    

    public function account()
    {
        $voucher = $this->belongsTo('Voucher', 'voucher_id');

    	return $voucher->getResults()->belongsTo('TransactionAccount', 'account_type_id');
    }

    // public function category()
    // {
    // 	$subCategory = $this->belongsTo('TransactionSubCategory', 'sub_category_id')->withTrashed();
        
    //     return $subCategory->getResults()->belongsTo('TransactionCategory')->withTrashed();
    // }

    public function subCategory()
    {
        return $this->belongsTo('TransactionSubCategory')->withTrashed();
    }

    public function item()
    {
        return $this->belongsTo('TransactionItem', 'item_id');
    }

    public function liquidations()
    {
        return $this->hasMany('Liquidation');
    }

    public function creator()
    {
    	return $this->belongsTo('User', 'created_by')->withTrashed();
    }

    public function updater()
    {
    	return $this->belongsTo('User', 'modified_by')->withTrashed();
    }

    public function generateUuid()
    {
        $id = $this->idForTransaction();

        do {
            $this->uuid = "";
            if($this->voucher->account_type_id !== 25)
            {
                $this->uuid .= date("Ymd", strtotime($this->created_at));
                $this->uuid .= '-';
            }
            $this->uuid .= 'T';
            $this->uuid .= substr($this->account->name, 0, 2);
            $this->uuid .= str_pad($id, 5, "0", STR_PAD_LEFT);

            ++$id;
        } while (Transaction::where('uuid', $this->uuid)->count() > 0);

        
        $this->save();
    }

    private function idForTransaction()
    {
        $accountTypeId = $this->voucher->account_type_id;

        $id = Transaction::where('created_at', '>', date('Y-m-d', strtotime($this->created_at->__toString())) . ' 00:00:01')
            ->where('created_at', '<', date('Y-m-d', strtotime($this->created_at->__toString())) . '24:00:00')
            ->where('id', '!=', $this->id)
            ->whereHas('voucher', function($subQuery) use ($accountTypeId)
            {
                $subQuery->where('account_type_id', $accountTypeId);
            })
            ->count() + 1;

        return $id;
    }

    public function refund()
    {
        return $this->belongsTo('Transaction', 'refund_id');
    }

    public function vendor()
    {
        return $this->belongsTo('TransactionVendor', 'vendor_id');
    }
}