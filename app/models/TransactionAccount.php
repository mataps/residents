<?php

use SourceScript\Common\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class TransactionAccount extends BaseModel{

	use SoftDeletingTrait;

    protected $table = 'transaction_accounts';

    protected $guarded = array();

    function transactions()
    {
    	return $this->hasMany('Transaction', 'account_type_id', 'id');
    }

    function affiliation()
    {
    	return $this->belongsTo('Affiliation', 'affiliation_id');
    }
}