<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use SourceScript\Common\Models\BaseModel;

class TransactionCategory extends BaseModel {

    use SoftDeletingTrait;

    protected $table = 'transaction_categories';

    protected $guarded = array();

    function subCategories()
    {
        return $this->hasMany('TransactionSubCategory', 'category_id', 'id')->orderBy('name', 'asc');
    }

    function transactions()
    {
    	return $this->hasManyThrough('Transaction', 'TransactionSubCategory', 'category_id', 'sub_category_id');
    }

    function creator()
    {
        return $this->belongsTo('User', 'created_by');
    }

    function updater()
    {
        return $this->belongsTo('User', 'modified_by');
    }

}