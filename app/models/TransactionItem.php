<?php

use SourceScript\Common\Mixins\JoinableTrait;
use SourceScript\Common\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class TransactionItem extends BaseModel {
	
	use JoinableTrait, SoftDeletingTrait;

	protected $table = 'transaction_items';

	protected $guarded = [];

	function parent()
	{
		return $this->belongsTo('TransactionCategory', 'transaction_category_id');
	}

	function transactions()
	{
		return $this->hasMany('Transaction', 'category_item_id', 'id');
	}

	function creator()
	{
		return $this->belongsTo('User', 'created_by');
	}

	function updater()
	{
		return $this->belongsTo('User', 'modified_by');
	}

}