<?php

use SourceScript\Common\Mixins\JoinableTrait;
use SourceScript\Common\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class TransactionSubCategory extends BaseModel {

	use JoinableTrait, SoftDeletingTrait;

	protected $table = 'transaction_sub_categories';

	/**
	 * Fields fillable by the model
	 */
	protected $fillable = ['name', 'category_id'];

	function category()
	{
		return $this->belongsTo('TransactionCategory', 'category_id');
	}

	function transactions()
	{
		return $this->hasMany('Transaction', 'sub_category_id', 'id');
	}

	function creator()
    {
        return $this->belongsTo('User', 'created_by');
    }

    function updater()
    {
        return $this->belongsTo('User', 'modified_by');
    }
}