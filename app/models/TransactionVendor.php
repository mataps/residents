<?php 

use SourceScript\Common\Models\BaseModel;

class TransactionVendor extends BaseModel{

    protected $table = 'transaction_vendors';

    protected $guarded = array();

    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function cityMunicipality()
    {
        return $this->belongsTo('CityMunicipality', 'city_municipality_id');
    }


    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function district()
    {
        return $this->belongsTo('District', 'district_id');
    }

    /**
     * Eloquent Relation
     * 
     * @return Relation
     */
    public function barangay()
    {
        return $this->belongsTo('Barangay', 'barangay_id');
    }


    public function creator()
    {
    	return $this->belongsTo('User', 'created_by');
    }


    public function updater()
    {
    	return $this->belongsTo('User', 'modified_by');
    }
}