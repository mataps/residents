<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use SourceScript\Common\Mixins\JoinableTrait;
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use SourceScript\Common\Models\BaseModel;

class User extends BaseModel implements UserInterface, RemindableInterface {
	use UserTrait, RemindableTrait, JoinableTrait, SoftDeletingTrait;

    const GENDER_MALE = 'Male';
    const GENDER_FEMALE = 'Female';

    const SINGLE = 'Single';
    const MARRIED = 'Married';
    const DIVORCED = 'Divorced';
    const WIDOWED = 'Widowed';

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

    protected $guarded = array();

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');

    function group()
    {
        return $this->belongsTo('Group', 'group_id');
    }

    function resident()
    {
        return $this->belongsTo('Resident', 'resident_id');
    }

    function permissions()
    {
        return $this->belongsToMany('Permission', 'users_permissions')->where('status', '<>', '0')->withPivot('status');
    }

    function allPermissions()
    {
        return $this->belongsToMany('Permission', 'users_permissions')->withPivot('status');
    }
    function getAllowedTransactionAccounts()
    {
        $account_permissions = $this->permissions;
        $account_ids = [];
        foreach ($account_permissions as $permission) {
            $permission_action = explode(".", $permission['name']);
            $permission = str_replace("manage_transactions_", "", $permission_action[1]);
            $permission = str_replace("view_transactions_", "", $permission);


            if($permission_action[0] == "transaction_system"){
                // dd($permission_array);
               $permission_name =  str_replace("_", " ", $permission);
               $account_type = TransactionAccount::where('name', $permission_name)->first();
                if(!is_null($account_type)){
                    $account_type_id = $account_type->id;
                   array_push($account_ids, $account_type_id);
                }
                
            }
        }

        return $account_ids;
    }


    public function history()
    {
        return $this->hasMany('History');
    }
}
