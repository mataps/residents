<?php

use SourceScript\Common\Models\BaseModel;

class UserPermission extends BaseModel{

    protected $table = 'users_permissions';

    protected $guarded = array();
}