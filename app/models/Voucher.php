<?php

use Illuminate\Database\Eloquent\SoftDeletingTrait;
use SourceScript\Common\Mixins\JoinableTrait;
use SourceScript\Common\Models\BaseModel;

class Voucher extends BaseModel {

	use JoinableTrait, SoftDeletingTrait;

	/**
	 * Table Name
	 * 
	 * @var string
	 */
	protected $table = 'vouchers';


    /**
     *
     * 
     */
    protected $hidden = array('details');

	/**
	 * Guarded fields
	 * 
	 * @var array
	 */
    protected $guarded = array();


    public function client()
    {
        return $this->morphTo();
    }

    public function transactions()
    {
        return $this->hasMany('Transaction')->orderBy('transaction_type');
    }

    public function incomes()
    {
        return $this->hasMany('Transaction', 'voucher_id')->whereIn('transaction_type', ['income', 'transfer-income']);
    }

    public function expenses()
    {
        return $this->hasMany('Transaction', 'voucher_id')->whereIn('transaction_type', ['expense', 'transfer-expense']);
    }

    public function account()
    {
    	return $this->belongsTo('TransactionAccount', 'account_type_id');
    }

    public function creator()
    {
        return $this->belongsTo('User', 'created_by');
    }

    public function updater()
    {
        return $this->belongsTo('User', 'modified_by');
    }


    /**
     * Generates uuid for voucher
     * 
     * @return void
     */
    public function generateUuid()
    {
        $id = $this->idForVoucher();

        do {
            $this->uuid = "";
            if($this->account->fixed_voucher_number_format == 0)
            {
                $this->uuid .= date("Ymd", strtotime($this->created_at));
                $this->uuid .= '-';
            }
            $this->uuid .= 'V';
            $this->uuid .= substr($this->account->name, 0, 2);
            $this->uuid .= str_pad($id, 5, "0", STR_PAD_LEFT);

            ++$id;
            
        } while (Voucher::where('uuid', $this->uuid)->count() > 0);
        

        $this->save();
    }

    private function idForVoucher()
    {
        $id = Voucher::where('created_at', '>', date('Y-m-d', strtotime($this->created_at->__toString())) . ' 00:00:00')
            ->where('created_at', '<', date('Y-m-d', strtotime($this->created_at->__toString())) . '24:00:00')
            ->where('account_type_id', $this->account_type_id)
            ->where('id', '!=' ,$this->id)
            ->count() + 1;

        return $id;
    }
}