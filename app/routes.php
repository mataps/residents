<?php

DB::connection()->disableQueryLog();

// Event::listen('illuminate.query', function($query)
// {
//     echo ($query);
//     echo '<br>';
// });

Route::group(['prefix' => 'api/v1', 'before' => 'permissions'], function()
{
    Route::group(['prefix' => 'alerts'], function()
    {
        Route::get('liquidatables', 'Api_TransactionsController@liquidatables');        
    });

    /**
     * History endpoints
     *
     */
    
    Route::group(['prefix' => 'history'], function()
    {
        Route::get('{resource}/{id}', 'Api_HistoryController@resource');
    });

    Route::get('users/history/{users}', 'Api_HistoryController@userActivity');



    /**
     * User management profile routes
     */
    Route::put('account/password', 'Api_UsersController@changePassword');
    /**
     * User management profile routes
     */
    


    /**
     * User Management
     */
    Route::get('account', 'Api_UsersController@account');
    Route::get('users/deactivated', 'Api_UsersController@deactivated');
    Route::resource('users', 'Api_UsersController');
    Route::get('users/deactivated/{id}', 'Api_UsersController@deactivatedUserShow');
    Route::post('users/reactivate/{id}', 'Api_UsersController@deactivatedUserShow');
    Route::delete('users/{Users}/remove', 'Api_UsersController@removeUsers');
    Route::get('users/{users}/mergedPermissions', 'Api_UsersController@getMergedPermissions');

    Route::resource('groups.users', 'Api_GroupsUsersController');
    Route::resource('groups', 'Api_GroupsController');

    Route::delete('groups/{Groups}/remove', 'Api_GroupsController@removeGroups');

    Route::resource('permissions', 'Api_PermissionsController');
    /**
     * User Management
     */
    
    /**
     * Resident Mangement
     */
    Route::delete('residents/{Residents}/remove', 'Api_ResidentsController@removeResidents');
    Route::delete('attributions/{Attributions}/remove', 'Api_AttributionsController@massDelete');
    Route::delete('affiliations/{Affiliations}/remove', 'Api_AffiliationsController@massDelete');
    Route::delete('positions/{Positions}/remove', 'Api_PositionsController@massDelete');

    Route::get('residents/search/{fullname}', 'Api_ResidentsController@searchFullName');
    Route::post('residents/add/attribute/{residents}', 'Api_ResidentsController@addResidentAffiliation');
    Route::post('residents/add/attribution/{residents}', 'Api_ResidentsController@addResidentAttribution');
    Route::get('residents/attribute/{residents}', 'Api_ResidentsController@getResidentAffiliation');
    
    Route::get('residents/{residents}/lastnames', 'Api_ResidentsController@showLastNameCounts');
    Route::get('residents/MQTY/{residents}', 'Api_ResidentsController@showRelatedMQTY');
    Route::get('residents/LQTY/{residents}', 'Api_ResidentsController@showRelatedLQTY');
    Route::get('residents/reports/{residents}/print/pdf', 'Api_ResidentsController@getResidentsPdf');
    Route::get('residents/reports/{residents}/print/html', 'Api_ResidentsController@getResidentsHtml');
    Route::get('residents/reports/spreadsheet', 'Api_ResidentsController@residentsSpreadsheet');
    Route::get('affiliations/{affiliations}/cityMunicipality/{cities}/print/pdf', 'Api_AffiliationsController@printPdf');

    Route::delete('households/{Households}/remove', 'Api_HouseholdsController@massDelete');

    Route::resource('residents', 'Api_ResidentsController');
    Route::resource('households', 'Api_HouseholdsController');
    Route::resource('affiliations', 'Api_AffiliationsController');

    Route::get('attributions/search', 'Api_AttributionsController@customSearch');

    Route::resource('attributions', 'Api_AttributionsController');
    Route::resource('barangays', 'Api_BarangaysController');
    Route::resource('districts', 'Api_DistrictsController');
    Route::resource('citiesmunicipalities', 'Api_CitiesMunicipalities');
    Route::resource('positions', 'Api_PositionsController');

    

    Route::group(['prefix' => 'positions/{positions}'], function()
    {
        Route::resource('affiliations', 'Api_PositionsAffiliationsController', ['only' => ['index', 'store']]);
    });


    Route::group(['prefix' => 'affiliations/{Affiliations}'], function()
    {
        Route::post('residents', 'Api_AffiliationsResidentsController@massStore');
    });

    Route::group(['prefix' => 'affiliations/{affiliations}'], function()
    {
        Route::resource('positions', 'Api_AffiliationsPositionsController', ['only' => ['index', 'store']]);
        Route::resource('residents', 'Api_AffiliationsResidentsController', ['only' => ['index', 'store', 'update']]);
    });

    Route::group(['prefix' => 'households/{households}'], function()
    {
        Route::get('vouchers', 'Api_HouseholdsVouchersController@index');
        Route::get('affiliations', 'Api_HouseholdsAffiliationsController@index');
        Route::get('attributions', 'Api_HouseholdsAttributionsController@index');

        Route::resource('residents', 'Api_HouseholdsResidentsController', ['only' => ['index']]);
    });

    Route::group(['prefix' => 'residents/{Residents}'], function()
    {
        Route::post('affiliations', 'Api_ResidentsAffiliationsController@massStore');
        Route::post('attributions', 'Api_ResidentsAttributionsController@massStore');
        Route::post('households', 'Api_ResidentsHouseholdsController@massStore');
    });

    Route::group(['prefix' => 'attributions/{Attributions}'], function()
    {
        Route::post('residents', 'Api_AttributionsResidentsController@massStore');
    });


    Route::delete('residents/{id}/relatives/{relative}', 'Api_ResidentsRelationshipsController@destroy');

    Route::group(['prefix' => 'residents/{residents}'], function()
    {
        Route::resource('relatives', 'Api_ResidentsRelationshipsController', ['only' => ['index', 'store', 'destroy']]);
        Route::get('housemates', 'Api_ResidentsController@getResidentHousemates');
        Route::resource('affiliations', 'Api_ResidentsAffiliationsController', ['only' => ['index', 'update', 'store', 'destroy']]);
        Route::resource('households', 'Api_ResidentsHouseholdsController', ['only' => ['index', 'store', 'update', 'destroy']]);
        Route::resource('attributions', 'Api_ResidentsAttributionsController', ['only' => ['index', 'store']]);
    });

    // Route::put('residents/{residents}/affiliations/{affiliations}/residents_affiliations/{residents_affiliations}', 'Api_ResidentsAffiliationsController@update');

    Route::put('residents/{residents}/affiliations/{affiliations}', 'Api_ResidentsAffiliationsController@update');
    Route::put('residents/{residents}/households/{households}',     'Api_ResidentsHouseholdsController@update');




    Route::get('residents/{residents}/transactions', 'Api_ResidentsTransactionsController@index', ['only' => 'index']);
    Route::get('transactions/parameters', 'Api_TransactionParametersController@parameters');
    /**
     * Resident Management
     */



    /**
     * Transaction Management System
     */
    Route::get('account/{accounts}/transactions', 'Api_AccountsTransactionsController@index');
    Route::get('accounts/{Accounts}/transactions', 'Api_AccountsTransactionsController@multiple');

    Route::get('transactions/{SettledTransactions}/print/pdf', 'Api_TransactionsController@printTransactionsPdf');
    Route::get('transactions/cashflow/spreadsheet', 'Api_TransactionsController@cashflowReportsSpreadsheet');
    Route::get('transactions/cashflow/pdf', 'Api_TransactionsController@cashflowReportsPdf');

    Route::get('transactions/reports/spreadsheet', 'Api_TransactionsController@reportsSpreadsheet');
    Route::get('transactions/reports/pdf', 'Api_TransactionsController@reportsPdf');

    Route::get('transactions/liquidations/spreadsheet', 'Api_TransactionsController@liquidationReportsSpreadsheet');
    Route::get('transactions/liquidations/pdf', 'Api_TransactionsController@liquidationReportsPdf');
    Route::get('transactions/{transactions}/pdf', 'Api_TransactionsController@voucherPdf');

    Route::group(['prefix' => 'transactions/{transactions}'], function()
    {
        Route::get('voucher/pdf', 'Api_TransactionsController@voucherPdf');
        Route::resource('liquidations', 'Api_TransactionsLiquidationsController', ['only' => ['index', 'store', 'update', 'destroy']]);

        Route::delete('liquidations/{Liquidations}/remove', 'Api_TransactionsLiquidationsController@massDestroy');
    });
    

    Route::get('vouchers/{vouchers}/print/html', 'Api_VouchersController@voucherHtml');
    Route::get('vouchers/{vouchers}/print/pdf', 'Api_VouchersController@voucherPdf');
    Route::get('vouchers/{vouchers}/individual/print/pdf', 'Api_VouchersController@individualVoucherPdf');

    Route::resource('transactions/accounts', 'Api_TransactionAccountsController');
    Route::resource('transactions/categories', 'Api_TransactionCategoriesController');
    Route::resource('transactions/sub_categories', 'Api_TransactionSubCategoriesController');
    Route::resource('transactions/items', 'Api_TransactionItemsController');
    Route::delete('transactions/categories/{Categories}/remove', 'Api_TransactionCategoriesController@massDelete');

    Route::post('transactions/salary', 'Api_TransactionsController@salaryStore');
    Route::group(['prefix' => 'transactions/{transactions}'], function()
    {
        Route::post('liquidate', 'Api_TransactionsController@liquidate');
        Route::post('refund', 'Api_TransactionsController@refund');
        Route::put('override', 'Api_TransactionsController@overrideUpdate');
    });

    Route::post('residents/{Residents}/transactions', 'Api_TransactionsController@massStore');
    // Route::post('residents/{Residents}/salary', 'Api_TransactionsController@salaryStore');

    Route::group(['prefix' => 'vouchers/{vouchers}'], function()
    {
        Route::resource('transactions', 'Api_VouchersTransactionsController', ['only' => ['index', 'store', 'destroy']]);
    });

    Route::post('transactions/settle/{transactions}', 'Api_TransactionsController@settleAccount');

    Route::post('vouchers/{vouchers}/settle', 'Api_VouchersController@settle');

    Route::delete('transactions/{unsettledTransactions}/remove', 'Api_TransactionsController@remove');
    Route::resource('transactions', 'Api_TransactionsController');

    Route::resource('vouchers', 'Api_VouchersController');

    Route::resource('vendors', 'Api_VendorsController');

    Route::delete('transactions/sub_categories/{Sub_categories', 'Api_TransactionSubCategoriesController@massDestroy');
    Route::delete('vendors/{Vendors}/remove', 'Api_VendorsController@massDelete');
    /**
     * Transaction Management System
     */


    /**
     * ScholarshipsManagementSystem
     */
    Route::model('scholarship', 'Scholarship');

    Route::delete('scholarships/{scholarship}/refresh-relation/{relation}', 'Api_ScholarshipsController@refreshRelation');
    Route::post('scholarships/inactive', 'Api_ScholarshipsController@inactivate');
    Route::resource('scholarships',     'Api_ScholarshipsController',       ['except' => ['edit', 'create']]);

    Route::get('scholarships/{scholarships}/id', 'Api_ScholarshipsController@generateId');

    Route::get('scholarships/leaders/pdf', 'Api_ScholarshipsController@leaderListPdf');
    Route::get('scholarships/leaders/spreadsheet', 'Api_ScholarshipsController@leaderlistSpreadsheet');
    Route::get('scholarships/masterlist/spreadsheet', 'Api_ScholarshipsController@masterListScholarshipsSpreadsheet');
    Route::get('scholarships/beneficiaries/pdf', 'Api_ScholarshipsController@beneficiaryListPdf');
    Route::get('scholarships/beneficiaries/spreadsheet', 'Api_ScholarshipsController@beneficiarylistSpreadsheet');
    Route::get('scholarships/payroll/pdf', 'Api_ScholarshipsController@payrollListPdf');

    Route::resource('scholarshiptypes', 'Api_ScholarshipTypesController',   ['except' => 'edit', 'create']);
    Route::resource('schools',          'Api_SchoolsController',            ['except' => ['edit', 'create']]);
    Route::resource('awards',           'Api_AwardsController',             ['except' => ['edit', 'create']]);
    Route::resource('subjects',         'Api_SubjectsController',           ['except' => ['edit', 'create']]);
    Route::resource('policies',         'Api_AllowancePoliciesController',  ['except' => ['edit', 'create', 'update', 'delete']]);
    Route::resource('policiesoverrides', 'Api_AllowancePolicyOverridesController', ['except' => ['edit', 'create']]);
    Route::resource('allowances',       'Api_AllowancesController',         ['except' => ['edit', 'create', 'store', 'delete']]);
    Route::resource('courses',          'Api_CoursesController',            ['except' => ['edit', 'create']]);

    Route::group(['prefix' => 'releases/{releases}'], function()
    {
        Route::get('report/pdf', 'Api_AllowanceReleasesController@reportsPdf');
        Route::get('report/spreadsheet', 'Api_AllowanceReleasesController@reportsSpreadsheet');
        Route::get('semesters', 'Api_AllowanceReleasesSemestersController@index');
        Route::post('semesters/{Semesters}', 'Api_AllowanceReleasesSemestersController@bulkClaimAllowance');
        Route::resource('allowances', 'AllowanceReleasesAllowanceController', ['except' => ['edit', 'create', 'update']]);
        // Route::post('semesters/{semesters}', 'Api_AllowanceReleasesController@generate');
    });

    Route::get('semesters/reports/spreadsheet', 'Api_SemestersController@semestersSpreadsheet');
    

    Route::resource('releases', 'Api_AllowanceReleasesController',          ['except' => ['edit', 'create', 'update']]);

    Route::put('scholarships/{Scholarships}/bulk_status', 'Api_ScholarshipsController@bulkUpdateStatus');
    Route::put('scholarships/{scholarships}/status', 'Api_ScholarshipsController@updateStatus');
    Route::post('scholarships/{scholarships}/transfer', 'Api_ScholarshipsController@transfer');
    
    Route::delete('policiesoverrides/{PoliciesOverrides}/remove', 'Api_AllowancePolicyOverridesController@massDelete');
    Route::delete('schools/{Schools}/remove', 'Api_SchoolsController@massDelete');
    Route::delete('awards/{Awards}/remove', 'Api_SchoolsController@massDelete');
    Route::delete('subjects/{Subjects}/remove', 'Api_SchoolsController@massDelete');
    Route::delete('courses/{Courses}/remove', 'Api_CoursesController@massDelete');
    Route::delete('awards/{Awards}/remove', 'Api_AwardsController@remove');
    Route::delete('schools/{Schools}/remove', 'Api_SchoolsController@remove');
    Route::delete('subjects/{Subjects}/remove', 'Api_SubjectsController@remove');
    Route::delete('semesters/{Semesters}/remove', 'Api_SemestersController@massDestroy');
    Route::delete('scholarships/{Scholarships}/remove', 'Api_ScholarshipsController@massDelete');
    Route::delete('allowances/{Allowances}/remove', 'Api_AllowancesController@massDelete');


    Route::group(['prefix' => 'scholarships/{scholarships}'], function()
    {
        Route::get('allowances', 'Api_ScholarshipsAllowancesController@index');
        Route::resource('releases', 'Api_ScholarshipsAllowanceReleasesController', ['except' => ['edit', 'create', 'update']]);
        
        Route::delete('awards/{Awards}/remove', 'Api_ScholarshipsAwardsController@massDelete');
        Route::resource('awards',       'Api_ScholarshipsAwardsController',     ['except' => ['edit', 'create', 'update']]);
        Route::resource('schools',      'Api_ScholarshipsSchoolsController',    ['except' => ['edit', 'create', 'update']]);
        Route::resource('semesters',    'Api_ScholarshipsSemestersController',  ['except' => ['edit', 'create', 'update']]);
    });

    Route::group(['prefix' => 'semesters/{semesters}'], function()
    {
        Route::get('allowances', 'Api_SemestersAllowancesController@index');
        Route::get('awards', 'Api_AwardsController@semester');
        Route::post('releases/{releases}', 'Api_SemestersAllowanceReleasesController@generateAllowance');
        Route::resource('releases', 'Api_SemestersAllowanceReleasesController', ['only' => ['index']]);
        Route::resource('grades', 'Api_SemestersGradesController', ['except' => ['edit', 'create']]);
        Route::put('gwa', 'Api_SemestersController@updateGWA');
    });

    Route::resource('semesters', 'Api_SemestersController');


    /**
     * ScholarshipsManagementSystem
     */
    

    /**
     * Graph routes by poging jc
     */
    Route::get('cities', 'Api_GraphsController@cities');


    /**
     * History routes by poging JC
     */
    Route::get('history', 'Api_HistoryController@index');


    Route::get('sigplus', 'Api_Sigplus@showSigplus');
    Route::post('sigplus', 'Api_Sigplus@postSigplus');

});

Route::get('login', 'MainController@showLogin');
Route::post('login', 'MainController@postLogin');
Route::get('logout', 'MainController@logout');

Route::get('/{all}', 'MainController@showHomepage')->where('all', '^((?!api).)*$');