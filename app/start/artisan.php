<?php

/*
|--------------------------------------------------------------------------
| Register The Artisan Commands
|--------------------------------------------------------------------------
|
| Each available Artisan command must be registered with the console so
| that it is available to be called. We'll register every command so
| the console gets access to each of the command object instances.
|
*/


Artisan::add(App::make('CalculateLastNames'));
Artisan::add(App::make('ElasticSearchIndex'));
Artisan::add(App::make('PullResidents'));
Artisan::add(App::make('AssignAffiliation'));
Artisan::add(App::make('PullTransactions'));
Artisan::add(App::make('PullScholars'));
Artisan::add(App::make('MigrateHousehold'));
Artisan::add(App::make('ResidentNotesMigrate'));
Artisan::add(App::make('MigrateAffiliationFromExcel'));
Artisan::add(App::make('SetBarangayNames'));
Artisan::add(App::make('ConvertScholarImage'));
Artisan::add(App::make('RefreshTransactions'));
Artisan::add(App::make('RefreshSemesters'));
Artisan::add(App::make('FixPrecint'));
Artisan::add(App::make('FixProbationary'));
Artisan::add(App::make('FixReferrer'));
Artisan::add(App::make('AllowanceReset'));
Artisan::add(App::make('SchoolLevelFix'));
Artisan::add(App::make('SetUuid'));
Artisan::add(App::make('FixBarangay'));
Artisan::add(App::make('FixApplied'));
Artisan::add(App::make('FixBarangayMismatch'));
Artisan::add(App::make('FixSchoolLevel'));
Artisan::add(App::make('FixSubjectCount'));
Artisan::add(App::make('MigrateHouseholdAccess'));
Artisan::add(new HistoryTables);

