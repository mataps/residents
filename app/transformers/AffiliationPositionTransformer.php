<?php

class AffiliationPositionTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' => '',
		'position' => 'transformPosition',
		'affiliation' => 'transformAffiliation'
	];

	function __construct(PositionTransformer $positionTransformer, AffiliationTransformer $affiliationTransformer)
	{
		$this->positionTransformer = $positionTransformer;
		$this->affiliationTransformer = $affiliationTransformer;
	}


	public function getTransformPosition($position)
	{
		return $this->positionTransformer->transform($position);
	}

	public function getTransformAffiliation($affiliation)
	{
		return $this->affiliationTransformer->transform($affiliation);
	}
}