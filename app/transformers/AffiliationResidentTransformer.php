<?php
class AffiliationResidentTransformer extends AbstractBaseTransformer {


	protected $fields = [
		'id' 				=> '',
        'name' 				=> '',
        'head'              => 'transformHead',
        'referrer'          => 'transformReferrer',
        'from'              => '',
        'to'                => '',
        'position'          => 'transformPosition',
        'founder' 			=> 'boolean',
        'founded' 			=> '',
        'district' 			=> 'transformAddress',
        'cityMunicipality' 	=> 'transformAddress',
        'barangay' 			=> 'transformAddress',
        'street' 			=> '',
        'description' 		=> '',
        'created_at' 		=> 'string',
        'updated_at' 		=> 'string'
	];

	/**
     * @var ResidentTransformer
     */
    private $residentTransformer;

    /**
     * @var UserTransformer
     */
    private $userTransformer;

	function __construct(UserTransformer $userTransformer,
        ResidentTransformer $residentTransformer)
    {
        $this->userTransformer = $userTransformer;
        $this->residentTransformer = $residentTransformer;
    }

	public function getTransformAddress($address)
	{
		return isset($address) ? $address->name : '';
	}

	public function getTransformHead()
    {
        $resident = Resident::find($this->model->head_id);
        $except = ['updated_at', 'created_at', 'updater', 'creator', 'precint', 'gender', 'district', 'cityMunicipality', 'barangay', 'street', 'email', 'mobile', 'phone', 'birthdate', 'civil_status'];
        if($resident)
        {
            return $this->residentTransformer->transform($resident, $except);
        }
        return '';
    }

    public function getTransformReferrer()
    {
        $resident = Resident::find($this->model->referrer_id);
        $except = ['updated_at', 'created_at', 'updater', 'creator', 'precint', 'gender', 'district', 'cityMunicipality', 'barangay', 'street', 'email', 'mobile', 'phone', 'birthdate', 'civil_status'];
        if($resident)
        {
            return $this->residentTransformer->transform($resident, $except);
        }
        return '';
    }

    /**
     * Transform position
     * 
     * @return array
     */
    public function getTransformPosition()
    {
        $position = Position::find($this->model->position_id);
        if($position)
        {
            return [
                'id' => $position->id,
                'name' => $position->name
            ];
        }
        return null;
    }
}