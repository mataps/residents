<?php

class AffiliationTransformer extends AbstractBaseTransformer{

    /**
     * Fields to be displayed
     * 
     * @var array
     */
    protected $fields = array(
        'id' => '',
        'name' => '',
        'founders_count' => 'foundersCount',
        'founded' => '',
        'district' => 'transformAddress',
        'cityMunicipality' => 'transformAddress',
        'barangay' => 'transformAddress',
        'street' => '',
        'members_count' => 'membersCount',
        'description' => '',
        'created_at' => 'string',
        'updated_at' => 'string'
    );

    /**
     * @var
     */
    private $residentTransformer;

    function __construct(ResidentTransformer $residentTransformer)
    {
        $this->residentTransformer = $residentTransformer;
    }

    function getFoundersCount()
    {
        return $this->model->founders->count();
    }

    function getMembersCount()
    {
        return $this->model->members->count();
    }

    function getTransformResidents($residents)
    {
        $data = [];
        foreach ($residents as $resident)
        {
            $data[] = array_add($this->transformResident($resident), 'residents_affiliations_id', $resident->pivot->id);
        }

        return $data;
    }

    function transformResident($resident)
    {
        return isset($resident) ? $this->residentTransformer->transform($resident) : '';
    }

    function getTransformAddress($address)
    {
        return ! isset ($address) ? '' : $address->name;
    }
}