<?php

class AllowancePolicyFormattedTransformer extends AbstractBaseTransformer {

    /**
     * Fields to be displayed
     * 
     * @var array
     */
    protected $fields = array(
        'id'                    => '',
        'name'                  => '',
        'allowance_policies'    => 'transformAllowancePolicies',
        );


    /**
     * Transform Policies
     * 
     * @param  $policies
     * @return array
     */
    public function getTransformAllowancePolicies($policies)
    {
        if($policies->count() > 0)
        {    
            foreach($policies as $index => $policy)
            {
                $data[$policy->school_level][$policy->school_type][] = array(
                    'id' => $policy->id,
                    'school_level' => $policy->school_level,
                    'school_type' => $policy->school_type,
                    'from' => $policy->from,
                    'to' => $policy->to,
                    'amount' => $policy->amount,
                    'scholarship_type_id' => $policy->scholarship_type_id
                    );
            }
            return $data;
        }
        else
        {
            $data = [
                'secondary'      => [
                    'public'    => [],
                    'private'   => []
                ],
                'collegiate'    => [
                    'public'    => [],
                    'private'   => []
                ]
            ];
            
            return $data;
        }
    }
}