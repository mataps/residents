<?php

use SourceScript\Common\Collections\ResultCollection;

class AllowancePolicyOverrideTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' 					=> '',
		'scholarship_types' 	=> 'transformScholarshipTypes',
		'creator' 				=> 'transformUser',
		'updater' 				=> 'transformUser'
	];

	/**
	 * @var ScholarshipTypeTransformer
	 */
	private $scholarshipTypeTransformer;


	/**
	 * @var UserTransformer
	 */
	private $userTransformer;


	function __construct(
		ScholarshipTypeTransformer $scholarshipTypeTransformer,
		UserTransformer $userTransformer)
	{
		$this->scholarshipTypeTransformer = $scholarshipTypeTransformer;
		$this->userTransformer = $userTransformer;
	}


	/**
	 * @param  ResultCollection $scholarshipTypes
	 * @return array
	 */
	public function getTransformScholarshipTypes(ResultCollection $scholarshipTypes)
	{
		return $this->scholarshipTypeTransformer = $scholarshipTypes;
	}


	/**
	 * @param  User   $user
	 * @return array
	 */
	public function getTransformUser(User $user)
	{
		$except = ['group', 'permissions', 'created_at', 'updated_at'];

		return $this->userTransformer->transform($user, $except);
	}
}