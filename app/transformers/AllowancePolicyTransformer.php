<?php

class AllowancePolicyTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' => '',
		'scholarship_type' => 'transformScholarshipType',
		'school_type' => '',
		'school_level' => '',
		'from' => '',
		'to' => '',
		'amount' => '',
		'creator' => 'transformUser',
		'updater' => 'transformUser'
	];


	/**
	 * @var ScholarshipTypeTransformer
	 */
	private $scholarshipTypeTransformer;


	/**
	 * @var UserTransformer
	 */
	private $userTransformer;


	function __construct(ScholarshipTypeTransformer $scholarshipTypeTransformer, UserTransformer $userTransformer)
	{
		$this->scholarshipTypeTransformer = $scholarshipTypeTransformer;
		$this->userTransformer = $userTransformer;
	}


	/**
	 * @param  ScholarshipType $scholarshipType
	 * @return array
	 */
	public function getTransformScholarshipType(ScholarshipType $scholarshipType)
	{
		return $this->scholarshipTypeTransformer->transform($scholarshipType);
	}


	/**
	 * @param  User $user
	 * @return array
	 */
	public function getTransformUser(User $user)
	{
		return $this->userTransformer->transform($user);
	}
}