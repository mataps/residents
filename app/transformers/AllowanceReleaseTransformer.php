<?php

use SourceScript\Common\Collections\ResultCollection;

class AllowanceReleaseTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' 					=> '',
		'uuid' 					=> '',
		'term' 					=> '',
		'school_year' 			=> '',
		'fixed_amount' 			=> 'boolean',
		'amount' 				=> '',
		'expiring_at' 			=> 'string',
		'scholarship_types' 	=> 'transformScholarshipTypes',
		'creator' 				=> 'transformUser',
		'updater' 				=> 'transformUser'
	];


	/**
	 * @var UserTransformer
	 */
	private $userTransformer;


	/**
	 * @var ScholarshipTypeTransformer
	 */
	private $scholarshipTypeTransformer;


	function __construct(
		UserTransformer $userTransformer,
		ScholarshipTypeTransformer $scholarshipTypeTransformer)
	{
		$this->userTransformer = $userTransformer;
		$this->scholarshipTypeTransformer = $scholarshipTypeTransformer;
	}


	/**
	 * Transform user
	 * @return array
	 */
	public function getTransformUser(User $user)
	{
		$except = ['permissions', 'group'];

		return isset($user) ? $this->userTransformer->transform($user, $except) : '';
	}


	public function getTransformScholarshipTypes(ResultCollection $scholarshipTypes)
	{
		return $this->scholarshipTypeTransformer->transformCollection($scholarshipTypes);
	}
}