<?php

class AllowanceTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
    protected $fields = [
        'id' 			=> '',
        'amount'        => '',
        'release'       => 'transformAllowanceRelease',
        'scholarship' 	=> 'transformScholarship',
        'policy' 		=> 'transformPolicy',
        'semester' 		=> 'transformSemester',    
        'stage' 		=> '',
        'creator' 		=> 'transformUser',
        'updater' 		=> 'transformUser',
        'created_at' 	=> 'string',
        'updated_at' 	=> 'string'
    ];


    /**
     * @var UserTransformer
     */
    private $userTransformer;


    /**
     * @var AllowanceReleaseTransformer
     */
    private $allowanceReleaseTransformer;


    function __construct(
    	UserTransformer 			$userTransformer,
    	ScholarshipTransformer 		$scholarshipTransformer,
    	AllowancePolicyTransformer 	$policyTransformer,
    	SemesterTransformer 		$semesterTransformer,
        AllowanceReleaseTransformer $allowanceReleaseTransformer)
    {
    	$this->userTransformer 			= $userTransformer;
    	$this->scholarshipTransformer 	= $scholarshipTransformer;
    	$this->policyTransformer 		= $policyTransformer;
    	$this->semesterTransformer 		= $semesterTransformer;
        $this->allowanceRelease         = $allowanceReleaseTransformer;
    }


    /**
     * Allowance release transformer
     * 
     * @param  AllowanceRelease $allowanceRelease
     * @return array
     */
    function getTransformAllowanceRelease(AllowanceRelease $allowanceRelease)
    {
        return $this->allowanceRelease->transform($allowanceRelease);
    }


    /**
     * User transformer
     * 
     * @param  User   $user
     * @return array
     */
    function getTransformUser(User $user)
    {
        $except = ['group', 'created_at', 'updated_at', 'permissions'];

    	return isset($user) ? $this->userTransformer->transform($user, $except) : '';
    }


    /**
     * Scholarship transformer
     * 
     * @param  Scholarship $scholarship
     * @return array
     */
    function getTransformScholarship(Scholarship $scholarship)
    {
    	$except = [
            'semesters',
    		'creator',
    		'updater'
    	];

    	return isset($scholarship) ? $this->scholarshipTransformer->transform($scholarship, $except) : '';
    }


    /**
     * Policy transformer
     * 
     * @param  Policy $policy
     * @return array
     */
    function getTransformPolicy($policy)
    {
    	$except = [
            'scholarship_type',
			'school_type',
			'school_level',
			'created_at',
			'updated_at',
			'creator',
			'updater'];

    	return isset($policy) ? $this->policyTransformer->transform($policy, $except) : '';
    }


    /**
     * Transform semester
     * 
     * @param  Semester $semester
     * @return array
     */
    function getTransformSemester(Semester $semester)
    {
    	$except = [
    		'scholarship',
    		'school',
    		'from',
    		'to',
    		'school_year',
    		'term',
    		'gwa',
    		'comment',
    		'created_at',
    		'updated_at',
    		'creator',
    		'updater'];

    	return isset($semester) ? $this->semesterTransformer->transform($semester, $except) : '';
    }
}