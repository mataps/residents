<?php

class AttributionTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' 			=> '',
		'name' 			=> '',
		'class' 		=> '',
		'creator' 		=> 'transformUser',
		'updater' 		=> 'transformUser',
		'created_at' 	=> '',
		'updated_at' 	=> ''
	];

	
	/**
	 * @var UserTransformer
	 */
	private $userTransformer;

	function __construct(UserTransformer $userTransformer)
	{
		$this->userTransformer = $userTransformer;
	}

	/**
	 * @param  User   $user
	 * @return array
	 */
	public function getTransformUser($user)
	{
		$except = ['group', 'created_at', 'updated_at', 'permissions', 'creator', 'updater'];

		return isset($user) ? $this->userTransformer->transform($user, $except) : '';
	}
}