<?php

use SourceScript\Common\Collections\ResultCollection;

class AwardTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
    protected $fields = [
        'id'            => '',
        'name'          => '',
        'description'   => '',
        'scholarships'  => 'scholarshipCount',
        'attributions'  => 'transformAttribution',
        'creator'       => 'transformUser',
        'updater'       => 'transformUser',
        'updated_at'    => 'string',
        'created_at'    => 'string'
    ];


    /**
     * @var UserTransformer
     */
    private $userTransformer;

    /**
     * @var AttributionTransformer
     */
    private $attributionTransformer;

    function __construct(UserTransformer $userTransformer, AttributionTransformer $attributionTransformer)
    {
    	$this->userTransformer = $userTransformer;
        $this->attributionTransformer = $attributionTransformer;
    }

    /**
     * User transformer
     * 
     * @param  User   $user
     * @return array
     */
    function getTransformUser($user)
    {
        $except = ['group', 'created_at', 'updated_at'];

    	return isset($user) ? $this->userTransformer->transform($user, $except) : '';
    }

    function getScholarshipCount($scholarships)
    {
        return $scholarships->count();
    }

    /**
     * Attributions transformer
     * 
     * @param  ResultCollection $attributions
     * @return array
     */
    function getTransformAttribution(ResultCollection $attributions)
    {
        return $this->attributionTransformer->transformCollection($attributions);
    }

}