<?php

class BarangayTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 *
	 * @var array
	 */
	protected $fields = [
		'id' 			=> '',
		'name' 			=> '',
		'created_at' 	=> 'string',
		'updated_at' 	=> 'string',
		'cityMunicipality' => 'transformCityMunicipality'
	];


	/**
	 * @var DistrictTransformer
	 */
	private $districtTransformer;

	private $cityMunicipalityTransformer;

	function __construct(DistrictTransformer $districtTransformer, CityMunicipalityTransformer $cityMunicipalityTransformer)
	{
		$this->districtTransformer = $districtTransformer;
		$this->cityMunicipalityTransformer = $cityMunicipalityTransformer;
	}

	public function getTransformCityMunicipality($city)
	{
		return isset($city) ? $this->cityMunicipalityTransformer->transform($city) : '';
	}

	/**
	 * @param  District $district
	 * @return array
	 */
	public function getDistrictTransformer($district)
	{
		return isset($district) ? $this->districtTransformer->transform($district) : '' ;
	}


}
