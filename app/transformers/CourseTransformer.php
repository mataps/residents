<?php

class CourseTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' => '',
		'name' => '',
		'abbreviation' => '',
		'creator' => 'transformUser',
		'updater' => 'transformUser',
		'created_at' => 'string',
		'updated_at' => 'string'
	];


	/**
	 * @var UserTransformer
	 */
	private $userTransformer;


	function __construct(UserTransformer $userTransformer)
	{
		$this->userTransformer = $userTransformer;
	}

	/**
	 * Transform user
	 * 
	 * @param  User   $user
	 * @return array
	 */
	public function getTransformUser(User $user)
	{
		$except = ['group', 'permissions', 'created_at', 'updated_at'];

		return isset($user) ? $this->userTransformer->transform($user, $except) : '';
	}
}