<?php

class DistrictTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' 	=> '',
		'name' 	=> 'string'
	];
}