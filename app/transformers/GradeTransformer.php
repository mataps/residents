<?php

class GradeTransformer extends AbstractBaseTransformer {


	protected $fields = [
		'id' => '',
		'grade' => 'double',
		'subject' => 'transformSubject',
		'units' => ''
	];

	/**
	 * @var SubjectTransformer
	 */
	private $subjectTransformer;

	function __construct(SubjectTransformer $subjectTransformer)
	{	
		$this->subjectTransformer = $subjectTransformer;
	}


	public function getTransformSubject($subject)
	{
		return isset($subject) ? $this->subjectTransformer->transform($subject) : '';
	}


}