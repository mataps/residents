<?php

class GraphCitiesTransformer extends AbstractBaseTransformer {

	protected $fields = array(
        'id' => 'integer',
        'name' => '',
        'residents' => 'transformResidents'
    );

	function getTransformResidents($residents)
	{
		return count($residents);
	}
}