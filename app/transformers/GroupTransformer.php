<?php

class GroupTransformer extends AbstractBaseTransformer{

    /**
     * @var
     */
    private $permissionTransformer;

    protected $fields = array(
        'id' => 'integer',
        'name' => '',
        'permissions' => 'permissionsTransform',
        'description' => '',
        'updated_at' => 'string',
        'created_at' => 'string',
        'available_start' => '',
        'available_end' => ''
    );

    function __construct(PermissionTransformer $permissionTransformer)
    {
        $this->permissionTransformer = $permissionTransformer;
    }


    function getPermissionsTransform($permissions)
    {
        return $permissions ? array_map([$this, 'transformPermission'], $permissions->all()) : [];
    }

    function transformPermission($permission)
    {
        return [
            'id' => $permission->id,
            'name' => $permission->name,
            'description' => $permission->description,
            'created_at' => (string) $permission->created_at,
            'updated_at' => (string) $permission->updated_at,
            'status' => $permission->pivot->status
        ];
    }
}