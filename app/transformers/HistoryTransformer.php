<?php

class HistoryTransformer extends AbstractBaseTransformer {


	protected $fields = [
		'id' => '',
		'action' => '',
		'resource_type' => '',
		'user' => 'transformUser',
		'resource' => 'transformResource',
		'created_at' => 'string',
		'updated_at' => 'string'
	];


	/**
	 * @var UserTransformer
	 */
	private $userTransformer;


	function __construct(UserTransformer $userTransformer)
	{
		$this->userTransformer = $userTransformer;
	}


	/**
     * User transformer
     * 
     * @param  User $user
     * @return array
     */
    public function getTransformUser(User $user)
    {
        $except = ['group', 'created_at', 'updated_at', 'permissions'];
        
        return ! isset($user) ? '' : $this->userTransformer->transform($user, $except);
    }

    public function getTransformResource($history)
    {
		$model = new $history->resource_type;

		$model->setTable('history_' . $model->getTable());

		return $model->find($history->history_id);
    }

}