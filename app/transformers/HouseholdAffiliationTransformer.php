<?php

class HouseholdAffiliationTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' 			=> '',
		'resident' 		=> 'transformResident',
		'affiliation' 	=> 'transformAffiliation',
		'position' 		=> '',
		'status' 		=> '',
		'remarks' 		=> '',
		'from' 			=> '',
		'to' 			=> '',
		'updater' 		=> 'transformUser',
		'creator' 		=> 'transformUser',
		'created_at' 	=> 'string',
		'updated_at' 	=> 'string'
	];


	/**
	 * @var ResidentTransformer
	 */
	private $residentTransformer;

	/**
	 * @var AffiliationTransformer
	 */
	private $affiliationTransformer;


	/**
	 * @var UserTransformer
	 */
	private $userTransformer;

	function __construct(ResidentTransformer $residentTransformer, AffiliationTransformer $affiliationTransformer, UserTransformer $userTransformer)
	{
		$this->residentTransformer = $residentTransformer;
		$this->affiliationTransformer = $affiliationTransformer;
		$this->userTransformer = $userTransformer;
	}

	/**
	 * Transform a user.
	 * 
	 * @param  User   $user
	 * @return array
	 */
	public function getTransformUser(User $user)
	{
		return $this->userTransformer->transform($user);
	}


	/**
	 * Transform a resident.
	 * 
	 * @param  Resident $resident
	 * @return array
	 */
	public function getTransformResident(Resident $resident)
	{
		return $this->residentTransformer->transform($resident);
	}


	/**
	 * Transform an affiliation.
	 * 
	 * @param  Affiliation $affiliation
	 * @return array
	 */
	public function getTransformAffiliation(Affiliation $affiliation)
	{
		return $this->affiliationTransformer->transform($affiliation);
	}
}