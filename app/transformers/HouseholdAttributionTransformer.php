<?php

class HouseholdAttributionTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' 			=> '',
		'resident' 		=> 'transformResident',
		'attribution' 	=> 'transformAttribution',
		'from' 			=> 'string',
		'to' 			=> 'string'
	];


	/**
	 * @var ResidentTransformer
	 */
	private $residentTransformer;


	/**
	 * @var AttributionTransformer
	 */
	private $attributionTransformer;


	function __construct(
		ResidentTransformer $residentTransformer,
		AttributionTransformer $attributionTransformer)
	{
		$this->residentTransformer = $residentTransformer;
		$this->attributionTransformer = $attributionTransformer;
	}


	/**
	 * Transform a resident.
	 * 
	 * @param  Resident $resident
	 * @return array
	 */
	public function getTransformResident(Resident $resident)
	{
		return $this->residentTransformer->transform($resident);
	}


	/**
	 * Transforms an attribution.
	 * 
	 * @param  Attribution $attribution
	 * @return array
	 */
	public function getTransformAttribution(Attribution $attribution)
	{
		return $this->attributionTransformer->transform($attribution);
	}
}