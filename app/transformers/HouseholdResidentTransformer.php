<?php

class HouseholdResidentTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' => '',
		'resident_id' => '',
		'household_id' => '',
		'role' => '',
		'from' => '',
		'to' => '',
		'voters_id' => '',
        'is_voter' => '',
        'last_name' => '',
        'first_name' => '',
        'middle_name' => '',
        'nickname' => '',
        'occupation' => '',
        'full_name' => '',
        'profilePic' => 'transformPhoto',
        'email' => '',
        'mobile' => '',
        'mobile_1' => '',
        'mobile_2' => '',
        'phone' => '',
        'precint' => '',
        'birthdate' => '',
        'gender' => '',
        'civil_status' => '',
        'district' => 'transformAddress',
        'cityMunicipality' => 'transformAddress',
        'barangay' => 'transformAddress',
        'street' => '',
        'attributes' => '',
        'creator' => 'transformUser',
        'updater' => 'transformUser',
        'blacklisted_at' => '',
        'deceased_at' => '',
        'updated_at' => 'string',
        'created_at' => 'string',
	];


    /**
     * @var
     */
    private $userTransformer;
    /**
     * @var PhotoTransformer
     */
    private $photoTransformer;

    function __construct(UserTransformer $userTransformer, PhotoTransformer $photoTransformer)
    {
        $this->userTransformer = $userTransformer;
        $this->photoTransformer = $photoTransformer;
    }

    function getTransformPhoto($photo)
    {
        return ! isset($photo) ? '' : URL::to('uploads') . '/' . str_replace(" ", "",Str::title($this->model->cityMunicipality->name)) . '/photos/' . $photo->filename;
    }

    function getTransformUser($user)
    {
        $except = ['group', 'permissions', 'created_at', 'updated_at'];

        return ! isset($user) ? '' : $this->userTransformer->transform($user, $except);
    }

    function getTransformAddress($address)
    {
        return ! isset ($address) ? '' : $address->name;
    }

    function getTransformHistory($history)
    {
        return $history;
    }

}