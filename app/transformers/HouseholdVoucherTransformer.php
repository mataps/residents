<?php

class HouseholdVoucherTransformer extends AbstractBaseTransformer {


	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' 			=> '',
		'uuid' 			=> '',
		'client' 		=> 'transformResident',
		'status' 		=> '',
		'remarks' 		=> '',
		'details' 		=> '',
		'amount' 		=> 'transformAmount',
		'settled_at' 	=> 'string',
		'creator' 		=> 'transformUser',
		'updater' 		=> 'transformUser',
		'created_at' 	=> 'string',
		'updated_at' 	=> 'string'
	];


	/**
	 * @var ResidentTransformer
	 */
	private $residentTransformer;


	/**
	 * @var UserTransformer
	 */
	private $userTransformer;

	function __construct(ResidentTransformer $residentTransformer, UserTransformer $userTransformer)
	{
		$this->residentTransformer = $residentTransformer;
		$this->userTransformer = $userTransformer;
	}

	/**
	 * @param  Resident $resident
	 * @return array
	 */
	public function getTransformResident(Resident $resident)
	{
		return $this->residentTransformer->transform($resident);
	}


	/**
	 * @return string
	 */
	public function getTransformAmount()
	{
		return $this->model->transactions()->sum('amount');
	}


	/**
	 * @param  User   $user
	 * @return array
	 */
	public function getTransformUser(User $user)
	{
		return $this->userTransformer->transform($user);
	}
}