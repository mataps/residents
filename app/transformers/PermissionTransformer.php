<?php

use Illuminate\Database\Eloquent\Model;

class PermissionTransformer extends AbstractBaseTransformer {

    public function transform(Model $model, $except = array())
    {
        return [
            'id' => $model->id,
            'component' => $this->getComponentName($model->name),
            'functionality' => $this->getFunctionality($model->name),
            'description' => $model->description
        ];
    }

    private function getComponentName($name)
    {
        list($component, $functionality) = explode('.', $name);
        return ucwords(str_replace('_', ' ', $component));
    }

    private function getFunctionality($name)
    {
        list($component, $functionality) = explode('.', $name);
        return ucfirst(str_replace('_', ' ', $functionality));
    }
} 