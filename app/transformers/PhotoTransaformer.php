<?php

use Illuminate\Database\Eloquent\Model;

class PhotoTransformer extends AbstractBaseTransformer{

    public function transform(Model $model, $except = array())
    {
        return 'uploads/' . $model->filename;
    }
} 