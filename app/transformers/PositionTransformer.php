<?php

use SourceScript\Common\Collections\ResultCollection;

class PositionTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' 		=> '',
		'name' 		=> '',
		'attributions' => 'transformAttributions',
		'creator' 	=> 'transformUser',
		'updater' 	=> 'transformUser'
	];


	function __construct(UserTransformer $userTransformer, AttributionTransformer $attributionTransformer)
	{
		$this->userTransformer = $userTransformer;
		$this->attributionTransformer = $attributionTransformer;
	}


	/**
	 * User transformer
	 * 
	 * @param  User   $user
	 * @return array
	 */
	public function getTransformUser(User $user)
	{
		$except = ['updated_at', 'created_at', 'permissions', 'group', 'updater', 'creator'];

		return $this->userTransformer->transform($user, $except);
	}


	/**
	 * Attributions transformer
	 * 
	 * @param  ResultCollection $attributions
	 * @return array
	 */
	public function getTransformAttributions(ResultCollection $attributions)
	{
		return $this->attributionTransformer->transformCollection($attributions);
	}
}