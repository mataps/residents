<?php

class RelativeTransformer extends AbstractBaseTransformer {


	/**
     * Fields to be displayed
     * 
     * @var array
     */
    protected $fields = array(
        'id'                => '',
        'resident_id'       => 'transformResidentId',
        'relation' 			=> '',
        'voters_id'         => '',
        'is_voter'          => 'boolean',
        'last_name'         => '',
        'first_name'        => '',
        'middle_name'       => '',
        'full_name'         => '',
        'profilePic'        => 'transformPhoto',
        'email'             => '',
        'mobile'            => '',
        'phone'             => '',
        'precint'           => '',
        'birthdate'         => '',
        'gender'            => '',
        'civil_status'      => '',
        'district'          => 'transformAddress',
        'cityMunicipality'  => 'transformAddress',
        'barangay'          => 'transformAddress',
        'street'            => '',
        'attributes'        => '',
        'creator'           => 'transformUser',
        'updater'           => 'transformUser',
        'blacklisted_at'    => '',
        'deceased_at'       => '',
        'updated_at'        => 'string',
        'created_at'        => 'string'
    );


	/**
     * @var UserTransformer
     */
    private $userTransformer;


    /**
     * @var PhotoTransformer
     */
    private $photoTransformer;


    /**
     * @var ResidentTransformer
     */
    private $residentTransformer;


    function __construct(
        UserTransformer $userTransformer,
        PhotoTransformer $photoTransformer)
    {
        $this->userTransformer = $userTransformer;
        $this->photoTransformer = $photoTransformer;
    }


    /**
     * Transform photo
     * 
     * @param  Photo $photo
     * @return string
     */
    public function getTransformPhoto($photo)
    {
        return ! isset($photo) ? '' : URL::to('uploads') . '/' . str_replace(" ", "",Str::title($this->model->cityMunicipality->name)) . '/photos/' . $photo->filename;
    }


    /**
     * Transform user
     * 
     * @param  User $user
     * @return array
     */
    public function getTransformUser($user)
    {
        $except = ['group', 'permissions', 'created_at', 'updated_at'];

        return ! isset($user) ? '' : $this->userTransformer->transform($user, $except);
    }


    /**
     * Transform address
     * 
     * @param  Address $address
     * @return string
     */
    public function getTransformAddress($address)
    {
        return ! isset ($address) ? '' : $address->name;
    }


    /**
     * Transform resident
     * 
     * @param  Resident $resident
     * @return array
     */
    public function getTransformResident($residents)
    {
        $data = [];

        foreach($residents as $index => $resident)
        {
            $data[$index]['id']           = $resident->id;
            $data[$index]['relation']     = $resident->pivot->relation;
            $data[$index]['voters_id']    = $resident->voters_id;
            $data[$index]['last_name']    = $resident->last_name;
            $data[$index]['first_name']   = $resident->first_name;
            $data[$index]['middle_name']  = $resident->middle_name;
            $data[$index]['full_name']    = $resident->full_name;
            $data[$index]['profilePic']   = $this->getTransformPhoto($resident->photo);
            $data[$index]['email']        = $resident->email;
            $data[$index]['birthdate']    = $resident->birthdate;
        }

        return $data;
    }
}