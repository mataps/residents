<?php

use SourceScript\Profiling\Repositories\PositionRepositoryInterface;

class ResidentAffiliationTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be dispalyed
	 * 
	 * @var array
	 */
	protected $fields = [
        'id'                => '',
        'head'              => 'transformHead',
        'referrer'          => 'transformReferrer',
        'founder'           => 'boolean',
        'from'              => '',
        'to'                => '',
        'position'          => 'transformPosition',
        'last_name'         => '',
        'first_name'        => '',
        'middle_name'       => '',
        'full_name'         => '',
        'profilePic'        => 'transformPhoto',
        'district'          => 'transformAddress',
        'cityMunicipality'  => 'transformAddress',
        'barangay'          => 'transformAddress',
        'street'            => '',
        'email'             => '',
        'mobile'            => '',
        'phone'             => '',
        'precint'           => '',
        'birthdate'         => '',
        'gender'            => '',
        'civil_status'      => '',
        'atm_number'        => '',
        'bank'              => '',
        'blacklisted_at'    => '',
        'deceased_at'       => '',
        'updated_at'        => 'string',
        'created_at'        => 'string',
        'creator'           => 'transformUser',
        'updater'           => 'transformUser',
	];

    /**
     * @var ResidentTransformer
     */
    private $residentTransformer;

    /**
     * @var UserTransformer
     */
    private $userTransformer;

    function __construct(UserTransformer $userTransformer,
        ResidentTransformer $residentTransformer)
    {
        $this->userTransformer = $userTransformer;
        $this->residentTransformer = $residentTransformer;
    }

    public function getTransformHead()
    {
        $resident = Resident::find($this->model->head_id);
        $except = ['updated_at', 'created_at', 'updater', 'creator', 'precint', 'gender', 'district', 'cityMunicipality', 'barangay', 'street', 'email', 'mobile', 'phone', 'birthdate', 'civil_status'];
        if($resident)
        {
            return $this->residentTransformer->transform($resident, $except);
        }
        return '';
    }

    public function getTransformReferrer()
    {
        $resident = Resident::find($this->model->referrer_id);
        $except = ['updated_at', 'created_at', 'updater', 'creator', 'precint', 'gender', 'district', 'cityMunicipality', 'barangay', 'street', 'email', 'mobile', 'phone', 'birthdate', 'civil_status'];
        if($resident)
        {
            return $this->residentTransformer->transform($resident, $except);
        }
        return '';
    }

    /**
     * Transform position
     * 
     * @return array
     */
    public function getTransformPosition()
    {
        $position = Position::find($this->model->position_id);
        if($position)
        {
            return [
                'id' => $position->id,
                'name' => $position->name
            ];
        }
        return null;
    }

    /**
     * Transform address
     * 
     * @param  Address $address
     * @return string
     */
    public function getTransformAddress($address)
    {
        return ! isset ($address) ? null : ['id' => $address->id, 'name' => $address->name];
    }

    /**
     * Transform user
     * 
     * @param  User $user
     * @return array
     */
    public function getTransformUser($user)
    {
        $except = ['group', 'permissions', 'created_at', 'updated_at'];

        return ! isset($user) ? '' : $this->userTransformer->transform($user, $except);
    }

    /**
     * Transform photo
     * 
     * @param  Photo $photo
     * @return string
     */
    public function getTransformPhoto($photo)
    {
        return ! isset($photo) ? '' : URL::to('uploads') . '/' . str_replace(" ", "",Str::title($this->model->cityMunicipality->name)) . '/photos/' . $photo->filename;
    }
}