<?php

class ResidentAttributionTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be dispalyed.
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' 			=> '',
		'name' 			=> '',
		'resident' 		=> 'transformResident',
		'class' 		=> '',
		'created_at' 	=> '',
		'updated_at' 	=> ''
	];


	/**
	 * @var UserTransformer
	 */
	private $userTransformer;

	function __construct(UserTransformer $userTransformer)
	{
		$this->userTransformer = $userTransformer;
	}


	/**
	 * @param  User   $user
	 * @return array
	 */
	public function getTransformUser(User $user)
	{
		$except = ['group', 'created_at', 'updated_at', 'permissions', 'creator', 'updater'];

		return $this->userTransformer->transform($user, $except);
	}

	public function getTransformResident()
	{
		return $this->model->resident;
	}
}