<?php

class ResidentHouseholdTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' 				=> '',
		'resident_id' 		=> '',
		'household_id' 		=> '',
		'role' 				=> '',
        'owner'         	=> 'transformResident',
        'label'         	=> '',
        'phone'         	=> '',
        'district'      	=> 'transformAddress',
        'cityMunicipality' 	=> 'transformAddress',
        'barangay' 			=> 'transformAddress',
        'street' 			=> '',
        'members_count' 	=> 'residentsCount',
        'creator'       	=> 'transformUser',
        'updater'       	=> 'transformUser',
        'updated_at'    	=> 'string',
        'created_at'    	=> 'string'
	];


    /**
     * @var UserTransformer
     */
    private $userTransformer;


    /**
     * @var ResidentTransformer
     */
    private $residentTransformer;


    function __construct(UserTransformer $userTransformer, ResidentTransformer $residentTransformer)
    {
        $this->userTransformer = $userTransformer;
        $this->residentTransformer = $residentTransformer;
    }

    function getTransformUser($user)
    {
        $except = ['resident', 'id', 'permissions'];
        return isset($user) ? $this->userTransformer->transform($user, $except) : '';
    }

    public function getTransformResident($resident)
    {
        $resident = $resident->first();

        $except = ['creator', 'updater'];
        return isset($resident) ? $this->residentTransformer->transform($resident, $except) : '';
    }

    public function getResidentsCount()
    {
        return $this->model->residents->count();
    }

    function getTransformAddress($address)
    {
        return ! isset ($address) ? '' : $address->name;
    }
}