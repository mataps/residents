<?php

class ResidentTransformer extends AbstractBaseTransformer
{

    /**
     * Fields to be displayed
     * 
     * @var array
     */
    protected $fields = array(
        'id'                => '',
        'nickname'          => '',
        'occupation'        => '',
        'parents'           => 'transformResident',
        'voters_id'         => '',
        'is_voter'          => 'boolean',
        'last_name'         => '',
        'first_name'        => '',
        'middle_name'       => '',
        'full_name'         => 'transformFullName',
        'profilePic'        => 'transformPhoto',
        'signature'         => 'transformSignature',
        'affiliations'      => 'transformAffiliations',
        'attributes'        => 'transformAttributes',
        'email'             => '',
        'mobile'            => '',
        'mobile_1'          => '',
        'mobile_2'          => '',
        'phone'             => '',
        'precint'           => '',
        'birthdate'         => '',
        'gender'            => '',
        'civil_status'      => '',
        'atm_number'        => '',
        'bank'              => '',
        'district'          => 'transformAddress',
        'cityMunicipality'  => 'transformAddress',
        'barangay'          => 'transformAddress',
        'street'            => '',
        'attributes'        => '',
        'creator'           => 'transformUser',
        'updater'           => 'transformUser',
        'blacklisted_at'    => '',
        'deceased_at'       => '',
        'updated_at'        => 'string',
        'created_at'        => 'string',
        'notes'             => ''
    );


    /**
     * @var UserTransformer
     */
    private $userTransformer;


    /**
     * @var PhotoTransformer
     */
    private $photoTransformer;


    /**
     * @var ResidentTransformer
     */
    private $residentTransformer;


    function __construct(
        UserTransformer $userTransformer,
        PhotoTransformer $photoTransformer)
    {
        $this->userTransformer = $userTransformer;
        $this->photoTransformer = $photoTransformer;
    }

    public function getTransformAffiliations($affiliations)
    {
        return $affiliations->lists('name');
    }


    public function getTransformAttributes($attributes)
    {
        return $attributes->lists('name');
    }


    /**
     * Transform photo
     * 
     * @param  Photo $photo
     * @return string
     */
    public function getTransformPhoto($photo)
    {
        return ! isset($photo) ? '' : URL::to('uploads') . '/' . str_replace(" ", "",Str::title($this->model->cityMunicipality->name)) . '/photos/' . $photo->filename;
    }

    /**
     * Transform photo
     * 
     * @param  Photo $photo
     * @return string
     */
    public function getTransformSignature($photo)
    {
        return ! isset($photo) ? '' : URL::to('uploads') . '/' . str_replace(" ", "",Str::title($this->model->cityMunicipality->name)) . '/sigs/' . $photo->filename;
    }

    /**
     * Transform user
     * 
     * @param  User $user
     * @return array
     */
    public function getTransformUser($user)
    {
        $except = ['group', 'permissions', 'created_at', 'updated_at'];

        return ! isset($user) ? '' : $this->userTransformer->transform($user, $except);
    }


    /**
     * Transform address
     * 
     * @param  Address $address
     * @return string
     */
    public function getTransformAddress($address)
    {
        return ! isset ($address) ? '' : $address->name;
    }


    public function getTransformFullName($full_name)
    {
        return ucwords(strtolower($full_name));
    }

    /**
     * Transform resident
     * 
     * @param  Resident $resident
     * @return array
     */
    public function getTransformResident($residents)
    {
        $data = [];
        if($residents)
        {
        foreach($residents as $index => $resident)
        {
            $data[$index]['id']           = $resident->id;
            $data[$index]['relation']     = $resident->pivot->relation;
            $data[$index]['voters_id']    = $resident->voters_id;
            $data[$index]['last_name']    = $resident->last_name;
            $data[$index]['first_name']   = $resident->first_name;
            $data[$index]['middle_name']  = $resident->middle_name;
            $data[$index]['full_name']    = $resident->full_name;
            $data[$index]['profilePic']   = $this->getTransformPhoto($resident->photo);
            $data[$index]['email']        = $resident->email;
            $data[$index]['birthdate']    = $resident->birthdate;
        }
        }

        return $data;
    }
}