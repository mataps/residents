<?php
class ScholarshipAwardTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
    protected $fields = [
        'id'            => '',
        'name'          => '',
        'description'   => '',
        'year' 			=> '',
        'remarks' 		=> '',
        'creator'       => 'transformUser',
        'updater'       => 'transformUser',
        'updated_at'    => 'string',
        'created_at'    => 'string'
    ];


    /**
     * @var UserTransformer
     */
    private $userTransformer;

    function __construct(UserTransformer $userTransformer)
    {
    	$this->userTransformer = $userTransformer;
    }

    /**
     * User transformer
     * 
     * @param  User   $user
     * @return array
     */
    function getTransformUser($user)
    {
        $except = ['group', 'created_at', 'updated_at', 'permissions'];

    	return isset($user) ? $this->userTransformer->transform($user, $except) : '';
    }

}