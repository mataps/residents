<?php

class ScholarshipTransformer extends AbstractBaseTransformer{

    /**
     * Fields to be displayed
     * 
     * @var array
     */
    protected $fields = array(
        'id'                => '',
        'resident'          => 'transformResident',
        'school'            => 'transformSchool',
        'comment'           => '',
        'status'            => '',
        'awards'            => 'transformAward',
        'applied_at'        => '',
        'transfered_to'      => 'transformScholarship',
        'creator'           => 'transformUser',
        'updater'           => 'transformUser',
        'updated_at'        => 'string',
        'created_at'        => 'string',
        'transferred_at'     => 'string',
        'parents'           => 'transformParent',
        'father_name'       => 'transformFather',
        'referrer_name'       => 'transformReferrer',
        'father_id'         => '',
        'referrer_id'         => '',
        'mother_id'         => '',
        'mother_name'       => 'transformMother',
        'guardian_name'     => 'transformGuardian',
        'parents_contact_no' => '',
        'others_contact_no' => '',
        'guardian_contact_no' => ''
    );


    /**
     * @var UserTransformer
     */
    private $userTransformer;

    /**
     * @var AwardTransformer
     */
    private $awardTransformer;

    /**
     * @var ResidentTransformer
     */
    private $residentTransformer;

    /**
     * @var SchoolTransformer
     */
    private $schoolTransformer;


    function __construct(
        UserTransformer             $userTransformer,
        AwardTransformer            $awardTransformer,
        ResidentTransformer         $residentTransformer,
        SchoolTransformer           $schoolTransformer)
    {
        $this->userTransformer              = $userTransformer;
        $this->awardTrasformer              = $awardTransformer;
        $this->residentTransformer          = $residentTransformer;
        $this->schoolTransformer            = $schoolTransformer;
    }

    public function getTransformScholarship()
    {
        $except = ['transferedTo'];

        return ($this->model->transfered) ? $this->transform($this->model->transfered, $except) : '';
    }

    public function getTransformFather()
    {

        if($this->model->father)
        {
            return $this->model->father->fullName();
        }
        return $this->model->father_name;
    }

    public function getTransformMother()
    {
        if($this->model->mother)
        {
            return $this->model->mother->fullName();
        }
        return $this->model->mother_name;
    }
    public function getTransformReferrer()
    {
        if($this->model->referrer)
        {
            return $this->model->referrer->fullName();
        }
        return null;
    }

    public function getTransformGuardian()
    {
        if($this->model->guardian)
        {
            return $this->model->guardian->fullName();
        }
        return $this->model->guardian_name;
    }


    /**
     * School Transformer
     * 
     * @param  School $school
     * @return array
     */
    public function getTransformSchool($school) 
    {
        $except = ['created_at', 'updated_at', 'creator', 'updater'];

        return ! isset($school) ? '' : $this->schoolTransformer->transform($school, $except);
    }


    /**
     * User transformer
     * 
     * @param  User $user
     * @return array
     */
    public function getTransformUser(User $user)
    {
        $except = ['group', 'created_at', 'updated_at', 'permissions'];
        
        return ! isset($user) ? '' : $this->userTransformer->transform($user, $except);
    }


    /**
     * Parent Transformer
     * 
     * @param 
     *
     */

    public function getTransformParent()
    {
        if ($this->model->father_name != null || $this->model->mother_name) {
            return $this->model->father_name . '/' . $this->model->mother_name;
        }
        
        $parents = $this->model->resident->parents;
        $output = "";
        foreach ($parents as $parent) {
            $output .= $parent->full_name;
            $output .= " / ";
        }

        return $output;

    }

    /**
     * Awards count
     * 
     * @param  Collection $awards
     * @return integer
     */
    public function getTransformAward($awards)
    {
        return $awards->count();
    }


    /**
     * Resident transformer
     * 
     * @param  Resident $resident
     * @return array
     */
    public function getTransformResident(Resident $resident = null)
    {
        return ! isset($resident) ? '' : $this->residentTransformer->transform($resident);
    }

}