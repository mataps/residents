<?php

class ScholarshipTypeTransformer extends AbstractBaseTransformer {
	
	/**
	 * Fields to be dispalyed
	 * 
	 * @var array
	 */
	protected $fields = array(
		'id' 			=> '',
		'name' 			=> '',
		'description' 	=> '',
		'initial_allowance' 	=> '',
		'probation_amount' 	=> '',
		'creator' 		=> 'transformUser',
		'updater' 		=> 'transformUser',
		'created_at' 	=> 'string',
		'updated_at' 	=> 'string'
		);


	/**
	 * @var UserTransformer
	 */
	private $userTransformer;


	function __construct(UserTransformer $userTransformer)
	{
		$this->userTransformer = $userTransformer;
	}

	/**
	 * User Transformer
	 * 
	 * @param  User   $user
	 * @return array
	 */
	public function getTransformUser(User $user)
	{
		$except = ['group', 'created_at', 'updated_at', 'permissions'];

		return isset($user) ? $this->userTransformer->transform($user, $except) : '';
	}
}