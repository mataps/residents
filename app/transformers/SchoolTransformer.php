<?php

class SchoolTransformer extends AbstractBaseTransformer {


	/**
	 * Transform Schools
	 * 
	 * @var array
	 */
	protected $fields = [
		'id'                => '',
		'name'              => '',
		'type'              => '',
		'level'             => '',
		'phone'             => '',
        'address'           => '',
		'creator'           => 'transformUser',
        'updater'           => 'transformUser',
        'updated_at'        => 'string',
        'created_at'        => 'string'
	];


    /**
     * @var
     */
    private $userTransformer;


    function __construct(UserTransformer $userTransformer)
    {
        $this->userTransformer = $userTransformer;
    }


    /**
     * Transforms user
     * 
     * @param  $user
     * @return array
     */
    public function getTransformUser($user)
    {
        $except = ['group', 'created_at', 'updated_at'];

        return ! isset($user) ? '' : $this->userTransformer->transform($user, $except);
    }
}