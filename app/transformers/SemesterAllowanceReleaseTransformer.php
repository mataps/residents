<?php

use SourceScript\Common\Collections\ResultCollection;

class SemesterAllowanceReleaseTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' 					=> '',
		'term' 					=> '',
		'school_year' 			=> '',
		'fixed_amount' 			=> 'boolean',
		'amount' 				=> 'transformAmount',
		'expiring_at' 			=> 'string',
		'allowances' 			=> 'transformAllowance',
		'creator' 				=> 'transformUser',
		'updater' 				=> 'transformUser'
	];


	/**
	 * @var UserTransformer
	 */
	private $userTransformer;


	/**
	 * @var AllowanceTransformer
	 */
	private $allowanceTransformer;


	function __construct(UserTransformer $userTransformer, AllowanceTransformer $allowanceTransformer)
	{
		$this->userTransformer = $userTransformer;
		$this->allowanceTransformer = $allowanceTransformer;
	}

	public function getTransformAmount()
	{
		if($this->model->fixed_amount) return $this->model->amount;

		return $this->data->computeAllowance($this->model);
	}

	/**
	 * Transform user
	 * 
	 * @param  User   $user
	 * @return array
	 */
	public function getTransformUser(User $user)
	{
		$except = ['permissions', 'group'];

		return isset($user) ? $this->userTransformer->transform($user, $except) : '';
	}

	/**
	 * Transform allowance
	 * 
	 * @param  ResultCollection $allowances
	 * @return array
	 */
	public function getTransformAllowance(ResultCollection $allowances)
	{
		$data = [];
		$except = ['release', 'scholarship', 'semester'];

		if($allowances)
		{
			foreach($allowances as $allowance)
			{
				$data[] = $this->allowanceTransformer->transform($allowance, $except);
			}
		}

		return $data;
	}
}