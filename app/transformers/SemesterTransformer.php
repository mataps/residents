<?php

class SemesterTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' 				=> '',
		'is_probation' 		=> 'boolean',
		'school_level' 		=> '',
		'uuid' 				=> '',
		'status' 			=> '',
		'client' 			=> 'transformResident',
		'beneficiary' 		=> 'transformResident',
		'course' 			=> 'transformCourse',
		'allowance_basis' 	=> 'transformSemester',
		'school' 			=> 'transformSchool',
		'scholarship' 		=> 'transformScholarship',
		'scholarship_type' 	=> 'transformScholarshipType',
		'from' 				=> 'string',
		'to' 				=> 'string',
		'school_year' 		=> '',
		'year_level' 		=> '',
		'term' 				=> '',
		'gwa' 				=> '',
		'comment' 			=> '',
		'subject_count' 	=> '',
		'grades' 			=> 'transformGrades',
		'creator' 			=> 'transformUser',
		'updater' 			=> 'transformUser',
		'allowances'		=> 'transformAllowance',
		'created_at'		=> 'string',
		'date_encoded'		=> 'string'
	];

	/**
	 * @var UserTransformer
	 */
	private $userTransformer;

	/**
	 * @var GradeTransformer
	 */
	private $gradeTransformer;

	/**
	 * @var ResidentTransformer
	 */
	private $residentTransformer;


	function __construct(
		UserTransformer $userTransformer,
		GradeTransformer $gradeTransformer,
		SchoolTransformer $schoolTransformer,
		ScholarshipTypeTransformer $scholarshipTypeTransformer,
		ScholarshipTransformer $scholarshipTransformer,
		CourseTransformer $courseTransformer,
		ResidentTransformer $residentTransformer)
	{
		$this->residentTransformer = $residentTransformer;
		$this->userTransformer = $userTransformer;	
		$this->gradeTransformer = $gradeTransformer;
		$this->schoolTransformer = $schoolTransformer;
		$this->scholarshipTypeTransformer = $scholarshipTypeTransformer;
		$this->scholarshipTransformer = $scholarshipTransformer;
		$this->courseTransformer = $courseTransformer;
	}

	public function getTransformResident($resident)
	{
		$except = ['updated_at', 'created_at', 'updater', 'creator', 'precint', 'gender', 'district', 'cityMunicipality', 'barangay', 'street', 'email', 'mobile', 'phone', 'birthdate', 'civil_status'];

		return isset($resident) ? $this->residentTransformer->transform($resident, $except) : '';
	}

	public function getTransformCourse($course)
	{
		return isset($course) ? $this->courseTransformer->transform($course) : '';
	}


	function getTransformSemester($semester)
	{
		return isset($semester) ? $this->transform($semester, ['allowance_basis', 'grades', 'scholarship']) : '';
	}


	function getTransformScholarshipType(ScholarshipType $scholarshipType)
	{
		return $this->scholarshipTypeTransformer->transform($scholarshipType);
	}

	function getTransformSchool($school)
	{
		$except = ['creator', 'updater', 'created_at', 'updated_at'];

		return isset($school) ? $this->schoolTransformer->transform($school, $except) : '';
	}

	function getTransformGrades($grades)
	{
		return isset($grades) ? $this->gradeTransformer->transformCollection($grades) : '';
	}

	function getTransformScholarship(Scholarship $scholar)
	{
		return $this->scholarshipTransformer->transform($scholar);
	}

	function getTransformAllowance($allowances)
	{
		$total = 0;
		foreach ($allowances as $allowance) 
		{
			$total += $allowance->amount;
		}

		return $total;
	}

	/**
     * User transformer
     * 
     * @param  User   $user
     * @return array
     */
    function getTransformUser(User $user)
    {
        $except = ['group', 'created_at', 'updated_at', 'permissions'];

    	return isset($user) ? $this->userTransformer->transform($user, $except) : '';
    }


}