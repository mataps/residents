<?php

class SubjectTransformer extends AbstractBaseTransformer {

	/**
	 * Transform Subjects
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' => '',
		'name' => '',
		'description' => '',
		'creator' => 'transformUser',
        'updater' => 'transformUser',
        'updated_at' => 'string',
        'created_at' => 'string'
	];

    /**
     * @var
     */
    private $userTransformer;

    function __construct(UserTransformer $userTransformer)
    {
        $this->userTransformer = $userTransformer;
    }

    function getTransformUser($user)
    {
        $except = ['group', 'created_at', 'updated_at'];

        return ! isset($user) ? '' : $this->userTransformer->transform($user, $except);
    }
}