<?php

class TransactionAccountTransformer extends AbstractBaseTransformer {

	protected $fields = [
		'id' => '',
		'name' => '',
		'signatory' => '',
		'label' => '',
		'designation' => '',
		'signatory_2' => '',
		'label_2' => '',
		'designation_2' => '',
		'fixed_voucher_number_format' => '',
		'affiliation' => 'transformAffiliation'
	];


	function __construct(AffiliationTransformer $affiliationTransformer)
	{
		$this->affiliationTransformer = $affiliationTransformer;
	}


	public function getTransformAffiliation($affiliation)
	{
		return isset($affiliation) ? $this->affiliationTransformer->transform($affiliation) : null;
	}
}