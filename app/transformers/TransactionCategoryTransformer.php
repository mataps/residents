<?php

class TransactionCategoryTransformer extends AbstractBaseTransformer {

	protected $fields = [
		'id' => '',
		'name' => '',
		'sub_categories' => 'transformSubCategory',
		'creator' => 'transformUser',
        'updater' => 'transformUser',
        'updated_at' => 'string',
        'created_at' => 'string'
	];


	/**
	 * @var UserTransformer
	 */
	private $userTransformer;

	function __construct(UserTransformer $userTransformer, TransactionSubCategoryTransformer $subCategoryTransformer)
	{
		$this->userTransformer = $userTransformer;
		$this->subCategoryTransformer = $subCategoryTransformer;
	}

	function getTransformSubCategory($transactionSubCategories)
	{
		return isset($transactionSubCategories) ? $this->subCategoryTransformer->transformCollection($transactionSubCategories) : '';
	}

	function getTransformUser(User $user)
    {
        $except = ['updated_at', 'created_at', 'permissions', 'group', 'updater', 'creator'];
        return ! isset($user) ? '' : $this->userTransformer->transform($user, $except);
    }
}
