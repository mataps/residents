<?php

class TransactionItemTransformer extends AbstractBaseTransformer {
	
	protected $fields = [
		'id' => '',
		'name' => '',
		'description' => '',
		'creator' => 'transformUser',
        'updater' => 'transformUser',
        'updated_at' => 'string',
        'created_at' => 'string'
	];

	/**
	 * @var UserTransformer
	 */
	private $userTransformer;

	function __construct(UserTransformer $userTransformer)
	{
		$this->userTransformer = $userTransformer;
	}

	function getTransformUser(User $user)
    {
        $except = ['updated_at', 'created_at', 'permissions', 'group', 'updater', 'creator'];
        return ! isset($user) ? '' : $this->userTransformer->transform($user, $except);
    }
}