<?php

class TransactionLiquidationTransformer extends AbstractBaseTransformer {


	/**
	 * Fields to be displayed.
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' 			=> '',
		'label' 		=> '',
		'description' 	=> '',
		'amount' 		=> '',
		'creator' 		=> 'transformUser',
		'updater' 		=> 'transformUser',
		'created_at' 	=> 'string',
		'updated_at' 	=> 'string'
	];


	/**
	 * @var UserTransformer
	 */
	private $userTransformer;


	function __construct(UserTransformer $userTransformer)
	{
		$this->userTransformer = $userTransformer;
	}


	/**
	 * @param  User   $user
	 * @return array
	 */
	public function getTransformUser(User $user)
	{
		$except = ['permissions', 'group'];

		return $this->userTransformer->transform($user, $except);
	}
}