<?php

class TransactionSubCategoryTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' => '',
		'name' => '',
		'category' => 'transformCategory',
		'creator' => 'transformUser',
		'updater' => 'transformUser',
		'created_at' => 'string',
		'updated_at' => 'string'
	];


	/**
	 * @var UserTransformer
	 */
	private $userTransformer;


	function __construct(UserTransformer $userTransformer)
	{
		$this->userTransformer = $userTransformer;
	}


	/**
	 * @param  User   $user
	 * @return array
	 */
	public function getTransformUser(User $user)
	{
		$except = ['permissions', 'groups'];

		return $this->userTransformer->transform($user, $except);
	}

	/**
	 * @param  TransactionCategory $transactionCategory
	 * @return array
	 */
	public function getTransformCategory(TransactionCategory $transactionCategory)
	{
		return [
			'id' => $transactionCategory->id,
			'name' => $transactionCategory->name
		];
	}
}