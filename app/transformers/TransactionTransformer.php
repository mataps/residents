<?php

class TransactionTransformer extends AbstractBaseTransformer {

    /**
     * Fields to be displayed
     * 
     * @var array
     */
    protected $fields = array(
        'id'                => '',
        'uuid'              => '',
        'vendor'            => 'transformVendor',
        'voucher'           => 'transformVoucher',
        'client'            => 'transformClient',
        'client_type'       => '',
        'referrer'          => 'transformResident',
        'beneficiary'       => 'transformBeneficiary',
        'beneficiary_type'  => '',
        'account'           => 'transformAccount',
        'category'          => 'transformCategory',
        'sub_category'      => 'transformSubCategory',
        'item'              => 'transformItem',
        'type'              => 'transformType',
        'settled_at'        => 'string',
        'refund'            => '',
        'details'           => '',
        'remarks'           => '',
        'amount'            => '',
        'liquidatable'      => 'boolean',
        'liquidated_at'     => 'string',
        'creator'           => 'transformUser',
        'updater'           => 'transformUser',
        'updated_at'        => 'string',
        'created_at'        => 'string'
    );


    /**
     * @var UserTransformer
     */
    private $userTransformer;


    /**
     * @var TransactionResidentTransformer
     */
    private $residentTransformer;


    /**
     * @var TransactionAccountTransformer
     */
    private $transactionAccountTransformer;


    /**
     * @var TransactionCategoryTransformer
     */
    private $transactionCategoryTransformer;


    /**
     * @var TransactionSubCategoryTransformer
     */
    private $transactionSubCategoryTransformer;


    /**
     * @var TransactionItemTransformer
     */
    private $transactionItemTransformer;


    /**
     * @var AffiliationTransformer
     */
    private $affiliationTransformer;


    /**
     * @var VoucherTransformer
     */
    private $voucherTransformer;


    /**
     * @var VendorTransformer
     */
    private $vendorTransformer;


    function __construct(
        UserTransformer $userTransformer,
        TransactionAccountTransformer $transactionAccountTransformer,
        ResidentTransformer $residentTransformer,
        TransactionCategoryTransformer $transactionCategoryTransformer,
        TransactionSubCategoryTransformer $transactionSubCategoryTransformer,
        TransactionItemTransformer $transactionItemTransformer,
        AffiliationTransformer $affiliationTransformer,
        VoucherTransformer $voucherTransformer,
        VendorTransformer $vendorTransformer)
    {
        $this->residentTransformer = $residentTransformer;
        $this->userTransformer = $userTransformer;
        $this->transactionAccountTransformer = $transactionAccountTransformer;
        $this->transactionCategoryTransformer = $transactionCategoryTransformer;
        $this->transactionSubCategoryTransformer = $transactionSubCategoryTransformer;
        $this->transactionItemTransformer = $transactionItemTransformer;
        $this->affiliationTransformer = $affiliationTransformer;
        $this->voucherTransformer = $voucherTransformer;
        $this->vendorTransformer = $vendorTransformer;
    }

    function getTransformVendor($vendor)
    {
        return isset($vendor) ? $this->vendorTransformer->transform($vendor) : '';
    }

    function getTransformType($type){
        return $this->model->transaction_type;
    }

    function getTransformTransaction(Transaction $refund)
    {
        return isset($refund) ? $this->transform($refund) : '';
    }


    function getTransformVoucher($voucher)
    {
        $except = ['creator', 'updater', 'client', 'client_type'];

        return isset($voucher) ? $this->voucherTransformer->transform($voucher, $except) : '';
    }

    function getTransformItem(TransactionItem $item)
    {
        $except = ['creator', 'updater'];

        return $item ? $this->transactionItemTransformer->transform($item, $except) : '';
    }

    function getTransformSubCategory($subCategory)
    {
        $except = ['creator', 'updater'];

        return $subCategory ? $this->transactionSubCategoryTransformer->transform($subCategory, $except) : '';
    }

    function getTransformCategory()
    {
        $except = ['creator', 'updater', 'sub_categories'];

        if($this->model->subCategory)
        {
            return $this->model->subCategory->category ? $this->transactionCategoryTransformer->transform($this->model->subCategory->category, $except) : '';
        }
        return '';
    }

    function getTransformUser($user)
    {
        $except = ['updated_at', 'created_at', 'permissions', 'group', 'updater', 'creator'];
        return ! isset($user) ? '' : $this->userTransformer->transform($user, $except);
    }

    function getTransformResident(Resident $resident)
    {
        $except = ['updated_at', 'created_at', 'updater', 'creator', 'precint', 'gender', 'district', 'cityMunicipality', 'barangay', 'street', 'email', 'mobile', 'phone', 'birthdate', 'civil_status'];
        return $resident ? $this->residentTransformer->transform($resident, $except) : '';
    }

    function getTransformAccount($account)
    {
        if(!$this->model->voucher) return '';
        return $account ? $this->transactionAccountTransformer->transform($account) : '';
    }

    function getTransformClient($client)
    {
        if(isset($client))
        {
            return $this->model->client_type == "Resident" ? $this->residentTransformer->transform($client) : $this->affiliationTransformer->transform($client);
        }

        return '';
        
    }

    function getTransformBeneficiary($beneficiary)
    {
        if(isset($beneficiary))
        {
            return $this->model->beneficiary_type == "Resident" ?  $this->residentTransformer->transform($beneficiary) : $this->affiliationTransformer->transform($beneficiary);    
        }
        return '';
    }
}