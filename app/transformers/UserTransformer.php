<?php

use SourceScript\Common\Collections\ResultCollection;

class UserTransformer extends AbstractBaseTransformer {

    /**
     * @var GroupTransformer
     */
    private $groupTransformer;

    protected $fields = array(
        'id' => 'integer',
        'group' => 'groupTransform',
        'resident' => 'residentTransform',
        'username' => '',
        'permissions' => 'permissionsTransform',
        'created_at' => 'string',
        'updated_at' => 'string',
        'available_start' => '',
        'available_end' => ''
    );

    function __construct(GroupTransformer $groupTransformer, PermissionTransformer $permissionTransformer)
    {
        $this->groupTransformer = $groupTransformer;
        $this->permissionTransformer = $permissionTransformer;
    }

    function getGroupTransform($value)
    {
        if ( ! $value)
            return null;

        return $this->groupTransformer->transform($value, ['permissions', 'created_at', 'updated_at']);
    }

    public function getResidentTransform($resident = null)
    {
        if (is_null($resident))
        {
            return null;
        }

        return [
            'id' => $resident->id,
            'last_name' => $resident->last_name,
            'first_name' => $resident->first_name,
            'middle_name' => $resident->middle_name,
            'full_name' => $resident->full_name,
            'profilePic' => $this->transformPhoto($resident->profilePic),
            'birthdate' => $resident->birthdate,
            'cityMunicipality' => $this->transformAddress($resident->cityMunicipality),
            'barangay' => $this->transformAddress($resident->barangay),
            'attributes' => $resident->attributes
        ];
    }

    function transformPhoto($photo)
    {
        return ! isset($photo) ? '' : '/uploads/' . $photo->filename;
    }

    function transformAddress($address)
    {
        return ! isset ($address) ? '' : $address->name;
    }

    function getPermissionsTransform(ResultCollection $permissions)
    {
        if($permissions->count())
        {
            foreach($permissions as $permission)
            {
                $data[] = $this->transformPermission($permission);
            }    
            return $data;
        }
        
        return '';
    }

    function transformPermission($permission)
    {

        $result = isset($permission) ? $this->permissionTransformer->transform($permission) : '';

        $result = array_add($result, 'status', (int) $permission->pivot->status);

        return $result;
    }
}