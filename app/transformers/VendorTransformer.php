<?php

class VendorTransformer extends AbstractBaseTransformer {


	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' 				=> '',
		'name' 				=> '',
		'district'      	=> 'transformAddress',
        'cityMunicipality' 	=> 'transformAddress',
        'barangay' 			=> 'transformAddress',
        'street' 			=> '',
        'creator' 			=> 'transformUser',
        'updater' 			=> 'transformUser',
        'updated_at'    	=> 'string',
        'created_at'    	=> 'string'
	];


	function __construct(UserTransformer $userTransformer)
	{
		$this->userTransformer = $userTransformer;
	}

	function getTransformAddress($address)
    {
        return ! isset ($address) ? '' : $address->name;
    }


    function getTransformUser($user)
    {
        $except = ['resident', 'id', 'permissions', 'group'];
        return isset($user) ? $this->userTransformer->transform($user, $except) : '';
    }
}