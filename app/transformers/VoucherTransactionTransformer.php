<?php

class VoucherTransactionTransformer extends AbstractBaseTransformer {


	protected $fields = array(
        'id' => '',
        'vendor' => 'transformVendor',
        'client' => 'transformClient',
        'client_type' => '',
        'referrer' => 'transformResident',
        'beneficiary' => 'transformBeneficiary',
        'beneficiary_type' => '',
        'account' => 'transformAccount',
        'category' => 'transformCategory',
        'sub_category' => 'transformSubCategory',
        'item' => 'transformItem',
        'type' => 'transformType',
        'settled' => '', // Uy, kier to, nilagay ko lang to, eto yung luma pero di ko tinanggal
        'settled_at' => 'string', // Nilagay ko to kasi ewan ko ba't `settled` yung name ng field.
        'details' => '',
        'remarks' => '',
        'amount' => '',
        'status' => '',
        'liquidatable' => 'boolean',
        'liquidated_at' => 'string',
        'creator' => 'transformUser',
        'updater' => 'transformUser',
        'updated_at' => 'string',
        'created_at' => 'string'
    );

    /**
     * @var UserTransformer
     */
    private $userTransformer;

    /**
     * @var TransactionResidentTransformer
     */
    private $residentTransformer;

    /**
     * @var TransactionAccountTransformer
     */
    private $transactionAccountTransformer;

    /**
     * @var TransactionCategoryTransformer
     */
    private $transactionCategoryTransformer;

    /**
     * @var TransactionSubCategoryTransformer
     */
    private $transactionSubCategoryTransformer;

    /**
     * @var TransactionItemTransformer
     */
    private $transactionItemTransformer;

    /**
     * @var AffiliationTransformer
     */
    private $affiliationTransformer;

    /**
     * @var VendorTransformer
     */
    private $vendorTransformer;


    /**
     * @param UserTransformer $userTransformer
     */
    function __construct(
        UserTransformer $userTransformer,
        TransactionAccountTransformer $transactionAccountTransformer,
        ResidentTransformer $residentTransformer,
        TransactionCategoryTransformer $transactionCategoryTransformer,
        TransactionSubCategoryTransformer $transactionSubCategoryTransformer,
        TransactionItemTransformer $transactionItemTransformer,
        AffiliationTransformer $affiliationTransformer,
        VendorTransformer $vendorTransformer)
    {
        $this->residentTransformer = $residentTransformer;
        $this->userTransformer = $userTransformer;
        $this->transactionAccountTransformer = $transactionAccountTransformer;
        $this->transactionCategoryTransformer = $transactionCategoryTransformer;
        $this->transactionSubCategoryTransformer = $transactionSubCategoryTransformer;
        $this->transactionItemTransformer = $transactionItemTransformer;
        $this->affiliationTransformer = $affiliationTransformer;
        $this->vendorTransformer = $vendorTransformer;
    }

    function getTransformVendor($vendor)
    {
        return isset($vendor) ? $this->vendorTransformer->transform($vendor) : '';
    }

    function getTransformType($type){
        return $this->model->transaction_type;
    }

    function getTransformItem(TransactionItem $item)
    {
        $except = ['creator', 'updater'];

        return $item ? $this->transactionItemTransformer->transform($item, $except) : '';
    }

    function getTransformSubCategory($subCategory)
    {
        $except = ['creator', 'updater'];

        return $subCategory ? $this->transactionSubCategoryTransformer->transform($subCategory, $except) : '';
    }

    function getTransformCategory()
    {
        $except = ['creator', 'updater', 'sub_categories'];

        if($this->model->subCategory)
        {
            return $this->model->subCategory->category ? $this->transactionCategoryTransformer->transform($this->model->subCategory->category, $except) : '';
        }
        return '';
    }

    function getTransformUser(User $user)
    {
        $except = ['updated_at', 'created_at', 'permissions', 'group', 'updater', 'creator'];
        return ! isset($user) ? '' : $this->userTransformer->transform($user, $except);
    }

    function getTransformResident(Resident $resident)
    {
        $except = ['updated_at', 'created_at', 'updater', 'creator', 'precint', 'gender', 'district', 'cityMunicipality', 'barangay', 'street', 'email', 'mobile', 'phone', 'birthdate', 'civil_status'];
        return $resident ? $this->residentTransformer->transform($resident, $except) : '';
    }

    function getTransformAccount($account)
    {
        return $account ? $this->transactionAccountTransformer->transform($account) : '';
    }

    function getTransformClient($client)
    {
        if(!isset($client)) return null;
        return $this->model->client_type == "Resident" ? $this->residentTransformer->transform($client) : $this->affiliationTransformer->transform($client);
    }

    function getTransformBeneficiary($beneficiary)
    {
        if(!isset($beneficiary)) return null;
        return $this->model->beneficiary_type == "Resident" ?  $this->residentTransformer->transform($beneficiary) : $this->affiliationTransformer->transform($beneficiary);
    }
}