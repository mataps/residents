<?php

class VoucherTransformer extends AbstractBaseTransformer {

	/**
	 * Fields to be displayed
	 * 
	 * @var array
	 */
	protected $fields = [
		'id' 				=> '',
		'uuid' 				=> '',
		'account' 			=> 'transformAccount',
		'client_type' 		=> '',
		'client' 			=> 'transformClient',
		'status' 			=> '',
		'remarks' 			=> '',
		// 'details' 			=> 'vdetails',
		'settled_at' 		=> 'string',
		'creator' 			=> 'transformUser',
        'updater' 			=> 'transformUser',
		'created_at' 		=> 'string',
		'updated_at' 		=> 'string'
	];


	/**
	 * @var ResidentTransformer
	 */
	private $residentTransformer;

	/**
	 * @var AffiliationTransformer
	 */
	private $affiliationTransformer;

	/**
	 * @var TransactionAccountTranformer
	 */
	private $transactionAccountTransformer;

	/**
	 * @var UserTransformer
	 */
	private $userTransformer;


	function __construct(
		ResidentTransformer $residentTransformer,
		AffiliationTransformer $affiliationTransformer,
		TransactionAccountTransformer $transactionAccountTransformer,
		UserTransformer $userTransformer)
	{
		$this->residentTransformer = $residentTransformer;
		$this->affiliationTransformer = $affiliationTransformer;
		$this->transactionAccountTransformer = $transactionAccountTransformer;
		$this->userTransformer = $userTransformer;
	}


	/**
	 * Client Transformer
	 * 
	 * @param  Resident/Affiliation $client
	 * @return array
	 */
	public function getTransformClient($client)
    {
    	$except = ['created_at', 'updated_at'];

        return $this->model->client_type == "Resident" ? $this->residentTransformer->transform($client, $except) : $this->affiliationTransformer->transform($client, $except);
    }


    /**
     * Account Transformer
     * 
     * @param  Account $account
     * @return array
     */
    public function getTransformAccount($account)
    {
        return $account ? $this->transactionAccountTransformer->transform($account) : '';
    }

    /**
     * User Transformer
     * 
     * @param  User   $user
     * @return array
     */
    public function getTransformUser(User $user)
    {
        $except = ['updated_at', 'created_at', 'permissions', 'group', 'updater', 'creator'];

        return ! isset($user) ? '' : $this->userTransformer->transform($user, $except);
    }
}