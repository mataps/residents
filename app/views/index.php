<!DOCTYPE html>
<html lang="en">
<head>
	<title ng-bind="$state.current.data.pageTitle + ' - EDMS'"></title>
	<meta name="fragment" content="!">
	<meta charset="utf-8">
    <!--IE Compatibility modes-->
    <meta http-equiv="X-UA-Compatible" content="IE=10; IE=9; IE=8; IE=7; IE=EDGE" />
    <!--Mobile first-->
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- stylesheets -->
	<link rel="stylesheet" type="text/css" href="/assets/vendor/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendor/font-awesome/css/font-awesome.min.css">
     <link rel="stylesheet" type="text/css" href="/assets/vendor/angular-ui-select/dist/select.css">
<!--     <link rel="stylesheet" type="text/css" href="assets/vendor/angular-snap/angular-snap.min.css">-->
	<link rel="stylesheet" type="text/css" href="/assets/vendor/jquery-ui/themes/flick/jquery-ui.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/vendor/ionicons/css/ionicons.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/vendor/ngprogress-lite/ngprogress-lite.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendor/angular-spinkit/build/angular-spinkit.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/vendor/angular-aside/dist/css/angular-aside.min.css">
    <link rel="stylesheet" type="text/css" href="/assets/css/style.css">
<!--    <link href="//fonts.googleapis.com/css?family=Open+Sans:300italic,300,400italic,400,600italic,600,700italic,700,800italic,800" rel="stylesheet" type="text/css">-->

    <style>
        .chart-asdasdasdadsd {
            height: 360px;
            width: 100%;
            margin-left: 25;
        }

        .flash-alert {
            z-index: 99999;
        }

				.spinning-dots-spinner {
					margin: 0;
					margin-right: 10px;
					display: inline-block;
				}
    </style>
    <base href="/">
</head>


<body>

    <!-- Subscribe to success flash messages. -->
    <div flash-alert="success" active-class="in" class="flash-alert fade" style="background: #f66b2a;">
        <button type="button" class="close" ng-click="hide()">&times;</button>
        <strong class="alert-heading">Success!</strong>
        <span class="alert-message">{{flash.message}}</span>
    </div>

    <!-- Subscribe to error flash messages. -->
    <div flash-alert="error" active-class="in" class="flash-alert fade">
        <button type="button" class="close" ng-click="hide()">&times;</button>
        <strong class="alert-heading">Error!</strong>
        <span class="alert-message">{{flash.message}}</span>
    </div>

    <!-- Subscribe to error flash messages. -->
    <div id="validation" flash-alert active-class="in" class="flash-alert fade">
        <button type="button" class="close" ng-click="hide()">&times;</button>
        <strong class="alert-heading">Error!</strong>
        <div class="alert-message" ng-repeat="message in flash.message">{{ message }}</div>
    </div>

    <!-- Subscribe to info flash messages. -->
    <div flash-alert="info" active-class="in" class="flash-alert fade">
        <button type="button" class="close" ng-click="hide()">&times;</button>
        <strong class="alert-heading"><circle-spinner></circle-spinner></strong>
        <span class="alert-message">{{flash.message}}</span>
    </div>

    <!-- Subscribe to warn flash messages. -->
    <div flash-alert="warn" active-class="in" class="flash-alert fade">
        <button type="button" class="close" ng-click="hide()">&times;</button>
        <strong class="alert-heading">Warning!</strong>
        <span class="alert-message">{{flash.message}}</span>
    </div>

    <div ui-view></div>

    <!-- <footer class="Footer bg-dark dker">
        <p><?php echo date('Y') ?>SourceScriptGalactus. Powered by <a href="http://sourcescript.ph">SourceScript Innovations</a></p>
    </footer> -->

	<!-- scripts -->
	<script src="/assets/js/libs.js"></script>
	<script src="/assets/js/lrDragNDrop.js"></script>
	<script src="/assets/js/holder.js"></script>
	<script src="/assets/vendor/snapjs/snap.min.js"></script>
    <script type="text/javascript">
        angular.module('variables', []);
        <?php foreach ($constants as $variable => $value)
        {
            if (!in_array($variable, array('__path', '__data', '__env', 'app', 'errors')))
            {
                echo sprintf("angular.module('variables').constant('%s', JSON.parse('%s'));\n\t\t", strtoupper($variable), addslashes(json_encode($value)));
            }
        } ?>
    </script>
	<script src="/assets/js/build.js"></script>
    <!--  Preloading images  -->
    <script>
        $(function() {
            new preLoader([
                "/assets/img/loader2.gif"
            ]);

            var $table = $('.table');
            $table.floatThead({
                scrollContainer: function($table){
                    return $table.closest('#content');
                }
            });
        })
    </script>
</body>
</html>
