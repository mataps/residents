<!DOCTYPE html>
<html lang="en">
<head>
  <title> EDMS Login </title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700">
  <link rel="stylesheet" href="/assets/vendor/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="/assets/css/style.css">
</head>

<body class="login">

  <div class="well login-box" @if ( Session::has('error') ) style="height:375px" @endif>
	    <img src="/assets/img/galactus_logo_big.png" class="img-responsive" style="margin-left: auto; margin-right: auto; margin-bottom: 10px;">
        @if(Session::has('error'))
          <div class="alert alert-warning">
            {{ Session::get('error') }}
          </div>
        @endif
        <form action="" method="POST">
            <div class="form-group">
                <label for="username-email">Username</label>
                <input name="username" id="username-email" placeholder="E-mail or Username" type="text" class="form-control" />
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input id="password" name="password"  placeholder="Password" type="password" class="form-control" />
            </div>
            <div class="form-group text-center">
                <button class="btn btn-danger btn-cancel-action">Cancel</button>
                <input type="submit" class="btn btn-success btn-login-submit" value="Login" />
            </div>
        </form>
    </div>
</body>
</html>
