<html>
<head>
	<style type="text/css">
		span {
			display: block;
		}

		div.page {
			page-break-after: always;
	        	page-break-inside: avoid;
	        	margin: 0px;
	        	padding: 0px;
		}

		div.page:last-of-type {
			display: none;
		}
	</style>
</head>
<body>
@foreach($barangays as $barangay)
@foreach($barangay as $member)
<div style="display: inline-block; width: 48%; height: 90px;">
	<div style="margin-left: 10px;">
		<div style="margin-left: 10px; width: 60px; background: #ccc; float: right;">
			@if($member->profilePic && $member->cityMunicipality)
			@if(file_exists(base_path('public/uploads') . '/' . str_replace(" ", "",Str::title($member->cityMunicipality->name)) . '/photos/' . $member->profilePic->filename ))
			<img src="{{ base_path('public/uploads') . '/' . str_replace(" ", "",Str::title($member->cityMunicipality->name)) . '/photos/' . $member->profilePic->filename }}" alt="" style="width: 60px;">
			@endif
			@endif
		</div>
		<span style="font-size: 12px;">
			{{ "$member->last_name, $member->first_name $member->middle_name" }}
		</span>
		@if($member->birthdate)
		<span style="font-size: 10px;">
			{{ date('Y/m/d', strtotime($member->birthdate)) }}
		</span>
		@endif

		<span style="font-size: 10px;">
			Position: {{ implode(", ", $member->positions->lists('name')) }}
			<br>
			@if($member->from)
			{{ $member->from }} - {{ $member->to }}
			@endif
		</span>
		<span style="font-size: 10px;">
			{{ $member->precinct }}
		</span>
		<span style="font-size: 10px;">
			{{ $member->district ? $member->district->name : '' }}
		</span>
		<span style="font-size: 10px;">
			{{ $member->cityMunicipality ? $member->cityMunicipality->name : '' }}, {{ $member->barangay ? $member->barangay->name : '' }}
		</span>
		<span style="font-size: 8px;">
			{{ $member->street }}
		</span>
		<span style="font-size: 8px;">
			{{ $member->phone }}
			@if($member->email)
			/ {{ $member->email }}
			@endif
		</span>
	</div>
</div>
@endforeach
	@if($disable_page_break == 'false')
		<div class="page"></div>
	@endif
@endforeach
</body>
</html>
