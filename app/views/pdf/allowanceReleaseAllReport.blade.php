<html>
	<head>
		<style type="text/css">

			tr {
			    page-break-inside: avoid;
			}

			table {
				width: 100%;
			}

			table th {
				text-align: left;
			}
		</style>
	</head>
	<body>
		<table>
			<thead>
				<tr>
					<th colspan="9">
						Release Report for {{ $release->uuid }}
					</th>
				</tr>
				<tr>
					<th>
						Last Name
					</th>
					<th>
						First Name
					</th>
					<th>
						Middle Name
					</th>
					<th>
						Barangay
					</th>
					<th>
						City/Municipality
					</th>
					<th>
						School
					</th>
					<th>
						GWA
					</th>
					<th>
						Amount
					</th>
					<th>
						Claimed
					</th>
				</tr>
			</thead>
			<tbody>
				@foreach($semesters as $semester)
				<tr>
					<td>
						{{ $semester->scholarship->resident->last_name }}
					</td>
					<td>
						{{ $semester->scholarship->resident->first_name }}
					</td>
					<td>
						{{ $semester->scholarship->resident->middle_name }}
					</td>
					<td>
						{{ $semester->scholarship->resident->barangay ? $semester->scholarship->resident->barangay->name : '' }}
					</td>
					<td>
						{{ $semester->scholarship->resident->cityMunicipality ? $semester->scholarship->resident->cityMunicipality->name : '' }}
					</td>
					<td>
						{{ $semester->school ? $semester->school->name : '' }}
					</td>
					<td>
						{{ $semester->gwa }}
					</td>
					<td>
						{{ $semester->allowances->count() > 0 ? $semester->allowances->first()->amount : '' }}
					</td>
					<td>
						{{ $semester->allowances->count() > 0 ? $semester->allowances->first()->created_at->toDateString() : '' }}
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
		
	</body>
</html>