<html>
<meta charset="UTF-8" />
	<head>
		<style type="text/css">
			body {
				font-size: 10px;
				font-family: verdana;
			}

			tr {
			    page-break-inside: avoid;
			}

			table {
				width: 100%;
				font-size: 14px;
				margin-top: 40px;
				border-collapse: collapse;
			}

			table, th, td {
				border: 1px solid black;
			}

			table th {
				text-align: left;
			}

			h1 {
				font-weight: normal;
				text-align: center;
				margin: 0px;
				text-transform: uppercase;
			}

			h2 {
				text-align: left;
				font-weight: bold;
				text-transform: uppercase;
				margin: 5px 0px;
			}

			h3 {
				margin: 0px;
				font-weight: normal;
			}

			div.page {
				page-break-after: always;
			}

			div.page:last-of-type {
				page-break-after: avoid;
			}
		</style>
	</head>
	<body>
		@foreach($barangays as $barangay)
		<div class="page">
			<h1>
				{{ implode(', ', $release->scholarship_types->lists('name')) }}
			</h1>
			<h1>
				Region III
			</h1>
			<h1>
				Province of Nueva Ecija
			</h1>
			<h2>
				Masterlist of beneficiaries
			</h2>
			<h3>
				For the payment of AICS at {{ $barangay['barangay']->name }} {{ $barangay['cityMunicipality']->name }}, Nueva Ecija, Philippines for the period of ________________________
			</h3>
			<table cellpadding="3">
				<thead>
					<tr>
						<th>
							No.
						</th>
						<th colspan="6">
							Name Of Client
						</th>
						<th colspan="2">
							Complete Address
						</th>
						<th>
							Type Of Assistance
						</th>
						<th>
							Amount
						</th>
					</tr>
					<tr>
						<th>
							
						</th>
						<th>
							Last
						</th>
						<th>
							First
						</th>
						<th>
							Middle
						</th>
						<th>
							Ext
						</th>
						<th>
							Age
						</th>
						<th>
							Sex
						</th>
						<th>
							Street
						</th>
						<th>
							Brgy./Municipality/City
						</th>
						<th>
							
						</th>
						<th>
							
						</th>
					</tr>
				</thead>
				<tbody>
				<?php $counter = 0; ?>
					@foreach($barangay['semesters'] as $index =>  $semester)
						@if($semester->client)		
						<tr>
							<?php $counter++; ?>
							<td>
								{{ $counter }}
							</td>
							<td>
								{{ $semester->client ? ucwords(mb_strtolower($semester->client->last_name, 'UTF-8')) : '' }}
							</td>
							<td>
								{{ $semester->client ? ucwords(mb_strtolower($semester->client->first_name , 'UTF-8')) : '' }}
							</td>
							<td>
								{{ $semester->client ? ucwords(mb_strtolower($semester->client->middle_name , 'UTF-8')) : '' }}
							</td>
							<td>
								
							</td>
							<td>
								{{ $semester->client ? ucwords(mb_strtolower($semester->client->getAge() , 'UTF-8')) : '' }}
							</td>
							<td>
								{{ $semester->client ? ucwords(mb_strtolower($semester->client->gender , 'UTF-8')) : '' }}
							</td>
							<td>
								{{ $semester->client ? ucwords(mb_strtolower($semester->getScholar()->street , 'UTF-8')) : '' }}
							</td>
							<td>
								{{ $semester->client ? ucwords(mb_strtolower($semester->getScholar()->barangay->name, 'UTF-8')) : '' }} / {{ $semester->client ? $semester->getScholar()->cityMunicipality->name : '' }}
							</td>
							<td>
								Educational Assistance
							</td>
							<td align="right">
								{{ number_format($semester->computeAllowance($release), 2) }}
							</td>
						</tr>
						@endif
					@endforeach
					<tr>
						<td colspan="10">
							Total
						</td>
						<td align="right">
							{{ number_format($barangay['total'], 2) }}
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		@endforeach
	</body>
</html>
