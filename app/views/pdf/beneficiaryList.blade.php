<head>
	<style type="">
		table {
				font-family: sans-serif;
			}

			tr {
			    page-break-inside: avoid;
			}

			tbody th {
				border-bottom: 1px solid #000;
				font-size: 12px;
			}

			tbody th:first-of-type {
				text-align: center;
			}

			tbody th:last-of-type {
				text-align: center;
				padding-top: 5px;
				padding-bottom: 5px;
			}

			tbody td:last-of-type {
				text-align: left;
			}

			tbody td {
				border-right: 1px solid #ccc;
				font-size: 10px;
				padding: 5px;
			}

			tbody td:nth-of-type(2) {
				text-align: center;
			}

			thead th {
				border-bottom: 1px solid #000;
				border-top: 1px solid #000;
				padding: 5px 20px;
				font-size: 12px;
			} 

			tr:nth-child(even) {
  				background-color: white;
			}
			
			tr:nth-child(odd) {
  				background-color: #DDDeee;
			}

			thead tr th.title {
				border: 0px;
				text-align: center;
				font-size: 18px;
				padding-left: 0px;
				padding-right: 0px;
			}

			thead tr th span.dates {
				font-size: 12px;
				font-weight: normal;
				float: right;
			}

			.font-title { 
				font-family: Arial; 
				font-weight: bold;
			}
			.font-data { font-family: Calibri }
	</style>
</head>

<table width="100%">
	<thead>
		<tr>
			<th>
				No.
			</th>
			<th>
				Lastname
			</th>
			<th>
				Firstname
			</th>
			<th>
				Maternalname
			</th>
			<th>
				Address
			</th>
			<th>
				Amount
			</th>
		</tr>
	</thead>
	<tbody>
		@foreach($scholarships as $index => $scholarship)
		<tr>
			<td>
				{{ ++$index }}
			</td>
			<td>
				{{ $scholarship->resident->last_name }}
			</td>
			<td>
				{{ $scholarship->resident->first_name }}
			</td>
			<td>
				{{ $scholarship->resident->middle_name }}
			</td>
			<td>
				{{ $scholarship->resident->cityMunicipality->name }}, {{ $scholarship->resident->barangay->name }}
			</td>
			<td>
				{{ $scholarship->semesters->first()->allowances->sum('amount') }}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>