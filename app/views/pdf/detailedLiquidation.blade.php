<html>
	<head>
		<style type="text/css">
			td {
				text-align: left;
			}

			tr {
			    page-break-inside: avoid;
			}

			table {
				font-family: sans-serif;
			}

			tbody th {
				border-bottom: 1px solid #000;
				font-size: 16px;
			}

			thead th {
				border-bottom: 1px solid #000;
				border-top: 1px solid #000;
				padding: 5px 20px;
				font-size: 12px;
			}

			thead tr th.title {
				border: 0px;
				text-align: left;
				font-size: 18px;
				padding-left: 0px;
				padding-right: 0px;
			}

			thead tr th span.dates {
				font-size: 12px;
				font-weight: normal;
				float: right;
			}
			
			.font-title { 
				font-family: Arial; 
				font-weight: bold;
			}
			.font-data { font-family: Calibri }

			.text-right {
				text-align: right;
			}

			.text-center {
				text-align: center;
			}
		</style>
	</head>
	<body>
		<table cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<th colspan="4" class="title">
						{{ $title }}
						<span class="dates">
							{{ $from }} - {{ $to }}
						</span>
					</th>
				</tr>
				<tr>
					<th colspan="3">
						
					</th>
					<th>
						Amount
					</th>
				</tr>
			</thead>
			<tbody>
				@foreach($transactions as $category)
					@foreach($category->subCategories as $subCategory)
						@if($subCategory->transactions->count() > 0)
						<tr>
							<th>
								{{ $category->name }} : {{ $subCategory->name }}
							</th>
							<th colspan="2">
								
							</th>
							<th class="text-right">
								{{-- {{ number_format($subCategory->transactions->sum('amount'), 2) }} --}}
							</th>
						</tr>
						<tr>
							<th>
								Details
							</th>
							<th>
								
							</th>
							<th>
								Non-Liquidated
							</th>
							<th>
								Liquidated
							</th>
						</tr>
						@endif
						@foreach($subCategory->transactions as $transaction)
						<tr>
							<td>
								<strong>Transaction:</strong> {{ $transaction->voucher->uuid }} <strong>Item: </strong> {{ $transaction->item->name }} <strong>Client: </strong>
								@if($transaction->client)
								@if($transaction->client_type == "Resident")
								{{ $transaction->client->last_name }}, {{ $transaction->client->first_name }}
								@else
								{{ $transaction->client->name }}
								@endif
								@endif
								<strong>Beneficiary: </strong>
								@if($transaction->beneficiary)
								@if($transaction->beneficiary_type == "Resident")
								{{ $transaction->beneficiary->last_name }}, {{ $transaction->beneficiary->first_name }}
								@else
								{{ $transaction->beneficiary->name }}
								@endif
								@endif
							</td>
							<td class="text-right">
								Transaction Amount
							</td>
							<td class="text-center">
								{{ number_format($transaction->amount, 2) }}
							</td>
							<td rowspan="{{ $transaction->liquidations->count() + 3 }}" valign="top" class="text-center">
								<strong>
									{{ number_format($transaction->amount - ($transaction->amount - $transaction->liquidations->sum('amount')), 2) }}
								</strong>
							</td>
						</tr>
						@foreach($transaction->liquidations as $liquidation)
						<tr>
							<td>
								
							</td>
							<td class="text-right">
								{{ $liquidation->label }} :
							</td>
							<td class="text-center">
								{{ number_format($liquidation->amount, 2) }}
							</td>
							<td>
								
							</td>
						</tr>
						@endforeach
						<tr>
							<td class="text-right">
								<strong>Total Liquidated</strong>
							</td>
							<td class="text-right">
								<strong>
									{{ number_format($transaction->liquidations->sum('amount'), 2) }}
								</strong>
							</td>
						</tr>
						<tr>
							<td class="text-right">
								<strong>Amount returned</strong>
							</td>
							<td class="text-right">
								<strong>
									{{ number_format($transaction->amount - $transaction->liquidations->sum('amount')) }}
								</strong>
							</td>
						</tr>
						@endforeach
					@endforeach
				@endforeach
				<tr>
					<th colspan="3" class="text-right">
						Transaction Total
					</td>
					<th>
						{{ number_format($total, 2) }}
					</th>
				</tr>
				<tr>
					<th colspan="3" class="text-right">
						Liquidation Total
					</th>
					<th>
						{{ number_format($liquidated, 2) }}
					</th>
				</tr>
				<tr>
					<th colspan="3" class="text-right">
						Amount returned
					</th>
					<th>
						{{ number_format($total - $liquidated, 2) }}
					</th>
				</tr>
			</tbody>
		</table>
		<br> 
		<table width="85%" border="0" style="border-collapse:collapse;">
			<thead>
				<th style="text-align:center; border: 0; background: #fff; font-size: 15px;">Prepared By:</th>
				<th style="text-align:center; border: 0; background: #fff; font-size: 15px;">Certified By:</th>
				<th style="text-align:center; border: 0; background: #fff; font-size: 15px;">Approved By:</th>
			</thead>

			<tbody>
				<tr style="background: #fff;">
					<td style="border:0;"><br></td>
					<td style="border:0;"><br></td>
					<td style="border:0;"><br></td>
				</tr>
				<tr style="background: #fff;">
					<td  style="text-align:center; border:0; font-size: 15px">{{ $user->resident->full_name }}</td>
					<td style="border:0; font-size: 15px">{{ $signatory }}</td>
					<td  style="text-align:center; border:0; font-size: 15px">{{ $signatory }}</td>
				</tr>
				<tr>
					<td style="text-align:center; border: 0; background: #fff; font-size: 15px"></td>
					<td style="border:0; background: #fff; font-size: 15px"></td>
					<td  style="text-align:center; border: 0; background: #fff; font-size: 15px"></td>
				</tr>
			</tbody>
		</table>


	</body>
</html>