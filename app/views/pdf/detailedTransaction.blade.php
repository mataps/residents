<html>
	<head>
		<style type="text/css">
			table {
				font-family: sans-serif;
			}

			tr {
			    page-break-inside: avoid;
			}

			tbody th {
				border-bottom: 1px solid #000;
				font-size: 12px;
			}

			tbody th:first-of-type {
				text-align: left;
			}

			tbody th:last-of-type {
				text-align: right;
				padding-top: 5px;
				padding-bottom: 5px;
			}

			tbody td:last-of-type {
				text-align: right;
			}

			tbody td {
				border-right: 1px solid #ccc;
				font-size: 8px;
				padding: 5px;
			}

			tbody td:nth-of-type(2) {
				text-align: center;
			}

			thead th {
				border-bottom: 1px solid #000;
				border-top: 1px solid #000;
				padding: 5px 20px;
				font-size: 12px;
			}

			thead tr th.title {
				border: 0px;
				text-align: left;
				font-size: 18px;
				padding-left: 0px;
				padding-right: 0px;
			}

			thead tr th span.dates {
				font-size: 12px;
				font-weight: normal;
				float: right;
			}
			
			.font-title { 
				font-family: Arial; 
				font-weight: bold;
			}
			.font-data { font-family: Calibri }
		</style>
	</head>
	<body>
		<table cellpadding="0" cellspacing="0" width="100%">
			<thead>
				<tr>
					<th colspan="8" class="title">
						{{ $title }}
						<span class="dates">
							{{ $from }} - {{ $to }}
						</span>
					</th>
				</tr>
				<tr>
					<th>
						Voucher
					</th>
					<th>
						Item
					</th>
					<th>
						Client
					</th>
					<th>
						Address
					</th>
					<th>
						Beneficiary
					</th>
					<th>
						Address
					</th>
					<th>
						Details
					</th>
					<th>
						Amount
					</th>
				</tr>
			</thead>
			<tbody>
				@foreach($transactions as $category)
					@foreach($category->subCategories as $subCategory)
						@if($subCategory->transactions->count() > 0)
						<tr>
							<th colspan="7">
								{{ $category->name }} : {{ $subCategory->name }}
							</th>
							<th>
								{{ number_format($subCategory->transactions->sum('amount')) }}.00
							</th>
						</tr>
						@endif
						@foreach($subCategory->transactions as $transaction)
						<tr>
							<td style="width: 90px;">
								{{ $transaction->voucher->uuid }}
							</td>
							<td>
								{{ $transaction->item->name }}
							</td>
							<td>
								@if($transaction->client)
									{{ $transaction->client->fullName() }}
								@endif
							</td>
							<td>
								@if($transaction->client)
									{{ $transaction->client->fullAddress() }}
								@endif
							</td>
							<td>
								@if($transaction->beneficiary)
									{{ $transaction->beneficiary->fullName() }}
								@endif
							</td>
							<td>
								@if($transaction->beneficiary)
									{{ $transaction->beneficiary->fullAddress() }}
								@endif
							</td>
							<td>
								{{ $transaction->details }}
							</td>
							<td>
								{{ number_format($transaction->amount) }}.00
							</td>
						</tr>
						@endforeach
					@endforeach
				@endforeach
				<tr>
					<th colspan="7">
						Total
					</td>
					<th>
						{{ number_format($total) }}.00
					</th>
				</tr>
			</tbody>
		</table>
		<br> 
		<table width="85%" border="0" style="border-collapse:collapse;">
			<thead>
				<th style="text-align:center; border: 0; background: #fff; font-size: 12px;">Prepared by:</th>
				<th style="text-align:center; border: 0; background: #fff; font-size: 12px;">Approved by:</th>
				<th style="text-align:center; border: 0; background: #fff; font-size: 12px;">Audited by:</th>
			</thead>
			<tbody>
				<tr>
					<td style="border:0;"><br></td>
					<td style="border:0;"><br></td>
					<td style="border:0;"><br></td>
				</tr>
				<tr>
					<td style="text-align:center; border:0; font-size: 13px;">{{ strtoupper("Shirwin H. Alcantara") }}</td>
					<td style="border:0; font-size: 13px">{{ strtoupper("Vanessa A. Relucio") }}</td>
					<td style="text-align: center; border: 0; font-size: 13px;">{{ strtoupper("Epifania P. Binuya") }}</td>
				</tr>
				<tr>
					<td style="text-align:center; border: 0; background: #fff; font-size: 12px">Accounting Head</td>
					<td style="border:0; background: #fff; font-size: 12px">Chief of Staff</td>
					<td  style="text-align:center; border: 0; background: #fff; font-size: 12px">Accountant</td>
				</tr>
			</tbody>
		</table>


	</body>
</html>