<html>
	<head>
		<style type="text/css">
			table {
				font-family: sans-serif;
			}

			tr {
			    page-break-inside: avoid;
			}

			tbody th {
				border-bottom: 1px solid #000;
				font-size: 12px;
			}

			tbody th:first-of-type {
				text-align: left;
			}

			tbody th:last-of-type {
				text-align: right;
				padding-top: 5px;
				padding-bottom: 5px;
			}

			tfoot th:first-of-type {
				text-align: left;
			}

			tfoot th:last-of-type {
				text-align: right;
				padding-top: 5px;
				padding-bottom: 5px;
			}

			tbody td:last-of-type {
				text-align: right;
			}

			tbody td {
				border-right: 1px solid #ccc;
				font-size: 10px;
				padding: 5px;
			}

			tbody td:nth-of-type(2) {
				text-align: center;
			}

			thead th {
				border-bottom: 1px solid #000;
				border-top: 1px solid #000;
				padding: 5px 20px;
				font-size: 12px;
			}

			thead tr th.title {
				border: 0px;
				text-align: left;
				font-size: 18px;
				padding-left: 0px;
				padding-right: 0px;
			}

			thead tr th span.dates {
				font-size: 12px;
				font-weight: normal;
				float: right;
			}
		</style>
	</head>
	<body>
		<table cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<th colspan="8" class="title">
						{{ $income['title'] }}
						<span class="dates">
							{{ $income['from'] }} - {{ $income['to'] }}
						</span>
					</th>
				</tr>
				<tr>
					<th>
						Voucher
					</th>
					<th>
						Item
					</th>
					<th>
						Client
					</th>
					<th>
						Address
					</th>
					<th>
						Beneficiary
					</th>
					<th>
						Address
					</th>
					<th>
						Details
					</th>
					<th>
						Amount
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th colspan="7">
						Beginning cash balance
					</th>
					<th>
						{{ number_format($availableCash) }}
					</th>
				</tr>
				@foreach($income['transactions'] as $category)
					@foreach($category->subCategories as $subCategory)
						@if($subCategory->transactions->count() > 0)
						<tr>
							<th colspan="7">
								{{ $category->name }} : {{ $subCategory->name }}
							</th>
							<th>
								{{ number_format($subCategory->transactions->sum('amount')) }}
							</th>
						</tr>
						@endif
						@foreach($subCategory->transactions as $transaction)
						<tr>
							<td>
								{{ $transaction->voucher->uuid }}
							</td>
							<td>
								<!-- {{ $transaction->item->name }} -->
								{{ $transaction->transaction_type }}
							</td>
							<td>
								@if($transaction->client)
									{{ $transaction->client->fullName() }}
								@endif
							</td>
							<td>
								@if($transaction->client)
								{{ $transaction->client->fullAddress() }}
								@endif
							</td>
							<td>
								@if($transaction->beneficiary)
									{{ $transaction->beneficiary->fullName() }}
								@endif
							</td>
							<td>
								@if($transaction->beneficiary)
								{{ $transaction->beneficiary->fullAddress() }}
								@endif
							</td>
							<td>
								{{ $transaction->details }}
							</td>
							<td>
								{{ number_format($transaction->amount) }}
							</td>
						</tr>
						@endforeach
					@endforeach
				@endforeach
				<tr>
					<th colspan="7">
						Total Collections
					</td>
					<th>
						{{ number_format($income['total']) }}
					</th>
				</tr>
				<tr>
					<th colspan="7">
						Total cash available for disbursements
					</th>
					<th>
						{{ number_format($availableCash + $income['total']) }}
					</th>
				</tr>

				@foreach($expense['transactions'] as $category)
					@foreach($category->subCategories as $subCategory)
						@if($subCategory->transactions->count() > 0)
						<tr>
							<th colspan="7">
								{{ $category->name }} : {{ $subCategory->name }}
							</th>
							<th>
								{{ number_format($subCategory->transactions->sum('amount')) }}
							</th>
						</tr>
						@endif
						@foreach($subCategory->transactions as $transaction)
						<tr>
							<td>
								{{ $transaction->voucher->uuid }}
							</td>
							<td>
								<!-- {{ $transaction->item->name }} -->
								{{ $transaction->transaction_type }}
							</td>
							<td>
								@if($transaction->client)
									{{ $transaction->client->fullName() }}
								@endif
							</td>
							<td>
								@if($transaction->client)
								{{ $transaction->client->fullAddress() }}
								@endif
							</td>
							<td>
								@if($transaction->beneficiary)
									{{ $transaction->beneficiary->fullName() }}
								@endif
							</td>
							<td>
								@if($transaction->beneficiary)
								{{ $transaction->beneficiary->fullAddress() }}
								@endif
							</td>
							<td>
								{{ $transaction->details }}
							</td>
							<td>
								{{ number_format($transaction->amount) }}
							</td>
						</tr>
						@endforeach
					@endforeach
				@endforeach
				<tr>
					<th colspan="7">
						Total disbursements
					</td>
					<th>
						{{ number_format($expense['total']) }}
					</th>
				</tr>
				<tr>
					<th colspan="7">
						Ending cash balance
					</th>
					<th>
						{{ number_format($availableCash + $income['total'] - $expense['total']) }}
					</th>
				</tr>
			</tbody>
		</table>
		<table width="85%" border="0" style="border-collapse:collapse;">
			<thead>
				<th style="text-align:center; border: 0; background: #fff; font-size: 15px;">Prepared By:</th>
				<th style="text-align:center; border: 0; background: #fff; font-size: 15px;">Certified By:</th>
				<th style="text-align:center; border: 0; background: #fff; font-size: 15px;">Approved By:</th>
			</thead>

			<tbody>
				<tr style="background: #fff;">
					<td style="border:0;"><br></td>
					<td style="border:0;"><br></td>
					<td style="border:0;"><br></td>
				</tr>
				<tr style="background: #fff;">
					<td  style="text-align:center; border:0; font-size: 15px">{{ $user->resident->full_name }}</td>
					<td style="border:0; font-size: 15px">{{ $income['signatory'] }}</td>
					<td  style="text-align:center; border:0; font-size: 15px">{{ $income['signatory'] }}</td>
				</tr>
				<tr>
					<td style="text-align:center; border: 0; background: #fff; font-size: 15px"></td>
					<td style="border:0; background: #fff; font-size: 15px"></td>
					<td  style="text-align:center; border: 0; background: #fff; font-size: 15px"></td>
				</tr>
			</tbody>
		</table>
	</body>
</html>