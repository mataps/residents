<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<style type="text/css">
		
	</style>
</head>
<!-- 
<style type="text/css">
	body {
		font-family: sans-serif;
	}

	.text-center {
		text-align: center;
	}

	.name {
		font-size: 24px;
	}

	.school {
		font-size: 12px;
	}

	.title {
		font-size: 32px;
		padding-left: 40px;
		padding-right: 40px;
	}

	.table {
		border: 1px solid #ccc;
		width: 500px;
		height: 300px;
	}

	.photo {
		text-align: center;
	}

	.photo img {
		border: 2px solid #f0f0f0;
		width: 150px;
		height: 150px;
	}

	.signature {
		border-top: 1px solid #000;
	}
</style> -->

<style type="text/css">

body {
	font-family: sans-serif;
}

.id-border {
	border: 2px solid #000;
	width: 615px;
	height: 250px;
}

.scholar-pic {
	display: block;
	float: left;
	border: 1px solid #ddd;
	width: 150px;
	height: 150px;
	margin-top: 25px;
	margin-right: 10px;
	margin-left: 10px;
	margin-bottom: 25px;
}

.profile-pic {
	width: 150px;
	height: 150px;
}

.released-by {
	float: right;
	margin-top: 10px;
	margin-right: 0px;
	margin-bottom: 0px;
}

.released-by:after {
	display: block;
	content: "";
	height: 2px;
	width: 175px;
	background-color: #000;
	margin-top: 15px;
	margin-right: 10px;
	margin-bottom: 0px;
}

	.scholar-sig {
	border: 1px dashed #ddd;
	width: 105px;
	height: 30px;
	margin-top: 0px;
	margin-left: 35px;
}

.scholar-info {
	display: block;
	font-size: 11px;
	margin-top: 15px;
	margin-bottom: 0px;
}

.scholar-name {
	font-size: 12px;
	font-weight: bold;
	margin-bottom: 0px;
}

.scholar-city {
	font-size: 11px;
	font-style: italic;
	margin-top: 0px;
	margin-bottom: 0px;
}

.scholar-school {
	font-size: 12px;
	font-style: italic;
	margin-bottom: 0px;
}

.scholar-course {
	font-size: 12px;
	font-style: italic;
	margin-top: 5px;
	margin-bottom: 0px;
}

.scholar-birthdate,
.scholar-birthdate-input {
	margin-top: 25px;
	font-size: 12px;
	margin-bottom: 0px;
}

.scholar-parents-f,
.scholar-parents-f-input {
	font-size: 12px;
	margin-top: 25px;
	margin-bottom: 0px;
}

.scholar-parents-m,
.scholar-parents-m-input {
	display: block;
	font-size: 12px;
	margin-top: 5px;
	margin-bottom: 0px;
}

</style>

<body>
	<div class="id-contents">
		<div class="id-border">
			<div class="scholar-pic">
				@if($data->resident->profilePic)
				<img class="profile-pic" src="{{ base_path('public/uploads/' . str_replace(' ', '', $data->resident->cityMunicipality->name ) . '/photos/' .  $data->resident->profilePic->filename) }}" alt=""> 
				@else
				<img class="profile-pic" src="{{ base_path('public/assets/img/user.gif') }}" alt="">
				@endif
			</div>

			<div class="released-by">
					<p class="released-by-title">Released by:</p>
			</div>

			<table class="scholar-info">
				<tr>
					<td>
						<p class="scholar-name"> {{ $data->resident->full_name }}</p>
					</td>
				</tr>

				<tr>
					<td>
						<p class="scholar-city"></p>
					</td>
				</tr>

				<tr>
					<td>
						<p class="scholar-school">{{ $data->school->name }}</p>
					</td>
				</tr>
					
				<tr>
					<td>
						<p class="scholar-course"></p>
					</td>
				</tr>

				<tr>
					<td>
						<p class="scholar-birthdate">Birthday: </p>
					</td>
					<td>
						<p class="scholar-birthdate-input"> {{ date('Y/m/d', strtotime($data->resident->birthdate)) }}</p>
					</td>
				</tr>
					
				<tr>
					<td>
						<p class="scholar-parents-f">Father:</p>
					</td>
					<td>
						<p class="scholar-parents-f-input"></p>
					</td>
				</tr>

				<tr>
					<td>
						<p class="scholar-parents-m">Mother:</p>
					</td>
					<td>
						<p class="scholar-parents-m-input"></p>
					</td>
				</tr>
			</table>

			<div class="scholar-sig"></div>

		</div>
	</div>
</body>
</html>
