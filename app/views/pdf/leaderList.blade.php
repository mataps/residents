<html>
<head>
	<style>
		table {
			font-family: sans-serif;
			width: 100%;
		}

		tr {
			    page-break-inside: avoid;
			}

		tbody tr td {
			font-size: 12px;
		}

		tbody th {
			font-size: 12px;
		}

		tbody th:first-of-type {
			text-align: center;
		}

		tbody td {
			font-size: 10px;
			padding: 5px;
		}

		thead th {
			border-bottom: 1px solid #000;
			border-top: 1px solid #000;
			padding: 5px 0px;
			font-size: 12px;
		}

		thead tr th {
			font-size: 14px;
			text-align: left;
		}

		tr:nth-child(even) {
			background-color: #fff;
		}

		tr:nth-child(odd) {
			background-color: #eee;
		}

		.font-title { 
			font-family: Arial; 
			font-weight: bold;
		}
		.font-data { font-family: Calibri }
	</style>
</head>
<body>
	

<table border="0" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<th>
				No.
			</th>
			<th>
				GWA
			</th>
			<th>
				Lastname
			</th>
			<th>
				Firstname
			</th>
			<th>
				Maternalname
			</th>
			<th>
				School
			</th>
		</tr>
	</thead>
	<tbody>
		@foreach($scholarships as $index => $scholarship)
		<tr>
			<td>
				{{ ++$index }}
			</td>
			<td>
				{{ $scholarship->gwa }}
			</td>
			<td>
				{{ $scholarship->resident->last_name }}
			</td>
			<td>
				{{ $scholarship->resident->first_name }}
			</td>
			<td>
				{{ $scholarship->resident->middle_name }}
			</td>
			<td>
				{{ $scholarship->school->name }}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
</body>
</html>