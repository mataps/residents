<!DOCTYPE html>
<html>
<head>
	<title>Reports</title>
	<style>
		table.main { border: 1px solid #000; }
		
		tr {
			    page-break-inside: avoid;
			}

		td.photo { width: 60px; }

		td.photo img {
			width: 60px;
			height: 60px;
			margin-right: 5px;
		}

		.text-center { text-align: center; }
		.text-right { text-align: right; }
		.text-left { text-align: left; }
		.pull-left { float: left; }
		.pull-right { float: right; }
		.pull-center { float: center; }
	</style>
</head>
<body>
	<table width="100%" class="main">
		<thead>
			<tr>
				<th class="text-left" colspan="6">
					<div class="pull-left"> Affiliation Name </div>
					<div class="pull-right" style="padding-right: 20%"> Date </div>
				</th>
			</tr>
			<tr width="10%">
				<th class="text-left" colspan="1">Town</th>
			</tr>
			<tr>
				<th class="text-left" style="padding-left: 8%" colspan="1">Barangay</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td width="10%" style="padding-left: 3%" valign="top">
					<img src="http://placehold.it/60x60">
				</td>
				<td width="5%" valign="top">
					<span>Name:<br></span>
					<span>Address:<br><br><br></span>
					<span>Date of Birth:<br></span>
					<span>Position:<br></span>
				</td>
				<td width="22%" valign="top">
					<span>{{ $residents->full_name }}<br></span>
					<span>{{ $residents->fullAddress() }}<br></span>
					<span>{{ $residents->birthdate }}<br><br></span>
					<span>positions<br></span>
				</td>
				<td width="10%" style="padding-left: 3%" valign="top">
					<img src="http://placehold.it/60x60">
				</td>
				<td width="5%" valign="top">
					<span>Name:<br></span>
					<span>Address:<br><br><br></span>
					<span>Date of Birth:<br></span>
					<span>Position:<br></span>
				</td>
				<td width="22%" valign="top">
					<span>{{ $residents->full_name }}<br></span>
					<span>{{ $residents->fullAddress() }}<br></span>
					<span>{{ $residents->birthdate }}<br><br></span>
					<span>positions<br></span>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>