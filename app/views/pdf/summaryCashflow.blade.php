<html>
	<head>
		<style type="text/css">
			table {
				font-family: sans-serif;
				width: 100%;
			}

			tr {
			    page-break-inside: avoid;
			}

			tbody th {
				border-bottom: 1px solid #000;
				font-size: 12px;
			}

			tbody th:first-of-type {
				text-align: left;
			}

			tbody td:last-of-type {
				text-align: right;
			}

			tfoot th:first-of-type {
				text-align: left;
			}

			tfoot td:last-of-type {
				text-align: right;
			}



			tbody td {
				font-size: 10px;
				padding: 5px;
			}

			tbody td:nth-of-type(2) {
				text-align: center;
			}

			thead th {
				border-bottom: 1px solid #000;
				border-top: 1px solid #000;
				padding: 5px 20px;
				font-size: 12px;
			}

			thead tr th.title {
				border: 0px;
				text-align: left;
				font-size: 18px;
				padding-left: 0px;
				padding-right: 0px;
			}

			thead tr th span.dates {
				font-size: 12px;
				font-weight: normal;
			}
		</style>
	</head>
	<body>
		<table cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<th colspan="2" class="title" style="text-align: right;">
						<span style="float: left;">
						{{ $income['title'] }}
						</span>
						<span class="dates">
							{{ $income['from'] }} - {{ $income['to'] }}
						</span>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th>
						Beginning cash balance
					</th>
					<th style="text-align: right;">
						{{ number_format($availableCash, 2) }}
					</th>
				</tr>
				<tr>
				    <th colspan="2">
				        Add:
				    </th>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				@foreach($income['transactions'] as $category)
					@if($category->transactions->count() > 0)
					<tr>
						<th>
							{{ $category->name }}
						</th>
						<th style="text-align:right;">
							{{ number_format($category->transactions->sum('amount'), 2) }}
						</th>
					</tr>
					@foreach($category->subCategories as $subCategory)
					    @if($subCategory->transactions->sum('amount') !== 0)
						@if($subCategory->transactions->count() > 0)
						<tr>
							<td style="padding-left: 40px;">
								{{ $subCategory->name }}
							</td>
							<td style="text-align: right; padding-right: 100px;">
								{{ number_format($subCategory->transactions->sum('amount'), 2) }}
							</td>
						</tr>
						@endif
						@endif
					@endforeach
					@endif
				@endforeach
				<tr>
					<th>
						Total Collections
					</td>
					<th style="text-align: right;">
						{{ number_format($income['total'], 2) }}
					</th>
				</tr>
				<tr>
					<th>
						Total Cash Available For Disbursements
					</th>
					<th style="text-align: right;">
						{{ number_format($availableCash + $income['total'], 2) }}
					</th>
				</tr>
				<tr>
				    <th colspan="2">
				        Less:
				    </th>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				@foreach($expense['transactions'] as $category)
					@if($category->transactions->count() > 0)
					<tr>
						<th>
							{{ $category->name }}
						</th>
						<th style="text-align: right;">
							{{ number_format($category->transactions->sum('amount'), 2) }}
						</th>
					</tr>
					@foreach($category->subCategories as $subCategory)
						@if($subCategory->transactions->sum('amount') !== 0)
						<tr>
							<td style="padding-left: 40px;">
								{{ $subCategory->name }}
							</td>
							<td style="text-align: right; padding-right: 100px;">
								{{ number_format($subCategory->transactions->sum('amount'), 2) }}
							</td>
						</tr>
						@endif
					@endforeach
					@endif
				@endforeach
				<tr>
					<th>
						Total Disbursements
					</td>
					<th style="text-align: right;">
						{{ number_format($expense['total'], 2) }}
					</th>
				</tr>
				<tr>
					<td colspan="2">
					&nbsp;
					</td>
				</tr>
				<tr>
					<th>
						Ending Cash Balance
					</th>
					<th style="text-align: right; font-size: 14px;">
						{{ number_format($availableCash + $income['total'] - $expense['total'], 2) }}
					</th>
				</tr>
			</tbody>
		</table>
		<br>
		<table width="100%" border="0" style="border-collapse:collapse;" align="center">
			<thead>
				<th style="text-align:center; border: 0; background: #fff; font-size: 12px;">Prepared by:</th>
				<th style="text-align:center; border: 0; background: #fff; font-size: 12px;">Approved by:</th>
				<th style="text-align:center; border: 0; background: #fff; font-size: 12px;">Audited by:</th>
			</thead>
			<tbody>
				<tr>
					<td style="border:0;"><br></td>
					<td style="border:0;"><br></td>
					<td style="border:0;"><br></td>
				</tr>
				<tr>
					<td style="text-align:center; border:0; font-size: 13px;">{{ strtoupper("Shirwin H. Alcantara") }}</td>
					<td style="border:0; font-size: 13px">{{ strtoupper("Vanessa A. Relucio") }}</td>
					<td style="text-align: center; border: 0; font-size: 13px;">{{ strtoupper("Epifania P. Binuya") }}</td>
				</tr>
				<tr>
					<td style="text-align:center; border: 0; background: #fff; font-size: 12px">Accounting Head</td>
					<td style="border:0; background: #fff; font-size: 12px">Chief of Staff</td>
					<td  style="text-align:center; border: 0; background: #fff; font-size: 12px">Accountant</td>
				</tr>
			</tbody>
		</table>
	</body>
</html>
