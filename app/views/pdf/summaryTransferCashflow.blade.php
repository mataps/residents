<html>
	<head>
		<style type="text/css">
			table {
				font-family: sans-serif;
				width: 100%;
			}

			tr {
			    page-break-inside: avoid;
			}

			tbody th {
				border-bottom: 1px solid #000;
				font-size: 12px;
			}

			tbody th:first-of-type {
				text-align: left;
			}

			tbody td:last-of-type {
				text-align: right;
			}

			tfoot th:first-of-type {
				text-align: left;
			}

			tfoot td:last-of-type {
				text-align: right;
			}



			tbody td {
				font-size: 10px;
				padding: 5px;
			}

			tbody td:nth-of-type(2) {
				text-align: center;
			}

			thead th {
				border-bottom: 1px solid #000;
				border-top: 1px solid #000;
				padding: 5px 20px;
				font-size: 12px;
			}

			thead tr th.title {
				border: 0px;
				text-align: left;
				font-size: 18px;
				padding-left: 0px;
				padding-right: 0px;
			}

			thead tr th span.dates {
				font-size: 12px;
				font-weight: normal;
				float: right;
			}
		</style>
	</head>
	<body>
		<table cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<th colspan="2" class="title">
						{{ $income['title'] }}
						<span class="dates">
							{{ $income['from'] }} - {{ $income['to'] }}
						</span>
					</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<th>
						Beginning cash balance
					</th>
					<th>
						{{ number_format($availableCash) }}
					</th>
				</tr>
				@foreach($income['transactions'] as $category)
					@if($category->transactions->count() > 0)
					<tr>
						<th>
							{{ $category->name }}
						</th>
						<th>
							{{ number_format($category->transactions->sum('amount')) }}.00
						</th>
					</tr>
					@foreach($category->subCategories as $subCategory)
						@if($subCategory->transactions->count() > 0)
						<tr>
							<td>
								{{ $subCategory->name }}
							</td>
							<td>
								{{ number_format($subCategory->transactions->sum('amount')) }}.00
							</td>
						</tr>
						@endif
					@endforeach
					@endif
				@endforeach
				<tr>
					<th>
						Total Collections
					</td>
					<th>
						{{ number_format($income['total']) }}.00
					</th>
				</tr>
				<tr>
					<th>
						Total cash available for disbursements
					</th>
					<th>
						{{ number_format($availableCash + $income['total']) }}
					</th>
				</tr>
				@foreach($expense['transactions'] as $category)
					@if($category->transactions->count() > 0)
					<tr>
						<th>
							{{ $category->name }}
						</th>
						<th>
							{{ number_format($category->transactions->sum('amount')) }}
						</th>
					</tr>
					@foreach($category->subCategories as $subCategory)
						
						<tr>
							<td>
								{{ $subCategory->name }}
							</td>
							<td>
								{{ number_format($subCategory->transactions->sum('amount')) }}
							</td>
						</tr>
						
					@endforeach
					@endif
				@endforeach
				<tr>
					<th>
						Total disbursements
					</td>
					<th>
						{{ number_format($expense['total']) }}
					</th>
				</tr>
				<tr>
					<th>
						Ending cash balance
					</th>
					<th>
						{{ number_format($availableCash + $income['total'] - $expense['total']) }}
					</th>
				</tr>
			</tbody>
		</table>
		<br> 
		<table width="85%" border="0" style="border-collapse:collapse;">
			<thead>
				<th style="text-align:center; border: 0; background: #fff; font-size: 15px;">Prepared By:</th>
				<th style="text-align:center; border: 0; background: #fff; font-size: 15px;">Certified By:</th>
				<th style="text-align:center; border: 0; background: #fff; font-size: 15px;">Approved By:</th>
			</thead>

			<tbody>
				<tr style="background: #fff;">
					<td style="border:0;"><br></td>
					<td style="border:0;"><br></td>
					<td style="border:0;"><br></td>
				</tr>
				<tr style="background: #fff;">
					<td  style="text-align:center; border:0; font-size: 15px">Brian</td>
					<td style="border:0; font-size: 15px">Brian</td>
					<td  style="text-align:center; border:0; font-size: 15px">Brian</td>
				</tr>
				<tr>
					<td style="text-align:center; border: 0; background: #fff; font-size: 15px">Accountant</td>
					<td style="border:0; background: #fff; font-size: 15px">Accountant</td>
					<td  style="text-align:center; border: 0; background: #fff; font-size: 15px">Accountant</td>
				</tr>
			</tbody>
		</table>
	</body>
</html>