<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
		table.main {
			border: 1px solid #000;
			text-align: left;
			font-family: Arial;
			font-size: 12px;
		}

		tr {
			    page-break-inside: avoid;
			}

		.text-center {
			text-align: center;
		}

		.text-right {
			text-align: right;
		}

		.border-top {
			border-top: 1px solid #000;
		}

		.border-bottom {
			border-bottom: 1px solid #000;
		}

		td.photo {
			width: 60px;
		}

		td.photo img {
			width: 60px;
			height: 60px;
			margin-right: 8px;
		}

		th {
			padding: 10px 0px;
		}

		tr.transaction:nth-of-type(even) {
			background: #eee;
		}

		td {
			vertical-align: top;
			padding: 5px 0px;
		}
	</style>
</head>
<body>

<table width="100%" class="main">
		<tr>
			<th style="font-size: 18px">
				<span style="margin-left: 10px">{{ $transaction->voucher->account->name }}</span>
				<span style="float: right; margin-left: 25%; padding-right: 25%; display: inline">Disbursement Voucher</span>
			</th>
			<th style="font-size: 18px; padding-right: 10px" class="text-right">
				<span>{{ $transaction->voucher->uuid }}</span>
			</th> 
		</tr>
		
		<tr>
			<th colspan="2" class="details text-center border-top border-bottom">
				Transaction Details
			</th>
		</tr>
	<tr class="transaction">
		<td>
			<table width="100%">
				<tr>
					<td width="10%">
						<strong>Particular</strong>
					</td>
					<td width="40%" colspan="2">
						{{ $transaction->sub_category->category->name }} - {{ $transaction->sub_category->name }}
					</td>
					<td width="10%">
						<strong>Item</strong>
					</td>
					<td width="40%" colspan="2">
						{{ $transaction->item->name }}
						@if(isset($transaction->liquidated_at))
						<br>
						<span>Liquidated Items:</span>
						<ul>
							@foreach($transaction->liquidations as $liquidation)
							<li>
								{{ $liquidation->amount }} - {{ $liquidation->label }}
							</li>
							@endforeach
						</ul>
						@endif
					</td>
				</tr>
				<tr>
					<td>
						<strong>Details</strong>
					</td>
					<td colspan="2">
						{{ $transaction->details }}
					</td>
					<td>
						<strong>Remarks</strong>
					</td>
					<td colspan="2">
						{{ $transaction->remarks }}
					</td>
				</tr>
				<tr>
					<td>
						<strong>Client Info</strong>
					</td>
					<td class="photo">
						@if($transaction->voucher->client)	
							@if($transaction->voucher->client->profilePic)
							@if($transaction->voucher->client->profilePic->filename)
								@if(file_exists(base_path('public/uploads/' . $transaction->client->cityMunicipality->name . '/photos/' . $transaction->client->profilePic->filename)))
									<img width="50px" src="{{ base_path('public/uploads/' . $transaction->client->cityMunicipality->name . '/photos/' . $transaction->client->profilePic->filename) }}" alt="">
								@else
									<img src="{{ base_path('public/assets/img/user.gif') }}" alt="">
								@endif
							@else
							<img src="{{ base_path('public/assets/img/user.gif') }}" alt="">
							@endif
							@endif
						@endif
					</td>
					<td>
						@if($transaction->client)
							{{ $transaction->client->full_name }}
							<br>
							<span style="font-style: italic; font-size: 10px">{{ $transaction->client->fullAddress() }}</span>
						@endif
					</td>
					<td>
						<strong>Beneficiary Info</strong>
					</td>
					<td class="photo">
					@if($transaction->beneficiary)
						@if($transaction->beneficiary_type == "Resident")
							@if($transaction->beneficiary->profilePic)
							@if($transaction->beneficiary->profilePic->filename)
								@if(file_exists(base_path('public/uploads/' . $transaction->beneficiary->cityMunicipality->name . '/photos/' . $transaction->beneficiary->profilePic->filename)))
									<img width="50px" src="{{ base_path('public/uploads/' . $transaction->beneficiary->cityMunicipality->name . '/photos/' . $transaction->beneficiary->profilePic->filename) }}" alt="">
								@else
									<img src="{{ base_path('public/assets/img/user.gif') }}" alt="">
								@endif
							@else
								<img src="{{ base_path('public/assets/img/user.gif') }}" alt="">
							@endif
							@endif
						@endif
					@endif
					</td>
					<td>
						@if($transaction->beneficiary)
						{{ $transaction->beneficiary->fullName() }}
						<br>
						<span style="font-style: italic; font-size: 10px">{{ $transaction->beneficiary->fullAddress() }}</span>
						@endif
					</td>
				</tr>
				<tr>
					<td colspan="7">
						
					</td>
				</tr>
			</table>
		</td>
		<td class="text-center" width="10%">
			<span style="display: inline"> Php</span> {{ number_format($transaction->amount, 2) }}
		</td>
	</tr>
	<tr>
		<td>
			<strong style="float: right">Total:</strong>
		</td>
		<td class="text-right" style="font-size: 18px">
			<strong>
				<span style="display: inline"> Php</span> {{ number_format($transaction->amount, 2) }}
			</strong>
		</td>
	</tr>	
</table>
<table width="100%" class="main" style="padding: 10px" height="20">
	<tbody>
		<tr>
			<td width="33%" style="padding-left: 4%;">
				<strong>{{ $transaction->voucher->account->label }}</strong>
				<br>
				<br>
				<br>
			</td>
			<td width="33%" style="padding-left: 4%;">
				@if($transaction->voucher->account->label_2 !== "" && $transaction->voucher->account->label_2 !== null)
				<strong>{{ $transaction->voucher->account->label_2 }}</strong>
				<br>
				<br>
				<br>
				@endif
			</td>
			<td width="33%" style="padding-left: 4%;">				
				<strong>Received By : </strong>
				<br>
				<br>
				<br>
			</td>
		</tr>
		<tr>
			<td width="33%" style="padding-left: 1%;" align="center">
				<hr width="55%" style="">
			</td>
			<td width="33%" style="padding-left: 1%;" align="center">
				@if($transaction->voucher->account->label_2 !== "" && $transaction->voucher->account->label_2 !== null)
				<hr width="55%" style="">
				@endif
			</td>
			<td width="33%" style="padding-left: 1%;" align="center">
				<hr width="55%" style="">
			</td>
		</tr>		
		<tr>
			<td width="33%" align="center" style="">
				<strong>{{ $transaction->voucher->account->signatory }}</strong>
			</td>
			<td width="33%" align="center" style="">
				@if($transaction->voucher->account->label_2 !== "" && $transaction->voucher->account->label_2 !== null)
				<strong>{{ $transaction->voucher->account->signatory_2 }}</strong>
				@endif
			</td>
			<td width="33%" align="center" style="">
				@if($transaction->voucher->client)
				<strong>{{ $transaction->voucher->client->fullName() }}</strong>
				@endif
			</td>
		</tr>

		<tr>
			<td width="33%" align="center" >{{ $transaction->voucher->account->designation }}</td>
			<td width="33%" align="center">{{ $transaction->voucher->account->designation_2 }}</td>	
			<td width="33%" align="center">Client</td>				
		</tr>
	</tbody>
</table>

</body>
</html>
