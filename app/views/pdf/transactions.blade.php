<html>
<!DOCTYPE html>
<head>
	<style type="text/css">
		body {
			font-family: Arial;
		}

		div.page {
			width: 100%;
		}

		div.page:nth-of-type(even)
		{
			page-break-after: always;
		}

		div.page:last-of-type {
			page-break-after: avoid;
		}

		div.header {
			height: 40px;
			border-top: 1px solid #000;
			border-left: 1px solid #000;
			border-right: 1px solid #000;
			border-bottom: 1px solid #000;
			padding-left: 10px;
			padding-right: 10px;
			text-align: center;
		}

		div.footer {
			height: 80px;
			border-left: 1px solid #000;
			border-right: 1px solid #000;
			border-bottom: 1px solid #000;
		}

		div.transaction table {
			font-size: 12px;
		}

		div.transaction {
			border-bottom: 1px solid #000;
			padding-left: 10px;
			padding-right: 10px;
			height: 150px;
			border-left: 1px solid #000;
			border-right: 1px solid #000;
		}

		div.signatory {
			width: 33%;
			height: 80px;
			float: right;
		}

		div.official {
			border-top: 1px solid #000;
			width: 90%;
			text-align: center;
			font-size: 12px;
			height: 35px;
		}

		span.position {
			display: block;
			font-size: 10px;
		}

		div.label {
			font-size: 10px;
			height: 45px;
		}

		div.total {
			border-left: 1px solid #000;
			border-right: 1px solid #000;
			border-bottom: 1px solid #000;
			font-size: 12px;
			padding-left: 10px;
			padding-right: 10px;
			padding-top: 5px;
			padding-bottom: 5px;
			text-align: right;
		}

		div.total span.amount {
			float: right;
		}

		div.photo {
			 width: 50px;
			 min-height: 50px;
			 background: #ccc;
			 float: left;
			 margin-right: 5px;
		}
	</style>
</head>
<body>
	@foreach($data as $index => $transaction)
	<div class="page">
		@if($index % 2 == 0)
		<div class="header" style="padding-top:10px;">
			<span style="float: left; font-size: 16px;">
				
			</span>
			Disbursement Voucher
			<span style="float: right; font-size: 12px; text-align: right;">
				
				<br>
				@if($data->count() > 1)
				Page {{ round(($index + 1) / round($data->count() / 2), 0, PHP_ROUND_HALF_DOWN) + 1 }} of {{ round($data->count() / 2) }}
				@endif
			</span>
		</div>	
		@endif
		
		<div class="transaction">
			<table style="width: 100%;">
				<tr>
					<td valign="top" style="font-size: 10px; width: 10%;">
						<strong>Particular</strong>
					</td>
					<td style="width: 30%;">
						{{ $transaction->sub_category->category->name }} - {{ $transaction->sub_category->name }}
					</td>
					<td valign="top" style="font-size: 10px; width: 10%;">
						<strong>Item</strong>
					</td>
					<td style="width: 30%;">
						{{ $transaction->item->name }}
					</td>
					<td style="text-align: right; font-size: 12px;">
						PHP {{ number_format($transaction->amount, 2) }}
					</td>
				</tr>
				<tr>
					<td valign="top" style="font-size: 10px;">
						<strong>Details</strong>
					</td>
					<td style="font-size: 10px;" valign="top">
						{{ $transaction->details }}
					</td>
					<td valign="top" style="font-size: 10px;">
						<strong>Remarks</strong>
					</td>
					<td style="font-size: 10px;" valign="top">
						{{ $transaction->remarks }}
					</td>
				</tr>
				<tr>
					<td valign="top" style="font-size: 10px;">
						<strong>Client Info</strong>
					</td>
					<td>
						@if($transaction->client->profilePic)
						@if(file_exists(base_path('public/uploads/' . $transaction->client->cityMunicipality->name . '/photos/' . $transaction->client->profilePic->filename)))
						<div class="photo">
							<img src="{{ base_path('public/uploads/' . $transaction->client->cityMunicipality->name . '/photos/' . $transaction->client->profilePic->filename) }}" alt="" style="width: 50px;">
						</div>
						@endif
						@endif
						{{ $transaction->client->fullName() }}
						<br>
						<span style="font-style: italic; font-size: 10px">{{ $transaction->voucher->client->fullAddress() }}</span>
					</td>
					<td valign="top" style="font-size: 10px;">
						<strong>Beneficiary Info</strong>
					</td>
					<td>
						@if($transaction->beneficiary)
						@if($transaction->beneficiary_type == "Resident")
						@if($transaction->beneficiary->profilePic)
						@if(file_exists(base_path('public/uploads/' . $transaction->beneficiary->cityMunicipality->name . '/photos/' . $transaction->beneficiary->profilePic->filename)))
						<div class="photo">
							<img src="{{ base_path('public/uploads/' . $transaction->beneficiary->cityMunicipality->name . '/photos/' . $transaction->beneficiary->profilePic->filename) }}" alt="" style="width: 50px;">
						</div>
						@endif
						@endif
						@endif
						{{ $transaction->beneficiary->fullName() }}
						<br>
						<span style="font-style: italic; font-size: 10px">{{ $transaction->beneficiary->fullAddress() }}</span>
						@endif
					</td>
				</tr>
			</table>
		</div>
		@if($index == $data->count() - 1)
		
		<div class="total">
			<span style="float:left; ">
				Expense total
			</span>
			PHP {{ number_format($total['expense']) }}
		</div>

		<div class="total">
			<span style="float:left; ">
				Income total
			</span>
			PHP {{ number_format($total['income']) }}
		</div>
		<div class="total">
			<span style="float: left;">
				@if($total['total'] > 0)
					Cashout
				@else
					Cashin
				@endif
			</span>
			PHP {{ number_format(abs($total['total']), 2) }}
		</div>
		</div>
		@endif
	</div>
	@endforeach
</body>
</html>