<html>
<head>
    <title>SigPlusWeb</title>
</head>
<script type="text/javascript">
    var Index;

    function pluginLoaded()
    {
        document.getElementById('sigplus').tabletState = 1;
        document.getElementById('sigplus').captureMode = 1;
        Index = setInterval(Refresh, 50);

        document.getElementById('sigplus').antiAliasSpotSize = .85;
        document.getElementById('sigplus').antiAliasLineScale = .55;
    }

    function onClear()
    {
        document.getElementById('sigplus').clearSignature();
    }


    function onDone()
    {
        if(document.getElementById('sigplus').totalPoints==0)
        {
            alert("Please sign before continuing");
            return false;
        }
        else
        {
            document.getElementById('sigplus').tabletState = 0;
            clearInterval(Index);

            //RETURN TOPAZ-FORMAT SIGSTRING
            document.getElementById('sigplus').compressionMode=1;

            //RETURN BMP BYTE ARRAY CONVERTED TO HEXADECIMAL STRING
            document.getElementById('sigplus').imageXSize = 500;
            document.getElementById('sigplus').imageYSize = 100;
            document.getElementById('sigplus').penWidth = 5;
            //SigPlus1.JustifyMode = 5;
            var bmpString = '';
            document.getElementById('sigplus').bitmapBufferWrite(5);
            var bmpSize = document.getElementById('sigplus').bitmapBufferSize();
            for(var a = 0; a < bmpSize; a++)
            {
                var byte = document.getElementById('sigplus').bitmapBufferByte(a).toString(16);
                if(byte.length === 1)
                {
                    bmpString += '0';
                }
                bmpString += byte;
            }
            document.SigForm.sigImgData.value=bmpString;
            //this example returns a bitmap image converted to a hexadecimal string
            //convert the string back to a byte array on the server for final imaging

            return bmpString;
        }

    }

    function submitForm()
    {
        document.SigForm.submit();
    }

    function Refresh()
    {
        document.getElementById('sigplus').refreshEvent();
    }


</script>
<style>
    body {
        margin: 0;
        overflow: hidden;
        width: 500px;
        height: 100px;
    }
    #sigplus {
        margin-left: -30px;
    }
    form {
        text-align: center;
    }
</style>
<body>
    <object id="sigplus"  type="application/sigplus" width="500" height="100">
        <param name="onload" value="pluginLoaded" />
    </object>

    <FORM ACTION="/sigplus" METHOD="post" NAME="SigForm">
            <input type="hidden" NAME="sigImgData" id="sigImgData">
    </FORM>
</body>

</html>