
<table>
    <thead>
        <tr>
	<th>No.</th>
        <th>Last Name</th>
        <th>First Name</th>
        <th>Middle Name</th>
        <th>Age</th>
        <th>Sex</th>
        <th>Street</th>
        <th>Brgy./Municipality/City</th>
        <th>Type of Assistance</th>
        <th>Amount</th>
        </tr>
    </thead>
    <tbody>
	@foreach($semesters as $index => $semester)
            <tr>
            <td>{{ $index + 1 }}</td>
            <td>{{ $semester->client ? $semester->client->last_name : ''}}</td>
            <td>{{ $semester->client ? $semester->client->first_name : ''}}</td>
            <td>{{ $semester->client ? $semester->client->middle_name : ''}}</td>
            <td>{{ $semester->client ? $semester->client->getAge() : ''}}</td>
            <td>{{ $semester->client ? $semester->client->gender : ''}}</td>
            <td>{{ $semester->client ? $semester->client->street : ''}}</td>
            <td>{{ $semester->client ? $semester->client->barangay->name : '' }} / {{ $semester->client ? $semester->client->cityMunicipality->name : '' }}</td>
            <td>Type Of Assistance</td>
            <td>{{ number_format($semester->computeAllowance($release), 2) }}</td>
            
        </tr>
	@endforeach
        </tbody>
</table>
