<table>
	<thead>
		<tr>
			<th>
				No.
			</th>
			<th>
				Lastname
			</th>
			<th>
				Firstname
			</th>
			<th>
				Maternalname
			</th>
			<th>
				Address
			</th>
			<th>
				Amount
			</th>
		</tr>
	</thead>
	<tbody>
		@foreach($scholarships as $index => $scholarship)
		<tr>
			<td>
				{{ ++$index }}
			</td>
			<td>
				{{ $scholarship->resident->last_name }}
			</td>
			<td>
				{{ $scholarship->resident->first_name }}
			</td>
			<td>
				{{ $scholarship->resident->middle_name }}
			</td>
			<td>
				{{ $scholarship->resident->cityMunicipality->name }}, {{ $scholarship->resident->barangay->name }}
			</td>
			<td>
				{{ $scholarship->semesters->first()->allowances->sum('amount') }}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>