<html>
	<head>
		<style type="text/css">
			table {
				font-family: sans-serif;
			}

			tbody th {
				border-bottom: 1px solid #000;
				font-size: 12px;
			}

			tbody th:first-of-type {
				text-align: left;
			}

			tbody th:last-of-type {
				text-align: right;
				padding-top: 5px;
				padding-bottom: 5px;
			}

			tbody td:last-of-type {
				text-align: right;
			}

			tbody td {
				border-right: 1px solid #ccc;
				font-size: 8px;
				padding: 5px;
			}

			thead th {
				border-bottom: 1px solid #000;
				border-top: 1px solid #000;
				font-size: 12px;
			}

			.text-right {
				text-align: right;
			}
		</style>
	</head>
	<body>
		<table cellpadding="0" cellspacing="0">
			<thead>
				<tr>
					<th style="width: 90px;">
						Voucher
					</th>
					<th>
						Item
					</th>
					<th>
						Client
					</th>
					<th>
						Address
					</th>
					<th>
						Beneficiary
					</th>
					<th>
						Address
					</th>
					<th>
						Details
					</th>
					<th>
						Amount
					</th>
				</tr>
			</thead>
			<tbody>
				@foreach($transactions as $category)
					@foreach($category->subCategories as $subCategory)
						@if($subCategory->transactions->count() > 0)
						<tr>
							<th colspan="7">
								{{ $category->name }} : {{ $subCategory->name }}
							</th>
							<th>
								{{ number_format($subCategory->transactions->sum('amount')) }}.00
							</th>
						</tr>
						@endif
						@foreach($subCategory->transactions as $transaction)
						<tr>
							<td>
								{{ $transaction->voucher->uuid }}
							</td>
							<td>
								{{ $transaction->item->name }}
							</td>
							<td>
								@if($transaction->client)
									{{ $transaction->client->fullName() }}
								@endif
							</td>
							<td>
								@if($transaction->client)
								{{ $transaction->client->fullAddress() }}
								@endif
							</td>
							<td>
								@if($transaction->beneficiary)
									{{ $transaction->beneficiary->fullName() }}
								@endif
							</td>
							<td>
								@if($transaction->beneficiary)
								{{ $transaction->beneficiary->fullAddress() }}
								@endif
							</td>
							<td>
								{{ $transaction->details }}
							</td>
							<td>
								{{ $transaction->amount }}
							</td>
						</tr>
						@endforeach
					@endforeach
				@endforeach
				<tr>
					<th colspan="7" class="text-right">
						Total
					</td>
					<th>
						{{ $total }}
					</th>
				</tr>
			</tbody>
		</table>
	</body>
</html>