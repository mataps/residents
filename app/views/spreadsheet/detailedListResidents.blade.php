<html>
    <head>
        <style type="text/css">
            table { font-family: sans-serif; }
 
            tbody th {
                border-bottom: 1px solid #000;
                font-size: 12px;
            }
 
            tbody th:first-of-type { text-align: left; }
 
            tbody th:last-of-type {
                text-align: right;
                padding-top: 5px;
                padding-bottom: 5px;
            }
 
            tbody td {
                border-right: 1px solid #ccc;
                font-size: 10px;
                padding: 5px;
            }
 
            thead th {
                border-bottom: 1px solid #000;
                border-top: 1px solid #000;
                padding: 5px 20px;
                font-size: 12px;
            }
 
            thead tr th.title {
                       
                text-align: left;
                font-size: 18px;
                padding-left: 0px;
                padding-right: 0px;
            }
 
            thead tr th span.dates {
                font-size: 12px;
                font-weight: normal;
                float: right;
            }
        </style>
    </head>
    <body>
        <table cellpadding="0" cellspacing="0">
            <thead>
                <tr>
                    <th colspan="8" class="title">Title</th>
                </tr>
                <tr>
                    <th> First Name </th>
                    <th> Maternal Name </th>
                    <th> Last Name </th>
                    <th> Voter </th>
                    <th> Email </th>
                    <th> Precint </th>
                    <th> Birthdate </th>
                    <th> Gender </th>
                    <th> Municipality </th>
                    <th> District </th>
                    <th> Barangay </th>
                    <th> Street </th>
                </tr>
                               
            </thead>
            <tbody>
                @foreach($residents as $resident)
                <tr>
                    <td> {{ $resident->first_name }} </td>
                    <td> {{ $resident->middle_name }} </td>
                    <td> {{ $resident->last_name }} </td>
                    <td> {{ $resident->is_voter ? "yes" : "no" }} </td>
                    <td> {{ $resident->email }} </td>
                    <td> {{ $resident->precint }} </td>
                    <td> {{ $resident->birthdate }} </td>
                    <td> {{ $resident->gender }} </td>
                    <td> {{ $resident->cityMunicipality ? $resident->cityMunicipality->name : '' }} </td>
                    <td> {{ $resident->district ? $resident->district->name : '' }} </td>
                    <td> {{ $resident->barangay ? $resident->barangay->name : '' }} </td>
                    <td> {{ $resident->street }} </td>
                                               
                </tr>
                @endforeach
            </tbody>
        </table>
    </body>
</html>