<!DOCTYPE html>
<html>
<head>
	<title>MasterList Scholarships</title>
	<style>
		table {
			font-family: sans-serif;
			width: 100%;
		}

		tr { 
			text-align: center;
		}

		tr th {
			width: 25px;
		}

		tr td {
			width: 25px;
		}
	</style>
</head>
<body>
	<table cellpadding="0" cellspacing="0">
		<thead>
			<tr>
				<th>Last Name</th>
				<th>First Name</th>
				<th>Middle Name</th>
				<th>Status</th>
				<th>Remarks</th>
			</tr>
		</thead>

		<tbody>
			@foreach($scholarships as $scholarship) 
			<tr>	
				<td>{{ $scholarship->resident->last_name }}</td>
				<td>{{ $scholarship['first_name'] }}</td>
				<td>{{ $scholarship['middle_name'] }}</td>
				<td>{{ $scholarship['status'] }}</td>
				<td style="width: 50; text-align: left">{{ $scholarship['comment'] }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>