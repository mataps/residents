<html>

	<head>
		<style type="text/css">
			table {
				font-family: sans-serif;
				width: 100%;
			}
			
			tbody th {
				border-bottom: 1px solid #000;
				font-size: 12px;
			}

			tbody th:first-of-type {
				text-align: left;
			}

			tbody td:last-of-type {
				text-align: right;
			}

			tfoot th:first-of-type {
				text-align: left;
			}

			tfoot td:last-of-type {
				text-align: right;
			}



			tbody td {
				font-size: 10px;
				padding: 5px;
			}

			tbody td:nth-of-type(2) {
				text-align: center;
			}

			thead th {
				border-bottom: 1px solid #000;
				border-top: 1px solid #000;
				padding: 5px 20px;
				font-size: 12px;
			}

			thead tr th.title {
				text-align: left;
				font-size: 18px;
				padding-left: 0px;
				padding-right: 0px;
			}

			thead tr th span.dates {
				font-size: 12px;
				font-weight: normal;
				float: right;
			}
		</style>
	</head>
	<body>
		<table cellpadding="0" cellspacing="0">
			<tbody>
				<tr>
					<th>
						Beginning cash balance
					</th>
					<th>
						{{ $availableCash }}
					</th>
				</tr>
				@foreach($income['transactions'] as $category)
					@if($category->transactions->count() > 0)
					<tr>
						<th>
							{{ $category->name }}
						</th>
						<th>
							{{ number_format($category->transactions->sum('amount')) }}.00
						</th>
					</tr>
					@foreach($category->subCategories as $subCategory)
						@if($subCategory->transactions->sum('amount') !== 0)
						<tr>
							<td>
								{{ $subCategory->name }}
							</td>
							<td>
								{{ number_format($subCategory->transactions->sum('amount')) }}.00
							</td>
						</tr>
						@endif
					@endforeach
					@endif
				@endforeach
				<tr>
					<th>
						Total Collections
					</td>
					<th>
						{{ number_format($income['total']) }}.00
					</th>
				</tr>
				<tr>
					<th>
						Total cash available for disbursements
					</th>
					<th>
						{{ number_format($availableCash + $income['total']) }}.00
					</th>
				</tr>
				@foreach($expense['transactions'] as $category)
					@if($category->transactions->count() > 0)
					<tr>
						<th>
							{{ $category->name }}
						</th>
						<th>
							{{ number_format($category->transactions->sum('amount')) }}.00
						</th>
					</tr>
					@foreach($category->subCategories as $subCategory)
						
						<tr>
							<td>
								{{ $subCategory->name }}
							</td>
							<td>
								{{ number_format($subCategory->transactions->sum('amount')) }}.00
							</td>
						</tr>
						
					@endforeach
					@endif
				@endforeach
				<tr>
					<th>
						Total disbursements
					</td>
					<th>
						{{ number_format($expense['total']) }}.00
					</th>
				</tr>
				<tr>
					<th>
						Ending cash balance
					</th>
					<th>
						{{ number_format($availableCash + $income['total'] - $expense['total']) }}.00
					</th>
				</tr>
			</tbody>
		</table>
	</body>
</html>