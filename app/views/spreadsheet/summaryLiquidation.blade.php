<html>
	<head>
		<style type="text/css">
			table {
				font-family: sans-serif;
			}

			tbody th {
				border-bottom: 1px solid #000;
				font-size: 12px;
			}

			tbody th:first-of-type {
				text-align: left;
			}

			tbody th:last-of-type {
				text-align: right;
				padding-top: 5px;
				padding-bottom: 5px;
			}

			tbody td:last-of-type {
				text-align: right;
			}

			tbody td {
				font-size: 10px;
				padding: 5px;
			}

			thead th {
				border-bottom: 1px solid #000;
				border-top: 1px solid #000;
				font-size: 12px;
			}

			.text-right {
				text-align: right;
			}
		</style>
	</head>
	<body>
		<table cellpadding="0" cellspacing="0">
			<tbody>
				@foreach($transactions as $category)
					@if($category->transactions->count() > 0)
					<tr>
						<th>
							{{ $category->name }}
						</th>
						<th>
							{{ number_format($category->transactions->sum('amount')) }}.00
						</th>
					</tr>
					@foreach($category->subCategories as $subCategory)
						
						<tr>
							<td>
								{{ $subCategory->name }}
							</td>
							<td>
								{{ number_format($subCategory->transactions->sum('amount')) }}.00
							</td>
						</tr>
						
					@endforeach
					@endif
				@endforeach
				<tr>
					<th>
						Total
					</td>
					<th>
						{{ number_format($total) }}.00
					</th>
				</tr>
			</tbody>
		</table>
	</body>
</html>