import java.sql.*;

public class DBImporter {
	// JDBC Driver
   static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
   static final String DB_URL = "jdbc:mysql://localhost/galactus_tj";

   static final String USERNAME = "root";
   static final String PASSWORD = "root";

   public static void main(String[] args) {
   		Connection connection = null;
   		Statement statement = null;

	   try{
      		//STEP 2: Register JDBC driver
      		Class.forName("com.mysql.jdbc.Driver");

      		System.out.println("Connecting Database...");

      		connection = DriverManager.getConnection(DB_URL, USERNAME, PASSWORD);

      		statement = connection.createStatement();

      		String old_db_sql = "SELECT * from old_residents";

      		ResultSet rs = statement.executeQuery(old_db_sql);

      		while(rs.next()){
      			String old_full_name = 	rs.getString("full_name");

      			System.out.println(old_full_name);
      		}


  	   }catch(SQLException se){
	      	//Handle errors for JDBC
	      	se.printStackTrace();
   	   }catch(Exception e){
	      //Handle errors for Class.forName
	      e.printStackTrace();
	   }finally{
	      //finally block used to close resources
	      try{
	         if(statement!=null)
	            statement.close();
	      }catch(SQLException se2){
	      }// nothing we can do
	      try{
	         if(connection!=null)
	            connection.close();
	      }catch(SQLException se){
	         se.printStackTrace();
	      }//end finally try
	   }//end try
   }

}