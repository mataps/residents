#!/usr/bin/python
import MySQLdb
import time
import os
import hashlib
import re
import MySQLdb.cursors
from progressbar import ProgressBar

#!/usr/bin/python
import MySQLdb
import time
import os
import hashlib
import re
import MySQLdb.cursors
from progressbar import ProgressBar



global_city_id = "41"

sql_query = "select count(r.barangay_id) as total_count, r.full_name as full_name, r.barangay_id as resident_barangay_id, b.name, b.`city_municipality_id`, " \
			"r.`city_municipality_id` as resident_municipality_id " \
			"from residents r join barangays b on r.barangay_id = b.id " \
			"where not b.city_municipality_id=r.city_municipality_id " \
			"and r.city_municipality_id = {} group by r.barangay_id".format(global_city_id)


# galactus = Transaction()
galactus = MySQLdb.connect(host="localhost",user="root", passwd="root",db="galactus_1")
comelec = MySQLdb.connect(host="10.0.10.11",user="root", passwd="123asdASD",db="Bongabon2014")
comelec_ref = MySQLdb.connect(host="10.0.10.11",user="root", passwd="123asdASD",db="ref")

cdo_db_cursor = galactus.cursor(MySQLdb.cursors.DictCursor)
cdo_db_cursor.execute(sql_query)
barangay_groups = cdo_db_cursor.fetchall()

counter = 0
pbar = ProgressBar(maxval=barangay_groups[0]['total_count'])

for leader in barangay_groups:
	city_search_sql = "select CONCAT('49', RESCITY, RESBARANGAY) as BGYCODE, FIRSTNAME, MATERNALNAME, LASTNAME " \
					  "from doctable where CONCAT_WS(' ', FIRSTNAME, MATERNALNAME, LASTNAME) = '{}'".format(leader['full_name'])
	counter += 1
	pbar.update(counter)

	comelec_cursor = comelec.cursor(MySQLdb.cursors.DictCursor)
	comelec_cursor.execute(city_search_sql)
	bgy_code = comelec_cursor.fetchone()['BGYCODE']

	get_bgy_sql = "select * from allbgy WHERE ID_BARANGAY={}".format(bgy_code)


	comelec_ref_cursor = comelec_ref.cursor(MySQLdb.cursors.DictCursor)
	comelec_ref_cursor.execute(get_bgy_sql)
	barangay = comelec_ref_cursor.fetchone()
	
	bgy_insert = "INSERT INTO barangays(name, city_municipality_id) values('{}', '{}')".format(barangay['AREANAME'], global_city_id)
	galactus_insert_cursor = galactus.cursor()
	galactus_insert_cursor.execute(bgy_insert)
	galactus.commit()

	get_new_bgy_sql = "SELECT id from barangays where name='{}' and city_municipality_id={}".format(barangay['AREANAME'], global_city_id)
	galactus_bgy_cursor = galactus.cursor()
	galactus_bgy_cursor.execute(get_new_bgy_sql)
	barangay_id = galactus_bgy_cursor.fetchone()[0]

	new_barangay_id = barangay_id
	old_barangay_id = leader['resident_barangay_id']

	updater = "update residents set barangay_id = {} where barangay_id = {} and city_municipality_id = {}".format(new_barangay_id, old_barangay_id, global_city_id)
	galactus_updater = galactus.cursor()
	galactus_updater.execute(updater)
	galactus.commit()


	# print voucher_number
	# print client_name
	# print galactus_client_id

pbar.finish()
