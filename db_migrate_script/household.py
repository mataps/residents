#!/usr/bin/python
import MySQLdb
import time
import os
import hashlib
import re
import MySQLdb.cursors
import string
from progressbar import ProgressBar

class Migrator():

	galactus = MySQLdb.connect(host="localhost", user="root", passwd="root", db="galactus_edbms")

	def get_municipality_id(self, municipality_name) :
	    try:
	        select_mun = "select id from cities_municipalities where name='{0}'".format(municipality_name)
	        g_cursor = galactus.cursor()
	        g_cursor.execute(select_mun)
	        municipality_id = g_cursor.fetchone()[0]
	        return municipality_id

	    except Exception, e :
	        add_mun = "insert into cities_municipalities(name) VALUES('{0}')".format(municipality_name)
	        g_cursor = galactus.cursor()
	        g_cursor.execute(add_mun)
	        galactus.commit()
	        municipality_id = g_cursor.lastrowid

	    return municipality_id

	def get_barangay_id(self, barangay_name, municipality_id):

	    if barangay_name is None :
		return ''

	    select_brgy = "select id from barangays where name='{0}' and city_municipality_id={1}".format(barangay_name, municipality_id)
	    g_cursor = galactus.cursor()
	    g_cursor.execute(select_brgy)
	    barangay_id = g_cursor.fetchone()[0]
	    return barangay_id

	def get_district_id(self, municipality_id):

		select_municipality = "select district_id from cities_municipalities where id = '{}'".format(municipality_id)
		g_cursor = galactus.cursor()
		g_cursor.execute(select_municipality)
		district_id = g_cursor.fetchone()[0]

		return district_id

	def get_resident_id(self, full_name):
		select_resident = "select id from residents where full_name='{}'".format(full_name)
		g_cursor = galactus.cursor()
		g_cursor.execute(select_resident)
		resident_id = g_cursor.fetchone()
		if resident_id is None :
			return ''
		return resident_id[0]

	def strip_slash(self, resident):
	    for (key, value) in resident.items():
	        if(value == None):
	                continue
	        if(isinstance(value, basestring)):
	                value = value.strip("\\")
	                resident[key] = re.sub("['\"]", '', value)
	    return resident

migrator = Migrator()
galactus = MySQLdb.connect(host="localhost", user="root", passwd="root", db="galactus_edbms")
household_db = MySQLdb.connect(host="localhost", user="root", passwd="root", db="cdodb_base")

get_households_sql = "select * from building join barangay on barangay.OID = building.barangay_OID join municipality on municipality.OID = barangay.municipality_id";
household_cursor = household_db.cursor(MySQLdb.cursors.DictCursor)
household_cursor.execute(get_households_sql)
households = household_cursor.fetchall()
counter = 0
progress = 0
pbar = ProgressBar(maxval=56653)

for household in households:
	counter = counter + 1
	progress = progress + 1
	household = migrator.strip_slash(household)

	city_municipality_id = migrator.get_municipality_id(household['municipality_name'])
	barangay_id = migrator.get_barangay_id(household['barangay_name'], city_municipality_id)
	district_id = migrator.get_district_id(city_municipality_id)

	household_insert_sql = "insert into households (id, label, phone, district_id, city_municipality_id, barangay_id, street, area_code, created_by, modified_by, owner_name, consumer_name, notes, status)" \
						   " values ('{}','{}', '{}', '{}', '{}', '{}', '{}', '{}', 1, 1, '{}', '{}', '{}', '{}')".format(counter, household['building_name'], '', district_id, city_municipality_id, barangay_id, household['street'], household['area_code'], household['owner_name'], household['consumer_name'], household['notes'], household['status'])
	g_house_cursor = galactus.cursor()
	g_house_cursor.execute(household_insert_sql)
	galactus.commit()	

	cdodb_household_id = household['OID'];
	owner_id = migrator.get_resident_id(household['owner_name'])

	get_residents_sql = "select r.OID as id, CONCAT_WS(' ', r.first_name, r.maternal_name, r.last_name) as full_name"\
						" from building_resident_detail as b join resident as r on r.OID = b.resident_OID where b.building_OID = {}".format(cdodb_household_id)
	household_resident_cursor = household_db.cursor(MySQLdb.cursors.DictCursor)
	household_resident_cursor.execute(get_residents_sql)
	cdodb_household_residents = household_resident_cursor.fetchall()

	for resident in cdodb_household_residents:
		resident = migrator.strip_slash(resident)
		g_resident_id = migrator.get_resident_id(resident['full_name'])
		resident_position = 'member'
		if g_resident_id == owner_id:
			resident_position = 'owner'
		h_r_insert_sql = "insert into households_residents (household_id, resident_id, role) values ('{}', '{}', '{}')".format(counter, g_resident_id, resident_position)
		galactus_cursor = galactus.cursor()
		galactus_cursor.execute(h_r_insert_sql)
		galactus.commit()

	pbar.update(progress)








