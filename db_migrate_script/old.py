#!/usr/bin/python
import MySQLdb
import time
import os
import hashlib
import re
import MySQLdb.cursors
from progressbar import ProgressBar

galactus = MySQLdb.connect(host="localhost",user="root", passwd="root",db="galactus_1")
galactus_insert = MySQLdb.connect(host="localhost",user="root", passwd="root",db="galactus_1")
galactus.begin()
old_db_cursor = galactus.cursor(MySQLdb.cursors.DictCursor)
old_db_cursor.execute("select * from old_residents")
old_residents = old_db_cursor.fetchall()

# total = len(old_residents)

old_resident_count_cursor = galactus.cursor()
old_resident_count_cursor.execute("select count(id) from old_residents")
old_residents_count  = old_resident_count_cursor.fetchone()[0]

counter = 0

pbar = ProgressBar(maxval=old_residents_count)

for old_resident in old_residents :
	counter += 1
	pbar.update(counter)
	full_name = old_resident['full_name']
	birthdate = old_resident['birthdate']
	galactus_insert.begin()
	galactus_cursor = galactus_insert.cursor()
	same_entry_sql = "select count(*) from residents where full_name='{}'and birthdate='{}'".format(full_name, birthdate)
	galactus_cursor.execute(same_entry_sql)
	same_entry = galactus_cursor.fetchone()[0]

	# print same_entry

	# print "{}/{} : {}".format(counter, total, full_name)

	if same_entry > 0:
		# print "Same Value"
		continue
	else:
		# print "Inserting"
		insert_sql = "INSERT INTO `residents` (`voters_id`, `is_voter`, `last_name`, `first_name`, `middle_name`, \
			`full_name`, `email`, `mobile`, `phone`, `precinct`, `birthdate`, `gender`, `civil_status`, `district_id`, \
			`city_municipality_id`, `barangay_id`, `street`, `area_code`, `signature_path`, `photo_id`, `deceased_at`,\
			 `blacklisted_at`, `created_by`, `modified_by`) \
			 VALUES \
			('{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}','{}')".format(old_resident['voters_id'],old_resident['is_voter'],old_resident['last_name'],old_resident['first_name'],old_resident['middle_name'],\
			old_resident['full_name'],old_resident['email'],old_resident['mobile'],old_resident['phone'],old_resident['precinct'],old_resident['birthdate'],old_resident['gender'],old_resident['civil_status'],old_resident['district_id'],\
			old_resident['city_municipality_id'],old_resident['barangay_id'],old_resident['street'],old_resident['area_code'],old_resident['signature_path'],old_resident['photo_id'],old_resident['deceased_at'], \
		 	old_resident['blacklisted_at'],old_resident['created_by'],old_resident['modified_by'])

		galactus_insert.begin()
		galactus_cursor_insert = galactus_insert.cursor()
		galactus_cursor_insert.execute(insert_sql)
		galactus_insert.commit()

pbar.finish()




