#!/usr/bin/python
import MySQLdb 
import time
import os
import hashlib
import re
import MySQLdb.cursors

class Migrator():

	galactus = MySQLdb.connect(host="localhost",user="root", \
		
	old_db = MySQLdb.connect(host="localhost",user="root", \
							   passwd="root",db="cdodb_base", \
    						   cursorclass = MySQLdb.cursors.SSCursor)

	def convert_blob(self, blob, city, fullname):
		ts = str(time.time()) + fullname
		filename = hashlib.md5(ts).hexdigest() + ".jpg"
		directory = city


		if not os.path.exists(directory):
		    os.makedirs(directory)

		filepath = city + "/" + filename
		image_file = open(filepath, "w")
		image_file.write(blob)
		image_file.close()
		return filename

	def save_photo(self, filename):
		filename = str(filename)
		g_cursor = galactus.cursor()
		sql = "INSERT INTO photos(filename) VALUES('" + filename +"')"
		g_cursor.execute(sql)
		galactus.commit()
		photo_id = g_cursor.lastrowid
		# galactus.close()
		return photo_id

	def get_municipality_id(self, municipality_name):
		try:
			select_mun = "select id from cities_municipalities where name='{0}'".format(municipality_name)
			g_cursor = galactus.cursor()
			g_cursor.execute(select_mun)
			municipality_id = g_cursor.fetchone()[0]
			# galactus.close()	
			return municipality_id

		except Exception, e:
			add_mun = "insert into cities_municipalities(name) VALUES('{0}')".format(municipality_name)
			g_cursor = galactus.cursor()
			g_cursor.execute(add_mun)
			galactus.commit()
			municipality_id = g_cursor.lastrowid
			# galactus.close()
		return municipality_id
	def get_barangay_id(self, barangay_name, municipality_id):
		try:
			select_brgy = "select id from barangays where name='{0}' and city_municipality_id={1}".format(barangay_name, municipality_id)
			g_cursor = galactus.cursor()
			g_cursor.execute(select_brgy)
			barangay_id = g_cursor.fetchone()[0]
			# galactus.close()
			return barangay_id
		except Exception, e:
			add_brgy = "insert into barangays(name, city_municipality_id) VALUES('{0}', '{1}')".format(barangay_name, municipality_id)
			g_cursor = self.galactus.cursor()
			g_cursor.execute(add_brgy)
			galactus.commit()
			barangay_id = g_cursor.lastrowid
			galactus.close
		return barangay_id		

	def strip_slash(self, resident):
		for (key, value) in resident.items():
			if(value == None):
				continue
			if(isinstance(value, basestring)):
				value = value.strip("\\")
				resident[key] = re.sub("['\"]", '', value)
		return resident

migrator = Migrator();
old_db = migrator.old_db
galactus = migrator.galactus

with old_db:

	
	old_residents = old_db.cursor(MySQLdb.cursors.SSDictCursor)
	old_residents.execute("select resident.is_registered as is_voter, resident.first_name as first_name, resident.maternal_name as maternal_name, resident.last_name as last_name, resident.street, barangay.barangay_name as barangay_name, municipality.municipality_name as municipality_name, resident.precinct as precinct, resident.birthdate as birthdate, resident.gender as gender, resident.civil_status as civil_status, resident.cellphone as cellphone, resident.telephone as telephone, resident.email as email, resident.is_blacklisted as is_blacklisted, resident.is_deceased as is_deceased, resident.image as image from resident join barangay on resident.residence_id = barangay.OID join municipality on barangay.municipality_id = municipality.OID");

	
	# print old_residents.rowcount
	total = old_residents.rowcount
	counter = 0;
	for resident in old_residents:
	
		resident = migrator.strip_slash(resident)

		counter += 1
		g_cursor = galactus.cursor(MySQLdb.cursors.SSCursor)
		# fullname = resident['first_name'] + ' ' + resident['maternal_name'] + ' ' +resident['last_name']
		fullname = "{} {} {}".format(resident['first_name'], resident['maternal_name'], resident['last_name']);
		log = "{}/{} : {}".format(str(counter), str(total), fullname)
		print log
		try:
			check_resident = "select id from old_residents where first_name='{}' and middle_name='{}' and last_name='{}' and full_name='{}' and birthdate='{}'".format(resident['first_name'], resident['maternal_name'], resident['last_name'], fullname, resident['birthdate'])
			g_cursor.execute(check_resident)
			resident_id = g_cursor.fetchone()[0]
			print 'Duplicate'
		except Exception, e:
			municipality_name = resident['municipality_name'].replace("City", "")
			
			if(resident['image'] == None):
				photo_id = ''
			else:
				filename = migrator.convert_blob(resident['image'],\
										 municipality_name,\
										 fullname)

			photo_id = migrator.save_photo(filename)		
			municipality_id = migrator.get_municipality_id(municipality_name)
			barangay_id = migrator.get_barangay_id(resident['barangay_name'], municipality_id)
			deceased = resident['is_deceased']
			if(resident['is_deceased'] == 0 or resident['is_deceased'] == None) :
				deceased = None
			blacklisted = resident['is_blacklisted']
			if(resident['is_blacklisted'] == 0 or resident['is_blacklisted'] == None) :
				blacklisted = None
			insert_resident = "INSERT INTO old_residents(first_name, middle_name, last_name, full_name, \
												email, mobile, phone, precinct, birthdate, gender, \
												civil_status, city_municipality_id, barangay_id, street, photo_id, \
												deceased_at, blacklisted_at, is_voter) VALUES('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', '{10}', '{11}', '{12}', '{13}', '{14}', '{15}', '{16}', '{17}')".format(resident['first_name'], resident['maternal_name'], resident['last_name'], fullname, resident['email'], resident['cellphone'], resident['telephone'], resident['precinct'], resident['birthdate'], resident['gender'], resident['civil_status'], municipality_id, barangay_id, resident['street'], photo_id, deceased, blacklisted, resident['is_voter'])
								
		
			g_cursor.execute(insert_resident)
			galactus.commit()
		
		# galactus.close()


