#!/usr/bin/python
import MySQLdb
import time
import os
import hashlib
import re
import MySQLdb.cursors
import string
from progressbar import ProgressBar

class Migrator():
        def strip_slash(self, resident):
                for (key, value) in resident.items():
                        if(value == None):
                                continue
                        if(isinstance(value, basestring)):
                                value = value.strip("\\")
                                resident[key] = re.sub("['\"]", '', value)
                return resident

		

galactus = MySQLdb.connect(host="localhost", user="root", passwd="root", db="galactus_edbms");
scholar_db = MySQLdb.connect(host="localhost", user="root", passwd="root", db="cdodb_test");

scholar_query_sql = "SELECT semester.start_date as start_date, semester.end_date as end_date," \
					" school.school_name as school_name, school.school_type as school_type," \
					" scholar_record.average as gwa, " \
					" scholar_record.scholar_level as scholar_level, " \
					" CONCAT_WS(' ', resident.first_name, resident.maternal_name, resident.last_name) as full_name, " \
					" resident.birthdate as birthdate, " \
					" scholar.father_name as father_name, " \
					" scholar.mother_name as mother_name, " \
					" scholar.guardian_name as guardian_name, " \
					" scholar.guardian_contact_no as guardian_contact_no, " \
					" scholar.scholar_status as scholar_status, " \
					" scholar.approval_date as approval_date, " \
					" scholar.notes as comment, " \
					" scholar.date_created as scholar_created_at, " \
					" scholar_record.date_created as scholar_record_created_at, " \
					" scholar.date_modified as scholar_updated_at, " \
					" scholar_record.date_modified as scholar_record_updated_at, " \
					" semester.semester as semester_name, " \
					" scholar.guardian_contact_no as contact_no " \
					" FROM scholar_record join scholar on scholar.OID = scholar_record.scholar_OID join resident on scholar.resident_OID = resident.OID left join scholar_school as school on school.OID = scholar_record.school_OID join scholar_sem_info as semester on semester.OID = sem_info_OID join scholar_course as course on course.OID = course_OID limit 46902, 1884"

scholar_cursor = scholar_db.cursor(MySQLdb.cursors.DictCursor)
scholar_cursor.execute(scholar_query_sql)
scholar_records = scholar_cursor.fetchall()


counter = 0

pbar = ProgressBar(maxval=1884)
migrator = Migrator()

for record in scholar_records :
	record = migrator.strip_slash(record)
	if record['scholar_level'] is None:
		scholar_year = ''
		scholar_level = ''
	else:
		record_scholar_level = list(record['scholar_level'])
		scholar_year = record_scholar_level[1]
		if record_scholar_level[0] == "C":
			scholar_level = 'collegiate'
		else:
			scholar_level = 'secondary'


	if record['scholar_status'] == 'G':
		scholar_status = 'graduated'
	elif record['scholar_status'] == 'A':
		scholar_status = 'active'
	elif record['scholar_status'] == 'F':
		scholar_status = 'for_verification'
	elif record['scholar_status'] == 'C':
		scholar_status = 'Closed'
	elif record['scholar_status'] == 'N':
		scholar_status = 'pending'
	elif record['scholar_status'] == 'S':
		scholar_status = 'stop'
	else:
		scholar_status = 'waiting'

	search_existing_sql = "SELECT id from residents_mock WHERE full_name = '{}' AND birthdate='{}'".format(record['full_name'], record['birthdate']);
	galactus_existing_user_cursor = galactus.cursor()
	galactus_existing_user_cursor.execute(search_existing_sql)
	galactus_existing_user = galactus_existing_user_cursor.fetchone()

	if galactus_existing_user is None:
		continue
	resident_id = galactus_existing_user[0]

	update_sql = "update residents set phone='{}' where id='{}'".format(record['contact_no'], resident_id)
	galactus_resident_update = galactus.cursor()
	galactus_resident_update.execute(update_sql)
	galactus.commit()

	search_existing_school_sql = "SELECT id from schools where name = '{}' AND level = '{}'".format(record['school_name'], scholar_level)
	galactus_existing_school_cursor = galactus.cursor()
	galactus_existing_school_cursor.execute(search_existing_school_sql)
	galactus_school = galactus_existing_school_cursor.fetchone()
	
	if record['school_type'] == "Pr":
		school_type = "private"
	else:
		school_type = "public"

	if galactus_school is None:
		# print "school is none currently adding {}".format(record['school_name'])
		insert_school_sql = "INSERT INTO schools (name, type, level,created_by, modified_by) values ('{}', '{}', '{}', 1, 1)".format(record['school_name'], school_type, scholar_level)
		galactus_insert_cursor = galactus.cursor()
		galactus_insert_cursor.execute(insert_school_sql)
		galactus.commit()
		search_existing_school_sql = "SELECT id from schools where name = '{}' AND level='{}'".format(record['school_name'], scholar_level)
		galactus_existing_school_cursor = galactus.cursor()
		galactus_existing_school_cursor.execute(search_existing_school_sql)
		galactus_school = galactus_existing_school_cursor.fetchone()

	galactus_school = galactus_school[0]

	scholarship_existing_sql = "select id from scholarships where resident_id={}".format(resident_id)
	galactus_scholarship_cursor = galactus.cursor()
	galactus_scholarship_cursor.execute(scholarship_existing_sql)
	scholarship_id = galactus_scholarship_cursor.fetchone()


	if scholarship_id is None:
		insert_scholar_sql = "insert into scholarships_scripts (resident_id, school_id, comment, status, created_at, updated_at, created_by, modified_by, transfered_by)" \
							 " values ('{}', '{}', '{}', '{}', '{}', '{}', 1, 1, NULL)".format(resident_id, galactus_school, record['comment'], scholar_status, record['scholar_created_at'], record['scholar_updated_at'])
		galactus_scholar_insert = galactus.cursor()
		galactus_scholar_insert.execute(insert_scholar_sql)
		galactus.commit()

		scholarship_existing_sql = "select id from scholarships where resident_id={}".format(resident_id)
		galactus_scholarship_cursor = galactus.cursor()
		galactus_scholarship_cursor.execute(scholarship_existing_sql)
		scholarship_id = galactus_scholarship_cursor.fetchone()


	scholarship_id = scholarship_id[0]

	semester_scholarship_type = list(record['semester_name'])
	if semester_scholarship_type[0] == "C":
		scholarship_type = 7
	else:
		scholarship_type = 8

    
	if record['semester_name'] is None:

		semester_details = string.split(record['semester_name'], "S")
		start_year = semester_details[0]

	if semester_scholarship_type[0] == "C":
		# start_year = string.split(semester_details[0], "-")
		start_year = "2014"

	end_year = int(start_year) + 1
	school_year = "{}-{}".format(start_year, end_year)

	insert_sem = "insert into semesters_scripts (scholarship_id, school_id, school_year, year_level, term, gwa, created_at, updated_at, created_by, modified_by, scholarship_type_id)" \
				 " values ('{}','{}','{}','{}','{}','{}','{}', 1, 1, '{}')".format(scholarship_id, galactus_school, school_year, scholar_year, semester_details[1],record['gwa'], record['scholar_record_created_at'], record['scholar_record_updated_at'], scholarship_type)

	print insert_sem;
	# galactus_sem_cursor = galactus.cursor()
	# galactus_sem_cursor.execute(insert_sem)
	# galactus.commit()

	counter += 1
	pbar.update(counter)



