#!/usr/bin/python
import MySQLdb
import time
import os
import hashlib
import re
import MySQLdb.cursors
from progressbar import ProgressBar

class Transaction():

	def __init__(self):
		self.galactus = MySQLdb.connect(host="localhost",user="root", passwd="root",db="galactus_duplicate")

    def strip_slash(self, object):
            for (key, value) in object.items():
                    if(value == None):
                            continue
                    if(isinstance(value, basestring)):
                            value = value.strip("\\")
                            object[key] = re.sub("['\"]", '', value)
            return object
	def getConnection(self):
		return self.galactus

	def getCursor(self):
		return self.getConnection().cursor()
	def getSubCategory(self, name):
		pass
	def getItem(self, name):
		pass
	def getAccount(self, account):
		pass
	def getUser(self, username):
		if username is None:
			return 'NULL'
		sql = "SELECT id from users where username = '{}'".format(username);
		cursor = self.getCursor()
		cursor.execute(sql)
		user = cursor.fetchone()
		return user[0]

	def getResidentId(self, full_name):
		
		sql_query = "SELECT id from residents_mock where full_name='{}'".format(full_name)

		galactus_cursor = self.getCursor()
		galactus_cursor.execute(sql_query)
		resident = galactus_cursor.fetchone()
		if resident is None:
			return None
		return resident[0]
	def checkIfFirstVoucher(self, voucher_number):

		sql_query = "SELECT count(id) from vouchers where uuid = '{}'".format(voucher_number)
		galactus_cursor = self.getCursor()
		galactus_cursor.execute(sql_query)
		count  = galactus_cursor.fetchone()[0]

		if count > 0:
			return False

		else:
			return True

	def getIdVoucher(self, voucher_number):
		sql_query = "SELECT id from vouchers where uuid = '{}'".format(voucher_number)		
		galactus_cursor = self.getCursor()
		galactus_cursor.execute(sql_query)
		v_id  = galactus_cursor.fetchone()[0]

		return v_id

	def insertVoucher(self, voucher_no, client_id, account_type_id, status, remarks, details, settled_at, created_at, updated_at, created_by, modified_by):
		if self.checkIfFirstVoucher(voucher_no):
			sql_query = "INSERT into vouchers (uuid," \
				        " client_id, client_type, " \
				        "account_type_id, status, remarks, "\
				        "details, settled_at, created_at, updated_at, created_by, modified_by)"\
						" values('{}','{}','Resident','{}','{}','{}','{}','{}','{}',"\
						"'{}', '{}', '{}')".format(voucher_no, client_id, account_type_id, status, remarks, details, \
												   settled_at, created_at, updated_at, \
						                           created_by, modified_by)
			g_insert = self.getConnection()
			galactus_insert_cursor = g_insert.cursor()
			galactus_insert_cursor.execute(sql_query)
			g_insert.commit()
		return True
	def insertTransaction(self, uuid, voucher_id, client_id, refferer_id, beneficiary_id, sub_category_id, \
						  item_id, transaction_type, details, remarks, amount, settled_at, \
						  created_at, modified_at, created_by, modified_by):

		sql_query = "INSERT into transactions (uuid, voucher_id, client_id, client_type, referrer_id, beneficiary_id, beneficiary_type," \
					"sub_category_id, item_id, transaction_type, details, remarks, amount, settled_at,"\
					"created_at, updated_at, created_by, modified_by) values ('{}', '{}', '{}', 'Resident', '{}', '{}', 'Resident', " \
					"'{}', '{}', '{}', '{}', '{}', '{}', '{}', " \
					"'{}', '{}', '{}', '{}')".format(uuid, voucher_id, client_id, refferer_id, beneficiary_id, sub_category_id, \
						  item_id, transaction_type, details, remarks, amount, settled_at, \
						  created_at, modified_at, created_by, modified_by)

		g_insert = self.getConnection()
		galactus_insert_cursor = g_insert.cursor()
		galactus_insert_cursor.execute(sql_query)
		g_insert.commit()
		return True


sql_query = "select transactions.*, " \
			"transaction_detail.*, " \
			"client.full_name as client_name, " \
			"beneficiary.full_name as beneficiary_name, " \
			"refferer.full_name as refferer_name, " \
			"accounts.account_names as account_name, " \
			"items.item_name as item_name, " \
			"CONCAT('old_', created_by.user_name) as created_by_name, " \
			"CONCAT('old_', modified_by.user_name) as modified_by_name " \
			"from transactions join transaction_detail on transactions.OID = transaction_detail.transactions_OID " \
			"join accounts on transactions.accounts_OID = accounts.OID " \
			"join transactors on transaction_detail.OID = transactors.transaction_detail_OID " \
			"join transaction_subcategory on transaction_subcategory.OID = transaction_detail.transaction_subcategory_OID " \
			"join resident_mock as client on client.OID = transactors.client_OID " \
			"join resident_mock as refferer on refferer.OID = transactors.referrer_OID " \
			"join resident_mock as beneficiary on beneficiary.OID = transactors.beneficiary_OID " \
			"join app_user as created_by on created_by.OID = transactions.handler_OID " \
			"left join app_user as modified_by on modified_by.OID = transactions.last_handler_OID " \
			"join items on items.OID = transaction_detail.items_OID;"


galactus = Transaction()
cdo_db = MySQLdb.connect(host="localhost",user="root", passwd="root",db="cdodb_base")

cdo_db_cursor = cdo_db.cursor(MySQLdb.cursors.DictCursor)
cdo_db_cursor.execute(sql_query)
old_transactions = cdo_db_cursor.fetchall()

counter = 0
pbar = ProgressBar(maxval=179315)

for transaction in old_transactions:
	transaction = galactus.strip_slash(transaction)
	counter += 1
	pbar.update(counter)
	voucher_number = transaction['voucher_no']
	
	client_name = transaction['client_name']
	beneficiary_name = transaction['beneficiary_name']
	refferer_name = transaction['refferer_name']
	galactus_client_id = galactus.getResidentId(client_name)
	galactus_beneficiary_id = galactus.getResidentId(beneficiary_name)
	galactus_refferer_id = galactus.getResidentId(refferer_name)
	galactus_created_by = galactus.getUser(transaction['created_by_name'])
	galactus_modified_by = galactus.getUser(transaction['modified_by_name'])

	if transaction['transaction_type'] == "E":
		transaction_type = 1
	elif transaction['transaction_type'] == "I":
		transaction_type = 2
	else:
		transaction_type = 2

	galactus.insertVoucher(voucher_number, galactus_client_id, transaction['accounts_OID'], \
						   3, transaction['remarks'], transaction['details'], transaction['settle_date'], \
						   transaction['date_created'], transaction['date_modified'], galactus_created_by, galactus_modified_by)

	voucher_id = galactus.getIdVoucher(voucher_number)

	galactus.insertTransaction(voucher_number, voucher_id, galactus_client_id, galactus_refferer_id, galactus_beneficiary_id, transaction['transaction_subcategory_OID'], \
						  transaction['items_OID'], transaction_type, transaction['details'], transaction['remarks'], transaction['amount'], transaction['settle_date'], \
						  transaction['date_created'], transaction['date_modified'], galactus_created_by, galactus_modified_by)
	# print voucher_number
	# print client_name
	# print galactus_client_id

pbar.finish()
