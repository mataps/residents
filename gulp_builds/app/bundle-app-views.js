'use strict';

var gulp = require('gulp')
  , paths = require('../paths')
  , flatten = require('gulp-flatten');

gulp.task('bundle-app-views', function() {
  gulp.src(paths.app + '**/*.html')
    .pipe(flatten())
    .pipe(gulp.dest(paths.partials));
});