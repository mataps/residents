'use strict';

var gulp        = require('gulp'),
    concat      = require('gulp-concat'),
    rename      = require('gulp-rename'),
    annotate    = require('gulp-ng-annotate'),
    plumber     = require('gulp-plumber'),
    jshint      = require('gulp-jshint'),
    paths       = require('../paths');

gulp.task('bundle-app', function () {
  var app = [
    paths.core + 'app.module.js',
    paths.app + 'app.loginCtrl.js',

    paths.core + 'utils/**/*.js',
    paths.core + 'services/**/*.js',
    paths.core + 'resources/**/*.js',
    paths.core + 'filters/**/*.js',
    paths.core + 'controllers/**/*.js',
    paths.components + '**/*.js',

    paths.app + '**/*.js',
    paths.core + 'app.bootstrap.js'
  ];

  return gulp.src(app)
    .pipe(plumber())
    .pipe(concat('build.js'))
    // .pipe(annotate())
    .pipe(jshint())
    .pipe(gulp.dest(paths.js));
});