'use strict';

var gulp        = require('gulp'),
    uglify      = require('gulp-uglify'),
    rename      = require('gulp-rename'),
    paths       = require('../paths'),
    plumber     = require('gulp-plumber');

gulp.task('uglify-app', function() {
  return gulp.src(paths.js + 'build.js')
    .pipe(plumber())
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(paths.js));
});