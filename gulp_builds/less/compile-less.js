'use strict';

var gulp    = require('gulp'),
    less    = require('gulp-less'),
    paths   = require('../paths');

gulp.task('compile-less', function () {
  var lessOpts = { paths: [paths.vendor + 'bootstrap/less'] };

  return gulp.src(paths.less + 'style.less')
    .pipe( less(lessOpts) )
    .pipe( gulp.dest(paths.css) );
});