'use strict';

var gulp        = require('gulp'),
    concat      = require('gulp-concat'),
    paths       = require('../paths');

gulp.task('bundle-libs', function () {
  var libs = [
    paths.vendor + 'jquery/dist/jquery.js',
    paths.vendor + 'jquery-ui/jquery-ui.js',
    paths.vendor + 'bootstrap/dist/js/bootstrap.js',
    paths.vendor + 'floatThead/dist/jquery.floatThead.js',
    paths.vendor + 'pre-loader/pre-loader.js',
    paths.vendor + 'angular/angular.js',
    paths.vendor + 'angular/angular-animate.js',
    paths.vendor + 'angular-sanitize/angular-sanitize.js',
    paths.vendor + 'lodash/dist/lodash.js',
    paths.vendor + 'restangular/dist/restangular.js',
    paths.vendor + 'angular-ui-router/release/angular-ui-router.js',
    paths.vendor + 'angular-ui-utils/ui-utils.js',
    paths.vendor + 'angular-ui-date/src/date.js',
    paths.vendor + 'angular-bootstrap/ui-bootstrap.js',
    paths.vendor + 'angular-bootstrap/ui-bootstrap-tpls.js',
    paths.vendor + 'ngprogress-lite/ngprogress-lite.js',
    paths.vendor + 'angular-animate/angular-animate.js',
    paths.vendor + 'angular-flash/dist/angular-flash.js',
    paths.vendor + 'angular-smart-table/dist/smart-table.js',
    paths.vendor + 'angular-local-storage/dist/angular-local-storage.js',
    paths.vendor + 'angular-ui-select/dist/select.js',
    paths.vendor + 'angular-file-upload/angular-file-upload.js',
    paths.vendor + 'angular-snap/angular-snap.js',
    paths.vendor + 'webcamjs/webcam.js',
    paths.vendor + 'angular-spinkit/build/angular-spinkit.js',
    paths.vendor + 'angular-aside/dist/js/angular-aside.js',
    paths.vendor + 'ui-router-extras/release/ct-ui-router-extras.js',
    paths.vendor + 'angular-srph-xhr/dist/angular-srph-xhr.js',
    paths.vendor + 'd3/d3.js',
    paths.vendor + 'angular-charts/dist/angular-charts.js',
    paths.vendor + 'angular-img-fallback/angular.dcb-img-fallback.js',
    paths.vendor + 'angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.js',
    paths.vendor + 'moment/moment.js',
    paths.vendor + 'bootstrap-daterangepicker/daterangepicker.js',
    paths.vendor + 'angular-daterangepicker/js/angular-daterangepicker.js',
    paths.vendor + 'ng-tags-input/ng-tags-input.js',
    paths.vendor + 'angular-srph-infinite-scroll/dist/angular-srph-infinite-scroll.js',
    paths.vendor + 'angular-input-masks/angular-input-masks.js',
    paths.vendor + 'ng-idle/angular-idle.js'
  ];

  return gulp.src(libs)
    .pipe( concat('libs.js') )
    .pipe( gulp.dest(paths.js) );
});
