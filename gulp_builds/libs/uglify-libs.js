'use strict';

var gulp        = require('gulp'),
    concat      = require('gulp-concat'),
    uglify      = require('gulp-uglify'),
    paths       = require('../paths'),
    rename      = require('gulp-rename'),
    plumber     = require('gulp-plumber');

gulp.task('uglify-libs', function() {
  return gulp.src(paths.js + 'libs.js')
    .pipe(plumber())
    .pipe(uglify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(gulp.dest(paths.js));
});