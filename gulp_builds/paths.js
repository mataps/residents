var paths             = {};

paths.public_     = './public/';
paths.temp        = paths.public_ + 'temp/';
paths.less        = paths.public_ + 'less/';

/** Built files / assets to be served */
paths.assets      = paths.public_ + 'assets/';
paths.js          = paths.assets + 'js/';
paths.css         = paths.assets + 'css/';
paths.partials    = paths.assets + 'partials/';
paths.vendor      = paths.assets + 'vendor/';

/** App directories */
paths.app         = paths.public_ + 'app/';
paths.core        = paths.public_ + 'core/';
paths.components  = paths.public_ + 'components/';

module.exports = paths;