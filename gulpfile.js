// **
// Gulp tasks
// **
// 
var gulp = require('gulp'),
  paths = require('./gulp_builds/paths'),
  livereload = require('gulp-livereload');


/** Less tasks */
require('./gulp_builds/less/compile-less');
require('./gulp_builds/less/minify-less');
gulp.task('build-less', ['compile-less', 'minify-less']);

/** App tasks */
require('./gulp_builds/app/bundle-app');
require('./gulp_builds/app/uglify-app');
require('./gulp_builds/app/bundle-app-views');
gulp.task('build-app', ['bundle-app', 'uglify-app', 'bundle-app-views']);

/** Vendor tasks */
require('./gulp_builds/libs/bundle-libs');
require('./gulp_builds/libs/uglify-libs');
gulp.task('build-libs', ['bundle-libs', 'uglify-libs']);

/** Perform all builds */
gulp.task('build', ['build-libs', 'build-app', 'build-less']);

/** Executes all tasks | watches for changes */
gulp.task('default', function () {
  livereload.listen();

  gulp.start('build');

  var appDirs = [
    paths.app + '**/*.js',
    paths.components + '**/*.js',
    paths.core + '**/*.js'
  ];

  // Watch app
  gulp.watch(appDirs, ['bundle-app']).on('change', livereload.changed);
  gulp.watch(paths.js + 'build.js', ['uglify-app']);
  gulp.watch(paths.js + 'lib.js', ['uglify-app']);
  gulp.watch(paths.app + '**/*.html', ['bundle-app-views']).on('change', livereload.changed);

  // Watch less
  gulp.watch(paths.less + '**/*.less', ['compile-less']).on('change', livereload.changed);
  gulp.watch(paths.css + 'build.css', ['minify-less']);
});