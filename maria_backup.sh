#!/bin/bash
### MySQL Server Login Info ###
MUSER="root"
MPASS="root"
MHOST="localhost"
MYSQL="$(which mysql)"
MYSQLDUMP="$(which mysqldump)"
BAK="/home/scourcescript/backup/galactus_db"
GZIP="$(which gzip)"
### FTP SERVER Login info ###
FTPU="MysqlBackup"
FTPP="p@55w0rd"
FTPS="10.0.10.13"
NOW=$(date +"%d-%m-%Y")
 
### See comments below ###
### [ ! -d $BAK ] && mkdir -p $BAK || /bin/rm -f $BAK/* ###
[ ! -d "$BAK" ] && mkdir -p "$BAK"
 
DBS="galactus_prod"

FILE=$BAK/$db.$NOW-$(date +"%T").gz
$MYSQLDUMP -u $MUSER -h $MHOST -p$MPASS $db -v | $GZIP -9v > $FILE

 
lftp -u $FTPU,$FTPP -e "mkdir /mysql_backups/$NOW;cd /mysql_backups/$NOW; mput /home/scourcescript/backup/mysql/*; quit" $FTPS
