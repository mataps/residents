+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('ChangePasswordController', controller);

  function controller($scope, flash) {
    $scope.form = {};

    $scope.success = function() {
      flash.success = 'Your password has been updated!';
      $scope.form = {};
    }

    $scope.error = function(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }
  }
})(angular);