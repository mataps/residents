+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    $stateProvider.state({
      parent: 'main',
      name: 'account-settings',
      url: '/account-settings',
      data: { pageTitle: 'Account Settings: Change Password' },
      controllerAs: 'passwordCtrl',
      controller: 'ChangePasswordController',
      templateUrl: '/app/$change-password/_.html'
    });
  }
})(angular);