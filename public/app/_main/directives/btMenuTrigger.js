+(function (angular) {

  angular.module('app')
    .directive('btMenuTrigger', function() {
      return {
        restrict: 'C',
        link: function(scope, elem, attr) {
          elem.on('click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            elem.parent().toggleClass('bt-menu-open');
          });
          elem.parent().on('click', function(e) {
            elem.parent().toggleClass('bt-menu-open');
          });
        }
      }
    });

})(angular);