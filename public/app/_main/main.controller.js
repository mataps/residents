+(function(angular, undefined) {
  'use strict';

  angular
      .module('app')
      .controller('MainController', MainController);

  function MainController($scope, $modal, AuthSrvc, USER, VERSION, BUILD) {
    $scope.USER = USER;
    $scope.toggleSidebar = false;
    $scope.version = VERSION[0];
    $scope.build = BUILD[0];


    $scope.showSidebar = function() {

      $scope.toggleSidebar = true;
    };

    $scope.hideSidebar = function() {
      $scope.toggleSidebar = false;
    };


    /**
     * Authentication data
     * @type {Object}
     */
    $scope.auth = {
      data: (function(service) {
        return service.data;
      })(AuthSrvc),
      status: (function(service) {
        return service.status;
      })(AuthSrvc)
    };

    $scope.addResident = function() {
      $modal.open({
        templateUrl: '/app/_main/modals/residents/add-resident.html',
        controller: 'AddResidentCtrl as _modal_',
        size: 'lg'
      });
    };

    $scope.addHousehold = function() {
      $modal.open({
        templateUrl: '/app/_modals/add-household.html',
        controller: 'AddHousehold',
        size: 'lg'
      });
    };

    $scope.addAffiliation = function() {
      $modal.open({
        templateUrl: '/app/_main/modals/affiliations/add_affiliations.html',
        controller: 'AddAffiliationsController',
        size: 'md'
      });
    };

    $scope.newTransaction = function () {
      $modal.open({
        templateUrl: '/app/_modals/new-transaction.html',
        controller: 'NewTransaction',
        size: 'md'
      });
    };

    $scope.createScholarship = function() {
      $modal.open({
        templateUrl: '/app/_modals/create-scholarship.html',
        controller: 'CreateScholarship',
        size: 'md'
      });
    };

    $scope.addUser = function() {
      $modal.open({
        templateUrl: '/app/_main/modals/users/add-user.html',
        controller: 'AddUserController',
        size: 'lg'
      });
    };

    $scope.addGroup = function() {
      $modal.open({
        templateUrl: '/app/_main/modals/groups/add-group.html',
        controller: 'AddGroupController',
        size: 'lg'
      });
    }
  }
})(angular);