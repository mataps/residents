+(function(angular, undefined) {

  'use strict';

  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var state = {
      name: 'main',
      abstract: true,
      templateUrl: '/app/_main/main.html',
      controller: 'MainController'
    };

    $stateProvider.state(state);
  }
})(angular);
