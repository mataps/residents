+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('AddAffiliationsController', AddAffiliationsController);

  function AddAffiliationsController($scope, $state, flash) {
    var vm = this;

    angular.extend(vm, {
      isLoading: false,
      custom: {
        datepickers: {},
        selected: {}
      },
      data: {
        members: [],
        transactions: []
      },

      $errorHandler: $errorHandler,
      $successHandler: $successHandler
    });

    /**
     * Success response
     */
    function $successHandler(response) {
      vm.isLoading = false;

      flash.success = 'Affiliation has been successfully registered!';

      $scope.$close();
    }

    /**
     * Error response
     */
    function $errorHandler(response) {
      vm.isLoading = false;

      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }
  }
})(angular);