+(function(angular, _, undefined) {
  function AddGroupController($scope, $controller, $state, UtilService, Restangular, Group, flash, $modalInstance, PERMISSIONS, GROUPS) {
    // Inherit BaseIndexCtrl's prototype,
    var controller = $controller('BaseIndexCtrl', {
      $scope: $scope,
      $state: $state,
      UtilService: UtilService
    });
    angular.extend(this, controller);

    var vm = this,
        permissions = angular.copy(PERMISSIONS),
        form = {
          data: { }
        };

    // ------------------------- //
    // ------------------------- //

    // Form data
    vm.form = form;
    $scope.form = form;

    // ------------------------- //
    // ------------------------- //


    var formatPermissions = function() {
      var components = _.countBy(permissions.data, 'component');
      return _.mapValues(components, function(value, key) {
        return _.filter(permissions.data, {'component':key});
      });
    };

    $scope.permissions = formatPermissions();
    $scope.groups = GROUPS.data;

    var success = function(res) {
      $scope.isLoading = false;
      flash.success = 'Group created';
      vm.pushToCollection(Group.collection, res);
      $scope.$close();
    };

    var error = function(res) {
      flash.to('validation').error = _.chain(res.data.errors).values().flatten().value();
    };

    $scope.onSubmit = function() {
      var _permissions = [];
      var formData = Restangular.copy($scope.form.data);

      angular.forEach($scope.permissions, function(permissions) {
        angular.forEach(permissions, function(permission) {
          _permissions.push(permission)
        });
      });

      formData.permissions = _permissions;
      Restangular.all('groups').post(formData).then(success, error);
    };

    $scope.resetAllPermissions = function() {
      angular.forEach($scope.permissions, function(permissions) {
        angular.forEach(permissions, function(permission) {
          permission.status = 0;
        });
      });
    };

    $scope.allowAllPermissions = function() {
      angular.forEach($scope.permissions, function(permissions) {
        angular.forEach(permissions, function(permission) {
          permission.status = 1;
        });
      });
    };
  }

  angular
      .module('app')
      .controller('AddGroupController', AddGroupController);
})(angular, _);