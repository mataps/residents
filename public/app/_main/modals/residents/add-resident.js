+(function(angular, _, undefined) {
  function AddResidentCtrl($scope, Resident, ssWebcam, FileUploader, flash, $state, sigPlus, SigplusResource, Restangular, $rootScope, datResolve) {
    var vm = this,
      // Form data
      form = {
        data: {},
        custom: {
          datepickers: { birthdate: false },
          openDatePicker: openDatePicker
        }
      };

    // ------------------------- //
    // ------------------------- //
    vm.isLoading = false;
    vm.form = form;
    vm.submit = submit;

    vm.districts = function($viewValue) {
      return Restangular.service('districts')
        .getList({ limit: 696969696969999, filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }
    
    vm.citiesMunicipalities = function($viewValue) {
      return Restangular.service('citiesmunicipalities')
        .getList({ limit: 696969696969999, filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }


    vm.barangays = function($viewValue) {
      return Restangular.service('barangays')
        .getList({ limit: 696969696969999, filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }

    vm.successXHRCallback = successXHRCallback;
    vm.errorXHRCallback = errorXHRCallback;

    // ------------------------- //
    // ------------------------- //

    /**
     * @description  Sends an XHR to the server with
     *               the form data for submit a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    function submit(formData) {
      load();

      // Uses either restangular
      // or the uploader's provided
      // XHR
      if (hasImage()) {
        vm.uploader.uploadAll();
      } else {
        Resident
        .Command
        .create(formData)
        .then(vm.successXHRCallback)
        .catch(vm.errorXHRCallback);
      }
    }

    // ------------------------- //
    // ------------------------- //
    function load(bool) {
      vm.isLoading = ( !_.isUndefined(bool) ) ? bool : true;
    }

    // ------------------------- //
    // ------------------------- //

    // Callback function used after
    // a successful XHR
    function successXHRCallback(response) {
      load(false);
      flash.success = 'The resident has been successfully registered!';

      $rootScope.$broadcast('RESIDENT_CREATED', response);
      // Close modal 
      datResolve(response);
      $scope.$close();
    }

    // Callback function used after an XHR
    // returning with errors
    function errorXHRCallback(response) {
      load(false);
      flash.to('validation').error = _.chain(response.data.errors).values().flatten().value();
    }

    // ------------------------- //
    // ------------------------- //
    function hasImage() {
      return vm.uploader.queue.length !== 0;
    }

    /**
     * @name  openDatePicker
     * @description  A toggle for the datepickers
     * @param  e $event
     * @param  string model 
     */
    function openDatePicker($event, model) {
      $event.preventDefault();
      $event.stopPropagation();

      vm.form.custom.datepickers[model] = !vm.form.custom.datepickers[model];
    }

    $scope.openSigPlus = function() {
      sigPlus.open({
        onDone: function(hex) {
          SigplusResource.post({sigImgData: hex}).then(function(res) {
            vm.form.data.signature_path = res.data.data.url;
          });
        }
      });
    };
  }

  angular
    .module('app')
    .controller('AddResidentCtrl', AddResidentCtrl);
})(angular, _);