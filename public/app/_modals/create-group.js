+(function(angular){

    angular.module('app')
        .controller('CreateGroup', function($scope, PERMISSIONS) {
            var permissions = PERMISSIONS;

            $scope.form = {
                data: {},
                errors: {}
            };

            var formatPermissions = function() {
                var components = _.countBy(permissions.data, 'component');
                return _.mapValues(components, function(value, key) {
                    return _.filter(permissions.data, {'component':key});
                });
            };

            $scope.permissions = formatPermissions();

            var success = function(res) {
                flash.success = 'Group created';
                $state.go('group.index');
            };

            var error = function(res) {
                flash.to('validation').error = _.chain(res.data.errors).values().flatten().value();
            };

            $scope.onSubmit = function() {
                $scope.form.data.permissions = _.pluck(_.filter(permissions.data, {'checked': true}), 'id');
                Restangular.all('groups').post($scope.form.data).then(success, error);
            };

            $scope.allowAllPermissions = function() {
                angular.forEach(permissions.data, function(permission) {
                    permission.status = 1;
                });
            };

            $scope.resetAllPermissions = function() {
                angular.forEach(permissions.data, function(permission) {
                    permission.status = 0;
                });
            };
        });

})(angular);