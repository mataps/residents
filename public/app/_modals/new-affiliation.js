+(function(angular){

    angular.module('app').controller('NewAffiliation', function($scope, Restangular,flash, $state) {



    	$scope.getResidents = function searchResident(name) {
    		var filter = {full_name : name};
    		var stringFilter = JSON.stringify(filter);
    		return Restangular.allUrl('residents').getList({filters: stringFilter}).then(function(res) {
                return res.data;
            });
    	}

      $scope.onSelect = function($item, $model, $label) {
        console.log($item);
        console.log($model);
        console.log($label);

        $scope.form.data.founder = $item.id;
      }

      var success = function(res) {
            $scope.isLoading = false;
            flash.success = 'Affiliation created';
            
            $state.go('affiliations', {}, {reload: true});
        };

        var error = function(res) {
            $scope.isLoading = false;
            flash.to('validation').error = _.chain(res.data.errors).values().flatten().value();
        };

      $scope.createAffiliation = function() {
            $scope.isLoading = true;
            
                Restangular.all('affiliations').post($scope.form.data).then(success, error);
            
        };

    });

})(angular);