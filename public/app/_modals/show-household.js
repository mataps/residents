+(function(angular){

    angular.module('app').controller('ShowHousehold', function($scope, Restangular, household, flash, $state, $modal, $modalInstance) {
        $scope.form = {
            data: household.clone(),
            error: {}
        };

        $scope.isLoading = false;

        $scope.openHouseInfo = function() {
            var modalInstance = $modal.open({
                templateUrl: '/app/_modals/show-household.html',
                controller: 'ShowHousehold',
                size: 'sm'
            });
        };

        var success = function(res) {
            $scope.isLoading = false;
            flash.success = 'Household updated';
            Restangular.copy($scope.form.data, household);
            $modalInstance.close();
        };

        var error = function(res) {
            $scope.isLoading = false;
            flash.to('validation').error = _.chain(res.data.errors).values().flatten().value();
        };


        $scope.addMember = function() {
            
        }

        $scope.save = function() {
            $scope.isLoading = true;
            $scope.form.data.put().then(success, error);
        };
    });

})(angular);