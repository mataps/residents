+(function(angular){

    angular.module('app').controller('ShowResident', function($scope, Restangular, ssWebcam, DistrictsVal, CitiesMunicipalitiesVal, BarangaysVal, FileUploader, resident, flash, $state, $modal, $modalInstance) {
        $scope.form = {
            data: resident.clone(),
            error: {}
        };

        $scope.districts = DistrictsVal;
        $scope.citiesMunicipalities = CitiesMunicipalitiesVal;
        $scope.barangays = BarangaysVal;

        function resetImagePreview() {
            $scope.webcamImage = undefined;
            $scope.uploader.clearQueue();
        }

        $scope.uploader = new FileUploader({
            url: 'api/v1/residents/' + $scope.form.data.id,
            alias: 'profile_image',
            headers: {
                Accept: 'application/json'
            },
            autoUpload: false,
            onAfterAddingFile: function(item) {
                $scope.webcamImage = undefined;
                if ($scope.uploader.queue.length > 1)
                    $scope.uploader.removeFromQueue($scope.uploader.queue[0]);
            },
            onBeforeUploadItem: function(item) {
                angular.forEach($scope.form.data.plain(), function(value, key){
                    var data = {};
                    data[key] = value;
                    item.formData.push(data);
                });
                item.formData.push({'_method': 'PUT'});
            },
            onSuccessItem: function(item, response, status, header){
                success(response);
            },
            onErrorItem: function(item, response, status, header){
                error(response);
            }
        });

        $scope.uploader.filters.push({
            name: 'imageFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });

        $scope.isLoading = false;
        $scope.datepickers = {
            birthdate: false
        };

        $scope.openHouseInfo = function() {
            var modalInstance = $modal.open({
                templateUrl: '/app/_modals/add-resident.html',
                controller: 'ShowResident',
                size: 'sm'
            });
        };

        var success = function(res) {
            $scope.isLoading = false;
            flash.success = 'Resident updated';
            Restangular.copy(res.data.data, resident);
            $modalInstance.close();
        };

        var error = function(res) {
            $scope.isLoading = false;
            flash.to('validation').error = _.chain(res.data.errors).values().flatten().value();
        };

        var hasImage = function() {
            return $scope.uploader.queue.length !== 0;
        };

        $scope.save = function() {
            $scope.isLoading = true;
            if (hasImage()) {
                $scope.uploader.uploadAll();
            } else {
                $scope.form.data.put().then(success, error);
            }
        };

        $scope.openDatepicker = function($event, model) {
            $event.preventDefault();
            $event.stopPropagation();

            $scope.datepickers[model] = !$scope.datepickers[model];
        };

        $scope.takePicture = function() {
            ssWebcam.open({
                width: 640,
                height: 480,
                dest_width: undefined,
                dest_height: undefined,
                onDone: function(data_uri) {
                    resetImagePreview();
                    $scope.webcamImage = data_uri;
                    $scope.form.data.profile_image = data_uri;
                }
            });
        };
    });

})(angular);