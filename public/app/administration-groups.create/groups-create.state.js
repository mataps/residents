+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'New group' };
    var modal;

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = undefined;
    // 
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('administration.groups');
      }
        
      modal = $modal
        .open({
          size: 'lg',
          resolves: resolves,
          controller: 'GroupsCreateCtrl',
          controllerAs: 'createCtrl',
          templateUrl: '/app/administration-groups.create/groups-create.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }
    

    var state = {
      url: '/create',
      name: 'administration.groups.create',
      data: data,
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);