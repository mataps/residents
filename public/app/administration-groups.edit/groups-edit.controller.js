+(function(angular, _, undefined) {
  function GroupsEditCtrl($scope, $http, $controller, Group, Restangular, flash, $state, PERMISSIONS, GROUPS, $modalInstance, UtilService) {
    // Inherit BaseIndexCtrl's prototype,
    var controller = $controller('BaseIndexCtrl', {
      $scope: $scope,
      $state: $state,
      UtilService: UtilService
    });
    angular.extend(this, controller);

  
        permissions = angular.copy(PERMISSIONS),
        form = {
          data: Group.instance.clone()
        };

    //autofill form data
    _.mapValues(Group.instance.permissions, function(value) {
      _.mapValues(permissions.data, function(val) {
        if (value.id === val.id) {
          val.status = value.status;
        }
      });
    });


     // viewmodel
    var vm = this;

    // ------------------------- //
    // ------------------------- //
    //vm.create = create;

    // Form data
    vm.form = form;

    vm.members = [];

    // ------------------------- //
    // ------------------------- //

    vm.toggle_add_member = false;

    (function init() {
      $http.get('/api/v1/groups/' + vm.form.data.id + '/users')
       .then(function(response) {
          vm.members = response.data.data;
          console.log(vm.members);
         });
 


    })();


    var formatPermissions = function() {
      var components = _.countBy(permissions.data, 'component');
      return _.mapValues(components, function(value, key) {
        return _.filter(permissions.data, {'component':key});
      });
    };


    vm.permissions = formatPermissions();
    vm.groups = GROUPS.data;

    var success = function(res) {
      vm.isLoading = false;
      flash.success = 'Group saved!';
      vm.updateInCollection(Group.collection, res);
      $modalInstance.close();
    };

    var error = function(res) {
      flash.to('validation').error = _.chain(res.data.errors).values().flatten().value();
    };



    vm.addMemberToggle = function()
    {
      vm.toggle_add_member = !vm.toggle_add_member;
      console.log(vm.toggle_add_member);
    }

    vm.addMember = function(user_id)
    {
      return $http.post('/api/v1/groups/' + vm.form.data.id + '/users', { user_id : user_id })
                  .then(function(response){
                    flash.success = "User is successfully added.";
                    vm.members.push(response.data);
                    vm.add_username = "";
                    vm.add_user_id = null;
                    vm.toggle_add_member = !vm.toggle_add_member;
                  });
    }

    vm.$removeMember = function(member){

        var index = vm.members.indexOf(member);


        return $http.delete('/api/v1/groups/' + vm.form.data.id + '/users/' + member.id)
                    .then(function(response){

                      flash.success = "User is sucessfully detached..";
                   
                       vm.members.splice(index, 1);
                    });

    }

    vm.submit = function() {
      var _permissions = [];
      var formData = Restangular.copy(vm.form.data);

      angular.forEach(vm.permissions, function(permissions) {
        angular.forEach(permissions, function(permission) {
          _permissions.push(permission)
        });
      });

      formData.permissions = _permissions;
      formData.put().then(success, error);
    };

    vm.resetAllPermissions = function() {
      angular.forEach(vm.permissions, function(permissions) {
        angular.forEach(permissions, function(permission) {
          permission.status = 0;
        });
      });
    };

    vm.allowAllPermissions = function() {
      angular.forEach(vm.permissions, function(permissions) {
        angular.forEach(permissions, function(permission) {
          permission.status = 1;
        });
      });
    };
  }

  angular
      .module('app')
      .controller('GroupsEditCtrl', GroupsEditCtrl);
})(angular, _);