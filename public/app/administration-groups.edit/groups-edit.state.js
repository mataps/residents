+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Edit group' };
    var modal;

    /**
     * @see  Group.Repository.get
     * @param  {Object} Group DI
     * @return promise
     */
    function getGroupByIdResolve(Group, $state, $stateParams) {
      return Group
          .Repository
          .get($stateParams.id);
    }

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = { getGroupByIdResolve: getGroupByIdResolve };
    // 
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('administration.groups');
      }
        
      modal = $modal
        .open({
          size: 'lg',
          controller: 'GroupsEditCtrl',
          controllerAs: 'editCtrl',
          templateUrl: '/app/administration-groups.edit/groups-edit.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }
    

    var state = {
      url: '/{id}/edit',
      name: 'administration.groups.edit',
      data: data,
      onEnter: onEnter,
      resolve: resolves,
      onExit: onExit
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);