+(function(angular, undefined) {
  var columns = [
    {
      key: 'name',
      label: 'Name',
      show: true,
      cellTemplate: '{{:: group.name }}',
      onClick: 'groups.show(group.id)'
    },
    {
      key: 'description',
      label: 'Description',
      show: true,
      cellTemplate: '{{:: group.description }}',
      onClick: 'groups.show(group.id)'
    }
  ];

  angular
    .module('app')
    .constant('GROUPS_COLUMNS', columns);
})(angular);