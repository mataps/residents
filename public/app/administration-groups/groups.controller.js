+(function(angular, undefined) {
    'use strict';

  angular
    .module('app')
    .controller('GroupsController', GroupsController);

  function GroupsController($scope, $controller, UtilService, Restangular, GROUPS_COLUMNS, Group, $state) {
    // Inherit BaseIndexCtrl's prototype,
    var controller = $controller('BaseIndexCtrl', {
      $scope: $scope,
      $state: $state,
      UtilService: UtilService
    });

    angular.extend(this, controller);

    // ------------------------- //
    // ------------------------- //
    //
    // viewmodel
    var vm = this;

    // Initialize vm data
    vm.columns = GROUPS_COLUMNS;
    vm.collection = Group.collection;

    // Manually bind the "this"
    // from the base controller
    vm.callServer = angular.bind(vm, vm.callServer);
    vm.applyPageData = angular.bind(vm, vm.applyPageData);
    vm.catchPageDataError = angular.bind(vm, vm.catchPageDataError);

    // Commands that contact the API / server
    vm.remove = remove;
    vm.getPageData = getPageData;

    /**
     * @description  Sends a DELETE to the server
     * @see  getPageData
     * @return void
     */
    function remove() {
      var selected = vm.getSelected().join(',');

      if ( !selected.length ) {
        flash.error = 'Please select a group.';
        return;
      }

      if(!confirm('Please confirm deleting the selected records.'))
        return;

      Restangular
        .one('groups', selected)
        .one('remove')
        .remove()
        .finally(function() {
           window.location.reload();
        });
    }

    vm.QQ = {};

    /**
     * Fetches data and replaces the current
     * @param  string query
     * @return void
     */
    function getPageData(query) {
      if ( query.page ) {
        // console.log(query);
        // console.log(vm.QQ);
        angular.extend(vm.QQ, query);
        console.log(vm.QQ);
      } else {
        vm.QQ = query;
        console.log(vm.QQ);
      }

      return Group
        .Query
        .all(vm.QQ)
        .then(vm.applyPageData)
        .catch(vm.catchPageDataError);
    }
  }
})(angular);