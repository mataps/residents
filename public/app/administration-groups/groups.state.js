+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var data = { pageTitle: 'Manage groups' };
    var resolves = { getGroupsCollectionResolve: getGroupsCollectionResolve };

    var state = {
      name: 'administration.groups',
      url: '/groups',
      data: data,
      resolve: resolves,
      controllerAs: 'groups',
      controller: 'GroupsController',
      templateUrl: '/app/administration-groups/groups.html'
    };

    $stateProvider.state(state);
  }

  function getGroupsCollectionResolve(Group) {
    return Group
       .Repository
       .all();
  };
})(angular);