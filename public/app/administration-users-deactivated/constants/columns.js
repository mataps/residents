+(function(angular, undefined) {
  var columns = [
    {
      key: 'username',
      label: 'Username',
      show: true,
      cellTemplate: '{{:: user.username }}',
      onClick: 'users.show(user.id, "administration.users.edit")'
    },
    {
      key: 'group.name',
      label: 'Group',
      show: true,
      cellTemplate: '{{:: user.group.name }}',
      onClick: 'users.show(user.id, "administration.users.edit")'
    },
    {
      key: 'description',
      label: 'Description',
      show: true,
      cellTemplate: '{{:: user.description }}',
      onClick: 'users.show(user.id, "administration.users.edit")'
    }
  ];

  angular
    .module('app')
    .constant('USERS_COLUMNS', columns);
})(angular);