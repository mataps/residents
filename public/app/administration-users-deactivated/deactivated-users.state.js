+(function(angular, undefined) {
    angular
        .module('app')
        .config(config);

    function config($stateProvider) {
        var data = { pageTitle: 'Manage Deactivated Users' };
        var resolves = { getDeactivatedUsersCollectionResolve: getDeactivatedUsersCollectionResolve };

        var state = {
          name: 'administration.deactivated_users',
          url: '/deactivated-users',
          data: data,
          resolve: resolves,
          controllerAs: 'users',
          controller: 'DeactivatedUsersController',
          templateUrl: '/app/administration-users-deactivated/users.html'
        };

        $stateProvider.state(state);
    }

    function getDeactivatedUsersCollectionResolve(User) {
        return User
            .Repository
            .deactivated();
      };
})(angular);