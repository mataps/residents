+(function(angular, _, undefined) {
  function UsersCreateCtrl($scope, User, Restangular, flash, $state, PERMISSIONS, GROUPS, $modalInstance) {
    var vm = this,
        permissions = PERMISSIONS,
      // Form defaults
      form = {
        data: { }
      };

    // ------------------------- //
    // ------------------------- //
    //vm.create = create;

    // Form data
    vm.form = form;
    $scope.form = form;
    $scope.isLoading = false;

    // ------------------------- //
    // ------------------------- //


    var formatPermissions = function() {
      var components = _.countBy(permissions.data, 'component');
      return _.mapValues(components, function(value, key) {
        return _.filter(permissions.data, {'component':key});
      });
    };

    $scope.permissions = formatPermissions();
    $scope.groups = GROUPS.data;

    var success = function(res) {
      $scope.isLoading = false;
      flash.success = 'User created';
      var user = res.data;
      angular.forEach(res.data.data, function(value, key) {
        user[key] = value;
      });
      User.collection.push(user);
      $modalInstance.close();
      $state.go('administration.users', null, {reload: true});
    };

    var error = function(res) {
      $scope.isLoading = false;
      flash.to('validation').error = _.chain(res.data.errors).values().flatten().value();
    };

    $scope.onSubmit = function() {
      $scope.isLoading = true;
      $scope.form.data.permissions = permissions.data;
      console.log(permissions.data);  
      Restangular.all('users').post($scope.form.data).then(success, error);
    };

    $scope.inheritAllPermissions = function() {
      var _index = $scope.groups
        .map(function(group) { return group.id; })
        .indexOf(parseInt(form.data.group_id, 10));

      console.log($scope.groups);
      console.log(form.data.group_id);

      var _group = $scope.groups[_index];
      console.log(_group);

      angular.forEach(permissions.data, function(permission) {
        var component = permission.component.toLowerCase().replace(' ', '_');
        var name = permission.functionality.toLowerCase().replace(' ', '_');
        var _name = component + '.' + name;

        var _permission = _group.permissions
          .map(function(p) { return p.name; })
          .indexOf(_name);

        var __permission = _group.permissions[_permission];

        permission.status = _permission == -1
          ? 0
          : ( angular.isString(__permission.status) ? parseInt(__permission.status, 10) : __permission.status );
      });
    };

    $scope.allowAllPermissions = function() {
      console.log('Logged');
      angular.forEach(permissions.data, function(permission) {
        permission.status = 1;
      });
    };

    $scope.resetAllPermissions = function() {
      angular.forEach(permissions.data, function(permission) {
        permission.status = 0;
      });
    };
  }

  angular
    .module('app')
    .controller('UsersCreateCtrl', UsersCreateCtrl);
})(angular, _);