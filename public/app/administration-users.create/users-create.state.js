+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'New user' };
    var modal;

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = undefined;
    // 
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('administration.users');
      }
        
      modal = $modal
        .open({
          size: 'lg',
          resolves: resolves,
          controller: 'UsersCreateCtrl',
          controllerAs: 'createCtrl',
          templateUrl: '/app/administration-users.create/users-create.html'
        })

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }
    

    var state = {
      url: '/create',
      name: 'administration.users.create',
      data: data,
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);