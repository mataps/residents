+(function(angular, _, undefined) {
  function UsersEditCtrl($scope, User, flash, $state, PERMISSIONS, GROUPS, $modalInstance, $http) {
    var vm = this,
        permissions = angular.copy(PERMISSIONS),
      // Form defaults
      form = {
        data: User.instance.clone()
      };

    //autofill form data
    if(form.data.group){
      
      form.data.group_id = form.data.group.id;
    }
    _.mapValues(form.data.permissions, function(value) {
      var permission = _.find(permissions.data, {id: value.id});
      if (permission) {
        permission.status = value.status;
      }

      return permission;
    });

    // ------------------------- //
    // ------------------------- //
    //vm.create = create;

    // Form data
    vm.form = form;
    $scope.form = form;

    // ------------------------- //
    // ------------------------- //


    var formatPermissions = function() {
      var components = _.countBy(permissions.data, 'component');
      return _.mapValues(components, function(value, key) {
        return _.filter(permissions.data, {'component':key});
      });
    };

    $scope.permissions = formatPermissions();
    $scope.groups = GROUPS.data;

    $scope.onUpdate = function(formData) {
      formData.permissions = permissions.data;
      formData.put().then(success, error);
    };

    var success = function(res) {
      flash.success = 'User updated';
      // var user = _.find(User.collection, { id: res.data.id });
     
      $modalInstance.close();
      $state.go('administration.users', null, { reload: true });
    };

    var error = function(res) {
      flash.to('validation').error = _.chain(res.data.errors).values().flatten().value();
    };

    $scope.inheritAllPermissions = function() {
      var _index = $scope.groups
        .map(function(group) { return group.id })
        .indexOf(parseInt(form.data.group_id));

      var _group = $scope.groups[_index];

      angular.forEach(permissions.data, function(permission) {
        var component = permission.component.toLowerCase().replace(' ', '_');
        var name = permission.functionality.toLowerCase().replace(' ', '_');
        var _name = component + '.' + name;

        var _permission = _group.permissions
          .map(function(p) { return p.name; })
          .indexOf(_name);

        var __permission = _group.permissions[_permission];

        permission.status = _permission == -1
          ? 0
          : ( angular.isString(__permission.status) ? parseInt(__permission.status, 10) : __permission.status );
      });
    };

    $scope.allowAllPermissions = function() {
      angular.forEach(permissions.data, function(permission) {
        permission.status = 1;
      });
    };

    $scope.resetAllPermissions = function() {
      angular.forEach(permissions.data, function(permission) {
        permission.status = 0;
      });
    };
    $scope.deactivateUser = function(formData){
        
       var delete_this = confirm("Are you sure you want to deactivate " + formData.username);

        if(delete_this){
          $http.delete('/api/v1/users/' + formData.id)
               .then(function(res){
                  flash.success = 'User successfully deactivated..';
                       
                  $modalInstance.close();
                  $state.go('administration.users', null, { reload: true });
               });
        }
    }
  }


  angular
    .module('app')
    .controller('UsersEditCtrl', UsersEditCtrl);
})(angular, _);