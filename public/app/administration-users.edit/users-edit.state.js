+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Edit user' };
    var modal;

    /**
     * @see  User.Repository.get
     * @param  {Object} User DI
     * @return promise
     */
    function getUserByIdResolve(User, $state, $stateParams) {
      return User
          .Repository
          .get($stateParams.id);
    }

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = { getUserByIdResolve: getUserByIdResolve };

    // 
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('administration.users');
      }
        
      modal = $modal
        .open({
          size: 'lg',
          resolves: resolves,
          controller: 'UsersEditCtrl',
          controllerAs: 'editCtrl',
          templateUrl: '/app/administration-users.edit/users-edit.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit(){
      modal.close();
    }


    var state = {
      name: 'administration.users.edit',
      url: '/{id}/edit',
      resolve: resolves,
      data: data,
      onEnter: onEnter
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);