+(function(angular, undefined) {
  var columns = [
    {
      key: 'username',
      label: 'Username',
      show: true,
      cellTemplate: '{{:: user.username }}',
      onClick: 'users.show(user.id)'
    },
    {
      key: 'group.name',
      label: 'Group',
      show: true,
      cellTemplate: '{{:: user.group.name }}',
      onClick: 'users.show(user.id)'
    },
    {
      key: 'description',
      label: 'Description',
      show: true,
      cellTemplate: '{{:: user.description }}',
      onClick: 'users.show(user.id)'
    }
  ];

  angular
    .module('app')
    .constant('USERS_COLUMNS', columns);
})(angular);