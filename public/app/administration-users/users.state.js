+(function(angular, undefined) {
    angular
        .module('app')
        .config(config);

    function config($stateProvider) {
        var data = { pageTitle: 'Manage users' };
        var resolves = { getUsersCollectionResolve: getUsersCollectionResolve };

        var state = {
          name: 'administration.users',
          url: '/users',
          data: data,
          resolve: resolves,
          controllerAs: 'users',
          controller: 'UsersController',
          templateUrl: '/app/administration-users/users.html'
        };

        $stateProvider.state(state);
    }

    function getUsersCollectionResolve(User) {
        return User
            .Repository
            .all();
    };
})(angular);