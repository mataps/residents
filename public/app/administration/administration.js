+(function(angular){

    angular.module('app').config(function ($stateProvider) {
        var state = {
            parent: 'main',
            name: 'administration',
            url: '/administration',
            abstract: true,
            template: '<ui-view/>'
        };

        $stateProvider.state(state);
    });

})(angular);
