+(function(angular){

    angular.module('app').config(function ($stateProvider) {
        var state = {
            name: 'administration.settings',
            url: '/settings',
            templateUrl: '/app/administration/settings.html',
            data: {
                pageTitle: 'General Settings'
            },
            resolve: {},
            controller: 'SettingsController'
        };

        $stateProvider.state(state);
    });

    angular.module('app').controller('SettingsController', function() {
    });

})(angular);
