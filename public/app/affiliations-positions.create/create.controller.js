+(function(angular, _, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('AffiliationsPositionsCreateController', AffiliationsPositionsCreateController);

  function AffiliationsPositionsCreateController($scope, $http, flash, Restangular) {
    var vm = this;

    angular.extend(vm, {
      isLoading: false,
      data: {},
      $create: $create,
      $attrRequest: $attrRequest

    });

    vm.data.attributions = "";

    function $create(data) {
      console.log(data);
      vm.isLoading = true;
      // data.attributions = data.attribution_name;
      console.log(data);

      return Restangular
        .one('')
        .post('positions', data)
        .then(function(response) {
          flash.success = 'The affiliation was successfully created!';
          $scope.$close();
        })
        .catch(function(response) {
          flash.to('validation').error = _.chain(response.data.errors)
            .values()
            .flatten()
            .value();
        })
        .finally(function() {
          vm.isLoading = false;
        });
    }

        function $attrRequest($value) {
      return $http.get('/api/v1/attributions/?filters=' + JSON.stringify({ name: $value }))
        .then(function(response) {
          console.log(response);
          return response.data.data;
        });
    }
  }
})(angular, _);