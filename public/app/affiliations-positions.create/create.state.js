+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .config(configBlock);

  function configBlock($stateProvider) {
    var modal;

    $stateProvider.state({
      name: 'affiliations-positions.create',
      url: '/create',
      data: { pageTitle: 'Create a new affiliation position' },
      onEnter: function($modal, $state) {
        modal = $modal.open({
          size: 'sm',
          controller: 'AffiliationsPositionsCreateController',
          controllerAs: 'createCtrl',
          templateUrl: 'app/affiliations-positions.create/create.html'
        });

        modal.result
          .then(transitionToOverlay)
          .catch(transitionToOverlay);

        function transitionToOverlay() {
          return $state.go('affiliations-positions', null, { reload: true });
        }
      },
      onExit: function() {
        modal.close();
      }
    });
  }
})(angular);