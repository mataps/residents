+(function(angular, _, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('AffiliationsPositionsEditController', AffiliationsPositionsEditController);

  function AffiliationsPositionsEditController($scope, $state, $stateParams, flash, Restangular, Position, $http) {
    var vm = this;

    angular.extend(vm, {
      isLoading: false,
      data: {},
      $update: $update,
      $attrRequest: $attrRequest
    });

    // Position.instance;
    //   .one('positions', $stateParams.id)
    //   .get()
    //   .then(function(response) {
    //     vm.data = response.data;

    //     var _attribution = response.data.attributions.data[0];
    //     var _undefined = angular.isUndefined(_attribution);
    //     vm.data.attribution_id = !_undefined ? _attribution.id : '';
    //     vm.data.attribution_name = !_undefined ? _attribution.name : '';
    //   })
    //   .finally(function() { vm.isLoading = false });
    vm.data = Position.instance;
    var _attribution = vm.data.attributions.data[0];
    var _undefined = angular.isUndefined(_attribution);
    vm.data.attribution_id = !_undefined ? _attribution.id : '';
    vm.data.attribution_name = !_undefined ? _attribution.name : '';

    function $update(data) {
      console.log(data);
      vm.isLoading = true;
      vm.data.attributions = vm.data.attribution_name;

      return $http
        .put( 'api/v1/positions/' + $stateParams.id , data )
        .then(function(response) { flash.success = 'The affiliation was successfully updated!'; })
        .catch(function(response) {
          flash.to('validation').error = _.chain(response.data.errors)
            .values()
            .flatten()
            .value();
        })
        .finally(function() { vm.isLoading = false; });
    }

    /**
     *
     */
    function $attrRequest($value) {
      return $http.get('/api/v1/attributions/?filters=' + JSON.stringify({ name: $value }))
        .then(function(response) {
          console.log(response);
          return response.data.data;
        });
    }
  }
})(angular, _);