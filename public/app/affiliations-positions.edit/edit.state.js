+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .config(configBlock);

  function configBlock($stateProvider) {
    var modal;

    $stateProvider.state({
      url: '/{id}/edit',
      name: 'affiliations-positions.edit',
      date: { pageTitle: 'Edit an affiliation position' },
      onEnter: function($modal, $state) {
        modal = $modal.open({
          size: 'sm',
          controller: 'AffiliationsPositionsEditController',
          controllerAs: 'editCtrl',
          templateUrl: 'app/affiliations-positions.edit/edit.html',
        });

        modal.result
          .then(transitionToOverlay)
          .catch(transitionToOverlay);

        function transitionToOverlay() {
          return $state.go('affiliations-positions');
        }
      },
      onExit: function() {
        modal.close();
      },
      resolve: {
        positionResolve: function(Position, $stateParams) {
          return Position
            .Repository
            .get($stateParams.id);
        }
      }
    });
  }
})(angular);