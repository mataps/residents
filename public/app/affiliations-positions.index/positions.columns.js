+(function(angular, undefined) {
  var columns = [
    {
      key: 'name',
      label: 'Name',
      show: true,
      cellTemplate: '{{:: position.name }}',
      onClick: 'positions.show(position.id)'
    },
    {
      key: 'attributions.name',
      label: 'Attribution',
      show: true,
      cellTemplate: '{{ position.attributions.data[0] !== undefined ? position.attributions.data[0].name : \'\' }}',
      onClick: 'positions.show(position.id)'
    }
  ];

  angular
    .module('app')
    .constant('POSITIONS_COLUMNS', columns);
})(angular);