+(function(angular, undefined) {
  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var state = {
      parent: 'main',
      name: 'affiliations-positions',
      url: '/positions',
      templateUrl: 'app/affiliations-positions.index/positions.html',
      data: { pageTitle: 'Manage affiliation positions' },
      resolve: { getPositionsResolve: getPositionsResolve },
      controllerAs: 'positions',
      controller: 'AffiliationsPositionsController'
    };

    $stateProvider.state(state);

    /**
     * XHR to the API's affiliation list
     * @see Affiliation {resource}
     * @see Affiliation.Repository.all()
     * @return promise .then
     */
    function getPositionsResolve(Restangular) {
      return Restangular
        .one('positions')
        .getList();
    }
  }
})(angular);