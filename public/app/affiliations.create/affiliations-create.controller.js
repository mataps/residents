+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('AffiliationsCreateCtrl', AffiliationsCreateCtrl);

  function AffiliationsCreateCtrl($scope, $state, flash, Restangular) {
    var vm = this;

    angular.extend(vm, {
      isLoading: false,
      custom: {
        datepickers: {},
        selected: {}
      },
      data: {
        members: [],
        transactions: []
      },

      districts: districts,
      citiesMunicipalities: citiesMunicipalities,
      barangays: barangays,
      $errorHandler: $errorHandler,
      $successHandler: $successHandler
    });

    /**
     * Success response
     */
    function $successHandler(response) {
      vm.isLoading = false;

      flash.success = 'Affiliation has been successfully registered!';

      $state.go('affiliations', null, { reload: true });
    }

    /**
     * Error response
     */
    function $errorHandler(response) {
      vm.isLoading = false;

      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }

    function districts($viewValue) {
      return Restangular.service('districts')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }
    
    function citiesMunicipalities($viewValue) {
      return Restangular.service('citiesmunicipalities')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }


    function barangays($viewValue) {
      return Restangular.service('barangays')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }
  }
})(angular);