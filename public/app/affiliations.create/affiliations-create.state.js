+(function(angular) {
  'use strict';

  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var modal;

    /** Trigger modal */
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('affiliations');
      }
        
      modal = $modal
        .open({
          size: 'sm',
          controller: 'AffiliationsCreateCtrl as createCtrl',
          templateUrl: '/app/affiliations.create/affiliations-create.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }
    

    var state = {
      url: '/create',
      name: 'affiliations.create',
      data: { pageTitle: 'New affiliations' },
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }
})(angular);