+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('AffiliationsEditCtrl', AffiliationsEditCtrl);

  function AffiliationsEditCtrl($scope, $state, $http, Affiliation, flash, UtilService, Restangular, Position) {
    var vm = this;

    angular.extend(vm, {
      isLoading: true,
      custom: {
        datepickers: {}
      },
      data: Affiliation.getInstance(),

      vouchers: [],
      positions: Position.copies.affiliations,
      barangays: barangays,
      districts: districts,
      citiesMunicipalities: citiesMunicipalities,
      $errorHandler: $errorHandler,
      $successHandler: $successHandler,
      $updateResidentSuccess: $updateResidentSuccess,
      $updateResidentError: $updateResidentError,
      isLoadingVouchers: true,
      isLoadingPositions: false
    });

    vouchers();

    vm.data.members = [];

    $http.get('/api/v1/affiliations/' + vm.data.id + '/residents')
      .then(function(response) {
        vm.isLoading = false;
        console.log(response);

        vm.data.members = response.data.data
          .map(function(member) {
            // member.resident = member.resident || {};
            // member.resident_id = member.resident.id;
            // member.resident_name = member.resident.full_name;

            member.referrer = member.referrer || {};
            member.referrer_id = member.referrer.id;
            member.referrer_name = member.referrer.full_name;

            member.head = member.head || {};
            member.head_id = member.head.id;
            member.head_name = member.head.full_name;

            return member;
          });
        vm.data.members.paging = response.data.paging;
        console.log(vm.data.members);
        return response;
      });
    /**
     * Success response
     */
    function $successHandler(response) {
      vm.isLoading = false;

      flash.success = 'Affiliation has been successfully updated!';

      $state.go('affiliations', null, { reload: true });
    }

    /**
     * Error response
     */
    function $errorHandler(response) {
      vm.isLoading = false;

      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }

    function $updateResidentSuccess() {
      vm.isLoading = false;

      flash.success = 'Resident role has been successfully updated!';
    }

    function $updateResidentError() {
      vm.isLoading = false;

      flash.success = 'Resident role has been successfully updated!';
    }

    $scope.founderLength = vm.data.members.filter(function(x) {
      return !!x.founder
    }).length;

    $scope.officerLength = vm.data.members.filter(function(x) {
      return !!x.position
    }).length;

    function districts($viewValue) {
      return Restangular.service('districts')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }

    function citiesMunicipalities($viewValue) {
      return Restangular.service('citiesmunicipalities')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }


    function barangays($viewValue) {
      return Restangular.service('barangays')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }

    function vouchers() {
      return Restangular
        .one('households', vm.data.id)
        .one('vouchers')
        .getList()
        .then(function(response) { vm.vouchers = response.data; })
        .finally(function() { vm.isLoadingVouchers = false; });
    }
  }
})(angular);
