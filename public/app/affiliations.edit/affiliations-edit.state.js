+(function(angular) {
  'use strict';

  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var modal;

    /** Trigger modal */
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('affiliations');
      }
        
      modal = $modal
        .open({
          size: 'lg',
          controller: 'AffiliationsEditCtrl as editCtrl',
          templateUrl: '/app/affiliations.edit/affiliations-edit.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }    

    var state = {
      url: '/{id}/edit',
      name: 'affiliations.edit',
      data: { pageTitle: 'Edit affiliation' },
      onEnter: onEnter,
      onExit: onExit,
      resolve: {
        getAffiliationResolve: getAffiliationResolve,
        getAffiliationAndPositionsResolve: getAffiliationAndPositionsResolve
      }
    };

    $stateProvider.state(state);

    /** Fetch from /aff/ */
    function getAffiliationResolve(Affiliation, $stateParams) {
      return Affiliation
        .Repository
        .get($stateParams.id);
    }

    /** Fetch from /aff/ */
    function getAffiliationAndPositionsResolve(Position, $stateParams) {
      return Position
        .Repository
        .getWithAffiliation($stateParams.id);
    }
  }
})(angular);