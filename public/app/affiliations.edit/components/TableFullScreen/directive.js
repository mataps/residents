+(function(angular, undefined) {
  "use strict";
  angular
    .module('app')
    .directive('gcTableFullscreen', directive);

  function directive() {
    return {
      scope: { fullscreen: '=' },

      restrict: 'EA',
      link: linkFn
    };

    function linkFn(scope, element) {
      // Elements to be modified
      var $win = angular.element(window);
      var $table = element.children('[srph-infinite-scroll]');
      var $modal = angular.element('.modal-dialog');
      var $modalContainer = angular.element('.modal-open .modal');

      // Base CSS 
      var $modalCss;
      var $elmCss;

      scope.$watch('fullscreen', function(changed, old) {

        if ( changed === old ) {
        }
        else if ( changed ) {
          applyFullscreen();
        } else {
          removeFullscreen();
        }
      });

      function applyFullscreen() {
        $modalCss = $modal.css(['margin', 'width']);
        $elmCss = element.css(['position', 'top', 'left', 'height', 'background']);
        element.css({ 'position': 'fixed', 'top': 0, 'left': 0, 'height': '100%', 'background': '#fff', 'width': '100%' });
        $modal.css({ 'margin': '0', 'width': '100%' });
        $table.css({ 'height': '100%', 'width': '100%' });
      }

      function removeFullscreen() {
        element.css($elmCss);
        $modal.css($modalCss); 
        $table.css({ 'height': '400px' });
      }
    }
  }
})(angular);