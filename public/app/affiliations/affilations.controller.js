+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('AffiliationsCtrl', AffiliationsCtrl);

  function AffiliationsCtrl($scope, $controller, UtilService, AFFILIATIONS_COLUMNS, Affiliation, flash, $state, $modal) {
    // Inherit BaseIndexCtrl's prototype,
    var controller = $controller('BaseIndexCtrl', {
      $scope: $scope,
      $state: $state,
      UtilService: UtilService
    });

    angular.extend(this, controller);

    // ------------------------- //
    // ------------------------- //
    // 
    // viewmodel
    var vm = this,    
       // Data collection (grabbed from the service)
      collection = [];

    // Copy items from the service
    // to the local collection variable
    angular.extend(collection, Affiliation.collection);

    vm.$state = $state;

    // Initialize vm data
    vm.columns = AFFILIATIONS_COLUMNS;
    vm.collection = collection;

    // Manually bind the "this"
    // from the base controller
    vm.callServer = angular.bind(vm, vm.callServer);
    vm.applyPageData = angular.bind(vm, vm.applyPageData);
    vm.catchPageDataError = angular.bind(vm, vm.catchPageDataError);

    // Commands that contact the API / server
    vm.getSelected = angular.bind(vm, vm.getSelected);
    vm.remove = remove;
    vm.generateMemberList = generateMemberList;
    vm.getPageData = getPageData;
    vm.$report = $report;
    vm.fullscreen = false;

    /**
     * @description  Sends a DELETE to the server
     * @return Restangular
     */
    function remove() {
      var selected = vm.getSelected().join(',');
      
      if ( !selected.length ) {
        flash.error = 'Please select a transaction.';
        return;
      }

      if(!confirm('Please confirm deleting the selected records.'))
        return;

      vm.load();

      return Affiliation
        .Command
        .delete(selected)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    };

    vm.QQ = {};

    /**
     * Fetches data and replaces the current
     * @param  string query
     * @return void
     */
    function getPageData(query) {
      if ( query.page ) {
        // console.log(query);
        // console.log(vm.QQ);
        angular.extend(vm.QQ, query);
        console.log(vm.QQ);
      } else {
        vm.QQ = query;
        console.log(vm.QQ);
      }

      return Affiliation
        .Query
        .all(vm.QQ)
        .then(vm.applyPageData)
        .catch(vm.catchPageDataError);
    }

    // Success XHR
    function successXHRCallback(response) {
      var message = 'Affiliation has been successfully archived!';
      flash.success = message;
      $state.reload();
      vm.load(false);
    }

    // Error XHR
    function errorXHRCallback(response) {
      var message = 'An error has occured. Please try again..';
      flash.error = message;
      vm.load(false);
    }


    function generateMemberList() {
        flash.info = 'Opening Member List Generation Window..';

        $modal.open({
          size: 'sm',
          templateUrl: '/app/affiliations/members/members.html',
          controller: 'AffiliationMemberController as memberCtrl'
        }).result.catch(function() {
          $state.reload();
        });
    }

    function $report() {
      var selected = vm.getSelected();

      if ( !selected.length ) {
        flash.error = 'Please select an affiliation first!';
        return;
      } else if ( selected.length > 1 ) {
        flash.error = 'Please select only one affiliation';
        return;
      }

      $modal.open({
        controller: 'AffiliationReportController',
        templateUrl: '/app/affiliations/reports/reports.html',
        resolve: {
          filterResolve: function() {
            return vm.__query;
          },
          affiliationIdResolve: function() {
            return selected[0];
          }
        }
      })
    }
  }
})(angular);
