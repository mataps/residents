+(function(angular, undefined) {
  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var state = {
      parent: 'main',
      name: 'affiliations',
      url: '/affiliations',
      templateUrl: 'app/affiliations/affiliations.html',
      data: { pageTitle: 'Manage affiliations' },
      resolve: { getAffiliationsResolve: getAffiliationsResolve },
      controllerAs: 'affiliations',
      controller: 'AffiliationsCtrl'
    };

    $stateProvider.state(state);

    /**
     * XHR to the API's affiliation list
     * @see Affiliation {resource}
     * @see Affiliation.Repository.all()
     * @return promise .then
     */
    function getAffiliationsResolve(Affiliation) {
      return Affiliation
        .Repository
        .all();
    }
  }
})(angular);