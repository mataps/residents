+(function (angular, undefined) {
  'use strict';

  angular
    .module('app')
    .directive('gcAffiliationFounderButton', directive);

  function directive() {
    return {
      /**
       *
       */
      scope: {
        data: '=',
        affiliationId: '@'
      },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    /**
     *
     */
    function controllerFn($scope, $http, flash, Restangular) {
      angular.extend($scope, {
        isLoading: false,
        $request: $request
      });

      function $request(data) {
        $scope.isLoading = true;

        console.log(data);

        var url = [
          'api/v1/affiliations/',
          $scope.affiliationId,
          '/residents/',
          data.resident.id
        ].join('');

        data.position_id = data.position.id;
        data.founder = ( typeof data.founder == 'boolean'
          ? !data.founder
          : !parseInt(data.founder, 10)
        );

        return $http.put(url, data)
          .then(success)
          .catch(error)
          .finally(function() { $scope.isLoading = false });

        //
        function success(response) {
          var founder = ( !!data.founder ? 'removed' : 'set' );
          var message = 'Resident has been ' + founder + ' as founder';

          flash.success = message;
        }

        //
        function error(response) {
          flash.to('validation').error = _.chain(response.data.errors)
            .values()
            .flatten()
            .value();
        }
      }
    }

    function templateFn() {
      return [
          '<button type="button" ',
            'ng-click="$request(data)" ',
            'ng-show="data.founder == \'0\' || data.founder == 0 || data.founder == false" ',
            'ng-disabled="isLoading" ',
            'class="btn btn-danget btn-sm pull-left">',
            'Set as Founder',
          '</button>',

          '<button type="button" ',
            'ng-click="$request(data)" ',
            'ng-show="data.founder == \'1\' || data.founder == 1 || data.founder == true" ',
            'ng-disabled="isLoading" ',
            'class="btn btn-danget btn-sm pull-left">',
            'Remove as Founder ',
          '</button> '
      ].join('');
    }
  }
})(angular);
