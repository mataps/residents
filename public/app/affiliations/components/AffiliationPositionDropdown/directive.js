+(function(angular, _, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcAffiliationPositionDropdown', directive);

  function directive() {
    var scope = {
      affiliation_id: '@affiliationId',
      request: '=',
      data: '='
    };

    return {
      restrict: 'EA',
      scope: scope,
      controller: controllerFn,
      template: templateFn
    };

    function controllerFn($scope, $http, Restangular, Position, flash) {
      // var _affiliations = Position.copies.affiliations;
      // var _positions = Position.copies.positions;
      // var posIds = _positions.map(function(pos) { return pos.id });
      // var affIds = _affiliations.map(function(pos) { return pos.id }).filter(function(id) { return posIds.indexOf(id) == -1 });
      // var ids = [affIds, posIds].join(',').split(',').sort().join(',');
      // $scope.isLoading = false;

      // $scope.positions = _positions.filter(function(pos) { return ids.split(',').indexOf(pos.id) !== -1 });
      // console.log($scope.positions);
      $scope.positions = Position.copies.affiliations;
      var ids = $scope.positions.map(function(pos) { return pos.id }).join(',');

      $scope.update = function(data) {
        if ( !angular.isUndefined($scope.data.position) ) {
          if ( !angular.isObject($scope.data.position) ) {
            $scope.data.position = {}
          }

          $scope.data.position.id = $scope.data.position_id;
        } else {
          $scope.data.position = {}
          $scope.data.position.id = $scope.data.position_id;
        }

        $scope.data.position.name = $scope.positions[($scope.positions
            .map(function(position) { return parseInt(position.position.id, 10); })
              .indexOf(parseInt($scope.data.position.id, 10)))]
            .position
            .name;


        if ( angular.isUndefined($scope.request) || !$scope.request ) {
          return;
        }

        $scope.isLoading = true;

        var url = [
          'api/v1/affiliations/',
          $scope.affiliation_id,
          '/residents/',
          data.resident.id
        ].join('');

        $http.put(url, data)
        .then(function(response) { flash.success = 'Position has been successfully updated'; })
        .catch(error)
        .finally(function() { $scope.isLoading = false });

        function error(response) {
          flash.to('validation').error = _
            .chain(response.data.errors)
            .values()
            .flatten()
            .value();
        }
      }
    }

    function templateFn() {
      return [
        '<select class="form-control" ng-model="data.position_id" ng-change="update(data)">',
          '<option value="">{{ isLoading ? \'The positions are being loaded..\' : \'Select a position..\' }} </option>',
          '<option ng-repeat="position in positions" value="{{ position.position.id }}" ng-selected="position.position.id == data.position.id">{{ position.position.name }}</option>',
       '</select>'
      ].join('');
    }
  }
})(angular, _);
