+(function(angular, _, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcAffiliationPositionDropdownAll', directive);

  function directive() {
    var scope = {
      model: '=ngModel'
    };

    return {
      restrict: 'EA',
      scope: scope,
      controller: controllerFn,
      template: templateFn
    };

    function controllerFn($scope, $http) {
      $scope.positions = [];
      $scope.isLoading = true;

      ;(function init() {
        $http.get('api/v1/positions')
          .then(function(res) {
            $scope.positions = res.data.data;
            return res;
          })
          .catch(function(err) {
            console.error(err);
            return err;
          })
          .finally(function() {
            $scope.isLoading = false;
          })
        })();
    }

    function templateFn() {
      return [
        '<select class="form-control" ng-model="model">',
          '<option value="">{{ isLoading ? \'The positions are being loaded..\' : \'Select a position..\' }} </option>',
          '<option ng-repeat="position in positions" value="{{ position.id }}">{{ position.name }}</option>',
       '</select>'
      ].join('');
    }
  }
})(angular, _);
