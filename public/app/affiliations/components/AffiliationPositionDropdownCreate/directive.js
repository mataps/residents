+(function(angular, _, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcAffiliationPositionCreateDropdown', directive);

  function directive() {
    var scope = {
      affiliation_id: '@affiliationId',
      data: '='
    };

    return {
      restrict: 'EA',
      scope: scope,
      controller: controllerFn,
      template: templateFn
    };

    function controllerFn($scope, $http, Restangular, Position) {
      var _positions = Position.copies.positions;
      var _affiliations = Position.copies.affiliations;
      var affIds = _affiliations.map(function(pos) { return pos.position.id });
      var posIds = _positions.map(function(pos) { return pos.id }).filter(function(id) { return affIds.indexOf(id) == -1; });
      var ids = posIds.sort().join(',');
      $scope.isLoading = false;

      $scope.positions = _positions.filter(function(pos) { return ids.indexOf(pos.id) !== -1 });
    }

    function templateFn() {
      return [
        '<div ng-if="isLoading"> <circle-spinner></circle-spinner> </div>',
        '<select class="form-control" ng-model="data.id" ng-if="!isLoading">',
          '<option value="">Select a position..</option>',
          '<option ng-repeat="position in positions" value="{{ position.id }}">{{ position.name }}</option>',
       '</select>'
      ].join('');
    }
  }
})(angular, _);