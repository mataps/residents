+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcAffiliationPositionDropdownDeferred', directive);

  function directive() {
    return {
      scope: {
        'id': '=affiliationId',
        'model': '=gcAffiliationPositionDropdownDeferred'
      },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    function controllerFn($scope, $http) {
      angular.extend({
        isLoading: true,
        positions: []
      });

      request();

      $scope.$watch('id', function(next, previous) {
        if ( !$scope.isLoading && next !== previous ) {
          $scope.model = undefined;
          request();
        }
      });

      function request() {
        var id = $scope.id;
        var url = ['api/v1/affiliations/', id, '/positions'].join('');

        $scope.isLoading = true;

        return $http.get(url)
          .then(function(response) { $scope.positions = response.data.data; })
          .finally(function() { $scope.isLoading = false })
      }
    }

    function templateFn() {
      return [
        '<select class="form-control" ng-model="model">',
          '<option value="">{{ isLoading ? \'Loading positions...\' : ( !!positions.length ? \'Select a position..\' : \'No positions..\' ) }}</option>',
          '<option ng-repeat="position in positions" value="{{ position.position.id }}">{{ position.position.name }}</option>',
       '</select>'
      ].join('');
    }
  }
})(angular);