+(function(angular, _, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcAffiliationPositionForm', directive);

  function directive() {
    return {
      restirct: 'EA',
      scope: {
        list: '=',
        data: '=',
        mode: '@',
        affiliation_id: '@affiliationId'
      },
      template: templateFn,
      controller: controllerFn
    };

    function controllerFn($scope, flash, Restangular, Position) {
      $scope.isLoading = false;
      $scope.$request = $request;
      $scope.data = /^create/i.test(mode) ? {} : $scope.data || {};

      var mode = $scope.mode || 'create';
      var _affiliations = Position.copies.affiliations;
      var ids = _affiliations.map(function(pos) { return pos.position.id }).join(',');

      function $request(data) {
        $scope.isLoading = true;

        console.log(mode);
        console.log(data);

        if ( data.id && ids.indexOf(data.id) == -1 )  {
          ids = [data.id, ids].join(',').split(',').sort().join(',');

          if ( ids[ids.length - 1] == ',' ) {
            ids = ids.substr(0, ids.length);
          }

          if ( ids[0] == ',' ) {
            ids = ids.substr(1, ids.length + 1);
          }
        }

        if ( /^create$/i.test(mode) ) {
          $create(data);
        }

        else if ( /^update$/i.test(mode) ) {
          $update(data);
        }
      }

      function $create(data) {
        return Restangular
          .one('affiliations', $scope.affiliation_id)
          .post('positions?positions=' + ids, data)
          .then(function(response) {
            var _pos = Position.copies.positions;
            var _ids = _pos.map(function(x) { return x.id; });
            var _res = _pos[_ids.indexOf( parseInt(data.id) )];

            // Position.copies.affiliations.push({ position: _res });
            $scope.list.push({ position: _res });
            flash.success = 'Position has been successfully added to this specific affiliation';
            $scope.data = {};
          })
          .catch(function(response) {
            flash.to('validation').error = _.chain(response.data.errors)
              .values()
              .flatten()
              .value();
          })
          .finally(function() { $scope.isLoading = false });
      }
    }

    function templateFn() {
      return [
        '<div class="row form-group">',
          '<div class="col-md-10">',
            // '<input type="text" class="form-control" ng-model="data.name" placeholder="Enter position name">',
            '<gc-affiliation-position-create-dropdown data="data" affiliation-id="{{ affiliation_id }}">',
            '</gc-affiliation-position-create-dropdown>',
          '</div>',
          '<div class="col-md-2">',
            '<button type="button" ng-click="$request(data)" class="btn btn-primary form-control" ng-disabled="isLoading">',
              '{{ mode == \'create\' ? \'Add\' : \'Update\' }} Position',
            '</button>',
          '</div>',
        '</div>'
      ].join('');
    }
  }
})(angular, _);