  +(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcAffiliationResidentActive', directive);

  function directive() {
    var scope = {
      data: '=',
      resident_id: '@residentId',
      affiliation_id: '@affiliationId',
    };

    return {
      restrict: 'EA',
      scope: scope,
      controller: controllerFn,
      template: templateFn
    };

    function controllerFn($scope, $http, Restangular, flash) {
      angular.extend($scope, {
        isLoading: false,
        position: [],
        $update: $update
      });

      function $update(data) {
        $scope.isLoading = true;

        data.position_id = data.position.id;

        return $http
          .put('api/v1/affiliations/' + $scope.affiliation_id + '/residents/' + $scope.resident_id, data)
          .then(function() { flash.success = 'Resident\'s status has been updated'; })
          .catch(error)
          .finally(function() { $scope.isLoading = false });

        function error(response) {
          flash.to('validation').error = _
            .chain(response.data.errors)
            .values()
            .flatten()
            .value();
        }
      }
    }

    function templateFn() {
      return [
        '<label>',
          '<input type="checkbox" ',
            'style="margin-right: 5px; margin-top: 2px;" ',
            'ng-disabled="isLoading" ',
            'ng-true-value="\'active\'" ',
            'ng-false-value="null" ',
            'ng-model="data.status" ',
            'ng-change="$update(data)">',
            'Active',
        '</label>'
      ].join('');
    }
  }
})(angular);