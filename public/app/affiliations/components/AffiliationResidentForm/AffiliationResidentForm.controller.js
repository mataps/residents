+(function(angular, _, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('GCAffiliationResidentFormController', AffiliationResidentFormController)

  function AffiliationResidentFormController($scope, flash, $http) {
    var vm = this;

    // collection, btnPlaceholder, inputPlaceholder
    angular.extend(vm, {
      head: {},
      resident: {},
      referrer: {},
      active: true,
      isLoading: false,
      $add: $add,
      $successHandler: $successHandler,
      $errorHandler: $errorHandler
    });

    /**
     * Add a resident to the collection
     * @param {Object} data [Resident]
     */
    function $add() {
      var data = {};
      data.affiliation_id = vm.affiliationId;
      data.founder = false;
      data.status = 'active';
      data.resident = vm.resident;
      data.resident_name = vm.resident_name;
      data.resident_id = vm.resident_id;
      data.referrer = vm.referrer;
      data.referrer_name = vm.referrer_name;
      data.referrer_id = vm.referrer_id;
      data.head = vm.head;
      data.head_name = vm.head_name;
      data.head_id = vm.head_id;
      data.position_id = vm.position_id;

      vm.isLoading = true;

      var inCollection  = _.chain(vm.collection)
        .pluck('full_name')
        .value()
        .indexOf(data.resident_name) !== -1;

      // Check if data is undefined
      // or check the length of the object, to check
      // if it is an empty object
      if ( angular.isUndefined(data) && !_.size(data) && inCollection ) {
        flash.error = 'An error has occured. It is possible that the resident is already in the affiliation';
        return;
      }

      $http
        .post('/api/v1/affiliations/' + vm.affiliationId + '/residents', data)
        .then(function(response) {
          vm.collection.splice(0, 0, angular.copy(data));
          $successHandler(response);
          // Undefine id and name
          vm.resident = {};
          vm.resident_name = '';
          vm.resident_id = undefined;

          vm.referrer = {};
          vm.referrer_name = '';
          vm.referrer_id = undefined;

          vm.head = {};
          vm.head_name = '';
          vm.head_id = undefined;
        })
        .catch(function(response) {
          $errorHandler(response);
        })
        .finally(function(result) {
          vm.isLoading = false;
        });
    }

    /**
     * Success handler
     */
    function $successHandler(response) {
      flash.success = 'Resident has been successfully added to the affiliation';
    }

    /**
     * [$errorHandler description]
     * @param  {[type]} response [description]
     * @return {[type]}          [description]
     */
    function $errorHandler(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }

  }
})(angular, _);