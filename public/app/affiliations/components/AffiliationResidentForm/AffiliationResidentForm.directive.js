+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .directive('gcAffiliationResidentForm', affiliationResidentForm)

  function affiliationResidentForm() {
    /** */
    var scope = {
      collection: '=',
      affiliationId: '@gcAffiliationResidentForm'
    };

    return {
      restrict: 'EA',
      scope: scope,
      templateUrl: 'gc-affiliation-resident-form.html',
      replace: true,
      bindToController: true,
      controllerAs: 'formCtrl',
      controller: 'GCAffiliationResidentFormController'
    };

    // Todo:
    // placeholders
    function linkFn() {

    }
  }
})(angular);