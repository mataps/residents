+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .run(runBlock);

  function runBlock($templateCache) {
    var template = [
      '<aside class="form-group">',
        '<div class="form-group">',
          '<label>Resident</label>',
          '<gc-resident-typeahead ',
            'id-variable="formCtrl.resident_id" ',
            'collection-variable="formCtrl.resident" ',
            'ng-model="formCtrl.resident_name">',
          '</gc-resident-typeahead>',
        '</div>',

        '<div class="form-group">',
          '<label>Referrer (Optional)</label>',
          '<gc-resident-typeahead ',
            'placeholder="Enter referrer\'s name.." ',
            'id-variable="formCtrl.referrer_id" ',
            'collection-variable="formCtrl.referrer" ',
            'ng-model="formCtrl.referrer_name">',
          '</gc-resident-typeahead>',
        '</div>',

        '<div class="form-group">',
          '<label>Head (Optional)</label>',
          '<gc-resident-typeahead ',
            'placeholder="Enter head\'s name.." ',
            'id-variable="formCtrl.head_id" ',
            'collection-variable="formCtrl.head" ',
            'ng-model="formCtrl.head_name">',
          '</gc-resident-typeahead>',
        '</div>',

        '<div class="row form-group">',
          '<div class="col-md-8">',
            '<label>Position</label>',
            '<gc-affiliation-position-dropdown ',
              'affiliation-id="{{ formCtrl.affiliationId }}" ',
              'data="formCtrl">',
            '</gc-affiliation-position-dropdown>',
          '</div>',

          '<div class="col-md-4">',
            '<label>&nbsp;</label>',
            '<button type="button" class="btn btn-primary form-control" ',
              'ng-click="formCtrl.$add()" ',
              'ng-disabled="formCtrl.isLoading">',
              '<i class="ion-plus"></i>',
              'Add',
            '</button>',
          '</div>',
        '</div>',
      '</aside>'
    ].join('');

    $templateCache.put(
      'gc-affiliation-resident-form.html',
      template
    );
  }
})(angular);
