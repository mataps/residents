+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('GCAffiliationResidentTableController', AffiliationResidentTableController);

  function AffiliationResidentTableController($scope, AFFILIATION_RESIDENTS_COLUMNS, Restangular, $http, flash, UtilService) {
    var vm = this;

    angular.extend(vm, {
      columns: AFFILIATION_RESIDENTS_COLUMNS,
      query: {},
      isLoading: false,
      isError: false,
      $request: $request,
      $filter: $filter,
      $remove: $remove,
      $call: $call,
      $updateResidentSuccess: $updateResidentSuccess,
      $updateResidentError: $updateResidentError
    });

    /**
     * Remove provided position in the collection
     * @param  {int} index [index in the collection]
     */
    function $remove(index) {
      var id = vm.affiliationId;
      var x = vm.collection[index];

      if ( angular.isUndefined(x) ) {
        flash.error = 'An error has occured. It is possible that the resident had already been kicked out of the affiliation.';
        return;
      }

      return $http
        .delete('api/v1/residents/' + (x.resident_id || x.resident.id) + '/affiliations/' + id)
        .then(function(response) {
          flash.success = 'Resident as been successfully removed from the affiliation!';
          vm.selected = undefined;
          vm.collection.splice(index, 1);
        })
        .catch(function(response) {
          flash.to('validation').error = _.chain(response.data.errors)
            .values()
            .flatten()
            .value();
        });
    }

    /**
     * Filter list
     */
    function $filter(type) {
      switch( (type || '').toLowerCase() ) {
        case 'founder':
          return vm.collection.filter(function(resident) {
            return !!parseInt(resident.founder, 10) || resident.founder || parseInt(resident.founder, 10) == 1;
          });

        case 'member':
          return vm.collection;

        default:
          return vm.collection.filter(function(resident) {
            // return (resident.position || '').toLowerCase() === type.toLowerCase() ;
            // return resident;
            return true;
          });
      }
    }

    /**
     *
     */
    function $request(q) {
      vm.isLoading = true;

      var id = vm.affiliationId;
      var next = vm.collection.paging.next;
      var params = angular.extend({}, next == undefined || q !== undefined ? {} : UtilService.decodeQuery(next), vm.query);

      return $http.get('/api/v1/affiliations/' + id + '/residents' + o2q(params))
        .then(function(r) {
          vm.isError = false;
          if ( angular.isUndefined(r.data.paging.previous) ) {
            vm.collection = r.data.data;
            vm.collection.paging = r.data.paging;
          } else {
            r.data.data.forEach(function(_r) {
              vm.collection.push(_r);
            });
            vm.collection.paging = r.data.paging;
          }
        })
        .catch(function() { vm.isError = true; })
        .finally(function() { vm.isLoading = false; });
    }

    function $call(tableState) {
      getTableQuery(tableState);
      $request(true);
    }

    function getTableQuery(tableState) {
      var query = {};
      vm.query = query;

      if (tableState.sort.predicate) {
        query.sort = tableState.sort.reverse ? '' : '-';
        query.sort += tableState.sort.predicate;
      }

      if (tableState.search.predicateObject) {
        query.filters = {};
        angular.forEach(tableState.search.predicateObject, function(value, key) {
            this[key] = value;
        }, query.filters);
      }

      return query;
    }

    /**
     * @name object-to-query
     * Translates a JavaScript object to a
     * URL string query (?x=&y=)
     *
     * @param {Object} query Object to be translated
     * @param {boolean} prepend If `?` should be prepended for the first key. Defaults to true
     *
     * @example
     * o2q({ x: '1': y: '2'}, true); // => ?x=1&y=2
     */
    function o2q(query) {
      // This function does not accept primitives and functions
      if (typeof query !== 'object') {
        throw new Error('Provided query must be an object');
      }

      var result = ''; // Our resulting string
      var keys = Object.keys(query);

      keys.forEach(function(key, i) {
        var value = JSON.stringify(query[key]); // Just our stringified JSON

        // We check if should prepend `?` for the first key.
        // The following is prepended by `&`
        var _prepend = (i > 0 ? '&' : '');

        result += _prepend + key + '=' + value;
      });

      return '?' + result;
    }

    function $updateResidentSuccess() {
      flash.success = 'Resident role has been successfully updated!';
    }

    function $updateResidentError() {
      flash.success = 'Resident role has been successfully updated!';
    }
  }
})(angular);