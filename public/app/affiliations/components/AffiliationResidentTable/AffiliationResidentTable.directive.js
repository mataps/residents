+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .directive('gcAffiliationResidentTable', affiliationResidentTable);

  function affiliationResidentTable() {
    return {
      scope: {
        collection: '=',
        selected: '=',
        filter: '=',
        affiliationId: '@gcAffiliationResidentTable',
        positions: '=',
        fullscreen: '='
      },
      restrict: 'EA',
      templateUrl: 'gc-affiliation-resident-table.html',
      bindToController: true,
      controllerAs: 'tableCtrl',
      controller: 'GCAffiliationResidentTableController'
    };
  }
})(angular);