+(function(angular, _, undefined) {
  "use strict";
  angular
    .module('app')
    .directive('gcAffiliationSelectedHeadInput', directive);

  function directive() {
    return {
      scope: {
        data: '=',
        affiliationId: '@'
      },

      template: templateFn,
      controller: controllerFn
    };

    function templateFn() {
      return [
        '<gc-resident-typeahead ',
          'id-variable="data.head_id" ',
          'collection-variable="data.head" ',
          'ng-model="data.head_name">',
        '</gc-resident-typeahead>'
      ].join('');
    }

    function controllerFn($scope, $http, flash) {
      // console.log($scope.data);
      //
      // $scope.$watch(function() {
      //   return $scope.data;
      // }, function(changed, old) {
      //   if ( changed == old ) return;
      //
      //   console.log(changed);
      //   $scope.data.head_id = changed.head.id;
      //   $scope.data.head_name = changed.head.full_name;
      // })

      $scope.$watch('data', function(changed, old) {
        if ( changed == undefined || old == undefined || changed.head_id == undefined || !(changed.head_id + '').length ) return;
        if ( changed.head_id == old.head_id ) return;

        var aid = $scope.affiliationId;
        var rid = changed.head_id;
        var data = $scope.data;
        return $http.put('/api/v1/affiliations/' + aid + '/residents/' + rid, data)
          .then(function() {
            flash.success = 'The resident was successfully assigned as a head';

            $scope.data.head.id = $scope.data.head_id;
            $scope.data.head.full_name = $scope.data.head_name;
          })
          .catch(function(res) {
            flash.to('validation').error = _.chain(res.data.errors)
              .values()
              .flatten()
              .value();
          });
      }, true);
    }
  }
})(angular, _);
