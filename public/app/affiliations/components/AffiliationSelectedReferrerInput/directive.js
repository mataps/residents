+(function(angular, _, undefined) {
  "use strict";
  angular
    .module('app')
    .directive('gcAffiliationSelectedReferrerInput', directive);

  function directive() {
    return {
      scope: {
        data: '=',
        affiliationId: '@'
      },

      template: templateFn,
      controller: controllerFn
    };

    function templateFn() {
      return [
        '<gc-resident-typeahead ',
          'id-variable="data.referrer_id" ',
          'collection-variable="data.referrer" ',
          'ng-model="data.referrer_name">',
        '</gc-resident-typeahead>'
      ].join('');
    }

    function controllerFn($scope, $http, flash) {
      // console.log($scope.data);
      //
      // $scope.$watch(function() {
      //   return $scope.data;
      // }, function(changed, old) {
      //   if ( changed == old ) return;
      //
      //   console.log(changed);
      //   $scope.data.referrer_id = changed.referrer.id;
      //   $scope.data.referrer_name = changed.referrer.full_name;
      // })

      $scope.$watch('data', function(changed, old) {
        if ( changed == undefined || old == undefined || changed.referrer_id == undefined || !(changed.referrer_id + '').length ) return;
        if ( changed.referrer_id == old.referrer_id ) return;

        var aid = $scope.affiliationId;
        var rid = changed.referrer_id;
        var data = $scope.data;
        return $http.put('/api/v1/affiliations/' + aid + '/residents/' + rid, data)
          .then(function() {
            flash.success = 'The resident was successfully assigned as a referrer';

            $scope.data.referrer.id = $scope.data.referrer_id;
            $scope.data.referrer.full_name = $scope.data.referrer_name;
          })
          .catch(function(res) {
            flash.to('validation').error = _.chain(res.data.errors)
              .values()
              .flatten()
              .value();
          });
      }, true);
    }
  }
})(angular, _);
