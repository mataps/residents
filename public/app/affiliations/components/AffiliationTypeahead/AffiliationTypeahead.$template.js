+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .run(runBlock);

  function runBlock($templateCache) {
    var template = [
      '<a>',
        '<div class="row">',
          '<div class="col-md-2" style="padding: 0px;">',
            '<img ng-src="{{match.model.profilePic}}" width="50px" height="50px" ng-hide="!match.model.profilePic">',
            '<img class="pull-left" src="holder.js/50x50" holder-fix ng-show= "!match.model.profilePic">',
          '</div>',

          '<div class="col-md-9">',
            '<span bind-html-unsafe="match.model.name | typeaheadHighlight:query">',
            '</span><br>',
            '<span style="font-size: 9px; font-style: italic;">',
              '{{ match.model.cityMunicipality }}',
            '</span>',
          '</div>',
        '</div>',
      '</a>'
    ].join('');

    $templateCache.put(
      'gc-affiliation-typeahead-$template.html',
      template
    );
  }
})(angular);