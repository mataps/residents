+(function(angular, undefined) {
  angular
    .module('app')
    .controller(
      'GCAffiliationTypeaheadController',
      AffiliationTypeaheadController
    );
    
  function AffiliationTypeaheadController($scope, Affiliation) {
    //
    var vm = this;

    transform();

    vm.request = request;
    vm.selectCb = selectCb;

    /**
     * [Affiliation, description]
     * @return {[type]} [description]
     */
    function request(name) {
      return Affiliation
        .Query
        .typeahead(name)
        .then(function(response) {
          return response.data;
        });
    }

    function selectCb($item, $model, $label) {
      if ( !angular.isUndefined(vm.idVariable) ) {
        vm.idVariable = $item.id;
        console.log(vm.idVariable);
      }

      if ( !angular.isUndefined(vm.collectionVariable) ) {
        vm.collectionVariable = $item;
      }
    }

    /** Transform data */
    function transform() {
      if ( !angular.isUndefined(vm.transformIdVariable) ) {
        vm.idVariable = vm.transformIdVariable;
      }

      if ( !angular.isUndefined(vm.transformCollectionVariable) ) {
        vm.collectionVariable = angular.copy(vm.transformCollectionVariable);
      }
    }
  }
})(angular);