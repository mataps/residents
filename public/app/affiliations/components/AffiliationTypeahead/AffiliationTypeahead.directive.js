+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .directive('gcAffiliationTypeahead', AffiliationTypeahead);

  function AffiliationTypeahead($compile) {
    var scope = {
      idVariable: '=',
      collectionVariable: '=',
      transformIdVariable: '@',
      transformCollectionVariable: '=',
      model: '=ngModel',
      placeholder: '@inputPlaceholder'
    };

    return {
      scope: scope,
      link: linkFn,
      template: templateFn,
      bindToController: true,
      controllerAs: 'typeahead',
      controller: 'GCAffiliationTypeaheadController',
      replace: true
    };

    function templateFn(e, attrs) {
      var placeholder = attrs.placeholder || 'Enter affiliation\'s name..';

      return [
        '<div>',
          '<input type="text" class="form-control" ',
          'placeholder="', placeholder, '" ',
          'ng-model="typeahead.model" ',
          'typeahead="affiliation.name for affiliation ',
            'in typeahead.request($viewValue)" ',
          'typeahead-wait-ms="50" ',
          'typeahead-template-url="gc-affiliation-typeahead-$template.html" ',
          'typeahead-on-select="typeahead.selectCb($item, $model, $label)">',
        '</div>'
      ].join('');
    }

    // TODO
    function linkFn(scope, element, attributes, controller) {
      // If the directive was assigned with "placeholder",
      // modify the default
      if ( !angular.isUndefined(controller.placeholder) ) {
        // element.removeAttr('placeholder');
        // element.removeAttr('gcAffiliationTypeahead');
        // attributes.$set('placeholder', controller.placeholder);
        // $compile(element)(scope);
      }
    }
  }
})(angular);