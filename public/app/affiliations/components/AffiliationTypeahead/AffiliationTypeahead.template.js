+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .run(runBlock);

  function runBlock($templateCache) {
    var template = [
      '<div>',
        '<input type="text" class="form-control" ',
        'placeholder="Enter affiliation.." ',
        'ng-model="typeahead.model" ',
        'typeahead="affiliation.name for affiliation ',
          'in typeahead.request($viewValue)" ',
        'typeahead-wait-ms="50" ',
        'typeahead-template-url="gc-affiliation-typeahead-$template.html" ',
        'typeahead-on-select="typeahead.selectCb($item, $model, $label)">',
      '</div>'
    ].join('');

    $templateCache.put(
      'gc-affiliation-typeahead.html',
      template
    );
  }
})(angular);