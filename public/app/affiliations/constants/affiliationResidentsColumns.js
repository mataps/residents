+(function(angular, undefined) {
  var columns = [
    {
      key: 'resident.avatar',
      label: '',
      show: true,
      cellTemplate: [
        '<img ng-src="{{ data.profilePic }}" ',
          'ng-if="!!selectedCtrl.model.profilePic" ',
          'fallback-src="fallback.gif" ',
          'holder-fix class="pull-left" ',
          'style="border-radius: 50%; margin-top: 10px;" height="36" width="36">',

        '<img src="holder.js/36x36" ',
          'ng-if="!data.profilePic" ',
          'holder-fix class="pull-left" ',
          'style="border-radius: 50%; background: #ddd; margin-top: 10px;" height="36" width="36">'
      ].join('')
    },
    {
      key: 'resident.first_name',
      label: 'First Name',
      show: true,
      cellTemplate: '{{ data.first_name }}'
    },
    {
      key: 'resident.middle_name',
      label: 'Middle Name',
      show: true,
      cellTemplate: '{{ data.middle_name }}'
    },
    {
      key: 'resident.last_name',
      label: 'Last Name',
      show: true,
      cellTemplate: '{{ data.last_name }}'
    },
    {
      key: 'resident.is_voter',
      label: 'Voter',
      show: true,
      cellTemplate: [
        '<span ng-if="!!data.is_voter" class="label label-success"><i class="ion-checkmark-circled"></i></span>',
        '<span ng-if="!data.is_voter" class="label label-warning"><i class="ion-minus-circled"></i></span>'
      ].join('')
    },
    {
      key: 'resident.is_scholar',
      label: 'Scholar',
      show: true,
      cellTemplate: [
        '<span ng-if="!!data.is_scholar" class="label label-success">',
          '<i class="ion-checkmark-circled"></i> S',
        '</span>'
      ].join('')
    },
    {
      key : 'resident.deceased_at',
      label: 'Deceased',
      show : true,
      cellTemplate: '<span ng-if="!!data.deceased_at" class="label label-danger" title="Deceased on {{:: data.deceased_at | date:\'MMM d, yyyy\'}}">D</span>'
    },
    {
      key : 'resident.blacklisted_at',
      label : 'Blacklisted',
      show : true,
      cellTemplate: '<span ng-if="!!data.blacklisted_at" class="label label-default" title="Deceased on {{:: data.blacklisted_at | date:\'MMM d, yyyy\'}}">B</span>'
    },
    {
      key: 'resident.gender',
      label: 'Gender',
      show: true,
      cellTemplate: '{{:: data.gender }}'
    },
    {
      key: 'resident.civil_status',
      label: 'Civil Status',
      show: true,
      cellTemplate: '{{:: data.civil_status }}'
    },
    {
      key: 'resident.email',
      label: 'Email',
      show: true,
      cellTemplate: '<span gc-mail-to="{{ data.email }}"></span>'
    },
    {
      key: 'resident.nickname',
      label: 'Nickname',
      show: true,
      cellTemplate: '{{ data.nickname }}'
    },
    {
      key: 'resident.occupation',
      label: 'Occupation',
      show: true,
      cellTemplate: '{{ data.occupation }}'
    },
    {
      key: 'resident.mobile',
      label: 'Mobile',
      show: true,
      cellTemplate: '{{:: data.mobile }}'
    },
    {
      key: 'resident.phone',
      label: 'Phone',
      show: true,
      cellTemplate: '{{:: data.phone }}'
    },
    {
      key: 'resident.precinct',
      label: 'Precinct',
      show: true,
      cellTemplate: '{{:: data.precinct }}'
    },
    {
      key: 'resident.birthdate',
      label: 'Birthdate',
      show: true,
      cellTemplate: '{{:: data.birthdate | date: \'MMM d, yyyy\' }}',
      field: 'birthdate.from',
      field2: 'birthdate.to'
    },
    {
      key: 'barangay.name',
      label: 'Barangay',
      show: true,
      cellTemplate: '{{ data.barangay.name }}'
    },
    {
      key: 'resident.district.name',
      label: 'District',
      show: true,
      cellTemplate: '{{:: data.district.name }}'
    },
    {
      key: 'resident.street',
      label: 'Street',
      show: true,
      cellTemplate: '{{ data.street }}'
    },
    {
      key: 'cityMunicipality.name',
      label: 'City Municipality',
      show: true,
      cellTemplate: '{{ data.cityMunicipality.name }}'
    },
    {
      key: 'position.name',
      label: 'Position',
      show: true,
      cellTemplate: [
        '<span ng-if="!data._isEditing">{{ data.position.name | studlyCase }}</span>',
        '<gc-affiliation-position-dropdown-breaking-change-lord ',
          'affiliation-id="{{ tableCtrl.affiliationId }}" ',
          'data="data" ',
          'request="true" ',
          'ng-if="data._isEditing" ',
          'ng-init="data.position_id = data.position.id">',
        '</gc-affiliation-position-dropdown-breaking-change-lord>'
      ].join('')
    },
    {
      key: 'founder',
      label: 'Founder',
      show: true,
      cellTemplate: [
        '<i class="ion-checkmark-circled" style="color: green;" ng-if="(data.founder == 1 || data.founder == \'1\' || data.founder == true) && !data._isEditing"></i>',
        '<div ng-if="data._isEditing">',
          '<button type="button"',
            'ng-click="data.founder = \'1\'"',
            'ng-show="data.founder == \'0\' || data.founder == 0"',
            'class="btn btn-danget btn-sm pull-left"',
            'srph-xhr="affiliations/{{tableCtrl.affiliationId}}/residents/{{data.id}}"',
            'request-type="put"',
            'request-data="data"',
            'request-success="tableCtrl.$updateResidentSuccess"',
            'request-error="tableCtrl.$updateResidentError">',
            'Set as Founder',
          '</button>',
          '<button type="button"',
            'ng-click="data.founder = \'0\'"',
            'ng-show="data.founder == \'1\' || data.founder == 1"',
            'class="btn btn-danget btn-sm pull-left"',
            'srph-xhr="affiliations/{{tableCtrl.affiliationId}}/residents/{{data.id}}"',
            'request-headers="Access-Control-Allow-Origin"',
            'request-type="put"',
            'request-data="data"',
            'request-success="tableCtrl.$updateResidentSuccess"',
            'request-error="tableCtrl.$updateResidentError">',
            'Remove as Founder',
          '</button>',
        '</div>'
      ].join('')
    },
    {
      key: 'status',
      label: 'Active',
      show: true,
      cellTemplate: [
        '<i class="ion-checkmark-circled" style="color: green;" ng-if="data.status == \'active\' && !data._isEditing"></i>',
        '<gc-affiliation-resident-active ',
          'data="data" ',
          'resident-id="{{ data.id }}" ',
          'affiliation-id="{{ tableCtrl.affiliationId }}" ',
          'ng-if="data._isEditing">',
        '</gc-affiliation-resident-active>'
      ].join('')
    },
    {
      key: 'head.full_name',
      label: 'Head',
      show: true,
      cellTemplate: [
        '<span ng-if="!data._isEditing">{{ data.head.full_name }}</span>',
        '<gc-affiliation-selected-head-input ',
          'data="data" ',
          'affiliation-id="{{ tableCtrl.affiliationId }}" ',
          'ng-if="data._isEditing">',
        '</gc-affiliation-selected-head-input>'
      ].join('')
    },
    {
      key: 'referrer.full_name',
      label: 'Referrer',
      show: true,
      cellTemplate: [
        '<span ng-if="!data._isEditing">{{ data.referrer.full_name }}</span>',
        '<gc-affiliation-selected-referrer-input ',
          'data="data" ',
          'affiliation-id="{{ tableCtrl.affiliationId }}" ',
          'ng-if="data._isEditing">',
        '</gc-affiliation-selected-referrer-input>'
      ].join('')
    },
    {
      key: 'options',
      label: '',
      show: true,
      cellTemplate: [
      '<div ng-if="!data._isEditing">',
        '<button type="button" ',
          'tooltip="Edit affiliation details of this resident" ',
          'class="btn btn-info btn-xs" ',
          'ng-click="data._isEditing = true">',
          '<i class="ion-edit"></i>',
        '</button>',
        '<button type="button" ',
          'tooltip="Kick the resident from the affiliation" ',
          'class="btn btn-danger btn-xs" ',
          'ng-click="tableCtrl.$remove($parent.$index)">',
          '<i class="ion-close"></i>',
        '</button>',
      '</div>',
      '<div ng-if="data._isEditing">',
        '<button type="button" ',
          'tooltip="Click to exit edit mode of this resident" ',
          'class="btn btn-info btn-xs" ',
          'ng-click="data._isEditing = false">',
          '<i class="ion-checkmark-circled"></i>',
        '</button>',
      '</div>'
      ].join('')
    }
  ];

  angular
    .module('app')
    .constant('AFFILIATION_RESIDENTS_COLUMNS', columns);
})(angular);