+(function(angular, undefined) {
  var columns = [
    {
      key: 'name',
      label: 'Name',
      show: true,
      cellTemplate: '{{:: affiliation.name }}',
      onClick: 'affiliations.show(affiliation.id)'
    },
    {
      key: 'description',
      label: 'Description',
      show: true,
      cellTemplate: '{{:: affiliation.description }}',
      onClick: 'affiliations.show(affiliation.id)'
    },
  ];

  angular
    .module('app')
    .constant('AFFILIATIONS_COLUMNS', columns);
})(angular);