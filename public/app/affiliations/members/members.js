+(function(angular, _, undefined) {
  "use strict";
  angular
    .module('app')
    .controller('AffiliationMemberController', AffiliationMemberController);

  function AffiliationMemberController($scope, $state, $window, $modal, Transaction, Restangular, flash, $http) {
    var vm = this;
    vm._selected_position = [];
    angular.extend(vm, {
      data: {
        affiliation: {},
        affiliation_id: '',
        referrer: {},
        referrer_id: '',
        head: {},
        head_id: ''
      },
      add_member_toggle: 0,
      add_member: {}
    });

    // ------------------------- //
    // ------------------------- //

    vm.cities = function($value) {
      return $http.get('/api/v1/citiesmunicipalities?filters=' + JSON.stringify({ name: $value }))
        .then(function(response) {
          return response.data.data;
        });
    };
    
    vm.barangays = function($value) {
      return $http.get('/api/v1/barangays?filters=' + JSON.stringify({ name: $value }))
        .then(function(response) {
          return response.data.data;
        });
    };

    vm.$itemRequest = function($value) {
      return $http.get('/api/v1/transactions/items/?filters=' + JSON.stringify({ name: $value }))
        .then(function(response) {
          return response.data.data;
        });
    };

    vm.$addMemberToggle = function(){
      vm.add_member_toggle = !vm.add_member_toggle;
    };

    vm.$addMember = function(member) {
      var new_member = member;
      vm.data.members.push(new_member);
      vm.add_member = {};
    };

    vm.$removeMember = function(member) {
      var index = vm.data.members.indexOf(member);
      vm.data.members.splice(index, 1);
    };

    vm.positions = function($viewValue) {
      return $http.get('/api/v1/positions/?filters=' + JSON.stringify({ name: $viewValue }))
        .then(function(res) {
          return res.data.data;
        });
    };

    vm.onQueryChange = function(){
      vm.position = [];
      for(var x = 0; x < vm._selected_position.length; x++){
        vm.position.push(vm._selected_position[x].text);   
      }
    }

    vm.pdf = function(data) {
         vm.position = [];
      for(var x = 0; x < vm._selected_position.length; x++){
        var search = new RegExp('-', 'g');
        var item = vm._selected_position[x].text.replace(search, " ");
        vm.position.push(item);   
      }
      if(data.disable_page_break == undefined){
        data.disable_page_break = false;
      } 
        return  '/api/v1/affiliations/' + data.affiliation_id +
        '/cityMunicipality/' + data.cityMunicipality_id +
        '/print/pdf?disable_page_break='+ data.disable_page_break +'&filters=' +
        JSON.stringify(angular.extend(
          vm.position.length == 0 ? {} : { 'position.name': { 'equal' : vm.position }},
          data.head_name == '' || data.head_name == undefined || data.head_id == '' ? {} : { 'head.id': data.head_id },
          data.referrer_name == '' || data.referrer_name == undefined || data.referrer_id == '' ? {} : { 'referrer.id': data.referrer_id },
          data.barangay_name == '' || data.barangay_name == undefined || data.barangay_id == '' ? {} : { 'barangay.id': data.barangay_id }
        ));
    };

    vm.excel = function(data){
      vm.position = [];
      for(var x = 0; x < vm._selected_position.length; x++){
        var search = new RegExp('-', 'g');
        var item = vm._selected_position[x].text.replace(search, " ");
        vm.position.push(item);   
      }
        return  '/api/v1/affiliations/' + data.affiliation_id +
        '/cityMunicipality/' + data.cityMunicipality_id +
        '/print/pdf?filters=' +
        JSON.stringify(angular.extend(
          { 'position.name': { 'equal' : vm.position }},
          data.head_name == '' || data.head_name == undefined || data.head_id == '' ? {} : { 'head.id': data.head_id },
          data.referrer_name == '' || data.referrer_name == undefined || data.referrer_id == '' ? {} : { 'referrer.id': data.referrer_id },
          data.barangay_name == '' || data.barangay_name == undefined || data.barangay_id == '' ? {} : { 'barangay.id': data.barangay_id }
        ));
    }
  }
})(angular, _);
