+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('AffiliationReportController', AffiliationReportController);

  function AffiliationReportController($scope, $http, USER, $filter, UtilService, filterResolve, affiliationIdResolve) {
    $scope.data = {};

    /**
     *
     */
    $scope.$report = function(type) {
      var position = $scope.data.position_id;
      return '/api/v1/affiliations/' +
        affiliationIdResolve +
        '/cityMunicipality/' +
        $scope.data.cityMunicipality_id +
        '?filters=' +
        JSON.stringify(
          angular.extend({}, position == undefined ? {} : { 'position.id': position }, filterResolve)
        );
    }

    /**
     *
     */
    function o2q(query) {
      // This function does not accept primitives and functions
      if (query == null || typeof query !== 'object') {
        throw new Error('Provided query must be an object');
      }

      var result = ''; // Our resulting string
      var keys = Object.keys(query);

      // Using native loops instead of `map` or `forEach`
      // http://jsperf.com/native-map-versus-array-looping
      // Now we'll iterate through the splitted keyz
      for ( var i = 0; i < keys.length; i++ ) {
        var key = keys[i];
        var _value = query[key]; // Just our stringified JSON

        // We'll check first if the value is null or undefined. We'll
        // have to just go to the next key since we do not want or
        // need to add it to the resulting qs.
        if ( _value === undefined || _value === null ) continue;

        var value = encodeURIComponent(_value);

        // Prepends the ampsersand separator `&` for
        // each key except the first key
        var _prepend = (i > 0 && !!result.length ? '&' : '');

        result += _prepend + key + '=' + value;
      }

      return '?' + result;
    }

    $scope.citiesMunicipalities = function($viewValue) {
      return $http.get('/api/v1/citiesmunicipalities?limit=69696969696969696969')
        .then(function(response) {
          return response.data.data;
        });
    }
  }
})(angular);