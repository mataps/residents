+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('AttributionsCreateCtrl', AttributionsCreateCtrl);

  function AttributionsCreateCtrl($scope, flash, $state, Restangular) {
    var vm = this;

    angular.extend(vm, {
      isLoading: false,
      custom: {},
      data: {},
      $create: $create,
      $errorHandler: $errorHandler,
      $successHandler: $successHandler
    });

    function $create(data) {
      vm.isLoading = true;
      
      return Restangular
        .service('attributions')
        .post(data)
        .then($successHandler)
        .catch($errorHandler)
        .finally(function() { vm.isLoading = false; });
    }

    /**
     * Success response
     */
    function $successHandler(response) {
      flash.success = 'Attribution has been successfully created!';

      $state.go('attributions', null, { reload: true });
    }

    /**
     * Error response
     */
    function $errorHandler(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }
  }
})(angular);