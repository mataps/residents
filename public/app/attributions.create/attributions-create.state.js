+(function(angular) {
  'use strict';

  angular
    .module('app')
    .config(config);

  function config($stateProvider) {

    var modal;

    /** Trigger modal */
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('attributions');
      }
        
      modal = $modal
        .open({
          size: 'sm',
          controller: 'AttributionsCreateCtrl as createCtrl',
          templateUrl: '/app/attributions.create/attributions-create.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }
    

    var state = {
      url: '/create',
      name: 'attributions.create',
      data: { pageTitle: 'New attribution' },
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }
})(angular);