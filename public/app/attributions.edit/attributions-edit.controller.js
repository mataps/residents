+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('AttributionsEditCtrl', AttributionsEditCtrl);

  function AttributionsEditCtrl($scope, Household, Restangular, flash, UtilService, $state, $http, $stateParams) {
    var vm = this;

    angular.extend(vm, {
      isLoading: true,

      $update: $update,
      $errorHandler: $errorHandler,
      $successHandler: $successHandler
    });

    init();

    function $update(data) {
      vm.isLoading = true;
      
      return $http
        .put('api/v1/attributions/' + $stateParams.id, data)
        .then($successHandler)
        .catch($errorHandler)
        .finally(function() { vm.isLoading = false; });
    }

    function $successHandler(response) {
      vm.isLoading = true;

      flash.success = 'Attributions has been updated!';

      $state.go('attributions', null, { reload: true });
    }

    function $errorHandler(response) {
      vm.isLoading = false;

      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }

    function init() {
      return Restangular
        .one('attributions', $stateParams.id)
        .get()
        .then(function(response) { vm.data = response.data;  })
        .catch(function(response) { $scope.$close(); })
        .finally(function() { vm.isLoading = false; });
    }
  }
})(angular);