+(function(angular) {
  'use strict';

  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var modal;
    /** Trigger modal */
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('attributions');
      }
        
      modal = $modal
        .open({
          size: 'sm',
          controller: 'AttributionsEditCtrl as editCtrl',
          templateUrl: '/app/attributions.edit/attributions-edit.html',
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }    

    var state = {
      url: '/{id}/edit',
      name: 'attributions.edit',
      data: { pageTitle: 'Edit attribution' },
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }
})(angular);