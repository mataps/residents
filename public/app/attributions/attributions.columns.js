+(function(angular, undefined) {
  var columns = [
    {
      key: 'name',
      label: 'Name',
      show: true,
      cellTemplate: '{{ attribution.name }}',
      onClick: 'attributions.show(attribution.id)'
    },
    {
      key: 'label',
      label: 'Label',
      show: true,
      cellTemplate: '{{ attribution.label }}',
      onClick: 'attributions.show(attribution.id)'
    }
  ];

  angular
    .module('app')
    .constant('ATTRIBUTIONS_COLUMNS', columns);
})(angular);