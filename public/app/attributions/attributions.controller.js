+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('AttributionsCtrl', AttributionsCtrl);

  function AttributionsCtrl($scope, $controller, UtilService, ATTRIBUTIONS_COLUMNS, getAttributionsResolve, Restangular, flash, $state) {
    // Inherit BaseIndexCtrl's prototype,
    var controller = $controller('BaseIndexCtrl', {
        $scope: $scope,
        $state: $state,
        UtilService: UtilService
    });

    angular.extend(this, controller);

    // ------------------------- //
    // ------------------------- //
    // 
    // viewmodel
    var vm = this,    
       // Data collection (grabbed from the service)
      collection = [];

    // Copy items from the service
    // to the local collection variable
    angular.extend(collection, getAttributionsResolve.data);

    vm.$state = $state;

    // Initialize vm data
    vm.columns = ATTRIBUTIONS_COLUMNS;
    vm.collection = collection;

    // Manually bind the "this"
    // from the base controller
    vm.callServer = angular.bind(vm, vm.callServer);
    vm.applyPageData = angular.bind(vm, vm.applyPageData);
    vm.catchPageDataError = angular.bind(vm, vm.catchPageDataError);

    // Commands that contact the API / server
    vm.getSelected = angular.bind(vm, vm.getSelected);
    vm.remove = remove;
    vm.getPageData = getPageData;

    /**
     * @description  Sends a DELETE to the server
     * @return Restangular
     */
    function remove() {
      var selected = vm.getSelected().join(',');

      if ( !selected.length ) {
        flash.error = 'Please select an attribution.';
        return;
      }

      if(!confirm('Please confirm deleting the selected records.'))
        return;

      return Restangular
        .service('attributions')
        .one(selected)
        .one('remove')
        .remove()
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    };

    vm.QQ = {};

    /**
     * Fetches data and replaces the current
     * @param  string query
     * @return void
     */
    function getPageData(query) {
      if ( query.page ) {
        // console.log(query);
        // console.log(vm.QQ);
        angular.extend(vm.QQ, query);
        console.log(vm.QQ);
      } else {
        vm.QQ = query;
        console.log(vm.QQ);
      }

      return Restangular
        .service('attributions')
        .getList(vm.QQ)
        .then(vm.applyPageData)
        .catch(vm.catchPageDataError);
    }

    // Success XHR
    function successXHRCallback(response) {
      var message = 'Attribute has been successfully archived!';
      flash.success = message;
      $state.reload();
      vm.load(false);
    }

    // Error XHR
    function errorXHRCallback(response) {
      var message = 'An error has occured. Please try again..';
      flash.to('validation').error = message;
      vm.load(false);
    }
  }
})(angular);