+(function(angular, undefined) {
  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var state = {
      parent: 'main',
      name: 'attributions',
      url: '/attributions',
      templateUrl: 'app/attributions/attributions.html',
      data: { pageTitle: 'Manage Attributes' },
      resolve: { getAttributionsResolve: getAttributionsResolve },
      controllerAs: 'attributions',
      controller: 'AttributionsCtrl'
    };

    $stateProvider.state(state);

    /**
     * XHR to the API's Attribution list
     */
    function getAttributionsResolve(Restangular) {
      return Restangular
        .one('attributions')
        .getList();
    }
  }
})(angular);