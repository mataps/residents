+(function(angular, undefined) {
  angular
    .module('app')
    .controller('DashboardController', DashboardController);

  function DashboardController($scope, Restangular) {


//


    $scope.cities = [];

    Restangular.one('citiesmunicipalities')
      .getList()
      .then(function(response) {
        response.data.forEach(function(val, index) { 
          if ( index < 10 ) {
            $scope.cities.push(val);
          }
        });
      });

    $scope.config = {
      title: 'EDMS',
      tooltips: true,
      labels: false,
      legend: {
        display: true,
        //could be 'left, right'
        position: 'right'
      }
    };

    $scope.data = {
      series: ['Residents', 'Transactions', 'Households', 'Affiliations', 'Users'],
      data: [{
        x: "Residents",
        y: [100, 500, 0],
        tooltip: "6902"
      }, {
        x: "Transactions",
        y: [300, 100, 100]
      }, {
        x: "Households",
        y: [351]
      }, {
        x: "Affiliations",
        y: [54, 0, 879]
      },{
        x: "Users",
        y: [54, 0, 879]
      }]
    };
  }
})(angular);