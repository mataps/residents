+(function(angular) {

  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
        var state = {
            parent: 'main',
            name: 'dashboard',
            url: '/',
            data: { pageTitle: 'Dashboard' },
            templateUrl: '/app/dashboard/dashboard.html',
            controller: 'DashboardController'
        };

        $stateProvider.state(state);
    }
})(angular);