+(function(angular, undefuned) {
  "use strict";
  angular
    .module('app')
    .controller('HistoryController', HistoryController);

  function HistoryController($scope) {
    angular.extend($scope, {
      modules: [
        'residents',
        'households',
        'affiliations',
        'attributions',
        'positions',

        'accounts',
        'categories',
        'sub_categories',
        'transactions',
        'vouchers',
        'liquidations',

        'awards',
        'schools',
        'semesters',
        'grades',
        'allowance_policies',
        'allowances',
        'allowance_releases',
        'scholarship_types',
        'subjects'
      ],
      apis: {
        'residents': 'residents',
        'households': 'households',
        'affiliations': 'affiliations',
        'attributions': 'attributions',
        'positions': 'positions',

        'accounts': 'accounts',
        'categories': 'residents',
        'sub_categories': 'sub_categories',
        'transactions': 'transactions',
        'vouchers': 'vouchers',
        'liquidations': 'liquidations',

        'awards': 'awards',
        'schools': 'schools',
        'semesters': 'semesters',
        'grades': 'grades',
        'allowance_policies': 'allowance_policies',
        'allowances': 'allowances',
        'allowance_releases': 'releases',
        'scholarship_types': 'scholarshiptypes',
        'subjects': 'subjects'
      },
      $moduleTitle: $moduleTitle
    });

    /**
     * Converts the module title to a
     * readable format (ucwords)
     */
    function $moduleTitle(title) {
      return title
        .split('_')
        .map(function(w) { return w.charAt(0).toUpperCase() + w.slice(1); })
        .join(' ');
    }
  }
})(angular);