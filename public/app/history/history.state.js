+(function(angular, undefined) {
  "use strict";
  angular
    .module('app')
    .config(configBlock);

  /**
   * Register main.history state (/history)
   */
  function configBlock($stateProvider) {
    $stateProvider.state({
      parent: 'main',
      name: 'history',
      url: '/history',
      controller: 'HistoryController',
      controllerAs: 'history',
      templateUrl: '/app/history/history.html',
      data: { pageTitle: 'Module History' }
    });
  }
})(angular);