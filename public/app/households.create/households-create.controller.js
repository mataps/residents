+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('HouseholdsCreateCtrl', HouseholdsCreateCtrl);

  function HouseholdsCreateCtrl($scope, flash, $state, Restangular) {
    var vm = this;

    angular.extend(vm, {
      isLoading: false,
      custom: {},
      data: {
        members: [],
        owner: {}
      },

      barangays: barangays,
      districts: districts,
      citiesMunicipalities: citiesMunicipalities,
      $errorHandler: $errorHandler,
      $successHandler: $successHandler
    });

    /**
     * Success response
     */
    function $successHandler(response) {
      vm.isLoading = false;

      flash.success = 'Household has been successfully registered!';

      $state.go('households', null, { reload: true });
    }

    /**
     * Error response
     */
    function $errorHandler(response) {
      vm.isLoading = false;

      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }

    function districts($viewValue) {
      return Restangular.service('districts')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }
    
    function citiesMunicipalities($viewValue) {
      return Restangular.service('citiesmunicipalities')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }


    function barangays($viewValue) {
      return Restangular.service('barangays')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }
  }
})(angular);