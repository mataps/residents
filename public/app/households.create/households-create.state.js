+(function(angular) {
  'use strict';

  angular
    .module('app')
    .config(config);

  function config($stateProvider) {

    var modal;

    /** Trigger modal */
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('households');
      }
        
      modal = $modal
        .open({
          size: 'sm',
          controller: 'HouseholdsCreateCtrl as createCtrl',
          templateUrl: '/app/households.create/households-create.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }
    

    var state = {
      url: '/create',
      name: 'households.create',
      data: { pageTitle: 'New household' },
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }
})(angular);