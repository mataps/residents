+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('HouseholdsEditCtrl', HouseholdsEditCtrl);

  function HouseholdsEditCtrl($scope, Household, Restangular, flash, UtilService, $state) {
    var vm = this,
      form = {
        data: Household.getInstance(),
        residents: [],
        owner: {},
        custom: {}
      };

    init();
    vouchers();

    // ------------------------- //
    vm.form = form;
    vm.isLoading = false;
    vm.isLoadingResidents = true;
    vm.isLoadingVouchers = true;
    vm.successXHRCallback = successXHRCallback;
    vm.errorXHRCallback = errorXHRCallback;
    vm.districts = districts;
    vm.barangays = barangays;
    vm.citiesMunicipalities = citiesMunicipalities;
    vm.vouchers = [];
    // ------------------------- //

    function successXHRCallback(response) {
      vm.isLoading = true;

      flash.success = 'Household has been updated!';

      $state.go('households', null, { reload: true });
    }

    function errorXHRCallback(response) {
      vm.isLoading = false;

      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }

    // Setter for the isLoading prop
    function load(bool) { vm.isLoading = UtilService.tralse(bool); }

    /**
     * Init
     */
    function init() {
      Restangular
        .one('households', data.id)
        .one('residents')
        .getList()
        .then(function(response) { angular.extend(form.residents, response.data); })
        .catch(function(response) { flash.error = 'An error has occured while fetching the residents'; })
        .finally(function() { vm.isLoadingResidents = false; });
    }

    function districts($viewValue) {
      return Restangular.service('districts')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }
    
    function citiesMunicipalities($viewValue) {
      return Restangular.service('citiesmunicipalities')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }


    function barangays($viewValue) {
      return Restangular.service('barangays')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }

    function vouchers() {
      return Restangular
        .one('households', form.data.id)
        .one('vouchers')
        .getList()
        .then(function(response) { vm.vouchers = response.data; })
        .finally(function() { vm.isLoadingVouchers = false; });
    }
  }
})(angular);