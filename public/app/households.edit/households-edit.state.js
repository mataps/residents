+(function(angular) {
  'use strict';

  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var modal;
    /** Trigger modal */
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('households');
      }
        
      modal = $modal
        .open({
          size: 'lg',
          controller: 'HouseholdsEditCtrl as editCtrl',
          templateUrl: '/app/households.edit/households-edit.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }    

    var state = {
      url: '/{id}/edit',
      name: 'households.edit',
      data: { pageTitle: 'Edit household' },
      resolve: { getHouseholdInstanceResolve: getHouseholdInstanceResolve },
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);

    /** Fetch a household instance from the API */
    function getHouseholdInstanceResolve(Household, $stateParams) {
      return Household
        .Repository
        .get($stateParams.id);
    }
  }
})(angular);