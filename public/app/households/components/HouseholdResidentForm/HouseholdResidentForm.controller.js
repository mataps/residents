+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('GCHouseholdResidentFormController', HouseholdResidentFormController);

  function HouseholdResidentFormController($scope, $http, flash) {
    var vm = this;

    console.log(vm);

    // collection, btnPlaceholder, inputPlaceholder
    angular.extend(vm, {
      isLoading: false,
      model: {},
      directive: {},
      $add: $add
    });

    /**
     * Add a resident to the collection
     * @param {Object} data [Resident data]
     */
    function $add(data) {
      var inCollection  = _.chain(vm.collection)
        .pluck('full_name')
        .value()
        .indexOf(data.full_name) !== -1;

      // Check if data is undefined
      // or check the length of the object, to check
      // if it is an empty object
      if ( !angular.isUndefined(data) && !inCollection ) {
        data.household_id = vm.householdId;
        data.resident_id = data.id;
        data.role = 'member';
        $http
          .post('api/v1/residents/' + data.id + '/households', data)
          .then(function(response) {
            // Push to the first position
            vm.collection.splice(0, 0, data);

            // Undefine id and name
            vm.model = {};
            vm.model_name = '';
            flash.success = 'Resident has been successfully added to the household!';
          })
          .catch(function() {

          })
          .finally(function() {



            vm.isLoading = false;

            
          })
      } else {
        flash.error = 'Resident is already in the household';
      }
    }
  }
})(angular);