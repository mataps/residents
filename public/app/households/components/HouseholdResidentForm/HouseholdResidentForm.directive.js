+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .directive('gcHouseholdResidentForm', HouseholdResidentForm);

  /**
   * Usage:
   *  <gc-household-resident-form collection="thatCtrl.collection">
   *  </gchousehold-resident-form>
   */
  function HouseholdResidentForm() {
    var scope = {
      btnPlaceholder: '@',
      inputPlaceholder: '@',
      collection: '=',
      householdId: '@gcHouseholdResidentForm'
    };

    return {
      scope: scope,
      templateUrl: 'gc-household-resident-form.html',
      bindToController: true,
      controllerAs: 'formCtrl',
      controller: 'GCHouseholdResidentFormController'
    };
  }
})(angular);