+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .run(runBlock);

  function runBlock($templateCache) {
    var template = [
      '<aside class="form-group">',
        '<div class="input-group">',
          '<gc-resident-typeahead ',
            'id-variable="formCtrl.model.id" ',
            'collection-variable="formCtrl.directive" ',
            'ng-model="formCtrl.model.name">',
          '</gc-resident-typeahead>',

          '<span class="input-group-btn">',
            '<button type="button" class="btn btn-primary" ',
            'ng-click="formCtrl.$add(formCtrl.directive)" ng-disabld="formCtrl.isLoading">',
              '<i class="ion-plus"></i>',
              'Add',
            '</button>',
          '</span>',
        '</div>',
      '</aside>'
    ].join('');

    $templateCache.put(
      'gc-household-resident-form.html',
      template
    );
  }
})(angular); 