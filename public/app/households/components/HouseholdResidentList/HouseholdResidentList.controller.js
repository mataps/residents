+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('GCHouseholdResidentListController', HouseholdResidentListController);

  function HouseholdResidentListController($scope, $http, flash) {
    var vm = this;

    angular.extend(vm, {
      isLoading: false,
      $remove: $remove,
      $isOwner: $isOwner,
      $setOwner: $setOwner,
      $removeOwner: $removeOwner,
      $updateResidentSuccess: $updateResidentSuccess,
      $removeResidentSuccess: $removeResidentSuccess
    });

    /**
     * Removes an item in the list
     * @param  {int} index Position to be spliced
     */
    function $remove(index) {
      vm.isLoading = true;

      var id = vm.householdId;
      var residentId = vm.collection[index].resident_id;

      $http
        .delete('api/v1/residents/' + residentId + '/households/' + id)
        .then(function(response) {
          // Compare if the selected is the same as the selected
          // item to be removed. If so, set to undefined, then
          // pop the index of the collection

        })
        .catch(function(response) {
          $removeResidentSuccess(response);
        })
        .finally(function(result) {
          if ( !angular.isUndefined(vm.selected) &&
            vm.selected === vm.collection[index] ) {
            vm.selected = {};
          }

          vm.collection.splice(index, 1);
          vm.$removeOwner();
          
          $removeResidentSuccess(response);
        });
    }

    /**
     * Set the owner to the given index in the colletion
     * @param  {int} index Position in the collection
     */
    function $setOwner(index) {
      $removeOwner();
      vm.collection[index].role = 'owner';
    }

    /**
     * Sets the owner to blank
     */
    function $removeOwner(index) {
      if ( !angular.isUndefined(index) ) {
        vm.collection[index].role = 'member';
      }
      
      vm.collection.map(function(member) {
        if ( member.role == 'owner' ) {
          member.role = 'member';
        }
      });
    }

    /**
     * Checks if the resident is identical to the set owner
     * @param  {int}  index [position in the colleciton]
     * @return {Boolean}
     */
    function $isOwner(index) {
      return vm.collection[index].role == 'owner';
    }

    function $updateResidentSuccess(response) {
      flash.success = 'Resident\'s role has been updated!';
      vm.isLoading = false;
    }

    function $removeResidentSuccess(response) {
      flash.success = 'Resident has been removed from the household!'; 
      vm.isLoading = false;
    }
  }
})(angular);