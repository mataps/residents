+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .directive('gcHouseholdResidentList', HouseholdResidentList);

  /**
   * Usage:
   *  <gc-household-resident-form collection="thatCtrl.collection">
   *  </gchousehold-resident-form>
   */
  function HouseholdResidentList() {
    return {
      scope: {
        collection: '=',
        selected: '=',
        owner: '=',
        householdId: '@gcHouseholdResidentList'
      },
      templateUrl: 'gc-household-resident-list.html',
      bindToController: true,
      controllerAs: 'listCtrl',
      controller: 'GCHouseholdResidentListController'
    };
  }
})(angular);