+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .run(runBlock);

  function runBlock($templateCache) {
    var template = [
      '<section class="row">',
        '<article ng-repeat="resident in listCtrl.collection track by $index" ',
          'ng-click="listCtrl.selected = listCtrl.collection[$index]" ',
          'class="clearfix col-md-6" style="margin-bottom: 10px; cursor: pointer; height: 80px;">',
          '<img ng-src="{{ resident.profilePic }}" fallback-src="fallback.gif" holder-fix class="pull-left" style="border-radius: 50%; background: #ddd; margin-top: 10px;" height="64" width="64">',
          '<aside class="pull-left" style="margin-left: 5px;">',
            '<h4 style="margin-top: 0; margin-bottom: 0; text-overflow: ellipsis; white-space: nowrap; overflow: hidden; max-width: 250px;"> {{ resident.full_name }} </h4>',
            '<h5 style="margin-top: 0;"><span gc-mail-to="{{resident.email}}"></span></h5>',
            '<section class="clearfix">',
              '<button type="button" ng-click="listCtrl.$remove($index)" class="btn btn-danget btn-sm pull-right">',
                'Remove',
              '</button>',
              '<button type="button" ng-disabled="listCtrl.isLoading" ',
                'ng-click="listCtrl.$setOwner($index)" ',
                'ng-if="!listCtrl.$isOwner($index)" ',
                'srph-xhr="residents/{{resident.resident_id}}/households/{{listCtrl.householdId}}" request-type="put" request-data="resident" request-success="listCtrl.$updateResidentSuccess" request-error="listCtrl.$updateResidentSuccess" ',
                'class="btn btn-danget btn-sm pull-left">',
                'Set as Owner',
              '</button>',
              '<button type="button" ng-disabled="listCtrl.isLoading" ',
                'ng-click="listCtrl.$removeOwner($index)" ',
                'ng-if="!!listCtrl.$isOwner($index)" ',
                'srph-xhr="residents/{{resident.resident_id}}/households/{{listCtrl.householdId}}" request-type="put" request-data="resident" request-success="listCtrl.$updateResidentSuccess" request-error="listCtrl.$updateResidentSuccess" ',
                'class="btn btn-danget btn-sm pull-left">',
                'Set as Member',
              '</button>',
            '</section>',
          '</aside>',
        '</article>',
      '</section>'
    ].join('');

    $templateCache.put(
      'gc-household-resident-list.html',
      template
    );
  }
})(angular);