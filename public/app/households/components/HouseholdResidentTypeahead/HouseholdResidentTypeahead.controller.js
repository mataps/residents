+(function(angular, _, undefined) {
  angular
    .module('app')
    .controller(
      'GCHouseholdResidentTypeaheadController',
      HouseholdResidentTypeaheadController
    );
    
  function HouseholdResidentTypeaheadController($scope, Resident) {
    //
    var vm = this;

    angular.extend(vm, {
      $appendOwnerProperty: $appendOwnerProperty,
      $syncList: $syncList
    });

    /**
     * Transform the $item variable in the typeahead
     * @param  {Object} $item [Item selected with the typeahead]
     * @return {Object} [Transformed object]
     */
    function $appendOwnerProperty($item) {
      return angular.extend($item, { owner: true });
    }

    /**
     * Syncs the "list" by adding/replacing the
     * passed item to the list
     * @param  {Object} item Item to be added to the list
     */
    function $syncList(item) {
      var index
        , list = vm.list
        , collection = vm.collectionVariable;

      if ( angular.isArray(list) ) {
        // Get index
        index = _.chain(list)
          .pluck('id')
          .value()
          .indexOf(item.id);

        // Replace if it is not yet
        // in the list. Otherwise,
        // push to the first pos
        if ( index !== -1 )  {
          return list.splice(index, 1, item);
        }
        
        return list.splice(0, 0, item);
      }
    }
  }
})(angular, _);