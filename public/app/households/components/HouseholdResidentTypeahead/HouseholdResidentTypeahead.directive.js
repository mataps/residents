+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .directive('gcHouseholdResidentTypeahead', HouseholdResidentTypeahead);

  function HouseholdResidentTypeahead($compile) {
    var scope;

    scope = {
      idVariable: '=',
      collectionVariable: '=',
      transformIdVariable: '@',
      transformCollectionVariable: '=',
      model: '=ngModel',
      list: '=syncList'
    };

    return {
      scope: scope,
      templateUrl: 'gc-household-resident-typeahead-template.html',
      link: linkFn,
      bindToController: true,
      controllerAs: 'typeahead',
      controller: 'GCHouseholdResidentTypeaheadController',
      replace: true
    };

    // TODO
    function linkFn(scope, element, attributes, controller) {
      // If the directive was assigned with "placeholder",
      // modify the default
      if ( !angular.isUndefined(controller.placeholder) ) {
        // element.removeAttr('placeholder');
        // element.removeAttr('gcHouseholdResidentTypeahead');
        // attributes.$set('placeholder', controller.placeholder);
        // $compile(element)(scope);
      }
    }
  }
})(angular);