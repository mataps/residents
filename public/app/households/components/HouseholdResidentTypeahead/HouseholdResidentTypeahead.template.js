+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .run(runBlock);

  function runBlock($templateCache) {
    var template = [
      '<div>',
        '<gc-resident-typeahead ',
          'ng-model="typeahead.model" ',
          'id-variable="typeahead.idVariable" ',
          'collection-variable="typeahead.collectionVariable" ',
          'transform-item="typeahead.$appendOwnerProperty" ',
          'post-callback="typeahead.$syncList">',
        '</gc-resident-typeahead>',
      '</div>'
    ].join('');

    $templateCache.put(
      'gc-household-resident-typeahead-template.html',
      template
    );
  }
})(angular);