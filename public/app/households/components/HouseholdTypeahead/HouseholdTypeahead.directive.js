+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .directive('gcHouseholdTypeahead', HouseholdTypeahead);

  function HouseholdTypeahead($compile) {
    var scope = {
      idVariable: '=',
      collectionVariable: '=',
      transformIdVariable: '@',
      transformCollectionVariable: '=',
      model: '=ngModel',
      placeholder: '@inputPlaceholder'
    };

    return {
      scope: scope,
      templateUrl: 'gc-household-typeahead.js',
      link: linkFn,
      bindToController: true,
      controllerAs: 'typeahead',
      controller: 'GCHouseholdTypeaheadController',
      replace: true
    };

    // TODO
    function linkFn(scope, element, attributes, controller) {
      // If the directive was assigned with "placeholder",
      // modify the default
      if ( !angular.isUndefined(controller.placeholder) ) {
        // element.removeAttr('placeholder');
        // element.removeAttr('gcHouseholdTypeahead');
        // attributes.$set('placeholder', controller.placeholder);
        // $compile(element)(scope);
      }
    }
  }
})(angular);