+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .run(runBlock);

  function runBlock($templateCache) {
    var template = [
      '<div>',
        '<input type="text" class="form-control" ',
        'placeholder="Enter households..." ',
        'ng-model="typeahead.model" ',
        'typeahead="household.name for household ',
          'in typeahead.request($viewValue)" ',
        'typeahead-wait-ms="50" ',
        'typeahead-template-url="gc-household-typeahead-$template.html" ',
        'typeahead-on-select="typeahead.selectCb($item, $model, $label)">',
      '</div>'
    ].join('');

    $templateCache.put(
      'gc-households-typeahead.html',
      template
    );
  }
})(angular);