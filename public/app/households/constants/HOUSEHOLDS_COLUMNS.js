+(function(angular, undefined) {
  'use strict';

  var columns = [
    {
      key: 'label',
      label: 'Label',
      show: true,
      align: 'left',
      cellTemplate: '{{:: household.label }}',
      onClick: 'households.show(household.id)'
    },
    {
      key: 'owner_name',
      label: 'Owner',
      show: true,
      align: 'left',
      cellTemplate: '<span ng-if="household.owner_name">{{:: household.owner_name }}</span><span style="color: red;" ng-if="!household.owner_name">No owner set!</span>',
      onClick: 'households.show(household.id)'
    },
    {
      key: 'consumer_name',
      label: 'Consumer',
      show: true,
      align: 'left',
      cellTemplate: '<span ng-if="household.consumer_name">{{:: household.consumer_name }}</span><span style="color: red;" ng-if="!household.consumer_name">No owner set!</span>',
      onClick: 'households.show(household.id)'
    },
    {
      key: 'phone',
      label: 'Telephone',
      show: false,
      align: 'left',
      cellTemplate: '{{:: household.phone }}',
      onClick: 'households.show(household.id)'
    },
    {
      key: 'municipality',
      label: 'Municipality',
      show: true,
      align: 'left',
      cellTemplate: '{{:: household.cityMunicipality }}',
      onClick: 'households.show(household.id)'
    },
    {
      key: 'barangay',
      label: 'Barangay',
      show: true,
      align: 'left',
      cellTemplate: '{{:: household.barangay}}',
      onClick: 'households.show(household.id)'
    },
    {
      key: 'district',
      label: 'District ',
      show: true,
      align: 'left',
      cellTemplate: '{{:: household.district }}',
      onClick: 'households.show(household.id)'
    },
    {
      key: 'street',
      label: 'Street',
      show: true,
      align: 'left',
      cellTemplate: '{{:: household.street }}'
    },
    {
      key: 'area_code',
      label: 'Area Code',
      show: true,
      align: 'left',
      cellTemplate: '{{:: household.area_code }}'
    },
    {
      key: 'creator.username',
      label: 'Created by',
      show: false,
      align: 'left',
      cellTemplate: '{{:: household.creator.username }}',
      onClick: 'households.show(household.id)'
    },
    {
      key: 'updater.username',
      label: 'Updated by',
      show: false,
      align: 'left',
      cellTemplate: '{{:: household.updater.username }}',
      onClick: 'households.show(household.id)'
    },
    {
      key: 'created_at',
      label: 'Created',
      show: false,
      align: 'left',
      cellTemplate: '{{:: household.created_at }}',
      onClick: 'households.show(household.id)'
    },
    {
      key: 'updated_at',
      label: 'Updated',
      show: false,
      align: 'left',
      cellTemplate: '{{:: household.updated_at }}',
      onClick: 'households.show(household.id)'
    },
    {
      key: 'notes',
      label: 'Notes',
      show: true,
      align: 'left',
      cellTemplate: '{{:: household.notes }}',
      onClick: 'households.show(household.id)'
    },    
  ];

  angular
    .module('app')
    .constant('HOUSEHOLDS_COLUMNS', columns);
})(angular);