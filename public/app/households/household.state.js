+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .config(config)

  function config ($stateProvider) {
    var state = {
      parent: 'main',
      name: 'households',
      url: '/households',
      templateUrl: '/app/households/households.html',
      data: { pageTitle: 'Manage Households' },
      resolve: { getAllHouseholdsResolve: getAllHouseholdsResolve },
      controller: 'HouseholdsCtrl',
      controllerAs: 'households'
    };
    
    $stateProvider.state(state);

    /** Fetch all households */
    function getAllHouseholdsResolve(Household) {
      return Household
        .Repository
        .all();
    }
  };
})(angular);