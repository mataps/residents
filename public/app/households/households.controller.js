+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('HouseholdsCtrl', HouseholdsCtrl);

  function HouseholdsCtrl($scope, $controller, UtilService, HOUSEHOLDS_COLUMNS, Household, flash, $state) {
    // Inherit BaseIndexCtrl's prototype,
    var controller = $controller('BaseIndexCtrl', {
        $scope: $scope,
        $state: $state,
        UtilService: UtilService
    });

    angular.extend(this, controller);

    // ------------------------- //
    // ------------------------- //
    // 
    // viewmodel
    var vm = this,    
       // Data collection (grabbed from the service)
      collection = [];

    // Copy items from the service
    // to the local collection variable
    angular.extend(collection, Household.collection);

    // Initialize vm data
    vm.columns = HOUSEHOLDS_COLUMNS;
    vm.collection = collection;
    vm.isLoading = false;

    // Manually bind the "this"
    // from the base controller
    vm.callServer = angular.bind(vm, vm.callServer);
    vm.applyPageData = angular.bind(vm, vm.applyPageData);
    vm.catchPageDataError = angular.bind(vm, vm.catchPageDataError);

    // Commands that contact the API / server
    vm.getSelected = angular.bind(vm, vm.getSelected);
    vm.remove = remove;
    vm.getPageData = getPageData;

    /**
     * @description  Sends a DELETE to the server
     * @return Restangular
     */
    function remove() {
      var selected = vm.getSelected().join(',');

      if ( !selected.length ) {
        flash.error = 'Please select a household.';
        return;
      }

      if(!confirm('Please confirm deleting the selected records.'))
        return;

      vm.load();

      return Household
        .Command
        .delete(selected)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    };

    vm.QQ = {};

    /**
     * Fetches data and replaces the current
     * @param  string query
     * @return void
     */
    function getPageData(query) {
      vm.load();
      if ( query.page ) {
        // console.log(query);
        // console.log(vm.QQ);
        angular.extend(vm.QQ, query);
        console.log(vm.QQ);
      } else {
        vm.QQ = query;
        console.log(vm.QQ);
      }
      
      return Household
        .Query
        .all(vm.QQ)
        .then(vm.applyPageData)
        .catch(vm.catchPageDataError);
    }

    // Success XHR
    function successXHRCallback(response) {
      var message = 'Household has been successfully archived!';
      flash.success = message;
      $state.reload();
      vm.load(false);
    }

    // Error XHR
    function errorXHRCallback(response) {
      var message = 'An error has occured. Please try again..';
      flash.error = message;
      vm.load(false);
    }
  }
})(angular);