+(function(angular, _, undefined) {
  function ResidentsCreateCtrl($scope, Resident, ssWebcam, FileUploader, flash, $state, sigPlus, SigplusResource, Restangular, $stateParams) {
    var vm = this,
      // Form data
      form = {
        data: {},
        custom: {
          datepickers: { birthdate: false },
          openDatePicker: openDatePicker
        }
      };

    // ------------------------- //
    // ------------------------- //
    vm.isLoading = false;
    vm.form = form;
    vm.submit = submit;

    $scope.$watch(function() {
      return vm.form.data.deceased_at;
    }, function(changed, old) {
      if ( changed == old ) return;

      if ( changed !== null && changed !== undefined && changed !== '' ) {
        vm.form.data.civil_status = 'Deceased';
      }
    });

    vm.districts = function($viewValue) {
      return Restangular.service('districts')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }

    vm.citiesMunicipalities = function($viewValue) {
      return Restangular.service('citiesmunicipalities')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }


    vm.barangays = function($viewValue, cityMunicipality) {
      console.log(cityMunicipality);
      return Restangular.service('barangays')
        .getList({ limit: 6969696969999999, filters: JSON.stringify({ name: $viewValue, 'cityMunicipality.name' : cityMunicipality }) })
        .then(function(response) {
          return response.data;
        });
    }

    vm.successXHRCallback = successXHRCallback;
    vm.errorXHRCallback = errorXHRCallback;

    // ------------------------- //
    // ------------------------- //

    /**
     * @description  Sends an XHR to the server with
     *               the form data for submit a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    function submit(formData) {
      load();

      // Uses either restangular
      // or the uploader's provided
      // XHR
      if (hasImage()) {
        vm.uploader.uploadAll();
      } else {
        Resident
        .Command
        .create(formData)
        .then(vm.successXHRCallback)
        .catch(vm.errorXHRCallback);
      }
    }

    // ------------------------- //
    // ------------------------- //
    function load(bool) {
      vm.isLoading = ( !_.isUndefined(bool) ) ? bool : true;
    }

    // ------------------------- //
    // ------------------------- //

    // Callback function used after
    // a successful XHR
    function successXHRCallback(response) {
      load(false);
      flash.success = 'The resident has been successfully registered!';

      // Remove the '.create' from the state name
      var index = $state.current.name.slice(0, -7);

      // Close modal
      $state.go('residents', null, { reload: false });
    }

    // Callback function used after an XHR
    // returning with errors
    function errorXHRCallback(response) {
      load(false);
      flash.to('validation').error = _.chain(response.data.errors).values().flatten().value();
    }

    // ------------------------- //
    // ------------------------- //
    function hasImage() {
      return vm.uploader.queue.length !== 0;
    }

    /**
     * @name  openDatePicker
     * @description  A toggle for the datepickers
     * @param  e $event
     * @param  string model
     */
    function openDatePicker($event, model) {
      $event.preventDefault();
      $event.stopPropagation();

      vm.form.custom.datepickers[model] = !vm.form.custom.datepickers[model];
    }

    $scope.openSigPlus = function() {
      sigPlus.open({
        onDone: function(hex) {
          SigplusResource.post({sigImgData: hex}).then(function(res) {
            vm.form.data.signature_path = res.data.data.url;
          });
        }
      });
    };
  }

  angular
    .module('app')
    .controller('ResidentsCreateCtrl', ResidentsCreateCtrl);
})(angular, _);
