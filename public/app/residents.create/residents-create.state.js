+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'New Resident' };
    var modal;

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = undefined;
    // 
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('residents');
      }
        
      modal = $modal
        .open({
          size: 'lg',
          resolves: resolves,
          controllerAs: 'createCtrl',
          controller: 'ResidentsCreateCtrl',
          templateUrl: '/app/residents.create/residents-create.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }
    

    var state = {
      url: '/create',
      name: 'residents.create',
      data: data,
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);