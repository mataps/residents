+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('ResidentsEditBulkAffiliationsAddController', controller);

  function controller($scope, Restangular, residentIDResolve, listResolve, $state, flash) {
    angular.extend($scope, {
      data: {
        affiliations: [{ id: '' }],
        founder: false,
        resident_id: residentIDResolve,
        position_id: 1
      },
      isLoading: false,
      $add: $add,
      $request: $request,
      $getIDs: $getIDs
    });

    /**
     * Adds an attribute
     */
    function $add() {
      $scope.data.affiliations.push({ id: null });
    }

    function $remove($index) {
      $scope.data.affiliations.splice($index, 1);
    }

    /**
     * Sends a request to the server to
     * process the data
     */
    function $request() {
      $scope.isLoading = true;

      var ids = rmComma(mappedIds($scope.data.affiliations).join(','));
      var data = $scope.data;

      console.log(ids);

      return Restangular
        .one('affiliations', ids)
        .post('residents', data)
        .then(function() {
          flash.success = 'Successfully bulk added the resident to different affiliations';
          $state.reload();
          $scope.$dismiss();
        })
        .catch(function(response) {
          var data = response.data;

          flash.to('validation').error = _.chain(data)
            .values()
            .flatten()
            .value();
        })
        .finally(function() { $scope.isLoading = false });
    }

    /**
     *
     */
    function $getIDs() {
      return mappedIds($scope.affiliations);
    }
  }

  /**
   * Returns the ID property of each object in an array
   * @returns object
   */
  function mappedIds(i) { return i.map(function(e) { return e.id }); }
  function rmComma(s) { return s.replace(/(^\,|\,$)/i, ''); }
})(angular);