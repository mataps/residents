+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('ResidentsEditBulkAttributesAddController', controller);

  function controller($scope, Restangular, residentIDResolve, flash, $state) {
    angular.extend($scope, {
      data: {
        attributions: [{ id: '' }],
        resident_id: residentIDResolve
      },
      isLoading: false,
      $add: $add,
      $request: $request,
      $getIDs: $getIDs
    });

    /**
     * Adds an attribute
     */
    function $add() {
      $scope.data.attributions.push({ id: '' });
    }

    function $remove($index) {
      $scope.data.attributions.splice($index, 1);
    }

    /**
     * Sends a request to the server to
     * process the data
     */
    function $request() {
      $scope.isLoading = true;

      var ids = mappedIds($scope.data.attributions).join(',');
      var data = $scope.data;

      if ( !ids.length ) {
        flash.error = 'Oops, please add an affilition';
        return;
      }

      return Restangular
        .one('attributions', ids)
        .post('residents', data)
        .then(function() {
          flash.success = 'Successfully bulk added the resident to different affiliations';
          $state.reload();
          $scope.$dismiss();
        })
        .catch(function(response) {
          var data = response.data.data;

          flash.to('validation').error = _.chain(data)
            .values()
            .flatten()
            .value();
        })
        .finally(function() { $scope.isLoading = false });
    }

    function $getIDs() {
      return mappedIds($scope.data.attributions);
    }
  }

  /**
   * Returns the ID property of each object in an array
   * @returns object
   */
  function mappedIds(i) { return i.map(function(e) { return e.id }); }
})(angular);