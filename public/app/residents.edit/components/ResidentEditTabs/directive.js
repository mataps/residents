+(function(angular, undefined) {
  "use strict"
  angular
    .module('app')
    .directive('gcResidentEditTabs', directive);

  function directive() {
    return {
      scope: {
        id: '@',
        es_id: '@esId'
      },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    function templateFn() {
      return [
        '<div style="margin-left: -15px; margin-right: -15px">',
        '<tabset>',
          '<tab id="affiliations-tab" heading="Affiliations" class="tab-primary"><div gc-resident-edit-tabs-affiliations id="{{ id }}"></div></tab>',
          '<tab id="attributions-tab" heading="Attribution" class="tab-primary"><div gc-resident-edit-tabs-attributions id="{{ id }}"></div></tab>',
          '<tab id="transactions-tab" heading="Transactions" class="tab-primary"><div gc-resident-edit-tabs-transactions id="{{ id }}"></div></tab>',
          '<tab id="family-tab" heading="Family" class="tab-primary"><div gc-resident-edit-tabs-family id="{{ id }}"></div></tab>',
          '<tab id="households-tab" heading="Households" class="tab-primary"><div gc-resident-edit-tabs-households id="{{ id }}"></div></tab>',
          '<tab id="lqty-tab" class="tab-primary">',
            '<tab-heading class="tab-primary">',
              'LQTY <span class="badge">{{ count.lqty }}</span>',
            '</tab-heading>',
            '<div gc-resident-edit-tabs-lqty id="{{ id }}" count="count.lqty"></div>',
          '</tab>',
          '<tab id="mqty-tab" class="tab-primary">',
            '<tab-heading class="tab-primary">',
              'MQTY <span class="badge">{{ count.mqty }}</span>',
            '</tab-heading>',
            '<div gc-resident-edit-tabs-mqty id="{{ id }}" count="count.mqty"></div>',
          '</tab>',
        '</tabset>',
        '</div>'
      ].join('');
    }

    function controllerFn($scope) {
      angular.extend($scope, {
        count: {
          lqty: 0,
          mqty: 0
        }
      });
    }
  }
})(angular);
