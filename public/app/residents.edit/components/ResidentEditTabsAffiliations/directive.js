+(function(angular, undefined) {
  "use strict"
  angular
    .module('app')
    .directive('gcResidentEditTabsAffiliations', directive);

  function directive() {
    return {
      scope: { id: '@' },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    function templateFn() {
      return [
        '<div>',
          '<div class="clearfix form-group" ng-if="!isAdding">',
            '<div class="pull-right">',
              '<button type="button" ng-click="$bulk()" class="btn btn-success" style="margin-right: 5px;"><span class="fa fa-plus"></span>Bulk Add Affiliation</button>',
              '<button ng-click="$add()" id="add-new-affiliation" class="btn btn-success"><span class="fa fa-plus"></span>Add Affiliation</button>',
            '</div>',
            '<div class="pull-left">',
              '<h4 class="text-center" ng-if="!affiliations.length"><small> This resident does not belong to any affiliation yet.</small></h4>',
            '</div>',
          '</div>',
          '<form ng-submit="$submit()" ng-if="isAdding">',
            '<div class="form-group"><div gc-affiliation-typeahead ng-model="data.affiliation_name" id-variable="data.affiliation_id"></div></div>',
            '<div class="form-group row">',
              '<div class="col-md-8"><div gc-affiliation-position-dropdown-deferred="data.position_id" affiliation-id="data.affiliation_id"></div></div>',
              '<div class="col-md-4"><label><input type="checkbox" ng-model="data.founder" ng-init="data.founder = false"></label> Founder</div>',
            '</div>',
            '<div class="form-group"><gc-resident-typeahead placeholder="Enter referrer\'s name" id-variable="data.referrer_id" ng-model="data.referrer_name"></gc-resident-typeahead></div>',
            '<div class="form-group"><gc-resident-typeahead placeholder="Enter head\'s name" id-variable="data.head_id" ng-model="data.head_name"> </gc-resident-typeahead></div>',
            '<div class="clearfix form-group">',
              '<button type="submit" class="btn btn-primary btn-sm pull-left" ng-disabled="isSubmitting">Submit</button>',
              '<button type="button" class="btn btn-default btn-sm pull-right" ng-click="$cancel()">Cancel</button>',
            '</div>',
          '</form>',
          '<table id="affiliations-table" class="table table-hover" ng-if="!!affiliations.length">',
            '<thead>',
              '<tr>',
                '<th>Affiliation</th>',
                '<th>Position</th>',
                '<th>Founder</th>',
                '<th>Referrer</th>',
                '<th>Head</th>',
                '<th></th>',
              '</tr>',
            '</thead>',
            '<tbody>',
              '<tr ng-repeat="affiliation in affiliations">',
                '<td style="cursor: pointer;" ng-click="$go(affiliation.id)">{{ affiliation.name }}</td>',
                '<td style="cursor: pointer;" ng-click="$go(affiliation.id)">',
                  '{{ affiliation.position.name }}',
                  '<span ng-if="affiliation.founder" class="label label-primary">Founder </span>',
                '</td>',
                '<td style="cursor: pointer;" ng-click="$go(affiliation.affiliation.id)">',
                  '<span class="label label-warning badge" ng-if="!affiliation.founder"><i class="ion-minus-circled"></i></span>',
                  '<span class="label label-success badge" ng-if="affiliation.founder"><i class="ion-checkmark-circled"></i></span>',
                '</td>',
                '<td style="cursor: pointer;" ng-click="$go(affiliation.affiliation.id)">{{ affiliation.referrer.full_name}}</td>',
                '<td style="cursor: pointer;" ng-click="$go(affiliation.affiliation.id)">{{ affiliation.head.full_name}}</td>',
                '<td>',
                  '<button type="button" ng-click="$remove($index)" class="btn btn-danger btn-xs" ng-disabled="affiliation[IS_DELETING_PROP]">',
                    '<i class="ion-trash-a"></i>',
                  '</button>',
                '</td>',
              '</tr>',
            '</tbody>',
          '</table>',
        '</div>'
      ].join('');
    }

    function controllerFn($scope, $http, flash, $modal, $state) {
      var IS_DELETING_PROP = '_isDeleting';

      angular.extend($scope, {
        affiliations: [],
        data: {
          affiliation_id: '',
          'referrer_id': '',
          head_id: ''
        }, // Form data
        isAdding: false,
        isSubmitting: false,
        $add: $add,
        $cancel: $cancel,
        $submit: $submit,
        $remove: $remove,
        $bulk: $bulk,
        $go: $go,

        IS_DELETING_PROP: IS_DELETING_PROP
      });

      

      (function init() {
        $http.get('api/v1/residents/' + $scope.id + '/affiliations')
          .then(function(res) { $scope.affiliations = res.data.data; });
      })();

      function $add() {
        $scope.isAdding = true;
        $scope.data = {
          affiliation_id: '',
          'referrer_id': '',
          head_id: ''
        };
      }

      function $cancel() {
        if ( !confirm('Are you sure to cancel adding an affiliation to this resident? You will lose all fields.') ) {
          return;
        }

        $scope.isAdding = false;
      }

      function $submit() {
        $scope.isSubmitting = true;
        
        return $http.post('api/v1/residents/' + $scope.id + '/affiliations', $scope.data)
          .then(function(res) {
            $scope.isAdding = false;
            flash.success = 'The resident has been successfully added to the affiliation!';
            $state.reload();
          })
          .catch(function(res) {
            flash.to('validation').error = _.chain(res.data.errors)
              .values()
              .flatten()
              .value();
          })
          .finally(function() {
            $scope.isSubmitting = false;
          });
      }

      function $remove(index) {
        if ( !confirm('Are you sure to remove this resident from the selected affiliation?') ) {
          return;
        }

        var affiliation = $scope.affiliations[index];
        flash.info = 'The resident is now being removed from the affiliation..';
        $scope.affiliations[index][IS_DELETING_PROP] = true;

        return $http.delete('api/v1/residents/' + $scope.id + '/affiliations/' + affiliation.id)
          .then(function(res) {
            $scope.affiliations.splice(index, 1);
            flash.success = 'The resident has been successfully removed from the affiliation';
          })
          .catch(function(res) {
            flash.error = res.data.errors;
          });
      }

      function $bulk() {
        $modal.open({
          templateUrl: '/app/residents.edit/bulk/affiliations/bulk.html',
          controller: 'ResidentsEditBulkAffiliationsAddController',
          resolve: {
            residentIDResolve: function() { return $scope.id; },
            listResolve: function() {
              return function(affs) {
                $scope.affiliations.concat(affs);
              }
            }
          }
        });
      }

      function $go(id) {
        $state.go('affiliations.edit', { id: id });
      }
    }
  }
})(angular);