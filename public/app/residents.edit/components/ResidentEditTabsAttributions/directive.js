+(function(angular, undefined) {
  "use strict"
  angular
    .module('app')
    .directive('gcResidentEditTabsAttributions', directive);

  function directive() {
    return {
      scope: { id: '@' },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    function templateFn() {
      return [
        '<div class="clearfix form-group" ng-if="!isAdding">',
          '<div class="pull-right">',
            '<button type="button" class="btn btn-success" style="margin-right: 5px;" ng-click="$bulk()"><span class="fa fa-plus"></span>Bulk Add Attributions</button>',
            '<button type"button" class="btn btn-success" ng-click="$add()"><span class="fa fa-plus"></span>Add attribution</button>',
          '</div>',
          '<div class="pull-left">',
            '<h4 class="text-center" ng-if="!attributions.length"><small> This resident has no assigned attribution yet.</small></h4>',
          '</div>',
        '</div>',
        '<form ng-submit="$submit()" ng-if="isAdding">',
          '<table class="table">',
          '<tr class="bg-light">',
            '<td><div gc-attribution-typeahead ng-model="data.attribute_id"></div></td>',
            '<td><input ui-date class="form-control" placeholder="MM-DD-YYYY" ng-model="data.from" id="from" datepicker-popup="MM-dd-yyyy"></td>',
            '<td><input ui-date class="form-control"  placeholder="MM-DD-YYYY" ng-model="data.to" id="to" datepicker-popup="MM-dd-yyyy"></td>',
            '<td><button type="submit" class="btn btn-primary btn-xs" style="margin-right: 5px;">Submit</button></td>',
            '<td><button type="button" class="btn btn-default btn-xs" ng-click="$cancel()">Cancel</button></td>',
            '</tr>',
          '</table>',
        '</form>',
        '<table class="table table-hover" ng-if="!!attributions.length">',
          '<thead>',
            '<tr>',
              '<th>Attribution</th>',
              '<th>From</th>',
              '<th>To</th>',
              '<th>Flag</th>',
              '<th></th>',
            '</tr>',
          '</thead>',
          '<tbody>',
            '<tr ng-repeat="attribution in attributions">',
              '<td>{{ attribution.name }}</td>',
              '<td><span ng-if="!!parseInt(attribution.pivot.from, 10)"> {{ attribution.pivot.from }} </span> </td>',
              '<td><span ng-if="!!parseInt(attribution.pivot.to, 10)"> {{ attribution.pivot.to }} </span></td>',
              '<td class="text-center"><span class="badge" ng-class="attribution.class">{{ attribution.label }}</span></td>',
              '<td>',
                '<button type="button" ng-click="$remove($index)" class="btn btn-danger btn-xs" ng-disabled="attributions[IS_DELETING_PROP]">',
                  '<i class="ion-trash-a"></i>',
                '</button>',
              '</td>',
            '</tr>',
          '</tbody>',
        '</table>'
      ].join('');
    }

    function controllerFn($scope, $http, flash, $modal, $state) {
      var IS_DELETING_PROP = '_isDeleting';

      angular.extend($scope, {
        attributions: [],
        data: { attribution_id: '' },
        isAdding: false,
        $add: $add,
        $cancel: $cancel,
        $submit: $submit,
        $remove: $remove,
        $bulk: $bulk,

        IS_DELETING_PROP: IS_DELETING_PROP
      });

      (function init() {
        $http.get('api/v1/residents/' + $scope.id + '/attributions?limit=99999')
          .then(function(res) { 
            // console.log(res);
            $scope.attributions = res.data.data });
      })();

      function $add() {
        $scope.data = { attribution_id: '' };
        $scope.isAdding = true;
      }

      function $cancel() {
        if ( !confirm('Are you sure to cancel adding an attribution to this resident? You will lose all fields.') ) {
          return;
        }

        $scope.isAdding = false;
      }

      function $submit() {
        $scope.isSubmitting = true;
        
        return $http.post('api/v1/residents/' + $scope.id + '/attributions', $scope.data)
          .then(function(res) {
            $scope.attributions.splice(0, 0, res.data.data);
            $scope.isAdding = false;
            flash.success = 'The attribution has been successfully added!';
              $state.reload();
          })
          .catch(function(res) {
            flash.to('validation').error = _.chain(res.data.errors)
              .values()
              .flatten()
              .value();
          })
          .finally(function() {
            $scope.isSubmitting = false;
             $state.reload();
          });
      }

      function $remove(index) {
        if ( !confirm('Are you sure to remove this attribution from the resident?') ) {
          return;
        }

        var attribution = $scope.attributions[index];

        flash.info = 'The resident is now being removed from the affiliation..';
        $scope.affiliations[index][IS_DELETING_PROP] = true;

        return $http.delete('api/v1/residents/' + $scope.id + '/attributions/' + attribution.id)
          .then(function(res) {
            $scope.attributions.splice(index, 1);
            flash.success = 'The attribution has been removed from the resident.';
          })
          .catch(function(res) {
            flash.error = res.data.errors;
          });
      }

      function $bulk() {
         $modal.open({
          templateUrl: '/app/residents.edit/bulk/attributions/bulk.html',
          controller: 'ResidentsEditBulkAttributesAddController',
          resolve: {
            residentIDResolve: function() {
              return $scope.id
            }
          }
        });
      }
    }
  }
})(angular);