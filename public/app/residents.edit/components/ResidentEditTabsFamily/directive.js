+(function(angular, undefined) {
  "use strict"
  angular
    .module('app')
    .directive('gcResidentEditTabsFamily', directive);

  function directive() {
    return {
      scope: { id: '@' },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    function templateFn() {
      return [
        '<div class="clearfix form-group" ng-if="!isAdding">',
          '<button type="button" ng-click="$add()" class="pull-right btn btn-success"><span class="fa fa-plus"></span>Add Relative</button>',
            '<div class="pull-left" ng-if="!relatives.length">',
              '<h4 class="text-center"><small> This resident has no assigned relatives yet.</small></h4>',
            '</div>',
          '</div>',
          '<div ng-if="isAdding">',
            '<div class="form-group">',
              '<gc-resident-typeahead ng-model="data.resident_name" id-variable="data.resident_id"></gc-resident-typeahead>',
            '</div>',
            '<div class="row form-group">',
              '<div class="col-md-8">',
                '<select ng-model="data.relation" class="form-control">',
                  '<option> Select a relationship.. </option>',
                  '<option value="parent">Guardian</option>',
                  '<option value="spouse">Spouse</option>',
                  '<option value="father">Father</option>',
                  '<option value="mother">Mother</option>',
                  '<option value="grandfather">Grandfather</option>',
                  '<option value="grandmother">Grandmother</option>',
                  '<option value="grandson">Grandson</option>',
                  '<option value="granddaughter">Granddaughter</option>',
                  '<option value="son">Son</option>',
                  '<option value="daughter">Daughter</option>',
                  '<option value="cousin">Cousin</option>',
                  '<option value="aunt">Aunt</option>',
                  '<option value="uncle">Uncle</option>',
                  '<option value="nephew">Nephew</option>',
                  '<option value="niece">Niece</option>',
                  '<option value="sister">Sister</option>',
                  '<option value="brother">Brother</option>',
                  '<option value="brother-in-law">Brother In Law</option>',
                  '<option value="sister-in-law">Sister In Law</option>',
                  '<option value="mother-in-law">Mother In Law</option>',
                  '<option value="father-in-law">Father In Law</option>',
                  '<option value="son-in-law">Son In Law</option>',
                  '<option value="daughter-in-law">Daughter In Law</option>',
                '</select>',
              '</div>',
              '<div class="col-md-4">',
                '<button type="button" ng-click="$submit(parent)" class="btn btn-success" ng-disabled="isSubmitting">Add</button>',
              '</div>',
            '</div>',
          '</div>',
          '<table id="relationships-table" class="table table-hover" ng-if="!!relatives.length">',
            '<thead>',
              '<tr>',
                '<th></th>',
                '<th>Full Name</th>',
                '<th>Relation</th>',
                '<th></th>',
              '</tr>',
            '</thead>',
            '<tbody id="attribute-body">',
              '<tr ng-repeat="relative in relatives">',
                '<td class="text-center"  style="cursor: pointer;" ng-click="kierPogi(relative.id, relative.es_id)">',
                  '<img width="60px" height="60px" ng-src="{{ relative.profilePic }}" fallback-src="fallback.gif" alt="" class="img-circle" ng-show="relative.profilePic">',
                  '<img src="holder.js/60x60" fallback-src="fallback.gif" holder-fix alt="" class="img-circle" ng-show="!relative.profilePic">',
                '</td>',
                '<td style="cursor: pointer;" ng-click="$go(relative.id, relative.es_id)">{{ relative.full_name }}</td>',
                '<td style="cursor: pointer;" ng-click="$go(relative.id, relative.es_id)">{{ relative.relation | studlyCase }}</td>',
                '<td>',
                  '<button type="submit" class="btn btn-danger" ng-click="$remove()" request-type="delete" request-success="parentRemoveSuccess" request-error="parentRemoveError" ng-disabled="parentDeleting">',
                    '<i class="ion-close"></i>',
                  '</button>',
                '</td>',
              '</tr>',
            '</tbody>',
          '</table>',
        '</div>',
      ].join('');
    }

    function controllerFn($scope, $http, flash, $state) {
      var IS_DELETING_PROP = '_isDeleting';

      angular.extend($scope, {
        relatives: [],
        data: { resident_id: '' },

        isSubmitting: false,
        isAdding: false,

        $add: $add,
        $cancel: $cancel,
        $submit: $submit,
        $remove: $remove,
        $go: $go,

        IS_DELETING_PROP: IS_DELETING_PROP
      });

      (function init() {
        $http.get('api/v1/residents/' + $scope.id + '/relatives')
          .then(function(res) { $scope.relatives = res.data;  console.log(res); });
      })();

      function $add() {
        $scope.isAdding = true;
        $scope.resident = { resident_id: '' };
      }

      function $cancel() {
        if ( !confirm('Are you sure to cancel? You will lose all fields.') ) {
          return;
        }

        $scope.isAdding = false;
      }

      function $submit() {
        $scope.isSubmitting = true;

        return $http.post('api/v1/residents/' + $scope.id + '/relatives', $scope.data)
          .then(function(res) {
            flash.success = 'Succesfully added resident as relative.';
            $scope.isAdding = false;
            $scope.relatives.splice(0, 0, res.data.data);
             $state.reload();
          })
          .catch(function(res) {
            flash.to('validation').error = _.chain(res.data.errors)
              .values()
              .flatten()
              .value();
          })
          .finally(function() { $scope.isSubmitting = false });
      }

      function $remove() {
        if ( !confirm('Are you sure to remove the relationship?') ) {
          return;
        }

        var relative = $scope.relatives[index];
        relative[IS_DELETING_PROP] = true;

        return $http.delete('api/v1/residents/' + $scope.id + '/relatives/' + relative.id)
          .then(function(res) {
            $scope.relatives.splice(index, 1);
            flash.success = 'The relationship has been successfully removed';
          })
          .catch(function() {
            flash.error = 'An error occured while removing the relationship..';
            relative[IS_DELETING_PROP] = false;
          });
      }

      function $go(id, es_id) {
        $state.go('residents.edit', { id: id, es_id: es_id }, { reload: true });
      }
    }
  }
})(angular);
