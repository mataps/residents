+(function(angular, undefined) {
  "use strict"
  angular
    .module('app')
    .directive('gcResidentEditTabsHouseholds', directive);

  function directive() {
    return {
      scope: { id: '@' },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    function templateFn() {
      return [
        '<div class="form-group row">',
          '<div class="col-md-7">',
            '<label>Household Name: </label>',
            '{{ household.label }}',
          '</div>',
          '<div class="col-md-5">',
            '<label>Phone #: </label>',
            '{{ household.phone }}',
          '</div>',
        '</div>',
        '<div class="form-group">',
          '<label>Address: </label>',
          '{{ household.street }} {{ household.barangay }} {{ household.cityMunicipality }}',
        '</div>',
        '<tabset class="tabs-left">',
          '<tab heading="Members">',
            '<table class="table" ng-if="!!housemates.length">',
              '<thead>',
                '<tr>',
                  '<th></th>',
                  '<th>Full Name</th>',
                  '<th>Gender</th>',
                '</tr>',
              '</thead>',
              '<tbody>',
                '<tr ng-repeat="housemate in housemates">',
                  '<td style="cursor: pointer;" ng-click="$go(housemate.id, housemate.es_id)">',
                    '<div class="text-center">',
                      '<img width="60px" height="60px" ng-src="{{ housemate.profilePic }}" fallback-src="fallback.gif" alt="" class="img-circle" ng-show="housemate.profilePic">',
                      '<img src="holder.js/60x60" fallback-src="fallback.gif" holder-fix alt="" class="img-circle" ng-show="!housemate.profilePic">',
                    '</div>',
                  '</td>',
                  '<td style="cursor: pointer; vertical-align: middle;" ng-click="$go(housemate.id, housemate.es_id)">{{ housemate.full_name }}</td>',
                  '<td style="cursor: pointer; vertical-align: middle;" ng-click="$go(housemate.id, housemate.es_id)">{{ housemate.gender }}</td>',
                '</tr>',
              '</tbody>',
          '</table>',
          '<h5 class="text-center" ng-if="!housemates.length"><small>No residents linked to this household.</small></h5>',
        '</tab>',
        '<tab heading="Transactions">',
          '<table class="table">',
            '<thead>',
              '<tr>',
                '<th> UUID </th>',
                '<th> Client </th>',
                '<th> Status </th>',
                '<th> Amount </th>',
              '</tr>',
            '</thead>',
            '<tbody>',
              '<tr ng-repeat="voucher in vouchers">',
                '<td> {{:: voucher.uuid }} </td>',
                '<td> {{:: voucher.client.full_name }} </td>',
                '<td> {{:: voucher.status }} </td>',
                '<td> PHP {{:: voucher.amount }} </td>',
              '</tr>',
            '</tbody>',
          '</table>',
        '<h5 class="text-center" ng-if="!vouchers.length"><small>No transactions linked to this household.</small></h5>'
      ].join('');
    }

    function controllerFn($scope, $http, $state) {
      angular.extend($scope, {
        household: {},
        housemates: [],
        vouchers: [],

        $go: $go
      });

      /**
       * Request for the household, housemates, and vouchers data.
       */
      (function init() {
        $http.get('api/v1/residents/' + $scope.id + '/households')
          .then(function(res) {
            $scope.household = res.data.data[0];  
            return $http.get('api/v1/households/' + $scope.household.id + '/vouchers?limit=999999');
          })
          .then(function(res) { $scope.vouchers = res.data.data; });

        $http.get('api/v1/residents/' + $scope.id + '/housemates?limit=10000')
          .then(function(res) { $scope.housemates = res.data.data; });
      })();

      function $go(id, es_id) {
        $state.go('residents.edit', { id: id, es_id: es_id }, { reload: true });
      }
    }
  }
})(angular);