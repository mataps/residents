+(function(angular, undefined) {
  "use strict"
  angular
    .module('app')
    .directive('gcResidentEditTabsMqty', directive);

  function directive() {
    return {
      scope: {
        count: '=',
        id: '@',
        es_id: '@'
      },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    function templateFn() {
      return [
        '<h4 class="text-center" ng-if="!mqty.length"><small> No resident with similar maternal name.</small></h4>',
        '<table class="table table-hover" ng-if="!!mqty.length">',
          '<thead>',
            '<tr>',
              '<th></th>',
              '<th>Full Name</th>',
              '<th>Precint #</th>',
            '</tr>',
           '</thead>',
           '<tbody>',
            '<tr ng-repeat="resident in mqty">',
              '<td class="text-center" style="cursor: pointer;" ng-click="$viewResident(resident.id, resident.es_id)">',
                '<img width="60px" height="60px" fallback-src="fallback.gif" ng-src="{{ resident.profilePic }}-sm" class="img-circle" ng-if="resident.profilePic">',
                '<img src="holder.js/60x60" fallback-src="fallback.gif" holder-fix alt="" class="img-circle" ng-if="!resident.profilePic">',
              '</td>',
              '<td style="cursor: pointer;" ng-click="$viewResident(resident.id, resident.es_id)">{{ resident.full_name }}</td>',
              '<td style="cursor: pointer;" ng-click="$viewResident(resident.id, resident.es_id)">{{ resident.precint || \'None\' }}</td>',
            '</tr>',
          '</tbody>',
        '</table>',
      ].join('');
    }

    function controllerFn($scope, $http, $state) {
      angular.extend($scope, {
        mqty: [],
        $viewResident: $viewResident
      });

      (function init() {
        $http.get('api/v1/residents/MQTY/' + $scope.id + '?limit=999999')
          .then(function(res) {
            $scope.mqty = res.data.data;
            $scope.count = $scope.mqty.length;
          });
      })();

      function $viewResident(id, es_id) {
        $state.go('residents.edit', { id: id, es_id: es_id }, { reload: true });
      }
    }
  }
})(angular);