+(function(angular, undefined) {
  "use strict"
  angular
    .module('app')
    .directive('gcResidentEditTabsTransactions', directive);

  function directive() {
    return {
      scope: {
        id: '@'
      },

      template: templateFn,
      controller: controllerFn
    };

    function templateFn() {
      return [
        '<h4 ng-if="!transactions.length"><small> The resident does have any transaction yet. </small></h4>',
        '<table class="table" ng-if="!!transactions.length">',
          '<thead>',
            '<tr>',
              '<th>Voucher</th>',
              '<th>Item</th>',
              '<th>Account</th>',
              '<th>Amount</th>',
              '<th>Settled</th>',
            '</tr>',
          '</thead>',
          '<tbody>',
            '<tr ng-repeat="transaction in transactions" >',
              '<td>{{ transaction.voucher.uuid }}</td>',
              '<td>{{ transaction.item.name }}</td>',
              '<td>{{ transaction.account.name }}</td>',
              '<td>Php {{ transaction.amount | money }}</td>',
              '<td>',
                '<span class="label label-warning badge" ng-if="!transaction.voucher.settled_at">',
                  '<i class="ion-minus-circled"></i>',
                '</span>',
                '<span ng-if="transaction.voucher.settled_at">',
                  '<span class="label label-success badge"><i class="ion-checkmark-circled"></i></span> ',
                  '{{ transaction.voucher.settled_at | date: \'mediumDate\' }}',
                '</span>',
              '</td>',
            '</tr>',
         '</tbody>',
        '</table>'
      ].join('');
    }

    function controllerFn($scope, $http) {
      angular.extend($scope, {
        transactions: []
      });

      (function init() {
        $http.get('api/v1/residents/' + $scope.id + '/transactions')
          .then(function(res) { $scope.transactions = res.data.data });
      })();
    }
  }
})(angular);