+(function(angular, undefined) {
  "use strict";
  angular
    .module('app')
    .directive('gcResidentProfile', directive);

  function directive() {
    return {
      scope: {
        es_id: '@esId',
        data: '=',
      },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    function templateFn() {
      return [
        '<div class="media">',
          '<div class="pull-left">',
            '<div gc-resident-profile-avatar editing="isEditing" data="data" es-id="{{ es_id }}" uploader="uploader"></div>',
          '</div>',
          '<div gc-resident-profile-fields update="$update()" cancel="$cancel()" data="copy" es-id="{{ es_id }}" ng-if="isEditing"></div>',
          '<div gc-resident-profile-static edit="$edit()" data="data" es_id="{{ es_id }}" ng-if="!isEditing"></div>',
        '</div>'
      ].join('');
    }

    function controllerFn($scope, $http, flash) {
      angular.extend($scope, {
        copy: angular.copy($scope.data),
        isEditing: false,
        isLoading: false,
        $edit: $edit,
        $cancel: $cancel,
        $update: $update,
        $successHandler: $successHandler,
        $errorHandler: $errorHandler
      });

      /**
       * 
       */
      function $update() {
        var data = $scope.copy;
        $scope.isLoading = true;

        if ( !!$scope.uploader && !!$scope.uploader.queue.length ) {
          $scope.uploader.uploadAll();
        } else {
          $http.put('api/v1/residents/' + data.id, data)
            .then($successHandler)
            .catch($errorHandler)
            .finally(function() {
              angular.extend($scope, {
              isLoading: false,
              isEditing: false
            });
          });
        }
      }

      function $edit() {
        $scope.isEditing = true;
      }

      /**
       * Cancel editing
       */
      function $cancel() {
        if ( !confirm('Are you sure to cancel any changes?') ) {
          return;
        }

        $scope.isEditing = false;
      }

      function $successHandler(res) {
        flash.success = 'The resident has been successfully updated!';

        angular.extend($scope, {
          isEditing: false,
          data: res.data.data,
          copy: angular.copy(res.data.data)
        });
      }

      function $errorHandler(res) {
        flash.to('validation').error = _.chain(res.data.errors)
          .values()
          .flatten()
          .value();
      }
    }
  }
})(angular);