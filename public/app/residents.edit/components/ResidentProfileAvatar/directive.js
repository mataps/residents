+(function(angular, undefined) {
  "use strict";
  angular
    .module('app')
    .directive('gcResidentProfileAvatar', directive);

  function directive() {
    return {
      scope: {
        uploader: '=',
        editing: '=',
        data: '=',
        copy: '=',
        es_id: '@esId',
        errorHandler: '=',
        successHandler: '='
      },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    function templateFn() {
      return [
        '<div ng-if="!editing">',
          '<img class="img-thumbnail" alt="Resident profile image" ng-src="{{ data.profilePic  }}" holder-fix fallback-src="fallback.gif" ng-show="data.profilePic">',
          '<img class="img-thumbnail" alt="Resident profile image" src="holder.js/120x120" holder-fix ng-hide="data.profilePic">',
        '</div>',
        '<div gc-display-picture ',
          'uploader="uploader" ',
          'success-callback="successHandler" ',
          'error-callback="errorHandler" ',
          'form-data="copy" ',
          'ng-if="editing" ',
          'def="{{ copy.profilePic }}" ',
          'url="api/v1/residents/{{ copy.id }}" ',
          'method="put">',
        '</div>'
      ].join('');
    }

    function controllerFn($scope) { }
  }
})(angular);
