+(function(angular, undefined) {
  "use strict"
  angular
    .module('app')
    .directive('gcResidentProfileFields', directive);

  function directive() {
    return {
      scope: {
        data: '=',
        update: '&',
        cancel: '&',
        es_id: '@esId'
      },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    function templateFn() {
      return [
        '<div class="media-body">',
          '<div class="clearfix form-group">',
            '<button class="pull-left btn btn-warning" ng-click="cancel()"> Cancel </button>',
            '<button type="button" class="pull-right btn btn-success" ng-click="update()"> Update </button>',
          '</div>',
          '<div class="form-group row">',
            '<div class="col-md-4"> <label> First Name </label> <input type="text" ng-model="data.first_name" class="form-control"> </div>',
            '<div class="col-md-4"> <label> Middle Name </label> <input type="text" ng-model="data.middle_name" class="form-control"> </div>',
            '<div class="col-md-4"> <label> Last Name </label> <input type="text" ng-model="data.last_name" class="form-control"> </div>',
          '</div>',
          '<div class="form-group row">',
            '<div class="col-md-6">',
              '<label>',
                'Is A Voter',
                '<input type="checkbox" ng-model="data.is_voter" class="form-control">',
              '</label>',
            '</div>',
            '<div class="col-md-6">',
              '<label>Voters ID</label>',
              '<input type="text" ng-model="data.voters_id" class="form-control">',
            '</div>',
          '</div>',
          '<div class="form-group row">',
            '<div class="col-md-6">',
              '<label> Street Name </label> <input type="text" ng-model="data.street" class="form-control">',
            '</div>',
            '<div class="col-md-6">',
              '<label for="district">District</label> <input id="district" type="text" class="form-control" ng-model="data.district" typeahead="district.name for district in $districts($viewValue) | filter:$viewValue" typeahead-wait-ms="500">',
            '</div>',
          '</div>',
          '<div class="form-group row">',
            '<div class="col-md-6"> <label for="barangay">Barangay</label> <input id="barangay" type="text" class="form-control" ng-model="data.barangay" typeahead="barangay.name for barangay in $barangays($viewValue, data.cityMunicipality) | filter:$viewValue" typeahead-wait-ms="500" typeahead-template-url="gc-barangay-typeahead-$template.html"> </div>',
            '<div class="col-md-6"> <label for="cityMunicipality"> City/Municipality </label> <input id="cityMunicipality" type="text" class="form-control" ng-model="data.cityMunicipality" typeahead="city.name for city in $cities($viewValue) | filter:$viewValue" typeahead-wait-ms="500"> </div>',
          '</div>',
          '<hr>',
          '<div class="form-group">',
            '<label> Nickname </label>',
            '<input type="text" class="form-control" ng-model="data.nickname" />',
          '</div>',
          '<div class="form-group">',
            '<label> Gender </label>',
            '<select ng-model="data.gender" class="form-control" ng-init="data.gender = data.gender || \'Male\' ">',
              '<option value="Male">Male</option>',
              '<option value="Female">Female</option>',
            '</select>',
          '</div>',
          '<div class="form-group">',
            '<label> Civil Status </label>',
            '<select ng-model="data.civil_status" class="form-control" ng-init="data.civil_status = data.civil_status || \'Single\'">',
              '<option value="Single">Single</option>',
              '<option value="Married">Married</option>',
              '<option value="Widowed">Widowed</option>',
              '<option value="Divorced">Divorced</option>',
              '<option value="Deceased">Deceased</option>',
            '</select>',
          '</div>',
          '<div class="form-group">',
            '<label> Occupation </label>',
            '<input type="text" class="form-control" ng-model="data.occupation" />',
          '</div>',
          '<div class="form-group row">',
            '<div class="col-md-4"> <label> Birthdate </label> <div class="input-group"> <input class="form-control" ng-model="data.birthdate" datepicker-popup="MM-dd-yyyy" show-weeks="false" is-open="datepickers.birthdate" ng-click="$handleDatepicker($event, \'birthdate\')"> <span class="input-group-addon"><i class="ion-calendar"></i></span> </div> </div>',
            '<div class="col-md-4"> <label> Deceased Date </label> <div class="input-group"> <input class="form-control" ng-model="data.deceased_at" datepicker-popup="MM-dd-yyyy" show-weeks="false" is-open="datepickers.deceased_at" ng-click="$handleDatepicker($event, \'deceased_at\')"> <span class="input-group-addon"><i class="ion-calendar"></i></span> </div> </div>',
            '<div class="col-md-4"> <label> Blacklisted Date </label> <div class="input-group"> <input class="form-control" ng-model="data.blacklisted_at" datepicker-popup="MM-dd-yyyy" show-weeks="false" is-open="datepickers.blacklisted_at" ng-click="$handleDatepicker($event, \'blacklisted_at\')"> <span class="input-group-addon"><i class="ion-calendar"></i></span> </div> </div>',
          '</div>',
          '<div class="form-group row">',
            '<div class="col-md-4"> <label> Mobile #1</label> <input type="text" ng-model="data.mobile" class="form-control"> </div>',
            '<div class="col-md-4"> <label> Mobile #2</label> <input type="text" ng-model="data.mobile_1" class="form-control"> </div>',
            '<div class="col-md-4"> <label> Mobile #3</label> <input type="text" ng-model="data.mobile_2" class="form-control"> </div>',
            '<div class="col-md-4"> <label> Phone # </label> <input type="text" ng-model="data.phone" class="form-control"> </div>',
            '<div class="col-md-4"> <label> Email </label> <input type="email" ng-model="data.email" class="form-control"> </div>',
          '</div>',
          '<div class="form-group row">',
            '<div class="col-md-12"><label> Notes</label> <textarea class="form-control" ng-model="data.notes"></textarea></div>',
          '</div>',
        '</div>'
      ].join('');
    }

    function controllerFn($scope, $http, Restangular) {
      angular.extend($scope, {
        datepickers: {
          birthdate: false,
          deceased_at: false,
          blacklisted_at: false
        },

        $barangays: $barangays,
        $districts: $districts,
        $cities: $cities,
        $handleDatepicker: $handleDatepicker
      });

      $scope.$watch('data.deceased_at', _handleDeceasedAtChange);

      /**
       * @name  openDatePicker
       * @description  A toggle for the datepickers
       * @param $event
       * @param model
       */
      function $handleDatepicker($event, model) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.datepickers[model] = !$scope.datepickers[model];
      }

      function $barangays($viewValue, cityMunicipality) {
        return Restangular.service('barangays')
        .getList({ limit: 6969696969999999, filters: JSON.stringify({ name: $viewValue, 'cityMunicipality.name' : cityMunicipality }) })
        .then(function(response) {
          return response.data;
        });
      }

      function $districts($viewValue) {
      return Restangular.service('districts')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
      }

      function $cities($viewValue) {
          return Restangular.service('citiesmunicipalities')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
      }

      /**
       * Set civil_status to deceased when it changes
       */
      function _handleDeceasedAtChange(changed, old) {
        if ( changed === old ) {
          return;
        }

        if ( changed !== null && changed !== undefined && changed !== '' ) {
          $scope.data.civil_status = 'Deceased';
        }
      }
    }
  }
})(angular);