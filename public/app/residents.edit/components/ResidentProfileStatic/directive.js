+(function(angular, undefined) {
  "use strict";
  angular
    .module('app')
    .directive('gcResidentProfileStatic', directive);

  function directive() {
    return {
      scope: {
        data: '=',
        'edit': '&'
      },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    /*
     * Template
     */
    function templateFn() {
      return [
        '<div class="media-body">',
          '<div class="clearfix">',
            '<div class="pull-left">',
              '<h4 class="media-heading">',
                '{{ data.last_name }}, {{ data.first_name }} {{ data.middle_name }}',
              '</h4>',
              '<span ng-if="!!data.blacklisted_at" class="label label-default" title="Deceased on {{ data.blacklisted_at | date: \'MMM d, yyyy\'}}">',
                'Blacklisted',
              '</span>',
              '<span ng-if="!!data.deceased_at" class="label label-danger" title="Deceased on {{ data.deceased_at | date: \'MMM d, yyyy\' }}">',
                'Deceased',
              '</span>',
              '<span ng-if="!!data.is_voter" class="label label-success">',
                '<i class="ion-checkmark-circled"></i> Voter',
              '</span>',
              '<span ng-if="!data.is_voter" class="label label-warning">',
                '<i class="ion-minus-circled"></i> Not Voter',
              '</span>',
              '<span ng-if="!!data.is_scholar" class="label label-success">',
                '<i class="ion-checkmark-circled"></i> Scholar',
              '</span>',
            '</div>',
            '<button type="button" ng-click="edit()" class="pull-right btn btn-primary" ss-has-permission="profiling.manage_residents"> Edit </button>',
          '</div> ',
          '<h5>{{ data.street }} </h5>',
          '<h5> {{ data.barangay }}, {{ data.cityMunicipality }} </h5>',
          '<h5> {{ data.district }} </h5>',
          '<hr style="margin: 8px auto" />',
          '<div class="row">',
            '<div class="col-md-4">',
              '<div>Nickname: {{ data.nickname }}</div>',
              '<div>Birthdate: {{ data.birthdate | date: \'MMM d, yyyy\' }}</div>',
              '<div>',
                '<span style="margin-right: 4px;">Age : {{ data.birthdate | timestamp | age }}</span>',               
              '</div>',
              '<div>Gender: {{ data.gender }}</div>',
              '<div>Civil Status: {{ data.civil_status }}</div>',
              '<div>Occupation: {{ data.occupation }}</div>',
            '</div>',
            '<div class="col-md-8">',
              '<div>Mobile Number # 1: {{ data.mobile }}</div>',
              '<div>Mobile Number # 2: {{ data.mobile_1 }}</div>',
              '<div>Mobile Number # 3: {{ data.mobile_2 }}</div>',
              '<div>Phone Number: {{ data.phone }}</div>',
              '<div>Email: {{ data.email }}</div>',
              '<div>Voter\'s ID: {{ data.voters_id }}</div>',
            '</div>',
            '<div class="col-md-12">',
              '<div>Notes : {{ data.notes }}</div>',
            '</div>',
          '</div>',
          '<img ng-src="{{ data.signature }}" ng-if="!!data.signature" height="70px" width="320px">',
        '</div>'
      ].join('');
    }

    function controllerFn($scope) { }
  }
})(angular);