+(function(angular, _, undefined) {
  "use strict";
  angular
    .module('app')
    .controller('ResidentsEditCtrl', ResidentsEditCtrl);

  function ResidentsEditCtrl($scope, $stateParams, Resident) {
    var vm = this;

    angular.extend(vm, {
      data: Resident.instance,
      es_id: $stateParams.es_id
    });
  }
})(angular, _);
