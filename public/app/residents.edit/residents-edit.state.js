+(function(angular) {
    function stateConfig($stateProvider) {
        /**
         * Data
         * @type {Object}
         */
        var data = { pageTitle: 'Edit Resident' };
        var modal;

        /**
         * @see  Resident.Repository.get
         * @param  {Object} Resident DI
         * @return promise
         */
        function getResidentByIdResolve(Resident, $state, $stateParams, $http) {
            return Resident
                .Repository
                .get($stateParams.id);
        }

        /**
         * Resolves
         * @type {Object}
         */
        var resolves = { getResidentByIdResolve: getResidentByIdResolve };

        // Activates the modal
        function onEnter($aside, $state, $templateCache) {
            modal = $aside
                .open({
                    controller: 'ResidentsEditCtrl as editCtrl',
                    templateUrl: '/app/residents.edit/residents-edit.html',
                    placement: 'right',
                    backdrop: false
                });

            modal
                .result
                // .then(function() {
                //     $state.go('residents', null);
                // })
                .catch(function() {
                    $state.go('residents', null);
                });
        }

        function onExit() {
            modal.close();
        }

        var state = {
            name: 'residents.edit',
            resolve: resolves,
            url: '/{id}/edit?es_id',
            data: data,
            onEnter: onEnter,
            onExit: onExit
        };

        $stateProvider.state(state);
    }

    angular
        .module('app')
        .config(stateConfig);
})(angular);