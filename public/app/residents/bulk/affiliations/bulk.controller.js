+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('ResidentsBulkAddAffiliationController', controllerFn);

  function controllerFn($scope, $state, Restangular, $http, selectedResolve, flash) {
    angular.extend($scope, {
      data: { affiliation_id: '', referrer_id: '', head_id: '' },
      isLoading: false,
      $bulkAdd: $bulkAdd
    });

    /**
     * Request to the bulk-add end-point
     */
    function $bulkAdd() {
      $scope.isLoading = true;

      Restangular
        .one('residents', selectedResolve)
        .post('affiliations', $scope.data)
        .then(function scc() {
          flash.success = 'Successfully added residents to affiliation.';
          $scope.$dismiss();
        })
        .catch(function err(r, f) { flash.to('validation').error = _.chain(r).values().flatten().value(); })
        .finally(function() { $scope.isLoading = false; });
    }
  }
})(angular);
