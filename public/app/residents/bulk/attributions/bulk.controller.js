+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('ResidentsBulkAddAttributionController', controllerFn);

  function controllerFn($scope, $state, Restangular, $http, selectedResolve, flash) {
    angular.extend($scope, {
      data: {},
      isLoading: false,
      $bulkAdd: $bulkAdd
    });

    /**
     * Request to the bulk-add end-point
     */
    function $bulkAdd() {
      $scope.isLoading = true;

      Restangular
        .one('residents', selectedResolve)
        .post('attributions', $scope.data)
        .then(function scc() {
          flash.success = 'Successfully added residents to attribution.';
          $scope.$dismiss();
        })
        .catch(function err(r, f) { flash.to('validation').error = _.chain(r).values().flatten().value(); })
        .finally(function() { $scope.isLoading = false; });
    }
  }
})(angular);
