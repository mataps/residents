+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('ResidentsBulkAddTransactionController', controllerFn);

  function controllerFn($scope, $state, Restangular, $http, selectedResolve, flash) {
    angular.extend($scope, {
      data: { client_id: '', referrer_id: '' },
      isLoading: false,
      $bulkAdd: $bulkAdd
    });

    /**
     * Request to the bulk-add end-point
     */
    function $bulkAdd() {
      $scope.isLoading = true;

      Restangular
        .one('residents', selectedResolve)
        .post('transactions', $scope.data)
        .then(function scc() {
          flash.success = 'Successfully added residents to affiliation.';
          $scope.$dismiss();
        })
        .catch(function err(r, f) { flash.to('validation').error = _.chain(r).values().flatten().value(); })
        .finally(function() { $scope.isLoading = false; });
    }
  }
})(angular);