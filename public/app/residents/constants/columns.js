+(function(angular, undefined) {
  var columns = [
    {
      key: 'last_name',
      label: 'Last name',
      show: true,
      cellTemplate: '{{:: resident.last_name }}',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },
    {
      key: 'first_name',
      label: 'First name',
      show: true,
      cellTemplate: '{{:: resident.first_name }}',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },
    {
      key: 'middle_name',
      label: 'Maternal name',
      show: true,
      cellTemplate: '{{:: resident.middle_name }}',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },
    {
      key: 'is_voter',
      label: 'Voter',
      show: true,
      cellTemplate: [
        '<span ng-if="!!resident.is_voter" class="label label-success"><i class="ion-checkmark-circled"></i></span>',
        '<span ng-if="!resident.is_voter" class="label label-warning"><i class="ion-minus-circled"></i></span>',
      ].join(''),
      onClick: 'residents.show(resident.id, resident.es_id)'
    },
    // {
    //   key: 'is_scholar',
    //   label: 'Scholar',
    //   show: true,
    //   cellTemplate: [
    //     '<span ng-if="!!resident.is_scholar" class="label label-success">',
    //       '<i class="ion-checkmark-circled"></i> S',
    //     '</span>'
    //   ].join(''),
    //   onClick: 'residents.show(resident.id, resident.es_id)'
    // },
    {
      key : 'deceased_at',
      label: 'Deceased',
      show : true,
      cellTemplate: '<span ng-if="!!resident.deceased_at" class="label label-danger" title="Deceased on {{:: resident.deceased_at | date:\'MMM d, yyyy\'}}">D</span>',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },
    {
      key : 'blacklisted_at',
      label : 'Blacklisted',
      show : true,
      cellTemplate: '<span ng-if="!!resident.blacklisted_at" class="label label-default" title="Deceased on {{:: resident.blacklisted_at | date:\'MMM d, yyyy\'}}">B</span>',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },

    {
      key: 'email',
      label: 'Email',
      show: true,
      cellTemplate: '<span gc-mail-to="{{::resident.email}}"></span>',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },
    {
      key: 'nickname',
      label: 'Nickname',
      show: true,
      cellTemplate: '{{:: resident.nickname }}',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },
    {
      key: 'occupation',
      label: 'Occupation',
      show: true,
      cellTemplate: '{{:: resident.occupation }}',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },
    {
      key: 'mobile',
      label: 'Mobile #1',
      show: false,
      cellTemplate: '{{:: resident.mobile }}',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },
    {
      key: 'mobile_1',
      label: 'Mobile #2',
      show: false,
      cellTemplate: '{{:: resident.mobile_1 }}',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },    
    {
      key: 'mobile_2',
      label: 'Mobile #3',
      show: false,
      cellTemplate: '{{:: resident.mobile_2 }}',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },
    {
      key: 'phone',
      label: 'Phone',
      show: false,
      cellTemplate: '{{:: resident.phone }}',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },
    {
      key: 'precinct',
      label: 'Precinct',
      show: true,
      cellTemplate: '{{:: resident.precinct }}',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },
    {
      key: 'birthdate',
      label: 'Birthdate',
      show: true,
      cellTemplate: '{{:: resident.birthdate | date: \'MMM d, yyyy\' }}',
      field: 'birthdate.from',
      field2: 'birthdate.to',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },
    {
      key: 'gender',
      label: 'Gender',
      show: true,
      cellTemplate: '{{:: resident.gender }}',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },
    {
      key: 'civil_status',
      label: 'Civil status',
      show: false,
      cellTemplate: '{{:: resident.civil_status }}',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },
    {
      key: 'district',
      label: 'District',
      show: true,
      cellTemplate: '{{:: resident.district }}',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },
    {
      key: 'cityMunicipality',
      label: 'Municipality',
      show: true,
      cellTemplate: '{{:: resident.cityMunicipality }}',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },
    {
      key: 'barangay',
      label: 'Barangay',
      show: true,
      cellTemplate: '{{:: resident.barangay }}',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },
    {
      key: 'street',
      label: 'Street',
      show: true,
      cellTemplate: '{{:: resident.street }}',
      onClick: 'residents.show(resident.id, resident.es_id)'
    },
    {
      key: 'notes',
      label: 'Notes',
      show: true,
      cellTemplate: '{{:: resident.notes }}',
      onClick: 'residents.show(resident.id, resident.es_id)'
    }
  ];

  angular
    .module('app')
    .constant('RESIDENTS_COLUMNS', columns);
})(angular);
