+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcBankDropdown', directive);

  function directive() {
    return {
      scope: {
        model: '=ngModel'
      },

      restrict: 'EA',
      template: templateFn
    };

    function templateFn() {
      return [
        '<select ng-model="model" class="form-control">',
          '<option value=""> None </option>',
          '<option value="BDO">BDO </option>',
          '<option value="BPI">BPI </option>',
          '<option value="Chinabank">China Bank </option>',
        '</select>'
      ].join('');
    }
  }
})(angular);