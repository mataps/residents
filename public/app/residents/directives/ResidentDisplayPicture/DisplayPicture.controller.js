+(function(angular, _, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('GCDisplayPictureCtrl', GCDisplayPictureCtrl);

  function GCDisplayPictureCtrl($scope, ssWebcam, FileUploader) {
    var vm = this;

    vm.takePicture = takePicture;
    vm.isLoading = false;

    // ------------------------- //
    // ------------------------- //

    vm.uploader = new FileUploader({
      url: vm.url ? vm.url : 'api/v1/residents',
      method: vm.method ? vm.method.toUpperCase() : 'POST',
      alias: 'profile_image',
      autoUpload: false,
      onAfterAddingFile: onAfterAddingFile,
      onBeforeUploadItem: onBeforeUploadItem,
      onSuccessItem: onSuccessItem,
      onErrorItem: onErrorItem,
      headers: {
        "Accept": 'application/json'
      }
    });

    vm.uploader.filters.push({
      name: 'imageFilter',
      fn: imageFilter
    });

    // ------------------------- //
    // ------------------------- //
    // 
    function onAfterAddingFile(item) {
      vm.webcamImage = undefined;

      if( vm.uploader.queue.length > 1 ) {
        console.log(vm.uploader.queue);
        vm.uploader.removeFromQueue(vm.uploader.queue[0]);
      }
    }

    function onBeforeUploadItem(item) {
      vm.isLoading = true;
      function pushFormData(value, key) {
        var data = {};
        data[key] = value;
        item.formData.push(data);
      }

      angular.forEach(vm.formData, pushFormData);
      console.log(item.formData);
    }

    function onSuccessItem(item, response, status, header) {
      vm.isLoading = false;
      vm.successCallback(response);
      console.log(item);
    }

    function onErrorItem(item, response, status, header) {
      vm.isLoading = false;
      vm.errorCallback({ data: response });
      console.log(item);
    }

    // ------------------------- //
    // ------------------------- //

    function imageFilter(item, options) {
      var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
      return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
    }

    // ------------------------- //
    // ------------------------- //

    function hasImage() {
      return vm.uploader.queue.length !== 0;
    }

    // ------------------------- //
    // ------------------------- //

    function resetImagePreview() {
      vm.webcamImage = undefined;
      vm.uploader.clearQueue();
    }

    function takePicture() {
      ssWebcam.open({
        width: 640,
        height: 480,
        dest_width: undefined,
        dest_height: undefined,
        onDone: function(data_uri) {
          resetImagePreview();
          vm.webcamImage = data_uri;
          vm.formData.profile_image = data_uri;
        }
      });
    };
  }
})(angular, _);