+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .directive('gcDisplayPicture', displayPicture);

  function displayPicture() {
    var template = [
      '<div class="form-group" ng-style="{ opacity: dpCtrl.isLoading ? 0.5 : 1 }">',
        '<label for="profile_image">',
        '<img class="img-thumbnail" ',
          'alt="Resident profile image" ',
          'ng-src="{{ dpCtrl.def }}" ',
          'holder-fix ',
          'fallback-src="fallback.gif" ',
          'ng-hide="!!dpCtrl.uploader.queue.length || !!dpCtrl.webcamImage || !dpCtrl.def">',
        '<img class="img-thumbnail" alt="Resident profile image" src="holder.js/120x120" holder-fix ng-if="!dpCtrl.def">',
        '<div class="img-thumbnail"',
          'ng-repeat="item in dpCtrl.uploader.queue"',
          'ng-thumb="{ file: item._file, width: 120, height: 150 }"',
          'ng-hide="!!dpCtrl.webcamImage">',
        '</div>',
        '<img class="img-thumbnail"',
          'alt="Resident profile image"',
          'ng-src="{{ dpCtrl.webcamImage }}"',
          'ng-show="!!dpCtrl.webcamImage">',
        '</label>',
        '<input id="profile_image" type="file" nv-file-select uploader="dpCtrl.uploader" style="display: none;" ng-disable="dpCtrl.isLoading">',
      '</div>',
      '<div class="form-group">',
        '<button type="button" class="btn btn-info btn-sm" ng-click="dpCtrl.takePicture()" ng-disabled="dpCtrl.isLoading">',
          '<i class="ion-social-instagram-outline"></i>',
          'Take picture',
        '</button>',
      '</div>',
      '<div style="margin-top: -250px;" ng-if="dpCtrl.isLoading">',
        '<three-bounce-spinner></three-bounce-spinner>',
      '</div>'
    ].join('');

    var scope = {
      uploader: '=',
      successCallback: '=',
      errorCallback: '=',
      formData: '=',
      def: '@',
      url: '@',
      method: '@'
    };

    return {
      restrict: 'EA',
      scope: scope,
      // replace: true,
      template: template,
      bindToController: true,
      controllerAs: 'dpCtrl',
      controller: 'GCDisplayPictureCtrl'
    }
  }
})(angular);