+(function(angular, _, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('GCResidentSelectedController', ResidentSelectedController);

  function ResidentSelectedController($scope) {
    var vm = this;

    angular.extend(vm, {
      $hasSelected: $hasSelected
    })

    /**
     * Checks if "vm.model" is existent
     * @return {Boolean}
     */
    function $hasSelected() {
      return (
        !angular.isUndefined(vm.model) &&
        _.size(vm.model)
      );
    }
  }
})(angular, _);