+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .directive('gcResidentSelected', ResidentSelected);

  function ResidentSelected() {
    return {
      restrict: 'EA',
      scope: { model: '=' },
      templateUrl: 'gc-resident-selected.html',
      bindToController: true,
      controllerAs: 'selectedCtrl',
      controller: 'GCResidentSelectedController'
    };
  }
})(angular);