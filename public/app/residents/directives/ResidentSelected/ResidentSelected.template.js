+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .run(runBlock);

  function runBlock($templateCache) {
    var template = [
      '<section class="clearfix form-group">',
        // Filler
        '<div ng-if="!selectedCtrl.$hasSelected()">',
          '<div class="outer-table">',
            '<div class="inner-table">',
              '<h1 style="font-size: 160px; margin-bottom: -40px;"> <small> <i class="ion-ios7-information"></i> </h1>',
              '<h2> <small> Select a resident to view! </small> </h2>',
            '</div>',
          '</div>',
        '</div>',

        // Actual
        '<div ng-if="!!selectedCtrl.$hasSelected()">',
          '<img ng-src="{{ selectedCtrl.model.profilePic }}" ng-if="!!selectedCtrl.model.profilePic" fallback-src="fallback.gif" holder-fix class="pull-left" style="border-radius: 50%; margin-top: 10px;" height="72" width="72">',
          '<img src="holder.js/72x72" ng-if="!selectedCtrl.model.profilePic" holder-fix class="pull-left" style="border-radius: 50%; background: #ddd; margin-top: 10px;" height="72" width="72">',
          '<div class="pull-left" style="margin-left: 5px;">',
            '<h4 ng-bind="selectedCtrl.model.full_name" style="margin-bottom: 0px;"></h4>',

            '<span ng-if="!!selectedCtrl.model.blacklisted_at" class="label label-default" title="Deceased on {{:: selectedCtrl.model.blacklisted_at | date: \'MMM d, yyyy\'}}">',
              'Blacklisted',
            '</span>',

            '<span ng-if="!!selectedCtrl.model.deceased_at" class="label label-danger" title="Deceased on {{:: selectedCtrl.model.deceased_at | date: \'MMM d, yyyy\'}}">',
              'Deceased',
            '</span>',

            '<span ng-if="!!selectedCtrl.model.is_voter" class="label label-success">',
              '<i class="ion-checkmark-circled"></i> Voter',
            '</span>',

            '<span ng-if="!selectedCtrl.model.is_voter" class="label label-warning">',
              '<i class="ion-minus-circled"></i> Not Voter',
            '</span>',

            '<h6> {{ selectedCtrl.model.barangay + (!!selectedCtrl.model.barangay ? \',\' : \'\') }} {{ selectedCtrl.model.cityMunicipality }} </h6>',
            '<table class="table table-striped">',
            '<tr><td>Birthdate</td><td>{{ selectedCtrl.model.birthdate }}</td></tr>',
            '<tr><td>Occupation</td><td>{{ selectedCtrl.model.occupation }}</td></tr>',
            '<tr><td>Gender</td><td>{{ selectedCtrl.model.gender }}</td></tr>',
            '<tr><td>Civil Status</td><td>{{ selectedCtrl.model.civil_status }}</td></tr>',
            '<tr><td>Nickname</td><td>{{ selectedCtrl.model.nickname }}</td></tr>',
            '</table>',
            '<div class="panel panel-default">',
              '<div class="panel-heading">',
                '<span class="panel-title">Contact Information</span>',
              '</div>',
              '<div class="panel-body">',
                '<table class="table">',
                    '<tr><td>Mobile #1</td><td>{{ selectedCtrl.model.mobile }}</td></tr>',
                    '<tr><td>Mobile #1</td><td>{{ selectedCtrl.model.mobile_1 }}</td></tr>',
                    '<tr><td>Mobile #2</td><td>{{ selectedCtrl.model.mobile_2 }}</td></tr>',
                    '<tr><td>Email</td><td>{{ selectedCtrl.model.email }}</td></tr>',
                '</table>',
              '</div>',
            '</div>',
            '<a href="#" class="pull-right">View More</a>',  
          '</div>',
        '</div>',
      '</section>'
    ].join('');

    $templateCache.put(
      'gc-resident-selected.html',
      template
    );
  }
})(angular);