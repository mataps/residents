+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcResidentSibling', directive);

  function directive() {
    return {
      scope: {
        id: '='
      },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    function controllerFn($scope, $http) {
      var $s = $scope; // Shorthand

      angular.extend($s, {
        loading: true,
        siblings: []
      });

      $request();

      function $request() {
        var id = $scope.id;
        var url = ['api/v1/residents/',id,'/relatives'].join('');
        var filter = { filters: { relative: 'sibling' } };

        return $http.get(url /*,{ params: filter }*/)
          .then(function(response) { $s.siblings = response.data.data; })
          .finally(function() { $s.loading = false });
      }

      $scope.$watch('id', function(changed, old) {
        if ( changed !== old && !loading ) {
          $request();
        }
      })
    }

    function templateFn(element, attributes) {
      return [
        '<div class="box">',
          '<div class="body" style="height: {{ !!resident.length ? \'300px\' : \'auto\' }}; overflow-x: scroll; overflow-y: hidden">',
            '<div ng-if="loading">',
              '<three-bounce-spinner></three-bounce-spinner>',
            '</div>',

            '<div ng-if="!loading">',
              '<h4 class="text-center" ng-if="!siblings.length"> <small> This scholar has no siblings. </small> </h4>',
              '<div class="row" ng-if="!!siblings.length">',
                '<div class="col-md-6" style="height: 135px; margin-bottom: 10px;" ng-repeat="resident in siblings">',
                  '<div class="pull-left" style="border-radius: 50%; margin-right: 10px;">',
                    '<img ng-src="{{ resident.profilePic }}" style="height: 72px; width: 72px; border-radius: 50%;" fallback-src="fallback.gif" ng-if="!!resident.profilePic" />',
                    '<img src="holder.js/72x72" ng-if="!resident.profilePic" holder-fix style="border-radius: 50%;" />',
                  '</div>',

                  '<div class="pull-left">',
                    '<h4> {{:: resident.full_name }} </h4>',

                    '<span ng-if="!!form.data.blacklisted_at" class="label label-default" title="Deceased on {{:: form.data.blacklisted_at | date: \'MMM d, yyyy\'}}">',
                      'Blacklisted',
                    '</span>',

                    '<span ng-if="!!form.data.deceased_at" class="label label-danger" title="Deceased on {{:: form.data.deceased_at | date: \'MMM d, yyyy\'}}">',
                      'Deceased',
                    '</span>',

                    '<span ng-if="!!form.data.is_voter" class="label label-success">',
                      '<i class="ion-checkmark-circled"></i> Voter',
                    '</span>',

                    '<span ng-if="!form.data.is_voter" class="label label-warning">',
                      '<i class="ion-minus-circled"></i> Not Voter',
                    '</span>',
                  '</div>',
                '</div>',
              '</div>',
            '</div>',
          '</div>',
        '</div>'
      ].join('');
    }
  }
})(angular);