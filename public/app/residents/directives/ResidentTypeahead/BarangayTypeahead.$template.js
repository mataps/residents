+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .run(runBlock);

  function runBlock($templateCache) {
    var template = [
      '<a>',
        '<h5 bind-html-unsafe="match.model.name | typeaheadHighlight:query" style="margin-top: 3px; margin-bottom: 2px;"></h5>',
        '<h5 style="margin-bottom: 3px; margin-top: 2px;"><small>{{ match.model.cityMunicipality.name }}</small></h5>',
      '</a>'
    ].join('');

    $templateCache.put(
      'gc-barangay-typeahead-$template.html',
      template
    );
  }
})(angular);
