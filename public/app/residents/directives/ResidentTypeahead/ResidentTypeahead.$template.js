+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .run(runBlock);

  function runBlock($templateCache) {
    var template = [
      '<a>',
        '<div class="row">',
          '<div class="col-md-2" style="padding: 0px;">',
            '<img class="pull-left" ng-src="{{ (match.model.profilePic ? match.model.profilePic : \'\' ) }}-sm" holder-fix fallback-src="fallback.gif" style="border-radius: 50%; background: #ddd; margin-top: 10px;" height="50" width="50">',
          '</div>',

          '<div class="col-md-9">',
            '<span bind-html-unsafe="match.model.full_name | typeaheadHighlight:query">',
            '</span><br>',
            '<span style="font-size: 9px; font-style: italic;">',
              '{{ match.model.street }}, {{ match.model.barangay }}',
            '</span>&nbsp;',
            '<span style="font-size: 9px; font-style: italic;">',
              '{{ match.model.cityMunicipality }}',
            '</span><br>',
            '<span ng-if="!!match.model.is_voter" class="label label-success"><i class="ion-checkmark-circled"></i>Voter</span>',
            '<span ng-if="!match.model.is_voter" class="label label-warning"><i class="ion-minus-circled"></i>Non-Voter</span>',
          '</div>',
        '</div>',
      '</a>'
    ].join('');

    $templateCache.put(
      'gc-resident-typeahead-$template.html',
      template
    );
  }
})(angular);