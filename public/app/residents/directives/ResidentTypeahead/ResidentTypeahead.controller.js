+(function(angular, _, undefined) {
  angular
    .module('app')
    .controller(
      'GCResidentTypeaheadController',
      ResidentTypeaheadController
    );

  function ResidentTypeaheadController($scope, Resident, Restangular, $http) {
    //
    var vm = this;
    var transformItem = vm.transformItem || angular.noop;
    var preCallback = vm.preCallback || angular.noop;
    var postCallback = vm.postCallback || angular.noop;

    transform();

    vm._barangay = {};
    vm.request = request;
    vm.selectCb = selectCb;

    /**
     * [resident, description]
     * @return {[type]} [description]
     */
    function request(name) {
      console.log(vm.barangay, vm._barangay);
      var filters = { filters: JSON.stringify(angular.extend(
        { full_name: name },
        (vm.barangay == undefined || vm.barangay == '' || vm._barangay.id == undefined ? {} : { barangay: vm.barangay }),
        (vm.cityMunicipality == undefined || vm.cityMunicipality == '' ? {} : { cityMunicipality: vm.cityMunicipality })
      )) };

      return Restangular
        .service('residents')
        .getList(filters)
        .then(function(response) {
          return response.data;
        });
    }

    function selectCb($item, $model, $label) {
      // Transform
      transformItem($item);
      // Pre phase
      preCallback($item);

      vm.idVariable = $item.id;

      if ( !angular.isUndefined(vm.collectionVariable) ) {
        vm.collectionVariable = $item;
      }

      // Post phase
      postCallback($item);
    }

    /** Transform data */
    function transform() {
      if ( !angular.isUndefined(vm.transformIdVariable) ) {
        vm.idVariable = vm.transformIdVariable;
      }

      if ( !angular.isUndefined(vm.transformCollectionVariable) ) {
        vm.collectionVariable = angular.copy(vm.transformCollectionVariable);
      }
    }

    vm.citiesMunicipalities = function($viewValue) {
      return $http.get('/api/v1/citiesmunicipalities?limit=69696969696969696969&filters=' + JSON.stringify({ name: $viewValue }) )
        .then(function(response) {
          return response.data.data;
        });
    }

    vm.barangays = function($viewValue) {
      return $http.get('/api/v1/barangays?limit=69696969696969696969&filters=' +
        JSON.stringify(angular.extend({ name: $viewValue, }, vm.cityMunicipality == undefined ? {} : { 'cityMunicipality.name': vm.cityMunicipality })))
        .then(function(response) {
          return response.data.data;
        });
    }
  }
})(angular, _);
