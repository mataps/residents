+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .directive('gcResidentTypeahead', gcResidentTypeahead);

  function gcResidentTypeahead($compile) {
    var scope = {
      model: '=ngModel',
      idVariable: '=',
      collectionVariable: '=',
      transformIdVariable: '@',
      transformCollectionVariable: '=',
      placeholder: '@inputPlaceholder',
      transformItem: '=',
      preCallback: '=',
      postCallback: '='
    };

    return {
      scope: scope,
      // templateUrl: 'gc-resident-typeahead-template.html',
      template: templateFn,
      bindToController: true,
      controllerAs: 'typeahead',
      controller: 'GCResidentTypeaheadController',
      replace: true
    };

    function templateFn(element, attributes) {
      var placeholder = attributes.placeholder || 'Enter resident name..';

      return [
        '<div class="row">',
          '<div class="col-md-6" style="padding-right: 0px;">',
            '<input type="text" class="form-control" ',
              'placeholder="Enter resident name.." ',
              'ng-model="typeahead.model" ',
              'typeahead-wait-ms="500" ',
              'typeahead="resident.full_name for resident ',
                'in typeahead.request($viewValue)" ',
              'typeahead-wait-ms="50" ',
              'typeahead-template-url="gc-resident-typeahead-$template.html" ',
              'typeahead-on-select="typeahead.selectCb($item, $model, $label)">',
          '</div>',

          '<div class="col-md-3" style="padding-left: 0px; padding-right: 0px;">',
            '<input type="text" class="form-control" ',
              'placeholder="Filter resident by barangay.." ',
              'ng-model="typeahead.barangay" ',
              'typeahead-wait-ms="500" ',
              'typeahead-template-url="gc-barangay-typeahead-$template.html" ',
              'typeahead-on-select="typeahead._barangay = $item" ',
              'typeahead="barangay.name for barangay ',
                'in typeahead.barangays($viewValue)">',
          '</div>',

          '<div class="col-md-3" style="padding-left: 0px;">',
            '<input type="text" class="form-control" ',
              'placeholder="Filter resident by city.." ',
              'ng-model="typeahead.cityMunicipality" ',
              'typeahead-wait-ms="500" ',
              'typeahead-on-select="vm.barangayOnSelect($item)" ',
              'typeahead="city.name for city ',
                'in typeahead.citiesMunicipalities($viewValue)">',
          '</div>',
        '</div>'
      ].join('');
    }
  }
})(angular);
