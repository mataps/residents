+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .run(runBlock);

  function runBlock($templateCache) {
    var template = [
      '<div class="row">',
        '<div class="col-md-4">',
          '<input type="text" class="form-control" ',
            'placeholder="Enter resident name.." ',
            'ng-model="typeahead.model" ',
            'typeahead-wait-ms="500" ',
            'typeahead="resident.full_name for resident ',
              'in typeahead.request($viewValue)" ',
            'typeahead-template-url="gc-resident-typeahead-$template.html" ',
            'typeahead-on-select="typeahead.selectCb($item, $model, $label)">',
        '</div>',

        '<div class="col-md-4">',
          '<input type="text" class="form-control" ',
            'placeholder="Filter resident by barangay.." ',
            'ng-model="typeahead.barangay" ',
            'typeahead-wait-ms="500" ',
            'typeahead-on-select="typeahead._barangay = $item" ',
            'typeahead="barangay.name for barangays in typeahead.barangays($viewValue)">',
        '</div>',

        '<div class="col-md-4">',
          '<input type="text" class="form-control" ',
            'placeholder="Filter resident by city.." ',
            'ng-model="typeahead.cityMunicipality" ',
            'typeahead-wait-ms="500" ',
            'typeahead="city.name for cities in typeahead.citiesMunicipalities($viewValue)">',
        '</div>',
      '</div>'
    ].join('');

    $templateCache.put(
      'gc-resident-typeahead-template.html',
      template
    );
  }
})(angular);
