+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('ResidentsCtrl', ResidentsCtrl);

  function ResidentsCtrl($scope, $http,  $controller, UtilService, RESIDENTS_COLUMNS, Resident, Restangular, flash, $state, $window, $modal) {
    // Inherit BaseIndexCtrl's prototype,
    var controller = $controller('BaseIndexCtrl', {
        $scope: $scope,
        $state: $state,
        UtilService: UtilService
    });

    angular.extend(this, controller);

    // ------------------------- //
    // ------------------------- //
    //
    // viewmodel
    var vm = this,
       // Data collection (grabbed from the service)
      collection = [];

    // Copy items from the service
    // to the local collection variable
    angular.extend(collection, Resident.collection);

    // Initialize vm data
    vm.columns = RESIDENTS_COLUMNS;
    vm.collection = collection;

    // Manually bind the "this"
    // from the base controller
    vm.callServer = angular.bind(vm, vm.callServer);
    vm.applyPageData = angular.bind(vm, vm.applyPageData);
    vm.catchPageDataError = angular.bind(vm, vm.catchPageDataError);

    // Commands that contact the API / server
    vm.getSelected = angular.bind(vm, vm.getSelected);
    vm.remove = remove;
    vm.getPageData = getPageData;
    vm.show = show;
    vm.$report = $report;
    vm.$addToAffiliation = $addToAffiliation;
    vm.$addToAttribution = $addToAttribution;
    vm.$addToTransaction = $addToTransaction;
    vm.loadAttribute = loadAttribute;
    vm.$f = $f;
    // Residents
    Restangular.service('affiliations')
      .getList({ limit: 69996969696969696 })
      .then(function(response) {
        vm.affiliations = response.data;
      });

    /**
     * @description  Sends a DELETE to the server
     * @return Restangular
     */
    function remove() {
      var selected = vm.getSelected().join(',');

      if ( !selected.length ) {
        flash.error = 'Please select a resident.';
        return;
      }

      if( !confirm('Please confirm deleting the selected records.'))
        return;

      vm.load();

      return Resident
        .Command
        .delete(selected)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    };

    vm.QQ = {};

    /**
     * Fetches data and replaces the current
     * @param  string query
     * @return void
     * &filters={"birthdate": { "from": 'asdasd': "to"}}
     */
    function getPageData(query) {
      // if ( query.page ) {
      //   // console.log(query);
      //   // console.log(vm.QQ);
      //   angular.extend(vm.QQ, query);
      //   console.log(vm.QQ);
      // } else {
      //   vm.QQ = query;
      //   console.log(vm.QQ);
      // }

      if ( !query.filters ) {
        query.filters = angular.copy(query);
      }

      if ( query.page ) {

      }

      // if ( !angular.isUndefined(query.filters) &&
      //   ( !angular.isUndefined(query.filters['birthdate.to']) || !angular.isUndefined(query.filters['birthdate.from']) )
      // ) {
      //   if ( !angular.isUndefined(vm.QQ.filters.birthdate) ) {
      //     angular.extend(vm.QQ.filters, query.filters);
      //     vm.QQ.filters = _.omit(vm.QQ.filters, 'birthdate.to');
      //     vm.QQ.filters = _.omit(vm.QQ.filters, 'birthdate.from');
      //   }
      // }

      if ( query.page ) {
        query.filters = angular.copy(vm.QQ.filters);
      }

      if ( query.filters && ! query.filters['affiliations'] ) {
        query.filters['affiliations'] = vm.QQ['affiliations'];
      }

      if ( query.filters && ! query.filters['attributions'] ) {
        query.filters['attributions'] = vm.QQ['attributions'];
      }

      vm.QQ.filters = angular.copy(query.filters);

      if ( query.filters.page ) {
        query.filters = _.omit(query.filters, 'page');
      }

      if ( query.filters.limit ) {
        query.filters = _.omit(query.filters, 'limit');
      }

      if ( query.filters.sort ) {
        query.filters = _.omit(query.filters, 'sort');
      }

      return Resident
        .Query
        .all(query )
        .then(vm.applyPageData)
        .catch(vm.catchPageDataError);
    }

    vm.onQueryChange = onQueryChange;
    function onQueryChange() {
      // console.log(vm._selected_affiliation);
      // console.log(vm._selected_attributes);
      vm.QQ['attributions'] = vm._selected_attributes;
      vm.QQ['affiliations'] = vm._selected_affiliation;

      if ( vm.QQ.filters ) {
        vm.QQ.filters['affiliations'] = vm._selected_affiliation;
        vm.QQ.filters['attributions'] = vm._selected_attributes;
      }

      if ( vm.QQ['affiliations'] === 'ALL') {
        vm.QQ['affiliations'] = undefined;

        if ( vm.QQ.filters ) {
          vm.QQ.filters['affiliations'] = undefined;
        }
      }

      getPageData(vm.QQ);
    }

    // Success XHR
    function successXHRCallback(response) {
      var message = 'Resident has been successfully archived!';
      flash.success = message;
      $state.reload();
      vm.load(false);
    }

    // Error XHR
    function errorXHRCallback(response) {
      var message = 'An error has occured. Please try again..';
      flash.to('validation').error = message;
      vm.load(false);
    }

    function show(id, es_id) {
      $state.go('residents.edit', { id: id, es_id: es_id });
    }

    function $report(type) {
      var _filter_ = angular.copy(vm.QQ.filters);

      ['barangay', 'cityMunicipality', 'district']
        .forEach(function(field) {
          _filter_[field + '.name'] = _filter_[field];
          _filter_ = _.omit(_filter_, field);
        });

      $modal.open({
        templateUrl: 'app/residents/reports/reports.html',
        controller: 'ResidentReportController',
        resolve: {
          typeResolve: function() {
            return type;
          },
          columnsResolve: function() {
            return vm.columns;
          },
          filterResolve: function() {
            return _filter_;
          }
        }
      });
    }

    function $addToAffiliation() {
      var selected = vm.getSelected().join(',');

      if ( !selected.length ) {
        flash.error = 'Please select (a) resident(s) to be bulk added to an affiliation.';
        return;
      }

      $modal.open({
        templateUrl: '/app/residents/bulk/affiliations/bulk.html',
        controller: 'ResidentsBulkAddAffiliationController',
        resolve: { selectedResolve: function() { return selected; }}
      })
    }

    function $addToAttribution() {
      var selected = vm.getSelected().join(',');

      if ( !selected.length ) {
        flash.error = 'Please select (a) resident(s) to be bulk added to an attribution.';
        return;
      }

      $modal.open({
        templateUrl: '/app/residents/bulk/attributions/bulk.html',
        controller: 'ResidentsBulkAddAttributionController',
        resolve: { selectedResolve: function() { return selected; }}
      })
    }

    function $addToTransaction() {
      var selected = vm.getSelected().join(',');

      if ( !selected.length ) {
        flash.error = 'Please select (a) resident(s) to be bulk added to a transaction.';
        return;
      }

      $modal.open({
        templateUrl: '/app/residents/bulk/transactions/bulk.html',
        controller: 'ResidentsBulkAddTransactionController',
        resolve: { selectedResolve: function() { return selected; }}
      })
    }


    function loadAttribute(query) {
      return $http.get('/api/v1/attributions/search/?filters=' + JSON.stringify({ name: query }))
          .then(function(response) {
          console.log(response);
          return response.data;
        });
        ;
    }

    function $f() {
      console.log('Called');
    }
  }
})(angular);