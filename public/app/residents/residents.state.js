+(function(angular, undefined) {
  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var data = { pageTitle: 'Manage residents' };
    var resolves = { getResidentsCollectionResolve: getResidentsCollectionResolve };
    
    var state = {
      parent: 'main',
      name: 'residents',
      url: '/residents',
      data: data,
      resolve: resolves,
      controllerAs: 'residents',
      controller: 'ResidentsCtrl',
      templateUrl: '/app/residents/residents.html'
      //views: {
      //  'tools': { templateUrl: '/app/residents/partials/tools.html', controller: '' },
      //  'content': { templateUrl: '/app/residents/residents.html', controller: 'ResidentsCtrl as residents' }
      //}
    };
    
    $stateProvider.state(state);
  }

  function getResidentsCollectionResolve(Resident) {
    return Resident
      .Repository
      .all();
  };
})(angular);