+(function(angular, undefined) {
  function AllowancePoliciesCreateCtrl($scope, $state, AllowancePolicy, flash, $state) {
    var vm = this,
      form = {
        data: {}
      };

    // ------------------------- //
    // ------------------------- //
    vm.form = form;
    vm.create = create;

    ////////////////////////////////
    // Todo loading
    ////////////////////////////////

    /**
     * @description  Sends an XHR to the server with
     *               the form data for create a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    function create(formData) {
      return AllowancePolicy
        .Command
        .create(formData)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    }

    function successXHRCallback(response) {
      flash.success = 'Policy has been successfully stored!';

      $state.go('scholarship-allowancePolicies', null, { reload: true });
    }

    function errorXHRCallback(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }
  }

  angular
    .module('app')
    .controller('AllowancePoliciesCreateCtrl', AllowancePoliciesCreateCtrl);
})(angular);