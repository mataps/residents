+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'New Allowance Policy' };
    var modal;
    
    // 
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('scholarship-allowancePolicies');
      }
        
      modal = $modal
        .open({
          size: 'sm',
          controller: 'AllowancePoliciesCreateCtrl as createCtrl',
          templateUrl: '/app/scholarships-allowancePolicies.create/policy-create.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }
    

    var state = {
      url: '/create',
      name: 'scholarship-allowancePolicies.create',
      data: data,
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);