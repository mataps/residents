+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('AllowancePoliciesCtrl', AllowancePoliciesCtrl);

  function AllowancePoliciesCtrl($scope, $controller, $state, UtilService, AllowancePolicy, ALLOWANCE_POLICIES_COLUMNS) {
    // Inherit BaseIndexCtrl's prototype,
    var controller = $controller('BaseIndexCtrl', {
      $scope: $scope,
      $state: $state,
      UtilService: UtilService
    });

    angular.extend(this, controller);

    // ------------------------- //
    // ------------------------- //

    // viewmodel
    var vm = this,    
       // Data collection (grabbed from the service)
      collection = [];

    // Copy items from the service
    // to the local collection variable
    angular.extend(collection, AllowancePolicy.collection);

    // ------------------------- //
    // ------------------------- //

    // Initialize vm data
    vm.columns = ALLOWANCE_POLICIES_COLUMNS;
    vm.collection = collection;

    // Manually bind the "this"
    // from the base controller
    vm.callServer = angular.bind(vm, vm.callServer);
    vm.applyPageData = angular.bind(vm, vm.applyPageData);
    vm.catchPageDataError = angular.bind(vm, vm.catchPageDataError);

    // Commands that contact the API / server
    vm.remove = remove;
    vm.getPageData = getPageData;

    /**
     * @description  Sends a DELETE to the server
     * @see  getPageData
     * @return void
     */
    function remove() {
      if(!confirm('Please confirm deleting the selected records.'))
        return;

      var selected = vm.getSelected();
    }

    /**
     * Fetches data and replaces the current
     * @see  AllowancePolicy.Query.all()
     * @param  string query
     * @return void
     */
    function getPageData(query) {
      return AllowancePolicy
        .Query
        .all(query)
        .then(vm.applyPageData)
        .catch(vm.catchPageDataError);
    }
  }
})(angular);