+(function(angular, undefined) {
  angular
    .module('app')
    .config(stateConfig);
  function stateConfig($stateProvider) {

    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Manage Allowance Policies' };

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = { getAllowancePoliciesCollectionResolve: getAllowancePoliciesCollectionResolve };

    var state = {
      parent: 'main',
      name: 'scholarship-allowancePolicies',
      url: '/scholarships/allowance-policies',
      data: data,
      resolve: resolves,
      controllerAs: 'policies',
      controller: 'AllowancePoliciesCtrl',
      templateUrl: '/app/scholarships-allowancePolicies/allowance-policies.html'
    };

    $stateProvider.state(state);
  }

  /**
   * @description Fetches all scholarship
   * @param  Restangular
   * @return Restangular
   */
  function getAllowancePoliciesCollectionResolve(AllowancePolicy) {
    return AllowancePolicy
      .Repository
      .all();
  }
})(angular);