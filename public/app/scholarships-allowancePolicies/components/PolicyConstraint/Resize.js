+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcResize', directive);

  function directive() {
    return {
      scope: {
        mousedown: '&',
        mousemove: '&',
        active: '=',
        first: '=',
        last: '=',
        index: '='
      },

      restrict: 'EA',
      link: linkFn
    };

    function linkFn(scope, element, attributes) {
      var last = scope.last;
      var first = scope.first;
      var index = scope.index;

      init();

      var x = 0;
      var y = 0;
      var startX = 0;
      var startY = 0;
      
      function $mousedown(evt) {
        // scope.mousedown({ evt: evt, index: index, first: first, last: last });
        evt.preventDefault();
        
        element.on('mousemove', $mousemove);
        element.on('mouseup', $mouseup);
      }

      function $mousemove(evt) {
        y = evt.pageY - startY;
        x = evt.pageX - startX;

        console.log(x, y);
      }

      function $mouseup(evt) {
        element.off('mousemove', $mousemove);
        element.off('mouseup', $mouseup);
      }

      function init() {
        if ( !first ) {
          element.append( _template('left') );
        }

        if ( !last ) {
          element.append( _template('right') );
        }

        element.on('mousedown', $mousedown);
        // element.on('mouseup', $mouseup);
      }

      function _template(dir) {
        return [
          '<div style="display: table; ',
            'height: 100%; ',
            'width: 12px; ',
            'cursor: ew-resize; ',
            'position: absolute; ',
            dir,
            ' :0;',
            'top: 0;',
            '">',
          '</div>'
        ].join('');
      }
    }
  }
})(angular);