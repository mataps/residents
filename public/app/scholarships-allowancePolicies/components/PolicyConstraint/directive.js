+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .directive('gcAllowanceConstraint', directive);

  function directive() {
    return {
      scope: {
        data: '=',
        type: '@',
        level: '@',
        type_id: '@',
        graph: '='
      },

      restrict: 'EA',
      link: linkFn,
      template: templateFn,
      controller: controllerFn
    };

    /**
     * Template
     */
    function templateFn(element, attributes) {
      return [
        '<section style="position: relative;">',

          '<div style="height: 40px; background: #ddd; overflow: hidden; border-radius: 3px; position: relative;" id="_bar">',
            '<div style="height: 40px; width: {{ $ppp(policy.to, policy.from, $first, $last) }}; background: {{ colors[$index] }}; display: block; position: absolute; left: {{ $position(policy.from, $first) }}" ng-repeat="policy in graph">', // gc-resize callback="$mousedown(evt, index, first, last)" active="true" first="$first" last="$last" index="$index">',
              '<small style="text-align: center; margin-top: 13px; display: block; color: #fff;">{{ policy.amount }}</small>',
            '</div>',
          '</div>',

          '<aside style="text-align: center; left: 0; right: 0; display: block; position: absolute; height: 40px; background: rgba(0, 0, 0, 0.225); opacity: 0; left: 0; top: 0;" id="_bar-control">',
            '<div style="margin-top: 5px;">',
              '<button type="button" ng-click="$edit()" class="btn btn-sm btn-primary" ng-if="!isEditing">',
                '<i class="ion-edit"></i>',
              '</button>',

              '<div ng-if="isEditing">',
                '<div ng-if="!isLoading">',
                  '<button type="button" ng-click="$cancel()" class="btn btn-sm btn-danger" ng-disabled="isLoading">',
                    '<i class="ion-close-round"></i>',
                  '</button>',

                  '<button type="button" ng-click="$request()" class="btn btn-sm btn-success" style="margin-left: 5px;" ng-disabled="isLoading">',
                    '<i class="ion-checkmark"></i>',
                  '</button>',

                  '<button type="button" ng-click="$add()" class="btn btn-sm btn-primary" style="margin-left: 20px;" ng-disabled="isLoading">',
                    '<i class="ion-plus"></i>',
                  '</button>',

                  '<button type="button" ng-click="$remove()" class="btn btn-sm btn-primary" style="margin-left: 5px;" ng-disabled="isLoading">',
                    '<i class="ion-android-remove"></i>',
                  '</button>',
                '</div>',

                '<div ng-if="isLoading">',
                  '<div class="three-bounce-spinner" style="margin-left: auto; margin-right: auto; margin-top: 10px;">',
                    '<div class="bounce1" style="background: #fff;"></div>',
                    '<div class="bounce2" style="background: #fff;"></div>',
                    '<div class="bounce3" style="background: #fff;"></div>',
                  '</div>',
                '</div>',
              '</div>',
            '</div>',
          '</aside>',

          // Measure //
          '<aside style="height: 15px;">',
            '<small style="left: 0; position: absolute;"> 50 </small>',
            '<small style="left: 0; right: 0; margin-left: auto; text-align:center; position: absolute;">75</small>',
            '<small style="right: 0; position: absolute;"> 100 </small>',
          '</aside>',

          '<div ng-if="isEditing" style="margin-top: 20px;"">',
            '<div class="row form-group">',
              '<div class="col-md-4"><label>From</label></div>',
              '<div class="col-md-4"><label>To</label></div>',
              '<div class="col-md-4"><label>Amount</label></div>',
            '</div>',
            '<div class="row form-group" style="margin-bottom: 2px;" ng-repeat="policy in graph">',
              '<div class="col-md-4">',
                '<div style="width: 5px; display: inline-block; background: {{ colors[$index % 4] }}; height: 100%; position: absolute;"></div>',
                '<input type="text" ng-model="policy.from" placeholder="From.." class="form-control" ng-disabled="isLoading">',
              '</div>',

              '<div class="col-md-4">',
                '<input type="text" ng-model="policy.to" placeholder="To.." class="form-control" ng-disabled="isLoading">',
              '</div>',

              '<div class="col-md-4">',
                '<input type="text" ng-model="policy.amount" placeholder="1000.00" class="form-control" ng-disabled="isLoading">',
              '</div>',
            '</div>',
          '</div>',
        '</section>'
      ].join('');
    }

    /**
     * Controller
     */
    function controllerFn($scope, flash, $http) {
      angular.extend($scope, {
        // Orange, Blue, Pink, Green, Violet
        old: {},
        colors: ['#F66B2A', '#35C5FA', '#FA355A', '#1A7A3D', '#7A4BA0'],
        isEditing: false,
        isLoading: false,

        $request: $request,
        $cancel: $cancel,
        $edit: $edit,
        $add: $add,
        $remove: $remove
      });

      function $request() {
        $scope.isLoading = true;

        return $http.post('api/v1/policies', $scope.data)
          .then(function() {
            flash.success = 'Policy has been updated!';
            $scope.isEditing = false;
          })
          .catch(function(response) {
            flash.to('validation').error = _.chain(response.data.data)
              .values()
              .flatten()
              .value();
          })
          .finally(function() { $scope.isLoading = false; });
      }

      function $cancel() {
        $scope.graph = angular.copy($scope.old);
        $scope.isEditing = false;
      }

      function $edit() {
        $scope.old = angular.copy($scope.graph);
        $scope.isEditing = true;
      }

      function $remove() {
        var LIMIT = 10;
        var graph = $scope.graph;
        var length = graph.length;
        var last = length - 1;

        if ( length == 1 ) {
          return;
        } else if ( length > 1 ) {
          graph.splice(last, 1);
          graph[last - 1].to = 100;
        }
      }

      function $add() {
        var LIMIT = 10;
        var graph = $scope.graph;

        if(graph == undefined || graph == null){
          $scope.graph = [];
          var graph = $scope.graph;
          graph.push({
            from: 50,
            to: 100,
            amount: '1000.00',
            level: $scope.level,
            type: $scope.type,
            scholarship_type_id: $scope.type_id
          });
        }
        else{
        var last = graph[graph.length - 1];

        if ( graph.length == 1 ) {
          last.to = 75;
          graph.push({
            from: 75,
            to: 100,
            amount: '1000.00',
            level: graph[0].school_level,
            type: graph[0].school_type,
            scholarship_type_id: graph[0].scholarship_type_id
          });
        } else if ( graph.length > 1 ) {
          last.to -= 10;
          graph.push({
            from: 90,
            to: 100,
            amount: '1000.00',
            level: graph[0].school_level,
            type: graph[0].school_type,
            scholarship_type_id: graph[0].scholarship_type_id
          });
        } else {
          graph.push({
            from: 50,
            to: 100,
            amount: '1000.00',
            level: $scope.level,
            type: $scope.type,
            scholarship_type_id: $scope.type_id
          });
        }          
        }

      }
    }

    function linkFn(scope, element, attributes) {
      var $bar = element.find('#_bar');
      var $ctl = element.find('#_bar-control');

      $ctl.on('mouseover', function(evt) { $ctl.css('opacity', '1'); });
      $ctl.on('mouseleave', function(evt) {
        if ( !scope.isEditing ) {
          $ctl.css('opacity', '0');
        }
      });

      var end = 100;
      var start = 50;
      var diff = end - start;

      var height = 40;
      var width = element.width();
      var ppp = width / diff;

      scope.$ppp = $ppp;
      scope.$position = $position;

      function $ppp(to, from, isFirst, isLast) {
        var percentage = ( parseInt(to, 10) - parseInt(from, 10) );

        return scope.graph.length > 1 && !isLast
          ? ( ppp *  parseInt(percentage, 10) ) + 'px'
          : (width - parseInt($position(from, isFirst)) ) + 'px';
      }

      function $position(percentage, isFirst) {
        return  scope.graph.length > 1 && !isFirst
          ? ( ppp * ( parseInt(percentage, 10) - start) ) + 'px'
          : '0';
      }
    }
  }
})(angular);
