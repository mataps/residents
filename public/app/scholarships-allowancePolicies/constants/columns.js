+(function(angular, undefined) {
  var columns = [
    {
      key: 'type',
      label: 'Type(Public/Private)',
      show: true,
      cellTemplate: '{{:: policy.school_type }}'
    },
    {
      key: 'level',
      label: 'Level',
      show: true,
      cellTemplate: '{{:: policy.school_level }}'
    },
    {
      key: 'Amount',
      label: 'Amount',
      show: true,
      cellTemplate: '{{:: policy.amount }}'
    }
  ];

  angular
    .module('app')
    .constant('ALLOWANCE_POLICIES_COLUMNS', columns);
})(angular);