+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('AllowancesCtrl', AllowancesCtrl);

  function AllowancesCtrl($scope, $controller, $state, flash, UtilService, Allowance, ALLOWANCE_COLUMNS, Restangular) {
    // Inherit BaseIndexCtrl's prototype,
    var controller = $controller('BaseIndexCtrl', {
      $scope: $scope,
      $state: $state,
      UtilService: UtilService
    });

    angular.extend(this, controller);

    // ------------------------- //
    // ------------------------- //

    // viewmodel
    var vm = this,    
       // Data collection (grabbed from the service)
      collection = [];

    // Copy items from the service
    // to the local collection variable
    angular.extend(collection, Allowance.collection);

    // ------------------------- //
    // ------------------------- //

    // Initialize vm data
    vm.columns = ALLOWANCE_COLUMNS;
    vm.collection = collection;

    // Manually bind the "this"
    // from the base controller
    vm.callServer = angular.bind(vm, vm.callServer);
    vm.applyPageData = angular.bind(vm, vm.applyPageData);
    vm.catchPageDataError = angular.bind(vm, vm.catchPageDataError);

    // Commands that contact the API / server
    vm.getPageData = getPageData;

    /**
     * Fetches data and replaces the current
     * @param  string query
     * @return void
     */
    function getPageData(query) {
      return Allowance
        .Query
        .all(query)
        .then(vm.applyPageData)
        .catch(vm.catchPageDataError);
    }
  }
})(angular);