+(function(angular, undefined) {
  function stateConfig($stateProvider) {

    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'List of generated allowances' };

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = { };

    var state = {
      parent: 'main',
      name: 'scholarship-allowances',
      url: '/scholarships/allowances',
      controllerAs: 'allowances',
      controller: 'AllowancesCtrl',
      data: data,
      resolve: resolves,
      templateUrl: '/app/scholarships-allowances/allowances.html'
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);