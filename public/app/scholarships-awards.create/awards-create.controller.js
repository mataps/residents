+(function(angular, undefined) {
  function AwardsCreateCtrl($scope, $http, Award, flash, $state) {
    var vm = this,
      form = {
        data: {}
      };

    // ------------------------- //
    // ------------------------- //
    vm.form = form;
    vm.create = create;
    vm.$attrRequest = $attrRequest;
    vm.$onChangeAttribute = $onChangeAttribute;

    ////////////////////////////////
    // Todo loading
    ////////////////////////////////

    /**
     * @description  Sends an XHR to the server with
     *               the form data for create a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    function create(formData) {
      console.log(formData);
      return Award
        .Command
        .create(formData)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    }

    function successXHRCallback(response) {
      flash.success = 'Award has been successfully created!';

      $state.go('scholarship-awards', null, { reload: true });
    }

    function errorXHRCallback(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }
         function $attrRequest($value) {
      return $http.get('/api/v1/attributions/?filters=' + JSON.stringify({ name: $value }))
        .then(function(response) {
          console.log(response);
          return response.data.data;
        });
    }

    function $onChangeAttribute($value)
    {
      console.log($value);
      vm.form.data.name = $value;
    }
  }

  angular
    .module('app')
    .controller('AwardsCreateCtrl', AwardsCreateCtrl);
})(angular);