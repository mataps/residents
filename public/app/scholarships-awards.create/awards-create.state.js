+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'New award' };
    var modal;

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = undefined;
    // 
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('scholarship-awards');
      }
        
      modal = $modal
        .open({
          size: 'sm',
          resolves: resolves,
          controller: 'AwardsCreateCtrl as createCtrl',
          templateUrl: '/app/scholarships-awards.create/awards-create.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }
    

    var state = {
      url: '/create',
      name: 'scholarship-awards.create',
      data: data,
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);