+(function(angular, undefined) {
  function AwardsEditCtrl($scope, $stateParams, flash, Award, $state) {
    var vm = this,
      // Form data, holds the information
      // input in the form while providing
      // information needed by the form
      form = { data: Award.getInstance() };

    // ------------------------- //
    // ------------------------- //
    vm.form = form;
    vm.update = update;
    vm.$attrRequest = $attrRequest;

    console.log(form);

    /**
     * @description  Sends an XHR to the server with
     *               the form data for create a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
   var _attribution = vm.form.data.attributions.data[0];
     var _undefined = angular.isUndefined(_attribution);
    vm.form.data.attribution_id = !_undefined ? _attribution.id : '';
    vm.form.data.attribution_name = !_undefined ? _attribution.name : '';    


    function update(formData) {
      formData.attributions = formData.attribution_name;
      console.log(formData);
      return Award
        .Command
        .update(formData)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
       // return formData.put()
       // form
       // return form.data.put()
       //  .then(successXHRCallback)
       //  .catch(errorXHRCallback);
    }

    function successXHRCallback(response) {
      flash.success = 'Award has been successfully updated!';

      $state.go('scholarship-awards', null, { reload: true });
    }

    function errorXHRCallback(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }
     function $attrRequest($value) {
      return $http.get('/api/v1/attributions/?filters=' + JSON.stringify({ name: $value }))
        .then(function(response) {
          console.log(response);
          return response.data.data;
        });
    }

  }

  angular
    .module('app')
    .controller('AwardsEditCtrl', AwardsEditCtrl);
})(angular);
