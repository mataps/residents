+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Edit award' };
    var modal;

    /**
     * @see  Award.Repository.get
     * @param  {Object} Award DI
     * @return promise
     */
    function getAwardByIdResolve(Award, $stateParams) {
      return Award
        .Repository
        .get($stateParams.id);
    }

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = { getAwardByIdResolve: getAwardByIdResolve };

    // Activates the modal
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('scholarship-awards');
      }
        
      modal = $modal
        .open({
          size: 'sm',
          controller: 'AwardsEditCtrl as editCtrl',
          templateUrl: '/app/scholarships-awards.edit/awards-edit.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }

    var state = {
      name: 'scholarship-awards.edit',
      resolve: resolves,
      url: '/{id}/edit',
      data: data,
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);