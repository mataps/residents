+(function(angular, undefined) {
  angular
    .module('app')
    .config(stateConfig);
  function stateConfig($stateProvider) {

    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Manage Awards' };

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = { getAwardsCollectionResolve: getAwardsCollectionResolve };

    var state = {
      parent: 'main',
      name: 'scholarship-awards',
      url: '/scholarships/awards',
      data: data,
      resolve: resolves,
      controllerAs: 'awards',
      controller: 'AwardsCtrl',
      templateUrl: '/app/scholarships-awards/awards.html'
    };

    $stateProvider.state(state);
  }

  /**
   * @description Fetches all scholarship
   * @param  Restangular
   * @return Restangular
   */
  function getAwardsCollectionResolve(Award) {
    return Award
      .Repository
      .all();
  }
})(angular);