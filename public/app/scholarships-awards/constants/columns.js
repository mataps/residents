+(function(angular, undefined) {
  var columns = [
    {
      key: 'name',
      label: 'Name',
      show: true,
      cellTemplate: '{{:: award.name }}',
      onClick: 'awards.show(award.id)'
    },
    {
      key: 'description',
      label: 'Description',
      show: true,
      cellTemplate: '{{:: award.description }}',
      onClick: 'awards.show(award.id)'
    },
  ];

  angular
    .module('app')
    .constant('AWARDS_COLUMNS', columns);
})(angular);