+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('ScholarshipsCoursesCreateController', ScholarshipsCoursesCreateController);

  function ScholarshipsCoursesCreateController($scope, flash, $state, Restangular, $http) {
    var vm = this;

    angular.extend(vm, {
      isLoading: false,
      custom: {},
      data: {},
      $create: $create,
      $errorHandler: $errorHandler,
      $successHandler: $successHandler
    });

    function $create(data) {
      vm.isLoading = true;

      return $http.post('api/v1/courses', data)
        .then($successHandler)
        .catch($errorHandler)
        .finally(function() { vm.isLoading = false; });
    }

    /**
     * Success response
     */
    function $successHandler(response) {
      flash.success = 'Course has been successfully created!';

      $state.go('scholarships-courses', null, { reload: true });
    }

    /**
     * Error response
     */
    function $errorHandler(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }
  }
})(angular);
