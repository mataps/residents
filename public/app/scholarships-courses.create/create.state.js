+(function(angular) {
  'use strict';

  angular
    .module('app')
    .config(config);

  function config($stateProvider) {

    var modal;

    /** Trigger modal */
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('scholarships-courses');
      }

      modal = $modal
        .open({
          size: 'sm',
          controller: 'ScholarshipsCoursesCreateController as createCtrl',
          templateUrl: '/app/scholarships-courses.create/create.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }


    var state = {
      url: '/create',
      name: 'scholarships-courses.create',
      data: { pageTitle: 'New course' },
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }
})(angular);
