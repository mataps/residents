+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('ScholarshipsCoursesEditController', ScholarshipsCoursesEditController);

  function ScholarshipsCoursesEditController($scope, Household, Restangular, flash, UtilService, $state, $http, $stateParams) {
    var vm = this;

    angular.extend(vm, {
      isLoading: true,
      data: {},
      $update: $update,
    });

    init();

    /**
     *
     */
    function $update(data) {
      vm.isLoading = true;

      return $http
        .put('api/v1/courses/' + $stateParams.id, data)
        .then(function() {
          flash.success = 'Course has been updated!';
          $state.go('scholarships-courses', null, { reload: true });
        })
        .catch(function(r) {
          flash.to('validation').error = _.chain(r.data.errors)
            .values()
            .flatten()
            .value();
        })
        .finally(function() { vm.isLoading = false; });
    }

    /**
     *
     */
    function init() {
      return Restangular
        .one('courses', $stateParams.id)
        .get()
        .then(function(response) {
          vm.data = response.data;
        })
        .catch(function(response) { vm.$close(); })
        .finally(function() { vm.isLoading = false; });
    }
  }
})(angular);
