+(function(angular) {
  'use strict';

  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var modal;
    /** Trigger modal */
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('scholarships-courses');
      }

      modal = $modal
        .open({
          size: 'sm',
          controller: 'ScholarshipsCoursesEditController as editCtrl',
          templateUrl: '/app/scholarships-courses.edit/edit.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }

    var state = {
      url: '/{id}/edit',
      name: 'scholarships-courses.edit',
      data: { pageTitle: 'Edit course' },
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }
})(angular);
