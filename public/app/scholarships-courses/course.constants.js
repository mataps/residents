+(function(angular, undefined) {
  var columns = [
    {
      key: 'name',
      label: 'Name',
      show: true,
      cellTemplate: '{{:: course.name }}',
      onClick: 'courses.show(course.id)'
    },
    {
      key: 'abbreviation',
      label: 'Abbreviation',
      show: true,
      cellTemplate: '{{:: course.abbreviation }}',
      onClick: 'courses.show(course.id)'
    }
  ];

  angular
    .module('app')
    .constant('COURSES_COLUMNS', columns);
})(angular);
