+(function(angular, undefined) {
  "use strict";
  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var data = { pageTitle: 'Manage Courses' };

    var state = {
      parent: 'main',
      name: 'scholarships-courses',
      url: '/scholarships/courses',
      data: data,
      resolve: { getCoursesResolve: getCoursesResolve },
      controllerAs: 'courses',
      controller: 'ScholarshipsCoursesController',
      templateUrl: '/app/scholarships-courses/course.html'
    };

    $stateProvider.state(state);

    function getCoursesResolve(Restangular) {
      return Restangular
        .one('courses')
        .getList();
    }
  }


})(angular);
