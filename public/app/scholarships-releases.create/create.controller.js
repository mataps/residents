+(function(angular, undefined) {
  function ScholarshipReleasesCreateController($scope, Award, flash, $state, typeResolve, Restangular) {
    var vm = this;

    vm.types = typeResolve.data.data;
    vm.data = {};
    vm.create = create;

    /**
     * @description  Sends an XHR to the server with
     *               the form data for create a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    function create() {
      var data = angular.copy(vm.data);
      var types = data.scholarship_types;
      
      if ( angular.isArray(types) ) {
        data.scholarship_types = data.scholarship_types.join(',');
      }

      return Restangular
        .one('')
        .post('releases', data)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    }

    function successXHRCallback(response) {
      flash.success = 'Release has been successfully created!';

      $state.go('scholarship-releases', null, { reload: true });
    }

    function errorXHRCallback(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }
  }

  angular
    .module('app')
    .controller('ScholarshipReleasesCreateController', ScholarshipReleasesCreateController);
})(angular);