+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'New release' };
    var modal;

    function onEnter($modal, $state, typeResolve) {
      function transitionToOverlay() {
        return $state.go('scholarship-releases');
      }

      console.log(typeResolve);
        
      modal = $modal
        .open({
          size: 'sm',
          resolve: { typeResolve: function() { return typeResolve; } },
          controller: 'ScholarshipReleasesCreateController as createCtrl',
          templateUrl: '/app/scholarships-releases.create/create.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }
    

    var state = {
      url: '/create',
      name: 'scholarship-releases.create',
      data: data,
      resolve: { typeResolve: typeResolve },
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);

    function typeResolve($http) {
      return $http.get('api/v1/scholarshiptypes');
    }
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);