+(function(angular, undefined) {
  function ScholarshipReleasesEditController($scope, Award, flash, $state, typeResolve, dataResolve, Restangular) {
    var vm = this;

    vm.types = typeResolve.data.data;
    vm.data = dataResolve.data;
    vm.update = update;

    console.log(vm.data);

    /**
     * @description  Sends an XHR to the server with
     *               the form data for update a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    function update() {
      var data = angular.copy(vm.data);
      var types = data.scholarship_types;

      if ( angular.isArray(types) ) {
        data.scholarship_types = data.scholarship_types.join(',');
      }

      return Restangular
        .one('releases')
        .put(data.id, data)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    }

    function successXHRCallback(response) {
      flash.success = 'Release has been successfully updated!';
    }

    function errorXHRCallback(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }
  }

  angular
    .module('app')
    .controller('ScholarshipReleasesEditController', ScholarshipReleasesEditController);
})(angular);