+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Update release' };
    var modal;

    function onEnter($modal, $state, typeResolve, dataResolve) {
      function transitionToOverlay() {
        return $state.go('scholarship-releases');
      }

      console.log(typeResolve);
        
      modal = $modal
        .open({
          size: 'sm',
          resolve: {
            dataResolve: function() { return dataResolve; },
            typeResolve: function() { return typeResolve; }
          },
          controller: 'ScholarshipReleasesEditController as editCtrl',
          templateUrl: '/app/scholarships-releases.edit/edit.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }
    

    var state = {
      url: '/{id}/edit',
      name: 'scholarship-releases.edit',
      data: data,
      resolve: {
        dataResolve: dataResolve,
        typeResolve: typeResolve
      },
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);

    function typeResolve($http) {
      return $http.get('api/v1/scholarshiptypes');
    }

    function dataResolve(Restangular, $stateParams) {
      return Restangular
        .one('releases', $stateParams.id)
        .getList();
    }
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);