+(function(angular, undefined) {
  var fixedAmountTemplate = [
    '<span ng-if="!!release.fixed_amount" class="label label-success"> <i class="ion-checkmark"></i> </span>',
    '<span ng-if="!release.fixed_amount" class="label label-warning"> <i class="ion-minus"></i> </span>'
  ].join('');

  var amountTemplate = [
    '<span ng-if="!release.amount" class="label label-primary"> Policy-based </span>',
    '<span ng-if="!!release.amount" class="label label-primary"> {{:: release.amount }} </span>',
  ].join('');

  var columns = [
      {
      key: 'uuid',
      label: 'UUID',
      show: true,
      cellTemplate: '{{:: release.uuid }}',
      onClick: 'releases.show(release.id)'
    },
    {
      key: 'school_year',
      label: 'School Year',
      show: true,
      cellTemplate: '{{:: release.school_year }}',
      onClick: 'releases.show(release.id)'
    },
    {
      key: 'term',
      label: 'Term',
      show: true,
      cellTemplate: '{{:: release.term }}',
      onClick: 'releases.show(release.id)'
    },
    {
      key: 'scholarship_types.scholarship_types.id',
      label: 'Scholarship Types',
      show: true,
      cellTemplate: '<span ng-repeat="type in release.scholarship_types.data"> {{:: ($index > 0 ? \',\' : \'\') + type.name }}',
      onClick: 'releases.show(release.id)'
    },
    {
      key: 'fixed_amount',
      label: 'Fixed Amount',
      show: true,
      cellTemplate: fixedAmountTemplate,
      onClick: 'releases.show(release.id)'
    },
    {
      key: 'amount',
      label: 'Amount',
      show: true,
      cellTemplate: '{{:: release.amount | money }}',
      onClick: 'releases.show(release.id)'
    }
  ];

  angular
    .module('app')
    .constant('RELEASES_COLUMNS', columns);
})(angular);