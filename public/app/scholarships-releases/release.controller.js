+(function(angular, _, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('ScholarshipReleasesController', ScholarshipReleasesController);

  function ScholarshipReleasesController($scope, $modal, $controller, $state, flash, UtilService, Restangular, getReleasesResolve, getTypesResolve, RELEASES_COLUMNS) {
    // Inherit BaseIndexCtrl's prototype,
    var controller = $controller('BaseIndexCtrl', {
      $scope: $scope,
      $state: $state,
      UtilService: UtilService
    });

    angular.extend(this, controller);

    // ------------------------- //
    // ------------------------- //

    // viewmodel
    var vm = this,    
       // Data collection (grabbed from the service)
      collection = [];

    // Copy items from the service
    // to the local collection variable
    angular.extend(collection, getReleasesResolve.data);

    // ------------------------- //
    // ------------------------- //

    // Initialize vm data
    vm.columns = RELEASES_COLUMNS;
    vm.collection = collection;
    vm.types = getTypesResolve.data.data;
    console.log(vm.types);

    // Manually bind the "this"
    // from the base controller
    vm.callServer = angular.bind(vm, vm.callServer);
    vm.applyPageData = angular.bind(vm, vm.applyPageData);
    vm.catchPageDataError = angular.bind(vm, vm.catchPageDataError);

    // Commands that contact the API / server
    vm.getSelected = angular.bind(vm, vm.getSelected);
    vm.remove = remove;
    vm.getPageData = getPageData;
    vm._filters = {};
    vm.report = report;
    vm.$validateReport = $validateReport;
    /**
     * @description  Sends a DELETE to the server
     * @see  getPageData
     * @return void
     */
    function remove() {
      if(!confirm('Please confirm deleting the selected records.'))
        return;

      vm.load();
      var selected = vm.getSelected();

      return Restangular
        .one('releases')
        .delete(selected)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    }

    // Success XHR
    function successXHRCallback(response) {
      flash.success = 'Release has been successfully archived!';
      $state.reload();
      vm.load(false);
    }

    // Error XHR
    function errorXHRCallback(response) {
      flash.error = 'An error has occured. Please try again...';
      vm.load(false);
    }

    /**
     * Fetches data and replaces the current
     * @param  string query
     * @return void
     */
    function getPageData(query) {
      vm._filters = query.filters;

      return Restangular
        .service('releases')
        .getList(query)
        .then(vm.applyPageData)
        .catch(vm.catchPageDataError);
    }

    function reportXLS() {
      var selected = vm.getSelected()[0];
      var filters = vm._filters;

      return '/api/v1/releases/' + selected + '/report/spreadsheet' + '?filters=' +
        JSON.stringify( angular.extend({}, filters == undefined ? {} : filters) );
    }

    function report(type) {
      var selected = vm.getSelected()[0];      
      return $modal.open({
        templateUrl: 'app/scholarships-releases/report/report.html',
        controller: 'ScholarshipReleasesReportController',
        size : 'md',
        resolve : {
          selectedResolve : function(){
            return vm.getSelected()[0]; 
          },
          typeResolve : function (){
            return type;
          }
        }
        });
    }


    function $validateReport(evt) {
      var selected = vm.getSelected();

      if ( !selected.length ) {
        flash.error = 'Please select a release to generate a report';
        evt.preventDefault();
        evt.stopPropagation && evt.stopPropagation();
      } else if ( selected.length > 1 ) {
        flash.error = 'Please select only one release to generate a report';
        evt.preventDefault();
        evt.stopPropagation && evt.stopPropagation();
      }
    }
  }
})(angular, _);