+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .config(stateConfig);

  function stateConfig($stateProvider) {

    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Manage Scholarship Releases' };

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = {
      getReleasesResolve: getReleasesResolve,
      getTypesResolve: getTypesResolve
    };

    var state = {
      parent: 'main',
      name: 'scholarship-releases',
      url: '/scholarships/releases',
      data: data,
      resolve: resolves,
      controllerAs: 'releases',
      controller: 'ScholarshipReleasesController',
      templateUrl: '/app/scholarships-releases/release.html'
    };

    $stateProvider.state(state);
  }

  /**
   * @description Fetches all scholarship
   * @param  Restangular
   * @return Restangular
   */
  function getReleasesResolve(Restangular) {
    return Restangular
      .one('releases')
      .getList();
  }

  function getTypesResolve($http) {
    return $http.get('api/v1/scholarshiptypes');
  }
})(angular);