+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('ScholarshipReleasesReportController', ScholarshipReleasesReportController);

  function ScholarshipReleasesReportController($scope, $http, $window, Restangular, selectedResolve, typeResolve) {
    $scope.form = { data : {} };
    $scope.selected = selectedResolve;
    $scope.type = typeResolve;
    $scope.citiesMunicipalities = function($viewValue) {
      return Restangular.service('citiesmunicipalities')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }


    $scope.barangays = function($viewValue, cityMunicipality) {
      return Restangular.service('barangays')
        .getList({ limit: 6969696969999999, filters: JSON.stringify({ name: $viewValue,  'cityMunicipality.name' : cityMunicipality}) })
        .then(function(response) {
          return response.data;
        });
    }

    $scope.getURL = function(type, filter){
            var filters = filter;
            return '/api/v1/releases/' + $scope.selected + '/report/' + type + '?filters=' +
             JSON.stringify( angular.extend({}, filters == undefined ? {} : filters) );
    }

  }
})(angular);