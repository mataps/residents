+(function(angular, undefined) {
  function SchoolsCreateCtrl($scope, School, flash, $state, UtilService) {
    var vm = this,
      form = { data: {} };

    // ------------------------- //
    // ------------------------- //
    vm.form = form;
    vm.create = create;
    vm.isLoading = false;

    ////////////////////////////////
    // Todo loading
    ////////////////////////////////

    /**
     * @description  Sends an XHR to the server with
     *               the form data for create a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    function create(formData) {
      load();

      School
        .Command
        .create(formData)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    }

    function successXHRCallback(response) {
      load(false);

      flash.success = 'School has been successfully registered!';

      $state.go('scholarship-schools', null, { reload: true });
    }

    function errorXHRCallback(response) {
      load(false);

      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }

    // Setter for the isLoading prop
    function load(bool) { vm.isLoading = UtilService.tralse(bool); }
  }

  angular
    .module('app')
    .controller('SchoolsCreateCtrl', SchoolsCreateCtrl);
})(angular);