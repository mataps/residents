+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'New Scholarship' };
    var modal;
    
    // 
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('scholarship-schools');
      }
        
      modal = $modal
        .open({
          size: 'sm',
          controller: 'SchoolsCreateCtrl as createCtrl',
          templateUrl: '/app/scholarships-schools.create/schools-create.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }
    

    var state = {
      url: '/create',
      name: 'scholarship-schools.create',
      data: data,
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);