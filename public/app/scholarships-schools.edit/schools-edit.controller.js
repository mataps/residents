+(function(angular, undefined) {
  function SchoolsEditCtrl($scope, $stateParams, flash, School, $state) {
    var vm = this,
      // Form data, holds the information
      // input in the form while providing
      // information needed by the form
      form = { data: School.instance.clone() };

    // ------------------------- //
    // ------------------------- //
    vm.form = form;
    vm.update = update;
    vm.isLoading = false;

    /**
     * @description  Sends an XHR to the server with
     *               the form data for create a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    function update(formData) {
      return School
        .Command
        .update(formData)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    }

    function successXHRCallback(response) {
      flash.success = 'School has been successfully updated!';

      $state.go('scholarship-schools', null, { reload: true });
    }

    function errorXHRCallback(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }
  }

  angular
    .module('app')
    .controller('SchoolsEditCtrl', SchoolsEditCtrl);
})(angular);
