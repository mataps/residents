+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Edit school' };
    var modal;

    /**
     * @see  School.Repository.get
     * @param  {Object} School DI
     * @return promise
     */
    function getSchoolByIdResolve(School, $stateParams) {
      return School
        .Repository
        .get($stateParams.id);
    }

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = { getSchoolByIdResolve: getSchoolByIdResolve };

    // Activates the modal
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('scholarship-schools');
      }
        
      modal = $modal
        .open({
          size: 'sm',
          controller: 'SchoolsEditCtrl as editCtrl',
          templateUrl: '/app/scholarships-schools.edit/schools-edit.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }

    var state = {
      name: 'scholarship-schools.edit',
      resolve: resolves,
      url: '/{id}/edit',
      data: data,
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);