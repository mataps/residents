+(function(angular, undefined) {
  var columns = [
    {
      key: 'name',
      label: 'Name',
      show: true,
      cellTemplate: '{{:: school.name }}',
      onClick: 'schools.show(school.id)'
    },
    {
      key: 'type',
      label: 'Type (Public/Private)',
      show: true,
      cellTemplate: '{{:: school.type }}',
      onClick: 'schools.show(school.id)'
    },
    {
      key: 'level',
      label: 'Level',
      show: true,
      cellTemplate: '{{:: school.level }}',
      onClick: 'schools.show(school.id)'
    },
    {
      key: 'address',
      label: 'Address',
      show: true,
      cellTemplate: '{{:: school.address }}',
      onClick: 'schools.show(school.id)'
    },
    {
      key: 'phone',
      label: 'Phone',
      show: false,
      cellTemplate: '{{:: school.phone }}',
      onClick: 'schools.show(school.id)'
    }
  ];

  angular
    .module('app')
    .constant('SCHOOL_COLUMNS', columns);
})(angular);