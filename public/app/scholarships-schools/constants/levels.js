+(function(angular, undefined) {
  var SCHOOL_LEVELS = [
    // {
    //   value: 'primary',
    //   label: 'Primary'
    // },
    {
      value: 'secondary',
      label: 'Secondary'
    },
    {
      value: 'collegiate',
      label: 'Collegiate'
    }
  ];

  angular
    .module('app')
    .constant('SCHOOL_LEVELS', SCHOOL_LEVELS);
})(angular);