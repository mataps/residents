+(function(angular, undefined) {
  var types = [
    {
      label: 'Public',
      value: 'public'
    },
    {
      label: 'Private',
      value: 'private'
    }
  ];

  angular
    .module('app')
    .constant('SCHOOL_TYPES', types);
})(angular);