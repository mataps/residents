+(function(angular, _, undefined) {
  angular
    .module('app')
    .directive('gcSchoolLevel', schoolLevel);

  function schoolLevel() {
    var template = [
      '<select ng-model="levelCtrl.model" ng-init="levelCtrl.model = levelCtrl.model ? levelCtrl.model : \'collegiate\'" class="form-control">',
        '<option value="collegiate">Collegiate</option>',
        '<option value="secondary">Secondary</option>',
      '</select>',
    ].join('');

    return {
      restrict: 'EA',
      scope: { model: '=ngModel' },
      template: template,
      bindToController: true,
      controllerAs: 'levelCtrl',
      controller: function($scope) {}
    };
  }
})(angular, _);