+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .run(runBlock);

  function runBlock($templateCache) {
    var template = [
      '<a>',
        '<div bind-html-unsafe="match.model.name | typeaheadHighlight:query"></div>',
        '<span style="color: #989898;">{{ match.model.level | capitalize }} ',
        '<small>({{ match.model.type | capitalize }})</small></span>',
      '</a>'
    ].join('');

    $templateCache.put(
      'gc-school-typeahead-$template.html',
      template
    );
  }
})(angular);