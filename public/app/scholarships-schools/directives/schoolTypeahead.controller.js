+(function(angular, undefined) {
  angular
    .module('app')
    .controller(
      'GCSchoolTypeaheadController',
      GCSchoolTypeaheadController
    );
    
  function GCSchoolTypeaheadController($scope, School) {
    //
    var vm = this;

    transform();

    vm.request = request;
    vm.selectCb = selectCb;

    /** Requests data to the server */
    function request(name) {
      return School
        .Query
        .typeahead(name)
        .then(function(response) {
          return response.data;
        });
    }

    /**
     * Apply ID when an item in the dropdown
     * list is selected
     */
    function selectCb($item, $model, $label) {
      vm.idVariable = $item.id;
    }

    /** Transform data */
    function transform() {
      if ( !angular.isUndefined(vm.transformIdVariable) ) {
        vm.idVariable = vm.transformIdVariable;
      }
    }
  }
})(angular);