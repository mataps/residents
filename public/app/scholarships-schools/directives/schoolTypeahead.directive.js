+(function(angular, undefined) {
  angular
    .module('app')
    .directive('gcSchoolTypeahead', gcSchoolTypeahead);

  function gcSchoolTypeahead($compile, $sce) {
    // Typeahead attributes
    var typeaheadOnSelect = 'schoolTypeahead.selectCb($item, $model, $label)';
    var typeahead = [
      'school.name for school ',
      'in schoolTypeahead.request($viewValue)'
    ].join('');

    var scope = {
      idVariable: '=',
      transformIdVariable: '@',
      model: '=ngModel' // Avoid ng-model conflcit with typehead
    };

    var template = [
      '<div>',
        '<input type="text" class="form-control" ',
        'ng-model="schoolTypeahead.model" ',
        'typeahead="', typeahead, '" ',
        'typeahead-on-select="', typeaheadOnSelect, '" ',
        'typeahead-template-url="gc-school-typeahead-$template.html">',
    '</div>'
    ].join('');

    return {
      scope: scope,
      restrict: 'EA',
      template: template,
      bindToController: true,
      controllerAs: 'schoolTypeahead',
      controller: 'GCSchoolTypeaheadController',
      replace: true
    };
  }
})(angular);
