+(function(angular, _, undefined) {
  angular
    .module('app')
    .directive('gcSchoolTypes', schoolTypes);

  function schoolTypes() {
    var template = [
      '<select ng-model="typeCtrl.model" ng-init="typeCtrl.model = typeCtrl.model ? typeCtrl.model : \'public\'" class="form-control">',
        '<option value="public">Public</option>',
        '<option value="private">Private</option>',
      '</select>',
    ].join('');

    return {
      restrict: 'EA',
      scope: { model: '=ngModel' },
      template: template,
      bindToController: true,
      controllerAs: 'typeCtrl',
      controller: function($scope) {}
    };
  }
})(angular, _);