+(function(angular, undefined) {
  function SchoolsCtrl($scope, $controller, $state, UtilService, School, SCHOOL_COLUMNS, SCHOOL_LEVELS, flash) {
    // Inherit BaseIndexCtrl's prototype,
    var controller = $controller('BaseIndexCtrl', {
        $scope: $scope,
        $state: $state,
        UtilService: UtilService
    });

    angular.extend(this, controller);

    // ------------------------- //
    // ------------------------- //

    // viewmodel
    var vm = this;
    
    // Data collection (grabbed from the service)
    var collection = [];

    // Copy items from the service
    // to the local collection variable
    angular.extend(collection, School.collection);

    // ------------------------- //
    // ------------------------- //

    // Initialize vm data
    vm.columns = SCHOOL_COLUMNS;
    vm.collection = collection;

    // Commands that contact the API / server
    vm.remove = remove;
    vm.getPageData = getPageData;

    // Manually bind the "this"
    // from the base controller
    vm.callServer = angular.bind(vm, vm.callServer);
    vm.applyPageData = angular.bind(vm, vm.applyPageData);
    vm.catchPageDataError = angular.bind(vm, vm.catchPageDataError);

    /**
     * @description  Sends a DELETE to the server
     * @see  getPageData
     * @return void
     */
    function remove() {
      if(!confirm('Please confirm deleting the selected records.'))
        return;

      vm.load();
      var selected = vm.getSelected().join(',');

      return School
        .Command
        .delete(selected)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    }

    // Success XHR
    function successXHRCallback(response) {
      flash.success = 'School has been successfully archived!';
      $state.reload();
      vm.load(false);
    }

    // Error XHR
    function errorXHRCallback(response) {
      flash.error = 'An error has occured. Please try again...';
      vm.load(false);
    }

    /**
     * Fetches data and replaces the current
     * @param  string query
     * @return void
     */
    function getPageData(query) {
     // Callback function for promises after
     // a server | ajax call
      var applyPageData = angular.bind(this, vm.applyPageData);
      var catchPageDataError = angular.bind(this, vm.catchPageDataError);

      School
        .Query
        .all(query)
        .then(applyPageData)
        .catch(catchPageDataError);
    }
  }

  angular
    .module('app')
    .controller('SchoolsCtrl', SchoolsCtrl);
})(angular);