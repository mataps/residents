+(function(angular, undefined) {
  function stateConfig($stateProvider) {

    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Manage Schools' };

    /**
     * @description Fetches all scholarship
     * @param  Restangular
     * @return Restangular
     */
    function getSchoolsCollectionResolve (School) {
      return School
        .Repository
        .all();
    }


    /**
     * Resolves
     * @type {Object}
     */
    var resolves = { getSchoolsCollectionResolve: getSchoolsCollectionResolve };

    var state = {
      parent: 'main',
      name: 'scholarship-schools',
      url: '/scholarships/schools',
      data: data,
      resolve: resolves,
      controllerAs: 'schools',
      controller: 'SchoolsCtrl',
      templateUrl: '/app/scholarships-schools/schools.html'
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);