+(function(angular, undefined) {
  function SemestersAltCreateCtrl($scope, $stateParams, UtilService, Scholarship, Semester, School, $state, flash) {
    var vm = this,
      // Form defaults
      form = {
        data: {
          beneficiary_id: '',
          client_id: '',
          referrer_id: '',
          client_type: 'Resident',
          beneficiary_type : 'Resident',
          gwa: 0,
          grades: { data: [] },
          
          scholarship_id: $stateParams.id,
          school: Scholarship.instance.school,
          school_id: Scholarship.instance.school.id,
          school_name: Scholarship.instance.school.name,
        
          status: Scholarship.instance.status
        }
      };

    vm.scholarship = Scholarship.instance;
    vm.semester_status = ['active', 'for_verification', 'waiting'];

    // ------------------------- //

    // Form data
    vm.form = form;
    vm.successXHRCallback = successXHRCallback;
    vm.errorXHRCallback = errorXHRCallback;

    // ------------------------- //

    if(Semester.instance.length > 0){
      var last = Semester.instance.length - 1;
      vm.form.data.allowance_basis_id = Semester.instance[last].id
      vm.form.data.scholarship_type_id = Semester.instance[last].id
    }

    /** Success callback */
    function successXHRCallback(response) {
      flash.success = 'Semester has been successfully created!';
        $scope.$close();
      $state.go('scholarships.show', { id: $stateParams.id }, { reload: true });
    }

    /** Error callback */
    function errorXHRCallback(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }
  }

  angular
    .module('app')
    .controller('SemestersAltCreateCtrl', SemestersAltCreateCtrl);
})(angular);
