+(function(angular, undefined) {
  function SemestersAltEditCtrl($scope, $stateParams, UtilService, Scholarship, Semester, flash, $state) {
    var vm = this,
      // Form defaults
      form = { data: Semester.getInstance() };

    // form.data.scholarship_id = $stateParams.id;

    // ------------------------- //

    // Form data
    vm.add_award_toggle =0;
    vm.edit_mode = 0;
    vm._dummy = 0;
    vm.form = form;
    vm.toggle = toggle;
    vm.addAwardToggle = addAwardToggle;
    vm.form.data.school_year = form.data.school_year;
    vm.form.data.course_id = vm.form.data.course.id;
    vm.form.data.course_name = vm.form.data.course.name;
    vm.form.data.course_abbreviation = vm.form.data.course.abbreviation;
    vm.form.data.allowance_basis_id = vm.form.data.allowance_basis.id;
    // window.form = form;
    // console.log(form);
    vm.isLoading = false;
    vm.successXHRCallback = successXHRCallback;
    vm.errorXHRCallback = errorXHRCallback;
    vm.update = update;
    /** Success Callback */
    function successXHRCallback(response) {
      vm.isLoading = false;
      flash.success = 'Semester has been successfully updated!';

      // $state.go('scholarships-semesters-list.show', { id: $stateParams.id }, { reload: true });
    }

    /** Error Callback */
    function errorXHRCallback(response) {
      vm.isLoading = false;

      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }

    function toggle()
    {
      vm.edit_mode = !vm.edit_mode;
    }

    function addAwardToggle()

    {
      vm.add_award_toggle = !vm.add_award_toggle;
    }


    function update(formData) {
      console.log('edit');
      return Semester
        .Command
        .update(formData)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    }
  }

  angular
    .module('app')
    .controller('SemestersAltEditCtrl', SemestersAltEditCtrl);
})(angular);
