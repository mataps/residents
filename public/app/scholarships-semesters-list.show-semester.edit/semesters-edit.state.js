+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Edit semester' };

    /**
     * @see  School.Repository.get
     * @param  {Object} School DI
     * @return promise
     */
    function getSemesterByResolve(Semester, $state, $stateParams) {
      console.log('sem_resolve ' + $stateParams.sem_id);
      return Semester
        .Repository
        .get($stateParams.sem_id);
    }

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = { getSemesterByResolve: getSemesterByResolve };

    var state = {
      url: '/semesters/{sem_id}',
      name: 'semesters-list.show.semester-edit',
      data: data,
      resolve: resolves,
      views: {
        'semester@': {
          controller: 'SemestersEditCtrl',
          controllerAs: 'semEditCtrl',
          templateUrl: '/app/scholarships.show-semester.edit/semesters-edit.html'
        }
      }
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);