+(function(angular, _, undefined) {

  var columns = [
    {
      key: 'term',
      label: 'Semester Term',
      show: true,
      cellTemplate: '{{ semester.term }}',
      onClick: 'semestersAlt.show(semester.id)'
    },
    {
      key: 'scholarship_type.name',
      label: 'Scholarship Type',
      show: true,
      cellTemplate: '{{ semester.scholarship_type.name }}',
      onClick: 'semestersAlt.show(semester.id)'
    },
    {
      key: 'school_year',
      label: 'School Year',
      show: true,
      cellTemplate: '{{ semester.school_year }}',
      onClick: 'semestersAlt.show(semester.id)'
    },
    {
      key: 'school.type',
      label: 'School ASType',
      show: true,
      cellTemplate: '{{ semester.school.type | capitalize }}',
      onClick: 'semestersAlt.show(semester.id)'
    },
    {
      key: 'school.year_level',
      label: 'Year Level',
      show: true,
      cellTemplate: '{{ semester.year_level }}',
      onClick: 'semestersAlt.show(semester.id)'
    },
    {
      key: 'gwa',
      label: 'GWA',
      show: true,
      cellTemplate: '{{ semester.gwa }}',
      onClick: 'semestersAlt.show(semester.id)'
    },
    {
      key: 'school.name',
      label: 'School',
      show: true,
      cellTemplate: '{{ semester.school.name }}',
      onClick: 'semestersAlt.show(semester.id)'
    },
  ];

  angular
    .module('app')
    .constant('SEMESTER_ALT_COLUMNS', columns);
})(angular, _);
