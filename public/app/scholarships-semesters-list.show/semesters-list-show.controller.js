+(function(angular, undefined) {
  angular
    .module('app')
    .controller('SemestersListShowCtrl', SemestersListShowCtrl);

  function SemestersListShowCtrl($scope, $http, Scholarship, flash, $window, $state, $modal) {
    var vm = this;
    vm.award_input = "";
    vm.edit_mode = 0;
    vm.toggle = toggle;
    vm.update = update;
    vm.add_award_toggle = 0;
    vm.addAwardToggle = addAwardToggle;
    vm.addAward = addAward;
    angular.extend(vm,{
      loading: false,
      generateLoading: false,
      $generateSuccess: $generateSuccess,
      $generateError: $generateError,
      awards: [],
      list_awards: [],
      form: { data: Scholarship.instance },
      $success: $success,
      $error: $error,
      $transfer: $transfer
    });
    // vm.form

    (function init() {
      $http.get('/api/v1/awards')
       .then(function(response) {
          vm.list_awards = response.data.data;
          console.log(vm.list_awards);
         });
      $http
        .get('/api/v1/scholarships/10/awards')
        .then(function(res) { vm.awards = res.data.data; });


    })();

    function $success() {
      vm.loading = false;
      flash.success = 'Scholarship has been successfully updated. Page is now refreshing..';
      toggle();
    }

    function $error(response) {
      vm.loading = false;
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();

      flash.error = 'An error has occured. Please try refreshing the page';
    }

    function $generateSuccess(response) { 
      vm.generateLoading = false;
      flash.success = 'Allowance has been generated. Page will be refreshed in a sec..';
      $state.go($state.current.name, { id: vm.form.data.id }, { reload: true });
    }

    function $generateError(response) {
      vm.generateLoading = false;
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();

      flash.error = 'An error has occured while generating the allowance. Please try refreshing the page..';
    }

    function $transfer() {
      $modal.open({
        templateUrl: 'app/scholarships.show/transfer/transfer.html',
        controller: 'TransferCtrl as transferCtrl',
        size: 'sm',
        resolve: {
          formResolve: function() {
            return vm.form.data;
          }
        }
      })
    }


    function toggle() {
      vm.edit_mode = !vm.edit_mode;
    }

    function update(formData) {
      var form = formData;
      console.log(form);
      $http.put('/api/v1/scholarships/' + form.id, form)
           .then($success)
           .catch($error);
    }

    function addAwardToggle() {
      vm.add_award_toggle = !vm.add_award_toggle;
    }


    function addAward($formRequest){
      console.log($formRequest);

            $http.post('/api/v1/scholarships/' + vm.form.data.id + '/awards', $formRequest)
        .then(function(response){
          console.log(response);
          flash.success = response.message;

                 var award_name = response.data.name
                 var year = response.data.year;
                 var remarks = response.data.remarks;
                  vm.awards.push({name : award_name, year : year, remarks : remarks});
                  // console.log()
                  vm.addAwardToggle();
             
        })
        .catch(function(){

        });
    }

  }
})(angular);