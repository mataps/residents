+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'View Scholarship' };
    var modal;

    /**
     * @see  School.Repository.get
     * @param  {Object} School DI
     * @return promise
     */
    function getScholarshipByIdResolve(Scholarship, $state, $stateParams) {
      console.log($stateParams);
      return Scholarship
        .Repository
        .get($stateParams.id);
    }

    function getSemesterByIdResolve($state, $stateParams, Semester, Restangular) {
      return Restangular
        .one('scholarships', $stateParams.id)
        .getList('semesters', { limit: 9999 })
        .then(function(res) {
          console.log('Hitting..');
          angular.extend(Semester.instance, res.data);
          return res;
        })
        .catch(function(err) {
          console.error(err);
          return err;
        });
    }

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = {
      getScholarshipByIdResolve: getScholarshipByIdResolve,
      getSemesterByIdResolve: getSemesterByIdResolve
    };
    //
    function onEnter($modal, $state, Scholarship) {
      function transitionToOverlay() {
        return $state.go('semesters-list');
      }

      modal = $modal
        .open({
          size: 'lg',
          controller: 'ScholarshipsShowCtrl as showCtrl',
          templateUrl: '/app/scholarships.show/'+ 'scholarships-show' + '.html',
          resolve : {
            origin_scholar : function(){
              return false;
            }
          }
        });


      modal
        .result
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }


    var state = {
      url: '/{id}',
      name: 'semesters-list.show',
      data: data,
      resolve: resolves,
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);
