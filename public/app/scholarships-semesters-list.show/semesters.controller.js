+(function(angular, undefined) {

  function SemestersAltCtrl($scope, $controller, $state, UtilService, $stateParams, Scholarship, Semester, SEMESTER_ALT_COLUMNS, Restangular) {
    // Inherit BaseIndexCtrl's prototype,
    var controller = $controller('BaseIndexCtrl', {
        $scope: $scope,
        $state: $state,
        UtilService: UtilService
    });

    angular.extend(this, controller);

    var vm = this;

    // Initialize vm data
    vm.show = show;
    vm.collection = Semester.instance;
    vm.columns = SEMESTER_ALT_COLUMNS;

    // Manually bind the "this"
    // from the base controller
    vm.callServer = angular.bind(vm, vm.callServer);
    vm.applyPageData = angular.bind(vm, vm.applyPageData);
    vm.catchPageDataError = angular.bind(vm, vm.catchPageDataError);
    vm.getPageData = getPageData;

    // getPageData();

    function show(id) {
      console.log(id);
      $state.go('semesters-list.show.semester-edit', { sem_id: id });
    }

    /** 
     * Fetches data and replaces the current
     * @param  string query
     * @return void
     */
    function getPageData(query) {
        vm.isLoading = true;

      return Restangular
        .one('scholarships', Scholarship.instance.id)
        .getList('semesters', angular.extend({}, { limit: 9999 }, query))
        .then(function(response) {
          vm.collection = response.data;
          Semester.instance = response.data;
        })
        .finally(function() { vm.isLoading = false });
    }
    function addAwardToggle(){
      vm.add_award_toggle = !vm.add_award_toggle;
    }

  }

  angular
    .module('app')
    .controller('SemestersAltCtrl', SemestersAltCtrl);
})(angular);
