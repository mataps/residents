+(function(angular, _, undefined) {
  function TransferCtrl($scope, $modal, SUBJECTS_DEFAULT, Scholarship, Restangular, flash, $state, formResolve) {
    var vm = this;
    var scholarship = angular.copy(formResolve);

    angular.extend(vm, {
      create: create,
      form: { data: {} },
      scholarship: scholarship
    });    

    /**
     * @description  Sends an XHR to the server with
     *               the form data for create a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    function create(formData) {
      return Restangular
        .one('scholarships', scholarship.id)
        .post('transfer', formData)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    }

    function successXHRCallback(response) {
      flash.success = 'Scholarship has been successfully created!';
      $state.go('scholarships', null, { reload: true });
    }

    function errorXHRCallback(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }
  }

  angular
    .module('app')
    .controller('TransferCtrl', TransferCtrl);
})(angular, _);