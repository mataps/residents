+(function(angular, _, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('SemesterReleaseBulkClaimController', SemesterReleaseBulkClaimController);

  function SemesterReleaseBulkClaimController($scope, $state, $modal, Restangular, flash, $http, SEMESTERS_LIST_CLAIM_COLUMNS) {
    angular.extend($scope, {
      data: {
        release: '',
        release_id: '',
        selected: false
      },
      semesters: [],
      columns: SEMESTERS_LIST_CLAIM_COLUMNS,
      isLoading: false,
      isSemestersLoading: false
    });

    $scope.$watch(function() {
      return $scope.data.release_id;
    }, function(changed, old) {
      if ( changed === old ) return;

      $scope.isSemestersLoading = true;

      return $http.get('/api/v1/releases/' + changed + '/semesters?limit=999999')
        .then(function(res) {
          $scope.data.selected = false;
          $scope.semesters = res.data.data;
        })
        .finally(function() {
          $scope.isSemestersLoading = false;
        });
    });

    $scope.$watch(function() {
      return $scope.data.selected;
    }, function(changed, old) {
      console.log(changed, old);
      if ( changed === old ) {
        return;
      }

      $scope.semesters.forEach(function(semester) {
        semester._selected = changed;
      });
    });

    // ------------------------- //
    // ------------------------- //

    /**
     * @description  Sends an XHR to the server with
     *               the form data for edit a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    $scope.$claim = function() {
      var release = $scope.data.release_id;
      var semesters = $scope.semesters
        .filter(function(s) { return s._selected == true; })
        .map(function(s) { return s.id; });

      if ( !semesters.length ) {
        flash.error = 'Please select a semester.';
        return;
      }

      $scope.isLoading = true;
      return $http.post('api/v1/releases/' + release + '/semesters/' + semesters)
        .then(function(r) {
          flash.success = 'Bulk Claim was succcessfull!';
          $state.go('semesters-list', null, { reload: true });
          $scope.$close();
        })
      .catch(function(response) {
          flash.to('validation').error = _.chain(response.data.errors)
            .values()
            .flatten()
            .value();
      })
      .finally(function() {
        $scope.isLoading = false;
      });
    }

    /**
     *
     */
    $scope.$requestReleases = function($value) {
      return $http.get('/api/v1/releases?filters=' + JSON.stringify({ uuid: $value }))
        .then(function(response) {
          return response.data.data;
        });
    }
  }
})(angular, _);
