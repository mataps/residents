+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .run(runBlock);

  function runBlock($templateCache) {
    var template = [
      '<a>',
        '<h5 bind-html-unsafe="match.model.uuid | typeaheadHighlight:query"></h5>',
        '<h5><small><span ng-repeat="type in match.model.scholarship_types.data">{{:: ($index > 0 ? \' ,\' : \'\') + type.name }}</span></small></h5>',
      '</a>'
    ].join('');

    $templateCache.put(
      'gc-release-typeahead-$template.html',
      template
    );
  }
})(angular);
