+(function(angular, undefined) {
  var status = [
    '<span ng-switch="semester.scholarship_status">',
      '<span ng-switch-when="waiting" class="label label-default">Waiting</span>',
      '<span ng-switch-when="for_verification" class="label label-primary">For Verification</span>',
      '<span ng-switch-when="active" class="label label-success">Active</span>',
      '<span ng-switch-when="denied" class="label" style="color: #333; background: #fff">Denied</span>',
      '<span ng-switch-when="closed" class="label" style="color: white; background: #333;">Closed</span>',
      '<span ng-switch-when="inactive" class="label" style="color: white; background: #333;">Inactive</span>',
      '<span ng-switch-when="stop" class="label" style="background: #f00">Stop</span>',
      '<span ng-switch-when="graduated" class="label" style="background: #E7E426; color: #333;">Graduated</span>',
    '</span>'
  ].join('');


var probation = [
    '<span ng-switch="semester.is_probation">',
      '<span ng-switch-when="true" class="label label-error">Under Probation</span>',
      '<span ng-switch-when="0" class="></span>',
    '</span>'
].join('');

  var columns = [
    {
      key: 'uuid',
      label: 'UUID',
      show: true,
      cellTemplate: '{{:: semester.uuid }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
        {
      key: 'school_year',
      label: 'School Year',
      show: true,
      cellTemplate: '{{:: semester.school_year }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
    {
      key: 'year_level',
      label: 'Yr. Level',
      show: true,
      cellTemplate: '{{:: semester.year_level }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
    {
      key: 'term',
      label: 'Term',
      show: true,
      cellTemplate: '{{:: semester.term }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
    {
      key: 'resident_full_name',
      label: 'Full Name',
      show: false,
      cellTemplate: '{{:: semester.resident_full_name }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    }, 
    {
      key: 'first_name',
      label: 'First Name',
      show: false,
      cellTemplate: '{{:: semester.first_name}}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },   
    {
      key: 'middle_name',
      label: 'Middle Name',
      show: false,
      cellTemplate: '{{:: semester.middle_name}}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },  
    {
      key: 'last_name',
      label: 'Last Name',
      show: false,
      cellTemplate: '{{:: semester.last_name}}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },     
    {
      key: 'gwa',
      label: 'GWA',
      show: true,
      cellTemplate: '{{:: semester.gwa }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
    {
      key: 'subject_count',
      label: 'Subject Count',
      show: true,
      cellTemplate: '{{:: semester.subject_count }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
    {
      key: 'course_name',
      label: 'Course',
      show: true,
      cellTemplate: '{{:: semester.course_name }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
    {
      key: 'school_name',
      label: 'School',
      show: true,
      cellTemplate: '{{:: semester.school_name }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
        {
      key: 'school_level',
      label: 'School Level',
      show: true,
      cellTemplate: '{{:: semester.school_level }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },

    {
      key: 'comment',
      label: 'Remarks',
      show: true,
      cellTemplate: '{{:: semester.comment }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
    // {
    //   key: 'scholarship.status',
    //   label: 'Remarks',
    //   show: true,
    //   cellTemplate: '{{:: semester.scholarship.status }}'
    //   // onClick: 'residents.show(semester.id, semester.es_id)'
    // },
    {
      key: 'scholarship_type_name',
      label: 'Scholarship Type',
      show: true,
      cellTemplate: '{{:: semester.scholarship_type_name }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
       {
      key: 'scholarship_status',
      label: 'Scholarship Status',
      show: true,
      cellTemplate: status,
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
       {
      key: 'status',
      label: 'Semester Status',
      show: true,
      cellTemplate: '{{:: semester.status }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
    {
      key: 'is_probation',
      label: 'Probationary',
      show: true,
      cellTemplate: probation,
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
        {
      key: 'allowances',
      label: 'Allowances',
      show: true,
      cellTemplate: '{{:: semester.allowances }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
    {
      key: 'father_name',
      label: 'Father',
      show: true,
      cellTemplate: '{{:: semester.father_name }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },    
    {
      key: 'mother_name',
      label: 'Mother',
      show: true,
      cellTemplate: '{{:: semester.mother_name }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
    {
      key: 'guardian_name',
      label: 'Guardian',
      show: true,
      cellTemplate: '{{:: semester.guardian_name }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
      {
      key: 'client_name',
      label: 'Client',
      show: true,
      cellTemplate: '{{:: semester.client_name }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
 
    {
      key: 'scholar_contact_no',
      label: 'Scholar Contact #',
      show: false,
      cellTemplate: '{{:: semester.scholar_contact_no }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
    {
      key: 'parent_contact_no',
      label: 'Scholar Contact #',
      show: false,
      cellTemplate: '{{:: semester.parent_contact_no }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },    
    {
      key: 'guardian_contact_no',
      label: 'Parents Contact #',
      show: false,
      cellTemplate: '{{:: semester.guardian_contact_no }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
    {
      key: 'others_contact_no',
      label: 'Other Contact #',
      show: false,
      cellTemplate: '{{:: semester.others_contact_no }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },
     {
      key: 'cityMunicipality',
      label: 'City',
      show: false,
      cellTemplate: '{{:: semester.cityMunicipality }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    },  
      {
      key: 'barangay',
      label: 'Barangay',
      show: false,
      cellTemplate: '{{:: semester.barangay }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    }, 
    {
      key: 'created_at',
      label: 'Date Submitted',
      show: true,
      cellTemplate: '{{:: semester.created_at }}',
      onClick: 'semestersList.show(semester.scholarship_id)'
    }, 
  ];

  angular
    .module('app')
    .constant('SEMESTERS_LIST_COLUMNS', columns);
})(angular);