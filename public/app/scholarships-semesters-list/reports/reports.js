+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('SemestersListReportController', ScholarshipReportController);

  function ScholarshipReportController($scope, $http, $window, typeResolve, filterResolve, columnsResolve) {
    angular.extend($scope, {
      selected: (function(columns, obj) {
        columns.map(function(c) { obj[c.key] = c.show; });
        return obj;
      })(columnsResolve, {}), // Selected columns
      columns: columnsResolve,
      type: typeResolve,

      getURL: getURL
    });

    function getURL(type) {
      var keys = Object.keys($scope.selected).filter(function(c) { return $scope.selected[c]; });

      return $window.location.origin +
        '/api/v1/semesters/reports/' + typeResolve +
        '?filters=' + JSON.stringify(filterResolve) +
        '&fields=' + keys.join(',');
    }
  }
})(angular);