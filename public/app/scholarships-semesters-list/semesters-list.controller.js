+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('SemestersListCtrl', SemestersListCtrl);

  function SemestersListCtrl($scope, $http,  $controller, UtilService, SEMESTERS_LIST_COLUMNS, Semester, Restangular, flash, $state, $window, $modal) {
    // Inherit BaseIndexCtrl's prototype,
    var controller = $controller('BaseIndexCtrl', {
        $scope: $scope,
        $state: $state,
        UtilService: UtilService
    });

    angular.extend(this, controller);

    // ------------------------- //
    // ------------------------- //
    //
    // viewmodel
    var vm = this,
       // Data collection (grabbed from the service)
      collection = [];

    // Copy items from the service
    // to the local collection variable
    angular.extend(collection, Semester.collection);

    // Initialize vm data
    vm.columns = SEMESTERS_LIST_COLUMNS;
    vm.collection = collection;
    console.log(collection)
    // Manually bind the "this"
    // from the base controller
    vm.callServer = angular.bind(vm, vm.callServer);
    vm.applyPageData = angular.bind(vm, vm.applyPageData);
    vm.catchPageDataError = angular.bind(vm, vm.catchPageDataError);

    // Commands that contact the API / server
    vm.getSelected = angular.bind(vm, vm.getSelected);
    vm.remove = remove;
    vm.getPageData = getPageData;
    vm.show = show;
    vm.xlsGenerator = xlsGenerator;
    vm.$addToAffiliation = $addToAffiliation;
    vm.$addToAttribution = $addToAttribution;
    vm.$addToTransaction = $addToTransaction;
    vm.$report = $report;
    vm.loadAttribute = loadAttribute;
    vm.$f = $f;
    vm.$claim = $claim;
    // Residents
    Restangular.service('affiliations').getList().then(function(response){
      vm.affiliations = response.data;
    })

    /**
     * @description  Sends a DELETE to the server
     * @return Restangular
     */
    function remove() {
      var selected = vm.getSelected().join(',');

      if ( !selected.length ) {
        flash.error = 'Please select a Semester.';
        return;
      }

      if( !confirm('Please confirm deleting the selected records.'))
        return;

      vm.load();

      return Semester
        .Command
        .delete(selected)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    };

    vm.QQ = {};

    /**
     * Fetches data and replaces the current
     * @param  string query
     * @return void
     * &filters={"birthdate": { "from": 'asdasd': "to"}}
     */
    function getPageData(query) {
      // if ( query.page ) {
      //   // console.log(query);
      //   // console.log(vm.QQ);
      //   angular.extend(vm.QQ, query);
      //   console.log(vm.QQ);
      // } else {
      //   vm.QQ = query;
      //   console.log(vm.QQ);
      // }

      if ( !query.filters ) {
        query.filters = angular.copy(query);
      }

      if ( query.page ) {

      }

      // if ( !angular.isUndefined(query.filters) &&
      //   ( !angular.isUndefined(query.filters['birthdate.to']) || !angular.isUndefined(query.filters['birthdate.from']) )
      // ) {
      //   if ( !angular.isUndefined(vm.QQ.filters.birthdate) ) {
      //     angular.extend(vm.QQ.filters, query.filters);
      //     vm.QQ.filters = _.omit(vm.QQ.filters, 'birthdate.to');
      //     vm.QQ.filters = _.omit(vm.QQ.filters, 'birthdate.from');
      //   }
      // }

      if ( query.page ) {
        query.filters = angular.copy(vm.QQ.filters);
      }

      if ( query.filters && ! query.filters['affiliations'] ) {
        query.filters['affiliations'] = vm.QQ['affiliations'];
      }

      if ( query.filters && ! query.filters['attributuons'] ) {
        query.filters['attributions'] = vm.QQ['attributions'];
      }

      vm.QQ.filters = angular.copy(query.filters);

      if ( query.filters.page ) {
        query.filters = _.omit(query.filters, 'page');
      }

      if ( query.filters.limit ) {
        query.filters = _.omit(query.filters, 'limit');
      }

      return Semester
        .Query
        .all(query )
        .then(vm.applyPageData)
        .catch(vm.catchPageDataError);
    }

    vm.onQueryChange = onQueryChange;
    function onQueryChange() {
      // console.log(vm._selected_affiliation);
      // console.log(vm._selected_attributes);
      vm.QQ['attributions'] = vm._selected_attributes;
      vm.QQ['affiliations'] = vm._selected_affiliation;

      if ( vm.QQ.filters ) {
        vm.QQ.filters['affiliations'] = vm._selected_affiliation;
        vm.QQ.filters['attributions'] = vm._selected_attributes;
      }

      if ( vm.QQ['affiliations'] === 'ALL') {
        vm.QQ['affiliations'] = undefined;

        if ( vm.QQ.filters ) {
          vm.QQ.filters['affiliations'] = undefined;
        }
      }

      getPageData(vm.QQ);
    }

    // Success XHR
    function successXHRCallback(response) {
      var message = 'Semester has been successfully archived!';
      flash.success = message;
      $state.reload();
      vm.load(false);
    }

    // Error XHR
    function errorXHRCallback(response) {
      var message = 'An error has occured. Please try again..';
      flash.to('validation').error = message;
      vm.load(false);
    }

    function show(sid) {
      $state.go('semesters-list.show', {id : sid});
    }


    function xlsGenerator() {
      console.log(vm.QQ);
      return [
        $window.location.origin,
        '/api/v1/residents/reports/spreadsheet',
        '?filters=',
        JSON.stringify(vm.QQ.filters)
      ].join('');
    }

    function $addToAffiliation() {
      var selected = vm.getSelected().join(',');

      if ( !selected.length ) {
        flash.error = 'Please select (a) Semester(s) to be bulk added to an affiliation.';
        return;
      }

      $modal.open({
        templateUrl: '/app/residents/bulk/affiliations/bulk.html',
        controller: 'ResidentsBulkAddAffiliationController',
        resolve: { selectedResolve: function() { return selected; }}
      })
    }

    function $addToAttribution() {
      var selected = vm.getSelected().join(',');

      if ( !selected.length ) {
        flash.error = 'Please select (a) Semester(s) to be bulk added to an attribution.';
        return;
      }

      $modal.open({
        templateUrl: '/app/residents/bulk/attributions/bulk.html',
        controller: 'ResidentsBulkAddAttributionController',
        resolve: { selectedResolve: function() { return selected; }}
      })
    }

    function $addToTransaction() {
      var selected = vm.getSelected().join(',');

      if ( !selected.length ) {
        flash.error = 'Please select (a) Semester(s) to be bulk added to a transaction.';
        return;
      }

      $modal.open({
        templateUrl: '/app/residents/bulk/transactions/bulk.html',
        controller: 'ResidentsBulkAddTransactionController',
        resolve: { selectedResolve: function() { return selected; }}
      })
    }


    function loadAttribute(query) {
      return $http.get('api/v1/attributions/search');
    }

    function $f() {
      console.log('Called');
    }

    function $claim() {
      $modal.open({
        templateUrl: '/app/scholarships-semesters-list/claim/claim.html',
        size: 'lg',
        controller: 'SemesterReleaseBulkClaimController'
      });
    }

   function $report(type) {
      $modal.open({
        templateUrl: 'app/scholarships-semesters-list/reports/reports.html',
        controller: 'SemestersListReportController',
        resolve: {
          typeResolve: function() {
            return type;
          },
          columnsResolve: function() {
            return vm.columns;
          },
          filterResolve: function() {
            return vm.__query.filters;
          }
        }
      });
    }

  }
})(angular);
