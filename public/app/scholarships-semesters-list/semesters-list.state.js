+(function(angular, undefined) {
  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var data = { pageTitle: 'Manage Scholarship Semesters' };
    var resolves = { getResidentsCollectionResolve: getResidentsCollectionResolve };
    
    var state = {
      parent: 'main',
      name: 'semesters-list',
      url: '/scholarship-alt',
      data: data,
      resolve: resolves,
      controllerAs: 'semestersList',
      controller: 'SemestersListCtrl',
      templateUrl: '/app/scholarships-semesters-list/semesters-list.html'
      //views: {
      //  'tools': { templateUrl: '/app/residents/partials/tools.html', controller: '' },
      //  'content': { templateUrl: '/app/residents/residents.html', controller: 'ResidentsCtrl as residents' }
      //}
    };
    
    $stateProvider.state(state);
  }

  function getResidentsCollectionResolve(Semester) {
    return Semester
      .Repository
      .all();
  };
})(angular);