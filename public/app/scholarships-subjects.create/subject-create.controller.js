+(function(angular, undefined) {
  function SubjectsCreateCtrl($scope, $state, flash, Subject) {
    var vm = this,
      form = {
        data: {},
      };

    // ------------------------- //
    // ------------------------- //
    vm.form = form;
    vm.create = create;

    ////////////////////////////////
    // Todo loading
    ////////////////////////////////

    /**
     * @description  Sends an XHR to the server with
     *               the form data for create a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    function create(formData) {
      console.log(formData);
      Subject
        .Command
        .create(formData)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    }

    function successXHRCallback(response) {
      flash.success = 'Subject has been successfully registered!';

      $state.go('scholarship-subjects', null, { reload: true });
    }

    function errorXHRCallback(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }
  }

  angular
    .module('app')
    .controller('SubjectsCreateCtrl', SubjectsCreateCtrl);
})(angular);