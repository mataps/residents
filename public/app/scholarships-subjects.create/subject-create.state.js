+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'New subject' };

    var modal;
    // 
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('scholarship-subjects');
      }
        
      modal = $modal
        .open({
          size: 'sm',
          controller: 'SubjectsCreateCtrl as createCtrl',
          templateUrl: '/app/scholarships-subjects.create/subject-create.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    //
    function onExit() {
      modal.close();
    }
    

    var state = {
      url: '/create',
      name: 'scholarship-subjects.create',
      data: data,
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);