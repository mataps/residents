+(function(angular, undefined) {
  function SubjectsEditCtrl($scope, $stateParams, $state, flash, UtilService, Subject) {
    var vm = this,
      // Form defaults
      form = { data: Subject.getInstance() };

    // ------------------------- //
    // ------------------------- //    
    
    vm.update = update;

    // Form data
    vm.form = form;

    // ------------------------- //
    // ------------------------- //    

    /**
     * @description  Sends an XHR to the server with
     *               the form data for create a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    function update(formData) {
      return Subject
        .Command
        .update(formData)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    }

    function successXHRCallback(response) {
      flash.success = 'Subject has been successfully updated!';

      $state.go('scholarship-subjects', null, { reload: true });
    }

    function errorXHRCallback(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }
  }

  angular
    .module('app')
    .controller('SubjectsEditCtrl', SubjectsEditCtrl);
})(angular);