+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Edit subject' };

    var modal;

    /**
     * @see  School.Repository.get
     * @param  {Object} School DI
     * @return promise
     */
    function getSubjectByIdResolve(Subject, $state, $stateParams) {
      return Subject
        .Repository
        .get($stateParams.id);
    }

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = { getSubjectByIdResolve: getSubjectByIdResolve };
    // 
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('scholarship-subjects');
      }
        
      modal = $modal
        .open({
          controller: 'SubjectsEditCtrl as editCtrl',
          templateUrl: '/app/scholarships-subjects.edit/subjects-edit.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }
    

    var state = {
      url: '/{id}/edit',
      name: 'scholarship-subjects.edit',
      data: data,
      resolve: resolves,
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);