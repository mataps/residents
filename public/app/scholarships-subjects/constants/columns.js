+(function(angular, undefined) {
  var columns = [
    {
      key: 'name',
      label: 'Name',
      show: true,
      cellTemplate: '{{:: subject.name }}',
      onClick: 'subjects.show(subject.id)'
    },
    {
      key: 'description',
      label: 'Description',
      show: true,
      cellTemplate: '{{:: subject.description }}',
      onClick: 'subjects.show(subject.id)'
    },
  ];

  angular
    .module('app')
    .constant('SUBJECTS_COLUMNS', columns);
})(angular);