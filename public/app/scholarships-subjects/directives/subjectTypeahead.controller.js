+(function(angular, undefined) {
  angular
    .module('app')
    .controller(
      'GCSubjectTypeaheadController',
      SubjectTypeaheadController
    );
    
  function SubjectTypeaheadController($scope, Subject) {
    //
    var vm = this;

    transform();

    vm.request = request;
    vm.selectCb = selectCb;

    /**
     * Requests data to the server
     * @return promise
     */
    function request(name) {
      return Subject
        .Query
        .typeahead(name)
        .then(function(response) {
          return response.data;
        });
    }

    function selectCb($item, $model, $label) {
      vm.idVariable = $item.id;
    }

    /** Transform data */
    function transform() {
      if ( !angular.isUndefined(vm.transformIdVariable) ) {
        vm.idVariable = vm.transformIdVariable;
      }
    }
  }
})(angular);