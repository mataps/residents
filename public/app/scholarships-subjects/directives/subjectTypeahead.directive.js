+(function(angular, undefined) {
  angular
    .module('app')
    .directive('gcSubjectTypeahead', subjectTypeahead);

  function subjectTypeahead($compile, $sce) {
    // Typeahead attributes
    var typeaheadOnSelect = 'subjectTypeahead.selectCb($item, $model, $label)';
    var typeahead = [
      'subject.name for subject ',
      'in subjectTypeahead.request($viewValue)'
    ].join('');
    var placeholder = 'Enter subject name...';

    var scope = {
      idVariable: '=',
      transformIdVariable: '@',
      model: '=ngModel' // Avoid ng-model conflcit with typehead
    };

    var template = [
      '<div>',
        '<input type="text" class="form-control" ',
        'placeholder="', placeholder, '" ng-model="subjectTypeahead.model" ',
        'typeahead="', typeahead, '" ',
        'typeahead-on-select="', typeaheadOnSelect, '">',
      '</div>'
    ].join('');

    return {
      scope: scope,
      restrict: 'EA',
      template: template,
      bindToController: true,
      controllerAs: 'subjectTypeahead',
      controller: 'GCSubjectTypeaheadController',
      replace: true
    };
  }
})(angular);