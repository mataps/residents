+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('SubjectsCtrl', SubjectsCtrl);

  function SubjectsCtrl($scope, $controller, $state, UtilService, Subject, SUBJECTS_COLUMNS, flash) {
    // Inherit BaseIndexCtrl's prototype,
    var controller = $controller('BaseIndexCtrl', {
        $scope: $scope,
        $state: $state,
        UtilService: UtilService
    });

    angular.extend(this, controller);

    // ------------------------- //
    // ------------------------- //

    // viewmodel
    var vm = this,    
       // Data collection (grabbed from the service)
      collection = [];

    // Copy items from the service
    // to the local collection variable
    angular.extend(collection, Subject.collection);

    // ------------------------- //
    // ------------------------- //

    // Initialize vm data
    vm.columns = SUBJECTS_COLUMNS;
    vm.collection = collection;

    // Manually bind the "this"
    // from the base controller
    vm.callServer = angular.bind(vm, vm.callServer);
    vm.applyPageData = angular.bind(vm, vm.applyPageData);
    vm.catchPageDataError = angular.bind(vm, vm.catchPageDataError);

    // Commands that contact the API / server
    vm.remove = remove;
    vm.getPageData = getPageData;

    /**
     * @description  Sends a DELETE to the server
     * @see  getPageData
     * @return void
     */
    function remove() {
      if(!confirm('Please confirm deleting the selected records.'))
        return;

      vm.load();
      var selected = vm.getSelected().join(',');

      return Subject
        .Command
        .delete(selected)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    }

    // Success XHR
    function successXHRCallback(response) {
      flash.success = 'Subject has been successfully saved!';
      $state.reload();
      vm.load(false);
    }

    // Error XHR
    function errorXHRCallback(response) {
      flash.error = 'An error has occured. Please try again...';
      vm.load(false);
    }

    /**
     * Fetches data and replaces the current
     * @param  string query
     * @return void
     */
    function getPageData(query) {
      return Subject
        .Query
        .all(query)
        .then(vm.applyPageData)
        .catch(vm.catchPageDataError);
    }
  }
})(angular);