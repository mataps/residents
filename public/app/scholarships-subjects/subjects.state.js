+(function(angular, undefined) {
  angular
    .module('app')
    .config(stateConfig);

  function stateConfig($stateProvider) {

    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Manage subjects' };

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = { getSubjectsCollectionResolve: getSubjectsCollectionResolve };

    var state = {
      parent: 'main',
      url: '/scholarships/subjects',
      name: 'scholarship-subjects',
      data: data,
      resolve: resolves,
      controllerAs: 'subjects',
      controller: 'SubjectsCtrl',
      templateUrl: '/app/scholarships-subjects/subjects.html'
    };

    $stateProvider.state(state);
  }

  /**
   * @description Fetches all subjects
   * @param  Restangular
   * @return Restangular
   */
  function getSubjectsCollectionResolve (Subject) {
    return Subject
      .Repository
      .all();
  }
})(angular);