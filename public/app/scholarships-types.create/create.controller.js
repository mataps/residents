+(function(angular, undefined) {
  function ScholarshipTypesCreateController($scope, School, flash, $state, UtilService) {
    var vm = this,
      form = { data: {} };

    // ------------------------- //
    // ------------------------- //
    vm.form = form;
    vm.isLoading = false;
    vm.successXHRCallback = successXHRCallback;
    vm.errorXHRCallback = errorXHRCallback;

    function successXHRCallback(response) {
      load(false);

      flash.success = 'Scholarship type has been successfully created!';

      $state.go('scholarships-types', null, { reload: true });
    }

    function errorXHRCallback(response) {
      load(false);

      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }

    // Setter for the isLoading prop
    function load(bool) { vm.isLoading = UtilService.tralse(bool); }
  }

  angular
    .module('app')
    .controller('ScholarshipTypesCreateController', ScholarshipTypesCreateController);
})(angular);