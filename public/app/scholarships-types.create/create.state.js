+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'New Scholarship' };
    var modal;
    
    // 
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('scholarships-types');
      }
        
      modal = $modal
        .open({
          size: 'sm',
          controller: 'ScholarshipTypesCreateController as createCtrl',
          templateUrl: '/app/scholarships-types.create/create.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }
    

    var state = {
      url: '/create',
      name: 'scholarships-types.create',
      data: data,
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);