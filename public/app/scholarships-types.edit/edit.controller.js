+(function(angular, undefined) {
  function ScholarshipTypeEditController($scope, $stateParams, $state, flash, UtilService, ScholarshipType) {
    var vm = this,
      // Form defaults
      form = { data: ScholarshipType.getInstance() };

    // Form data
    vm.form = form;
    vm.successXHRCallback = successXHRCallback;
    vm.errorXHRCallback = errorXHRCallback;

    function successXHRCallback(response) {
      flash.success = 'Scholarship Type has been successfully updated!';

      $state.go('scholarships-types', null, { reload: true });
    }

    function errorXHRCallback(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }
  }

  angular
    .module('app')
    .controller('ScholarshipTypeEditController', ScholarshipTypeEditController);
})(angular);