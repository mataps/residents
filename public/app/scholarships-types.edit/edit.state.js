+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Edit Scholarship Type' };

    var modal;

    /**
     * @see  School.Repository.get
     * @param  {Object} School DI
     * @return promise
     */
    function getScholarshipTypeByIdResolve(ScholarshipType, $state, $stateParams) {
      return ScholarshipType
        .Repository
        .get($stateParams.id);
    }

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = { getScholarshipTypeByIdResolve: getScholarshipTypeByIdResolve };
    // 
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('scholarships-types');
      }
        
      modal = $modal
        .open({
          controller: 'ScholarshipTypeEditController as editCtrl',
          templateUrl: '/app/scholarships-types.edit/edit.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }
    

    var state = {
      url: '/{id}/edit',
      name: 'scholarships-types.edit',
      data: data,
      resolve: resolves,
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);