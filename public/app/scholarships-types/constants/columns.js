+(function(angular, undefined) {
  var columns = [
    {
      key: 'name',
      label: 'Name',
      show: true,
      cellTemplate: '{{:: type.name }}',
      onClick: 'typse.show(type.id)'
    },
    {
      key: 'description',
      label: 'Description',
      show: true,
      cellTemplate: '{{:: type.description }}',
      onClick: 'types.show(type.id)'
    }
  ];

  angular
    .module('app')
    .constant('SCHOLARSHIP_TYPES_COLUMNS', columns);
})(angular);