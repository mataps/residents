+(function(angular, undefined) {
  function ScholarshipTypesController($scope, $controller, $state, UtilService, ScholarshipType, SCHOLARSHIP_TYPES_COLUMNS, flash) {
    // Inherit BaseIndexCtrl's prototype,
    var controller = $controller('BaseIndexCtrl', {
        $scope: $scope,
        $state: $state,
        UtilService: UtilService
    });

    angular.extend(this, controller);

    // ------------------------- //
    // ------------------------- //

    // viewmodel
    var vm = this;
    
    // Data collection (grabbed from the service)
    var collection = [];

    // Copy items from the service
    // to the local collection variable
    angular.extend(collection, ScholarshipType.collection);

    // ------------------------- //
    // ------------------------- //

    // Initialize vm data
    vm.columns = SCHOLARSHIP_TYPES_COLUMNS;
    vm.collection = collection;

    // Manually bind the "this"
    // from the base controller
    vm.callServer = angular.bind(vm, vm.callServer);
    vm.applyPageData = angular.bind(vm, vm.applyPageData);
    vm.catchPageDataError = angular.bind(vm, vm.catchPageDataError);

    // Commands that contact the API / server
    vm.remove = remove;
    vm.getPageData = getPageData;

    vm.show = show;

    /**
     * @description  Sends a DELETE to the server
     * @see  getPageData
     * @return void
     */
    function remove() {
      if(!confirm('Please confirm deleting the selected records.'))
        return;

      vm.load();
      var selected = vm.getSelected();

      return ScholarshipType
        .Command
        .delete(selected)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    }

    function show(id) {
     
      $state.go('scholarships-types.edit', {id: id});
    }

    // Success XHR
    function successXHRCallback(response) {
      flash.success = 'Scholarship Type has been successfully saved!';
      $state.reload();
      vm.load(false);
    }

    // Error XHR
    function errorXHRCallback(response) {
      flash.error = 'An error has occured. Please try again...';
      vm.load(false);
    }

    /**
     * Fetches data and replaces the current
     * @param  string query
     * @return void
     */
    function getPageData(query) {
      ScholarshipType
        .Query
        .all(query)
        .then(vm.applyPageData)
        .catch(vm.catchPageDataError);
    }
  }

  angular
    .module('app')
    .controller('ScholarshipTypesController', ScholarshipTypesController);
})(angular);