+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * @description Fetches all ScholarshipType
     * @param  Restangular
     * @return Restangular
     */
    function getScholarshipTypesCollectionResolve (ScholarshipType) {
      return ScholarshipType
        .Repository
        .all();
    }

    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Manage scholarship Types' };

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = { getScholarshipTypesCollectionResolve: getScholarshipTypesCollectionResolve };

    /**
     * State Config
     * @type {Object}
     */
    var state = {
      parent: 'main',
      url: '/scholarships/types',
      name: 'scholarships-types',
      controllerAs: 'types',
      controller: 'ScholarshipTypesController',
      data: data,
      resolve: resolves,
      templateUrl: '/app/scholarships-types/types.html'
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);