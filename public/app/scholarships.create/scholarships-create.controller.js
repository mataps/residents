+(function(angular, _, undefined) {
  function ScholarshipsCreateCtrl($scope, $modal, SUBJECTS_DEFAULT, Scholarship, flash, $state) {
    var vm = this;

    angular.extend(vm, {
      create: create,
      createResident: createResident,
      form: {
        data: { gwa: '0', resident_id: '', resident_name: '' }
      }
    });    

    /**
     * @description  Sends an XHR to the server with
     *               the form data for create a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    function create(formData) {
      var dt = new Date();
      var date = dt.getFullYear() + "/" + (dt.getMonth() + 1) + "/" + dt.getDate();
      formData.applied_at = date;
      console.log(formData);
      return Scholarship
        .Command
        .create(formData)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    }

    function successXHRCallback(response) {
      flash.success = 'Scholarship has been successfully created!';
      $scope.$close();
      $state.go('scholarships.show', {id:response.data.data.id});
    }

    function errorXHRCallback(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }

    function createResident() {
      $modal.open({
        templateUrl: '/app/_main/modals/residents/add-resident.html',
        controller: '__AddResidentCtrl as _modal_',
        size: 'lg',
        resolve: {
          datResolve: function() {
            return function(response) {
              console.log(response);
              vm.form.data.resident_name = response.data.data.full_name;
              vm.form.data.resident_id = response.data.data.id;
            }
          }
        }
      });      
    }
  }

  angular
    .module('app')
    .controller('ScholarshipsCreateCtrl', ScholarshipsCreateCtrl);
})(angular, _);