+(function(angular, undefined) {
  function stateConfig($stateProvider) {

    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Manage Scholarship Settings' };

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = { };

    var state = {
      parent: 'main',
      url: '/scholarships/settings',
      name: 'scholarship-settings',
      controller: 'ScholarshipSettingsCtrl',
      data: data,
      resolve: resolves,
      templateUrl: '/app/scholarships.settings/tpl.html'
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);