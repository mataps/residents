+(function(angular, _, undefined) {
  "use strict";
  angular
    .module('app')
    .controller('GCSubjectFormCreateController', GCSubjectFormCreateController);

  function GCSubjectFormCreateController($scope, $http, SubjectFormService, Subject, $state, flash) {
    // public methods
    $scope.addSubject = addSubject;
    $scope.removeSubject = removeSubject;
    $scope.subjectTypeahead = subjectTypeahead;

    /**
     * Pushes a new subject to the "subject array"
     * @see  form.data.subjects
     */
    function addSubject() {
      $scope.grades.splice(0, 0, {
        subject: {
          id: '',
          name: ''
        },
        grade: 0,
        units: 0
      });
    }

    /**
     * @param  {int} index Position of the subject
     * @see  form.data.subjects
     */
    function removeSubject(index) {
      if ( !confirm('Are you sure to remove this grade?') ) {
        return;
      }

      $scope.grades.splice(index, 1);
    }

    /**
     * @name computeGWA
     * @see GWAService.compute
     * @description Used to compute the current GWA
     */
    function computeGWA(newValue, oldValue) {
      if ( newValue !== oldValue ) {
        $scope.gwa = SubjectFormService.compute($scope.grades);
      }
    }

    /** Sends a request to the server for filtered data */
    function subjectTypeahead(name) {
      return Subject
        .Query
        .typeahead(name)
        .then(function(response) {
          return response.data;
        });
    }

    $scope.$watch('grades', computeGWA, true);
  }
})(angular, _);