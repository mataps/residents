+(function(angular, _, undefined) {
  "use strict";
  angular
    .module('app')
    .directive('gcCreateGradeList', subjectForm);

  function subjectForm() {
    var template = [
      '<div class="box">',
        '<header class="head clearfix">',
          '<h4 class="pull-left" style="margin: 5px; color: #000;">',
            '<button class="btn btn-primary btn-sm success" type="button" ng-click="addSubject()">',
              '<i class="ion-plus-round"></i>',
            '</button>',
          ' Subjects </h4>',
        '</header>',

      '<div class="body">',
        '<div class="form-group row">',
          '<div class="col-md-5">',
              '<label>Subject</label>',
          '</div>',
          '<div class="col-md-2">',
              '<label>Grade</label>',
          '</div>',
          '<div class="col-md-2">',
              '<label>Unit</label>',
          '</div>',
          '<div class="col-md-3">',
              '<label>Options</label>',
          '</div>',
        '</div>',
        '<div style="display: table; height: 150px; width: 100%" ng-if="!grades.length">',
          '<div style="display: table-cell; vertical-align: middle; text-align: center">',
            '<h4><small> No grades yet. </small></h4>',
          '</div>',
        '</div>',
        '<div class="form-group row" ng-repeat="grade in grades" ng-if="!!grades.length">',
          '<div class="col-md-5">',
            '<gc-subject-typeahead id-variable="grade.subject.id" ng-model="grade.subject.name">',
            '</gc-subject-typeahead>',
          '</div>',

          '<div class="col-md-2">',
            '<input id="status" type="text" step="0.25" min="0.25" class="form-control" ng-model="grade.grade">',
          '</div>',

          '<div class="col-md-2">',
            '<input id="status" type="text" step="1" min="1" class="form-control" ng-model="grade.units">',
          '</div>',

          '<div class="col-md-3">',
              '<button class="btn btn-danger btn-xs" type="button" ng-click="removeSubject($index)" type="button" tooltip="Remove the grade">',
                '<i class="ion-trash-a"></i>',
              '</button>',
            '</div>',
          '</div>',
        '</div>',
      '</div>'
    ].join('');

    return {
      scope: { gwa: '=', grades: '=' },

      restrict: 'EA',
      template: template,
      controller: 'GCSubjectFormCreateController'
    };
  }
})(angular);