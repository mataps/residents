+(function(angular, undefined) {
  function SemestersCreateCtrl($scope, $stateParams, UtilService, Scholarship, Semester, School, $state, flash, $location, $http) {
    var vm = this;
      // Form defaults
    var form = {
      data: {
        client_id: '',
        client: {},
        gwa: 0,
        grades: [],
        scholarship_id: $stateParams.id,
        school: Scholarship.instance.school,
        school_id: Scholarship.instance.school.id,
        school_name: Scholarship.instance.school.name,
        status: Scholarship.instance.status
      }
    };

    console.log(Semester.instance);

    vm.isLoading = false;
    vm.submit = submit;
    vm.scholarship = Scholarship.instance;
    vm.semester_status = ['active', 'for_verification', 'waiting'];

    // ------------------------- //

    // Form data
    vm.form = form;
    vm.successXHRCallback = successXHRCallback;
    vm.errorXHRCallback = errorXHRCallback;

    // var 0 = Semester.instance.length - 1;

    if ( Semester.instance[0] !== undefined ) {
      vm.form.data.allowance_basis_id = Semester.instance[0].id;
      vm.form.data.scholarship_type_id = Semester.instance[0].scholarship_type.id;
      vm.form.data.course_id = Semester.instance[0].course.id;
      vm.form.data.course_name = Semester.instance[0].course.name;
      vm.form.data.term = Semester.instance[0].term;
      vm.form.data.school_level = Semester.instance[0].school_level;
      vm.form.data.year_level = Semester.instance[0].year_level;
      vm.form.data.status = Semester.instance[0].status;

      if ( Semester.instance[0].client !== undefined || typeof Semester.instance[0].client == 'object' ) {
        vm.form.data.client = Semester.instance[0].client;
        vm.form.data.client_id = Semester.instance[0].client.id;
      }
    }
    // ------------------------- //

    /** Success callback */
    function successXHRCallback(response) {
      flash.success = 'Semester has been successfully created!';
      
      $location.path($state.href('^', { id: $stateParams.id }, { reload: true }));
    }

    /** Error callback */
    function errorXHRCallback(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }

    function submit() {
      vm.isLoading = true;
      var data = vm.form.data;
      if( Semester.instance[0] !== undefined && vm.form.data.school_level !== Semester.instance[0].school_level ) {
        data.allowance_basis_id = undefined;
      }
      data.grades.map(function(grade) {
        if ( typeof grade.subject === "object" && grade.subject.id ) {
          grade.subject_id = grade.subject.id;
        }

      // YOLO Validation
      // if ( grade.subject.id == undefined || grade.subject.id == null ) {
      //   return flash.error = 'The subject is required.';
      // } else if ( isNaN(data.grade) ) {
      //   return flash.error = 'The grade needs to be a number.';
      // } else if ( isNaN(data.units) ) {
      //   return flash.error = 'The units needs to be a number.';
      // }

        return grade;
      });

      return $http.post('api/v1/semesters', data)
        .then(successXHRCallback)
        .catch(errorXHRCallback)
        .finally(function() { vm.isLoading = false; });
    }

    // $scope.$watch('grades', computeGWA, true);
  }

  angular
    .module('app')
    .controller('SemestersCreateCtrl', SemestersCreateCtrl);
})(angular);
