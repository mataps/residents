+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Create semester' };

    var state = {
      url: '/semesters/create',
      name: 'scholarships.show.semester-create',
      data: data,
      views: {
        'semester@': {
          controller: 'SemestersCreateCtrl',
          controllerAs: 'semCreateCtrl',
          templateUrl: '/app/scholarships.show-semester.create/semester-create.html'
        }
      }
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);
