+(function(angular, undefined) {
  function SemestersEditCtrl($scope, $http, $stateParams, UtilService, Scholarship, Semester, flash, $state) {
    var vm = this;
    // Form defaults
    var form = { data: Semester.getInstance() };
    // form.data.scholarship_id = $stateParams.id;

    // ------------------------- //
    vm.semester_status = ['active', 'for_verification', 'waiting', 'closed', 'stop', 'graduated'];
    vm.school_levels = [ { value : 'collegiate', label :'College' }, { value : 'secondary', label: 'High School'}];
    // Form data
    vm.add_award_toggle =0;
    vm.edit_mode = 0;
    vm._dummy = 0;
    vm.form = form;
    console.log(vm.form);
    vm.toggle = toggle;
    vm.addAwardToggle = addAwardToggle;
    vm.form.data.school_year = form.data.school_year;
    vm.gwa = form.data.gwa;
    vm.form.data.allowance_basis_id = vm.form.data.allowance_basis.id;

    if ( typeof vm.form.data.course == 'object' ) {
      vm.form.data.course_id = vm.form.data.course.id;
      vm.form.data.course_name = vm.form.data.course.name;
      vm.form.data.course_abbreviation = vm.form.data.course.abbreviation;
    }


    if ( typeof vm.form.data.school == 'object' ) {
      vm.form.data.school_name = vm.form.data.school.name;
      vm.form.data.school_id = vm.form.data.school.id;
    }else{
      console.log("no School");
    }
    // window.form = form;
    // console.log(form);
    vm.isLoading = false;
    vm.isUpdating
    vm.successXHRCallback = successXHRCallback;
    vm.errorXHRCallback = errorXHRCallback;
    vm.update = update;

    $scope.$watch('semEditCtrl.form.data.gwa', function(newValue,oldValue) {
      if(vm.gwa !== newValue) {

        updateGWA(vm.form.data.id, {'gwa': vm.form.data.gwa});
      }
    });

    /** Success Callback */
    function successXHRCallback(response) {
      vm.isLoading = false;
      flash.success = 'Semester has been successfully updated!';

      if(vm.edit_mode){
        toggle();
      }

      // $state.go('scholarships-semesters-list.show', { id: $stateParams.id }, { reload: true });
    }

    /** Error Callback */
    function errorXHRCallback(response) {
      vm.isLoading = false;

      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }

    function toggle()
    {
      vm.edit_mode = !vm.edit_mode;
    }

    function addAwardToggle() {
      vm.add_award_toggle = !vm.add_award_toggle;
    }


    function update(formData) {
      vm.isUpdating = true;
      return $http.put('/api/v1/semesters/' + formData.id, formData)
        .then(successXHRCallback)
        .catch(errorXHRCallback)
        .finally(function() { vm.isUpdating = false; });
    }

    function updateGWA(semID, gwa) {
      vm.isUpdating = true;
      return $http.put('/api/v1/semesters/' + semID + "/gwa", gwa)
        .finally(function() { vm.isUpdating = false; });
    }
  }

  angular
    .module('app')
    .controller('SemestersEditCtrl', SemestersEditCtrl);
})(angular);
