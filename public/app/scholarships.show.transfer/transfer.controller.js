+(function(angular, undefined) {
  function ScholarshipTransferController($scope, Scholarship, flash, formResolve) {
    var vm = this;
    
    vm.form = { data: {} };
    vm.scholarship = formResolve;
  }

  angular
    .module('app')
    .controller('ScholarshipTransferController', ScholarshipTransferController);
})(angular);