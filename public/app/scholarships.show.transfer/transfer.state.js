+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Transfer Scholarship' };

    // 
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('scholarships');
      }
        
      $modal
        .open({
          size: 'lg',
          controller: 'ScholarshipTransferController as transferCtrl',
          templateUrl: '/app/scholarships.show.transfer/transfer.html'
        })
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }
    

    var state = {
      url: '/transfer',
      name: 'scholarships.show.transfer',
      data: data,
      onEnter: onEnter
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);