+(function(angular, _, undefined) {
  function ScholarshipsShowCtrl($scope, $http, Scholarship, flash, $window, $state, $modal, origin_scholar) {
    var vm = this;
      vm.award_input = {};
  
    vm.origin_scholar = origin_scholar;
    vm.edit_mode = 0;
    vm.toggle = toggle;
    vm.update = update;
    vm.add_award_toggle = 0;
    vm.cancelAddAward = cancelAddAward;
    vm.startAddAward = startAddAward;
    vm.addAward = addAward;
    vm.addAwardLoading = false;
    vm.removeAward = removeAward;
    vm.removeRelative = removeRelative;
    angular.extend(vm,{
      loading: false,
      generateLoading: false,
      $generateSuccess: $generateSuccess,
      $generateError: $generateError,
      awards: [],
      list_awards: [],
      form: { data: Scholarship.instance },
      $success: $success,
      $successSetStatus: $successSetStatus,
      $error: $error,
      $transfer: $transfer
    });

    (function init() {
      $http.get('/api/v1/awards')
       .then(function(response) {
          vm.list_awards = response.data.data;
          console.log(vm.list_awards);
         });
      $http
        .get('/api/v1/scholarships/' + vm.form.data.id + '/awards')
        .then(function(res) { vm.awards = res.data.data; });


    })();

    function $success() {
      vm.loading = false;
      flash.success = 'Scholarship has been successfully updated. Page is now refreshing..';
      toggle();
    }

     function $successSetStatus() {
      vm.loading = false;
      flash.success = 'Scholarship has been successfully updated. Page is now refreshing..';
      $state.go($state.current.name, { id: vm.form.data.id }, { reload: true });
    }

    function $error(response) {
      vm.loading = false;
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();

      flash.error = 'An error has occured. Please try refreshing the page';
    }

    function $generateSuccess(response) {
      vm.generateLoading = false;
      flash.success = 'Allowance has been generated. Page will be refreshed in a sec..';
      $state.go($state.current.name, { id: vm.form.data.id }, { reload: true });
    }

    function $generateError(response) {
      vm.generateLoading = false;
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();

      flash.error = 'An error has occured while generating the allowance. Please try refreshing the page..';
    }

    function $transfer() {
      $modal.open({
        templateUrl: 'app/scholarships.show/transfer/transfer.html',
        controller: 'TransferCtrl as transferCtrl',
        size: 'sm',
        resolve: {
          formResolve: function() {
            return vm.form.data;
          }
        }
      })
    }


    function toggle() {
      vm.edit_mode = !vm.edit_mode;
    }

    function update(formData) {
      console.log(formData);
      $http.put('/api/v1/scholarships/' + formData.id, formData)
        .then($success)
        .catch($error);
    }

    function removeRelative(relative_id, relation)
    {
       if ( !confirm('Are you sure to remove the relationship?') ) {
          return;
        }

      $http.delete('/api/v1/residents/' + vm.form.data.resident.id + '/relatives/' + relative_id)
           .then(function(res){
              console.log('removed');
              refreshRelative(relation);
           });


    }

    function refreshRelative(relation)
    {
      $http.delete('/api/v1/scholarships/' +  vm.form.data.id + '/refresh-relation/' + relation)
           .then(function(res){
              if(relation == 'father'){
                $http.delete()
                vm.form.data.father_name = null;
                vm.form.data.father_id = null;
              }
              if(relation == 'mother'){
                 vm.form.data.mother_name = null;
                 vm.form.data.mother_id = null;
              }
           });
    }

    function startAddAward() {
      vm.add_award_toggle = true;
      vm.award_input = {};
    }

    function cancelAddAward() {
      if ( !confirm('Are you sure to cancel adding an award? You will lose all fields.') ) {
        return;
      }

      vm.add_award_toggle = false;
    }

    function addAward($formRequest) {
      vm.addAwardLoading = true;

      $http.post('/api/v1/scholarships/' + vm.form.data.id + '/awards', $formRequest)
        .then(function(response) {
          flash.success = response.message;
          vm.awards.push(response.data);
          vm.add_award_toggle = false;
          vm.award_input = {};
        })
        .catch(function(res) {
          flash.to('validation').error = _.chain(res.data.errors)
            .values()
            .flatten()
            .value();
        })
        .finally(function() {
          vm.addAwardLoading = false;
        });
    }

    function removeAward(index) {
      var award = vm.awards[index];
      award._isDeleting = true;

      return $http.delete('api/v1/scholarships/' +   vm.form.data.id  + '/awards/'  + award.id + '/remove')
        .then(function() {
          vm.awards.splice(index, 1);
          flash.success = 'Successfully removed award';
        })
        .catch(function() {
          award._isDeleting = false;
          flash.error = 'An error occured while removing the award. Please try again!';
        });
    }
  }

  angular
    .module('app')
    .controller('ScholarshipsShowCtrl', ScholarshipsShowCtrl);
})(angular, _);