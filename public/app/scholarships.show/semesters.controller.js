+(function(angular, undefined) {

  function SemestersCtrl($scope, $controller, $state, UtilService, $stateParams, Scholarship, Semester, SEMESTER_COLUMNS, Restangular) {
    // Inherit BaseIndexCtrl's prototype,
    var controller = $controller('BaseIndexCtrl', {
        $scope: $scope,
        $state: $state,
        UtilService: UtilService
    });

    angular.extend(this, controller);

    var vm = this;

    vm.origin_scholar = true;

    vm.initialize = function(origin_scholar){
      vm.origin_scholar = origin_scholar;
    }

    // Initialize vm data
    vm.show = show;
    vm.collection = Semester.instance;
    vm.columns = SEMESTER_COLUMNS;

    // Manually bind the "this"
    // from the base controller
    vm.callServer = angular.bind(vm, vm.callServer);
    vm.applyPageData = angular.bind(vm, vm.applyPageData);
    vm.catchPageDataError = angular.bind(vm, vm.catchPageDataError);
    vm.getPageData = getPageData;

    // getPageData();

    function show(id) {
      console.log(id);
      if(vm.origin_scholar == false){
        $state.go('semesters-list.show.semester-edit', { sem_id: id });
      }
      else{
        $state.go('scholarships.show.semester-edit', { sem_id: id });
        
      }
    }

    /**
     * Fetches data and replaces the current
     * @param  string query
     * @return void
     */
    function getPageData(query) {
        vm.isLoading = true;

      return Restangular
        .one('scholarships', Scholarship.instance.id)
        .getList('semesters', angular.extend({}, { limit: 9999 }, query))
        .then(function(response) {
          vm.collection = response.data;
          Semester.instance = response.data;
        })
        .finally(function() { vm.isLoading = false });
    }
  }

  angular
    .module('app')
    .controller('SemestersCtrl', SemestersCtrl);
})(angular);
