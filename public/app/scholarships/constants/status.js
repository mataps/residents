+(function(angular, undefined) {
  var stages = [
    {
      label: 'Waiting',
      value: 'waiting'
    },
    {
      label: 'For Verification',
      value: 'for_verification'
    },
    {
      label: 'Active',
      value: 'active'
    },
    {
      label: 'Closed',
      value: 'closed'
    },
    {
      label: 'Denied',
      value: 'denied'
    },
    {
      label: 'Stop',
      value: 'stop'
    },
    {
      label: 'Denied',
      value: 'denied'
    },
    {
      label: 'Graduated',
      value: 'graduated'
    },
  ];

  angular
    .module('app')
    .constant('SCHOLARSHIP_STATUSES', stages);
})(angular);