+(function(angular, undefined) {
  var SUBJECTS_DEFAULT = [
    { name: '', grade: 1, unit: 1 }
  ];

  angular
    .module('app')
    .constant('SUBJECTS_DEFAULT', SUBJECTS_DEFAULT);
})(angular);