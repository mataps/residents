+(function(angular, undefined) {
  var terms = [1, 2];

  angular
    .module('app')
    .constant('SCHOLARSHIP_TERMS', terms);
})(angular);