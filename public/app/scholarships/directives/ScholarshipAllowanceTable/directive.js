+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcScholarshipAllowanceTable', directive);

  function directive() {
    return {
      scope:  {
        semesterID: '@semesterId',
        // dummy value used for $watch (flag
        // in which a request will be made when
        // the value changes).
        dummy: '='
      },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    /**
     *
     */
    function templateFn(element, attrs) {
      return [
        '<div class="box" ng-if="!data.length">',
          '<div class="body" style="text-align: center;">',
            '<h4> <small> No allowance has been claimed yet. </small> </h4>',
          '</div>',
        '</div>',

        '<table ng-if="!!data.length" class="table">',
          '<thead>',
            '<tr>',
              '<th> Amount </th>',
              '<th> Claimed </th>',
              '<th></th>',
            '</tr>',
          '</thead>',

          '<tbody>',
            '<tr ng-repeat="allowance in data">',
              '<td>',
                '<span ng-if="!!allowance.release.fixed_amount"><i class="ion-checkmark"></i><strong>PHP {{:: allowance.amount }}</strong>(Fixed)</span>',
                '<span ng-if="!allowance.release.fixed_amount" > <i class="ion-checkmark"></i><strong>PHP{{:: allowance.amount }}</strong>(Policy-based)</span>',
              '</td>',
              '<td>',
                '<span >',
                  'Claimed on {{:: allowance.created_at | timestamp | date: \'MMMM d, yyyy\' }}',
                '</span>',
              '</td>',
              '<td>',
                '<span>',
                  '<button ng-click="$unclaim(allowance.id)">Unclaim</button>',
                '</span>',
              '</td>',
            '</tr>',
          '</tbody>',
        '</table>'
      ].join('');
    }

    function controllerFn($scope, $http, Restangular, flash) {
      angular.extend($scope, {
        isLoading: false,
        data: [],
        $request: $request,
        $unclaim: $unclaim
      });

      $request();

      // Request the new data when the dummy
      // value changes. The dummy value changes
      // (expectedly) when an
      $scope.$watch(function() {
        return $scope.dummy
      }, function(changed, old) {
        if ( changed == old ) {
          return;
        }

        $request();
      });

      function $unclaim(id){

        console.log('clicked');

        return $http.delete('api/v1/allowances/' + id + '/remove')
                    .then(function(res){
                        flash.success = "Successfully removed.";
                    });

      }

      function $request() {
        $scope.isLoading  = true;
        var semID = $scope.semesterID;

        return Restangular
          .one('semesters', semID)
          .one('allowances')
          .getList()
          .then(function(response) {
            $scope.data = response.data;
          })
          .finally(function() {
            $scope.isLoading = false;
          })
      }
    }
  }
})(angular);
