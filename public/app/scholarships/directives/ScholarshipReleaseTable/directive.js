+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcScholarshipReleaseTable', directive);

  function directive() {
    return {
      scope:  {
        semesterID: '@semesterId',
        // dummy value used for $watch (flag
        // in which a request will be made when
        // the value changes).
        dummy: '='
      },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    /**
     *
     */
    function templateFn(element, attrs) {
      return [
        '<div class="box" ng-if="!data.length">',
          '<div class="body" style="text-align: center;">',
            '<h4> <small> No releases for this semester are available. </small> </h4>',
          '</div>',
        '</div>',

        '<table ng-if="!!data.length" class="table">',
          '<thead>',
            '<tr>',
              '<th> Amount </th>',
              '<th> Claim </th>',
            '</tr>',
          '</thead>',

          '<tbody>',
            '<tr ng-repeat="release in data">',
              '<td>',
                '<span> PHP {{:: release.amount }} </span>',
              '</td>',
              '<td>',
                '<span ng-if="!release.expiring_at">',
                  '<button type="button" ',
                    '',
                    'ng-click="$generate(release.id, $index)">',
                    'Claim Allowance',
                  '</button>',
                '</span>',
                '<span class="label label-danger" ng-if="!!release.expiring_at">',
                  'Expired on {{:: release.expiring_at | timestamp | date: \'MMMM d, yyyy\' }}',
                '</span>',
              '</td>',
            '</tr>',
          '</tbody>',
        '</table>'
      ].join('');
    }

    function controllerFn($scope, Restangular, flash) {
      angular.extend($scope, {
        isLoading: false,
        data: [],
        $generate: $generate,
        $request: $request
      });

      $request();
      // Request the new data when the dummy
      // value changes. The dummy value changes
      // (expectedly) when an
      $scope.$watch(function() {
        return $scope.dummy;
      }, function(changed, old) {
        if ( changed == old ) {
          return;
        }

        $request();
      });

      function $generate(id, index) {
        $scope.isLoading = true;
        var data = $scope.data;
        var relID = data.id;
        var semID = $scope.semesterID;

        return Restangular
          .one('semesters', semID)
          .one('releases')
          .post(id)
          .then(function() {
            flash.success = 'Allowance has been successfully generated and claimed!';
            // $scope.data[index].allowances.push({ created_at: new Date });
            $scope.dummy++;
          })
          .catch(function(response) {
            flash.to('validation').error = _.chain(response.data.data)
              .values()
              .flatten()
              .value();
          })
          .finally(function() { $scope.isLoading = false });
      }

      function $request() {
        $scope.isLoading  = true;
        var semID = $scope.semesterID;

        return Restangular
          .one('semesters', semID)
          .one('releases')
          .getList()
          .then(function(response) {
            console.log(response);
            $scope.data = response.data.filter(function(release) {
              return !release.allowances.length
            });
          })
          .finally(function() {
            $scope.isLoading = false;
          })
      }
    }
  }
})(angular);
