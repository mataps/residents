+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcScholarshipStatus', directive);

  function directive() {
    return {
      scope: {
        status: '=gcScholarshipStatus'
      },

      restrict: 'EA',
      template: templateFn
    };

    function templateFn() {
      return [
        '<span ng-switch="status">',
          '<span ng-switch-when="waiting" class="label label-default">Waiting</span>',
          '<span ng-switch-when="for_verification" class="label label-primary">For Verification</span>',
          '<span ng-switch-when="active" class="label label-success">Active</span>',
          '<span ng-switch-when="denied" class="label" style="color: #333; background: #fff">Denied</span>',
          '<span ng-switch-when="closed" class="label" style="color: white; background: #333;">Closed</span>',
          '<span ng-switch-when="inactive" class="label" style="color: white; background: #333;">Inactive</span>',
          '<span ng-switch-when="stop" class="label" style="background: #f00">Stop</span>',
          '<span ng-switch-when="graduated" class="label" style="background: #E7E426; color: #333;">Graduated</span>',
        '</span>'
      ].join('');
    }
  }
})(angular);
