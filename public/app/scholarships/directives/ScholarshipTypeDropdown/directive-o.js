+(function(angular, undefined) {
  "use strict";
  angular
    .module('app')
    .directive('gcScholarshipTypeDropdownYolo', directive);

  function directive() {
    return {
      scope: {
        "model": "=ngModel"
      },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    /**
     * Template
     */
    function templateFn() {
      return [
        '<select class="form-control" ng-model="model">',
          '<option value=""> {{ isLoading ? "Loading.." : (isSuccessful ? "Select a type" : "An error occured..") }} </option>',
          '<option ng-repeat="type in types" value="{{ type.id }} " ng-selected="type.id == model">',
            '{{ type.name }}',
          '</option>',
        '</select>'
      ].join('');
    }

    /**
     * Controller
     */
    function controllerFn($scope, $http, $timeout) {
      $scope.types = [];
      $scope.isSuccessful = true;
      $scope.isLoading = true;

      (function() {
        $http.get('/api/v1/scholarshiptypes')
          .then(function(response) {
            $timeout(function() {
              $scope.types = response.data.data;
              $scope.model = angular.isNumber($scope.model)? $scope.model.toString() : $scope.model;
            }, 0);
          })
          .catch(function() { $scope.isSuccessful = false; })
          .finally(function() { $scope.isLoading = false; });
      })();
    }
  }
})(angular);
