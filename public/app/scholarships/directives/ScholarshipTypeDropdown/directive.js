+(function(angular, undefined) {
  "use strict";
  angular
    .module('app')
    .directive('gcScholarshipTypeDropdown', directive);

  function directive() {
    return {
      replace: true,
      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    /**
     * Template
     */
    function templateFn() {
      return [
        '<select class="form-control">',
          '<option value=""> {{ isLoading ? "Loading.." : (isSuccessful ? "Select a type" : "An error occured..") }} </option>',
          '<option ng-repeat="type in types" value="{{ type.id }}">',
            '{{ type.name }}',
          '</option>',
        '</select>'
      ].join('');
    }

    /**
     * Controller
     */
    function controllerFn($scope, $http) {
      $scope.types = [];
      $scope.isSuccessful = true;
      $scope.isLoading = true;

      (function() {
        $http.get('/api/v1/scholarshiptypes')
          .then(function(response) {
            $scope.types = response.data.data;
          })
          .catch(function() { $scope.isSuccessful = false; })
          .finally(function() { $scope.isLoading = false; });
      })();
    }
  }
})(angular);