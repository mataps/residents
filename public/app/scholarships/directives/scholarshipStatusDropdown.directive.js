+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .directive('gcScholarshipStatusDropdown', scholarshipStatusDropdown);

  function scholarshipStatusDropdown() {
    var template = [
      '<select class="form-control" ng-model="statusCtrl.statusModel" ',
      'ng-init="statusCtrl.init()" name="status">',
        '<option ng-repeat="status in statusCtrl.statuses" value="{{ status.value }}">',
          '{{ status.label }}',
        '</option>',
      '</select>'
    ].join('');

    return {
      restrict: 'EA',
      template: template,
      scope: { statusModel: '=' },
      bindToController: true,
      controllerAs: 'statusCtrl',
      controller: controller
    };

    function controller($scope, SCHOLARSHIP_STATUSES) {
      var vm = this;

      vm.statuses = SCHOLARSHIP_STATUSES;
      vm.init = init;

      function init() {
        vm.statusModel = vm.statuses[0].value;
      }
    }
  }
})(angular);