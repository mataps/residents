+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .directive('gcScholarshipTermDropdown', scholarshipTermDropdown);

  function scholarshipTermDropdown() {
    var template = [
      '<select class="form-control" ng-model="termCtrl.termModel" ',
      'ng-init="termCtrl.init()" name="term">',
        '<option ng-repeat="term in termCtrl.terms" value="{{ term }}" ng-selected="term == termCtrl.termModel">',
          '{{ term }}',
        '</option>',
      '</select>'
    ].join('');

    return {
      restrict: 'EA',
      template: template,
      scope: { termModel: '=' },
      bindToController: true,
      controllerAs: 'termCtrl',
      controller: controller
    };

    function controller($scope) {
      var vm = this;

      vm.terms = [1, 2];
      vm.init = init;

      function init() {
        if ( angular.isUndefined(vm.termModel) ) {
          vm.termModel = vm.terms[0];
        }
      }
    }
  }
})(angular);