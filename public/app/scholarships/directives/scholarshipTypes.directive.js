+(function(angular, _, undefined) {
  angular
    .module('app')
    .directive('gcScholarshipTypes', scholarshipTypes);

  function scholarshipTypes() {
    var template = [
      '<wave-spinner ng-show="typeCtrl.isLoading"></wave-spinner>',
      '<select ng-model="typeCtrl.model" class="form-control" ng-if="!typeCtrl.isLoading">',
        '<option ng-repeat="type in typeCtrl.types" ng-selected = "typeCtrl.model == type.id" value="{{ type.id }}">{{ type.name }}</option>',
      '</select>',
    ].join('');

    return {
      restrict: 'EA',
      scope: { model: '=ngModel' },
      template: template,
      bindToController: true,
      controllerAs: 'typeCtrl',
      controller: function($scope, $http) {
        var vm = this;

        angular.extend(vm, {
          type: [],
          isLoading: true
        });

        // console.log(vm.model);

        requestTypes();

        //
        function requestTypes() {
          return $http
            .get('api/v1/scholarshiptypes')
            .then(function(response) {
              vm.types = response.data.data;
              vm.isLoading = false;

              return response;
            })
            .catch(function(err) {
              console.error(err);
            })
        }
      }
    };
  }
})(angular, _);