+(function(angular, _, undefined) {
  function SubjectFormController($scope, $http, SubjectFormService, Subject, $state, flash) {
    // public methods
    $scope.grades = [];
    $scope.addSubject = addSubject;
    $scope.removeSubject = removeSubject;
    $scope.subjectTypeahead = subjectTypeahead;
    $scope.submit = submit;
    $scope.startEdit = startEdit;
    $scope.cancelEdit = cancelEdit;

    (function init() {
      return $http.get('api/v1/semesters/' + $scope.id + '/grades')
        .then(function(res) {
          $scope.grades = res.data.data;
        });
    })();

    /**
     * Pushes a new subject to the "subject array"
     * @see  form.data.subjects
     */
    function addSubject() {
      $scope.grades.splice(0, 0, {
        subject: {
          id: '',
          name: ''
        },
        grade: 0,
        units: 0,
        _isEditing: true
      });
    }

    /**
     * @param  {int} index Position of the subject
     * @see  form.data.subjects
     */
    function removeSubject(index) {
      if ( !confirm('Are you sure to remove this grade?') ) {
        return;
      }

      var grade = $scope.grades[index];

      if ( grade.id == undefined ) {
        return $scope.grades.splice(index, 1);
      }

      grade._isLoading = true;
      return $http.delete('api/v1/semesters/' + $scope.id + '/grades/' + grade.id)
        .then(function(res) {
          flash.success = 'Successfully removed the grade.';
          $scope.grades.splice(index, 1);
          computeGWA();
        })
        .catch(function() {
          flash.error = 'An error occured while removing the grade.';
          grade._isLoading = false;
        });
    }

    function submit(index) {
      var grade = $scope.grades[index];
      var type = grade.id == undefined ? 'post' : 'put'
      var url = ('api/v1/semesters/' + $scope.id + '/grades') + (grade.id == undefined ? '' : '/' + grade.id);
      var data = {
        id: grade.id,
        subject_id: grade.subject.id,
        grade: grade.grade,
        units: grade.units
      };

      // YOLO Validation
      // if ( data.subject_id == undefined || data.subject_id == null ) {
      //   return flash.error = 'The subject is required.';
      // } else if ( isNaN(data.grade) ) {
      //   return flash.error = 'The grade needs to be a number.';
      // } else if ( isNaN(data.units) ) {
      //   return flash.error = 'The units needs to be a number.';
      // }

      grade._isLoading = true;
      return $http[type](url, data)
        .then(function(res) {
          flash.success = 'Successfully stored the grade';
          $scope.grades[index] = res.data.data;
          computeGWA();
        })
        .catch(function(res) {
          grade._isLoading = false;
          flash.to('validation').error = _.chain(res.data.errors)
            .values()
            .flatten()
            .value();
        });
    }

    /**
     * @name computeGWA
     * @see GWAService.compute
     * @description Used to compute the current GWA
     */
    function computeGWA() {
      $scope.gwa = SubjectFormService.compute($scope.grades);
    }

    /** Sends a request to the server for filtered data */
    function subjectTypeahead(name) {
      return Subject
        .Query
        .typeahead(name)
        .then(function(response) {
          return response.data;
        });
    }

    function startEdit(index) {
      $scope.grades[index]._copy = angular.copy($scope.grades[index]);
      $scope.grades[index]._isEditing = true;
    }

    function cancelEdit(index) {
      if ( !confirm('Are you sure to cancel editing? You\'ll lose changes') ) {
        return;
      }

      if ( $scope.grades[index].id === undefined ) {
        return $scope.grades.splice(index, 1);
      }

      console.log($scope.grades[index]._copy);
      $scope.grades[index] = angular.copy($scope.grades[index]._copy);
      $scope.grades[index]._isEditing = false;
    }
  }

  angular
    .module('app')
    .controller('GCSubjectFormController', SubjectFormController);
})(angular, _);