+(function(angular, _, undefined) {
  function subjectForm() {
    var template = [
      '<div class="box">',
        '<header class="head clearfix">',
          '<h4 class="pull-left" style="margin: 5px; color: #000;">',
            '<button class="btn btn-primary btn-sm success" type="button" ng-click="addSubject()">',
              '<i class="ion-plus-round"></i>',
            '</button>',
          ' Subjects </h4>',
      '</header>',
      '<div class="body">',
        '<div class="form-group row">',
          '<div class="col-md-5">',
              '<label>Subject</label>',
          '</div>',
          '<div class="col-md-2">',
              '<label>Grade</label>',
          '</div>',
          '<div class="col-md-2">',
              '<label>Unit</label>',
          '</div>',
          '<div class="col-md-3">',
              '<label>Options</label>',
          '</div>',
        '</div>',
        '<div style="display: table; height: 150px; width: 100%" ng-if="!grades.length">',
          '<div style="display: table-cell; vertical-align: middle; text-align: center">',
            '<h4><small> No grades yet. </small></h4>',
          '</div>',
        '</div>',
        '<div class="form-group row" ng-repeat="grade in grades" ng-if="!!grades.length">',
          '<div class="col-md-5">',

            '<div ng-if="grade._isEditing">',
            '<gc-subject-typeahead id-variable="grade.subject.id" ng-model="grade.subject.name">',
            '</gc-subject-typeahead>',
            '</div>',
            '<p ng-if="!grade._isEditing">{{ grade.subject.name }}</p>',
          '</div>',

          '<div class="col-md-2">',

            '<input id="status" type="text" step="0.25" min="0.25" class="form-control" ng-model="grade.grade" ng-if="grade._isEditing">',
            '<p ng-if="!grade._isEditing">{{ grade.grade }}</p>',
          '</div>',

          '<div class="col-md-2">',

            '<input id="status" type="text" step="1" min="1" class="form-control" ng-model="grade.units" ng-if="grade._isEditing">',
            '<p ng-if="!grade._isEditing">{{ grade.units }}</p>',
          '</div>',

          '<div class="col-md-3">',
            '<div ng-if="grade._isEditing">',
              '<button class="btn btn-warning btn-xs" type="button" ng-click="cancelEdit($index)" type="button" style="margin-right: 3px;" tooltip="Cancel editing..">',
                '<i class="ion-close"></i>',
              '</button>',

              '<button class="btn btn-success btn-xs" type="button" ng-click="submit($index)" type="button" tooltip="Save changes for this grade" ng-disabled="grade._isLoading">',
                '<i class="ion-checkmark-circled"></i>',
              '</button>',
              '</div>',
              '<div ng-if="!grade._isEditing">',
                '<button class="btn btn-info btn-xs" type="button" ng-click="startEdit($index)" type="button" ng-disabled="grade._isLoading" style="margin-right: 10px;" tooltip="Update this grade">',
                  '<i class="ion-edit"></i>',
                '</button>',
                '<button class="btn btn-danger btn-xs" type="button" ng-click="removeSubject($index)" type="button" ng-disabled="grade._isLoading" tooltip="Remove the grade">',
                  '<i class="ion-trash-a"></i>',
                '</button>',
              '</div>',
            '</div>',
          '</div>',
        '</div>',
      '</div>'
    ].join('');

    return {
      scope: {
        id: '=',
        gwa: '='
      },

      restrict: 'EA',
      template: template,
      controller: 'GCSubjectFormController'
    };
  }

  angular
    .module('app')
    .directive('gcSubjectForm', subjectForm);
})(angular);