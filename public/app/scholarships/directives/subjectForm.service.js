+(function(angular, _, undefined) {
  // GWA deciaml format (5.000069 to 5.00)
  var FIXED_FORMAT = 2;

  /** Rounds GWA to.25 */
  function roundGWA(value) {
    return (
      Math.round(value * 4) / 4
    ).toFixed(2);
  }

  function SubjectFormService() {
    /**
     * @description
     * @param  array grades [description]
     * @return int
     */
    function compute(grades) {
      var total = 0;
      var units = 0;

      grades.forEach(function(grade, index) {
        units += parseFloat(grade.units);
        total += (parseFloat(grade.grade) * parseFloat(grade.units));
      });

      return (total / units).toFixed(2);
    }

    return {
      compute: compute
    };
  }

  angular
    .module('app')
    .factory('SubjectFormService', SubjectFormService);
})(angular, _);