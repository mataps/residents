+function(angular, undefined) {
  angular
    .module('app')
    .controller('GCSubjectFormAddController', SubjectFormAddController);

  function SubjectFormAddController($scope, SubjectFormService) {
    var vm = this;

    vm.addSubject = addSubject;
    
    /**
     * Pushes a new subject to the "subject array"
     * @see  form.data.subjects
     */
    function addSubject(name, grade, unit) {
      SubjectFormService.addSubject(vm.subjects);
    }
  }
}(angular);