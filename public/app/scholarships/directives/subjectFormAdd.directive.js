+function(angular, undefined) {
  angular
    .module('app')
    .directive('gcSubjectFormAdd', subjectFormAdd);

  function subjectFormAdd() {
    var template = [
      '<span ng-click="gcSFAddCtrl.addSubject()" ng-transclude></span>'
    ].join('');

    return {
      restrict: 'EA',
      replace: true,
      transclude: true,
      scope: { 'subjects': '=' },
      template: template,
      bindToController: true,
      controllerAs: 'gcSFAddCtrl',
      controller: 'GCSubjectFormAddController'
    };
  }
}(angular);