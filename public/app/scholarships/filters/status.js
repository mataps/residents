+(function(angular, _, undefined) {
  angular
    .module('app')
    .filter('gcScholarshipStatus', gcScholarshipStatus);

    function gcScholarshipStatus(SCHOLARSHIP_STATUSES) {
      return filter;

      function filter(input) {
        var status = SCHOLARSHIP_STATUSES,
          values,
          index;

        // Pluck all 'value' properties
        // of each object in the array
        values = _.chain(status)
          .pluck('value')
          .value();

        // Get the index of the status
        // to check existence
        index = values.indexOf(input);

        if ( index !== -1 ) {
          return status[index].label;
        }

        return '_NO_VALUE_';
      }
    }
})(angular, _);