+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('ScholarshipReleaseController', ScholarshipReleaseController);

  function ScholarshipReleaseController($scope, Restangular, flash, typesResolve) {
    angular.extend($scope, {
      isLoading: false,
      types: typesResolve.data.data,
      data: { scholarship_types: [] },
      $release: $release
    });

    function $release() {
      $scope.isLoading = true;

      var url = 'api/v1/releases';
      var data = angular.copy($scope.data);
      data.scholarship_types = data.scholarship_types.join(',');

      return Restangular
        .one('')
        .post('releases', data)
        .then(function(response) { flash.success = 'Release successfull' })
        .catch(function(response) { _error(response) })
        .finally(function(response) { $scope.isLoading = false; });

      function _error(response) {
        flash.to('validation').error = _.chain(response.data.errors)
          .values()
          .flatten()
          .value();
      }
    }
  }
})(angular);