+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('ScholarshipResetController', ScholarshipResetController);

  function ScholarshipResetController($scope, Restangular, flash, callbackResolve, $http) {
    angular.extend($scope, {
      isLoading: false,
      data: {},
      $reset: $reset,
    });

    function $reset() {
      $scope.isLoading = true;
      flash.info = "Fetching neccessary data.. This may take long... Please wait.";
      return $http.post('/api/v1/scholarships/inactive', $scope.data)
        .then(function(response) {
          flash.success = 'Scholarship inactivate successfull';
          callbackResolve();
          $scope.$close();
        })
        .catch(function(response) { _error(response) })
        .finally(function(response) { $scope.isLoading = false; });

      function _error(response) {
        flash.to('validation').error = _.chain(response.data.errors)
          .values()
          .flatten()
          .value();
      }
    }
  }
})(angular);
