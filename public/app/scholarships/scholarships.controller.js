+(function(angular, undefined) {
  function ScholarshipsCtrl($scope, $http, Restangular, $controller, $state, UtilService, Scholarship, SCHOLARSHIP_COLUMNS, flash, $modal) {
    // Inherit BaseIndexCtrl's prototype,
    var controller = $controller('BaseIndexCtrl', {
        $scope: $scope,
        $state: $state,
        UtilService: UtilService
    });

    angular.extend(this, controller);

    // ------------------------- //
    // ------------------------- //

    // viewmodel
    var vm = this;

    // Data collection (grabbed from the service)
    var collection = [];
    var filter = {};

    // Copy items from the service
    // to the local collection variable
    angular.extend(collection, Scholarship.collection);

    // ------------------------- //
    // ------------------------- //

    // Initialize vm data
    vm.columns = SCHOLARSHIP_COLUMNS;
    vm.collection = collection;

    // Manually bind the "this"
    // from the base controller
    vm.callServer = angular.bind(vm, vm.callServer);
    vm.applyPageData = angular.bind(vm, vm.applyPageData);
    vm.catchPageDataError = angular.bind(vm, vm.catchPageDataError);

    // Commands that contact the API / server
    vm.remove = remove;
    vm.getPageData = getPageData;

    vm.show = show;
    vm.$release = $release;
    vm.$bulkStatus = $bulkStatus;
    vm.$reset = $reset;
    vm.$report = $report;
    vm.onYearChange = vm.onYearChange;
    vm.onSemChange = onSemChange;
    vm._filter_ = filter;


    vm.loadAttribute = loadAttribute;
    Restangular.service('affiliations').getList().then(function(response){
      vm.affiliations = response.data;
    })


    function loadAttribute(query) {
      console.log(query);
      return $http.get('/api/v1/attributions/search/?filters=' + JSON.stringify({ name: query }))
          .then(function(response) {
          console.log(response);
          return response.data;
        });
        ;
    }
    /**
     * @description  Sends a DELETE to the server
     * @see  getPageData
     * @return void
     */
    function remove() {
      if(!confirm('Please confirm deleting the selected records.'))
        return;

      vm.load();
      var selected = vm.getSelected();

      return Scholarship
        .Command
        .delete(selected)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    }

    function show(id) {
      $state.go('scholarships.show', {id: id});
    }

    // Success XHR
    function successXHRCallback(response) {
      flash.success = 'Scholarship has been successfully archived!';
      $state.reload();
      vm.load(false);
    }

    // Error XHR
    function errorXHRCallback(response) {
      flash.error = 'An error has occured. Please try again...';
      vm.load(false);
    }

    /**
     * Fetches data and replaces the current
     * @param  string query
     * @return void
     */
    function getPageData(query) {
      if ( query.page ) {
        query.filters = angular.copy( filter.filters );
      }

      if ( ! query.filters ) {
        query.filters = angular.copy(query);
      }

      if ( query.filters && ! query.filters['year'] ) {
        query.filters['year'] = filter['year'];
      }

      if ( query.filters && ! query.filters['semester'] ) {
        query.filters['semester'] = filter['semester'];
      }

      filter.filters = angular.copy(angular.extend({}, query.filters));

      if ( query.filters.page ) {
        query.filters = _.omit(query.filters, 'page');
      }

      if ( query.filters.limit ) {
        query.filters = _.omit(query.filters, 'limit');
      }

      if ( query.filters.sort ) {
        query.filters = _.omit(query.filters, 'sort');
      }

      return Scholarship
        .Query
        .all(query)
        .then(vm.applyPageData)
        .catch(vm.catchPageDataError);
    }

    function $release() {
      $modal.open({
        controller: 'ScholarshipReleaseController',
        templateUrl: 'app/scholarships/release/release.html',
        resolve: {
          typesResolve: function($http) { return $http.get('api/v1/scholarshiptypes'); }
        }
      });
    }

    function $bulkStatus() {
      var selected = vm.getSelected();

      if ( !selected.length ) {
        flash.error = 'Please select a scholarship first to bulk set status.';
        return;
      }

      $modal.open({
        controller: 'ScholarshipBulkStatusController',
        templateUrl: 'app/scholarships/status/status.html',
        resolve: {
          selectedResolve: function() {
            return selected.join(',');
          },
          callbackResolve: function() {
            return function() {
              $state.reload();
            }
          }
        }
      });
    }

    /**
     * Gets year in sets (1991-1992)
     * @param  {[type]} range [description]
     * @return {[type]}       [description]
     */
    $scope._ysets = (function (range) {
      range = parseInt(range, 10);

      var current = new Date().getFullYear();
      var start   = current - range;
      var end     = current + range;

      return _.range(start, end);
    })(30);

     function onYearChange() {
      filter['year'] = vm.__year;

      if ( filter.filters ) {
        filter.filters['year'] = vm.__year;
      }

      if ( filter['year'] === 'undefined') {
        filter['year'] = undefined;

        if ( filter.filters ) {
          filter.filters['year'] = undefined;
        }
      }

      getPageData(filter);
    }

     function onSemChange() {
      filter['semester'] = vm.__sem;

      if ( filter.filters ) {
        filter.filters['semester'] = vm.__sem;
      }

      if ( filter['semester'] === undefined) {
        filter['semester'] = undefined;

        if ( filter.filters ) {
          filter.filters['semester'] = undefined;
        }
      }

      getPageData(filter);
    }

    function $reset() {
      $modal.open({
        controller: 'ScholarshipResetController',
        templateUrl: 'app/scholarships/reset/reset.html',
        resolve: {
          callbackResolve: function() {
            return function() {
              $state.reload();
            }
          }
        }
      });
    }

    function $report(type) {
      $modal.open({
        templateUrl: 'app/scholarships/reports/reports.html',
        controller: 'ScholarshipReportController',
        resolve: {
          typeResolve: function() {
            return type;
          },
          columnsResolve: function() {
            return vm.columns;
          },
          filterResolve: function() {
            return vm.__query.filters;
          }
        }
      });
    }

     vm.onQueryChange = onQueryChange;
    function onQueryChange() {
      // console.log(vm._selected_affiliation);
      // console.log(vm._selected_attributes);
      vm.QQ['attributions'] = vm._selected_attributes;
      vm.QQ['affiliations'] = vm._selected_affiliation;

      if ( vm.QQ.filters ) {
        vm.QQ.filters['affiliations'] = vm._selected_affiliation;
        vm.QQ.filters['attributions'] = vm._selected_attributes;
      }

      if ( vm.QQ['affiliations'] === 'ALL') {
        vm.QQ['affiliations'] = undefined;

        if ( vm.QQ.filters ) {
          vm.QQ.filters['affiliations'] = undefined;
        }
      }

      getPageData(vm.QQ);
    }
       vm.QQ = {};

        /**
     * Fetches data and replaces the current
     * @param  string query
     * @return void
     * &filters={"birthdate": { "from": 'asdasd': "to"}}
     */
    function getPageData(query) {
      // if ( query.page ) {
      //   // console.log(query);
      //   // console.log(vm.QQ);
      //   angular.extend(vm.QQ, query);
      //   console.log(vm.QQ);
      // } else {
      //   vm.QQ = query;
      //   console.log(vm.QQ);
      // }

      if ( !query.filters ) {
        query.filters = angular.copy(query);
      }

      if ( query.page ) {

      }

      // if ( !angular.isUndefined(query.filters) &&
      //   ( !angular.isUndefined(query.filters['birthdate.to']) || !angular.isUndefined(query.filters['birthdate.from']) )
      // ) {
      //   if ( !angular.isUndefined(vm.QQ.filters.birthdate) ) {
      //     angular.extend(vm.QQ.filters, query.filters);
      //     vm.QQ.filters = _.omit(vm.QQ.filters, 'birthdate.to');
      //     vm.QQ.filters = _.omit(vm.QQ.filters, 'birthdate.from');
      //   }
      // }

      if ( query.page ) {
        query.filters = angular.copy(vm.QQ.filters);
      }

      if ( query.filters && ! query.filters['affiliations'] ) {
        query.filters['affiliations'] = vm.QQ['affiliations'];
      }

      if ( query.filters && ! query.filters['attributions'] ) {
        query.filters['attributions'] = vm.QQ['attributions'];
      }

      vm.QQ.filters = angular.copy(query.filters);

      if ( query.filters.page ) {
        query.filters = _.omit(query.filters, 'page');
      }

      if ( query.filters.limit ) {
        query.filters = _.omit(query.filters, 'limit');
      }

      if ( query.filters.sort ) {
        query.filters = _.omit(query.filters, 'sort');
      }

      return Scholarship
        .Query
        .all(query )
        .then(vm.applyPageData)
        .catch(vm.catchPageDataError);
    }

  }

  angular
    .module('app')
    .controller('ScholarshipsCtrl', ScholarshipsCtrl);
})(angular);
