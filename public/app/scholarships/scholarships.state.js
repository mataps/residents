+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * @description Fetches all scholarship
     * @param  Restangular
     * @return Restangular
     */
    function getScholarshipCollectionResolve (Scholarship) {
      return Scholarship
        .Repository
        .all();
    }

    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Manage scholarships' };

    /**
     * Resolves
     * @type {Object}
     */
    var resolves = { getScholarshipCollectionResolve: getScholarshipCollectionResolve };

    /**
     * State Config
     * @type {Object}
     */
    var state = {
      parent: 'main',
      url: '/scholarships',
      name: 'scholarships',
      controllerAs: 'scholarships',
      controller: 'ScholarshipsCtrl',
      data: data,
      resolve: resolves,
      templateUrl: '/app/scholarships/scholarships.html'
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);