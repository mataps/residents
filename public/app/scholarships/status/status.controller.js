+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('ScholarshipBulkStatusController', ScholarshipBulkStatusController);

  function ScholarshipBulkStatusController($scope, Restangular, flash, selectedResolve, callbackResolve, $http) {
    angular.extend($scope, {
      isLoading: false,
      data: { scholarship_types: [] },
      $bulkSetStatus: $bulkSetStatus,
    });

    function $bulkSetStatus() {
      $scope.isLoading = true;

      var url = '/api/v1/scholarships/' + selectedResolve + '/bulk_status';
      var data = angular.copy($scope.data);

      return $http.put(url, data)
        .then(function(response) {
          flash.success = 'Bulk set status successfull';
          callbackResolve();
          $scope.$close();
        })
        .catch(function(response) { _error(response) })
        .finally(function(response) { $scope.isLoading = false; });

      function _error(response) {
        flash.to('validation').error = _.chain(response.data.errors)
          .values()
          .flatten()
          .value();
      }
    }
  }
})(angular);
