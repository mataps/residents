+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('TransactionCategoriesCreateController', TransactionCategoriesCreateController);

  function TransactionCategoriesCreateController($scope, flash, $state, Restangular, $http) {
    var vm = this;

    angular.extend(vm, {
      isLoading: false,
      custom: {},
      data: {},
      $create: $create,
      $errorHandler: $errorHandler,
      $successHandler: $successHandler
    });

    function $create(data) {
      vm.isLoading = true;
      
      return $http.post('api/v1/transactions/categories', data)
        .then($successHandler)
        .catch($errorHandler)
        .finally(function() { vm.isLoading = false; });
    }

    /**
     * Success response
     */
    function $successHandler(response) {
      flash.success = 'Transaction Category has been successfully created!';

      $state.go('transactions-categories', null, { reload: true });
    }

    /**
     * Error response
     */
    function $errorHandler(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }
  }
})(angular);