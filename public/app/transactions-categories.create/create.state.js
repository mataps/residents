+(function(angular) {
  'use strict';

  angular
    .module('app')
    .config(config);

  function config($stateProvider) {

    var modal;

    /** Trigger modal */
    function onEnter($modal, $state, flash) {
      function transitionToOverlay() {
        return $state.go('transactions-categories');
      }

      flash.info = 'The category creation window is now opening.. please wait.';

      modal = $modal
        .open({
          size: 'sm',
          controller: 'TransactionCategoriesCreateController as createCtrl',
          templateUrl: '/app/transactions-categories.create/create.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }


    var state = {
      url: '/create',
      name: 'transactions-categories.create',
      data: { pageTitle: 'New transaction category' },
      onEnter: onEnter,
      onExit: onExit,
      resolve: {
        loadingNotificationResolve: function(flash) {
          flash.info = 'The category creation window is now opening.. please wait.';
        }
      }
    };

    $stateProvider.state(state);
  }
})(angular);
