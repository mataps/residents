+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('TransactionCategoriesEditController', TransactionCategoriesEditController);

  function TransactionCategoriesEditController($scope, Household, Restangular, flash, UtilService, $state, $http, $stateParams) {
    var vm = this;

    angular.extend(vm, {
      isLoading: true,
      old: {},
      data: {},
      $update: $update,
      $addItem: $addItem,
      $rmItem: $rmItem,
      $saveItem: $saveItem,
      $editItem: $editItem,
      $cancelItem: $cancelItem
    });

    init();

    /**
     *
     */
    function $update(data) {
      vm.isLoading = true;
      
      return $http
        .put('api/v1/transactions/categories/' + $stateParams.id, data)
        .then(function() {
          flash.success = 'Transaction category has been updated!';
          $state.go('transactions-categories', null, { reload: true });
        })
        .catch(function(r) {          
          flash.to('validation').error = _.chain(r.data.errors)
            .values()
            .flatten()
            .value();
        })
        .finally(function() { vm.isLoading = false; });
    }

    /**
     *
     */
    function $addItem() {
      var data = { editing: true, _saved: false };
      data.old = angular.copy(data);
      vm.data.sub_categories.data.splice(0, 0, data);
    }

    /**
     *
     */
    function $rmItem(index) {
      if ( !confirm('Are you sure to delete this sub category with the name [' + vm.data.sub_categories.data[index].name + ']?'))
        return;

      return Restangular.one('transactions')
        .one('sub_categories', vm.data.sub_categories.data[index].id)
        .remove()
        .then(function(r) {
          flash.success = 'Successfully deleted sub category with name [' + vm.data.sub_categories.data[index].name + '].';
          vm.data.sub_categories.data.splice(index, 1);
        })
        .catch(function(r) {
          flash.to('validation').error = _.chain(r.data.errors)
            .values()
            .flatten()
            .value();
        })
    }

    /**
     *
     */
    function $editItem(index) {
      vm.data.sub_categories.data[index].old = angular.copy(vm.data.sub_categories.data[index]);
      vm.data.sub_categories.data[index].editing = true;
    }

    /**
     *
     */
    function $saveItem(index) {
      var data = vm.data.sub_categories.data[index];
      data.category_id = vm.data.id;
      var saved = data._saved || angular.isUndefined(data._saved);
      var id = !saved ? '' : data.id;
      var action = !saved ? 'save' : 'update';

      if ( !confirm('Are you sure to ' + action + ' this sub category with the name [' + data.name + ']?'))
        return;

      return (
          !saved
          ? Restangular.one('transactions').one('sub_categories').post(id, data)
          : $http.put('api/v1/transactions/sub_categories/' + id, data)
        )
        .then(function(r) {
          var _data = r.data.data;
          vm.data.sub_categories.data[index] = _data;
        })
        .catch(function(r) {
          flash.to('validation').error = _.chain(r.data.errors)
            .values()
            .flatten()
            .value();
        })
        .finally(function() {
          vm.data.sub_categories.data[index].loading = false;
        });
    }

    /**
     *
     */
    function $cancelItem(index) {
      var saved = vm.data.sub_categories.data[index]._saved;
      if ( saved || saved == undefined ) {
        if ( !confirm('Are you sure to cancel update?'))
          return;

        vm.data.sub_categories.data[index] = vm.data.sub_categories.data[index].old;
        vm.data.sub_categories.data[index].editing = false;
      } else {
        if ( !confirm('Are you sure to cancel creating a new sub category?'))
          return;

        vm.data.sub_categories.data.splice(index, 1);
      }
    }

    /**
     *
     */
    function init() {
      return Restangular
        .one('transactions')
        .one('categories', $stateParams.id)
        .get()
        .then(function(response) {
          var _data = response.data;
          _data.sub_categories.data.sort(function(a, b) {
            return a.name > b.name;
          });

          vm.data = _data;
        })
        .catch(function(response) { vm.$close(); })
        .finally(function() { vm.isLoading = false; });
    }
  }
})(angular);