+(function(angular) {
  'use strict';

  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var modal;
    /** Trigger modal */
    function onEnter($modal, $state, flash) {
      function transitionToOverlay() {
        return $state.go('transactions-categories');
      }

      modal = $modal
        .open({
          size: 'sm',
          controller: 'TransactionCategoriesEditController as editCtrl',
          templateUrl: '/app/transactions-categories.edit/edit.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }

    var state = {
      url: '/{id}/edit',
      name: 'transactions-categories.edit',
      data: { pageTitle: 'Edit transaction category' },
      onEnter: onEnter,
      onExit: onExit,
      resolve: {
        loadingNotificationResolve: function(flash) {
          flash.info = 'The category update window is now opening.. please wait.';
        }
      }
    };

    $stateProvider.state(state);
  }
})(angular);
