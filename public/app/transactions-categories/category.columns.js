+(function(angular, undefined) { 
  var columns = [
    {
      key: 'name',
      label: 'Name',
      show: true,
      cellTemplate: '{{:: category.name }}',
      onClick: 'categories.show(category.id)'
    }
  ];

  angular
    .module('app')
    .constant('TRANSACTION_CATEGORIES_COLUMNS', columns);
})(angular);