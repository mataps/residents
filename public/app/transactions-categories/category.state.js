+(function(angular, undefined) {
  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var data = { pageTitle: 'Manage Transactions Categories' };
  
    var state = {
      parent: 'main',
      name: 'transactions-categories',
      url: '/transactions/categories',
      data: data,
      resolve: { getCategoriesResolve: getCategoriesResolve },
      controllerAs: 'categories',
      controller: 'TransactionsCategoriesController',
      templateUrl: '/app/transactions-categories/category.html'
    };
    
    $stateProvider.state(state);

    function getCategoriesResolve(Restangular) {
      return Restangular
        .one('transactions')
        .one('categories')
        .getList();
    }
  }

 
})(angular);