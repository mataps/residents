+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('TransactionVendorCreateController', TransactionVendorCreateController);

  function TransactionVendorCreateController($scope, flash, $state, Restangular, $http) {
    var vm = this;

    angular.extend(vm, {
      isLoading: false,
      custom: {},
      data: {},
      $create: $create,
      $barangays: $barangays,
      $districts: $districts,
      $citiesMunicipalities: $citiesMunicipalities,
      $errorHandler: $errorHandler,
      $successHandler: $successHandler
    });

    function $create(data) {
      $scope.isLoading = true;
      // var data = $scope.data;

      return $http.post('/api/v1/vendors', data)
        .then(function(r) {
          flash.success = 'Successfully created vendor';
          // callback(r.data.data.name, r.data.data.id);
          $state.go('transactions-vendors', null, {reload:true});
        })
        .catch(function(r) {
          flash.to('validation').error = _.chain(r.data.errors)
            .values()
            .flatten()
            .value();
        })
        .finally(function() {
          $scope.isLoading = true;
        });
    }
    /**
     * Success response
     */
    function $successHandler(response) {
      flash.success = 'Transaction Vendor has been successfully created!';

      $state.go('transactions-vendors', null, { reload: true });
    }

    /**
     * Error response
     */
    function $errorHandler(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }

      /**
     * Fetch districts
     */
    function $districts($viewValue) {
      return Restangular.service('districts')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }
    
    /**
     * Fetch cities
     */
    function $citiesMunicipalities($viewValue) {
      return Restangular.service('citiesmunicipalities')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }

    /**
     * Fetch barangays
     */
    function $barangays($viewValue) {
      return Restangular.service('barangays')
        .getList({ limit: 6969696969999999, filters: JSON.stringify({name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }
  }
})(angular);