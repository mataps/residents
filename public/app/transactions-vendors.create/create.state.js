+(function(angular) {
  'use strict';

  angular
    .module('app')
    .config(config);

  function config($stateProvider) {

    var modal;

    /** Trigger modal */
    function onEnter($modal, $state, flash) {
      function transitionToOverlay() {
        return $state.go('transactions-Vendors');
      }

      flash.info = 'The vendor creation window is now opening.. please wait.';

      modal = $modal
        .open({
          size: 'sm',
          controller: 'TransactionVendorCreateController as createCtrl',
          templateUrl: '/app/transactions-vendors.create/create.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }


    var state = {
      url: '/create',
      name: 'transactions-vendors.create',
      data: { pageTitle: 'New Transaction Vendor' },
      onEnter: onEnter,
      onExit: onExit,
      resolve: {
        loadingNotificationResolve: function(flash) {
          flash.info = 'The vendor creation window is now opening.. please wait.';
        }
      }
    };

    $stateProvider.state(state);
  }
})(angular);
