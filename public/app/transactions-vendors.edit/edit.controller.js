+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('TransactionVendorEditController', TransactionVendorEditController);

  function TransactionVendorEditController($scope, Household, Restangular, flash, UtilService, $state, $http, $stateParams) {
    var vm = this;

    angular.extend(vm, {
      isLoading: true,
      old: {},
      data: {},
      $update: $update,
      $citiesMunicipalities: $citiesMunicipalities,
      $barangays: $barangays
    });


    init();

    /**
     *
     */
    function $update(data) {
      vm.isLoading = true;
      
      return $http
        .put('api/v1/vendors/' + $stateParams.id, data)
        .then(function() {
          flash.success = 'Transaction vendor has been updated!';
          $state.go('transactions-vendors', null, { reload: true });
        })
        .catch(function(r) {          
          flash.to('validation').error = _.chain(r.data.errors)
            .values()
            .flatten()
            .value();
        })
        .finally(function() { vm.isLoading = false; });
    }



 
    function $citiesMunicipalities($viewValue) {
      return Restangular.service('citiesmunicipalities')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }

    /**
     * Fetch barangays
     */
    function $barangays($viewValue) {
      return Restangular.service('barangays')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }

 
    function init() {
      return Restangular
        .one('vendors', $stateParams.id)
        .get()
        .then(function(response) {
          var _data = response.data;
     

          vm.data = _data;
        })
        .catch(function(response) { vm.$close(); })
        .finally(function() { vm.isLoading = false; });
    }
  }
})(angular);