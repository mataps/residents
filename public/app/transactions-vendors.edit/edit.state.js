+(function(angular) {
  'use strict';

  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var modal;
    /** Trigger modal */
    function onEnter($modal, $state, flash) {
      function transitionToOverlay() {
        return $state.go('transactions-vendors');
      }

      modal = $modal
        .open({
          size: 'sm',
          controller: 'TransactionVendorEditController as editCtrl',
          templateUrl: '/app/transactions-vendors.edit/edit.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }

    var state = {
      url: '/{id}/edit',
      name: 'transactions-vendors.edit',
      data: { pageTitle: 'Edit transaction vendor' },
      onEnter: onEnter,
      onExit: onExit,
      resolve: {
        loadingNotificationResolve: function(flash) {
          flash.info = 'The vendor update window is now opening.. please wait.';
        }
      }
    };

    $stateProvider.state(state);
  }
})(angular);
