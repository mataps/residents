+(function(angular, undefined) { 
  var columns = [
    {
      key: 'name',
      label: 'Name',
      show: true,
      cellTemplate: '{{:: vendor.name }}',
      onClick: 'vendors.show(vendor.id)'
    },
        {
      key: 'barangay',
      label: 'Barangay',
      show: true,
      cellTemplate: '{{:: vendor.barangay }}',
      onClick: 'vendors.show(vendor.id)'
    },
        {
      key: 'cityMunicipality',
      label: 'City/Municipality',
      show: true,
      cellTemplate: '{{:: vendor.cityMunicipality }}',
      onClick: 'vendors.show(vendor.id)'
    }
  ];


  angular
    .module('app')
    .constant('TRANSACTION_VENDOR_COLUMNS', columns);
})(angular);