+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('TransactionVendorController', TransactionVendorController);

  function TransactionVendorController($scope, $controller, UtilService, TRANSACTION_VENDOR_COLUMNS, getVendorResolve, Restangular, flash, $state) {
    // Inherit BaseIndexCtrl's prototype,
    var controller = $controller('BaseIndexCtrl', {
        $scope: $scope,
        $state: $state,
        UtilService: UtilService
    });

    angular.extend(this, controller);

    // ------------------------- //
    // ------------------------- //
    // 
    // viewmodel
    var vm = this,    
       // Data collection (grabbed from the service)
      collection = [];

    // Copy items from the service
    // to the local collection variable
    console.log(getVendorResolve.data);
    angular.extend(collection, getVendorResolve.data);

    vm.$state = $state;

    // Initialize vm data
    vm.columns = TRANSACTION_VENDOR_COLUMNS;
    vm.collection = collection;

    // Manually bind the "this"
    // from the base controller
    vm.callServer = angular.bind(vm, vm.callServer);
    vm.applyPageData = angular.bind(vm, vm.applyPageData);
    vm.catchPageDataError = angular.bind(vm, vm.catchPageDataError);

    // Commands that contact the API / server
    vm.getSelected = angular.bind(vm, vm.getSelected);
    vm.remove = remove;
    vm.getPageData = getPageData;

    /**
     * @description  Sends a DELETE to the server
     * @return Restangular
     */
    function remove() {
      if(!confirm('Please confirm deleting the selected records.'))
        return;

      vm.load();

      var selected = vm.getSelected().join(',');

      return Restangular
        .one('transactions')
        .one('categories', selected)
        .one('remove')
        .remove()
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    };

    function show(sid) {
      $state.go('transactions-vendors.edit', {id : sid});
    }

    /**
     * Fetches data and replaces the current
     * @param  string query
     * @return void
     */
    function getPageData(query) {
      return Restangular
        .one('vendors')
        .getList()
        .then(vm.applyPageData)
        .catch(vm.catchPageDataError);
    }

    // Success XHR
    function successXHRCallback(response) {
      var message = 'Account has been successfully archived!';
      flash.success = message;
      $state.reload();
      vm.load(false);
    }

    // Error XHR
    function errorXHRCallback(response) {
      var message = 'An error has occured. Please try again..';
      flash.error = message;
      vm.load(false);
    }
  }
})(angular);