+(function(angular, undefined) {
  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var data = { pageTitle: 'Manage Transactions Vendors' };
  
    var state = {
      parent: 'main',
      name: 'transactions-vendors',
      url: '/transactions/vendors',
      data: data,
      resolve: { getVendorResolve: getVendorResolve },
      controllerAs: 'vendors',
      controller: 'TransactionVendorController',
      templateUrl: '/app/transactions-vendors/vendor.html'
    };
    
    $stateProvider.state(state);

    function getVendorResolve(Restangular) {
      return Restangular
        .one('vendors')
        .getList();
    }
  }

 
})(angular);