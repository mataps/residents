+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('TransactionAccountCreateCtrl', TransactionAccountCreateCtrl);

  function TransactionAccountCreateCtrl($scope, flash, $state, Restangular, $http) {
    var vm = this;

    angular.extend(vm, {
      isLoading: false,
      custom: {},
      data: { affiliation_id: '' },
      $create: $create,
      $errorHandler: $errorHandler,
      $successHandler: $successHandler
    });

    function $create(data) {
      vm.isLoading = true;
      
      return $http.post('api/v1/transactions/accounts', data)
        .then($successHandler)
        .catch($errorHandler)
        .finally(function() { vm.isLoading = false; });
    }

    /**
     * Success response
     */
    function $successHandler(response) {
      flash.success = 'Transaction Account has been successfully created!';

      $state.go('transactions-accounts', null, { reload: true });
    }

    /**
     * Error response
     */
    function $errorHandler(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }
  }
})(angular);