+(function(angular) {
  'use strict';

  angular
    .module('app')
    .config(config);

  function config($stateProvider) {

    var modal;

    /** Trigger modal */
    function onEnter($modal, $state, flash) {
      function transitionToOverlay() {
        return $state.go('transactions-accounts');
      }

      modal = $modal
        .open({
          size: 'sm',
          controller: 'TransactionAccountCreateCtrl as createCtrl',
          templateUrl: '/app/transactions.accounts.create/create.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }


    var state = {
      url: '/create',
      name: 'transactions-accounts.create',
      data: { pageTitle: 'New transaction account' },
      onEnter: onEnter,
      onExit: onExit,
      resolve: {
        loadingNotificationResolve: function(flash) {
          flash.info = 'The account creation window is now opening.. please wait.';
        }
      }
    };

    $stateProvider.state(state);
  }
})(angular);
