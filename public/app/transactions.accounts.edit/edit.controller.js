+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('TransactionAccountEditCtrl', TransactionAccountEditCtrl);

  function TransactionAccountEditCtrl($scope, Household, Restangular, flash, UtilService, $state, $http, $stateParams) {
    var vm = this;

    angular.extend(vm, {
      isLoading: true,
      data: {},
      $update: $update,
      $errorHandler: $errorHandler,
      $successHandler: $successHandler
    });

    init();

    function $update(data) {
      vm.isLoading = true;
      
      return $http
        .put('api/v1/transactions/accounts/' + $stateParams.id, data)
        .then($successHandler)
        .catch($errorHandler)
        .finally(function() { vm.isLoading = false; });
    }

    function $successHandler(response) {
      vm.isLoading = true;

      flash.success = 'Transaction account has been updated!';

      $scope.$close();
    }

    function $errorHandler(response) {
      vm.isLoading = false;

      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }

    function init() {
      return Restangular
        .one('transactions')
        .one('accounts', $stateParams.id)
        .get()
        .then(function(response) {
          vm.data = response.data;
          vm.data.affiliation_id = vm.data.affiliation.id;
          vm.data.affiliation_name = vm.data.affiliation.name;
        })
        .catch(function(response) { $scope.$close(); })
        .finally(function() { vm.isLoading = false; });
    }
  }
})(angular);