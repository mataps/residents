+(function(angular) {
  'use strict';

  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var modal;
    /** Trigger modal */
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('transactions-accounts');
      }

      modal = $modal
        .open({
          size: 'sm',
          controller: 'TransactionAccountEditCtrl as editCtrl',
          templateUrl: '/app/transactions.accounts.edit/edit.html',
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }

    var state = {
      url: '/{id}/edit',
      name: 'transactions-accounts.edit',
      data: { pageTitle: 'Edit transaction account' },
      onEnter: onEnter,
      onExit: onExit,
      resolve: {
        loadingNotificationResolve: function(flash) {
          flash.info = 'The account edit window is now opening.. please wait.';
        }
      }
    };

    $stateProvider.state(state);
  }
})(angular);
