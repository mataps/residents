+(function(angular, undefined) {
 
  var columns = [
    {
      key: 'account.name',
      label: 'Account Name',
      show: true,
      cellTemplate: '{{:: account.name }}',
      onClick: 'accounts.show(account.id)'
    },
    {
      key: 'affiliation.name',
      label: 'Affiliation Name',
      show: true,
      cellTemplate: '{{:: account.affiliation.name }}',
      onClick: 'accounts.show(account.id)'
    },
    {
      key: 'signatory',
      label: 'Signatory',
      show: true,
      cellTemplate: '{{:: account.signatory }}',
      onClick: 'accounts.show(account.id)'
    },
    {
      key: 'signatory_2',
      label: 'Signatory 2',
      show: true,
      cellTemplate: '{{:: account.signatory }}',
      onClick: 'accounts.show(account.id)'
    },
  ];

  angular
    .module('app')
    .constant('TRANSACTION_ACCOUNTS_COLUMNS', columns);
})(angular);