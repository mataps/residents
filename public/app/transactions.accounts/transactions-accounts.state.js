+(function(angular, undefined) {
  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var data = { pageTitle: 'Manage Transactions Accounts' };
  
    var state = {
      parent: 'main',
      name: 'transactions-accounts',
      url: '/transactions/accounts',
      data: data,
      resolve: { getAccountsResolve: getAccountsResolve },
      controllerAs: 'accounts',
      controller: 'TransactionsAccountsCtrl',
      templateUrl: '/app/transactions.accounts/transactions-accounts.html'
    };
    
    $stateProvider.state(state);

    function getAccountsResolve(Restangular) {
      return Restangular
        .one('transactions')
        .one('accounts')
        .getList();
    }
  }

 
})(angular);