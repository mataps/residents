+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('_AddAffiliationCtrl', _AddAffiliationCtrl);

  function _AddAffiliationCtrl($scope, $state, flash, afResolve, Restangular, $http) {
    var vm = this;

    angular.extend(vm, {
      isLoading: false,
      custom: {
        datepickers: {},
        selected: {}
      },
      data: {},

      $create: $create,
      $errorHandler: $errorHandler,
      $successHandler: $successHandler,
      districts: districts,
      citiesMunicipalities: citiesMunicipalities,
      barangays: barangays
    });

    /**
     * Success response
     */
    function $successHandler(response) {
      vm.isLoading = false;
      afResolve(response.data.data);
      $scope.$close();
    }

    /**
     * Error response
     */
    function $errorHandler(response) {
      vm.isLoading = false;

      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }

    function districts($viewValue) {
      return Restangular.service('districts')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }

    function citiesMunicipalities($viewValue) {
      return Restangular.service('citiesmunicipalities')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }

    function barangays($viewValue) {
      return Restangular.service('barangays')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }

    function $create() {
      vm.isLoading = true;

      return $http.post('api/v1/affiliations', vm.data)
        .then($successHandler)
        .catch($errorHandler)
        .finally(function() { vm.isLoading = false });
    }
  }
})(angular);
