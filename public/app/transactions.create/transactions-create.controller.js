+(function(angular, _, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('TransactionsCreateCtrl', TransactionsCreateCtrl);

  function TransactionsCreateCtrl($scope, UtilService, Transaction, Restangular, flash, $state, $modal, $http, USER, localStorageService) {
    var vm = this;
    var _filter_ = localStorageService.get('TRANSACTION_ACCOUNT_FILTER');

    angular.extend(vm, {
      form: {
        data: {
          beneficiary_id: '',
          client_id: '',
          referrer_id: '',
          client_type: 'Resident',
          beneficiary_type : 'Resident'
        }
      },
      custom: {},
      isCategoryLoading: true
    });

    $scope.$watch(
      function() {
        return vm.form.data.category_id;
      },
      function(changed, old) {
        if ( changed == old ) return;

        // Map and find the category
        // of the given id
        var id = vm.categories
          .map(function(c) { return parseInt(c.id, 10); })
          .indexOf(parseInt(changed, 10));

        vm._selected = vm.categories[id];
      }
    );

    Restangular.one('transactions').one('categories?limit=1000&sort=name').get().then(function(response) {
      vm.categories = response.data.data;
      vm.isCategoryLoading = false;
    });

    Restangular.one('transactions').one('accounts?limit=1000').get().then(function(response) {
      vm.account_types = response.data.data;

      if (  _filter_ !== undefined && _filter_ !== null ) {
        vm.form.data.account_type_id = _filter_;
      }

      console.log(_filter_, vm.form.data.account_type_id);
    });

    Restangular.one('transactions').one('items').get().then(function(response) {
      vm.items = response.data.data;
    });

    /**
     * @description  Sends an XHR to the server with
     *               the form data for create a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    vm.$create = function(formData) {
       return Transaction
        .Command
        .create(formData)
        .then(function(response) {
          flash.success = 'Transaction has been successfully created!';
          $state.go('transactions.voucher-show', { voucher_id: response.data.data.voucher.id }, { reload: true });
     
          $scope.$close();
        })
        .catch(function(response) {
          flash.to('validation').error = _.chain(response.data.errors)
            .values()
            .flatten()
            .value();
        });
    }

    /**
     *
     */
    vm.$clientCreate = function() {
      $modal.open({
        templateUrl: '/app/_main/modals/residents/add-resident.html',
        controller: '_AddResidentCtrl as _modal_',
        size: 'lg',
        resolve: {
          datResolve: function() {
            return function(response) {
              vm.form.data.client_name = response.data.full_name;
              vm.form.data.client_id = response.data.id;
            }
          }
        }
      });
    }

    /**
     *
     */
    vm.$beneficiaryCreate = function() {
      if ( vm.form.data.beneficiary_type == 'Resident') {
        $modal.open({
          templateUrl: '/app/_main/modals/residents/add-resident.html',
          controller: '_AddResidentCtrl as _modal_',
          size: 'lg',
          resolve: {
            datResolve: function() {
              return function(response) {
                vm.form.data.beneficiary_name = response.full_name;
                vm.form.data.beneficiary_id = response.id;
              }
            }
          }
        });
      } else {
        $modal.open({
          templateUrl: '/app/transactions.create/_MODALS_/affiliation.html',
          controller: '_AddAffiliationCtrl as _modal_',
          size: 'lg',
          resolve: {
            afResolve: function() {
              return function(data) {
                vm.form.data.beneficiary_name = data.name;
                vm.form.data.beneficiary_id = data.id;
              }
            }
          }
        });
      }
    }

    /**
     *
     */
    vm.$referrerCreate = function() {
      $modal.open({
        templateUrl: '/app/_main/modals/residents/add-resident.html',
        controller: '_AddResidentCtrl as _modal_',
        size: 'lg',
        resolve: {
          datResolve: function() {
            return function(response) {
              vm.form.data.referrer_name = response.data.full_name;
              vm.form.data.referrer_id = response.data.id;
            }
          }
        }
      });
    }

    /**
     *
     */
    vm.$itemRequest = function($value) {
      return $http.get('/api/v1/transactions/items/?filters=' + JSON.stringify({ name: $value }))
        .then(function(response) {
          return response.data.data;
        });
    }
  }
})(angular, _);
