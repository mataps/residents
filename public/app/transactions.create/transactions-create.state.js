+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'New Transaction' };
    var modal;

    //
    function onEnter($modal, $state, flash) {
      function transitionToOverlay() {
        return $state.go('transactions');
      }

      modal = $modal
        .open({
          size: 'md',
          controllerAs: 'createCtrl',
          controller: 'TransactionsCreateCtrl',
          templateUrl: '/app/transactions.create/transactions-create.html',
          backdrop: false
        
        });

      modal
        .result
        .catch(transitionToOverlay);
    }

    var state = {
      url: '/create',
      name: 'transactions.create',
      data: data,
      onEnter: onEnter,
      resolve: {
        loadingNotificationResolve: function(flash) {
          flash.info = 'The transaction creation window is now opening.. please wait.';
        }
      }
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);
