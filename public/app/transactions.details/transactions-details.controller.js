+(function(angular, _, undefined) {
  "use strict";
  angular
    .module('app')
    .controller('TransactionsDetailsCtrl', TransactionsDetailsCtrl);

  function TransactionsDetailsCtrl($scope, $state, $window, Transaction, UtilService, Restangular, flash, $http, $modal) {
    var vm = this;
    vm.form = { data: { client_type: 'Resident' } };
    vm.isLoading = false;
    vm.isSettleLoading = false;

    // Watch
    $scope.$watch(
      function() {
        return vm.form.data.category_id;
      },
      function(changed, old) {
        if ( changed == old ) return;

        // Map and find the category
        // of the given id
        var id = vm.categories
          .map(function(c) { return parseInt(c.id, 10); })
          .indexOf(parseInt(changed, 10));

        vm._selected = vm.categories[id];
      }
    );

    // eeeee
    angular.extend(vm, {
      form: {
        data: (function() {
          var t = Transaction.instance.clone();
          t.beneficiary_id = t.beneficiary.id;
          t.beneficiary_name = t.beneficiary[t.beneficiary_type.toLowerCase() == 'resident' ? 'full_name' : 'name'];
          t.referrer_name = t.referrer.full_name;
          t.referrer_id = t.referrer.id;
          t.item_id = t.item.id;
          t.item_name = t.item.name;
          t.category_id = t.category.id;
          t.sub_category_id = t.sub_category.id;
          t.account_type_id = t.account.id;
          t.voucher_id = t.voucher.id;
          t.vendor_id = t.vendor.id;
          t.vendor_name = t.vendor.name;
          return t;
        })()
      }
    });

    Restangular.one('transactions').one('categories?limit=1000&sort=name').get().then(function(response) {
      vm.categories = response.data.data;

      var id = vm.categories
        .map(function(c) { return parseInt(c.id, 10); })
        .indexOf(parseInt(vm.form.data.category.id));

        vm._selected = vm.categories[id];
    });

    Restangular.one('transactions').one('accounts?limit=1000').get().then(function(response) {
      vm.account_types = response.data.data;
    });

    Restangular.one('transactions').one('items').get().then(function(response) {
      vm.items = response.data.data;
    });

    // ------------------------- //
    // ------------------------- //

    vm.$settle = function () {
      if ( !confirm('Are you sure to settle this transaction?') ) {
        return;
      }

      vm.isSettleLoading = true;

      return Restangular.one('transactions')
        .one('settle', vm.form.data.id)
        .post()
        .then(function(response) {
          flash.success = "Transaction has been settled";
          vm.form.data.settled_at = new Date();
          // $state.go('transactions', { id: vm.form.data.id }, { reload: true });
          $state.reload();
        })
        .finally(function() {
          vm.isSettleLoading = false;
        });
    }

    /**
     *
     */
    vm.$edit = function() {
      $modal.open({
        templateUrl: '/app/transactions.details/transactions-edit.html',
        controller: 'TransactionsDetailsCtrl',
        controllerAs: 'detailsCtrl'
      });
    }

    /**
     * @description  Sends an XHR to the server with
     *               the form data for edit a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    vm.$update = function(id, formData) {
      vm.isLoading = true;

      return $http.put('api/v1/transactions/' + id, formData)
        .then(function() {
          flash.success = 'Transaction has been successfully updated!';
          $state.reload();
          $copse.$close();
        })
      .catch(function(response) {
          flash.to('validation').error = _.chain(response.data.errors)
            .values()
            .flatten()
            .value();
      })
      .finally(function() {
        vm.isLoading = false;
      });
    }

    /**
     *
     */
    vm.$liquidate = function() {
      $modal.open({
        templateUrl: '/app/transactions.voucher-show/modals/liquidation.html',
        controller: 'TransactionLiquidationController',
        resolve: {
          liquidationsResolve: function() {
            return Restangular.one('transactions', vm.form.data.id)
              .getList('liquidations', JSON.stringify({ limit: 1000 }))
              .then(function(r) {
                return r.data;
              });
          },
          dataResolve: function() {
            return vm.form.data;
          },
          callbackResolve: function() {
            return function() {
              vm.form.data.liquidated_at = new Date().getTime().toString();
              console.log(vm.form.data);
            }
          }
        }
      });
    }

    /**
     *
     */
    vm.$refund = function() {
      $modal.open({
        templateUrl: '/app/transactions.voucher-show/modals/refund.html',
        controller: 'TransactionRefundController',
        resolve: {
          dataResolve: function() {
            return vm.form.data;
          },
          callbackResolve: function() {
            return function(data) {
              vm.form.data.refund = data || {};
            }
          }
        }
      });
    }

    /**
     *
     */
    vm.$beneficiaryCreate = function() {
      if ( vm.form.data.beneficiary_type == 'Resident') {
        $modal.open({
          templateUrl: '/app/_main/modals/residents/add-resident.html',
          controller: '_AddResidentCtrl as _modal_',
          size: 'lg',
          resolve: {
            datResolve: function() {
              return function(response) {
                vm.form.data.beneficiary_name = response.data.full_name;
                vm.form.data.beneficiary_id = response.data.id;
              }
            }
          }
        });
      } else {
        $modal.open({
          templateUrl: '/app/transactions.create/_MODALS_/affiliation.html',
          controller: '_AddAffiliationCtrl as _modal_',
          size: 'lg',
          resolve: {
            afResolve: function() {
              return function(data) {
                vm.form.data.beneficiary_name = data.name;
                vm.form.data.beneficiary_id = data.id;
              }
            }
          }
        });
      }
    }

    /**
     *
     */
    vm.$referrerCreate = function() {
      $modal.open({
        templateUrl: '/app/_main/modals/residents/add-resident.html',
        controller: '_AddResidentCtrl as _modal_',
        size: 'lg',
        resolve: {
          datResolve: function() {
            return function(response) {
              vm.form.data.referrer_name = response.data.full_name;
              vm.form.data.referrer_id = response.data.id;
            }
          }
        }
      });
    }

    /**
     *
     */
    vm.$itemRequest = function($value) {
      return $http.get('/api/v1/transactions/items/?filters=' + JSON.stringify({ name: $value }))
        .then(function(response) {
          return response.data.data;
        });
    }

    /**
     *
     */
    vm.$viewLiquidation = function() {
      $modal.open({
        controller: 'TransactionLiquidationItemsController',
        templateUrl: '/app/transactions.voucher-show/modals/item.html',
        resolve: {
          items: function() {
            return $http.get('/api/v1/transactions/' + vm.form.data.id + '/liquidations' + '?limit=1000')
              .then(function(r) {
                return r.data.data;
              });
          }
        }
      })
    }
  }
})(angular, _);
