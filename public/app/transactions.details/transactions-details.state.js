+(function(angular) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Transaction Details' };
    var modal;

    //
    function onEnter($modal, $state, Transaction, flash) {
      function transitionToOverlay() {
        return $state.go('transactions');
      }

      var transaction = Transaction.instance;

      modal = $modal
        .open({
          size: 'lg',
          controllerAs: 'detailsCtrl',
          controller: 'TransactionsDetailsCtrl',
          templateUrl: '/app/transactions.details/transactions-details.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }


    var state = {
      url: '/details/{transaction_id}',
      resolve : {
        getTransactionResolve : function (Transaction, $stateParams, flash) {
          flash.info = 'The transaction details window is now opening.. please wait.';

          return Transaction
            .Repository
            .get($stateParams.transaction_id);
        }
      },
      name: 'transactions.details',
      data: data,
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);
