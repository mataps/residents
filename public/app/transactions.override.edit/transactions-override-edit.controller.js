+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('OverrideTransactionController', controller);

  function controller($scope, flash, Restangular, $http) {
    $scope.form = {};

    $scope.voucher_number = "";

    Restangular.one('transactions').one('categories?limit=1000&sort=name').get().then(function(response) {    
      $scope.categories = response.data.data;

      var id = $scope.categories
        .map(function(c) { return parseInt(c.id, 10); })
        .indexOf(parseInt($scope.form.data.category.id));
        console.log($scope.categories[id]);
        $scope.selected_categ = $scope.categories[id];
    });
    
    Restangular.one('transactions').one('accounts?limit=1000').get().then(function(response) {
      $scope.account_types = response.data.data;
    });

    Restangular.one('transactions').one('items').get().then(function(response) {
      $scope.items = response.data.data;
    });

    $scope.success = function() {
      flash.success = 'Your password has been updated!';
      $scope.form = {};
    }

    $scope.searchVoucher = function () {
      var voucher_number = $scope.voucher_number;
      Restangular.service('transactions').getList({ filters: JSON.stringify({ "voucher_uuid": voucher_number }) })
                .then(function(res){
                  var trx_id = res.data[0].id;

               Restangular.service('transactions').one(trx_id).get().then(function(trx){
                var t = trx.data;
                  console.log(t);   

                t.beneficiary_id = t.beneficiary.id;
                t.beneficiary_name = t.beneficiary[t.beneficiary_type.toLowerCase() == 'resident' ? 'full_name' : 'name'];
                t.referrer_name = t.referrer.full_name;
                t.referrer_id = t.referrer.id;
                t.item_id = t.item.id;
                t.item_name = t.item.name;
                t.category_id = t.category.id;
             
                t.account_type_id = t.account.id;
                t.voucher_id = t.voucher.id;
                t.vendor_id = t.vendor.id;
                t.vendor_name = t.vendor.name;

                $scope.form.data = t;           
                var id = $scope.categories
                               .map(function(c) { return parseInt(c.id, 10); })
                               .indexOf(parseInt($scope.form.data.category.id));
                $scope.selected_categ = $scope.categories[id];


                  t.sub_category_id = t.sub_category.id;

               });
     

                });
    }

    $scope.error = function(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }

    $scope.update = function(formData) {   
      var id = formData.id;
      console.log(id);
      return $http.put('api/v1/transactions/' + id + '/override/', formData)
        .then(function() { flash.success = 'Transaction has been successfully updated!'; })
      .catch(function(response) { 
          flash.to('validation').error = _.chain(response.data.errors)
            .values()
            .flatten()
            .value();
      });
    }
  }
})(angular);