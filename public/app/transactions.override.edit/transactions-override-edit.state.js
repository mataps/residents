+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    $stateProvider.state({
      parent: 'main',
      name: 'transaction-override-edit',
      url: '/transaction-override-edit',
      data: { pageTitle: 'Transaction Override Edit' },
      controllerAs: 'overrideCtrl',
      controller: 'OverrideTransactionController',
      templateUrl: '/app/transactions.override.edit/transactions-override-edit.html'
    });
  }
})(angular);