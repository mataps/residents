+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('VoucherCreateController', VoucherCreateController);

  function VoucherCreateController($scope, $modal, $stateParams, Restangular, VOUCHER_TRANSACTIONS_COLUMNS) {
    var data = { transactions: [] };
    var columns = VOUCHER_TRANSACTIONS_COLUMNS;

    angular.extend($scope, {
      data: data,
      columns: columns,
      // isLoading: true,
      isLoading: false,

      openTransaction: openTransaction,
      addTransaction: addTransaction,

      getResidents: getResidents,
      onClientNameSelect: onClientNameSelect,
      clientCreate: clientCreate
    });

    /**
     * 
     */
    function openTransaction(index) {
      $modal.open({
        templateUrl: '/app/transactions.details/transactions-details.html',
        controller: 'TransactionsDetailsCtrl as detailsCtrl',
        resolve: {
          transactionResolve: function() {
            return data.transactions[index];
          }
        }
      });
    }

    /**
     * 
     */
    function addTransaction() {
      $modal.open({
        templateUrl: '/app/transactions.create/transactions-details.html',
        controller: 'VoucherTransactionsCreateCtrl as transactions',
        resolve: {
          callbackResolve: function() {
            return function addResponseToData(response) {
              data.transactions.push(response);
            }
          }
        }
      });
    }

    function getResidents(name) {
      var filter = {full_name : name};
      var stringFilter = JSON.stringify(filter);
      return Restangular.allUrl('residents').getList({filters: stringFilter}).then(function(res) {
        return res.data;
      });
    }

    function clientCreate() {
      $modal.open({
        templateUrl: '/app/_main/modals/residents/add-resident.html',
        controller: '_AddResidentCtrl as _modal_',
        size: 'lg',
        resolve: {
          datResolve: function() {
            return function(response) {
              onClientNameSelect(response.data.data);
            }
          }
        }
      });
    }

    function onClientNameSelect($item, $model, $label) {
      vm.form.data.client_name = $item.full_name;
      vm.form.data.client_id = $item.id;
    }
  }
})(angular);