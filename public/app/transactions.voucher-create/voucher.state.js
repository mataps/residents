+(function(angular, undefined) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Create a transaction voucher' };
    var modal;

    // 
    function onEnter($modal, $state) {
      function transitionToOverlay() {
        return $state.go('transactions');
      }
        
      modal = $modal
        .open({
          size: 'md',
          controller: 'VoucherCreateController',
          templateUrl: '/app/transactions.voucher-create/voucher.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }
    

    var state = {
      url: '/vouchers/create',
      name: 'transactions.voucher-create',
      data: data,
      onEnter: onEnter,
      onExit: onExit
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);