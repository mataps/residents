+(function(angular, _, undefined) {
  "use strict";
  angular
    .module('app')
    .controller('VoucherTransactionsCreateCtrl', VoucherTransactionsCreateCtrl);

  function VoucherTransactionsCreateCtrl($scope, UtilService, Transaction, Restangular, flash, $state, $modal, callbackResolve, voucherIDResolve, clientIDResolve, baseTransactionResolve, $http) {
    var vm = this;

    angular.extend(vm, {
      form: {
        data: angular.extend(baseTransactionResolve, {
          beneficiary_id: '',
          beneficiary_name: '',
          beneficiary: {},
          referrer_id: '',
          referrer_name: '',
          referrer: {},
          client_id: clientIDResolve,
          client_type: 'Resident',
          beneficiary_type: 'Resident',
          voucher_id: voucherIDResolve,
          vendor_id: baseTransactionResolve.vendor.id,
          vendor_name: baseTransactionResolve.vendor.name,
          category_id: baseTransactionResolve.category.id,
          sub_category_id: baseTransactionResolve.sub_category.id,
          account_type_id: baseTransactionResolve.account.id,
          transaction_type: baseTransactionResolve.type,
          item_id: baseTransactionResolve.item.id,
          item_name: baseTransactionResolve.item.name,
          created_at: ''
        })
      },
      custom: {},
      isCategoryLoading: true
    });

    $scope.$watch(
      function() {
        return vm.form.data.category_id;
      },
      function(changed, old) {
        if ( changed == old ) return;

        // Map and find the category
        // of the given id
        var id = vm.categories
          .map(function(c) { return parseInt(c.id, 10); })
          .indexOf(parseInt(changed, 10));

        vm._selected = vm.categories[id];
      }
    );

    Restangular.one('transactions').one('categories?limit=1000&sort=name').get().then(function(response) {
      vm.categories = response.data.data;
      var id = vm.categories
        .map(function(c) { return parseInt(c.id, 10); })
        .indexOf(parseInt(vm.form.data.category.id));

      vm._selected = vm.categories[id];
      vm.form.data.sub_category_id = baseTransactionResolve.sub_category.id;
      vm.isCategoryLoading = false;
    });

    Restangular.one('transactions').one('accounts?limit=1000&sort=name').get().then(function(response) {
      vm.account_types = response.data.data;
    });

    Restangular.one('transactions').one('items').get().then(function(response) {
      vm.items = response.data.data;
    });

    /**
     *
     */
    vm.$clientCreate = function() {
      $modal.open({
        templateUrl: '/app/_main/modals/residents/add-resident.html',
        controller: '_AddResidentCtrl as _modal_',
        size: 'lg',
        resolve: {
          datResolve: function() {
            return function(response) {
              vm.form.data.client_name = response.data.full_name;
              vm.form.data.client_id = response.data.id;
            }
          }
        }
      });
    }

    /**
     *
     */
    vm.$beneficiaryCreate = function() {
      if ( vm.form.data.beneficiary_type == 'Resident') {
        $modal.open({
          templateUrl: '/app/_main/modals/residents/add-resident.html',
          controller: '_AddResidentCtrl as _modal_',
          size: 'lg',
          resolve: {
            datResolve: function() {
              return function(response) {
                vm.form.data.beneficiary_name = response.data.full_name;
                vm.form.data.beneficiary_id = response.data.id;
              }
            }
          }
        });
      } else {
        $modal.open({
          templateUrl: '/app/transactions.create/_MODALS_/affiliation.html',
          controller: '_AddAffiliationCtrl as _modal_',
          size: 'lg',
          resolve: {
            afResolve: function() {
              return function(data) {
                vm.form.data.beneficiary_name = data.name;
                vm.form.data.beneficiary_id = data.id;
              }
            }
          }
        });
      }
    }

    /**
     *
     */
    vm.$referrerCreate = function() {
      $modal.open({
        templateUrl: '/app/_main/modals/residents/add-resident.html',
        controller: '_AddResidentCtrl as _modal_',
        size: 'lg',
        resolve: {
          datResolve: function() {
            return function(response) {
              vm.form.data.referrer_name = response.data.full_name;
              vm.form.data.referrer_id = response.data.id;
            }
          }
        }
      });
    }

    /**
     *
     */
    vm.$itemRequest = function($value) {
      return $http.get('/api/v1/transactions/items/?filters=' + JSON.stringify({ name: $value }))
        .then(function(response) {
          return response.data.data;
        });
    }

    /**
     * @description  Sends an XHR to the server with
     *               the form data for create a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    vm.$create = function(formData) {
        if ( formData.liquidatable == false ) {
          formData = _.omit(formData, 'liquidatable');
        }

       return Transaction
        .Command
        .create(formData)
        .then(function(response) {
          flash.success = 'Transaction has been successfully created!';
          callbackResolve(response.data.data);
          $scope.$close();
        })
        .catch(function(response) {
          flash.to('validation').error = _.chain(response.data.errors)
            .values()
            .flatten()
            .value();
        });
    }

    vm.$reset = function() {
      if ( !confirm('Are you sure to reset the fields?') )
        return;

      angular.extend(vm.form.data, {
        referrer_name: '',
        referrer_id: '',
        beneficiary_name: '',
        beneficiary_id: '',
        beneficiary: {},
        referrer: {},
        vendor_id: '',
        vendor_name: '',
        category_id: '',
        sub_category_id: '',
        account_type_id: '',
        details: '',
        remarks: '',
        transaction_type: '',
        item_id: '',
        item_name: '',
        created_at: ''
      });

      vm.form.data = _.omit(vm.form.data, 'liquidatable');
      flash.info = 'The fields have been reset!';
    }
  }
})(angular, _);
