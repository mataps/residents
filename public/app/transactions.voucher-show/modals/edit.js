+(function(angular, _, undefined) {

  function VoucherTransactionsEditController($scope, $http, UtilService, Transaction, Restangular, flash, $state, $modal, voucherIDResolve, transactionResolve) {
    var vm = this,
     form = {
        data: angular.extend({}, transactionResolve, {
          voucher_id: voucherIDResolve,
          item_id: transactionResolve.item.id,
          item_name: transactionResolve.item.name,
          beneficiary_id: transactionResolve.beneficiary.id,
          beneficiary_name: transactionResolve.beneficiary[transactionResolve.beneficiary_type == "Resident" ? 'full_name' : 'name'],
          client_id: transactionResolve.client.id,
          client_name: transactionResolve.client[transactionResolve.beneficiary_type == "Resident" ? 'full_name' : 'name'],
          referrer_id: transactionResolve.referrer.id,
          referrer_name: transactionResolve.referrer.full_name,
          sub_category_id: transactionResolve.sub_category.id,
          account_type_id: transactionResolve.account.id
        }),
        custom: {},
        isCategoryLoading: true
      };
    // ------------------------- //
    // ------------------------- //
    vm.$update = $update;
    $scope.$watch(function(){
      return vm.form.data.category;
    }, function(newValue, oldValue){
      console.log(newValue);


      vm.form.data.sub_category_id = newValue.id;
      vm.form.data.category_id = newValue.parent.id
    });

    Restangular.one('transactions').one('categories?limit=1000&sort=name').get().then(function(response) {
      vm.categories = response.data.data;
      vm.isCategoryLoading = false;
    });

    Restangular.one('affiliations').get().then(function(response) {
      vm.affiliations = response.data.data;
    });


    Restangular.one('transactions').one('accounts?limit=1000&sort=name').get().then(function(response) {
      vm.account_types = response.data.data;
    });

    Restangular.one('transactions').one('items').get().then(function(response) {
      vm.items = response.data.data;
    });

    $scope.__M = null;

    $scope.clientCreate = function() {
      $modal.open({
        templateUrl: '/app/_main/modals/residents/add-resident.html',
        controller: '_AddResidentCtrl as _modal_',
        size: 'lg',
        resolve: {
          datResolve: function() {
            return function(response) {
              $scope.onClientNameSelect(response.data.data);
            }
          }
        }
      });
    }

    $scope.beneficiaryCreate = function() {
      if ( form.beneficiary_type == 'Resident' ) {
        $modal.open({
          templateUrl: '/app/_main/modals/residents/add-resident.html',
          controller: '_AddResidentCtrl as _modal_',
          size: 'lg',
          resolve: {
            datResolve: function() {
              return function(response) {
                $scope.onBeneficiaryNameSelect(response.data.data);
              }
            }
          }
        });
      } else {
        $modal.open({
          templateUrl: '/app/transactions.edit/_MODALS_/affiliation.html',
          controller: '_AddAffiliationCtrl as _modal_',
          size: 'sm',
          resolve: {
            afResolve: function() {
              return function(response) {
                $scope.onBeneficiaryNameSelect(response.data.data);
              }
            }
          }
        });
      }
    }

    $scope.referrerCreate = function() {
      $modal.open({
        templateUrl: '/app/_main/modals/residents/add-resident.html',
        controller: '_AddResidentCtrl as _modal_',
        size: 'lg',
        resolve: {
          datResolve: function() {
            return function(response) {
              $scope.onReferrerNameSelect(response.data.data);
            }
          }
        }
      });
    }

    // Form data
    vm.form = form;

    $scope.itemRequest = function($value) {
      return Restangular
        .one('transactions')
        .one('items')
        .getList()
        .then(function(response) {
          return response.data;
        });
    }

    $scope.itemTransform = function($item) {
      vm.form.data.item_id = $item.id;
    }



    $scope.getResidents = function searchResident(name) {
        var filter = {full_name : name};
        var stringFilter = JSON.stringify(filter);
        return Restangular.allUrl('residents').getList({filters: stringFilter}).then(function(res) {
                return res.data;
            });
      }

    // ------------------------- //
    // ------------------------- //
    $scope.onClientNameSelect = function($item, $model, $label) {
        console.log($item);
        vm.form.data.client_name = $item.full_name;
        vm.form.data.client_id = $item.id;
        console.log('ASD');
      }

    $scope.onBeneficiaryNameSelect = function($item, $model, $label) {
        vm.form.data.beneficiary_name = form.data.beneficiary_type == 'Resident' ? $item.full_name : $item.name;
        vm.form.data.beneficiary_id = $item.id;
      }
   $scope.onReferrerNameSelect = function($item, $model, $label) {
        vm.form.data.referrer_name = $item.full_name;
        vm.form.data.referrer_id = $item.id;
      }


    vm.proceedDetails = function(){
      $state.go('transactions.details', {transaction_id : '1'});

    }

    /**
     * @description  Sends an XHR to the server with
     *               the form data for edit a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    function $update(id, formData) {
      console.log(formData);
      // $scope.$close();

       return $http.put('api/v1/transactions/' + id, formData)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    }

    function successXHRCallback(response) {
      flash.success = 'Transaction has been successfully created! Refreshing in a bit...';
      $state.reload();
    }

    function errorXHRCallback(response) {
      flash.to('validation').error = _.chain(response.data.errors)
        .values()
        .flatten()
        .value();
    }
  }

  angular
    .module('app')
    .controller('VoucherTransactionsEditController', VoucherTransactionsEditController);
})(angular, _);
