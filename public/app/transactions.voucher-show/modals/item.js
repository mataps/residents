+(function(angular, undefined) {
  "use strict"
  angular
    .module('app')
    .controller('TransactionLiquidationItemsController', TransactionLiquidationItemsController);

  function TransactionLiquidationItemsController($scope, items) {
    $scope.items = items;
    $scope.total = (function() {
      var total = 0;

      for ( var i = 0; i < $scope.items.length; i++ ) {
        total += parseFloat($scope.items[i].amount, 10);
      }

      return total;
    })();
  }
})(angular);