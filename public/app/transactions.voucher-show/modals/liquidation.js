+(function(angular, undefined) {
  "use strict";
  angular
    .module('app')
    .controller('TransactionLiquidationController', TransactionLiquidationController);

  function TransactionLiquidationController($scope, Restangular, $http, dataResolve, flash, callbackResolve, liquidationsResolve) {

    angular.extend($scope, {
      data: angular.extend({}, dataResolve, { item_name: dataResolve.item.name }),
      isLoading: false,
      $liquidate: $liquidate,
      total: 0,
      items: liquidationsResolve,
      old: {},
      $addItem: $addItem,
      $rmItem: $rmItem,
      $saveItem: $saveItem,
      $editItem: $editItem,
      $cancelItem: $cancelItem
    });

    //
    $total();

    /**
     *
     */
    function $liquidate() {
      flash.info = 'Liquidating the transaction.. please wait.';
      $scope.isLoading = true;
      var data = $scope.data;

      return $http.post('api/v1/transactions/' + data.id + '/liquidate', data)
        .then(function() {
          flash.success = 'Successfully liquidated the transaction';
          callbackResolve();
          $scope.$close();
        })
        .catch(function(r) {
          flash.to('validation').error = _.chain(r.data.errors)
            .values()
            .flatten()
            .value();
        })
        .finally(function() { $scope.isLoading = false });
    }

    /**
     *
     */
    function $addItem() {
      var data = { amount: 0, editing: true, _saved: false, loading: false }
      data.old = angular.copy(data);
      $scope.items.splice(0, 0, data);
      flash.info = 'An item was added to the set of liquidation items';
    }

    /**
     *
     */
    function $rmItem(index) {
      if ( !confirm('Are you sure to delete this item with the label [' + $scope.items[index].label + ']?'))
        return;

        flash.info = 'An item was removed from the set of liquidation items';

      return Restangular.one('transactions', $scope.data.id)
        .one('liquidations', $scope.items[index].id)
        .remove()
        .then(function(r) {
          flash.success = 'Successfully deleted item with label [' + $scope.items[index].label + '].';
          $scope.items.splice(index, 1);
        })
        .catch(function(r) {
          flash.to('validation').error = _.chain(r.data.errors)
            .values()
            .flatten()
            .value();
        })
    }

    /**
     *
     */
    function $editItem(index) {
      $scope.items[index].old = angular.copy($scope.items[index]);
      $scope.items[index].editing = true;
      flash.info = 'An item from the list was updated.';
    }

    /**
     *
     */
    function $saveItem(index) {
      $scope.items[index].loading = true;

      var data = $scope.items[index];
      var saved = data._saved || angular.isUndefined(data._saved);
      var request = !saved ? 'post' : 'customPUT';
      var id = !saved ? '' : data.id;
      var action = !saved ? 'save' : 'update';

      if ( !confirm('Are you sure to ' + action + ' this item with the label [' + data.label + ']?'))
        return;

      flash.info = 'An item with the label [' + data.label + '] is being ' + action + 'd to the database.. please wait.';

      return (
          !saved
          ? Restangular.one('transactions', $scope.data.id)
            .post('liquidations', data)
          : $http.put('api/v1/transactions/' + $scope.data.id + '/liquidations/' + id, data)
        )
        .then(function(r) {
          var _data = r.data.data;

          $scope.items[index] = _data;
        })
        .catch(function(r) {
          flash.to('validation').error = _.chain(r.data.errors)
            .values()
            .flatten()
            .value();
        })
        .finally(function() {
          $scope.items[index].loading = false;
        });
    }

    /**
     *
     */
    function $cancelItem(index) {
      var saved = $scope.items[index]._saved;
      if ( saved || saved == undefined ) {
        if ( !confirm('Are you sure to cancel update?'))
          return;

        $scope.items[index] = $scope.items[index].old;
        $scope.items[index].editing = false;
      } else {
        if ( !confirm('Are you sure to cancel creating a new liquidation item?'))
          return;

        $scope.items.splice(index, 1);
      }
    }

    /**
     *
     */
    function $total() {
      $scope.total = 0;

      if ( $scope.items.length == 1 ) {
        $scope.total = parseFloat($scope.items[0].amount);
      } else {

        for ( var i = 0; i < $scope.items.length; i++ ) {
          $scope.total += parseFloat($scope.items[i].amount);
        }
      }
    }

    $scope.$watch('items', function(changed, old) {
      if (changed !== old) $total();
    }, true);
  }
})(angular);
