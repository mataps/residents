+(function(angular, undefined) {
  "use strict";
  angular
    .module('app')
    .controller('TransactionRefundController', TransactionRefundController);

  function TransactionRefundController($scope, Restangular, $http, dataResolve, flash, callbackResolve) {
    angular.extend($scope, {
      data: angular.extend({}, dataResolve, { beneficiary_id: dataResolve.beneficiary.id, beneficiary_name: dataResolve.beneficiary.full_name }),
      isLoading: false,
      $refund: $refund
    });

    /**
     *
     */
    function $refund() {
      $scope.isLoading = true;
      var data = $scope.data;

      if ( !confirm('Are you sure to refund this transaction?') )
        return;

      flash.info = 'The transaction will now be refunded.. please wait.';

      return Restangular
        .one('transactions', data.id)
        .post('refund', data)
        .then(function(r) {
          flash.success = 'Transaction has been successfully refunded';
          callbackResolve(r.data);
          $scope.$close();
        })
        .catch(function(r) { flash.to('validation').error = _.chain(r.data.data).values().flatten().value(); })
        .finally(function() { $scope.isLoading = false; });
    }
  }
})(angular);
