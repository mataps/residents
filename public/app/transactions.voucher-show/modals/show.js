+(function(angular, _, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('VoucherTransactionsController', VoucherTransactionsController);

  function VoucherTransactionsController($scope, $state, $window, $modal, Transaction, UtilService, Restangular, flash, transactionResolve, voucherResolve, callbackResolve, $http) {
    // console.log(Transaction);
    var vm = this;
    angular.extend(vm, {
      isLoading: false,
      form: { data: angular.extend({}, transactionResolve, {
        beneficiary_id: transactionResolve.beneficiary.id,
        beneficiary_name: transactionResolve.beneficiary[transactionResolve.beneficiary_type.toLowerCase() == 'resident' ? 'full_name' : 'name'],
        client_id: transactionResolve.client.id,
        client_name: transactionResolve.client.full_name,
        client_type: 'Resident',
        account_type_id: transactionResolve.account.id,
        referrer_id: transactionResolve.referrer.id,
        referrer_name: transactionResolve.referrer.name,
        voucher_id: voucherResolve.id,
        item_name: transactionResolve.item.name,
        vendor_id: transactionResolve.vendor.id,
        vendor_name: transactionResolve.vendor.name,
        category_id: transactionResolve.category.id,
        sub_category_id: transactionResolve.sub_category.id,
      })},
      voucher: voucherResolve
    });

    // Watch
    $scope.$watch(
      function() {
        return vm.form.data.category_id;
      },
      function(changed, old) {
        if ( changed == old ) return;

        // Map and find the category
        // of the given id
        var id = vm.categories
          .map(function(c) { return parseInt(c.id, 10); })
          .indexOf(parseInt(changed, 10));

        vm._selected = vm.categories[id];
      }
    );

    Restangular.one('transactions').one('categories?limit=1000&sort=name').get().then(function(response) {
      vm.categories = response.data.data;

      var id = vm.categories
        .map(function(c) { return parseInt(c.id, 10); })
        .indexOf(parseInt(vm.form.data.category.id));

        vm._selected = vm.categories[id];
    });

    Restangular.one('transactions').one('accounts?limit=1000').get().then(function(response) {
      vm.account_types = response.data.data;
    });

    Restangular.one('transactions').one('items').get().then(function(response) {
      vm.items = response.data.data;
    });

    /**
     *
     */
    vm.$refund = function() {
      $modal.open({
        templateUrl: '/app/transactions.voucher-show/modals/refund.html',
        controller: 'TransactionRefundController',
        resolve: {
          dataResolve: function() {
            return vm.form.data;
          },
          callbackResolve: function() {
            return function(data) {
              vm.form.data.refund = data || {};
            }
          }
        }
      });
    }

    // ------------------------- //
    // ------------------------- //

    vm.$settle = function () {
      if ( !confirm('Are you sure to settle this transaction?') ) {
        return;
      }

      return Restangular.one('transactions')
        .one('settle', vm.form.data.id)
        .post()
        .then(function(response) {
          flash.success = "Transaction has been settled";
          vm.form.data.settled_at = new Date();
          $state.reload();
        });
    }

    /**
     *
     */
    vm.$edit = function() {
      $modal.open({
        templateUrl: '/app/transactions.details/transactions-edit.html',
        controller: 'VoucherTransactionsController as detailsCtrl',
        resolve: {
          transactionResolve: function() {
            return transactionResolve;
          },
          voucherResolve: function() {
            return voucherResolve;
          },
          callbackResolve: function() {
            return callbackResolve;
          }
        }
      });
    }

    /**
     * @description  Sends an XHR to the server with
     *               the form data for edit a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    vm.$update = function(id, formData) {
      return $http.put('api/v1/transactions/' + id, formData)
        .then(function(r) {
          flash.success = 'Transaction has been successfully updated!';
          callbackResolve(formData);
          $scope.$dismiss();
        })
      .catch(function(response) {
          flash.to('validation').error = _.chain(response.data.errors)
            .values()
            .flatten()
            .value();
      });
    }

    /**
     *
     */
    function $liquidate() {
      $modal.open({
        templateUrl: '/app/transactions.voucher-show/modals/liquidation.html',
        controller: 'TransactionLiquidationController',
        resolve: {
          dataResolve: function() {
            return vm.form.data;
          },
          callbackResolve: function() {
            return function() {
              vm.form.data.liquidated_at = new Date().getTime().toString();
            }
          },
          liquidationsResolve: function() {
            return Restangular.one('transactions', vm.form.data.id)
              .getList('liquidations', JSON.stringify({ limit: 1000 }))
              .then(function(r) {
                return r.data.data;
              });
          }
        }
      })
    }

    /**
     *
     */
    vm.$liquidate = function() {
      $modal.open({
        templateUrl: '/app/transactions.voucher-show/modals/liquidation.html',
        controller: 'TransactionLiquidationController',
        resolve: {
          liquidationsResolve: function() {
            return Restangular.one('transactions', vm.form.data.id)
              .getList('liquidations', JSON.stringify({ limit: 1000 }))
              .then(function(r) {
                return r.data;
              });
          },
          dataResolve: function() {
            return vm.form.data;
          },
          callbackResolve: function() {
            return function() {
              vm.form.data.liquidated_at = new Date().getTime().toString();
              console.log(vm.form.data);
            }
          }
        }
      });
    }

    /**
     *
     */
    vm.$beneficiaryCreate = function() {
      if ( vm.form.data.beneficiary_type == 'Resident') {
        $modal.open({
          templateUrl: '/app/_main/modals/residents/add-resident.html',
          controller: '_AddResidentCtrl as _modal_',
          size: 'lg',
          resolve: {
            datResolve: function() {
              return function(response) {
                vm.form.data.beneficiary_name = $item.full_name;
                vm.form.data.beneficiary_id = $item.id;
              }
            }
          }
        });
      } else {
        $modal.open({
          templateUrl: '/app/transactions.create/_MODALS_/affiliation.html',
          controller: '_AddAffiliationCtrl as _modal_',
          size: 'lg',
          resolve: {
            afResolve: function() {
              return function(data) {
                vm.form.data.beneficiary_name = data.name;
                vm.form.data.beneficiary_id = data.id;
              }
            }
          }
        });
      }
    }

    /**
     *
     */
    vm.$referrerCreate = function() {
      $modal.open({
        templateUrl: '/app/_main/modals/residents/add-resident.html',
        controller: '_AddResidentCtrl as _modal_',
        size: 'lg',
        resolve: {
          datResolve: function() {
            return function(response) {
              vm.form.data.referrer_name = $item.full_name;
              vm.form.data.referrer_id = $item.id;
            }
          }
        }
      });
    }

    /**
     *
     */
    vm.$itemRequest = function($value) {
      return $http.get('/api/v1/transactions/items/?filters=' + JSON.stringify({ name: $value }))
        .then(function(response) {
          return response.data.data;
        });
    }

    /**
     *
     */
    vm.$viewLiquidation = function() {
      $modal.open({
        controller: 'TransactionLiquidationItemsController',
        templateUrl: '/app/transactions.voucher-show/modals/item.html',
        resolve: {
          items: function() {
            return $http.get('/api/v1/transactions/' + vm.form.data.id + '/liquidations' + '?limit=1000')
              .then(function(r) {
                return r.data.data;
              });
          }
        }
      })
    }
  }
})(angular, _);
