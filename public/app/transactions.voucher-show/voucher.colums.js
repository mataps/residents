+(function(angular, undefined) {
  'use strict';

  var columns = [
    {
      key: 'beneficiary.full_name',
      label: 'Beneficiary Name',
      show: true,
      cellTemplate: '{{ transaction.beneficiary_type.toLowerCase() == "resident" ? transaction.beneficiary.full_name : transaction.beneficiary.name }}',
      onClick: 'openTransaction($parent.$index)'
    },
    {
      key: 'account.id',
      label: 'Account',
      show: true,
      cellTemplate: '{{ transaction.account.name }}',
      onClick: 'openTransaction($parent.$index)'
    },
    {
      key: 'category',
      label: 'Category',
      show: true,
      cellTemplate: '{{ transaction.category.name }}',
      onClick: 'openTransaction($parent.$index)'
    },
    {
      key: 'sub_category',
      label: 'Sub-Category',
      show: true,
      cellTemplate: '{{ transaction.sub_category.name }}',
      onClick: 'openTransaction($parent.$index)'
    },
    {
      key: 'total_cash',
      label: 'Amount',
      show: true,
      cellTemplate: '{{ transaction.amount | money }} PHP',
      onClick: 'openTransaction($parent.$index)'
    },
    {
      key: 'settled_at',
      label: 'Settled',
      show: true,
      cellTemplate: '<span ng-if="transaction.settled_at !== \'\' && !!transaction.settled_at.length && !transaction.settled_at !== null">' +
          '<i class="ion-checkmark-circled" style="color: green;"></i>' +
        '</span>' +

        '<span ng-if="transaction.settled_at == \'\' || !transaction.settled_at.length || transaction.settled_at == null">' +
          '<i class="ion-close-circled" style="color: red;"></i>' +
        '</span>',
      onClick: 'openTransaction($parent.$index)'
    },
    {
      key: 'liquidated_at',
      label: 'Liquidated',
      show: true,
      cellTemplate: '<span ng-if="!!transaction.settled_at.length && transaction.settled_at !== null && transaction.settled_at !== \'0000-00-00 00:00:00\'">' +
        '<button type="button" ' +
          'class="btn btn-info" ' +
          'ng-click="$liquidate(transaction)" ' +
          'ng-if="!!transaction.liquidatable && (!transaction.liquidated_at.length || transaction.liquidated_at == null)">' +
          'Liquidate' +
        '</button>' +
        '<span ng-if="!!transaction.liquidatable && (!!transaction.liquidated_at.length && transaction.liquidated_at !== null)"><i class="ion-checkmark-circled" style="color: green;"></i> Liquidated</span>' +
        '<span ng-if="!transaction.liquidatable"><i class="ion-close-circled" style="color: red;"></i> Non-liquidatable</span>' +
        '</span>',
      onClick: '!!transaction.settled_at.length || transaction.settled_at !== null || transaction.settled_at !== \'0000-00-00 00:00:00\' && (!!transaction.liquidatable && (!transaction.liquidated_at.length || transaction.liquidated_at == null)) ? null : openTransaction($parent.$index)'
    },
  ];

  angular
    .module('app')
    .constant('VOUCHER_TRANSACTIONS_COLUMNS', columns);
})(angular);
