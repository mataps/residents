
+(function(angular, _, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('VoucherController', VoucherController);

  function VoucherController($scope, $window, $modal, $stateParams, Restangular, VOUCHER_TRANSACTIONS_COLUMNS, flash, $q) {
    var data = {};
    var transactions = [];
    var columns = VOUCHER_TRANSACTIONS_COLUMNS;

    init();

    angular.extend($scope, {
      data: data,
      transactions: transactions,
      columns: columns,
      // isLoading: true,
      isLoading: false,

      settle: settle,
      print: print,
      printIndividual: printIndividual,
      openTransaction: openTransaction,
      addTransaction: addTransaction,
      $liquidate: $liquidate,
      $settleAll: $settleAll,
      isVoucherSettleLoading: true
    });

    /**
     *
     */
    function init() {
      Restangular.one('vouchers', $stateParams.voucher_id)
        .one('transactions')
        .getList()
        .then(function(response) { angular.extend(transactions, response.data); })
        .finally(function() { $scope.isLoading = false; });

      Restangular.one('vouchers', $stateParams.voucher_id)
        .get()
        .then(function success(response) { angular.extend(data, response.data); });
    }

    /**
     *
     */
    function settle() {
      if ( !confirm('Are you sure to settle this transaction?') ) {
        return;
      }

      $scope.isSettling = true;

      Restangular.one('vouchers', $stateParams.voucher_id)
        .one('settle')
        .post()
        .then(function success(response) {
          flash.success = 'Voucher has been successfully settled!';
          data.settled_at = new Date();
        })
        .catch(function error(response) {
          flash.error = 'An error has occured while settling the voucher. Please try again.';
        })
        .finally(function atLast() {
          $scope.isSettling = false;
        })
    }

    function print() {
      var win = $window.open($window.location.origin + '/api/v1/vouchers/' + data.id + '/print/pdf', '_blank');
      //Browser has allowed it to be opened
      if( win ) {
        win.focus();
      } else {
        flash.error = 'Please allow popups for this website.';
      }
    }

    function printIndividual() {
      var win = $window.open($window.location.origin + '/api/v1/vouchers/' + data.id + '/individual/print/pdf', '_blank');
      //Browser has allowed it to be opened
      if( win ) {
        win.focus();
      } else {
        flash.error = 'Please allow popups for this website.';
      }
    }

    /**
     *
     */
    function openTransaction(index) {
      flash.info = 'Opening transaction window.. loading data..';

      $modal.open({
        templateUrl: '/app/transactions.voucher-show/modals/show.html',
        controller: 'VoucherTransactionsController as detailsCtrl',
        size: 'lg',
        resolve: {
          transactionResolve: function() {
            return transactions[index];
          },

          voucherResolve: function() {
            return data;
          },

          voucherIDResolve: function() {
            return data.id;
          },

          callbackResolve: function() {
            return function(data) {
              transactions[index] = data;
            }
          }
        }
      });
    }

    /**
     *
     */
    function addTransaction() {
      flash.info = 'Opening transaction creation window..';

      $modal.open({
        templateUrl: '/app/transactions.voucher-show/modals/create.html',
        controller: 'VoucherTransactionsCreateCtrl as createCtrl',
        resolve: {
          voucherIDResolve: function() {
            return $scope.data.id
          },
          callbackResolve: function() {
            return function addResponseToData(data) {
              $scope.transactions.push(data);
            }
          },
          clientIDResolve: function() {
            return $scope.data.client.id;
          },
          baseTransactionResolve: function() {
            return transactions[0];
          }
        }
      });
    }

    /**
     *
     */
    function $liquidate(transaction) {
      $modal.open({
        templateUrl: '/app/transactions.voucher-show/modals/liquidation.html',
        controller: 'TransactionLiquidationController',
        resolve: {
          dataResolve: function() {
            return transaction;
          },
          callbackResolve: function() {
            return function() {
              transaction.liquidated_at = new Date().getTime().toString();
            }
          },
          liquidationsResolve: function() {
            return Restangular.one('transactions', transaction.id)
              .getList('liquidations', JSON.stringify({ limit: 1000 }))
              .then(function(r) {
                return r.data;
              });
          }
        }
      })
    }

    /**
     *
     */
    function $settleAll() {
      var unsettled = transactions
        .filter(function(transaction) {
          return !transaction.settled_at.length ||
            transaction.settled_at == null
        });

      if ( !unsettled.length ) {
        flash.error = 'All transactions have already been settled.';
        return;
      }

      if ( !confirm('Are you sure to settle all transactions in this voucher?') ) {
        console.log(unsettled);
        return;
      }

      $scope.isVoucherSettleLoading = true;

      return $q.all(unsettled
        .map(function(transaction) {
          return Restangular
            .one('transactions')
            .one('settle', transaction.id)
            .post();
        })
      )
        .then(function(response) {
          flash.success = "The transactions have been settled";
          $state.reload();
          return response;
        })
        .catch(function(response) {
          flash.to('validation').error = _.chain(response.data.errors)
            .values()
            .flatten()
            .value();
          return response;
        })
        .finally(function() {
          $scope.isVoucherSettleLoading = false;
            $state.reload();
        });
    }
  }
})(angular, _);
