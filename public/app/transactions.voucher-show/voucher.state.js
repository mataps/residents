+(function(angular, undefined) {
  function stateConfig($stateProvider) {
    /**
     * Data
     * @type {Object}
     */
    var data = { pageTitle: 'Voucher Details' };
    var modal;

    //
    function onEnter($modal, $state, flash) {
      function transitionToOverlay() {
        return $state.go('transactions');
      }

      modal = $modal
        .open({
          size: 'md',
          controller: 'VoucherController',
          templateUrl: '/app/transactions.voucher-show/voucher.html'
        });

      modal
        .result
        .then(transitionToOverlay)
        .catch(transitionToOverlay);
    }

    function onExit() {
      modal.close();
    }


    var state = {
      url: '/vouchers/{voucher_id}',
      name: 'transactions.voucher-show',
      data: data,
      onEnter: onEnter,
      onExit: onExit,
      resolve: {
        loadingNotificationResolve: function(flash) {
          flash.info = 'Opening voucher window.. loading data..';
        }
      }
    };

    $stateProvider.state(state);
  }

  angular
    .module('app')
    .config(stateConfig);
})(angular);
