+(function(angular, undefined) {
  var statusTemplate = [
    "<span class='label label-warning badge' ng-if='!transaction.settled_at.length || transaction.settled_at == null'><i class='ion-minus-circled'></i></span>",
    "<span ng-if='!!transaction.settled_at.length && transaction.settled_at !== null'><span class='label label-success badge'><i class='ion-checkmark-circled'></i></span> {{:: transaction.settled_at | timestamp | date: 'mediumDate' }}</span>"
    ].join('');

  var liquidateTemplate = [
    '<span ng-if="!!transaction.liquidatable">',
      '<span ng-if="!!transaction.liquidated_at.length && transaction.liquidated_at !== null"><i class="ion-checkmark-circled" style="color: green;"></i></span>',
      '<span ng-if="!transaction.liquidated_at.length || transaction.liquidated_at == null"><i class="ion-close-circled" style="color: red;"></i></span>',
    '</span>',
    '<span ng-if="!transaction.liquidatable"><i class="ion-alert-circled" style="color: rgb(255, 136, 20);"></i> Non-liquidatable</span>'
  ].join('');

    var transaction_type = [
    '<span ng-switch="transaction.transaction_type">',
      '<span ng-switch-when="expense" class="label label-primary">Expense</span>',
      '<span ng-switch-when="non-monetary" class="label label-default">Non-Monetary</span>',
      '<span ng-switch-when="transfer-income" class="label" style="background: #9b59b6; color: #ecf0f1">Transfer Income</span>',
      '<span ng-switch-when="transfer-expense" class="label" style="background: #1abc9c; color: #ecf0f1">Transfer Expense</span>',
      '<span ng-switch-when="income" class="label" style="background: #E7E426; color: #333;">Income</span>',
    '</span>'
  ].join('');

  var columns = [
    {
      key: 'voucher_uuid',
      label: 'Voucher #',
      show: true,
      cellTemplate: '<a ui-sref="transactions.voucher-show({ voucher_id: transaction.voucher_id })">{{:: transaction.voucher_uuid }}</a>'
    },
    {
      key: 'client_name',
      label: 'Client Name',
      show: true,
      // cellTemplate: '{{:: transaction.client_type.toLowerCase() == "resident" ? transaction.client_full_name : transaction.client_name }}',
      cellTemplate: '{{:: transaction.client_name }}',
      onClick: 'transactions.show(transaction.id)'
    },
    {
      key: 'beneficiary_name',
      label: 'Beneficiary Name',
      show: true,
      // cellTemplate: '{{:: transaction.beneficiary_full_name }}',
      cellTemplate: '{{:: transaction.beneficiary_name }}',
      onClick: 'transactions.show(transaction.id)'
    },
    {
      key: 'account_name',
      label: 'Account',
      show: true,
      cellTemplate: '{{:: transaction.account_name }}',
      onClick: 'transactions.show(transaction.id)'
    },
    {
      key: 'transaction_type',
      label: 'Transaction Type',
      show: true,
      cellTemplate: transaction_type,
      onClick: 'transactions.show(transaction.id)'
    },
    {
      key: 'category',
      label: 'Category',
      show: true,
      cellTemplate: '{{:: transaction.category }}',
      onClick: 'transactions.show(transaction.id)'
    },
    {
      key: 'sub_category',
      label: 'Sub-Category',
      show: true,
      cellTemplate: '{{:: transaction.sub_category }}',
      onClick: 'transactions.show(transaction.id)'
    },
        {
      key: 'item',
      label: 'Item',
      show: true,
      cellTemplate: '{{:: transaction.item }}',
      onClick: 'transactions.show(transaction.id)'
    },
     {
      key: 'details',
      label: 'Details',
      show: true,
      cellTemplate: '{{:: transaction.details }}',
      onClick: 'transactions.show(transaction.id)'
    },
        {
      key: 'vendor_name',
      label: 'Vendor',
      show: true,
      cellTemplate: '{{:: transaction.vendor_name }}',
      onClick: 'transactions.show(transaction.id)'
    },
    {
      key: 'amount',
      label: 'Amount',
      show: true,
      cellTemplate: '{{:: transaction.amount | money }} PHP',
      onClick: 'transactions.show(transaction.id)'
    },
    {
      key: 'settled_at',
      label: 'Settled At',
      show: true,
      cellTemplate: statusTemplate,
      field1: 'voucher.settled_at_from',
      field2: 'voucher.settled_at_to',
      onClick: 'transactions.show(transaction.id)'
    },
    {
      key: 'liquidatable',
      label: 'Liquidatable',
      show: true,
      cellTemplate: liquidateTemplate,
      onClick: 'transactions.show(transaction.id)'
    },
    {
      key: 'created_at',
      label: 'Created At',
      show: true,
      cellTemplate: "{{:: transaction.created_at | timestamp | date: 'mediumDate' }}",
      onClick: 'transactions.show(transaction.id)'
    }
  ];

  angular
    .module('app')
    .constant('TRANSACTIONS_COLUMNS', columns);
})(angular);
