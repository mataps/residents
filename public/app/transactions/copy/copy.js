+(function(angular, _, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('TransactionCopyController', TransactionCopyController);

  function TransactionCopyController($scope, $state, $window, $modal, Transaction, Restangular, flash, transactionResolve,  $http) {
    var vm = this;

    angular.extend(vm, {
      isLoading: false,
      data: angular.extend({}, transactionResolve, {
        beneficiary_id: '',
        beneficiary_name: '',
        beneficiary: { full_name: '', name: '' },
        beneficiary_type: 'Resident',
        client_id: '',
        client_name: '',
        client_type: 'Resident',
        client: { full_name: '', name: '' },
        referrer_id: '',
        referrer_name: '',
        referrer: { full_name: '', name: '' },
        account_type_id: transactionResolve.account.id,
        item_name: transactionResolve.item.name,
        vendor_id: transactionResolve.vendor.id,
        vendor_name: transactionResolve.vendor.name,
        category_id: transactionResolve.category.id,
        sub_category_id: transactionResolve.sub_category.id
      }),
      _selected: transactionResolve.category,
      custom: {},
      isCategoryLoading: true
    });

    // Watch
    $scope.$watch(
      function() {
        return vm.data.category_id;
      },
      function(changed, old) {
        if ( changed == old ) return;

        // Map and find the category
        // of the given id
        var id = vm.categories
          .map(function(c) { return parseInt(c.id, 10); })
          .indexOf(parseInt(changed, 10));

        vm._selected = vm.categories[id];
      }
    );

    Restangular.one('transactions').one('categories?limit=1000&sort=name').get().then(function(response) {
      vm.categories = response.data.data;

      var id = vm.categories
        .map(function(c) { return parseInt(c.id, 10); })
        .indexOf(parseInt(vm.data.category.id));

        vm._selected = vm.categories[id];
        vm.isCategoryLoading = false;
    });

    Restangular.one('transactions').one('accounts?limit=1000').get().then(function(response) {
      vm.account_types = response.data.data;
    });

    Restangular.one('transactions').one('items').get().then(function(response) {
      vm.items = response.data.data;
    });

    // ------------------------- //
    // ------------------------- //

    /**
     * @description  Sends an XHR to the server with
     *               the form data for edit a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    vm.$copy = function(formData) {
      vm.isLoading = true;

      var ___data = !!formData.liquidatable
        ? formData
        : _.omit(formData, 'liquidatable');

      return $http.post('api/v1/transactions', ___data)
        .then(function(r) {
          flash.success = 'Transaction has been successfully copied!';
          $state.go('transactions', null, { reload: true });
          $scope.$close();
        })
      .catch(function(response) {
          flash.to('validation').error = _.chain(response.data.errors)
            .values()
            .flatten()
            .value();
      })
      .finally(function() {
        vm.isLoading = false;
      });
    }

    /**
     *
     */
    vm.$beneficiaryCreate = function() {
      if ( vm.data.beneficiary_type == 'Resident') {
        $modal.open({
          templateUrl: '/app/_main/modals/residents/add-resident.html',
          controller: '_AddResidentCtrl as _modal_',
          size: 'lg',
          resolve: {
            datResolve: function() {
              return function(response) {
                vm.data.beneficiary_name = $item.full_name;
                vm.data.beneficiary_id = $item.id;
              }
            }
          }
        });
      } else {
        $modal.open({
          templateUrl: '/app/transactions.create/_MODALS_/affiliation.html',
          controller: '_AddAffiliationCtrl as _modal_',
          size: 'lg',
          resolve: {
            afResolve: function() {
              return function(data) {
                vm.data.beneficiary_name = data.name;
                vm.data.beneficiary_id = data.id;
              }
            }
          }
        });
      }
    }

    vm.$clientCreate = function() {
      $modal.open({
        templateUrl: '/app/_main/modals/residents/add-resident.html',
        controller: '_AddResidentCtrl as _modal_',
        size: 'lg',
        resolve: {
          datResolve: function() {
            return function(response) {
              vm.form.data.client_name = response.data.full_name;
              vm.form.data.client_id = response.data.id;
            }
          }
        }
      });
    }


    /**
     *
     */
    vm.$referrerCreate = function() {
      $modal.open({
        templateUrl: '/app/_main/modals/residents/add-resident.html',
        controller: '_AddResidentCtrl as _modal_',
        size: 'lg',
        resolve: {
          datResolve: function() {
            return function(response) {
              vm.data.referrer_name = $item.full_name;
              vm.data.referrer_id = $item.id;
            }
          }
        }
      });
    }

    /**
     *
     */
    vm.$itemRequest = function($value) {
      return $http.get('/api/v1/transactions/items/?filters=' + JSON.stringify({ name: $value }))
        .then(function(response) {
          return response.data.data;
        });
    }
  }
})(angular, _);
