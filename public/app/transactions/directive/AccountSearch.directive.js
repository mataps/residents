+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcAccountSearch', directive);

  function directive($compile) {
    return { link: linkFn }

    function linkFn(scope, element, attributes, controller) {
      if ( element.attr('gc-account-search') == 'account' ) {
        attributes.$set('ngModel', 'transactions.account_temp');
      }

      element.removeAttr('gc-account-search');
      $compile(element)(scope);
    }
  }
})(angular);