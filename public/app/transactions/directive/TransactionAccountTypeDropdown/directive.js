+(function(angular, undefined) {
  'use strict'
  angular
    .module('app')
    .directive('gcTransactionAccountTypeDropdown', TransactionAccountTypeDropdown);

  function TransactionAccountTypeDropdown() {
    return {
      scope: {
        model: '=ngModel'
      },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    function controllerFn($scope, $http) {
      angular.extend($scope, {
        isLoading: false,
        types: []
      });

      (function $init() {
        $scope.isLoading = true;

        $http.get('api/v1/transactions/accounts')
          .then(function(response) { $scope.types = response.data.data })
          .finally(function() { $scope.isLoading = false; });
      })();
    }

    /**
     * Directive template
     */
    function templateFn() {
      return [
        '<div>',
          '<select ng-model="model" class="form-control">',
            '<option value=""> {{ !isLoading ? \'Select an account type..\' : \'Loading types..\' }} </option>',
            '<option value="{{ type.id }}" ng-repeat="type in types"> {{ type.name }} </option>',
          '</select>',
        '</div>'
      ].join('\n');
    }
  }
})(angular);