+(function(angular, undefined) {
  'use strict'
  angular
    .module('app')
    .directive('gcTransactionCategoryDropdown', TransactionCategoryDropdown);

  function TransactionCategoryDropdown() {
    return {
      scope: {
        model: '=ngModel'
      },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    function controllerFn($scope, $http) {
      angular.extend($scope, {
        isLoading: false,
        categories: []
      });

      (function $init() {
        $scope.isLoading = true;

        $http.get('api/v1/transactions/categories')
          .then(function(response) { $scope.categories = response.data.data })
          .finally(function() { $scope.isLoading = false; });
      })();
    }

    /**
     * Directive template
     */
    function templateFn() {
      return [
        '<div>',
          '<select ng-model="model" class="form-control">',
            '<option value=""> {{ !isLoading ? \'Select a category..\' : \'Loading categories..\' }} </option>',
            '<optgroup ng-repeat="category in categories" label="{{ category.name }}">',
              '<option ng-repeat="sub_category in category.sub_categories.data" value="{{ sub_category.id }}">{{ sub_category.name }}</option>',
            '</optgroup>',
          '</select>',
        '</div>'
      ].join('\n');
    }
  }
})(angular);