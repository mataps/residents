+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcTransactionItemTypeahead', TransactionItemTypeahead);

  function TransactionItemTypeahead() {
    return {
      scope: {
        /**
         * Model / variable (ID) to assign
         * the id of the result
         * < .. ng-model="data.item.id">
         * < .. ng-model="data.item_id">
         */
        model: '=ngModel',

        /**
         * Blacklisted IDs (array)
         * <.. blacklisted="[1, 2, 3]"
         */
        blacklisted: '='
      },

      // replace: true,
      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    /**
     * Directiv template
     */
    function templateFn(e, attrs) {
      var placeholder = attrs.placeholder || 'Enter item name..';

      return [
        '<input type="text" ',
          'ng-model="temp.name" ',
          'class="form-control" ',
          'placeholder="', placeholder,'" ',
          'typeahead-on-select="$itemOnSelect($item)" ',
          'typeahead="item.name for item in $items()">',
      ].join('');
    }

    /**
     * Controllre
     */
    function controllerFn($scope, Restangular, flash) {
      angular.extend($scope, {
        isLoading: false,
        temp: {},
        $items: $items,
        $itemOnSelect: $itemOnSelect
      });

      /**
       * List of items.
       * Request the API for the list of items
       */
      function $items () {
        $scope.isLoading = true;

        return Restangular
          .one('transactions')
          .one('items')
          .getList()
          .then(function(r) { return r.data })
          .finally(function() { $scope.isLoading = false });
      };

      /**
       * Assigns the selected item from the typeahead
       * to the model. Also validates it
       */
      function $itemOnSelect($item) {
        // If the blacklist exists, yolo.
        if ( !angular.isUndefined($scope.blacklisted) &&
          $scope.blacklisted.indexOf($item.id) == -1
        ) {
          $scope.temp = {};
          flash.error = 'The item already exists in your list';
          return;
        }

        $scope.model = $item.id;
      }
    }
  }
})(angular);