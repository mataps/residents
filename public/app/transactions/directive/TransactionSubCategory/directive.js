+(function(angular, undefined) {
  "use strict";
  angular
    .module('app')
    .directive('gcTransactionSubCategory', directive);

  function directive() {
    return {
      scope: {
        // $scope model to bind the data.
        // `ngModel` So it comes out naturally (<yolo ng-model="">)
        model: '=ngModel'
      },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    /**
     * {}
     */
    function controllerFn($scope, $http) {
      angular.extend($scope, {
        categories: [],
        isLoading: false,
        isSuccess: false
      });

      // Init
      (function $init() {
        $http.get('api/v1/sub_categories/')
          .then(function(response) {
            $scope.categories = response.data.data;
            $scope.isSuccess = true;
          })
          .finally(function() { $scope.isLoading = false; });
      })();
    }

    /**
     * Directive template
     */
    function templateFn() {
      return [
        '<select ng-model="model" class="form-control">',
          '<option> {{ isLoading ? \'Loading...\' : (!isSuccess ? \'An error occured..\' : \'Select a sub category\') }}</option>',
          '<option ng-repeat="category in categories" value="{{ category.id }}">{{ category.name }} </option>',
        '</select>'
      ].join('');
    }
  }
})(angular);