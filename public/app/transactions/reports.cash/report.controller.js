+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('TransactionCashFlowReportController', TransactionCashFlowReportController);

  function TransactionCashFlowReportController($scope, Restangular, USER, $filter) {
    Restangular
      .one('transactions')
      .one('accounts')
      .get()
      .then(function(response) {
        $scope.account_types = response.data.data;
        $scope.account_types.forEach(function(type) {
          if ( permissions.indexOf(type.name.toLowerCase()) !== -1 ) {
            $scope.filtered_account_types.push(type);
          }
        });

        if ( !!$scope.filtered_account_types.length ) {
          $scope.report.account_type_id = $scope.filtered_account_types[0].id;
        }
      });

    $scope.report = { account_type_id: [] };
    $scope.filtered_account_types = [];
    $scope.report = { datepicker: {} };
    var permissions = (function(user) {
      return user.permissions
        .filter(function(permission) {
          return permission.component.toLowerCase() == 'transaction system'
            && permission.status == 1;
        })
        .map(function(permission) {
          return permission.functionality
            .toLowerCase()
            .replace(/(view|manage|transactions)\s/g, '')
            .trim();
        });
    })(USER);

    $scope._format = function(x) {
      return angular.isArray(x) ? x.join(',') : x;
    }

    $scope._date = function(x) {
      return x instanceof Date ? $filter('date')(x, 'd-M-yyyy') : x;
    }
  }
})(angular);