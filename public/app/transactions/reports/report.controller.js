+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('TransactionReportController', TransactionReportController);

  function TransactionReportController($scope, Restangular, USER, $filter, UtilService, columnsResolve) {
    $scope._columns = columnsResolve;
    $scope.columns = (function(obj) {
      columnsResolve.map(function(c) { obj[c.key] = c.show; });
      console.log(obj);
      return obj;
    })({});
    $scope.report = { account_type_id: [] };
    $scope.filtered_account_types = [];
    $scope.custom = { datepickers: {} };
    var permissions = (function(user) {
      return user.permissions
        .filter(function(permission) {
          return permission.component.toLowerCase() == 'transaction system'
            && permission.status == 1;
        })
        .map(function(permission) {
          return permission.functionality
            .toLowerCase()
            .replace(/(view|manage|transactions)\s/g, '')
            .trim();
        });
    })(USER);
    Restangular
      .one('transactions')
      .one('accounts')
      .get()
      .then(function(response) {
        $scope.account_types = response.data.data;
        $scope.account_types.forEach(function(type) {
          if ( permissions.indexOf(type.name.toLowerCase()) !== -1 ) {
            $scope.filtered_account_types.push(type);
          }
        });

        if ( !!$scope.filtered_account_types.length ) {
          $scope.report.account_type_id = $scope.filtered_account_types[0].id;
        }
      });

    $scope._format = function(x) {
      return angular.isArray(x) ? x.join(',') : x;
    }

    $scope._date = function(x) {
      return $filter('date')(x, 'd-M-yyyy');
    }

    $scope.$report = function(type) {
      var keys = Object.keys($scope.columns).filter(function(c) { return $scope.columns[c]; });
      
      return '/api/v1/transactions/' + $scope.report.type + '/' + type + o2q(angular.extend({}, $scope.report, {
        to: $scope._date($scope.report.to), 
        from: $scope._date($scope.report.from),
        account_type_id: $scope._format($scope.report.account_type_id)
      })) + '&fields=' + keys.join(',');
    }

    function o2q(query) {
      // This function does not accept primitives and functions
      if (query == null || typeof query !== 'object') {
        throw new Error('Provided query must be an object');
      }

      var result = ''; // Our resulting string
      var keys = Object.keys(query);

      // Using native loops instead of `map` or `forEach`
      // http://jsperf.com/native-map-versus-array-looping
      // Now we'll iterate through the splitted keyz
      for ( var i = 0; i < keys.length; i++ ) {
        var key = keys[i];
        var _value = query[key]; // Just our stringified JSON

        // We'll check first if the value is null or undefined. We'll
        // have to just go to the next key since we do not want or
        // need to add it to the resulting qs.
        if ( _value === undefined || _value === null ) continue;

        var value = encodeURIComponent(_value);
        
        // Prepends the ampsersand separator `&` for
        // each key except the first key
        var _prepend = (i > 0 && !!result.length ? '&' : '');

        result += _prepend + key + '=' + value;
      }

      return '?' + result;
    }
  }
})(angular);