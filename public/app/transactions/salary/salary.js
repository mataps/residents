+(function(angular, _, undefined) {
  'use strict';
  angular
    .module('app')
    .controller('TransactionSalaryController', TransactionSalaryController);

  function TransactionSalaryController($scope, $state, $window, $modal, Transaction, Restangular, flash, $http) {
    var vm = this;
    vm.total_amount = 0;
    vm.add_member = {}; 
    vm.add_member_toggle = 0;
    angular.extend(vm, {
      isLoading: false,
      data: { affiliation_id: '', affiliation: {}, members: [] },
      custom: {},
      isCategoryLoading: true,
      isMembersLoading: false
    });

    // Watch
    $scope.$watch(
      function() {
        return vm.data.category_id;
      },
      function(changed, old) {
        if ( changed == old ) return;

        // Map and find the category
        // of the given id
        var id = vm.categories
          .map(function(c) { return parseInt(c.id, 10); })
          .indexOf(parseInt(changed, 10));

        vm._selected = vm.categories[id];
      }
    );

    $scope.$watch(function() {
      return vm.data.affiliation_id
    }, function(changed, old) {
      if ( changed === old ) return;

      vm.isMembersLoading = true;

      $http.get('/api/v1/affiliations/' + vm.data.affiliation_id + '/residents?limit=999999')
        .then(function(res) {
          console.log(res);
          vm.data.members = res.data.data
           
        })
        .finally(function() {
          vm.isMembersLoading = false;
        });
    });

    Restangular.one('transactions').one('categories?limit=1000&sort=name').get().then(function(response) {
      vm.categories = response.data.data;
      vm.isCategoryLoading = false;
    });

    Restangular.one('transactions').one('accounts?limit=1000').get().then(function(response) {
      vm.account_types = response.data.data;
    });

    Restangular.one('transactions').one('items').get().then(function(response) {
      vm.items = response.data.data;
    });

    // ------------------------- //
    // ------------------------- //

    /**
     * @description  Sends an XHR to the server with
     *               the form data for edit a new
     *               school
     * @param  {Object} formData [description]
     * @return void
     */
    vm.$salary = function(formData) {
      vm.isLoading = true;

      var ___data = !!formData.liquidatable
        ? formData
        : _.omit(formData, 'liquidatable');

      return $http.post('api/v1/transactions/salary', ___data)
        .then(function(r) {
          flash.success = 'Transaction salary was succcessfull!';
          $state.go('transactions', null, { reload: true });
          $scope.$close();
        })
      .catch(function(response) {
          flash.to('validation').error = _.chain(response.data.errors)
            .values()
            .flatten()
            .value();
      })
      .finally(function() {
        vm.isLoading = false;
      });
    }

    /**
     *
     */
    vm.$itemRequest = function($value) {
      return $http.get('/api/v1/transactions/items/?filters=' + JSON.stringify({ name: $value }))
        .then(function(response) {
          return response.data.data;
        });
    }

    vm.$addMemberToggle = function(){
      vm.add_member_toggle = !vm.add_member_toggle;
    }

    vm.$addMember = function(member) {
          var new_member = member;
          vm.data.members.push(new_member);
          vm.add_member = {};
          vm.computeTotal();
    }

    vm.$removeMember = function(member) {

      console.log(member);
      var index = vm.data.members.indexOf(member);
       vm.data.members.splice(index, 1);
       vm.computeTotal();

    }

    vm.computeTotal = function(){
      var i = 0;
      vm.total_amount = 0.0;
      var total = 0.0;
      for (i = 0; i < vm.data.members.length; i++) {
        var amt = vm.data.members[i].amount;
        var amount = parseFloat(( (typeof(amt) != 'undefined'  && amt ? amt : 0.0)) );
        
        total =  total +  amount;
      }

      if(typeof(vm.add_member.member) != 'undefined' ) {

        var amt2 = vm.add_member.member.amount;
        total = total + parseFloat( (typeof(amt2) != 'undefined'  && amt2 ? amt2 : 0.0) );
      }

      vm.total_amount = total;
    }

    vm.isFloat = function(n){
        return   n===Number(n)  && n%1!==0;
    }
  }
})(angular, _);
