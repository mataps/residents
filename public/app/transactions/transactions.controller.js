+(function(angular, _, undefined) {
  function TransactionsCtrl($scope, Restangular, $controller, $state, USER, UtilService, Transaction, TRANSACTIONS_COLUMNS, $modal, $window, flash, localStorageService) {
    // Inherit BaseIndexCtrl's prototype,
    var controller = $controller('BaseIndexCtrl', {
        $scope: $scope,
        $state: $state,
        UtilService: UtilService
    });


    angular.extend(this, controller);
// viewmodel
    var vm = this;
    vm.account_types = [];
    vm.unliquidatedCount = 0;


    var _perms = (function() {
      return USER.permissions
        .filter(function(permission) {
          return permission.component.toLowerCase() == 'transaction system' && permission.status == 1;
        })
        .map(function(permission) {
          return permission.functionality.toLowerCase().replace(/(view|manage|transactions)\s/g, '').trim();
        });
    })();
    console.log('here');
    console.log(_perms);

    Restangular.one('alerts')
      .one('liquidatables')
      .get()
      .then(function(r) {
        vm.unliquidatedCount = r.data.count;
      });
    // ------------------------- //
    // ------------------------- //
    Restangular.one('transactions').one('accounts').get().then(function(response){
      console.log('here testing');
      console.log(response);
      vm.account_types = response.data.data;
      $scope.account_types = vm.account_types;

      console.log(_perms);
      vm.account_types.map(function(type) {
        if ( _perms.indexOf(type.name.toLowerCase())) {

          vm.filtered_account_types.push(type);
        }

        // vm.filtered_account_types = vm.filtered_account_types
        //   .map(function(account) { return account.name })
        //   .sort()
        //   .map(function(name) {
        //     var id = vm.filtered_account_types
        //       .filter(function(account) { return account.name == name })[0]
        //       .id;

        //     return {
        //       id: id,
        //       name: name
        //     };
        //   });

        var __SELECTED = localStorageService.get('TRANSACTION_ACCOUNT_FILTER');
        if ( __SELECTED !== undefined  ) {
          vm.selected_account = __SELECTED;
        }

        var __SELECTED = localStorageService.get('TRANSACTION_ACCOUNT_DATE_FILTER');
        console.log(__SELECTED);
        if ( __SELECTED !== undefined  ) {
          vm.selected_date = __SELECTED;
        }
      });

      // if ( !!vm.filtered_account_types.length ) {
      //   vm.selected_account = vm.filtered_account_types[0].id;
      // }
    });

    vm.filtered_account_types = [];


    // Data collection (grabbed from the service)
    var collection = [];

    // Copy items from the service
    // to the local collection variable
    angular.extend(collection, Transaction.collection);

    // ------------------------- //
    // ------------------------- //

    // Initialize vm data
    vm.columns = TRANSACTIONS_COLUMNS;
    vm.collection = collection;

    // Manually bind the "this"
    // from the base controller
    vm.callServer = angular.bind(vm, vm.callServer);
    vm.applyPageData = angular.bind(vm, vm.applyPageData);
    vm.catchPageDataError = angular.bind(vm, vm.catchPageDataError);
    vm.show = show;
    // Commands that contact the API / server
    vm.remove = remove;
    vm.copy = copy;
    vm.salary = salary;
    vm.getPageData = getPageData;
    vm.selected_account;
    vm.account_temp;
    vm.account_date_temp
    vm.onAccountQueryChange = onAccountQueryChange;
    vm.onDateQueryChange = onDateQueryChange;

    $scope.report = { datepickers: {} };

    $scope.$$report = function() {
      $modal.open({
        templateUrl: 'app/transactions/reports/reports.html',
        controller: 'TransactionReportController',
        resolve: {
          columnsResolve: function() {
            return vm.columns;
          }
        }
      });
    }

    $scope.$$reportCash = function() {
      $modal.open({ templateUrl: 'app/transactions/reports.cash/reports.html', controller: 'TransactionCashFlowReportController' })
        .result
        .then(function() { $scope.report = { datepickers: {} } });
    }

    var filter = {};
    /**
     * @description  Sends a DELETE to the server
     * @see  getPageData
     * @return void
     */

     function show(id)
     {
        $state.go('transactions.details', {transaction_id:id});
     }

    function remove() {
      var selected = vm.getSelected().join(',');

      if ( !selected.length ) {
        flash.error = 'Please select a transaction.';
        return;
      }

      if(!confirm('Please confirm deleting the selected records.'))
        return;

      vm.load();

      return Transaction
        .Command
        .delete(selected)
        .then(successXHRCallback)
        .catch(errorXHRCallback);
    }

    function copy() {
      var selected = vm.getSelected();

      if ( !selected.length ) {
        flash.error = 'Please select a transaction.';
        return;
      } else if ( selected.length > 1 ) {
        flash.error = 'Please select only 1 transaction to proceed copying.';
        return;
      }

      flash.info = 'Opening copy transaction window.. data is loading..';

      $modal.open({
        templateUrl: '/app/transactions/copy/copy.html',
        controller: 'TransactionCopyController as copyCtrl',
        resolve: {
          transactionResolve: function() {
            return Transaction.Query
            .get(parseInt(selected[0], 10).toString())
            .then(function(r) {
              return r.data;
            });
          }
        }
      }).result.catch(function() {
        $state.reload();
      });
    }

    function salary() {
      flash.info = 'Opening salary window..';

      $modal.open({
        templateUrl: '/app/transactions/salary/salary.html',
        controller: 'TransactionSalaryController as salaryCtrl'
      }).result.catch(function() {
        $state.reload();
      });
    }


    // Success XHR
    function successXHRCallback(response) {
      flash.success = 'Subject has been successfully archived!';
      $state.reload();
      vm.load(false);
    }

    // Error XHR
    function errorXHRCallback(response) {
      flash.error = 'An error has occured. Please try again...';
      vm.load(false);
    }

    /**
     * Fetches data and replaces the current
     * @param  string query
     * @return void
     */
    function getPageData(query) {
      if ( query.page ) {
        query.filters = angular.copy( filter.filters );
      }

      if ( ! query.filters ) {
        query.filters = angular.copy(query);
      }

      if ( localStorageService.get('TRANSACTION_ACCOUNT_FILTER') !== undefined &&
        localStorageService.get('TRANSACTION_ACCOUNT_FILTER') !== null ) {
        query.filters.account_type_id = localStorageService.get('TRANSACTION_ACCOUNT_FILTER');
      }

      var date  = localStorageService.get('TRANSACTION_ACCOUNT_DATE_FILTER');
      if ( date !== undefined && date !== null ) {
        query.filters.created_at_persist = date;
      }

      if ( query.filters.created_at_persist == null || query.filters.created_at_persist == '' ) {
        query.filters = _.omit(query.filters, 'created_at_persist');
      }

      // if ( query.filters && ! query.filters['account_type_id'] && query.filters['account_type_id'] !== null && query.filters['account_type_id'] !== undefined ) {
      //   query.filters['account_type_id'] = filter['account_type_id'];
      // }
      //
      // if ( query.filters && ! query.filters['created_at'] && query.filters['created_at'] !== undefined && query.filters['created_at'] !== null ) {
      //   query.filters['created_at'] = filter['created_at'];
      // }

      filter.filters = angular.copy(angular.extend({}, query.filters));

      if ( query.account_type_id ) {
        query = _.omit(query, 'account_type_id');
      }

      if ( query.created_at_persist ) {
        query = _.omit(query, 'created_at_persist');
      }

      if ( query.filters.page ) {
        query.filters = _.omit(query.filters, 'page');
      }

      if ( query.filters.limit ) {
        query.filters = _.omit(query.filters, 'limit');
      }

      if ( query.filters.sort ) {
        query.filters = _.omit(query.filters, 'sort');
      }

      console.log(query);

      return Transaction
        .Query
        .all(query)
        .then(vm.applyPageData)
        .catch(vm.catchPageDataError);
    }

    function onAccountQueryChange() {
      filter['account_type_id'] = vm.selected_account;
      vm.account_temp = vm.selected_account;
      localStorageService.set('TRANSACTION_ACCOUNT_FILTER', vm.selected_account );

      if ( filter.filters ) {
        filter.filters['account_type_id'] = vm.selected_account;
      }

      // if ( filter['account_type_id'] == '' || filter['account_type_id'] === undefined ) {
      //   filter['account_type_id'] = _.omit(filter, 'account_type_id');

        // if ( filter.filters ) {
        //   filter.filters['account_type_id'] = _.omit(filter.filters, 'account_type_id');
        // }
      // }

      getPageData(filter);
    }

    function onDateQueryChange() {
      filter['created_at_persist'] = vm.selected_date;
      vm.account_date_temp = vm.selected_date;
      localStorageService.set('TRANSACTION_ACCOUNT_DATE_FILTER', vm.selected_date == null ? '' : vm.selected_date );

      if ( filter.filters ) {
        filter.filters['created_at_persist'] = vm.selected_date == null ? '' : vm.selected_date;
      }

      // if ( filter['created_at'] == '' || filter['created_at'] == undefined ) {
        // filter['created_at'] = _.omit(filter, 'created_at');

        // if ( filter.filters ) {
        //   filter.filters['created_at'] = _.omit(filter.filters, 'created_at');
        // }
      // }

      getPageData(filter);
    }

    // var dF = {};
    // function dateChange(key, from) {
    //   var field = from ? 'from' : 'to';
    //   dF.filters = {};
    //   dF.filters[key] = {};
    //   dF.filters[key][field] = vm['settled_at_' + field];
    //
    //   getPageData(dF);
    //
    // }
  }

  angular
    .module('app')
    .controller('TransactionsCtrl', TransactionsCtrl);
})(angular, _);
