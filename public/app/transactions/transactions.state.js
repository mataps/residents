+(function(angular, undefined) {
  angular
    .module('app')
    .config(config);

  function config($stateProvider) {
    var data = { pageTitle: 'Manage Transactions' };
    var resolves = { getTransactionCollectionResolve: getTransactionCollectionResolve };
    
    var state = {
      parent: 'main',
      name: 'transactions',
      url: '/transactions',
      data: data,
      resolve: resolves,
      controllerAs: 'transactions',
      controller: 'TransactionsCtrl',
      templateUrl: '/app/transactions/transactions.html'
    };
    
    $stateProvider.state(state);
  }

  function getTransactionCollectionResolve(Transaction) {
    return Transaction
      .Repository
      .all();
  };
})(angular);