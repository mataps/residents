// +(function(angular, undefined) {
//   'use strict';
//   angular
//     .module('app')
//     .directive('gcAffiliationTypeahead', affiliationTypeahead);

//   function affiliationTypeahead() {
//     return {
//       scope: {
//         /**
//          * Model / variable (ID) to assign
//          * the id of the result
//          * < .. ng-model="data.affiliation.id">
//          * < .. ng-model="data.affiliation_id">
//          */
//         model: '=ngModel',

//         /**
//          * For use, e.g. <.. blacklisted="$item
//          */
//         init: '=',

//         /**
//          * Blacklisted IDs (array)
//          * <.. blacklisted="[1, 2, 3]"
//          */
//         blacklisted: '='
//       },

//       // replace: true,
//       restrict: 'EA',
//       template: templateFn,
//       controller: controllerFn
//     };

//     /**
//      * Directiv template
//      */
//     function templateFn(e, attrs) {
//       var placeholder = attrs.placeholder || 'Enter attribute name..';

//       return [
//         '<input type="text" ',
//           'ng-model="temp.name" ',
//           'class="form-control" ',
//           'placeholder="', placeholder,'" ',
//           'typeahead-on-select="$affiliationOnSelect($item)" ',
//           'typeahead="attribute.name for attribute in $affiliations()">',
//       ].join('');
//     }

//     *
//      * Controllre
     
//     function controllerFn($scope, Restangular, flash) {
//       angular.extend($scope, {
//         isLoading: false,
//         temp: {},
//         $affiliations: $affiliations,
//         $affiliationOnSelect: $affiliationOnSelect
//       });

//       (function init() {
//         $scope.init && ($scope.temp.name = $scope.init);
//       })();

//       /**
//        * List of affiliations.
//        * Request the API for the list of affiliations
//        */
//       function $affiliations () {
//         $scope.isLoading = true;

//         return Restangular
//           .one('affiliations')
//           .getList()
//           .then(function(r) { return r.data })
//           .finally(function() { $scope.isLoading = false });
//       };

//       /**
//        * Assigns the selected affiliation from the typeahead
//        * to the model. Also validates it
//        */
//       function $affiliationOnSelect($item) {
//         // If the blacklist exists, yolo.
//         if ( !angular.isUndefined($scope.blacklisted) &&
//           $scope.blacklisted.indexOf($item.id) == -1
//         ) {
//           $scope.temp = {};
//           flash.error = 'The affiliation already exists in your list';
//           return;
//         }

//         $scope.model = $item.id;
//       }
//     }
//   }
// })(angular);