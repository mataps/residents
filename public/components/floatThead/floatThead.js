+(function (angular) {

  angular.module('app')
    .directive('floatThead', function($timeout) {
      return {
        restrict: 'A',
        link: function(scope, elem, attr) {
          var initialize = function() {
            elem.floatThead({
              scrollContainer: function($table){
                return $table.parent();
              },
              zIndex: 100
            });
            //elem.floatThead({
            //  useAbsolutePositioning: true
            //})
          };

          //$timeout(initialize, 0);
        }
      }
    })

})(angular);