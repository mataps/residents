+(function(){

    angular.module('app').directive('ssBindTemplate', function($compile) {
        return function(scope, elem, attrs) {
            var ensureCompileRunsOnce = scope.$watch(
                function(scope) {
                    // watch the 'compile' expression for changes
                    return scope.$eval(attrs.ssBindTemplate);
                },
                function(value) {
                    // when the 'compile' expression changes
                    // assign it into the current DOM
                    elem.html(value);

                    // compile the new DOM and link it to the current
                    // scope.
                    // NOTE: we only compile .childNodes so that
                    // we don't get into infinite loop compiling ourselves
                    $compile(elem.contents())(scope);

                    // Use un-watch feature to ensure compilation happens only once.
                    ensureCompileRunsOnce();
                }
            );

        };
    });

}(angular));
