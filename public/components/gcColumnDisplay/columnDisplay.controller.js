+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .controller('GCColumnDisplayController', GCColumnDisplayController);

  function GCColumnDisplayController($scope, localStorageService) {
    var vm = this;

    // Use the passed columns, or use the value of the key
    // stored in the localStorage
    var columns = localStorageService.get(vm.key) ||
      localStorageService.set(vm.key, vm.columns);

    // localStorageService.set @returns true
    // and localStorageService.get obviously returns
    // an object or whatever the key contains.
    // 
    // To avoid FOUC or bugs, we should just check
    // if the value of the key in the localStorage
    // is an object
    if ( angular.isObject(columns) ) {
      vm.columns = columns;
    }

    // ------------------------- //

    vm.set = set;

    /**
     * @description Overrides / sets a key in the localSTorage
     */
    function set() {
      localStorageService.set(vm.key, vm.columns);
    }
  }
})(angular);