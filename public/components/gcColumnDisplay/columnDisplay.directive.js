+(function(angular, _, undefined) {
  function columnDisplayDropdown() {
    var scope, template;
 
    // onColumnChange - a callback function
    // when the columns change
    // columns - column data
    scope = {
      key: '@',
      columns: '='
    };
 
    // Directive template
    template = [
      '<div class="btn-group">',
        '<a href="" ',
        'class="btn btn-default btn-sm dropdown-toggle" ',
        'data-toggle="dropdown">',
          'Columns <span class="caret"></span>',
        '</a>',
 
        '<ul class="dropdown-menu dropdown-menu-right" ',
        'role="menu" ng-click="$event.stopPropagation()">',
          '<li ng-repeat="column in cddCtrl.columns">',
            '<label class="dropdown-checkbox">',
              '<input id="column.{{column.key}}" type="checkbox" ',
              'ng-model="column.show" ng-change="cddCtrl.set()"/>',
              '{{ column.label }}',
            '</label>',
          '</li>',
        '</ul>',
 
      '</div>'
    ].join('');
 
    return {
      scope: scope,
      replace: true,
      restrict: 'EA',
      template: template,
      controllerAs: 'cddCtrl',
      controller: 'GCColumnDisplayController',
      bindToController: true
    }; 
  }
 
  angular
    .module('app')
    .directive('gcColumnDisplayDropdown', columnDisplayDropdown);
})(angular, _);