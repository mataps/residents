+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .run(runBlock);

  function runBlock($templateCache) {
    var template = [
      '<a>',
        '<div>',
          '<span bind-html-unsafe="match.model.name | typeaheadHighlight:query"></span>',
          ' <span>({{ match.model.abbreviation }})</span>',
        '</div>',
      '</a>'
    ].join('');

    $templateCache.put(
      'gc-course-typeahead-$template.html',
      template
    );
  }
})(angular);
