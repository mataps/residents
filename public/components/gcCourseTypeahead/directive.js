+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .directive('gcCourseTypeahead', gcCourseTypeahead);

  function gcCourseTypeahead($compile) {
    var scope = {
      model: '=ngModel',
      idVariable: '=',
      collectionVariable: '=',
      transformIdVariable: '@',
      transformCollectionVariable: '=',
      placeholder: '@inputPlaceholder',
      transformItem: '=',
      preCallback: '=',
      postCallback: '='
    };

    return {
      scope: scope,
      // templateUrl: 'gc-resident-typeahead-template.html',
      template: templateFn,
      bindToController: true,
      controllerAs: 'typeahead',
      controller: controllerFn,
      replace: true
    };

    function templateFn(element, attributes) {
      var placeholder = attributes.placeholder || 'Enter course name..';

      return [
        '<div>',
          '<input type="text" class="form-control" ',
          'placeholder="', placeholder, '" ',
          'ng-model="typeahead.model" ',
          'typeahead-wait-ms="500" ',
          'typeahead="course.name for course ',
            'in typeahead.request($viewValue)" ',
          'typeahead-wait-ms="50" ',
          'typeahead-template-url="gc-course-typeahead-$template.html" ',
          'typeahead-on-select="typeahead.selectCb($item, $model, $label)">',
        '</div>'
      ].join('');
    }

    function controllerFn($scope, $http) {
      //
      var vm = this;
      var transformItem = vm.transformItem || angular.noop;
      var preCallback = vm.preCallback || angular.noop;
      var postCallback = vm.postCallback || angular.noop;

      transform();

      vm.request = request;
      vm.selectCb = selectCb;

      /**
       * [resident, description]
       * @return {[type]} [description]
       */
      function request(name) {
        return $http.get('api/v1/courses?filters=' + JSON.stringify({ name: name }))
          .then(function(response) {
            return response.data.data;
          });
      }

      function selectCb($item, $model, $label) {
        // Transform
        transformItem($item);
        // Pre phase
        preCallback($item);

        vm.idVariable = $item.id;

        if ( !angular.isUndefined(vm.collectionVariable) ) {
          vm.collectionVariable = $item;
        }

        // Post phase
        postCallback($item);
      }

      /** Transform data */
      function transform() {
        if ( !angular.isUndefined(vm.transformIdVariable) ) {
          vm.idVariable = vm.transformIdVariable;
        }

        if ( !angular.isUndefined(vm.transformCollectionVariable) ) {
          vm.collectionVariable = angular.copy(vm.transformCollectionVariable);
        }
      }
    }
  }
})(angular);
