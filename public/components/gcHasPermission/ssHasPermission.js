+(function(angular){

    angular.module('app')
        .directive('ssHasPermission', function(USER) {

            return {
                restrict: 'A',
                link: function(scope, elem, attrs) {
                    var permissionName = attrs.ssHasPermission;
                    // console.log(USER.permissions);
                    var hasPermission = function() {
                        var components = USER.permissions.map(function(permission, index) {
                          var component = permission
                            .component
                            .toLowerCase()
                            .replace(' ', '_')

                          var name = permission
                            .functionality
                            .toLowerCase()
                            .replace(' ', '_');

                          var newPermission = angular.copy(permission);
                          newPermission.component = component + '.' + name;
                          return newPermission;
                        });
                        var permission = _.find(components, {component: permissionName});
                        return permission && !!parseInt(permission.status, 10);
                    };

                    if (!hasPermission())
                    {
                        elem.hide();
                    }
                }
            }
        });

})(angular);