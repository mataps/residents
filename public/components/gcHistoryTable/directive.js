+(function(angular, undefined) {
  "use strict";
  angular
    .module('app')
    .directive('gcHistoryTable', gcHistoryTable);

  function gcHistoryTable() {
    return {
      scope: {
        'api': '@gcHistoryTable'
      },

      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    }

    /**
     * Controller
     */
    function controllerFn($scope, $http) {
      // We really don't want to track down changes
      var api = $scope.api;

      // Initialize data
      angular.extend($scope, {
        isLoading: true,
        // We can just write `$scope.data.paging.next == undefined`,
        // but that's tedious compared to `$scope.isDisabled`.
        // Let's just set the `isDisabled` flag accordingly :)
        isDisabled: true,
        isSuccessful: true,
        $request: $request,
        data: []
      });

      // Run request, init
      (function init() {
        $scope.$request();
      })();

      /**
       * Request data from an API
       */
      function $request() {
        // Let's raise our `loading` flag / state
        $scope.isLoading = true;

        // We'll check if the paging propety already exists, and use it.
        // Otherwise, we haven't loaded any data yet, and probably at page 1.
        var paging = $scope.data.paging;
        var page = angular.isUndefined(paging) ? 1 : paging.next;
        var url =
          'api/v1/history/' +
          '?limit=30&page=' + page +
          '&filters={resource_type:' + api + '}';

        return $http.get(url)
          .then(function(response) {
            if ( angular.isUndefined(response.data.paging.previous) ) {
              $scope.data = response.data.data;
            } else {
              response.data.data.forEach(function(data) {
                $scope.data.push(data);
              });
            }

            $scope.data.paging = response.data.paging;
            $scope.isDisabled = angular.isUndefined(response.data.paging) ||
              angular.isUndefined(response.data.paging.next);
          })
          .catch(function() { $scope.isSuccessful = false; })
          .finally(function() { $scope.isLoading = false });
      }
    }

    /**
     * Template
     */
    function templateFn() {
      return [
        '<div srph-infinite-scroll="$request()" disabled="isDisabled || !isSuccessful" container="true" threshold="0">',
          '<div ng-if="isSuccessful && !!data.length">',
            '<table class="table" st-table="data" st-pipe="$request">',
              '<thead>',
                '<tr>',
                  '<th> Action </th>',
                  '<th> Updater </th>',
                  '<th> Date </th>',
                '</tr>',
                '<tr>',
                  '<th> <input type="text" st-search="action" st-delay="250" class="form-control"> </th>',
                  '<th> <input type="text" st-search="user.username" st-delay="250" class="form-control"> </th>',
                  '<th> <gc-search-date-range predicate="updated_at"> </gc-search-date-range> </th>',
                '</tr>',
              '</thead>',
              '<tbody>',
                '<tr ng-repeat="history in data">',
                  '<td>{{ history.action | capitalize:true }}</td>',
                  '<td>{{ history.user.username }} {{ history.resident.full_name }}</td>',
                  '<td>{{ history.updated_at | timestamp | date: \'MMMM d, yyyy\' }}</td>',
                '</tr>',
              '</tbody>',
            '</table>',
          '</div>',
          '<div class="text-center">',
            '<div ng-if="!isLoading && isSuccessful && !data.length">No history to display yet.</div>',
            '<div ng-if="isSuccessful && isLoading"> Loading.. </div>',
            '<div ng-if="isSuccessful && !isLoading && !!data.count"> Load more.. </div>',
            '<div ng-if="!isSuccessful"> Oops, an error had occured. Please refresh the page. </div>',
          '</div>',
        '</div>'
      ].join('');
    }
  }
})(angular);