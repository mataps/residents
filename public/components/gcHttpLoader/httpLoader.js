+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .directive('httpLoader', httpLoader);

  function httpLoader() {
    return {
      restrict: 'EA',
      scope: { loader: '=httpLoader' },
      template: '',
      link: linkFn
    };
  }

  function linkFn(scope, element, attrs) {
    // Callback function applied
    // when changes with the 'loader'
    // variable are watched.
    function watchLoader(isLoading) {
      // Adds or remove class and disabled attribute
      // depending on the current value of the
      // 'loader'
      if (isLoading) {
        element.addClass('loading');
        element.attr('disabled', true);
      } else {
        element.removeClass('loading');
        element.removeAttr('disabled');
      }
    }

    scope.$watch('loader', watchLoader);
  }
})(angular);