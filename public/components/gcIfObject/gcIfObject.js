+(function(angular) {
  'use strict';

  angular
    .module('app')
    .directive('gcIfObject', directive);

  function directive() {
    return {
      restrict: 'EA',
      scope: { obj: '=gcIfObject' },
      replace: true,
      transclude: true,
      template: '<div> <div ng-if="gcIfCtrl.$show()" ng-transclude></div> </div>',
      bindToController: true,
      controllerAs: 'gcIfCtrl',
      controller: controllerFn
    }

    function controllerFn($scope) {
      var vm = this;

      angular.extend(vm, {
        $show: $show
      });

      /**
       * [$show description]
       * @return {[type]} [description]
       */
      function $show() {
        return !angular.isUndefined(vm.obj) || (
          angular.isObject(vm.obj) &&
          Object.keys(vm.obj).length
        );
      }
    }
  }
})(angular);