+(function(angular, undefined){
  'use strict';
    angular.module('app').directive('ssLoader', function($parse) {
        return {
            scope: {
                isLoading: '=ssLoader'
            },
            link: function (scope, elem, attrs, ctrl) {
                elem.addClass('loader');
                var css = {
                    position: 'fixed',
                    display: 'none',
                    height: '128px',
                    fontWeight: 'bold'
                };

                scope.$watch('isLoading', function(newValue) {
                    var target = angular.element(attrs.ssLoaderTarget);

                    if (newValue) {
                        css.height = target.height();
                        css.width = target.width();
                        css.top = target.offset().top;
                        css.paddingTop = css.height / 3;
                        css.left = target.offset().left;
                        css.display = 'block';
                        target.css('opacity', .2);
                        elem.css(css);
                    } else {
                        css.display = 'none';
                        target.css('opacity', 1);
                        elem.css(css);
                    }
                });
            }
        };
    });

})(angular);
