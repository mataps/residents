+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .directive('gcLogout', Logout);

  function Logout($window) {
    return {
      restrict: 'A',
      link: function(scope, element, attributes) {
        if ( element.prop('tagName').toLowerCase() !== 'a' ) {
          throw new Exception('Tard, use gcLogout with an anchor(<a>)');
        }

        element.on('click', function(evt) {
          evt.preventDefault();
          evt.stopPropagation();

          // Send to our /logout
          // as registered in our Laravel routes
          $window.location.pathname = '/logout';
        });
      }
    }
  }
})(angular);