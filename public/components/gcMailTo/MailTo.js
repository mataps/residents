+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcMailTo', directive);

  function directive() {
    return {
      restrict: 'EA',
      scope: { mail: '@gcMailTo' },
      replace: true,
      bindToController: true,
      controllerAs: 'mailCtrl',
      controller: function($scope, flash) {
        var vm = this;

        vm.flash = function() {
          flash.success = 'Initializing email app...';
        }
      },
      template: '<a href="mailto:{{mailCtrl.mail}}" ng-click="mailCtrl.flash()">{{mailCtrl.mail}}</a>'
    }
  }
})(angular);