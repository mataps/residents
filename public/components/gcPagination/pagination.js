+(function(angular, _, undefined) {
  // Usage:
  // <ss-pagination
  // paging="collection.paging"
  // custom-classes="paging pull-right btn-group btn-group-sm"
  // next-callback="callbackX()"
  // prev-callback="callbackX()">
  // </ss-pagination>
  function pagination() {
    // Data, collection of objects
    var scope = {
      paging: '=',
      nextCallback: '&',
      nextPrePhaseCallback: '&',
      nextPostPhaseCallback: '&',
      prevCallback: '&',
      prevPrePhaseCallback: '&',
      prevPostPhaseCallback: '&',
      customClasses: '@'
    };
 
    return {
      scope: scope,
      replace: true,
      restrict: 'EA',
      template: getTemplate
    };
 
    /**
     * @name getTemplate
     * @description Returns the template string
     * @param {Object} element
     * @param {Object} attrs
     * @return {String}
     */
    function getTemplate(element, attrs) {
      var template = [
      '<div class="{{ customClasses }}">',
        '<button type="button" class="btn btn-default" gc-pagination-step callback="prevCallback()" ',
          'ng-disabled="paging.previous === undefined">',
            '<i class="ion-chevron-left"></i>',
          '</button>',
            
          '<span class="paging-info">',
            '<strong>',
                '<span ng-bind="paging.start_count"></span>',
                  ' - ',
                '<span ng-bind="paging.end_count"></span>',
            '</strong>',
            ' of ',
            '<strong>',
              '<span ng-bind="paging.total"></span>',
            '</strong>',
          '</span>',
          '<button type="button" class="btn btn-default" gc-pagination-step callback="nextCallback()" ',
          'ng-disabled="paging.next === undefined">',
            '<i class="ion-chevron-right"></i>',
        '</button>',
      '</div>'
      ];
 
      return template.join('');
    }
  }
 
  angular
    .module('app')
    .directive('gcPagination', pagination);
})(angular);