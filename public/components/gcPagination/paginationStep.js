+(function(angular, _) {
  function paginationStep() {
    // callback is called when the data is being fetched
    // prePhaseCallback, called before the data is fetched
    // postPhaseCallback, called after the data is fetched
    var scope = {
      callback: '&',
      prePhaseCallback: '&',
      postPhaseCallback: '&'
    };
 
    return {
      scope: scope,
      restrict: 'EA',
      transclude: true,
      template: getTemplate,
      link: link
      // controller: 'PaginationCtrl'
    };
 
    /**
     * @name getTemplate
     * @description Simply generates the template string
     * @return {String}
     */
    function getTemplate() {
      return [
        '<div ng-transclude>',
        '</div>'
      ].join('');
    }
 
    function link(scope, element, attrs) {
      if ( _.isUndefined(scope.callback) ) {
        throw new Error('Callback to call server was not specified!');
      }
 
      element.on('click', movePage);
 
      /**
       * [goToNextPage description]
       * @return {[type]} [description]
       */
      function movePage() {
        // Call pre-phase callback
        if ( !_.isUndefined(scope.prePhaseCallback) ) {
          scope.prePhaseCallback();
        }
 
        getData();
      }
 
      function getData() {
        scope.callback();
 
        // Call post-phase callback
        if ( !_.isUndefined(scope.postPhaseCallback) ) {
          scope.postPhaseCallback();
        }
      }
    }
  }
 
  angular
    .module('app')
    .directive('gcPaginationStep', paginationStep);
})(angular, _);