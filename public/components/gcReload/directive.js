+function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcReload', gcReload);

  function gcReload() {
    return {
      scope: true,
      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    //
    function templateFn() {
      return [
        '<button type="button" ng-click="reload()" class="btn btn-default">',
          '<i class="ion-ios-refresh"></i>',
        '</button>'
      ].join('')
    }

    //
    function controllerFn($scope, $state, flash) {
      $scope.reload = reload;

      function reload() {
        $state.reload();
        flash.success = 'Filters will reset in a sec...';
      }
    }
  }
}(angular);