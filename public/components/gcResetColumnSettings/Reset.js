+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcResetTableSettings', directive);

  function directive() {
    return {
      restrict: 'EA',
      scope: {
        columns: '=',
        defaults: '@',
        key: '@gcResetTableSettings',
      },
      link: linkFn,
      bindToController: true,
      controllerAs: 'resetCtrl',
      controller: controllerFn
    }

    /**
     * Directive controller
     */
    function controllerFn($scope, $injector, localStorageService, flash) {
      var vm = this;

      // Since angular internally converts
      // @ scopes to lowercase (_ to .)
      var defaults = $injector.get(
        vm.defaults.toUpperCase().replace(/\./i, '_')
      );

      angular.extend(vm, {
        reset: reset
      });

      /**
       * Performs reset
       * @param  {string} key
       */
      function reset(key) {
        flash.success = 'Settings for columns has been reset!';
        localStorageService.set(key, defaults);
        vm.columns = angular.copy(defaults);
      }
    }

    /**
     * Add listener
     */
    function linkFn(scope, element, attributes, controller) {
      element.on('click', function(evt) {
        evt.preventDefault();
        evt.stopPropagation();

        scope.$apply(controller.reset(controller.key));
      });
    }
  }
})(angular);