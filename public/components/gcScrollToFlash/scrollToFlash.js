angular.module('app').directive('flashAlert', function(flash) {
    return {
        restrict: 'A',
        require: ['?flashAlert'],
        link: function(scope, element, attr, controllers) {
            var scrollTop = function() {
                $('body').animate({scrollTop: 0 }, 'fast');
            };

            flash.subscribe(scrollTop, attr.flashAlert, attr.id);
        }
    };
});