+(function(angular, undefined) {
  angular
    .module('app')
    .directive('gcSearchDateRange', gcSearchDateRange);

  function gcSearchDateRange($filter) {
    return {
      restrict: 'E',
      require: '^stTable',
      scope: { predicate: '=' },
      link: linkFn,
      template: templateFn
    };

    function linkFn(scope, element, attributes, table) {
      var input = element.find('input');
      var predicate = scope.predicate;
      var data = { startDate: null, endDate: null };
      
      scope.data = data;

      scope.$watch('data', function(changed, old) {
        var query = {};

        if( changed.startDate == null && changed.endDate == null && old.startDate == null && old.endDate == null ){
          return;
        }

        if ( !scope.isBeforeOpen && !scope.isAfterOpen ) {
          if ( changed.startDate) {
            query.from = $filter('date')(changed.startDate, 'd-M-yyyy');
          }

          if ( changed.endDate ) {
            query.to = $filter('date')(changed.endDate, 'd-M-yyyy');
          }

          table.search(query, predicate);
        }

      });
    }

    function templateFn() {
      return [
        '<div style="width: 175px;">',
          // '<input type="text" placeholder="From.." datepicker-popup="MM-dd-yyyy" class="form-control" ng-model="before" is-open="isBeforeOpen" ng-click="openBefore($event)" id="qwe" style="height: 26px; width: 85px; display: inline-block">',
          // '<input type="text" placeholder="To.." datepicker-popup="MM-dd-yyyy" class="form-control" ng-model="after" is-open="isAfterOpen" ng-click="openAfter($event)" id="asd" style="height: 26px; width: 85px; display: inline-block">',
          '<input date-range-picker class="form-control date-picker" type="text" style="height: 26px;" ng-model="data" class="form-control">',
        '</div>'
      ].join('');
    }
  }
})(angular);