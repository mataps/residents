+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcSearchDropdownEqual', directive);

  function directive($timeout) {
    return {
      require: '^stTable',
      scope: { predicate: '=?gcSearchDropdownEqual' },
      link: function (scope, element, attr, table) {
        var promise = null;
        var throttle = attr.stDelay || 400;
        var predicate = scope.predicate;

        element.bind('change', function (evt) {
          evt = evt.originalEvent || evt;

          if (promise !== null) {
            $timeout.cancel(promise);
          }

          promise = $timeout(function () {
            if ( evt.target.value == undefined || evt.target.value == '' ) {
              table.search({}, predicate);
            } else {
              table.search({ equal: evt.target.value }, predicate);
            }
            
            promise = null;
          }, throttle);
        });
      }
    };
  }
})(angular);
