+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcSearchEqual', directive);

  function directive ($timeout) {
    return {
      scope: { predicate: '=?gcSearchEqual' },
      require: '^stTable',
      link: linkFn
    };

    function linkFn(scope, element, attr, table) {
      var promise = null;
      var predicate = scope.predicate;
      var throttle = attr.stDelay || 400;

      // scope.$watch('predicate', function (newValue, oldValue) {
      //   if (newValue !== oldValue) {
      //     table.tableState().search = {};
      //     table.search(element[0].value, newValue);
      //   }
      // });

      // table state -> view
      // scope.$watch(function () {
      //   return table.tableState().search;
      // }, function (newValue, oldValue) {
      //   var predicateExpression = predicate;
        
      //   if (newValue.predicateObject && newValue.predicateObject[predicateExpression] !== element[0].value) {
      //     element[0].value = newValue.predicateObject[predicateExpression] || '';
      //   }
      // }, true);

      // view -> table state
      element.on('input', function (evt) {
        evt = evt.originalEvent || evt;
        
        if (promise !== null) {
          $timeout.cancel(promise);
        }
        
        promise = $timeout(function () {
          table.search({ equal: evt.target.value }, predicate);
          promise = null;
        }, throttle);
      });
    }
  }
})(angular);