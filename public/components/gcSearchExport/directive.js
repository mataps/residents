+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcSearchExport', gcSearchExport);

  /**
   * Exports the filter from stTable
   */
  function gcSearchExport($compile, $timeout) {
    return {
      restrict: 'EA',
      require: '^stTable',
      link: linkFn,
      scope: { filter: '=gcSearchExport' }
    };

    function linkFn(scope, element, attributes, table) {
      scope.filter = getTableQuery( table.tableState() );
      scope.$watch(
        function() { return table.tableState().search },
        function(changed, old) { console.log('yolo'); scope.filter = getTableQuery(); }
      );

      function getTableQuery() {
        var tableState = table.tableState();
        var query = {};

        if (tableState.sort.predicate) {
          query.sort = tableState.sort.reverse ? '' : '-';
          query.sort += tableState.sort.predicate;
        }

        if (tableState.search.predicateObject) {
          query.filters = {};
          angular.forEach(tableState.search.predicateObject, function(value, key) {
              this[key] = value;
          }, query.filters);
        }

        return query;
      }
    }
  }
})(angular);