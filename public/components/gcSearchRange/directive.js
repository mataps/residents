+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcSearchRange', gcSearchRange);

  function gcSearchRange ($timeout) {
    return {
      require: '^stTable',
      scope: {
        predicate: '=?gcSearchRange',
        key: '@'
      },
      link: function (scope, element, attr, ctrl) {
        var tableCtrl = ctrl;
        var promise = null;
        var throttle = attr.stDelay || 400;
        var search;
        var dates = {};

        position = parseInt(scope.position, 10);

        if ( position < 1 || position > 2 ) {
          throw new Error('Position must either be 1 or 2');
        }

        scope.$watch('predicate', function (newValue, oldValue) {
          if (newValue !== oldValue) {
            ctrl.tableState().search = {};
            tableCtrl.search(element[0].value || '', newValue);
          }
        });

        //table state -> view
        scope.$watch(function () {
          return ctrl.tableState().search;
        }, function (newValue, oldValue) {
          var predicateExpression = scope.predicate || '$';
          if (newValue.predicateObject && newValue.predicateObject[predicateExpression] !== element[0].value) {
            element[0].value = newValue.predicateObject[predicateExpression] || '';
          }
        }, true);

        // view -> table state
        element.bind('input', function (evt) {
          evt = evt.originalEvent || evt;
          if (promise !== null) {
            $timeout.cancel(promise);
          }
            
          promise = $timeout(function () {
            tableCtrl.search(evt.target.value, scope.predicate || '');
            promise = null;
          }, throttle);
        });
      }
    };
  }

})(angular);