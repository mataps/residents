+(function(angular, _, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcSearchYearRange', directive);

  function directive($timeout) {
    return {
      scope: {
        range: '@',
        predicate: '=?gcSearchYearRange'
      },

      restrict: 'EA',
      require: '^stTable',
      link: linkFn,
      template: templateFn,
      controller: controllerFn
    };

    function templateFn() {
      return [
        '<select class="form-control">',
          '<option value=""> All </option>',
          '<option ng-repeat="set in sets" value="{{set + \'-\' + (set + 1)}}">',
            '{{set + \'-\' + (set + 1)}}',
          '</option>',
        '</select>'
      ].join('');
    }

    function controllerFn($scope) {
      $scope.range || ($scope.range = 30);

      /**
      * Gets year in sets (1991-1992)
      * @param  {[type]} range [description]
      * @return {[type]}       [description]
      */
      $scope.sets = (function getYearSets(range) {
        range = parseInt(range, 10);

        var current = new Date().getFullYear();
        var start   = current - range;
        var end     = current + range;
     
        return _.range(start, end);
      })($scope.range);
    }

    function linkFn(scope, element, attributes, table) {
      var promise = null;
      var throttle = attributes.stDelay || 400;
      var predicate = scope.predicate;

      element.bind('change', function (evt) {
        evt = evt.originalEvent || evt;

        if (promise !== null) {
          $timeout.cancel(promise);
        }

        promise = $timeout(function () {
          table.search({ equal: evt.target.value }, predicate);
          promise = null;
        }, throttle);
      });
    }
  }
})(angular, _);