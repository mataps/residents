+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcSelectRow', directive);

  function directive($window, $document, $state, $timeout) {
    return {
      restrict: 'A',
      require: '^stTable',
      link: linkFn
    };

    function linkFn(scope, element, attributes, ctrl) {
      var win = angular.element($window);
      var rows = element.children('tr');
      var active;
      var parent = angular.element('#yolo-pls-swag');
      
      var CLASS = 'st-selected';
      var focus = false;

      /**
       * Handle clicks
       */
      rows.on('click', function (evt) {
        _removeActive();

        var row = evt.target;
        active = row.data('index');

        console.log('CLIIIIIICKED!!!!!!');
        console.log(active);
        
        rows[active].addClass(CLASS);
      });

      scope.$watch(
        function() {
          return $document[0].activeElement;
        },
        function(changed, old) {
          $timeout(function() {
            focus = changed !== undefined && (changed.tagName == 'INPUT' || changed.tagName == 'TEXTAREA' || changed.tagName == 'SELECT')
          }, 1);
        }
      );

      win.on('keydown', function(evt) {
        var KEY_UP = 38;
        var KEY_DOWN = 40;
        var key = evt.keyCode;
        var rows = element.children('tr');

        if ( !focus && (key == KEY_UP || key == KEY_DOWN) ) { 
          _removeActive(rows[active]);

          active = active == null
            ? 0
            : _adjustIndex( active - (key == KEY_UP ? 1 : -1), rows.length );

          var row = angular.element(rows[active]);
          row.addClass(CLASS);

          var top = active == 0
            ? 0
            : (parent.scrollTop() + row.offset().top) - (row.height() * 2) ;

          parent.scrollTop( top );

          var id = row.data('id');
          var es = row.data('es');
          var state = row.data('state');
          $state.go(state, { id: id, es_id: es });
        }
      });

      function _removeActive(el) {
        // We wrap this in a try-catch statement
        // to avoid errors in case active is still null (declared, unassigned)
        // such as rows[null] cannot have the removeClass property.
        try {
          // find elements with .st-selected instead
          // but I think that would be less efficient because we query the DOM
          // every fucking time
          // Fuck you, DOM. Thanks for being so fucking slow ./.
          angular.element(el).removeClass( CLASS );
        } catch(e) {}
      }

      /**
       * Utility method to adjust index exceeding number
       * only from 0 to max
       */
      function _adjustIndex(i, m) {
        return i < 0 ? 0 : (i >= m ? m - 1: i);
      }
    }
  }
})(angular);