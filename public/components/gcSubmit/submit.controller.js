+function(angular, undefined) {
  angular
    .module('app')
    .controller('GCSubmitController', SubmitController)

  function SubmitController($scope, gcSubmitFactory) {
    var vm = this;

    vm.request = request;
    // variables from the directive
    // vm.url, vm.type

    /** Sends an XHR to the server */
    function request(data) {
      var options = {
        id: vm.idVariable,
        url: vm.url,
        type: vm.type
      };

      gcSubmitFactory.request(options, data)
        .then(vm.successCb || angular.noop)
        .catch(vm.errorCb|| angular.noop);
    }

  }
}(angular);