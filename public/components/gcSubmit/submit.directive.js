+function(angular, undefined) {
  angular
    .module('app')
    .directive('gcSubmit', gcSubmit);

  /**
   * Usage
      <gc-submit=""
        request-type="post"
        request-data="thisCtrl.collections">
        <button...></button>
      </gc-submit>
   */
  function gcSubmit($compile) {
    var scope = {
      url: '@gcSubmit',
      type: '@requestType',
      data: '=requestData',
      successCb: '=requestSuccess',
      errorCb: '=requestError'
    };

    return {
      scope: scope,
      // replace: true,
      // template: template,
      controllerAs: 'gcSubmitCtrl',
      controller: 'GCSubmitController',
      bindToController: true,
      link: linkFn
    };

    function linkFn(scope, element, attributes, controller) {
      element.on('click', function() {
        controller.request(controller.data);
      });
    }
  }
}(angular);