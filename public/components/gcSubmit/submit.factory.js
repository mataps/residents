+function(angular, undefined) {
  angular
    .module('app')
    .factory('gcSubmitFactory', submitFactory);

  function submitFactory($http) {
    return {
      request: request
    };

    /** Sends an XHR to the server */
    function request(options, data) {
      // Base url to send the request to
      var BASE = 'api/v1/';

      var url = BASE + options.url,
          type = (options.type).toLowerCase();

      // A switch to determine which
      // request to use
      var requests = {
        get: function() {
          return $http.get(url);
        },
        put: function() {
          return $http.put(url, data);
        },
        post: function() {
          return $http.post(url, data);
        }
      };

      return (
        requests[type] ||
        requests['get']
      )();
    }
  }
}(angular);