+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .run(runBlock);

  function runBlock($templateCache) {
    var template = [
      '<a>',
        '<div>',
          '<span bind-html-unsafe="match.model.username | typeaheadHighlight:query"></span>',
        '</div>',
      '</a>'
    ].join('');

    $templateCache.put(
      'gc-user-typeahead-$template.html',
      template
    );
  }
})(angular);
