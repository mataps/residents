+(function(angular, _, undefined) {
  "use strict";
  angular
    .module('app')
    .controller('VendorCreateController', VendorCreateController);

  function VendorCreateController($scope, Restangular, flash, $http, callback) {
    angular.extend($scope, {
      data: {},
      isLoading: false,
      $create: $create,
      $barangays: $barangays,
      $districts: $districts,
      $citiesMunicipalities: $citiesMunicipalities
    });

    /**
     * Create stuff
     */
    function $create() {
      $scope.isLoading = true;
      var data = $scope.data;

      return $http.post('/api/v1/vendors', data)
        .then(function(r) {
          flash.success = 'Successfully created vendor';
          callback(r.data.data.name, r.data.data.id);
          $scope.$close();
        })
        .catch(function(r) {
          flash.to('validation').error = _.chain(r.data.errors)
            .values()
            .flatten()
            .value();
        })
        .finally(function() {
          $scope.isLoading = true;
        });
    }

    /**
     * Fetch districts
     */
    function $districts($viewValue) {
      return Restangular.service('districts')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }
    
    /**
     * Fetch cities
     */
    function $citiesMunicipalities($viewValue) {
      return Restangular.service('citiesmunicipalities')
        .getList({ filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }

    /**
     * Fetch barangays
     */
    function $barangays($viewValue) {
      return Restangular.service('barangays')
        .getList({ limit: 6969696969999999, filters: JSON.stringify({ name: $viewValue }) })
        .then(function(response) {
          return response.data;
        });
    }
  }
})(angular, _);