+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .run(runBlock);

  function runBlock($templateCache) {
    var template = [
      '<a>',
        '<div bind-html-unsafe="match.model.name | typeaheadHighlight:query"></div>',
        '<div style="font-size: 9px; font-style: italic;">',
          '{{ match.model.street }}, {{ match.model.barangay }}',
        '</div>',
        '<div style="font-size: 9px; font-style: italic;">',
          '{{ match.model.cityMunicipality }}',
        '</div>',
      '</a>'
    ].join('');

    $templateCache.put(
      'gc-vendor-typeahead-$template.html',
      template
    );
  }
})(angular);