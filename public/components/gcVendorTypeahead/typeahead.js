+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcVendorTypeahead', directive);

  function directive() {
    return {
      scope: {
        /**
         * Model / variable (ID) to assign
         * the id of the result
         * < .. ng-model="data.bank.id">
         * < .. ng-model="data.bank_id">
         */
        model: '=ngModel',
        idModel: '=idModel'
      },

      // replace: true,
      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };
    /**
     * Directiv template
     */
    function templateFn(e, attrs) {
      var placeholder = attrs.placeholder || 'Enter vendor name..';

      return [
        '<div>',
          '<input type="text" ',
            'ng-model="model" ',
            'class="form-control" ',
            'placeholder="', placeholder,'" ',
            'typeahead-on-select="idModel = $item.id" ',
            'typeahead-template-url="gc-vendor-typeahead-$template.html" ',
            'typeahead="vendor.name for vendor in $vendors($viewValue)">',
        '</div>',
        '<div>',
          '<i class="ion-ios-information"></i>&nbsp;',
          '<a href="#" ng-click="$create()">',
            'Click here to quick-create a vendor.',
          '</a>',
        '</div>'
      ].join('');
    }

    /**
     * Controllre
     */
    function controllerFn($scope, Restangular, flash, $modal) {
      angular.extend($scope, {
        isLoading: false,
        $vendors: $vendors,
        $create: $create
      });

      /**
       * List of vendors.
       * Request the API for the list of vendors
       */
      function $vendors($value) {
        $scope.isLoading = true;

        return Restangular
          .service('vendors')
          .getList({ filters: JSON.stringify({ name: $value }) })
          .then(function(r) { return r.data })
          .finally(function() { $scope.isLoading = false });
      }

      /**
       *
       */
      function $create() {
        $modal.open({
          templateUrl: '/components/gcVendorTypeahead/quick.html',
          controller: 'VendorCreateController',
          resolve: {
            callback: function() {
              return function($value, $id) {
                $scope.model = $value;
                $scope.idModel = $id;
              }
            }
          }
        });
      }
    }
  }
})(angular);