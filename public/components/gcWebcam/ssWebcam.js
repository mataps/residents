+(function(angular, Webcam) {

    var countDownInterval = undefined;
    var initialCount = 3;
    var counter = initialCount;
    var lastDataUri = undefined;


    angular.module('app').provider('ssWebcam', function() {

        var options = {
            width: undefined,
            height: undefined,
            dest_width: undefined,
            dest_height: undefined,
            crop_width: undefined,
            crop_height: undefined,
            image_format: 'jpeg',
            jpeg_quality: 90,
            force_flash: false,
            onSnap: function() {},
            onDone: function() {},
            webcamContainerSelector: '.webcam-preview',
            template: '<button type="button" class="close" ng-click="$close()"><span aria-hidden="true">&times;</span></button><ss-webcam-counter></ss-webcam-counter><ss-webcam-preview></ss-webcam-preview><div class="modal-footer"><button class="btn btn-primary" ng-click="$snap()" ng-bind="takeCaption"></button><button class="btn btn-success" ng-show="showDone" ng-click="$done()">Done</button></div>'
        };

        this.setOptions = function(opts){
            options = angular.extend({}, opts, options);
        };


        this.$get = function($q, $rootScope, $modal, $interval, $timeout) {
            var webcamInstance = {};

            function getTemplatePromise(options) {
                return options.template ? $q.when(options.template) :
                    $http.get(options.templateUrl, {cache: $templateCache}).then(function (result) {
                        return result.data;
                    });
            }

            function stopCountDown() {
                if (angular.isDefined(countDownInterval)) {
                    $interval.cancel(countDownInterval);
                    countDownInterval = undefined;
                }
            }

            function resetCounter() {
                counter = initialCount;
            }

            function resetLastImageData() {
                lastDataUri = undefined;
            }

            var open = function(webcamOptions) {
                //merge options
                options = webcamOptions = angular.extend({}, options, webcamOptions);
                //check options
                if (!webcamOptions.template && !webcamOptions.templateUrl) {
                    throw new Error('One of template or templateUrl options is required.');
                }
                var templatePromise = $q.all([getTemplatePromise(webcamOptions)]);

                templatePromise.then(function onLoadtemplate(template) {
                    Webcam.set(webcamOptions);
                    stopCountDown();

                    webcamInstance = $modal.open({
                        template: template,
                        windowClass: 'webcam-preview-modal',
                        controller: function($scope) {
                            $scope.counter = counter;
                            $scope.takeCaption = 'Take';
                            $scope.$done = done;
                            $scope.$snap = function() {
                                snap();
                            };

                            $scope.$watch(function() {
                                return counter;
                            }, function(value) {
                                if (value === initialCount) {
                                    return;
                                }

                                if (!value) {
                                    $timeout(function(){
                                        $scope.takeCaption = 'Retake';
                                        $scope.showDone = true;
                                    }, 1000);
                                }
                            });
                        }
                    });
                },  function resolveError(reason) {
                    throw new Error(reason);
                });
            };

            var close = function () {
                stopCountDown();
                webcamInstance.close();
            };

            var snap = function() {
                stopCountDown();
                resetLastImageData();
                var $rootScope = angular.injector(['ng']).get('$rootScope');
                countDownInterval = $interval(function() {
                    if (!counter) {
                        resetCounter();
                        stopCountDown();
                        Webcam.snap(function(dataUri){
                            $rootScope.$apply(function() {
                                lastDataUri = dataUri;
                                options.onSnap(dataUri);
                            });
                        });
                    } else {
                        $rootScope.$apply(function() {
                            counter--;
                        });
                    }
                }, 1000);
            };

            var done = function() {
                options.onDone(lastDataUri);
                close();
            };

            return {
                open: open,
                close: close,
                snap: snap,
                done: done
            };
        };
    }).directive('ssWebcamPreview', function($compile, $timeout) {
        return {
            restrict: 'E',
            link: function(scope, elem) {
                elem.addClass('webcam-preview');
                Webcam.attach(elem.get(0));

                var imgPreview = angular.element('<img alt="Webcam preview" ng-src="{{previewSrc}}"/>');
                elem.append(imgPreview);

                scope.$watch(function() {
                    return lastDataUri;
                }, function(newValue, oldValue) {
                    if (angular.isDefined(newValue) && newValue !== oldValue) {
                        elem.find('video').hide();
                        imgPreview.show();
                        scope.previewSrc = newValue;
                        $timeout(function() {
                            $compile(imgPreview)(scope);
                        });
                    } else {
                        elem.find('video').show();
                        imgPreview.hide();
                    }
                });
            }
        };
    }).directive('ssWebcamCounter', function($timeout) {
        return {
            restrict: 'E',
            template: '{{counter}}',
            link: function($scope, elem) {
                elem.addClass('webcam-counter');
                elem.hide();

                $scope.$watch(function() {
                    return counter;
                }, function(value) {
                    if (value === initialCount) {
                        return;
                    }

                    if (!value) {
                        $timeout(function() {
                            elem.hide();
                        }, 1000)
                    } else {
                        elem.show();
                    }
                    $scope.counter = ++value;
                });
            }
        };
    });
})(angular, Webcam);