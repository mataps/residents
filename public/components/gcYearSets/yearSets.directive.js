+(function(angular, _, undefined) {
  angular
    .module('app')
    .directive('gcYearSets', yearSets);

  function yearSets() {
    var template = [
      '<select ng-model="ySetCtrl.model" ng-init="ySetCtrl.init()" class="form-control" ng-disabled="ySetCtrl.disabled">',
        '<option ng-repeat="year in ySetCtrl.sets" value="{{ year + \'-\' + (year + 1) }}" ng-selected="(year + \'-\' + (year + 1)) == ySetCtrl.model">',
          '{{year}} - {{year + 1}}',
        '</option>',
      '</select>',
    ].join('');

    return {
      restrict: 'EA',
      scope: {
        range: '@' ,
        model: '=yearModel',
        disabled: '=disabled'
      },
      template: template,
      bindToController: true,
      controllerAs: 'ySetCtrl',
      controller: function($scope) {
        var vm = this;

        vm.range || (vm.range = 15);
        vm.init = init;
        vm.concatenate = concatenate;
        vm.currentYear = vm.concatenate(new Date().getFullYear());

        /**
         * Gets year in sets (1991-1992)
         * @param  {[type]} range [description]
         * @return {[type]}       [description]
         */
        vm.sets = (function getYearSets(range) {
          range = parseInt(range, 10);

          var current = new Date().getFullYear();
          var start   = current - range;
          var end     = current + range;
     
          return _.range(start, end);
        })(vm.range);

        function init() {
          if ( angular.isUndefined(vm.model) ) {
            vm.model = vm.currentYear;
          }
        }

        /**
         *
         */
        function concatenate(year) {
          return year + '-' + (year + 1);
        }
      }
    };
  }
})(angular, _);
