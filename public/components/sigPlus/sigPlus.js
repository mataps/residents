+(function (angular) {

  angular.module('app')
    .provider('sigPlus', function() {

      this.$get = function($modal) {
        var sigPlusInstance;
        var options = {
          onDone: function(hexadecimal) {}
        };

        function open(opts) {

          options = angular.extend({}, options, opts);

          sigPlusInstance = $modal.open({
            template: '<iframe id="sigplus-iframe" src="/api/v1/sigplus" style="width:100%; height:100%; border:0; padding:40px 64px 0; overflow:hidden;"></iframe><div class="modal-footer"><button class="btn btn-success" ng-click="$done()">Done</button><button class="btn btn-danger" ng-click="$clear()">Clear</button><button class="btn btn-warning" ng-click="$cancel()">Cancel</button></div>',
            windowClass: 'sig-plus-preview-modal',
            controller: function($scope, $modalInstance) {
              $scope.$done = function() {
                var hexadecimal = $('#sigplus-iframe').get(0).contentWindow.onDone();
                options.onDone(hexadecimal);
                close();
              };
              $scope.$cancel = function() {
                close();
              };
              $scope.$clear = function() {
                $('#sigplus-iframe').get(0).contentWindow.onClear();
              };
            }
          });
        }

        function close() {
          sigPlusInstance.close();
        }

        return {
          open: open,
          close: close
        }
      };
    })

})(angular);