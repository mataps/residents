+(function(angular, undefined) {

    angular.module('ss-table', [
        'smart-table',
        'lrDragNDrop',
        'ss-table.header'
    ]);

})(angular);