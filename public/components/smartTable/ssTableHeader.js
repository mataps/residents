+(function(angular) {

    angular.module('ss-table.header', ['smart-table', 'lrDragNDrop'])
        .directive('ssTableHeader', function($parse) {
            return {
                require: '^stTable',
                restrict: 'A',
                scope: false,
                templateUrl: '/src/client/directives/smartTable/ssTableHeaderTemplate.html',
                link: function(scope, elem, attrs, ctrl) {
                    var getter = $parse(attrs.columns);
                    var setter = getter.assign;
                },
                controller: function($scope) {
                    $scope.parseKey = function(key) {
                        return "'"+key+"'";
                    }
                }
            }
        });

})(angular);