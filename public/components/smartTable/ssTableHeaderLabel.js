+(function(angular) {

    angular.module('ss-table.header')
        .directive('ssTableHeaderLabel', function($parse, $compile) {
            return {
                restrict: 'A',
                replace: true,
                link: function(scope, elem, attrs) {
                    var getter = $parse(attrs.ssTableHeaderLabel);
                    var col = getter(scope);
                }
            }
        });
})(angular);