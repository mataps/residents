+(function(angular, _, undefined) {
  angular
    .module('app')
    .directive('stControl', stControl);

  function stControl() {
    return {
      require: '^stTable',
      template: '',
      scope: { 'stControl': '=stControl' },
      link: linkFn
    }
  }

  function linkFn(scope, element, attr, ctrl) {
    // to js-all-selected..//
    var states = ['all-selected', 'some-selected', 'js-non-selected'];

    /**
     * @description Removes all states, then adds the new one.
     * @param string newClass
     */
    function setClass(newClass) {
      function removeState(state) { element.removeClass(state); }

      angular.forEach(states, removeState);
      element.addClass(newClass);
    }

    // select() shorthands
    function deselectAll() { select(false); }
    function selectAll() { select(true); }

    /**
     * Sets the "isSelected property"
     * @param  {[type]} bool [description]
     * @return {[type]}      [description]
     */
    function select(bool) {
      function setToBool(item) { item.isSelected = bool; }
      angular.forEach(scope.stControl, setToBool);
    }

    /**
     * @name  isSelected
     * @description Checks if one of the checkboxes are :checked
     * @return {Boolean}
     */
    function isSelected() {
      return element.hasClass('all-selected') 
      || element.hasClass('some-selected');
    }

    /**
     * @name watchChanges
     * @description Callback applied when angular
     * watches any changes with the variable
     */
    function watchChanges(newValue, oldValue) {
      var selected = _.where(newValue, {'isSelected': true});

      switch(selected.length) {
        case 0:
          setClass('non-selected');
          element.prop('checked', false);
          break;
        case oldValue.length:
          setClass('all-selected');
          element.prop('checked', true);
          break;
        default:
          setClass('some-selected');
          element.prop('checked', false);
      }
    }

    /**
     * @name  onControllerSelect
     * @description Callback applied after every "change" event is fired
     */
    function onControllerSelect() {
      if ( isSelected() ) {
        deselectAll();
      } else {
        selectAll();
      }

      scope.$apply();
    }

    element.on('change', onControllerSelect); // Binds changes on the element
    scope.$watch('stControl', watchChanges, true); // Watch on changes with the collection
  }
})(angular, _);