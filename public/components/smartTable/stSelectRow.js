angular.module('app').directive('csSelect', function () {

    return {
        require: '^stTable',
        template: '',
        scope: {
            row: '=csSelect'
        },
        link: function (scope, element, attr, ctrl) {
            element.find(':checkbox').bind('change', function (event) {
                scope.$apply(function () {
                    ctrl.select(scope.row, 'multiple');
                });
            });

            scope.$watch('row.isSelected', function (newValue, oldValue) {
                if (newValue === true) {
                    element.addClass('st-selected');
                } else {
                    element.removeClass('st-selected');
                }
            });
        }
    };
});