+(function(angular){

    angular.module('app')
        .directive('triStateCheckbox', function($parse) {

            return {
                restrict: 'A',
                template: '<input type="checkbox">',
                replace: true,
                scope: {
                    model: '=triStateCheckbox'
                },
                link: function(scope, elem, attrs) {
                    var states = [0, 2, 1];
                    var classes = ['state1', 'state2', 'state3'];

                    elem.bind('click', function(e) {
                        e.preventDefault();
                        var selected = states.indexOf(scope.model) + 1;
                        scope.model = states.concat(states)[selected];
                    });

                    scope.$watch('model', function(newValue, oldValue) {
                        if (states.indexOf(newValue) !== -1) {
                            var selected = states.indexOf(newValue);
                            elem.removeClass(classes.join(' '));
                            elem.addClass(classes[selected]);
                        } else {
                            elem.addClass(classes[0]);
                            scope.model = states[0];
                        }
                    });
                }
            }
        });

})(angular);