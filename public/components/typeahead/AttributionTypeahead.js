+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcAttributionTypeahead', AttributionTypeahead);

  function AttributionTypeahead() {
    return {
      scope: {
        /**
         * Model / variable (ID) to assign
         * the id of the result
         * < .. ng-model="data.attribution.id">
         * < .. ng-model="data.attribution_id">
         */
        model: '=ngModel',

        /**
         * For use, e.g. <.. blacklisted="$item
         */
        init: '=',

        /**
         * Blacklisted IDs (array)
         * <.. blacklisted="[1, 2, 3]"
         */
        blacklisted: '='
      },

      // replace: true,
      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };

    /**
     * Directiv template
     */
    function templateFn(e, attrs) {
      var placeholder = attrs.placeholder || 'Enter attribute name..';

      return [
        '<input type="text" ',
          'ng-model="temp.name" ',
          'class="form-control" ',
          'placeholder="', placeholder,'" ',
          'typeahead-on-select="$attributionOnSelect($item)" ',
          'typeahead="attribute.name for attribute in $attributions()">',
      ].join('');
    }

    /**
     * Controllre
     */
    function controllerFn($scope, Restangular, flash) {
      angular.extend($scope, {
        isLoading: false,
        temp: {},
        $attributions: $attributions,
        $attributionOnSelect: $attributionOnSelect
      });

      (function init() {
        if ( $scope.init !== "undefined" ) {
          $scope.temp.name = $scope.init;
        }
      })();

      /**
       * List of attributions.
       * Request the API for the list of attributions
       */
      function $attributions () {
        $scope.isLoading = true;

        return Restangular
          .one('attributions')
          .getList()
          .then(function(r) { return r.data })
          .finally(function() { $scope.isLoading = false });
      };

      /**
       * Assigns the selected attribution from the typeahead
       * to the model. Also validates it
       */
      function $attributionOnSelect($item) {
        // If the blacklist exists, yolo.
        if ( !angular.isUndefined($scope.blacklisted) &&
          $scope.blacklisted.indexOf($item.id) == -1
        ) {
          $scope.temp = {};
          flash.error = 'The attribution already exists in your list';
          return;
        }

        $scope.model = $item.id;
      }
    }
  }
})(angular);