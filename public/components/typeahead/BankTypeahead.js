+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('gcBankTypeahead', directive);

  function directive() {
    return {
      scope: {
        /**
         * Model / variable (ID) to assign
         * the id of the result
         * < .. ng-model="data.bank.id">
         * < .. ng-model="data.bank_id">
         */
        model: '=ngModel',
        vmodel: '=vModel',
        cmodel: '=cModel',

        /**
         * Blacklisted IDs (array)
         * <.. blacklisted="[1, 2, 3]"
         */
        blacklisted: '='
      },

      // replace: true,
      restrict: 'EA',
      template: templateFn,
      controller: controllerFn
    };
    /**
     * Directiv template
     */
    function templateFn(e, attrs) {
      var placeholder = attrs.placeholder || 'Enter bank name..';

      return [
        '<input type="text" ',
          'ng-model="temp.name" ',
          'class="form-control" ',
          'placeholder="', placeholder,'" ',
          'typeahead-on-select="$bankOnSelect($item)" ',
          'typeahead="bank.name for bank in $banks()">'
      ].join('');
    }

    /**
     * Controllre
     */
    function controllerFn($scope, Restangular, flash) {
      angular.extend($scope, {
        isLoading: false,
        temp: {},
        $banks: $banks,
        $bankOnSelect: $bankOnSelect
      });

      (function init() {
        $scope.temp.name = $scope.vmodel;
      })();

      /**
       * List of banks.
       * Request the API for the list of banks
       */
      function $banks () {
        $scope.isLoading = true;

        return Restangular
          .one('banks')
          .getList()
          .then(function(r) { return r.data })
          .finally(function() { $scope.isLoading = false });
      };

      /**
       * Assigns the selected bank from the typeahead
       * to the model. Also validates it
       */
      function $bankOnSelect($item) {
        $scope.model = $item.id;
        $scope.vmodel = $item.name;
      }
    }
  }
})(angular);