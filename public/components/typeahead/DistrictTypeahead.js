+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .directive('districtTypeahead', directive);

  function directive($scope) {

    return {
      template: templateFn,
      replace: true,
      bindToController: true,
      controller: controllerFn,
      controllerAs: 'districtCtrl',
      scope: { model: '=ngModel' },
    };

    /**
     * Directiv template
     */
    function templateFn() {
      return [
        '<input type="text" ng-model="districtCtrl.model" typeahead="resident.name for resident in districtCtrl.districts">',
      ].join('');
    }

    /**
     * Controllre
     */
    function controllerFn(DISTRICTS) {
        var vm = this;

        angular.extend(vm, {
          districts: DISTRICTS,
          $init: $init
        });

        console.log(DISTRICTS);

        /**
         * Initializes model value
         */
        function $init() {
          vm.model = vm.model || 1;
        }
      }
  }
})(angular);