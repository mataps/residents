+(function(angular, _, undefined) {
  angular
    .module('app')
    .directive('gcYearComponent', yearSets);

  function yearSets() {
    var template = [
      '<select ng-model="ySetCtrl.model" ng-init="init()" class="form-control">',
        '<option ng-repeat="year in ySetCtrl.sets" value="{{ year + \'-\' + (year + 1) }}" ng-selected="(year + \'-\' + (year + 1)) == ySetCtrl.model">',
          '{{year}} - {{year + 1}}',
        '</option>',
      '</select>',
    ].join('');

    return {
      restrict: 'EA',
      scope: scope,
      template: template,
      controller: function($scope) {
        /**
         * Gets year in sets (1991-1992)
         * @param  {[type]} range [description]
         * @return {[type]}       [description]
         */
        $scope.sets = (function (range) {
          range = parseInt(range, 10);

          var current = new Date().getFullYear();
          var start   = current - range;
          var end     = current + range;

          return _.range(start, end);
        })(30);

        (function init() {
          if ( angular.isUndefined(vm.model) ) {
            $scope.model = $scope.sets[0] + '-' + $scope.sets[1];
          }
        })()
      }
    };
  }
})(angular, _);
