/* ========================================================================
* Angular Bootstrapper
* ======================================================================== */

+(function (angular, document) {
  'use strict';

  // Wrap in a try-catch exception to catch
  // the exception and log the thrown exception
  try {
    angular.bootstrap(document, ['app']);
  } catch (e) {
    console.error(e.stack || e.message || e);
  }
})(angular, document);