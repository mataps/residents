+(function(angular, _, undefined) {

  // Serves as model / base for "index" controllers
  // (for tables or listing). This eliminates redundant
  // boilerplate code.
  function BaseIndexCtrl($scope, $state, $window, $modal, Idle, Keepalive, UtilService, $modalStack) {
    var columns = [],
        collection = [],
        storageProperties = {};


    $scope.$on('IdleStart', function() { 
        console.log('start idle');
        

    });

     $scope.$on('IdleTimeout', function() {
        console.log('idle timeout');

        alert("The system will automatically log you out for being idle..");
        $window.location.pathname = '/logout';

     });


     $scope.start = function() {
      console.log('start nigga');
      Idle.watch();
      $scope.started = true;
    };

     $scope.stop = function() {
        console.log('stop nigga');
        Idle.unwatch();
        $scope.started = false;

      };




    // ------------------------- //
    // ------------------------- //

    this.collection = collection;
    this.columns = columns;

    // "loading" boolean variable
    this.isLoading = false;
    // Sets the isLoading property to either false or true
    this.load = load;

    // Table
    this.getPage = getPage;
    this.getTableQuery = getTableQuery;
    this.getSelected = getSelected;
    this.callServer = callServer;
    // Default response/promise callback fns
    this.applyPageData = applyPageData;
    this.catchPageDataError = catchPageDataError;

    //
    this.show = show;
    this.resetFilter = resetFilter;

    // ------------------------- //
    // ------------------------- //

    /**
     * @name load
     * @description Sets the load variable
     * @return void
     */
    function load(bool) {
      this.isLoading = ( !_.isUndefined(bool) ) ? bool : true;
    }

    var copyResponseToResource = function(resource, response) {
      angular.forEach(response.data.data, function(value, key) {
        resource[key] = value;
      });
    };

    this.pushToCollection = function(collection, response) {
      var new_resource = response.data;
      copyResponseToResource(new_resource, response);
      collection.push(new_resource);
    };

    this.updateInCollection = function(collection, response) {
      var to_update = _.find(collection, { id: response.data.data.id });
      if (to_update) {
        copyResponseToResource(to_update, response);
      }
    };

    // ------------------------- //
    // ------------------------- //

    /**
     * @name getIDSelected
     * @description Plucks the ids of each object
     * having the isSelected property as true
     * @return {Array}
     */
    function getSelected(property) {

      property || (property = 'id');


      var selected = _.chain(this.collection)
        .where('isSelected')
        .pluck(property)
        .value();

      console.log(selected);

      return selected;
    }

    // ------------------------- //
    // ------------------------- //

    /**
     * @name  applyPageData
     * @description Callback function for promises after
     * a server | ajax call
     * @see  getPageData
     * @param  {[type]} response [description]
     * @return {[type]}          [description]
     */
    function applyPageData(response) {
      if ( angular.isUndefined(response.data.paging.previous) ) {
        console.log('Replacing..');
        this.collection = response.data;
      } else {
        response.data.forEach(function(data) {
          this.collection.push(data);
        }.bind(this));
        this.collection.paging = response.data.paging;
        this.collection.next = response.data.paging.next;
        this.collection.previous = response.data.paging.previous;
      }

      this.load(false);
    }

    /**
     * @name  catchPageDataError
     * @description  Callback function for promises
     * if a promise is not resolved
     * @return {[type]} [description]
     */
    function catchPageDataError(e) {
      console.log(e);
      this.load(false);
    }

    /**
     * @name  getPage
     * @description Runs tasks, then gets pageData.
     * @param  string dir previous|next
     * @return void
     */
    function getPage(dir) {
      this.load(true);

      params = angular.extend({}, UtilService.decodeQuery(
        this.collection.paging[dir]
      ), this.__query);

      return this.getPageData(params);
    }

    /**
     * @name getPageData
     * @description Fetches data and replaces the current
     * @param  string query
     * @return void
     v*/
    function getPageData(query) {
      // ScholarshipSrvc
      //       .Query
      //       .all(query)
      //       .then(function(response) {
      //           loading();
      //           applyResponse(response);
      //       })
      //       .catch(function() {
      //           loading(false);
      //       });
    }

    function getTableQuery(tableState) {
      var query = {};
      this.__query = query;

      if (tableState.sort.predicate) {
        query.sort = tableState.sort.reverse ? '' : '-';
        query.sort += tableState.sort.predicate;
      }

      if (tableState.search.predicateObject) {
        query.filters = {};
        angular.forEach(tableState.search.predicateObject, function(value, key) {
            this[key] = value;
        }, query.filters);
      }

      if ( query.filters && query.filters.sort !== undefined ) {
        query.filters = _.omit(query.filters, 'sort');
      }

      if ( this.__query.filters && this.__query.filters.sort !== undefined ) {
        this.__query.filters = _.omit(this.__query.filters, 'sort');
      }

      console.log(query);

      return query;
    }

    /**
     * @name callServer
     * @see  getPageData
     * @see  load
     * @param  Object tableState Smart table's tableState
     * @return void
     */
    function callServer(tableState) {
      this.load();
      this.getPageData(
        this.getTableQuery(tableState)
      );
    }

    function show(id, stateName) {
      var state = (
        stateName ||
        $state.current.name + '.edit'
      );

      console.log(id);
      $state.go(state, { id: id });
    }

    function resetFilter() {
      $state.reload();
    }
  }

  angular
    .module('app')
    .controller('BaseIndexCtrl', BaseIndexCtrl)
    .run(['Idle', function(Idle){
      Idle.watch();
    }]);
})(angular, _);
