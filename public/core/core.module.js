+(function(angular, undefined) {
  angular
    .module('app.components', [
      'app.controllers',
      'app.filters',
      'app.resources',
      'app.services'
    ]);
})(angular);