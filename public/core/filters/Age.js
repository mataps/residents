+(function(angular, undefined) {
  'use strict';
  angular
    .module('app')
    .filter('age', age);

  function age() {
    return function(input) {
      var current = Date.now();
      var birthdate = input;

      var ageMs = current - birthdate;
      var ageDate = new Date(ageMs);
      var age = ageDate.getUTCFullYear() - 1970;

      return Math.abs( parseInt(age, 10) );
    }
  }
})(angular);