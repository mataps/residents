+(function(angular, undefined) {
  "use strict";
  angular
    .module('app')
    .filter('capitalize', capitalize);

    /**
     * Capitalize the first letter of the word(s)
     */
  function capitalize() {
    return function(input, each) {
      if ( !angular.isString(input) ) {
        return '';
      }
      
      return !input.length
        ? ''
        : (each
          ? input.split(' ')
            .map(uppercaseFirst)
            .join(' ')
          : uppercaseFirst(input)
        );
    }

    /**
     * Capitalizes the first letter
     */
    function uppercaseFirst(w) {
      return w.charAt(0).toUpperCase() + w.slice(1);
    }
  }
})(angular);