+(function(angular, undefined) {
  "use strict";
  angular
    .module('app')
    .filter('money', filter);

  function filter() {
    return function (input) {
      var str = parseFloat(input, 10).toString();
      var _str = str.split('.');
      var value = _str[0];
      var decimals = _str[1] || '00';
      var result = '';

      for ( var len = value.length, i = len - 1; i >= 0; i-- ) {
        result = value.charAt(i) + ((len - i - 1) % 3 == 0 && i !== len - 1 ? ',' : '' ) + result;
      }

      return result + '.' + decimals;
    };
  }
})(angular);