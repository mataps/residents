+(function(angular, undefined) {
	'use strict';
  angular
    .module('app')
    .filter('studlyCase', filter);

  function filter() {
    return filterFn;

    function filterFn(input) {
      // Split each word (returns an array of words)
      // then convert each first letter of the word
      // to uppercase. (returns the array of modified words)
      // Then, join each array by a space.
      return input
        .split(' ')
        .map(uppercaseLetter)
        .join(' ');

      /**
       * Converts the first letter to uppercase
       * @see Array.map
       */
      function uppercaseLetter(word) {
        if ( ! word.length ) {
          return;
        }

        var first = word.charAt(0);

        return /^[a-z]/g.test(first)
          ? first.toUpperCase() + word.slice(1)
          : word;
      }
    }
  }
})(angular);