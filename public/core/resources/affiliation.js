+(function(angular) {
  function Affiliation(Restangular) {
    var service,
      collection = [],
      instance = {},
      Affiliation,
      Query,
      Command,
      Repository;
    
    /**
     * @description Restangular object
     * @var {object}
     */
    Affiliation = Restangular.service('affiliations');

    /**
    * Functions used to query data
    * 
    * @type {Object}
    */
    Query = {
      all: function(params) {
        return Affiliation
          .getList(params);
      },

      get: function(id) {
          return Affiliation.one(id).get();
      },
      typeahead: function(name) {
        return Affiliation
          .getList({
            filters: JSON.stringify({ name: name })
          });
      }
    };

    /**
    * Functions used for @see nding request to the API
    * 
    * @type {Object}
    */
    Command = {
      create: function(data) {
        return Affiliation.post(data);
      },

      update: function(data) {
        return data.put();
      },

      delete: function(ids) {
        return Affiliation
          .one(ids)
          .one('remove')
          .remove();
      }
    };

    /** Return a copy of an instance */
    function getInstance() {
      return instance.clone();
    }

    /**
     * Functions with built in after-actions. Usually
     * built for resolves
     * @type {Object}
     */
    Repository = {
      all: function() {
        return Query
          .all()
          .then(function(response) {
            angular.extend(collection, response.data);
            return response;
          });
      },

      /**
       * @description  Fetches (one) Affiliation from the API
       *               then applies the returned data to
       *               the service's instance property
       * @param  {[type]} id [description]
       * @return {[type]}    [description]
       */
      get: function(id) {
        return Query
          .get(id)
          .then(function(response) {
            angular.extend(instance, response.data);
            return response;
          });
      }
    };

    /** Return a copy of an instance */
    function getInstance() {
      return instance.clone();
    }

    return {
      /**
       * [list description]
       * @type {Object}
       */
      collection: collection,
      
      /**
       * [instance description]
       * @type {Object}
       */
      instance: instance,

      /** */
      getInstance: getInstance,

      /**
       * [query description]
       * @type {[type]}
       */
      Query: Query,

      /**
       * [command description]
       * @type {[type]}
       */
      Command: Command,

      /**
       * [Repository description]
       * @type {Array}
       */
      Repository: Repository
    };
  };

  angular
    .module('app')
    .factory('Affiliation', Affiliation);
})(angular);