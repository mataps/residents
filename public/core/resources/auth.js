function AuthSrvc (Restangular) {

  /**
   * @description Restangular object
   * @var {object}
   */
  var Auth = Restangular.all('auth');

  /**
   * Functions used for queries
   *
   * @type {Object}
   */
  var Query = {

    check: function () {
      
    },

    guest: function () {
      
    },

    data: function () {
      
    }
    
  };

  /**
   * Functions used for commands
   * 
   * @type {Object}
   */
  var Command = {
    attempt: function (data, callback) {
      
    },
    logout: function () {
      
    }
  }

  return {
    /**
     * [status description]
     * @type {Boolean}
     */
    status: false,
    /**
     * [data description]
     * @type {Object}
     */
    data: {},
    /**
     * [query description]
     * @type {[type]}
     */
    query: Query,
    /**
     * [command description]
     * @type {[type]}
     */
    command: Command
  }
}

angular.module('app').factory('AuthSrvc', AuthSrvc);