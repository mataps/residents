+(function(angular) {
  function Award(Restangular) {
    var service,
      collection = [],
      instance = {},
      Award,
      Query,
      Command,
      Repository;
    
    /**
     * @description Restangular object
     * @var {object}
     */
    Award = Restangular.service('awards');

    /**
    * Functions used to query data
    * 
    * @type {Object}
    */
    Query = {
      all: function(params) {
        return Award
          .getList(params);
      },

      get: function(id) {
        return Award
          .one(id)
          .get();
      }
    };

    /**
    * Functions used for @see nding request to the API
    * 
    * @type {Object}
    */
    Command = {
      create: function(data) {
        return Award.post(data);
      },

      update: function(data) {
        return data.put();
      },

      delete: function(ids) {
        return Award
          .one(ids)
          .one('remove')
          .remove();
      }
    };

    /**
     * Functions with built in after-actions. Usually
     * built for resolves
     * @type {Object}
     */
    Repository = {
      all: function() {
        return Query
          .all()
          .then(function(response) {
            angular.extend(collection, response.data);
            return response;
          });
      },

      /**
       * @description  Fetches (one) Award from the API
       *               then applies the returned data to
       *               the service's instance property
       * @param  {[type]} id [description]
       * @return {[type]}    [description]
       */
      get: function(id) {
        return Query
          .get(id)
          .then(function(response) {
            angular.extend(instance, response.data);
            return response;
          });
      }
    };

    /**
     * Getter for the deep-copied instance
     * @return Restangular
     */
    function getInstance() {
      return instance.clone();
    }

    return {
      /**
       * [list description]
       * @type {Object}
       */
      collection: collection,
      
      /**
       * [instance description]
       * @type {Object}
       */
      instance: instance,

      /**
       * instance getter
       * @type {[type]}
       */
      getInstance: getInstance,

      /**
       * [query description]
       * @type {[type]}
       */
      Query: Query,

      /**
       * [command description]
       * @type {[type]}
       */
      Command: Command,

      /**
       * [Repository description]
       * @type {Array}
       */
      Repository: Repository
    };
  };

  angular
    .module('app')
    .factory('Award', Award);
})(angular);