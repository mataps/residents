+(function(angular) {
  function Position($http, Restangular, $q) {
    var service,
      copies = {},
      collection = [],
      instance = {},
      Position,
      Query,
      Command,
      Repository;
    
    /**
     * @description Restangular object
     * @var {object}
     */
    Position = Restangular.service('positions');

    /**
    * Functions used to query data
    * 
    * @type {Object}
    */
    Query = {
      all: function(params) {
        return Position
          .getList(params);
      },

      get: function(id) {
          return Position.one(id).get();
      },
      typeahead: function(name) {
        return Position
          .getList({
            filters: JSON.stringify({ name: name })
          });
      }
    };

    /**
    * Functions used for @see nding request to the API
    * 
    * @type {Object}
    */
    Command = {
      create: function(data) {
        return Position.post(data);
      },

      update: function(data) {
        return data.put();
      },

      delete: function(ids) {
        return Position
          .one(ids)
          .one('remove')
          .remove();
      }
    };

    /** Return a copy of an instance */
    function getInstance() {
      return instance.clone();
    }

    /**
     * Functions with built in after-actions. Usually
     * built for resolves
     * @type {Object}
     */
    Repository = {
      all: function() {
        return Query
          .all()
          .then(function(response) {
            angular.extend(collection, response.data);
            return response;
          });
      },

      /**
       * @description  Fetches (one) Position from the API
       *               then applies the returned data to
       *               the service's instance property
       * @param  {[type]} id [description]
       * @return {[type]}    [description]
       */
      get: function(id) {
        return Query
          .get(id)
          .then(function(response) {
            angular.extend(instance, response.data);
            return response;
          });
      },

      getWithAffiliation: function(id) {
        var positions = $http
          .get('api/v1/positions')
          .then(function(response) {
            copies.positions = response.data.data;
            return response;
          });

        var affiliations = $http
          .get('api/v1/affiliations/' + id + '/positions')
          .then(function(response) {
            copies.affiliations = response.data.data;
            return response;
          });

        return $q.all(positions, affiliations);
      }
    };

    /** Return a copy of an instance */
    function getInstance() {
      return instance.clone();
    }

    return {
      copies: copies,

      /**
       * [list description]
       * @type {Object}
       */
      collection: collection,
      
      /**
       * [instance description]
       * @type {Object}
       */
      instance: instance,

      /** */
      getInstance: getInstance,

      /**
       * [query description]
       * @type {[type]}
       */
      Query: Query,

      /**
       * [command description]
       * @type {[type]}
       */
      Command: Command,

      /**
       * [Repository description]
       * @type {Array}
       */
      Repository: Repository
    };
  };

  angular
    .module('app')
    .factory('Position', Position);
})(angular);