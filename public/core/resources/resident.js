+(function(angular) {
  function Resident(Restangular, $http) {
    var service,
      collection = [],
      instance = {},
      Resident,
      Query,
      Command,
      Repository;
    
    /**
     * @description Restangular object
     * @var {object}
     */
    Resident = Restangular.service('residents');

    /**
    * Functions used to query data
    * 
    * @type {Object}
    */
    Query = {
      all: function(params) {
        return Resident
          .getList(params);
      },

      get: function(id) {
        return Resident
          .one(id)
          .get();
      },

      typeahead: function(name) {
        return Resident
          .getList({
            filters: JSON.stringify({ full_name: name })
          });
      }
    };

    /**
    * Functions used for @see nding request to the API
    * 
    * @type {Object}
    */
    Command = {
      create: function(data) {
        return Resident.post(data);
      },

      update: function(data) {
        return data.put();
      },

      delete: function(ids) {
        return Resident
          .one(ids)
          .one('remove')
          .remove();
      }
    };

    /**
     * Functions with built in after-actions. Usually
     * built for resolves
     * @type {Object}
     */
    Repository = {
      all: function() {
        return Query
          .all()
          .then(function(response) {
            angular.extend(collection, response.data);
            return response;
          });
      },

      /**
       * @description  Fetches (one) Resident from the API
       *               then applies the returned data to
       *               the service's instance property
       * @param  {[type]} id [description]
       * @return {[type]}    [description]
       */
      get: function(id) {
        return $http
          .get('api/v1/residents/' + id)
          .then(function(response) {
            angular.extend(instance, response.data);
            return response;
          });
      }
    };

    return {
      /**
       * [list description]
       * @type {Object}
       */
      collection: collection,
      
      /**
       * [instance description]
       * @type {Object}
       */
      instance: instance,

      /**
       * [query description]
       * @type {[type]}
       */
      Query: Query,

      /**
       * [command description]
       * @type {[type]}
       */
      Command: Command,

      /**
       * [Repository description]
       * @type {Array}
       */
      Repository: Repository
    };
  };

  angular
    .module('app')
    .factory('Resident', Resident);
})(angular);