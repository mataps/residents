+(function(angular) {
  function School (Restangular) {
    var service,
      collection = [],
      instance = {},
      Schools,
      Query,
      Command,
      Repository;
    
    /**
     * @description Restangular object
     * @var {object}
     */
    School = Restangular.service('schools');

    /**
    * Functions used to query data
    * 
    * @type {Object}
    */
    Query = {
      all: function(params) {
        return School
          .getList(params);
      },

      get: function(id) {
        return School
          .one(id)
          .get();
      },

      typeahead: function(name) {
        return School
          .getList({
            filters: JSON.stringify({ name: name })
          });
      }
    };

    /**
    * Functions used for @see nding request to the API
    * 
    * @type {Object}
    */
    Command = {
      create: function(data) {
        return School.post(data);
      },

      update: function(data) {
        return data.put();
      },

      delete: function(ids) {
        return School
          .one(ids)
          .one('remove')
          .remove();
      }
    };

    /**
     * Functions with built in after-actions. Usually
     * built for resolves
     * @type {Object}
     */
    Repository = {
      all: function() {
        return Query
          .all()
          .then(function(schools) {
            angular.extend(collection, schools.data);
            return schools;
          });
      },

      /**
       * @description  Fetches (one) school from the API
       *               then applies the returned data to
       *               the service's instance property
       * @param  {[type]} id [description]
       * @return {[type]}    [description]
       */
      get: function(id) {
        return Query
          .get(id)
          .then(function(resource) {
            angular.extend(instance, resource.data);
            return resource;
          });
      }
    };

    /**
     * Getter for the deep-copied instance
     * @return Restangular
     */
    function getInstance() {
      return instance.clone();
    } 

    return {
      /**
       * [list description]
       * @type {Object}
       */
      collection: collection,
      
      /**
       * [instance description]
       * @type {Object}
       */
      instance: instance,

      /**
       * instance getter
       * @type {[type]}
       */
      getInstance: getInstance,   

      /**
       * [query description]
       * @type {[type]}
       */
      Query: Query,

      /**
       * [command description]
       * @type {[type]}
       */
      Command: Command,

      /**
       * [Repository description]
       * @type {Array}
       */
      Repository: Repository
    };
  };

  angular
    .module('app')
    .factory('School', School);
})(angular);