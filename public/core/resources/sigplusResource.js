+(function(angular) {
  function SigplusResource(Restangular) {
    return Restangular.service('sigplus');
  }

  angular
    .module('app')
    .factory('SigplusResource', SigplusResource);
})(angular);