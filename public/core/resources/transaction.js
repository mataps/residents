+(function(angular) {
	function Transaction(Restangular, localStorageService) {
		var service,
			collection = [],
			instance = {},
			Transaction,
			Query,
			Command,
			Repository;

		/**
		 * @description Restangular object
		 * @var {object}
		 */
		Transaction = Restangular.service('transactions');

		/**
		 * Functions used to query data
		 *
		 * @type {Object}
		 */
		Query = {
			all: function(params) {
				return Transaction
					.getList(params);
			},

			get: function(id) {
				return Transaction.one(id).get();
			}
		};

		/**
		 * Functions used for @see nding request to the API
		 *
		 * @type {Object}
		 */
		Command = {
			create: function(data) {
				return Transaction.post(data);
			},

			update: function(data) {
				return data.put();
			},

			delete: function(ids) {
				return Transaction
          .one(ids)
          .one('remove')
          .remove();
			}
		};

		/**
		 * Functions with built in after-actions. Usually
		 * built for resolves
		 * @type {Object}
		 */
		Repository = {
			all: function() {
				var ta = localStorageService.get('TRANSACTION_ACCOUNT_FILTER');
				var date = localStorageService.get('TRANSACTION_ACCOUNT_DATE_FILTER');
				var filter = (ta !== undefined && ta !== '' && ta !== null) || (date !== undefined && date !== '' && date !== null)
					? { filters: angular.extend(ta !== undefined && ta !== '' && ta !== null ? {
							account_type_id: ta,
						}: {}, date !== undefined && date !== '' && date !== null ? {
							created_at_persist: date
						}: {})
					}
					: {};

				console.log(date);

				return Query
					.all(filter)
					.then(function(response) {
						angular.extend(collection, response.data);
						return response;
					});
			},

			/**
			 * @description  Fetches (one) Transaction from the API
			 *               then applies the returned data to
			 *               the service's instance property
			 * @param  {[type]} id [description]
			 * @return {[type]}    [description]
			 */
			get: function(id) {
				console.log(id);
				return Query
					.get(id)
					.then(function(response) {
						angular.extend(instance, response.data);
						return response;
					});
			}
		};

		return {
			/**
			 * [list description]
			 * @type {Object}
			 */
			collection: collection,

			/**
			 * [instance description]
			 * @type {Object}
			 */
			instance: instance,

			/**
			 * [query description]
			 * @type {[type]}
			 */
			Query: Query,

			/**
			 * [command description]
			 * @type {[type]}
			 */
			Command: Command,

			/**
			 * [Repository description]
			 * @type {Array}
			 */
			Repository: Repository
		};
	};

	angular
		.module('app')
		.factory('Transaction', Transaction);
})(angular);
