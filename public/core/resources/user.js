+(function(angular) {
	function UserService(Restangular) {
		var service,
			collection = [],
			instance = {},
			User,
			Query,
			Command,
			Repository;

		/**
		 * @description Restangular object
		 * @var {object}
		 */
		User = Restangular.service('users');

		/**
		 * Functions used to query data
		 *
		 * @type {Object}
		 */
		Query = {
			all: function(params) {
				return User
					.getList(params);
			},

			get: function(id) {
				return User.one(id).get();
			},

			deactivated : function(params)
			{
				console.log('sad');
				return User.one('deactivated').getList(params);
			},

			getDeactivated : function(id){
				return User.one('deactivated').one(id).get();
			}
		};

		/**
		 * Functions used for @see nding request to the API
		 *
		 * @type {Object}
		 */
		Command = {
			create: function(data) {
				return User.post(data);
			},

			update: function(data) {
				console.log(data);
				// return User.put();
			},

			delete: function(ids) {
				return User
          .one(ids)
          .one('remove')
          .remove();
			}
		};

		/**
		 * Functions with built in after-actions. Usually
		 * built for resolves
		 * @type {Object}
		 */
		Repository = {
			all: function() {
				return Query
					.all()
					.then(function(response) {
						collection = [];
						angular.extend(collection, response.data);
						return response;
					});
			},

			deactivated : function(){
				return Query
					  .deactivated()
					  .then(function(response){
					  	
					  		angular.extend(collection, response.data);
					  		return response;
					  });
			},

			/**
			 * @description  Fetches (one) User from the API
			 *               then applies the returned data to
			 *               the service's instance property
			 * @param  {[type]} id [description]
			 * @return {[type]}    [description]
			 */
			get: function(id) {
				return Query
					.get(id)
					.then(function(response) {
						angular.extend(instance, response.data);
						return response;
					});
			},
			getDeactivated : function(id){
				return Query
						.getDeactivated(id)
						.then(function(response) {
						angular.extend(instance, response.data);
						return response;
					});
			}
		};

		return {
			/**
			 * [list description]
			 * @type {Object}
			 */
			collection: collection,

			/**
			 * [instance description]
			 * @type {Object}
			 */
			instance: instance,

			/**
			 * [query description]
			 * @type {[type]}
			 */
			Query: Query,

			/**
			 * [command description]
			 * @type {[type]}
			 */
			Command: Command,

			/**
			 * [Repository description]
			 * @type {Array}
			 */
			Repository: Repository
		};
	};

	angular
		.module('app')
		.factory('User', UserService);
})(angular);