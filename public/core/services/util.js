+(function(angular, _, undefined) {
  'use strict';
 
  function UtilService() {
 
    /**
     * Decodes the URL's query string
     * @return string
     */
    function decodeQuery(query) {
      var decode = function(query) {
        return query
          .replace(/(^\?)/, '')
          .split("&")
          .map(function(n) {
            return n = n.split("="), this[n[0]] = n[1], this
          }.bind({}))[0];
      };
 
      return decode(query) || '';
    }
 
    /**
     * Gets year in sets (1991-1992)
     * @param  {[type]} range [description]
     * @return {[type]}       [description]
     */
    function getYearSets(range) {
      var current = new Date().getFullYear();
      var start   = current - range;
      var end     = current + range;
 
      return _.range(start, end);
    }
 
    /**
     * Computes thte total values of the array
     * @param  array grade
     * @param  int property
     * @return int
     */
    function arrayTotal(array, property) {
      var value,
        total = 0, // Total value
        propDefined = !_.isUndefined(property);
 
      // Iterates through each
      array.forEach(function(element, index) {
        value = (
          _.isObject(element) && propDefined
          ? element[property] : value
        );        
 
        total += parseInt(value, 10);
      });
 
      return total;
    }

    /**
     * @name  tralse
     * @description Returns given boolean, then fallsback to true
     * @return {Boolean}
     */
    function tralse(bool) {
      return ! angular.isUndefined(bool) ? bool : true;
    }
 
    return {
      decodeQuery: decodeQuery,
      getYearSets: getYearSets,
      arrayTotal: arrayTotal,
      tralse: tralse
    };
  }
 
  angular
    .module('app')
    .factory('UtilService', UtilService);
})(angular, _);