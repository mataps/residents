//describe('Authentication Service', function() {
//	beforeEach(module('app'));
//	beforeEach(module('stateMock'));
//
//	var $httpBackend, $state, srvc;
//	var checkHandler;
//	var guestHandler;
//	var dataHandler;
//	var loginHandler;
//
//	var data = {
//		username: 'blabla',
//		password: 'blabla'
//	};
//
//	beforeEach(inject(function(_$httpBackend_, AuthSrvc, _$state_) {
//		$httpBackend = _$httpBackend_;
//		srvc = AuthSrvc;
//		$state = _$state_;
//
//		$state.expectTransitionTo('main');
//
//		checkHandler = $httpBackend.whenGET('/api/auth/check');
//		guestHandler = $httpBackend.whenGET('/api/auth/guest');
//		// dataHandler = $httpBackend.whenGET('/api/auth/data');
//		// loginHandler = $httpBackend.whenGET('/api/auth/login');
//	}));
//
//	afterEach (function () {
//		$httpBackend.verifyNoOutstandingExpectation ();
//		$httpBackend.verifyNoOutstandingRequest ();
//	});
//
//
//	describe("its defaults", function() {
//		it("should have a status of false", function() {
//			expect(srvc.status).toBeFalsy();
//		});
//
//		it("should have empty data", function() {
//			expect(srvc.data).toEqual({});
//		});
//	});
//
//	describe("Query", function() {
//		it("should request to server if session is authenticated", function () {
//			checkHandler.respond(200, { status: true });
//
//			$httpBackend.expectGET('/api/auth/check');
//
//			srvc.query.check();
//
//			$httpBackend.flush();
//
//			// expect(srvc.status).toBeTruthy();
//			// promise.then(function () {
//			// 	// expect(srvc.status).toBe(true);
//			// });
//			// expect(srvc.status).toEqual(true);
//		});
//
//		it("should request to server if session is guest", function () {
//			guestHandler.respond({ status: false });
//			$httpBackend.expectGET('/api/auth/guest')
//
//			srvc.query.guest();
//
//			$httpBackend.flush();
//			// expect(srvc.status).toBe(false);
//		});
//
//		it("should fetch auth data", function () {
//			$httpBackend.expectGET('/api/auth/data')
//				.respond(200, {
//					status: true
//				});
//
//			srvc.query.data();
//			$httpBackend.flush();
//		});
//	});
//
//	// describe("Command", function () {
//	// 	it("should be able to send a login request", function () {
//	// 		loginHandler.respond({ status: true });
//
//	// 		$httpBackend.expectPOST('/api/auth/attempt', data).respond({ status: true });
//
//	// 		srvc.command.attempt(data);
//
//	// 		$httpBackend.flush();
//	// 	});
//	// });
//});