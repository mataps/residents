+(function(angular){

    angular.module('app').run(function($rootScope, Restangular, $modal, $state, $stateParams) {
        var resolve = function(event, toState, toParams, fromState, fromParams, error) {
            //Unauthorized
            if(error.status === 401) {
                var modalInstance = $modal.open({
                    templateUrl: '/src/client/components/_modals/login.html',
                    controller: 'LoginCtrl'
                });

                modalInstance.result.then(function() {
                    $state.transitionTo(toState);
                });

                return modalInstance.result;
            }

            return true; // error not handled
        };

        $rootScope.$on('$stateChangeError', resolve);
    });

})(angular);