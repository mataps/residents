+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .config(config);

  function config($locationProvider) {
    $locationProvider.html5Mode(true);
    $locationProvider.hashPrefix('!');
  }

})(angular);