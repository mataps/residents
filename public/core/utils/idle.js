+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .config(config);

  function config(IdleProvider, KeepaliveProvider) {
      IdleProvider.idle(1800);
      IdleProvider.timeout(5);
      KeepaliveProvider.interval(10);
  }
})(angular);