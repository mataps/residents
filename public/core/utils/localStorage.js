+(function(angular, undefined) {

  'use strict';

  angular
    .module('app')
    .config(config);

  function config(localStorageServiceProvider) {
    localStorageServiceProvider.setPrefix('galactus');
    localStorageServiceProvider.setStorageCookie(0, '/');
    localStorageServiceProvider.setNotify(true, true);
  }

})(angular);
