+(function(angular, undefined){

  angular
    .module('app')
    .run(runBlock);

  function runBlock($rootScope, ngProgressLite) {
    var
      start = function () { ngProgressLite.start(); },
      done = function () { ngProgressLite.done(); };

    $rootScope.$on('$stateChangeStart', function() { start(); });
    $rootScope.$on('$stateChangeSuccess', function() { done(); });
    $rootScope.$on('$stateChangeError', function() { done(); });
    $rootScope.$on('$stateNotFound', function() { done() });
  }

})(angular);