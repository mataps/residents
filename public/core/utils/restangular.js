+(function(angular, undefined) {

  angular
    .module('app')
    .config(config);

  function config(RestangularProvider) {
    /** Base URL */
    RestangularProvider.setBaseUrl('/api/v1');

    /** Fetch all responses */
    RestangularProvider.setFullResponse(true);

    /** Modify responses for certain operations */
    RestangularProvider.setResponseExtractor(function(response, operation) {
      /** Modifies response for getList() */
      if (operation === 'getList') {
        var res = response.data;

        if ( response.paging) {
          res.paging = response.paging;
        }

        return res;
      }

      return response;
    });
  }

})(angular);