+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .config(config);

  function config(uiSelectConfig) {
    uiSelectConfig.theme = 'bootstrap';
  }
})(angular);
