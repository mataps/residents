+(function(angular, undefined) {
  'use strict';

  angular
    .module('app')
    .config(config);

  function config(srphXhrFactoryProvider) {
    srphXhrFactoryProvider.setBaseURL('/api/v1/');
  }
})(angular);