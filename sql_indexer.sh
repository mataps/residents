#!/bin/sh
kill -9 $(ps aux | grep '[e]lasticsearch' | awk '{ print $2 }')
# rm -rf /home/sourcescript/indexing/elasticsearch-1.5.1/data/elasticsearch/nodes/
rm -rf /usr/local/var/elasticsearch/elasticsearch_admin/nodes/
elasticsearch -d

sleep 10

curl -XDELETE "http://127.0.0.1:9200/_river/g_river"
curl -XDELETE "http://127.0.0.1:9200/_river/t_river"
curl -XDELETE "http://127.0.0.1:9200/_river/s_river"
curl -XDELETE "http://127.0.0.1:9200/_river/sem_river"

curl -XDELETE "http://127.0.0.1:9200/galactus_production/residents"
curl -XDELETE "http://127.0.0.1:9200/galactus_production/transactions"
curl -XDELETE "http://127.0.0.1:9200/galactus_production/scholarships"
curl -XDELETE "http://127.0.0.1:9200/galactus_production/semesters"

curl -XPUT "http://127.0.0.1:9200/_river/g_river/_meta" -d '{
    "type" : "jdbc",
    "jdbc" : {
        "url" : "jdbc:mysql://localhost:3306/galactus_edbms",
        "user" : "root",
        "password" : "root",
        "sql" : "SELECT residents.id as _id, districts.name as district, residents.precinct as precinct, residents.nickname as nickname, residents.occupation as occupation, residents.notes as notes, resident_att.`attributions_name` as `attributions[]`, ra.`affiliation_name` as `affiliations[]`, residents.civil_status as civil_status, residents.deceased_at as deceased_at, residents.blacklisted_at as blacklisted_at, residents.id AS id,residents.first_name as first_name,residents.last_name AS last_name,residents.middle_name AS middle_name,residents.full_name AS full_name,residents.email AS email,residents.voters_id AS voters_id,residents.is_voter as is_voter,residents.mobile AS mobile,residents.mobile_1 AS mobile_1,residents.mobile_2 AS mobile_2, residents.phone AS phone,residents.gender AS gender,barangays.name AS barangay,cities_municipalities.name as cityMunicipality,residents.street as street,residents.birthdate as birthdate, CONCAT(\"/uploads/\", cities_municipalities.name, \"/photos/\", photos.filename) as profilePic from residents left join photos on residents.photo_id = photos.id join barangays on residents.barangay_id=barangays.id join cities_municipalities on residents.city_municipality_id = cities_municipalities.id LEFT JOIN (select resident_id, affiliations.name as affiliation_name from residents_affiliations left join affiliations on `residents_affiliations`.affiliation_id = affiliations.id) ra on (residents.id =  ra.resident_id) LEFT JOIN (select resident_id, attributions.name as attributions_name from residents_attributes left join attributions on `residents_attributes`.attribute_id = attributions.id) resident_att on (residents.id =  resident_att.resident_id) join districts on residents.district_id = districts.id",
        "index":"galactus_production",
        "type":"residents"
      	}
     }'  

curl -XPUT "http://127.0.0.1:9200/_river/t_river/_meta" -d '{
    "type" : "jdbc",
    "jdbc" : {
        "url" : "jdbc:mysql://localhost:3306/galactus_edbms",
        "user" : "root",
        "password" : "root",
        "sql" : "select t.id as _id, vendor.name as vendor_name, t.transaction_type as transaction_type, t.created_by as created_by, t.liquidatable as liquidatable, t.liquidated_at as liquidated_at, voucher.uuid as voucher_uuid, t.voucher_id as voucher_id,  t.id as id, client.full_name as client_name, t.client_type as client_type, IF(t.beneficiary_type = \"Resident\", beneficiary.full_name, ba.name) as beneficiary_name, t.beneficiary_type as beneficiary_type, referer.full_name as referer_name, accounts.name as account_name, accounts.id as account_type_id, category.name as category, sub_categories.name as sub_category, item.name as item, t.settled_at as settled_at, DATE_FORMAT(t.created_at, \"%Y-%m-%d\") as created_at, t.details as details, t.remarks as remarks, t.amount as amount, t.id as es_id from transactions as t left join residents as referer on referer.id = t.referrer_id left join residents as client on client.id = t.client_id left join residents as beneficiary on beneficiary.id = t.beneficiary_id left join affiliations as ba on ba.id = t.beneficiary_id left join transaction_sub_categories as sub_categories on sub_categories.id = t.sub_category_id left join transaction_categories as category on category.id = sub_categories.category_id left join vouchers as voucher on voucher.id = t.voucher_id left join transaction_accounts as accounts on accounts.id = voucher.account_type_id left join transaction_items as item on item.id = t.item_id left join transaction_vendors as vendor on vendor.id = t.vendor_id where t.deleted_at is NULL",
        "index":"galactus_production",
        "type":"transactions"
        }
     }'


curl -XPUT "http://127.0.0.1:9200/_river/s_river/_meta" -d '{
    "type" : "jdbc",
    "jdbc" : {
        "url" : "jdbc:mysql://localhost:3306/galactus_edbms",
        "user" : "root",
        "password" : "root",
        "sql" : "select DATE_FORMAT(s.approved_at, \"%Y-%m-%d\") as approved_at, semester_d.school as school_name, semester_d.term as term, semester_d.`year_level` as year_level, semester_d.course as course_name, semester_d.scholar_type as scholarship_type, s.id as _id, s.id as id, r.last_name as last_name, r.middle_name, r.first_name, r.birthdate as birthdate, b.name as barangay_name, cm.name as cityMunicipality, r.street as street, r.birthdate, districts.name as district, r_attr.attributions_name as `attributions[]`, ra_aff.affiliation_name as `affiliations[]`, s_award.award_name as `awards[name]`, s_award.year as `awards[year]`, s_award.remarks as `awards[remarks]`, r.civil_status as civil_status, s.status as status, s.comment as `comment`, s.father_name as father_name, s.mother_name as mother_name, s.guardian_name as guardian_name, r.mobile as scholar_contact_no, s.parents_contact_no as parents_contact_no, s.others_contact_no as others_contact_no, s.guardian_contact_no as guardian_contact_no from scholarships as s left join residents as r on r.id =  s.resident_id left join barangays as b on r.barangay_id=b.id left join cities_municipalities cm on r.city_municipality_id = cm.id left  join (select resident_id, affiliations.name as affiliation_name from residents_affiliations left  join affiliations on `residents_affiliations`.affiliation_id = affiliations.id) ra_aff on (r.id =  ra_aff.resident_id) left  join (select resident_id, attributions.name as attributions_name from residents_attributes left  join attributions on `residents_attributes`.attribute_id = attributions.id) r_attr on (r.id =  r_attr.resident_id) left join districts on r.district_id = districts.id left  join (select scholarship_id, awards.name as award_name, award_scholarship.year as year, award_scholarship.remarks as remarks from award_scholarship left  join awards on `award_scholarship`.award_id = awards.id) s_award on (s.id =  s_award.scholarship_id) left join (select sm.scholarship_id, sms.name as school, sm.year_level, sm.term, smc.name as course, sm.school_year, smst.name as scholar_type from semesters as sm  join (select MAX(id) as id, scholarship_id from semesters group by scholarship_id) as ism on sm.`scholarship_id` = ism.scholarship_id AND sm.id = ism.id left join schools as sms on sms.id = sm.school_id left join courses as smc on smc.id = sm.course_id left join scholarship_types as smst on smst.id = sm.`scholarship_type_id` ) as semester_d on semester_d.scholarship_id = s.id",
        "index":"galactus_production",
        "type":"scholarships"
        }
     }'


## Create for st.name  as scholarship_type_name
curl -XPUT "http://127.0.0.1:9200/_river/sem_river/_meta" -d '{
    "type" : "jdbc",
    "jdbc" : {
        "url" : "jdbc:mysql://localhost:3306/galactus_edbms",
        "user" : "root",
        "password" : "root",
        "sql" : "select a.amount as allowances, if(semesters.scholarship_type_id = \"8\", \"DSWD\", \"CHED\") as scholarship_type_name, semesters.school_level as school_level, semesters.is_probation as is_probation,  r.city_municipality as cityMunicipality, r.barangay_name as barangay, semesters.id as _id, semesters.uuid as uuid,semesters.status as status, DATE_FORMAT(semesters.created_at, \"%Y-%m-%d\") as created_at, scholarships.status as scholarship_status, semesters.school_year as school_year, semesters.year_level as year_level, semesters.term as term, semesters.gwa as `gwa`, semesters.subject_count as subject_count, c.name as course_name, r.last_name as last_name, r.first_name as first_name, r.middle_name as middle_name, r.full_name as resident_full_name, scholarships.id as scholarship_id, schools.name as school_name, scholarships.comment as comment, scholarships.father_name as father_name, scholarships.mother_name as mother_name, scholarships.guardian_name as guardian_name, referrer.full_name as referrer_name, client.full_name as client_name, r.mobile as scholar_contact_no, scholarships.parents_contact_no as parents_contact_no, scholarships.guardian_contact_no as guardian_contact_no, scholarships.others_contact_no as others_contact_no from semesters left join scholarships on scholarships.id = semesters.scholarship_id left join (select residents.id as id, residents.first_name, residents.mobile, residents.full_name, residents.last_name, residents.middle_name,  cm.name as city_municipality, b.name as barangay_name from residents left join barangays as b on b.id = residents.barangay_id left join cities_municipalities as cm on cm.id = residents.city_municipality_id ) as r on r.id = scholarships.resident_id left join courses as c on c.id = semesters.course_id left join schools on schools.id =  semesters.school_id left join residents as client on client.id = client_id left join residents as  referrer on referrer.id = referrer_id left join allowances as a on  a.semester_id = semesters.id",
        "index":"galactus_production",
        "type":"semesters"
        }
     }'

